<?php

namespace Drupal\delta_data_testing\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class DeltaDataTestingConfigForm.
 */
class DeltaDataTestingConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'delta_data_testing.config',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'delta_data_test_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('delta_data_testing.config');
    $form['test_data_tab'] = [
      '#type' => 'details',
      '#title' => $this->t('Data Testing Tab'),
      '#open' => FALSE,
      '#weight' => 4,
    ];
    $form['test_data_tab']['data_test_mode'] = [
      '#type' => 'select',
      '#title' => $this->t('Set test mode on / off'),
      '#default_value' => $config->get('sync.data_test_mode'),
      '#options' => ['on' => 'ON', 'off' => 'OFF'],
      '#default_value' => $config->get('data_test_mode'),
      '#description' => $this->t("For development and testing purpose, please enable this data test mode, <br /><b><font color='red'>Post testing please turn off test mode.</font> </b>"),
    ];

    $form['test_data_tab']['test_json'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Paste API Response Data'),
      '#default_value' => $config->get('test_json'),
      '#cols' => 70,
      '#rows' => 40,
      '#description' => $this->t('According to your requirement, please modify this JSON data for your testing purpose.'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $jsonData = $form_state->getValue('test_json');
    $data = json_decode($jsonData);
    if (is_null($data)) {
        $form_state->setErrorByName('test_json', $this->t('Please enter valid JSON'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $this->config('delta_data_testing.config')
    ->set('test_json', $form_state->getValue('test_json'))
    ->set('data_test_mode', $form_state->getValue('data_test_mode'))
    ->save();
  }

}

<?php

namespace Drupal\delta_lms_sso;

use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\Core\Config\ConfigFactoryInterface;
use GuzzleHttp\ClientInterface;
use Drupal\user\Entity\Role;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\RequestException;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\Serializer\Serializer;

class ServicesLms {

  /**
   * @var \Drupal\Core\Entity\EntityStorageInterface|mixed|object
   */
  protected $entityTypeManager;

  protected $accountId;

  protected $currentUser;

  protected $priorityAcces;

  /**
   * Logger Factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactory
   */
  protected $loggerFactory;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $customEntityTypeManager;

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $userConfigFactory;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityManager;

  /**
   * Guzzle\Client instance.
   *
   * @var \Guzzle\Client
   */
  protected $httpClient;

  /**
   * @var \Symfony\Component\Serializer\Serializer
   */
  protected $serializer;

  /**
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $current_user;

  /**
   * @var \Drupal\Core\Logger\LoggerChannelFactory
   */
  protected $loggerChannelFactory;

  /**
   * @param \Drupal\Core\Entity\EntityTypeManager $entityTypeManager
   * @param \Drupal\Core\Session\AccountProxyInterface $currentUser
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $customEntityTypeManager
   * @param \Drupal\Core\Logger\LoggerChannelFactory $loggerFactory
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   * @param \GuzzleHttp\ClientInterface $httpClient
   * @param \Symfony\Component\Serializer\Serializer $serializer
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   */
  public function __construct(EntityTypeManager $entityTypeManager, AccountProxyInterface $currentUser, EntityTypeManagerInterface $customEntityTypeManager, LoggerChannelFactory $loggerFactory, ConfigFactoryInterface $configFactory, ClientInterface $httpClient, Serializer $serializer, MessengerInterface $messenger) {
    $this->current_user = $currentUser;
    $this->customEntityTypeManager = $customEntityTypeManager;
    $this->loggerChannelFactory = $loggerFactory;
    $this->loggerFactory = $loggerFactory->get('checkout_flow');
    $this->configFactory = $configFactory->get('delta_services.deltaservicesconfig');
    $this->userConfigFactory = $configFactory->get('delta_lms_sso.delta_lms_sso_config');
    $this->entityManager = $entityTypeManager;
    $this->httpClient = $httpClient;
    $this->serializer = $serializer;
    $this->messenger = $messenger;

    try {
      $this->entityTypeManager = $entityTypeManager->getStorage('user_role');
    } catch (\Exception $error) {
      $this->loggerChannelFactory->get('delta_lms_sso')->alert(t('@err', ['@err' => $error->__toString()]));
      $this->messenger->addWarning(t('Unable to proceed. Please contact an administrator. Error code: 8FBDC239-@line-@time', ['@line' => __LINE__, '@time' => time()]));
    }
  }

  /**
   * Http Client function to make request to the API.
   *
   * @param $url
   * @param $method
   * @param $user
   *
   * @return false|mixed|void
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function getApiRequestCa($url, $method, $user) {
    // Skilljar API.
    $this->loggerFactory->notice('API_Request: @message', [
      '@message' => $url,
    ]);

    $client = \Drupal::httpClient();
    $xmlRequest = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:mas="http://mascocanada.com/">
      <soapenv:Header/>
        <soapenv:Body>
          <mas:UserInfo>
              <!--Optional:-->
              <mas:UserName>' . $user . '</mas:UserName>
          </mas:UserInfo>
        </soapenv:Body>
    </soapenv:Envelope>';

    $options = [
      'body'    => $xmlRequest,
      'headers' => [
        "Content-Type" => "text/xml",
        "accept" => "*/*",
        "accept-encoding" => "gzip, deflate"
      ]
    ];

    try {
      $request = $client->request($method, $url, $options);
      $code = $request->getStatusCode();

      if ($code == 200 || $code == 201) {
        $body = $request->getBody()->getContents();

        // Extract content as an array.
        $response = $this->serializer->decode($body, 'xml');
        $this->loggerFactory->notice('API_Response_@message', [
          '@message' => json_encode($body),
        ]);
        $response['username'] = $user;

        return $response;
      }
    } catch (RequestException $e) {
      watchdog_exception('delta_lms_sso', $e);
      $this->loggerFactory->notice('USR_VPN_Connection_ERR_@message', [
        '@message' =>  ' Connection Error',
      ]);

      return FALSE;
    }

    if (NULL === $request) {
      $responseData = ['Web service connectivity issue'];
      $this->loggerFactory->notice('USR_VPN_Connection_ERR_@message', [
        '@message' =>  ' Connection Error',
      ]);
    }
  }

  /**
   * Http Client function to make request to the API.
   *
   * @param $url
   * @param $key
   * @param $method
   * @param $obj
   * @param array $MoreHeaders
   * @param string $requested_from
   *
   * @return false|mixed|void
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function getApiRequest($url, $key, $method, $obj, $MoreHeaders = [], $requested_from = 'USER_API') {
    // Skilljar API.
    $this->loggerFactory->notice('API_Request: @message', [
      '@message' => $url,
    ]);

    $client = \Drupal::httpClient();

    $encoded_api_key = 'Basic ' . base64_encode($key . ':');
    $options = [
      'connect_timeout' => 60,
      'headers' => array(
        'FORWARDED' => null,
        'Authorization' =>  $encoded_api_key,
      ),
      'verify' => FALSE,
      'http_errors' => 1
    ];

    if (count($obj) > 0) {
      $options['body'] = json_encode($obj);
    }

    if ($requested_from == 'GET_CREATE' || $requested_from == 'UPDATE_USER' || $requested_from == 'UPDATE_SIGN_UP_FIELD' || $requested_from == 'GET_FIELDS') {
      $options['headers']['Content-Type'] = 'application/json';
    }

    try {
      $request= $client->request($method, $url, $options);
      $code = $request->getStatusCode();

      if ($code == 200 || $code == 201) {
        $body = $request->getBody()->getContents();
        $response = json_decode($body, TRUE);
        $this->loggerFactory->notice('API_Response_@message', [
          '@message' => json_encode($response),
        ]);

        return $response;
      }
    } catch (RequestException $e) {
      watchdog_exception('delta_lms_sso', $e);
      $this->loggerFactory->notice('USR_VPN_Connection_ERR_@message', [
        '@message' => $requested_from . ' Connection Error',
      ]);

      return FALSE;
    }

    if (NULL === $request) {
      $responseData = ['Web service connectivity issue'];
      $this->loggerFactory->notice('USR_VPN_Connection_ERR_@message', [
        '@message' => $requested_from . ' Connection Error',
      ]);
    }
  }

  /**
   * Check if user exits if not create him in skilljar LMS.
   *
   * @param $domain
   * @param $key
   * @param $current_user
   * @param $api_url
   *
   * @return false|mixed|void
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function getCreateUser($domain, $key, $current_user, $api_url) {
    $arr_user = [
      'user' => [
        'email' => $current_user['user']['email'],
        'first_name' => $current_user['user']['first_name'],
        'last_name' => $current_user['user']['last_name']
      ],
    ];

    // Send API Request
    $url = $api_url . "domains/" . $domain . "/users";

    $lms_user = $this->getApiRequest($url, $key ,'POST', $arr_user, [], 'GET_CREATE');
    return $lms_user;
  }

  /**
   * Check for update user fields in Skilljar LMS.
   *
   * @param $domain
   * @param $key
   * @param $current_user
   * @param $lms_user
   * @param $api_url
   *
   * @return false|mixed|void
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function updateUser($domain, $key, $current_user, $lms_user, $api_url) {
    $arr_user = [
      'email' => $current_user['user']['email'],
      'first_name' => $current_user['user']['first_name'],
      'last_name' => $current_user['user']['last_name']
    ];

    // Send API Request
    $url = $api_url . "users/" . $lms_user['user']['id'];
    $lms_user_updated = $this->getApiRequest($url, $key, 'PUT', $arr_user, [], 'UPDATE_USER');

    return $lms_user_updated;
  }

  /**
   * Get the url to do the SSO.
   *
   * @param $domain
   * @param $key
   * @param $current_user
   * @param $lms_user
   * @param $country
   * @param $api_url
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function updateSignUpFields($domain, $key, $current_user, $lms_user, $country, $api_url) {
    // Get related fields to the lms sso with the corresponding prefix.
    $url =  $api_url . 'domains/' . $domain . '/signup-fields';
    $get_fields = $this->getApiRequest($url, $key ,'GET', [], [], 'GET_FIELDS');

    // Send sign up field values to the LMS.
    foreach ($get_fields['results'] as $sign_up_field) {
      //Get user info for the corresponding field
      $user_field_content = $current_user['signup_fields'][$sign_up_field['label']];

      if (empty($user_field_content)) {
        $user_field_content = 'false';
      }

      if ($user_field_content === false) {
        $user_field_content = 'false';
      }

      if ($user_field_content === true) {
        $user_field_content = 'true';
      }

      if (($country == 2) || $country == 1) {
        $data = [
          "signup_field" => [
            "id" => $sign_up_field['id'],
          ],
          "value" => $user_field_content,
        ];
        // Send API Request
        $url =  $api_url . 'domains/' . $domain . '/users/' . $lms_user['user']['id'] . '/signup-fields';
        $update_fields = $this->getApiRequest($url, $key ,'POST', $data, [], 'UPDATE_SIGN_UP_FIELD');
      }
    }
  }

  /**
   * Get the url to do the SSO.
   *
   * @param $domain
   * @param $token
   *
   * @return string
   */
  public function loginLmsSso($domain, $token) {
    if (!empty($token)) {
      // if token exits sends to Skilljar
      $url = 'https://' . $domain . '/auth/login/callback?token=' . $token;
    } else {
      $url = "internal:/prouser/login";
    }

    return $url;
  }

  /**
   * Method function to start call to the API.
   *
   * @param $user_account_mgmt
   *
   * @return string
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function callApi($user_account_mgmt) {

    // Call admin page configuration
    $api_url = $this->userConfigFactory->get('elearning_api');
    $domain = $this->userConfigFactory->get('elearning_domain');
    $key = $this->userConfigFactory->get('elearning_key');
    $country = $this->userConfigFactory->get('elearning_country');

    if ($country == 2) {
      $user_account_mgmt = $user_account_mgmt["soap:Body"]["UserInfoResponse"]["UserInfoResult"]["userInfo"]["user"];
    }

    // Set up data from User Management
    if ($country == 1) {
      $current_user = [
        'user' => [
          'email' => $user_account_mgmt['email'],
          'first_name' => $user_account_mgmt['firstName'],
          'last_name' => $user_account_mgmt['lastName'],
        ],
        'signup_fields' => [
          'User name' => $user_account_mgmt['username'],
          'City' => ($user_account_mgmt['city'] == FALSE) ? "" : $user_account_mgmt['city'],
          'Rep agency' => $user_account_mgmt['agency'],
          'User type' => $user_account_mgmt['groupDescription'],
          'State' => ($user_account_mgmt['state'] == FALSE) ? "" : $user_account_mgmt['state'],
          'Zip' => ($user_account_mgmt['zip'] == FALSE) ? "" : $user_account_mgmt['zip'],
          'Ferguson Flag' => $user_account_mgmt['ferguson'],
          'Rewards Flag' => $user_account_mgmt['rewards'],
          'Company' => $user_account_mgmt['company'],
        ]
      ];
    } else {
      $current_user = [
        'user' => [
          'email' => $user_account_mgmt['email'],
          'first_name' => $user_account_mgmt['firstName'],
          'last_name' => $user_account_mgmt['lastName'],
        ],
        'signup_fields' => [
          'User name' => $user_account_mgmt['username'],
          'City' => ($user_account_mgmt['city'] == FALSE) ? "" : $user_account_mgmt['city'],
          'Rep agency' => $user_account_mgmt['agency'],
          'User type' => $user_account_mgmt['roles']['role']['roleName'],
          'State' => ($user_account_mgmt['province'] == FALSE) ? "" : $user_account_mgmt['province'],
          'Zip' => ($user_account_mgmt['postalCode'] == FALSE) ? "" : $user_account_mgmt['postalCode'],
          'Rewards Flag' => $user_account_mgmt['rewardseligible'],
          'Company' => $user_account_mgmt['companyName'],
        ]
      ];
    }

    // Call to get or create user in the LMS and user comes back from Skilljar API with token
    $lms_user = $this->getCreateUser($domain, $key, $current_user, $api_url);

    // Update user in LMS with the data from UM
    $lms_user_updated = $this->updateUser($domain, $key, $current_user, $lms_user, $api_url);

    // Create sign up fields in the LMS with UM data
    $this->updateSignUpFields($domain, $key, $current_user, $lms_user, $country, $api_url);

    // Send token back to the submit for redirection
    return $this->loginLmsSso($domain, $lms_user['login_token']);
  }
}

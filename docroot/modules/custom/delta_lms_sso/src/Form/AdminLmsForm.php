<?php
namespace Drupal\delta_lms_sso\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * An User management API Config.
 */
class AdminLmsForm extends ConfigFormBase
{

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs a new User Account ApiSettingsForm object.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    // parent::__construct($config_factory);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'delta_lms_sso.delta_lms_sso_config',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'delta_lms_sso_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('delta_lms_sso.delta_lms_sso_config');

    $form['elearning'] = [
      '#type' => 'details',
      '#title' => $this->t("Elearning Details"),
    ];
    $form['elearning']['elearning_api'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Elearning API'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('elearning_api'),
    ];
    $form['elearning']['elearning_canada_endpoint'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Canada API'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('elearning_canada_endpoint'),
    ];
    $form['elearning']['elearning_domain'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Elearning Domain'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('elearning_domain'),
    ];
    $form['elearning']['elearning_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Skilljar key'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('elearning_key'),
    ];

    $form['elearning']['elearning_country'] = [
      '#title' => $this->t('Country'),
      '#type' => 'select',
      '#description' => 'Select the country for the domain.',
      '#default_value' => $config->get('elearning_country'),
      '#options' => [
        t('- Select country -'), 
        t('United States'),
        t('Canada')
      ],
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $this->config('delta_lms_sso.delta_lms_sso_config')
      
      ->set('elearning_domain', $form_state->getValue('elearning_domain'))
      ->set('elearning_key', $form_state->getValue('elearning_key'))
      ->set('elearning_country', $form_state->getValue('elearning_country'))
      ->set('elearning_api', $form_state->getValue('elearning_api'))
      ->set('elearning_canada_endpoint', $form_state->getValue('elearning_canada_endpoint'))
      ->save();

  }
}

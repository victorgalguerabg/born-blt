<?php

namespace Drupal\delta_lms_sso\Form;

use Drupal\Core\Database\Database;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\delta_lms_sso\ServicesLms;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\user_account_mgmt\UserAccountManagement;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\user\Entity\User;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Request;
use Drupal\checkout_services\CheckoutServices;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\user\UserStorageInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Implementation of User Registration Form.
 */
class LmsButtonForm extends FormBase {

  /**
   * @var \Drupal\delta_lms_sso\ServicesLms
   */
  protected $servicesLms;

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

    /**
   * @var \Drupal\user_account_mgmt\UserAccountManagement
   */
  protected $userAccountManagement;

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $generalConfig;

  /**
   * @var \Drupal\user_account_mgmt\UserAccountManagement
   */
  protected $userAccountMgmt;

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $userAccountFactory;

  /**
   * Symfony\Component\HttpFoundation\RequestStack definition.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * @var \Drupal\checkout_services\CheckoutServices
   */
  protected $services;

  /**
   * Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Drupal\Core\Session\AccountProxyInterface
   */
  protected $user;

  /**
   * @var \Drupal\Core\TempStore\PrivateTempStoreFactory
   */
  protected $tempStoreFactory;

  /**
   * @var \Drupal\Core\TempStore\PrivateTempStoreFactory
   */
  protected $userSession;

  /**
   * Constructs a User Registration Form object.
   */
  public function __construct( ServicesLms $userlms, MessengerInterface $messenger,  ConfigFactoryInterface $configFactory, UserAccountManagement $usermanagement, RequestStack $request_stack,CheckoutServices $checkoutServices, AccountProxyInterface $currentUser, UserStorageInterface $user, PrivateTempStoreFactory $temp_store_factory) {
    $this->requestStack = $request_stack;
    $this->servicesLms = $userlms;
    $this->configFactory = $configFactory->get('delta_lms_sso.delta_lms_sso_config');
    $this->generalConfig = $configFactory->get('delta_services.deltaservicesconfig');
    $this->userAccountFactory = $configFactory->get('user_account_mgmt.user_account_mgmt_config');
    $this->userAccountMgmt = $usermanagement;
    $this->messenger = $messenger;
    $this->services = $checkoutServices;
    $this->currentUser = $currentUser;
    $this->user = $user;
    $this->tempStoreFactory = $temp_store_factory;
    $this->userSession = $this->tempStoreFactory->get('loginData');
    $this->tempStore = $this->tempStoreFactory->get('delta_lms_sso');
  }

    /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('delta_lms_sso.delta_lms_sso_services'),
      $container->get('messenger'),
      $container->get('config.factory'),
      $container->get('user_account_mgmt.user_account_management'),
      $container->get('request_stack'),
      $container->get('checkout_services.checkoutflow'),
      $container->get('current_user'),
      $container->get('entity_type.manager')->getStorage('user'),
      $container->get('tempstore.private')
    );
  }

  /**
   * Implementation User Registration getFormId().
   * @return string
   */
  public function getFormId() {
    return 'sso_button_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {

    $username = \Drupal::request()->query->get('user');

    //Creating submit button for the SSO.
    $current_user_status = $this->currentUser->isAuthenticated();

    $form['username'] = [
      '#type' => 'hidden',
      '#value' => $username ,
    ];

    if ($current_user_status != 1) {
      $form['login_url'] = [
        '#type' => 'markup',
        '#markup' => '<a href="/prouser/login" class="button button--primary">'.$this->t('Log in to take training Courses').'</a>',
        '#weight' => '1',
      ];
    } else {
      $form['submit'] = [
        '#type' => 'submit',
        '#value' => $this->t('Take training courses'),
        '#button_type' => 'primary',
        '#attributes' => [
          'class' => [],
        ]
      ];
    }
    return $form;
  }

  /**
   * Redirect to the internal path or to the external url.
   */
  public function sendToPage(string $path) {
    $url = Url::fromUri($path);
    $destination = $url->toString();
    $response = new RedirectResponse($destination, 301);
    $response->send();
  }

  /**
   * User Registration Validation based on steps.
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * User Registration form submit.
   * @param array $form
   * @param FormStateInterface $form_state
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    // Getting current username from drupal after user logs in PRO login form.
    $username = $form_state->getValue('username');
    $country = trim($this->configFactory->get('elearning_country'));
    $canada_endpoint = trim($this->configFactory->get('elearning_canada_endpoint'));

    if (empty($username)) {
      $current_user = $this->currentUser;
      // $username is used for authentication, so we use AccountName instead of DisplayName
      $username = $current_user->getAccountName();
    }

    //$username = 'test';  //user for testing Canada
    if (!empty($username)) {
      if ($country == 1) {
        // Send request to UM to get user data.for US
        $userDetailsurl = trim($this->userAccountFactory->get('userValidateApi')) . $username;
        $userDetailsRequest = $this->userAccountMgmt->userValidationApiCall($userDetailsurl, 'GET','', [],'ELEARN_ACCT_USR_DETAILS');
      } else {
        // Send request to another API for Canada
        $userDetailsRequest = $this->servicesLms->getApiRequestCa($canada_endpoint, "POST", $username);
      }

      // Redirect user to Skilljar platform with the corresponding url and token to open session (SSO).
      $url_lms = $this->servicesLms->callApi($userDetailsRequest);
      $this->sendToPage($url_lms);
    } else {
      // use below if you have to redirect on your known url
      $path = 'internal:/prouser/login';
      $this->sendToPage($path);
    }
  }
}

<?php

/**
 * @file
 * Contains \Drupal\delta_lms_sso\Plugin\Block\LmsButtonBlock.
 */

namespace Drupal\delta_lms_sso\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;


/**
 * Provides a 'LMS SSO button' Block.
 *
 * @Block(
 *   id = "Lms SSO button_block",
 *   admin_label = @Translation("Lms SSO button Block"),
 *   category = @Translation("Lms SSO button Block"),
 * )
 */
class LmsButtonBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The form builder.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface.
   */
  protected $formBuilder;

  /**
   * Constructs a new UserPasswordBlock plugin.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Form\FormBuilderInterface $formBuilder
   *   The form builder.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, FormBuilderInterface $formBuilder) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->formBuilder = $formBuilder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('form_builder')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];
    $build['form'] = $this->formBuilder->getForm('Drupal\delta_lms_sso\Form\LmsButtonForm');
    /*$build['#markup'] = $this->t('Skiller LMS SSO button');*/ 
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function blockAccess(AccountInterface $account) {

    $current_user = \Drupal::currentUser();
    $rol = \Drupal\user\Entity\User::load($current_user->id())->getRoles(true);

    if(empty($rol)){
      $roles = \Drupal\user\Entity\User::load($current_user->id())->getRoles();
    }else{
      $roles = $rol;
    }

    if (in_array("administrator", $roles)) {
      return AccessResult::allowedIfHasPermission($account, 'access content');
    }

    $roles_results = user_role_permissions($roles);
    foreach ($roles_results as $permissions) {
      if (in_array("access LMS SSO", $permissions)) {
        return AccessResult::allowedIfHasPermission($account, 'access content');
      }
    }

    return AccessResult::forbidden();
    
  }
  
}
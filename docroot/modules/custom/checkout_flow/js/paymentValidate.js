(function ($, Drupal,drupalSettings) {

    Drupal.behaviors.payment_form_validate = {
        attach: function (context, settings) {

            $(".ordersubmitbtn").css("display", "none");
            //bill_ship_address_same
            $('#edit-billshipaddresssame').on("click", function(){
                if($(this).prop("checked") === true){
                    $("#edit-paymentinfo").css("display", "none");
                    $(this).val(0);
                }
                else if($(this).prop("checked") === false){
                    $("#edit-paymentinfo").css("display", "block");
                    $(this).val(1);
                }
            });
        }
    };

    Drupal.behaviors.payment_validate = {
        attach: function (context, settings) {
            // Attach a click listener to the clear button.
            var submitBtn = document.getElementById('payment-submit-button');
            submitBtn.addEventListener('click', function () {
                var errorCount = formValidation(drupalSettings.errorConfigs);
                var iframeUrl = $('#IFrame').attr('src');
                var access_token = $('#edit-access-token').val();
                if (iframeUrl) {
                    $XIFrame.validate({
                        iFrameId: 'dieCommFrame',
                        targetUrl: iframeUrl,
                        autosizeheight: true,
                        onValidate: function(e) {
                            if (e) {
                                if(errorCount === 0) {
                                    $XIFrame.submit({
                                        autosizeheight: true,
                                        iFrameId: 'dieCommFrame',
                                        targetUrl: iframeUrl,
                                        onSuccess: function (msg) {
                                            var message = JSON.parse(msg);
                                            if (message && message.data.HasPassed) {
                                                // all good, send the form:
                                                $.ajax({
                                                    url: "/paymetric/response_packet/" + access_token,
                                                    contentType: "application/xml",
                                                    dataType: 'xml',
                                                    async: false,
                                                    success: function (result) {
                                                        if ($(result).find('RequestError').length !== 0) {
                                                            $(".error-on-paymetric").html("<span>Something Went wrong, page will reload after 3 seconds, please try again.");
                                                            $('html, body').animate({scrollTop: 0}, 500);
                                                            setTimeout(function () {
                                                                location.reload(true);
                                                            }, 3000);
                                                        }
                                                        else {
                                                            if ($(result).find('FormField')) {
                                                                var cardResponse = "";
                                                                $(result).find('FormField').each(function () {
                                                                    if ("Card Type" === $(this).find('Name').text()) {
                                                                        cardResponse += $(this).find('Value').text();
                                                                    }
                                                                    if ("Expiration Month" === $(this).find('Name').text()) {
                                                                        cardResponse += "|" + $(this).find('Value').text();
                                                                    }
                                                                    if ("Expiration Year" === $(this).find('Name').text()) {
                                                                        cardResponse += "|" + $(this).find('Value').text();
                                                                    }
                                                                    if ("Card Number" === $(this).find('Name').text()) {
                                                                        cardResponse += "|" + $(this).find('Value').text();
                                                                    }
                                                                    if ("Card Holder Name" === $(this).find('Name').text()) {
                                                                        cardResponse += "|" + $(this).find('Value').text();
                                                                    }
                                                                    if ("Card Security Code" === $(this).find('Name').text()) {
                                                                        cardResponse += "|" + $(this).find('Value').text();
                                                                    }
                                                                });
                                                                $("#xml-access-token").val(cardResponse);

                                                                setTimeout(function () {
                                                                    $('#edit-submit').trigger('click');
                                                                }, 1000);
                                                            }
                                                        }
                                                    },
                                                    error: function (result) {
                                                        $(".error-on-paymetric").html("<span>Token expired, page will reload after 3 seconds, please try again.");
                                                        $('html, body').animate({scrollTop: 0}, 500);
                                                        setTimeout(function () {
                                                            location.reload(true);
                                                        }, 3000);
                                                    }
                                                });
                                            }
                                            else {
                                                $('html, body').animate({scrollTop: 0}, 500);
                                            }
                                        },
                                        onError: function (msg) {
                                            $(".error-on-paymetric").html("<span>Token expired, page will reload after 3 seconds, please try again.");
                                            $('html, body').animate({scrollTop: 0}, 500);
                                            setTimeout(function () {
                                                location.reload(true);
                                            }, 3000);
                                        }
                                    });
                                }
                            }
                        }
                    });
                }

            }, false);

            // Billing Form Validation.
            function formValidation(errorMsg) {
                var i = 0;
                if($("#edit-billshipaddresssame").val() == 1) {

                    var fullname = $("#edit-paymentfullname").val();
                    var zipcode = $("#edit-paymentzipcode").val();
                    var city = $("#edit-paymentcity").val();
                    var address = $("#edit-paymentaddressline").val();

                    // Name Validation.
                    if(!fullname.match(/^[a-zA-Z ]+$/) || fullname.length == 0) {
                        i++;
                    }

                    // Address Validation.
                    if(!address.match(/^[0-9a-zA-Z,\/ ]+$/) || address.length == 0) {
                        i++;
                    }

                    // City Validation.
                    if(!city.match(/^[a-zA-Z ]+$/) || city.length == 0) {
                        i++;
                    }

                    // State Validation.
                    if($("#edit-paymentstate").val() === "") {
                        i++;
                    }

                    // ZipCode Validation.
                    if(!zipcode.match(/^[0-9]+$/) || zipcode.length == 0) {
                        i++;
                    }
                }

                if($("#edit-accept-order-details").prop("checked") === false){
                    i++;
                }

                return i;
            }
        }
    };
})(jQuery, Drupal, drupalSettings);


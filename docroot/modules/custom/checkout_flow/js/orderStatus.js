(function ($, Drupal, drupalSettings) {

    Drupal.behaviors.shippingFormValidate = {
        attach: function (context, settings) {

            $( document ).ready(function() {
                $('head').append('<script> dataLayer.push({\n' +
                    '            \'event\': "checkout",\n' +
                    '            \'ecommerce\': {\n' +
                    '              \'purchase\': {\n' +
                    '                \'actionField\': '+drupalSettings.headObj+',\n' +
                    '                \'products\': '+drupalSettings.productObj+'\n' +
                    '              }\n' +
                    '            }\n' +
                    '          })</script>');
            });

            $(".order-status-btn").once().on("click", function(e){
                e.preventDefault();
                var OrderNo = $("#edit-ordernumber").val();
                var email = $("#edit-email").val();
                var zip = $("#edit-zipcode").val();

                var errCnt = 0;
                var errMsg = "";
                if (OrderNo == "") {
                    errMsg += "<label>"+settings.errorMsg.orderDetail+"</label>";
                    errCnt++;
                }

                if (email == "") {
                    errMsg += "<label>"+settings.errorMsg.orderDetail+"</label>";
                    errCnt++;
                }
                else {
                    var filter = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
                    if (!filter.test(email)) {
                        errCnt++;
                    }
                }

                if (zip == "") {
                    errMsg += "<label>"+settings.errorMsg.zipcode+"</label>";
                    errCnt++;
                }
                else {
                    var zipRegex = /^\d{5}$/;
                    if (!zipRegex.test(zip)) {
                       errCnt++;
                    }
                }

                if (errCnt == 0){
                    $(".errMsg").html("");
                    $.ajax({
                        url: "/ecommerce/ajax-order-status",
                        type: 'POST',
                        data: { "orderno": OrderNo, "email": email, "zip": zip },
                        async: false,
                        success: function (result) {
                            var orderStatusTab = "";
                            if (result.length != 0) {
                                orderStatusTab += "<div class='your_orders'><h1 class='iconbox_title'>Your Orders</h1>" +
                                    "<div class='grid our_Order_table'><table>";
                                $.each(result, function (key,val) {
                                    var orderStatus = val['status'];
                                    if(orderStatus == 'Partially processed'){
                                        orderStatus = 'Order Received – In Process';
                                    }
                                    orderStatusTab += "<tr><td><div class='title'>"+settings.tableHeads.orderPlaced+"</div><div class='value'>"+val['datePlaced']+"</div></td>";
                                    orderStatusTab += "<td><div class='title'>"+settings.tableHeads.orderNumber+"</div><div class='value'>"+val['orderNumber']+"</div></td>";
                                    orderStatusTab += "<td><div class='title'>"+settings.tableHeads.orderStatus+"</div><div class='value'>"+orderStatus+"</div></td>";
                                    orderStatusTab += "<td><div class='title'>"+settings.tableHeads.orderTotal+"</div><div class='value'>$"+val['orderTotal']+"</div></td>";
                                    orderStatusTab += "<td><div class='title'></div><div class='value'><a class='cta-link cta-link--sm has-icon--right' href='/ecommerce/order-details?orderNo="+val['encodedOrderNo']+"&companyNo="+val['encodedCompanyNumber']+"&docType="+val['encodedDocType']+"'>"+settings.orderStatusMsg+"</a></div></td></tr>";
                                });
                                orderStatusTab += "</table></div></div>";
                            }
                            else {
                                orderStatusTab += "<h2> NO RECORDS FOUND.</h2>";
                            }
                            $(".track_order .order-status-form").html("Track A Different Order");
                            $(".track_order").find("p:first").html("");
                            $(".order-status-results").html(orderStatusTab);
                            $('html, body').animate({scrollTop:0},500);
                        },
                        error: function (result) {
                        }
                    });
                }
            });
        }
    };
    
})(jQuery, Drupal, drupalSettings);

(function ($, Drupal, drupalSettings) {
  var maxLimit;
    var letterNumber = /[^a-zA-Z]/g;
    var qtyValue;

     $(document).ajaxComplete(function(event, xhr, settings) {
      //Disable the checkout button when user clicks that button for the first time.
      if (xhr.status == 200 && settings.extraData != undefined && settings.extraData._triggering_element_value !== 'undefined' && settings.extraData._triggering_element_value == "Place an order as a guest") {
        var uri = settings.url;
        var clean_uri = uri.substring(0, uri.indexOf("?"));
        if($("input.checkout-guest-order").length && clean_uri != undefined &&   clean_uri == '/ecommerce/checkout' && settings.extraData._triggering_element_name == 'op' && settings.type == 'POST'){
          //The “Please Wait” message along with the loading spinner needs to stay up until the page redirects.
          if($("div.checkout-guest-order").length){
            $("div.checkout-guest-order").append('<div class="guest-login-show-loader"></div>');
            if($("div.guest-login-show-loader").length){
              $("div.guest-login-show-loader").addClass("ajax-progress-throbber").html('<div class="throbber">&nbsp</div><div class="message">Please wait.</div>');
            }
          }
          $("input.checkout-guest-order").prop('disabled', true);
        }
      }
     });

    $(".numeric-stepper__input").on('keypress keyup blur', function() {
      if(!$(this).val().match(letterNumber)){
        $(this).val( $(this).val().replace(letterNumber,'') );
      }
    });

    // hide common Actual price block in Bottom of the page for Recommerce pproducts.
    if($(".pdp-section").hasClass("recommerce-block")) {
      $("#block-footerdisclaimer").css("display","none");
    }

    // Set Maximum quantity for products
    if(drupalSettings.commerce != "" && drupalSettings.commerce != undefined) {
      $(".cart-form .commerce .numeric-stepper__input").attr("data-max",drupalSettings.commerce.quantity);
      if($(".numeric-stepper").hasClass("recommerce")) {
        $("#block-footerdisclaimer").css("display","none");
      }
      else {
        $(".pdp-prod-buttons .numeric-stepper .numeric-stepper__input").attr("data-max",drupalSettings.commerce.quantity);
      }
    }

    if(drupalSettings.recommerce != "" && drupalSettings.recommerce != undefined) {
      $(".cart-form .recommerce .numeric-stepper__input").attr("data-max",drupalSettings.recommerce.quantity);
    }

    $(".js-numeric-increment").on("click", function(){
      var maxLimitVal = $(this).parents(".numeric-stepper").find(".numeric-stepper__input").attr('data-max');
      var qtyVal = $(this).parents(".numeric-stepper").find(".numeric-stepper__input").val();
      if(maxLimitVal == qtyVal) {
        if($(this).parents(".numeric-stepper").find(".quantity-err").length == 0) {
          if($(this).parents(".numeric-stepper").find(".recommerce").length == 1 || $(this).parent(".numeric-stepper").hasClass("recommerce")) {
            $(this).parents(".numeric-stepper").append("<span class='quantity-err'>"+drupalSettings.recommerce.errormsg+"</span>");
          }
          else {
            $(this).parents(".numeric-stepper").append("<span class='quantity-err'>"+drupalSettings.commerce.errormsg+"</span>");
          }
        }
      }
      else {
        $(".quantity-err").remove();
      }
    });

    // Remove Error Message
    $(".js-numeric-decrement").on("click", function(){
      if($(this).parents(".numeric-stepper").find(".quantity-err")) {
        $(".quantity-err").remove();
      }
    });


    // Qty filed validation
    $(".numeric-stepper__input").focusout(function() {
      maxLimit = parseInt($(this).attr('data-max'));
      qtyValue = $(this).val();
      // not exceed thee max value
      if ( parseInt($(this).val()) > maxLimit) {
        $(this).val(maxLimit);
        if($(this).parents(".numeric-stepper").find(".quantity-err").length == 0) {
          if($(this).parents(".numeric-stepper").find(".recommerce").length == 1 || $(this).parents(".numeric-stepper").hasClass("recommerce")) {
            $(this).parents(".numeric-stepper").append("<span class='quantity-err'>"+drupalSettings.recommerce.errormsg+"</span>");
          }
          else {
            $(this).parents(".numeric-stepper").append("<span class='quantity-err'>"+drupalSettings.commerce.errormsg+"</span>");
          }
        }
      }
      else {
        $(this).val(qtyValue);
        $(".quantity-err").remove();
      }
      // Qty filed replace with min value if the field is empty
      if($(this).val() == '' ) {
        $(this).val(1);
      }
  });

})(jQuery, Drupal, drupalSettings);

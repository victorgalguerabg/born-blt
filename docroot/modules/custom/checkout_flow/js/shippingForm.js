(function ($, Drupal, drupalSettings) {

    Drupal.behaviors.shippingForm = {
        attach: function (context, settings) {
            $( document ).ready(function() {
                $('#checkout-flow-shipping-form input[name="shipping_option"]:checked').trigger("click");
            });

            $('.shipping-options').once().on("click",function() {
                if($(this).val() !== "") {
                    var val = $(this).val();
                    var dataVal = $(this).data(val);
                    $(".selected-shipping-opt").val(dataVal);

                    var selectedOption = $(this).val();
                    if (selectedOption == "cgr") {
                        var currentSelectedValueCgr = $(this).attr("data-cgr");
                        var currentTotalCgr = getCurrentTotal(currentSelectedValueCgr);
                        if(currentSelectedValueCgr == 0) {
                          $(".form-shipping-option").html("Free");
                        }
                        else {
                          $(".form-shipping-option").html("$" + currentSelectedValueCgr);
                        }
                        $(".form-order-total-ship").html("$" + currentTotalCgr);
                    }

                    if (selectedOption == "cnd") {
                        var currentSelectedValue = $(this).attr("data-cnd");
                        var currentTotal = getCurrentTotal(currentSelectedValue);
                        if(currentSelectedValue == 0) {
                          $(".form-shipping-option").html("Free");
                        }
                        else {
                          $(".form-shipping-option").html("$" + currentSelectedValue);
                        }
                        $(".form-order-total-ship").html("$" + currentTotal);
                    }
                }
            });

            function getCurrentTotal(selectedOption) {
                var currentTotal = $.trim($(".form-order-subtotal").text());
                var splitTot = currentTotal.split("$");
                var subTotal = splitTot[1].replace(',','');
                var total = parseFloat(subTotal) + parseFloat(selectedOption);
                return total.toFixed(2);
            }
        }
    };
})(jQuery, Drupal, drupalSettings);

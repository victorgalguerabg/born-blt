<?php

namespace Drupal\checkout_flow\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\checkout_services\CheckoutServices;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\delta_services\DeltaService;
use Drupal\delta_cache\DeltaCacheService;
use Drupal\product_details\ProductService;
use Drupal\webform\Entity\Webform;
use Drupal\webform\Entity\WebformSubmission;
use Drupal\webform\WebformSubmissionForm;
use Symfony\Component\HttpFoundation\RequestStack;


/**
 * An Order Conformation controller.
 */
class OrderConformationController extends ControllerBase {

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * @var \Drupal\checkout_services\CheckoutServices
   */
  protected $services;

  /**
   * @var \Drupal\Core\TempStore\PrivateTempStoreFactory
   */
  protected $tempStoreFactory;

  /**
   * @var \Drupal\user\PrivateTempStore
   */
  protected $store;

  /**
   * Drupal\delta_services\DeltaService definition.
   *
   * @var \Drupal\delta_services\DeltaService
   */
  protected $deltaservice;

  /**
   * @var \Drupal\user\PrivateTempStore
   */
  protected $skuCache;

  /**
   * @var \Drupal\product_details\ProductService
   */
  protected $productService;

  /**
   * Symfony\Component\HttpFoundation\RequestStack definition.
   */
  protected $requestStack;

  /**
   * Object initiative.
   */
  public function __construct(CheckoutServices $services, ConfigFactoryInterface $configFactory, PrivateTempStoreFactory $temp_store_factory, DeltaService $deltaservice, DeltaCacheService $skuCache, ProductService $productService, RequestStack $requestStack) {
    $this->configFactory = $configFactory->get('checkout_services.checkout_configs');
    $this->services = $services;
    $this->tempStoreFactory = $temp_store_factory;
    $this->store = $this->tempStoreFactory->get('orderdetails');
    $this->deltaservice = $deltaservice;
    $this->skuCache = $skuCache;
    $this->productService = $productService;
    $this->requestStack = $requestStack;

  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('checkout_services.checkoutflow'),
      $container->get('config.factory'),
      $container->get('tempstore.private'),
      $container->get('delta_services.service'),
      $container->get('delta_cache.cache_service'),
      $container->get('product_details.service'),
      $container->get('request_stack')
    );
  }

  /**
   * Returns a render-able array for a Order Details page.
   */
  public function orderDetails() {
    $rtnArray = [];
    $queryString = $this->requestStack->getCurrentRequest()->query->all();
    if(!empty(trim($queryString['orderNo'])) &&trim($queryString['companyNo'])) {
      $parmArray = [
        'orderNo' => $this->deltaservice->decrypt(trim($queryString['orderNo'])),
        'customerNo' => $this->deltaservice->decrypt(trim($queryString['companyNo'])),
        'docType' => $this->deltaservice->decrypt(trim($queryString['docType'])),
      ];
      $apiResponse = $this->services->orderDetailsWebCall($parmArray);

      if (!isset($apiResponse['Error']) && !empty($apiResponse['orderNumber'])) {
        $rtnArray['orderNo'] = $apiResponse['orderNumber'];
        $rtnArray['orderPlaced'] = date("M d, Y", substr($apiResponse['orderPlaced'], 0, 10));
        $rtnArray['orderSubTotal'] = number_format($apiResponse['orderSubTotal'], 2, '.', '');
        $rtnArray['tax'] = $apiResponse['tax'];
        $rtnArray['shippingCharge'] = $apiResponse['shippingCharge'];
        $rtnArray['orderTotal'] = number_format($apiResponse['orderTotal'], 2, '.', '');
        foreach ($apiResponse['orderItems'] as $key => $val) {
          $orderArr['src'] = $val['imageFilename'];
          $orderArr['value'] = $val['description'];
          $orderArr['order_number'] = $val['skuCode'];
          $orderArr['price'] = number_format($val['unitPrice'], 2, '.', '');
          $getOrderStatus = $val['orderStatus'];
          if($getOrderStatus == 'Not Yet Processed'){
            $getOrderStatus = $this->t('Order Received');
          }
          $orderArr['status'] = $getOrderStatus;
          $orderArr['number'] = $val['trackingNumber'];
          $orderArr['count'] = $val['quantity'];
          $orderArr['extendedPrice'] = number_format($val['extendedPrice'], 2, '.', '');
          $orderArr['order_name'] = $this->t("Part #");
          $rtnArray['orderItems'][] = $orderArr;
        }
      }
    }
    $rtnArray['tableTitles'][0]['title'] = $this->t("Item");
    $rtnArray['tableTitles'][1]['title'] = $this->t("Order Status");
    $rtnArray['tableTitles'][2]['title'] = $this->t("Tracking Number");
    $rtnArray['tableTitles'][3]['title'] = $this->t("Quantity");
    $rtnArray['tableTitles'][4]['title'] = $this->t("Price");

    $renderable = [
      '#theme' => 'order_details',
      '#order_details' => $rtnArray,
    ];
   return $renderable;
  }

  /**
   * Returns a render-able array for a Order Status page.
   */
  public function orderStatus() {
    $rtnArray = [];
    if (!empty($_POST['orderno'])) {
      $apiResponse = $this->services->orderStstusWebCall($_POST);
      if (count($apiResponse) != 0) {
        foreach ($apiResponse as $key => $val) {
          $rtnArray[] = [
            "orderNumber" => $val['orderNumber'],
            "datePlaced" => date("M d, Y", strtotime($val['datePlaced'])),
            "status" => $val['status'],
            "orderTotal" => number_format($val['orderTotal'],2),
            "companyNumber" => $val['companyNumber'],
            "documentType" => $val['documentType'],
            "encodedOrderNo" => $this->deltaservice->encrypt($val['orderNumber']),
            "encodedDocType" => $this->deltaservice->encrypt($val['documentType']),
            "encodedCompanyNumber" => $this->deltaservice->encrypt($val['companyNumber']),
          ];
        }
      }
    }
    return new JsonResponse($rtnArray);
  }

  /**
   * Returns a render-able array for a Order Status page.
   */
  public function orderConfirmation() {
    $orderData = [];
    $productJsonObj = [];
    $orderDetails = [];
    $companyNo = $this->configFactory->get('companyNumber');
    $docType = $this->configFactory->get('documentType');

    $orderObject = json_decode($this->services->gerOrderconfirmationObj(), TRUE);
    if (!empty($orderObject)) {
      $orderData = json_decode($orderObject['field_order_object'][0]['value'], TRUE);
      $orderData['encodedOrderNo'] = $this->deltaservice->encrypt($orderData['orderNumber']);
      $orderData['encodedCompanyNumber'] = $this->deltaservice->encrypt($companyNo);
      $orderData['encodedDocType'] = $this->deltaservice->encrypt($docType);

      // SHipping option config values.
      $orderData['shippingOption'] = "";
      if($orderData['shippingServiceLevel'] == 'CGR') {
        $orderData['shippingOption'] = $this->configFactory->get('cgr');
      }

      if($orderData['shippingServiceLevel'] == 'CND') {
        $orderData['shippingOption'] = $this->configFactory->get('cnd');
      }

      $this->services->changeOrderState();

      if($orderObject['field_email_subscription'][0]['value'] == 1) {
        // Notification subscription

        $exploadedName = explode(" ", $orderData['shippingAddress']['fullName']);
        $firstname = $lastname = "";
        if(count($exploadedName) > 1) {
          $lastname = $exploadedName[count($exploadedName)-1];
          foreach ($exploadedName as $key => $value) {
            if($key != (count($exploadedName)-1) ) {
              $firstname .= $value." ";
            }
          }
        }
        else {
          $firstname = $orderData['shippingAddress']['fullName'];
        }

        $salesForce = [
          'first_name' => $firstname,
          'last_name' => $lastname,
          'email' => $orderData['email'],
          'phone' => $orderData['phone'],
          'address_1' => $orderData['shippingAddress']['address1'],
          'address_2' => $orderData['shippingAddress']['address2'],
          'city' => $orderData['shippingAddress']['city'],
          'state' => $orderData['shippingAddress']['state'],
          'zipcode' => $orderData['shippingAddress']['zip5'],
          'record_type' => $this->configFactory->get('recordType'),
          'email_origin' => $this->configFactory->get('emailOrigin'),
          'organization' => $this->configFactory->get('agency'),
          'subscriber_status'=> $this->configFactory->get('subscriberStatus')
        ];
        $this->notificationSubscribe($salesForce);
      }

      $orderDetails = ['id' => $orderData['orderNumber'], "revenue" => $orderData['orderTotal'], 'tax' => $orderData['tax'], 'shipping' => $orderData['shippingCharge'] ];
      foreach ($orderData['orderItems'] as $key => $value) {
        $skuCacheDetails = $this->skuCache->deltaGetCache('PDP_'.$value['skuCode']);
        if(!empty($skuCacheDetails)) {
          $brandName = $skuCacheDetails->values->Brand;
          $collectionName = (!empty($skuCacheDetails->values->Collection) )? $skuCacheDetails->values->Collection : 'Delta - No Collection';
          $variant = (!empty($skuCacheDetails->facetFinish) )? $skuCacheDetails->facetFinish : "";
          $category = "Delta/" . $this->getProductCategory($skuCacheDetails);
        }
        else {
          $productDetails = $this->productService->productBaseDetails($value['skuCode']);
          $brandName = $productDetails->values->Brand;
          $collectionName = (!empty($productDetails->values->Collection) )? $productDetails->values->Collection : 'Delta - No Collection';
          $variant = (!empty($productDetails->facetFinish) )? $productDetails->facetFinish : "";
          $category = "Delta/" . $this->getProductCategory($productDetails);
        }

        $listPrice = $this->services->getProductListPrice($value['skuCode']);
        if(!empty($listPrice)) {
          $orderData['orderItems'][$key]['collection'] = ($listPrice['collection']) ? $listPrice['collection'] : "";
          $orderData['orderItems'][$key]['skulistprice'] = ($listPrice['listprice']) ? $listPrice['listprice'] : "";
          $orderData['orderItems'][$key]['recertified'] = ($listPrice['recertified']) ? $listPrice['recertified'] : "";
        }

        $item = [];
        $item['name'] = $value['description'];
        $item['id'] = $value['skuCode'];
        $item['price'] = $value['unitPrice'];
        $item['brand'] = strip_tags(html_entity_decode($collectionName));
        $item['category'] = $category;
        $item['variant'] = $variant;
        $item['quantity'] = $value['quantity'];
        $productJsonObj[] = $item;
      }
    }
    $renderable = [
      '#theme' => 'order_conformation',
      '#order_conformation' => $orderData,
      '#attached' => [
        'library' => 'checkout_flow/order_status',
        'drupalSettings' => [
          'productObj' => json_encode($productJsonObj),
          'headObj' => json_encode($orderDetails),
        ],
      ]
    ];
    return $renderable;
  }

  /**
   * Returns a product category to object.
   */
  public function getProductCategory($skuCacheDetails) {
    $repairpart = !empty($skuCacheDetails->values->RepairPart) ? $skuCacheDetails->values->RepairPart : 'F';
    $maincategoryfinal = "";

    if(!empty($skuCacheDetails->categories)) {
      $basecategory = [];
      foreach ($skuCacheDetails->categories as $urlalias) {
        $urlalias_seperate = explode("_", $urlalias);
        $sitefromcategory[] = $urlalias_seperate[0];
        if ($urlalias_seperate[0] == "Delta") {
          $basecategory[] = strtolower($urlalias_seperate[1]);
        }
      }

      if(!empty($sitefromcategory) && in_array("Delta", $sitefromcategory)){
        $basecategoryfinal = array_unique($basecategory);
        if ($repairpart == "T" && in_array("parts", $basecategoryfinal)) {
          $maincategoryfinal = "Parts/Repair Parts";
        }
        elseif (in_array('kitchen', $basecategoryfinal)) {
          $maincategoryfinal = "kitchen";
        }
        elseif(in_array('bathroom', $basecategoryfinal)) {
          $maincategoryfinal =  "bathroom";
        }
      }
      elseif(empty($skuCacheDetails->categories) && $repairpart == "T"){
        $maincategoryfinal = "Parts/Repair Parts";
      }
      elseif (empty($skuCacheDetails->categories) && $repairpart == "F"){
        $maincategoryfinal = "kitchen";
      }
    }
    return $maincategoryfinal;
  }

  // Email notification Subscription.
  public function notificationSubscribe($salesForce) {
    $webform_id = 'checkout_request';
     $webform = Webform::load($webform_id);
     // Create webform submission.
     $values = [
       'webform_id' => $webform->id(),
       'data' => $salesForce,
     ];

     /** @var \Drupal\webform\WebformSubmissionInterface $webform_submission */
     $webform_submission = WebformSubmission::create($values);
    try {
      $webform_submission->save();
    } catch (\Exception $error) {
      $this->loggerFactory->get('checkout_flow')->alert(t('@err', ['@err' => $error->__toString()]));
      $this->messenger->addWarning(t('Unable to proceed. Please contact an administrator. Error code: 5D109773-@line-@time', ['@line' => __LINE__, '@time' => time()]));
    }
  }

}

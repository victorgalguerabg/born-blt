<?php

namespace Drupal\checkout_flow\Plugin\views\field;

use Drupal\commerce_cart\CartManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\Plugin\views\field\UncacheableFieldHandlerTrait;
use Drupal\views\ResultRow;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;

/**
 * Defines a form element for removing the order item.
 *
 * @ViewsField("commerce_order_item_update_button")
 */
class UpdateButton extends FieldPluginBase {

  use UncacheableFieldHandlerTrait;

  /**
   * The cart manager.
   *
   * @var \Drupal\commerce_cart\CartManagerInterface
   */
  protected $cartManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  public static $entityType;

  /**
   * The entity type user.
   *
   * @var AccountProxy
   */
  public $currentUser;

  /**
   * The entity type user.
   *
   * @var AccountProxy
   */
  public static $current_User;

  /**
   * The entity type user.
   *
   * @var LoggerChannelFactoryInterface
   */
  public static $logger;

  /**
   * The entity type user.
   *
   * @var MessengerInterface
   */
  public static $messenger;

  /**
   * Constructs a new EditRemove object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\commerce_cart\CartManagerInterface $cart_manager
   *   The cart manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, CartManagerInterface $cart_manager,
    EntityTypeManagerInterface $entity_type_manager, AccountProxyInterface $currentUser,
    LoggerChannelFactoryInterface $logger,
    MessengerInterface $messenger) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->cartManager = $cart_manager;
    $this->entityTypeManager = $entity_type_manager;
    $this->currentUser = $currentUser;
    self::$current_User = $currentUser;
    self::$entityType = $entity_type_manager;
    self::$logger = $logger;
    self::$messenger = $messenger;

  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('commerce_cart.cart_manager'),
      $container->get('entity_type.manager'),
      $container->get('current_user'),
      $container->get('logger.factory'),
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function clickSortable() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getValue(ResultRow $row, $field = NULL) {
    return '<!--form-item-' . $this->options['id'] . '--' . $row->index . '-->';
  }

  /**
   * Form constructor for the views form.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function viewsForm(array &$form, FormStateInterface $form_state) {
    // Make sure we do not accidentally cache this form.
    $form['#cache']['max-age'] = 0;
    // The view is empty, abort.
    if (empty($this->view->result)) {
      unset($form['actions']);
      return;
    }

    $form[$this->options['id']]['#tree'] = TRUE;
    $index = 0;
    foreach ($this->view->result as $row_index => $row) {
      foreach ($this->view->field as $fid => $field) {
        if ($fid == 'sku') {
          $name = $field->getValue($row);
        }
      };
      $form[$this->options['id']][$row_index] = [
        '#type' => 'submit',
        '#value' => $this->t('Update'),
        '#name' => 'update-order-item-' . $row_index,
        '#update_order_item' => TRUE,
        '#row_index' => $row_index,
        '#index' => $name,
        '#attributes' => ['class' => ['update-order-item', 'use-ajax-submit']],
        '#ajax' => [ 'callback' =>  __CLASS__ . '::getTotalOrder', 'wrapper' => 'update_cart_item'],
      ];
    }
  }

  /**
   * Submit handler for the views form.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function viewsFormSubmit(array &$form, FormStateInterface $form_state) {

    $triggering_element = $form_state->getTriggeringElement();

    //if (!empty($triggering_element['#update_order_item'])) {
    if($form_state->getUserInput()['_triggering_element_value'] == 'Update') {
      $recommerceConfig = \Drupal::config('delta.max_product_qty');
      $commerceQty = $recommerceConfig->get('max_qty_delta');
      $recommerceQty = $recommerceConfig->get('max_qty_recommerce');

      $row_index = $triggering_element['#row_index'];
      $sku = $triggering_element['#index'];
      try {
        $order_storage = $this->entityTypeManager->getStorage('commerce_order');
      } catch (\Exception $error) {
        self::$logger->get('checkout_flow')->alert(t('@err', ['@err' => $error->__toString()]));
        self::$messenger->addWarning(t('Unable to proceed. Please try again leter. Please contact an administrator. Error code: C2580148-@line-@time', ['@line' => __LINE__, '@time' => time()]));
      }
      /** @var \Drupal\commerce_order\Entity\OrderInterface $cart */
      $cart = $order_storage->load($this->view->argument['order_id']->getValue());

      /** @var \Drupal\commerce_order\Entity\OrderItemInterface $order_item */
      foreach ($this->view->result as $view_row_index => $row) {
        foreach ($this->view->field as $fid => $field) {
          if ($fid == 'sku') {
            if($field->getValue($row) == $sku ) {
              $viewRowIndex = $view_row_index;
            }
          }
          if($fid == 'field_refurbished_product') {
            $furbishedValue = $field->getValue($row);
          }
        };
      }
      $order_item = $this->getEntity($this->view->result[$viewRowIndex]);
      $updatedOrderQuantity = $form_state->getValue("edit_quantity");
      $quantity = $updatedOrderQuantity[$row_index];
      if ($order_item->getQuantity() != $quantity) {
        if ($quantity > 0) {
          if($furbishedValue == 1) {
            if($quantity <= $recommerceQty) {
              $order_item->setQuantity($quantity);
            }
            else {
              $order_item->setQuantity($recommerceQty);
            }
          }
          else {
            if($quantity <= $commerceQty) {
              $order_item->setQuantity($quantity);
            }
            else {
              $order_item->setQuantity($commerceQty);
            }
          }
          $this->cartManager->updateOrderItem($order_item->getOrder(), $order_item, FALSE);
        }
        else {
          // Treat quantity "0" as a request for deletion.
          $this->cartManager->removeOrderItem($order_item->getOrder(), $order_item, FALSE);
        }
      }

      $save_cart = TRUE;
      if ($save_cart) {
        try {
          $cart->save();
        } catch (\Exception $error) {
          self::$logger->get('checkout_flow')->alert(t('@err', ['@err' => $error->__toString()]));
          self::$messenger->addWarning(t('Unable to proceed. Please try again leter. Please contact an administrator. Error code: C2580148-@line-@time', ['@line' => __LINE__, '@time' => time()]));
        }
        if (!empty($triggering_element['#show_update_message'])) {
          $this->messenger->addMessage($this->t('Your shopping cart has been updated.'));
        }
      }
    }


  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Do nothing.
  }

  /**
   * {@inheritdoc}
   */
  public static function getTotalOrder(array &$form, FormStateInterface $form_state) {
    $uid =  self::$current_User->id();
    $total_line_items = 0;
    //get quantity
    $triggering_element = $form_state->getTriggeringElement();
    $row_index = $triggering_element['#row_index'];
    $updatedOrderQuantity = $form_state->getValue("edit_quantity");
    $quantity = $updatedOrderQuantity[$row_index];

    $store_id = 1;
    $order_type = 'default';
    $cart_manager = \Drupal::service('commerce_cart.cart_manager');
    $cart_provider = \Drupal::service('commerce_cart.cart_provider');
    try {
      $store = self::$entityType->getStorage('commerce_store')->load($store_id);
    } catch (\Exception $error) {
      self::$logger->get('checkout_flow')->alert(t('@err', ['@err' => $error->__toString()]));
      self::$messenger->addWarning(t('Unable to proceed. Please try again leter. Please contact an administrator. Error code: C2580148-@line-@time', ['@line' => __LINE__, '@time' => time()]));
    }
    $cart = $cart_provider->getCart($order_type, $store);
    $total_items = count($cart-> getItems());
    foreach ($cart->getItems() as $order_item) {
      $total_line_items += $order_item->getQuantity();
    }

    $ajax_response = new AjaxResponse();
    //edit-update-button-1
    if ($quantity != 0) {
      $total = (double) explode(" ", $cart->getTotalPrice())[0];
      $currency_formatter = \Drupal::service('commerce_price.currency_formatter');
      if ($total != '')
      {
        $total_amount = $currency_formatter->format($total, 'USD'); // $2.99
      }
      $ajax_response->addCommand(new HtmlCommand('.views-element-container .order-total-line__subtotal .order-total-line-value', $total_amount));
      $ajax_response->addCommand(new HtmlCommand('.cart_count', "$total_line_items"));
    }
    else {
      $trIndex = $row_index+1;
      $ajax_response->addCommand(new InvokeCommand("table tr:nth-child($trIndex)", 'css' , array('display', 'none')));
      if ( $total_items == 0) {
        $twig = \Drupal::service('twig');
        $template = $twig->loadTemplate(\Drupal::theme()->getActiveTheme()->getPath() . '/templates/cart/commerce-cart-empty-page.html.twig');
        $templateHtml = $template->render([]);
        $ajax_response->addCommand(new HtmlCommand('.ecommerce-cart', "$templateHtml"));
      }
      $ajax_response->addCommand(new HtmlCommand('.cart_count', 0));
    }

    return $ajax_response;
  }

}

<?php

namespace Drupal\checkout_flow\Plugin\views\field;

use Drupal\commerce_cart\CartManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\Plugin\views\field\UncacheableFieldHandlerTrait;
use Drupal\views\ResultRow;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\AppendCommand;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\delta_cache\DeltaCacheService;

/**
 * Defines a form element for removing the order item.
 *
 * @ViewsField("commerce_order_item_removes_button")
 */
class RemoveButton extends FieldPluginBase {

  use UncacheableFieldHandlerTrait;

  /**
   * The cart manager.
   *
   * @var \Drupal\commerce_cart\CartManagerInterface
   */
  protected $cartManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   *
   */
  protected $entityTypeManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  public static $entityType;

  /**
   * The entity type user.
   *
   * @var AccountProxy
   */
  public $currentUser;

  /**
   * The entity type user.
   *
   * @var AccountProxy
   */
  public static $current_User;

  public static $productDetails;

  /**
   * @var \Drupal\user\PrivateTempStore
   */
  protected $skuCache;

  /**
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected static $logger;

  /**
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected static $messenger;

  /**
   * Constructs a new EditRemove object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\commerce_cart\CartManagerInterface $cart_manager
   *   The cart manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, CartManagerInterface $cart_manager,
    EntityTypeManagerInterface $entity_type_manager, AccountProxyInterface $currentUser,
    DeltaCacheService $skuCache,LoggerChannelFactoryInterface $logger, MessengerInterface $messenger) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->cartManager = $cart_manager;
    $this->entityTypeManager = $entity_type_manager;
    $this->currentUser = $currentUser;
    $this->skuCache = $skuCache;
    self::$current_User = $currentUser;
    self::$entityType = $entity_type_manager;
    $this->logger = $logger;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('commerce_cart.cart_manager'),
      $container->get('entity_type.manager'),
      $container->get('current_user'),
      $container->get('delta_cache.cache_service'),
      $container->get('logger.factory'),
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function clickSortable() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getValue(ResultRow $row, $field = NULL) {
    $entity = $this->getEntity($row);
    if ($entity == NULL) {
      return NULL;
    }
    return '<!--form-item-' . $this->options['id'] . '--' . $row->index . '-->';
  }

  /*public function getValue(ResultRow $values, $field = NULL) {
  $alias = isset($field) ? $this->aliases[$field] : $this->field_alias;
  if (isset($values->{$alias})) {
    return $values->{$alias};
  }
} */

  /**
   * Form constructor for the views form.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function viewsForm(array &$form, FormStateInterface $form_state) {
    // Make sure we do not accidentally cache this form.
    $form['#cache']['max-age'] = 0;
    // The view is empty, abort.
    if (empty($this->view->result)) {
      unset($form['actions']);
      return;
    }
    $index = 0;
    $form[$this->options['id']]['#tree'] = TRUE;
    foreach ($this->view->result as $row_index => $row) {
      foreach ($this->view->field as $fid => $field) {
        if ($fid == 'sku') {
          $name = $field->getValue($row);
        }
      };
      $form[$this->options['id']][$row_index] = [
        '#type' => 'submit',
        '#value' => $this->t('Remove'),
        '#name' => 'remove-order-item-' . $row_index,
        '#remove_order_items' => TRUE,
        '#row_index' => $row_index,
        '#index' => $name,
        '#attributes' => ['class' => ['remove-order-items', 'use-ajax-submit']],
        '#ajax' => [ 'callback' =>   __CLASS__ . '::getTotalOrder', 'event' => 'click'],
      ];
    }

  }

  /**
   * Submit handler for the views form.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function viewsFormSubmit(array &$form, FormStateInterface $form_state) {
    $form_state->setRebuild(TRUE);
    $triggering_element = $form_state->getTriggeringElement();
    if($form_state->getUserInput()['_triggering_element_value'] == 'Remove') {
      $row_index = $triggering_element['#row_index'];
      $sku = $triggering_element['#index'];
      foreach ($this->view->result as $view_row_index => $row) {
        foreach ($this->view->field as $fid => $field) {
          if ($fid == 'sku') {
              if($field->getValue($row) == $sku ) {
                $viewRowIndex = $view_row_index;
              }
          }
        };
      }
      /** @var \Drupal\commerce_order\Entity\OrderItemInterface $order_item */
      $order_item = $this->getEntity($this->view->result[$viewRowIndex]);
      $title = $order_item->getPurchasedEntity()->getTitle();
      $sku = $order_item->getPurchasedEntity()->getSku();
      $order_item->getPurchasedEntity()->getOrderItemTypeId();
      $price = explode(" ", $order_item->getTotalPrice())[0];
      try {
        $url = $order_item->getPurchasedEntity()->toUrl()->toString();
      } catch (\Exception $error) {
        self::$logger->get('checkout_flow')->alert(t('@err', ['@err' => $error->__toString()]));
        self::$messenger->addWarning(t('Unable to proceed. Please contact an administrator. Error code: 02DC5F9C-@line-@time', ['@line' => __LINE__, '@time' => time()]));
      }
      $category = "Delta/";
      $skuCacheDetails = $this->skuCache->deltaGetCache('PDP_'.$sku);
      $brandName = $skuCacheDetails->values->Brand;
      $collectionName = (!empty($skuCacheDetails->values->Collection) )? $skuCacheDetails->values->Collection : 'Delta - No Collection';
      $variant = (!empty($skuCacheDetails->facetFinish) )? $skuCacheDetails->facetFinish : "";
      if (strpos($url, "parts") !== FALSE) {
        $category .= "Parts/Repair Parts";
      }
      if (strpos($url, "kitchen") !== FALSE) {
        $category .= "Kitchen";
      }
      if (strpos($url, "bathroom") !== FALSE) {
        $category .= "Bathroom";
      }
      self::$productDetails = [
        'title'=> $title,
        'brand' => $collectionName,
        'category' => $category,
        'id' => $sku,
        'name' => $title,
        'price' => $price,
        'variant' => $variant,
      ];
      $this->cartManager->removeOrderItem($order_item->getOrder(), $order_item);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Do nothing.
  }

  /**
   * {@inheritdoc}
   */
  public static function getTotalOrder(array &$form, FormStateInterface $form_state) {
    $uid =  self::$current_User->id();
    $total_line_items = 0;
    // get total items in the cart.
    $store_id = 1;
    $order_type = 'default';
    $cart_manager = \Drupal::service('commerce_cart.cart_manager');
    $cart_provider = \Drupal::service('commerce_cart.cart_provider');
    try {
      $store = self::$entityType->getStorage('commerce_store')->load($store_id);
    } catch (\Exception $error) {
      self::$logger->get('checkout_flow')->alert(t('@err', ['@err' => $error->__toString()]));
      self::$messenger->addWarning(t('Unable to proceed. Please contact an administrator. Error code: 02DC5F9C-@line-@time', ['@line' => __LINE__, '@time' => time()]));
    }
    $cart = $cart_provider->getCart($order_type, $store);
    $total_items = count($cart-> getItems());

    // get quantity
    $triggering_element = $form_state->getTriggeringElement();
    $row_index = $triggering_element['#row_index'];
    $updatedOrderQuantity = $form_state->getValue("edit_quantity");
    $quantity = $updatedOrderQuantity[$row_index];
    self::$productDetails['quantity'] = $quantity;

    $ajax_response = new AjaxResponse();
    if($total_items != 0) {
      foreach ($cart->getItems() as $order_item) {
        $total_line_items += $order_item->getQuantity();
      }
      $total = (double) explode(" ", $cart->getTotalPrice())[0];
      $currency_formatter = \Drupal::service('commerce_price.currency_formatter');
      if ($total != '') {
        $total_amount = $currency_formatter->format($total, 'USD'); // $2.99
      }
      $ajax_response->addCommand(new HtmlCommand('.views-element-container .order-total-line__subtotal .order-total-line-value', "$total_amount"));
      $ajax_response->addCommand(new HtmlCommand('.cart_count', "$total_line_items"));

    }
    else {
      $twig = \Drupal::service('twig');
      $template = $twig->loadTemplate(\Drupal::theme()->getActiveTheme()->getPath() . '/templates/cart/commerce-cart-empty-page.html.twig');
      $templateHtml = $template->render([]);
      $ajax_response->addCommand(new HtmlCommand('.ecommerce-cart', "$templateHtml"));
      $ajax_response->addCommand(new HtmlCommand('.cart_count', '0'));

    }
    $ajax_response->addCommand(new AppendCommand('head', "<script>dataLayer.push({'event':'removeFromCart','ecommerce': {'remove': {'products': ".json_encode(self::$productDetails)."}}});</script>"));
    $ajax_response->addCommand(new HtmlCommand('.loader', ""));
    return $ajax_response;
  }

}

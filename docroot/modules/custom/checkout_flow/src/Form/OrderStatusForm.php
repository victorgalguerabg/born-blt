<?php

namespace Drupal\checkout_flow\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\checkout_services\CheckoutServices;

/**
 * Implementation of Checkout flow multi step Form.
 */
class OrderStatusForm extends FormBase {

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * @var \Drupal\checkout_services\CheckoutServices
   */
  protected $services;

  protected $errorsConfig;

  /**
   * Constructs a CheckoutForm object.
   */
  public function __construct(ConfigFactoryInterface $configFactory,CheckoutServices $checkoutServices)
  {
    $this->configFactory = $configFactory->get('checkout_services.checkout_configs');
    $this->errorsConfig = $configFactory->get('checkout_flow.checkout_flow_errors_configs');
    $this->services = $checkoutServices;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container)
  {
    return new static(
      $container->get('config.factory'),
      $container->get('checkout_services.checkoutflow')
    );
  }

  /**
   * return FormId
   */
  public function getFormId()
  {
    return 'order_status_form';
  }

  /**
   * Implementation Order Status form build().
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    // paymetric API call JS
    $form['#attached']['library'][] = 'checkout_flow/order_status';
    $form['#attached']['drupalSettings']['orderStatusMsg'] = $this->t("View Order Details");
    $tableHeadNames = [
      'orderPlaced' => $this->t("Order Placed"),
      "orderNumber" => $this->t("Order Number"),
      "orderStatus" => $this->t("Order Status"),
      "orderTotal" => $this->t("Order Total")
    ];
    $form['#attached']['drupalSettings']['tableHeads'] = $tableHeadNames;
    $form['#attached']['drupalSettings']['errorMsg'] = $this->getOrderStatusErrorsConfig();


    //Order Number Field
    $form['ordernumber'] = [
      '#type' => 'textfield',
      '#title' => $this->t("Order Number*"),
      '#attributes' => [
        'class' => ['form-item__textfield form-item__textfield--required required'],
        'id' => 'edit-ordernumber',
      ],
    ];
    

    //Email Address Field
    $form['email'] = [
      '#type' => 'email',
      '#title' => $this->t("Email Address*"),
      '#attributes' => [
        'class' => ['form-item__textfield form-item__textfield--required required validate-email'],
        'id' => 'edit-email',
      ],
    ];

    $form['zipcode'] = [
      '#type' => 'textfield',
      '#title' => $this->t("Zip Code*"),
      '#min' => 5,
      '#attributes' => [
        'class' => ['form-item__textfield form-item__textfield--required required validate-zipcode'],
        'id' => 'edit-zipcode',
      ],
    ];

    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Track Order'),
      '#button_type' => 'primary',
      '#attributes' => [
        'class' => ['order-status-btn button button--primary button--lg'],
      ],
    );

    $form['#theme'] = 'form_order_status';

    return $form;
  }


  /**
   * Order Status form Validation based on steps.
   *
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * Order Status form submit.
   * @param array $form
   * @param FormStateInterface $form_state
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * Return error config messages.
   */
  public function getOrderStatusErrorsConfig() {
    return array(
      "orderDetail" => $this->errorsConfig->get("orderno"),
      "zipcode" => $this->errorsConfig->get("orderzipcode"),
    );
  }
}
<?php

namespace Drupal\checkout_flow\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\RedirectCommand;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\checkout_services\CheckoutServices;
use Drupal\user_account_mgmt\UserAccountManagement;
use Drupal\paymetric\PaymetricTokenServiceInterface;
use Drupal\user\Entity\User;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\persistent_login\EventSubscriber\TokenHandler;
use Drupal\user\UserStorageInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;

/**
 * Implementation of Checkout flow multi step Form.
 */
class CheckoutForm extends FormBase {
  /**
   * @var \Drupal\user\PrivateTempStoreFactory
   */
  protected $tempStoreFactory;
  /**
   * @var \Drupal\user\PrivateTempStore
   */
  protected $store;

  /**
   * @var \Drupal\user\PrivateTempStore
   */
  protected $userSession;

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;
  /**
   * Symfony\Component\HttpFoundation\RequestStack definition.
   */
  protected $requestStack;
  /**
   * @var \Drupal\checkout_services\CheckoutServices
   */
  protected $services;
  /**
   * Drupal\paymetric\PaymetricTokenServiceInterface definition.
   *
   * @var \Drupal\paymetric\PaymetricTokenServiceInterface
   */
  protected $paymetric_token_service;
  protected $access_token;
  protected $errorsConfig;
  /**
   * @var \Drupal\user_account_mgmt\UserAccountManagement
   */
  protected $userAccountMgmt;
  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $userAccountFactory;
  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $currentUser;
  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * @var \Drupal\persistent_login\EventSubscriber\TokenHandler
   */
  protected $login_services;

  protected $user;

  /**
   * Drupal\Core\Logger\LoggerChannelFactoryInterface definition.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * Constructs a CheckoutForm object.
   */
  public function __construct(PrivateTempStoreFactory $temp_store_factory, ConfigFactoryInterface $configFactory, RequestStack $requestStack, CheckoutServices $checkoutServices, PaymetricTokenServiceInterface $paymetric_token_service, UserAccountManagement $usermanagement, AccountProxyInterface $currentUser, MessengerInterface $messenger, TokenHandler $persistentLogin, UserStorageInterface $user,LoggerChannelFactoryInterface $logger_factory) {
    $this->tempStoreFactory = $temp_store_factory;
    $this->store = $this->tempStoreFactory->get('checkout_form_data');
    $this->userSession = $this->tempStoreFactory->get('loginData');
    $this->configFactory = $configFactory->get('checkout_services.checkout_configs');
    $this->errorsConfig = $configFactory->get('checkout_flow.checkout_flow_errors_configs');
    $this->userAccountFactory = $configFactory->get('user_account_mgmt.user_account_mgmt_config');
    $this->requestStack = $requestStack;
    $this->services = $checkoutServices;
    $this->paymetric_token_service = $paymetric_token_service;
    $this->userAccountMgmt = $usermanagement;
    $this->currentUser = $currentUser;
    $this->messenger = $messenger;
    $this->configPLogin = $configFactory->get('persistent_login.settings');
    $this->login_services = $persistentLogin;
    $this->user = $user;
    $this->loggerFactory = $logger_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('tempstore.private'),
      $container->get('config.factory'),
      $container->get('request_stack'),
      $container->get('checkout_services.checkoutflow'),
      $container->get('paymetric.token'),
      $container->get('user_account_mgmt.user_account_management'),
      $container->get('current_user'),
      $container->get('messenger'),
      $container->get('persistent_login.token_handler'),
      $container->get('entity_type.manager')->getStorage('user'),
      $container->get('logger.factory'),
    );
  }

  /**
   * return FormId
   */
  public function getFormId() {
    return 'checkout_flow_multi_step_form';
  }

  /**
   * Implementation checkout flow form build().
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $queryString = $this->requestStack->getCurrentRequest()->query->all();
    if ($queryString['form_id'] != 'checkout_flow_multi_step_form' && $queryString['ajax_form'] != '1') {
      // Step 1 user validate Form.
      $logged_in = $this->currentUser->isAuthenticated();
      if ($logged_in) {
        $apiResponse = $this->orderValidationApi();
        if (!empty($apiResponse['errors'])) {
          foreach ($apiResponse['errors'] as $errorMsg) {
            $this->messenger->addError($errorMsg['message']);
          }
          $this->messenger->addError("cart is empty, please add products.");
          $response = new RedirectResponse("/cart");
          $response->send();
        }
        $response = new RedirectResponse("/ecommerce/shipping");
        $response->send();
      }
      else {
        $cartResponse = $this->services->cartCheck();
        if (is_null($cartResponse)) {
          $this->messenger->addError($this->t("Cart is empty, Please add Products to continue"));
          $response = new RedirectResponse("/cart");
          $response->send();
        }
        $form = $this->stepOne();
      }
    }
    else {
      $form = $this->stepOne();
    }
    return $form;
  }

  /**
   * Implementation checkout flow step one form build.
   */
  protected function stepOne() {
    $form['username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Username/Email Address'),
      '#required' => TRUE,
      '#default_value' => $this->store->get('username') ? $this->store->get('username') : '',
      '#attributes' => [
        'class' => ['form-item__textfield form-item__textfield--required'],
      ],
    ];
    $form['password'] = [
      '#type' => 'password',
      '#title' => $this->t('Password'),
      '#required' => TRUE,
      '#default_value' => '',
      '#field_suffix' => '<a role="button" class="form-item__pwd-toggle" aria-label="Show Password">' . $this->t('Show Password') . '</a>',
      '#attributes' => [
        'class' => ['form-item__textfield form-item__textfield--password form-item__textfield--required'],
        'autocomplete' => 'off',
      ],
    ];

    $form['persistent_login'] = [
      '#type' => 'checkbox',
      '#title' => $this->configPLogin->get('login_form.field_label'),
      '#cache' => [
        'tags' => ['config:persistent_login.settings'],
      ],
      '#default_value' =>TRUE,
      '#attributes' => [
        'class' => ['custom-input__native'],
      ],
      '#label_attributes' => ["class" =>  ["custom-input__label"]],

    ];
    $form['actions']['login'] = [
      '#type' => 'submit',
      '#value' => $this->t('Login'),
      '#button_type' => 'primary',
      '#attributes' => [
        'class' => ['checkout-login button--lg'],
      ],
      '#ajax' => [
        'callback' => '::validateUser',
        'event' => 'click',
        'wrapper' => 'edit-output',
      ],
    ];
    $form['guestorder'] = [
      '#type' => 'button',
      '#value' => $this->t('Place an order as a guest'),
      '#button_type' => 'primary',
      '#limit_validation_errors' => [],
      '#attributes' => [
        'class' => ['checkout-guest-order button--secondary'],
      ],
      '#ajax' => [
        'callback' => '::guestOrder',
        'event' => 'click',
        'wrapper' => 'edit-output',
      ],
    ];
    $form['#theme'] = 'form_account';
    return $form;
  }

  /**
   * Checkout Flow form Validation based on steps.
   *
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $apiResponse = $this->orderValidationApi();
    if (!empty($apiResponse['errors'])) {
      foreach ($apiResponse['errors'] as $errorMsg) {
        $this->messenger->addError($errorMsg['message']);
      }
      $this->messenger->addError("cart is empty, please add products.");
      $response = new RedirectResponse("/cart");
      $response->send();
    }
  }

  /**
   * Checkout flow form submit.
   * @param array $form
   * @param FormStateInterface $form_state
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   *
   * User Validation through API.
   * @param array $form
   * @param FormStateInterface $form_state
   */
  public function validateUser(array &$form, FormStateInterface $form_state) {

    $form_data =  $form_state->getUserInput();
    $errCnt = 0;
    try {
      $this->store->set("username", $form_data['username']);
    } catch (\Exception $error) {
      $this->loggerFactory->get('checkout_flow_checkout_form')->alert(t('@err', ['@err' => $error->__toString()]));
      $this->messenger->addWarning(t('Unable to proceed, please try again. If the problem persists, please contact an administrator. Error code: 8FAAE95D-@line-@time', ['@line' => $error->getLine(), '@time' => time()]));
    }
    if (empty($form_data['username'])) {
      $errCnt++;
    }
    if (empty($form_data['password'])) {
      $errCnt++;
    }
    if ($errCnt == 0) {
      $userData = ["username" => $form_data['username'],"password" => $form_data['password']];
      $authResponse = $this->userAccountMgmt->userValidationApiCall(trim($this->userAccountFactory->get('userAuthApi')),"POST", $userData, [], 'VALIDATE_USR');
      if (isset($authResponse['success']) && !empty($authResponse['success'])) {
        $account = user_load_by_name($form_data['username']);
        if($account) {
          user_login_finalize($account);
          if ($form_state->getValue('persistent_login', FALSE)) {
            $this->login_services->setNewSessionToken($this->currentUser->id());
          }
          $userDescription = $account->field_group_description->value;
          $userId = $account->field_user_id->value;

          if($userDescription == '' || $userId == '') {
            $userDetailsurl = trim($this->userAccountFactory->get('userValidateApi')) . $form_data['username'];
            $userDetailsRequest = $this->userAccountMgmt->userValidationApiCall($userDetailsurl, 'GET', '', [], 'USR_AUTHEN');
            if (isset($userDetailsRequest['userID'])) {
              $account->set('field_group_description', $userDetailsRequest['groupDescription']);
              $account->set('field_user_id', $userDetailsRequest['userID']);
              $account->save();
            }
          }
          try {
            // User Based session storage
            $this->userSession->set('currentUserGRPDescription', $userDescription);
            $this->userSession->set('currentUserId', $userId);
          } catch (\Exception $error) {
            $this->loggerFactory->get('checkout_flow_checkout_form')->alert(t('@err', ['@err' => $error->__toString()]));
            $this->messenger->addWarning(t('Unable to proceed, please try again. If the problem persists, please contact an administrator. Error code: 8FAAE95D-@line-@time', ['@line' => $error->getLine(), '@time' => time()]));
          }

          // Check Existing Roles.
          $userExistsDetailsurl = trim($this->userAccountFactory->get('userValidateApi')) . $form_data['username'];
          $userExistsDetailsRequest = $this->userAccountMgmt->userValidationApiCall($userExistsDetailsurl, 'GET','', [], 'GET_USR_ROLES');
          if (isset($userExistsDetailsRequest['userID'])) {

            try {
              // Generate roles.
              $existsRoles = $this->userAccountMgmt->generateRoles($userExistsDetailsRequest['roles']);
            } catch (\Exception $error) {
              $this->loggerFactory->get('checkout_flow_checkout_form')->alert(t('@err', ['@err' => $error->__toString()]));
              $this->messenger->addWarning(t('Unable to proceed, please try again. If the problem persists, please contact an administrator. Error code: 8FAAE95D-@line-@time', ['@line' => $error->getLine(), '@time' => time()]));
            }

            // Get the roles to be assigned to the user.
            $existsGetUserRoles = [];
            if ($existsRoles) {
              $existsGetUserRoles = $this->userAccountMgmt->getRoles($userExistsDetailsRequest['roles']);
              $userLoad = $this->user->load($this->currentUser->id());
              foreach ($existsGetUserRoles as $roleName) {
                if($roleName != 'authenticated') {
                  $userLoad->addRole($roleName);
                  try {
                    $userLoad->save();
                  } catch (\Exception $error) {
                    $this->loggerFactory->get('checkout_flow_checkout_form')->alert(t('@err', ['@err' => $error->__toString()]));
                    $this->messenger->addWarning(t('Unable to proceed, please try again. If the problem persists, please contact an administrator. Error code: 8FAAE95D-@line-@time', ['@file' => $error->getFile(), '@line' => $error->getLine(), '@time' => time()]));
                  }
                }
              }
              $mappedRoles = $this->currentUser->getRoles();
              foreach ($mappedRoles as $existingRoleNames) {
                if (!in_array($existingRoleNames,$existsGetUserRoles)) {
                  $userLoad->removeRole($existingRoleNames);
                  try {
                    $userLoad->save();
                  } catch (\Exception $error) {
                    $this->loggerFactory->get('checkout_flow_checkout_form')->alert(t('@err', ['@err' => $error->__toString()]));
                    $this->messenger->addWarning(t('Unable to proceed, please try again. If the problem persists, please contact an administrator. Error code: 8FAAE95D-@line-@time', ['@line' => $error->getLine(), '@time' => time()]));
                  }
                }
              }
            }
          }

          return (new AjaxResponse())->addCommand(
            new RedirectCommand('/ecommerce/shipping')
          );
        }
        else {
          $userDetailsurl = trim($this->userAccountFactory->get('userValidateApi')) . $form_data['username'];
          $userDetailsRequest = $this->userAccountMgmt->userValidationApiCall($userDetailsurl, 'GET','', [],'USR_ACCNT_NOT_EXIST');
          if (isset($userDetailsRequest['userID'])) {
            try {
              // Generate roles.
              $roles = $this->userAccountMgmt->generateRoles($userDetailsRequest['roles']);
            } catch(\Exception $error) {
              $this->loggerFactory->get('checkout_flow_checkout_form')->alert(t('@err', ['@err' => $error->__toString()]));
              $this->messenger->addWarning(t('Unable to proceed, please try again. If the problem persists, please contact an administrator. Error code: 8FAAE95D-@line-@time', ['@line' => $error->getLine(), '@time' => time()]));
            }
            // Get the roles to be assigned to the user.
            $getUserRoles = [];
            if ($roles) {
              $getUserRoles = $this->userAccountMgmt->getRoles($userDetailsRequest['roles']);
            }
            $userCreate = User::create([
              'name' => $form_data['username'],
              'password' => rand(1, 10000),
              'mail' => $userDetailsRequest['email'],
              'roles' => $getUserRoles,
              'status' => 1,
            ]);
            $userCreate->set('field_first_name', $userDetailsRequest['firstName']);
            $userCreate->set('field_last_name', $userDetailsRequest['lastName']);
            $userCreate->set('field_user_id', $userDetailsRequest['userID']);
            $userCreate->set('field_group_description', $userDetailsRequest['groupDescription']);
            try{
              $savedUser = $userCreate->save();
            } catch(\Exception $error) {
              $this->loggerFactory->get('checkout_flow_checkout_form')->alert(t('@err', ['@err' => $error->__toString()]));
              $this->messenger->addWarning(t('Unable to proceed, please try again.'));
            }

            if ($savedUser) {
              $account = user_load_by_name($form_data['username']);
              user_login_finalize($account);
              if ($form_state->getValue('persistent_login', FALSE)) {
                $this->login_services->setNewSessionToken($this->currentUser->id());
              }
              try{
                // User Based session storage
                $this->userSession->set('currentUserGRPDescription', $userDetailsRequest['groupDescription']);
                $this->userSession->set('currentUserId', $userDetailsRequest['userID']);
              } catch(\Exception $error) {
                $this->loggerFactory->get('checkout_flow_checkout_form')->alert(t('@err', ['@err' => $error->__toString()]));
                $this->messenger->addWarning(t('Unable to proceed, please try again. If the problem persists, please contact an administrator. Error code: 8FAAE95D-@line-@time', ['@line' => $error->getLine(), '@time' => time()]));
              }

              return (new AjaxResponse())->addCommand(
                new RedirectCommand('/ecommerce/shipping')
              );
            }
          }
          else {
            $errMsg = $this->t("Invalid username/password");
            $msg = '<div class="alert alert--error form-item--error-message">'.$errMsg.'<i class="icon-icon_filter-hide-small"></i></div>';
            return (new AjaxResponse())->addCommand(
              new HtmlCommand('.login-error-msg', "$msg")
            );
          }
        }
      }
      else {
        $errMsg = $this->t("Invalid username/password");
        $msg = '<div class="alert alert--error form-item--error-message">'.$errMsg.'<i class="icon-icon_filter-hide-small"></i></div>';
        return (new AjaxResponse())->addCommand(
          new HtmlCommand('.login-error-msg', "$msg")
        );
      }
    }
  }

  /**
   * Redirecting to Shipping page.
   */
  public function guestOrder(array &$form, FormStateInterface $form_state) {
    return (new AjaxResponse())->addCommand(
      new RedirectCommand('/ecommerce/shipping')
    );
  }

  /**
   * returns Order validation API Response.
   */
  public function orderValidationApi() {
    $apiObj = $this->services->OrderValidationObjConstruct();
    if (is_array($apiObj)) {
      $apiResponse = $this->services->ecommerceApiCall(trim($this->configFactory->get('orderItemValidationApi')),'POST',[], $apiObj,'CHKOUT_ORDER_VALIDATE_API');
      $apiResponse['tax'] = "0.00";
      $apiResponse['orderItems'][0]['tax'] = "0.00";
      $this->services->UpdateResponseObj($apiResponse);
      if(is_array($apiResponse)) {
        return $apiResponse;
      }
    }
    else {
      $this->messenger->addError($this->t("Cart is empty, Please add Products to continue"));
      $response = new RedirectResponse("/cart");
      $response->send();
    }
  }
}

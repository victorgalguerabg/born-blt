<?php
namespace Drupal\checkout_flow\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use CommerceGuys\Addressing\Subdivision\SubdivisionRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\checkout_services\CheckoutServices;
use Drupal\user_account_mgmt\UserAccountManagement;
use Drupal\paymetric\PaymetricTokenServiceInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\PropertyAccess\PropertyAccess;

/**
 * Implementation of Checkout flow multi step Form.
 */
class ShippingForm extends FormBase {
  /**
   * @var \Drupal\Core\TempStore\PrivateTempStoreFactory
   */
  protected $tempStoreFactory;
  /**
   * @var \Drupal\user\PrivateTempStore
   */
  protected $store;
  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;
  /**
   * Symfony\Component\HttpFoundation\RequestStack definition.
   */
  protected $requestStack;
  /**
   * @var \Drupal\checkout_services\CheckoutServices
   */
  protected $services;
  /**
   * Drupal\paymetric\PaymetricTokenServiceInterface definition.
   *
   * @var \Drupal\paymetric\PaymetricTokenServiceInterface
   */
  protected $paymetric_token_service;
  protected $access_token;
  protected $errorsConfig;
  /**
   * @var \Drupal\user_account_mgmt\UserAccountManagement
   */
  protected $userAccountMgmt;
  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $userAccountFactory;
  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $currentUser;
  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Constructs a CheckoutForm object.
   */
  public function __construct(PrivateTempStoreFactory $temp_store_factory, ConfigFactoryInterface $configFactory, RequestStack $requestStack, CheckoutServices $checkoutServices, PaymetricTokenServiceInterface $paymetric_token_service, UserAccountManagement $usermanagement, AccountProxyInterface $currentUser, MessengerInterface $messenger) {
    $this->tempStoreFactory = $temp_store_factory;
    $this->store = $this->tempStoreFactory->get('checkout_form_data');
    $this->configFactory = $configFactory->get('checkout_services.checkout_configs');
    $this->errorsConfig = $configFactory->get('checkout_flow.checkout_flow_errors_configs');
    $this->userAccountFactory = $configFactory->get('user_account_mgmt.user_account_mgmt_config');
    $this->requestStack = $requestStack;
    $this->services = $checkoutServices;
    $this->paymetric_token_service = $paymetric_token_service;
    $this->userAccountMgmt = $usermanagement;
    $this->currentUser = $currentUser;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('tempstore.private'),
      $container->get('config.factory'),
      $container->get('request_stack'),
      $container->get('checkout_services.checkoutflow'),
      $container->get('paymetric.token'),
      $container->get('user_account_mgmt.user_account_management'),
      $container->get('current_user'),
      $container->get('messenger')
    );
  }

  /**
   * return FormId
   */
  public function getFormId() {
    return 'checkout_flow_shipping_form';
  }

  /**
   * Implementation checkout flow form build().
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $this->services->combineUserCarts();
    $emailSubscription = TRUE;
    $Object = json_decode($this->services->gerOrderObj($emailSubscription), TRUE);
    $currentObj = json_decode($Object['field_order_object'][0]['value'],TRUE);
    $apiResponse = $this->orderValidationApi();
    \Drupal::logger('shipping')->notice(json_encode($apiResponse));
    if (!empty($apiResponse['errors'])) {
      foreach ($apiResponse['errors'] as $errorMsg) {
        $this->messenger->addError($errorMsg['message']);
      }
      $this->messenger->addError("cart is empty, please add products.");
      $response = new RedirectResponse("/cart");
      $response->send();
    }
    if(!empty($currentObj)) {
      $emailSubscription = $Object['field_email_subscription'][0]['value'];
      $form = $this->stepTwo($currentObj,$emailSubscription);
      $listOrderItems = json_decode($this->services->gerOrderObj(), TRUE);
      // For Refurbished Products.
      if(!empty($listOrderItems['orderItems'])){
        foreach ($listOrderItems['orderItems'] as $key => $value) {
          if(!empty($value['skuCode'])) {
            $listPrice = $this->services->getProductListPrice($value['skuCode']);
            \Drupal::logger('shipping')->notice(json_encode($listPrice));
            if(count($listPrice) > 0) {
              if(!empty($listPrice['collection'])) {
                $listOrderItems['orderItems'][$key]['collection'] = $listPrice['collection'];
              }
              else {
                $listOrderItems['orderItems'][$key]['collection'] = "";
              }
              if(!empty($listPrice['listprice'])) {
                $listOrderItems['orderItems'][$key]['strikePrice'] = $listPrice['listprice'];
              }
              else {
                $listOrderItems['orderItems'][$key]['strikePrice'] = "";
              }
              if(!empty($listPrice['recertified'])) {
                $listOrderItems['orderItems'][$key]['recertified'] = $listPrice['recertified'];
              }
              else {
                $listOrderItems['orderItems'][$key]['recertified'] = "";
              }
            }
          }
        }
      }
      $form['cart_orders'] = $listOrderItems;
      $form['noticeHead'] = trim($this->configFactory->get('noticeHead'));
      $form['noticeMsg'] = trim($this->configFactory->get('noticeMsg'));
      $form['#attached']['library'][] = 'checkout_flow/shipping_form';
      return $form;
    }
    else {
      $response = new RedirectResponse("/cart");
      $response->send();
    }
  }

  /**
   * Implementation checkout flow shipping form build().
   */
  protected function stepTwo($currentObj, $subscription) {
    $groundPrice = trim($this->configFactory->get('ground'));
    $priority = trim($this->configFactory->get('priority'));

    if($groundPrice == 0) {
      $groundPrice = "Free";
    }
    else {
      $groundPrice = "$".$groundPrice;
    }

    if($priority == 0) {
      $priority = "Free";
    }
    else {
      $priority = "$".$priority;
    }
    $form['shipping_option'] = [
      '#type' => 'radios',
      '#title' => $this->t('Shipping Option*'),
      '#options' => [
        'cgr' => 'Ground ('.$groundPrice.')',
        'cnd' => 'Priority ('.$priority.')',
      ],
      '#attributes' => ["class" => ["shipping-options"],"data-CGR" => trim($this->configFactory->get('ground')),"data-CND" => trim($this->configFactory->get('priority'))],
      '#default_value' => $currentObj['shippingServiceLevel'] ? strtolower($currentObj['shippingServiceLevel']) : "cgr",
      '#required' => TRUE,
      '#label_attributes' => ["class" => ["custom-input__label"]],
    ];

    $form['selectedOption'] = [
      '#type' => 'hidden',
      '#title' => 'selected option',
      '#default_value' => trim($this->configFactory->get('ground')) ? trim($this->configFactory->get('ground')) : '',
      '#attributes' => [
        'class' => ['selected-shipping-opt'],
      ],
    ];

    $form['shipping_details'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => 'shipping_details',
      ],
    ];
    $form['shipping_details']['shipping_address'] = [
      '#type' => 'item',
      '#markup' => $this->t('Shipping Address.'),
    ];
    $shippingInfo = '';
    if(!empty($this->configFactory->get('shippingInfo'))){
      $shippingInfo = $this->configFactory->get('shippingInfo');
    }

    $form['shipping_details']['shipping_info'] = [
      '#type' => 'item',
      '#markup' => $shippingInfo,
    ];

    $form['shipping_details']['fullname'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Full Name*'),
      '#default_value' => $currentObj['shippingAddress']['fullName'] ? $currentObj['shippingAddress']['fullName'] : '',
      '#attributes' => [
        'class' => ['form-item__textfield form-item__textfield--required required validate-name'],
      ],
    ];
    $form['shipping_details']['address_line'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Address Line 1*'),
      '#default_value' => $currentObj['shippingAddress']['address1'] ? $currentObj['shippingAddress']['address1'] : '',
      '#attributes' => [
        'class' => ['form-item__textfield form-item__textfield--required required validate-address'],
      ],
    ];
    $form['shipping_details']['address_line_optional'] = [
      '#type' => 'textfield',
      '#title' => 'Address Line 2 <span class="optional"> (optional)</span>',
      '#default_value' => $currentObj['shippingAddress']['address2'] ? $currentObj['shippingAddress']['address2'] : '',
      '#attributes' => [
        'class' => ['form-item__textfield form-item__textfield--required'],
      ],
    ];
    $form['shipping_details']['city'] = [
      '#type' => 'textfield',
      '#title' => $this->t('City*'),
      '#default_value' => $currentObj['shippingAddress']['city'] ? $currentObj['shippingAddress']['city'] : '',
      '#attributes' => [
        'class' => ['form-item__textfield form-item__textfield--required required validate-city'],
      ],
    ];
    $form['shipping_details']['state'] = [
      '#type' => 'select',
      '#title' => $this->t('State*'),
      '#options' => $this->getUsStates(),
      '#default_value' => $currentObj['shippingAddress']['state'] ? $currentObj['shippingAddress']['state'] : '',
      '#attributes' => [
        'class' => ['required'],
      ],
    ];
    $form['shipping_details']['zipcode'] = [
      '#type' => 'number',
      '#title' => $this->t('Zip*'),
      '#min' => 5,
      '#default_value' => $currentObj['shippingAddress']['zip5'] ? $currentObj['shippingAddress']['zip5'] : '',
      '#attributes' => [
        'class' => ['form-item__textfield form-item__textfield--required required validate-zipcode'],
      ],
    ];
    $form['contact_details'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => 'contact_details custom-input custom-input--checkbox',
      ],
    ];
    $form['contact_details']['phone_number'] = [
      '#type' => 'tel',
      '#title' => $this->t('Phone Number*'),
      '#default_value' => $currentObj['phone'] ? $currentObj['phone'] : '',
      '#attributes' => [
        'class' => ['form-item__textfield form-item__textfield--required required validate-phone'],
      ],
    ];
    $form['contact_details']['email'] = [
      '#type' => 'email',
      '#title' => $this->t('Email Address*'),
      '#default_value' => $currentObj['email'] ? $currentObj['email'] : '',
      '#attributes' => [
        'class' => ['form-item__textfield form-item__textfield--required required validate-email'],
      ],
    ];
    $form['contact_details']['notification'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('I would like to receive offers, news and information via email'),
      '#attributes' => [
        'class' => ['notification-email custom-input__native'],
      ],
      '#label_attributes' => [
        'class' => ['custom-input__label'],
      ],
      '#default_value' =>($subscription) ? TRUE : FALSE, // for default checked and false is not checked
    ];
    $form['actions']['continue'] = [
      '#type' => 'submit',
      '#value' => $this->t('continue'),
      '#button_type' => 'primary',
      '#attributes' => [
        'class' => ['ship-continue-btn'],
      ],
    ];

    $form['#theme'] = 'form_shipment';
    return $form;
  }

  /**
   * Checkout Flow form Validation based on steps.
   *
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $errCnt = 0;
    if (!empty($form_state->getValue('fullname'))) {
      if (!preg_match("/^[a-zA-Z ]*$/", $form_state->getValue('fullname'))) {
        $form_state->setErrorByName('fullname', $this->t('Full Name is allowed Only letters and white space.'));
        $errCnt++;
      }
    }
    else {
      $form_state->setErrorByName('fullname', $this->errorsConfig->get("fullname"));
      $errCnt++;
    }
    if (!empty($form_state->getValue('address_line'))) {
      if (!preg_match('/^[0-9a-zA-Z,\/ ]+$/', $form_state->getValue('address_line'))) {
        $form_state->setErrorByName('address_line', $this->t('Address Line 1 allowed only letters, Numbers.'));
        $errCnt++;
      }
    }
    else {
      $form_state->setErrorByName('address_line', $this->errorsConfig->get("addressline1"));
      $errCnt++;
    }
    if (!empty($form_state->getValue('city'))) {
      if (!preg_match("/^[a-zA-Z ]*$/", $form_state->getValue('city'))) {
        $form_state->setErrorByName('city', $this->t('City is allowed Only letters and white space.'));
        $errCnt++;
      }
    }
    else {
      $form_state->setErrorByName('city', $this->errorsConfig->get("city"));
      $errCnt++;
    }
    if (!empty($form_state->getValue('zipcode'))) {
      if (strlen($form_state->getValue('zipcode')) < 5) {
        $form_state->setErrorByName('zipcode', $this->t('Please enter a valid Zipcode.'));
        $errCnt++;
      }
    }
    else {
      $form_state->setErrorByName('zipcode', $this->errorsConfig->get("zipcode"));
      $errCnt++;
    }
    if (!empty($form_state->getValue('email'))) {
      if (!filter_var($form_state->getValue('email'), FILTER_VALIDATE_EMAIL)) {
        $form_state->setErrorByName('email', $this->t('Please enter valid email.'));
        $errCnt++;
      }
    }
    else {
      $form_state->setErrorByName('email', $this->errorsConfig->get("email"));
      $errCnt++;
    }
    if (empty($form_state->getValue('phone_number'))) {
      $form_state->setErrorByName('phone_number', $this->errorsConfig->get("phone"));
      $errCnt++;
    }

    if ($errCnt == 0) {

      // Email Notification Object
      $emailNotification = $this->services->emailSubscription($form_state->getValue('notification'));

      $shippingAddress = [
        "addressNumber" => NULL,
        "fullName" => $form_state->getValue('fullname'),
        "address1" => $form_state->getValue('address_line'),
        "address2" => $form_state->getValue('address_line_optional'),
        "city" => $form_state->getValue('city'),
        "state" => $form_state->getValue('state'),
        "zip5" => $form_state->getValue('zipcode'),
        "zip4" => NULL,
        "country" => NULL,
        "valid" => FALSE,
        "messages" => NULL,
      ];
      $randomNumber = $this->services->randomNumber([9]);
      $phone = $form_state->getValue('phone_number');
      if (strpos($phone, '-') !== false) {
        $customerOrderNumber = substr($phone, 0, 3) . ' ' . substr($phone, 4, 3) . ' ' . substr($phone, 8, 4). ' ' . $randomNumber;
      }
      elseif (strpos($phone, '.') !== false) {
        $customerOrderNumber = substr($phone, 0, 3) . ' ' . substr($phone, 4, 3) . ' ' . substr($phone, 8, 4). ' ' . $randomNumber;
      }
      else {
        $customerOrderNumber = substr($phone, 0, 3) . ' ' . substr($phone, 3, 3) . ' ' . substr($phone, 6, 4). ' ' . $randomNumber;
      }
      $orderObject = json_decode($this->services->gerOrderObj(), TRUE);
      $propertyAccessor = PropertyAccess::createPropertyAccessor();
      $propertyAccessor->setValue($orderObject, '[customerOrderNumber]', $customerOrderNumber);
      $propertyAccessor->setValue($orderObject, '[shippingServiceLevel]', strtoupper($form_state->getValue('shipping_option')));
      $propertyAccessor->setValue($orderObject, '[shippingCharge]', $form_state->getValue('selectedOption'));
      $propertyAccessor->setValue($orderObject, '[phone]', $form_state->getValue('phone_number'));
      $propertyAccessor->setValue($orderObject, '[email]', $form_state->getValue('email'));
      $propertyAccessor->setValue($orderObject, '[shippingAddress]', $shippingAddress);
      //order validation API Call
      $apiResponse = $this->allOrdersValidationApi($orderObject);
      if (!empty($apiResponse['errors'])) {
        foreach ($apiResponse['errors'] as $errorMsg) {
          $this->messenger->addError($errorMsg['message']);
        }
        $response = new RedirectResponse("/ecommerce/shipping");
        $response->send();
      }
      $response = new RedirectResponse("/ecommerce/billing");
      $response->send();
    }
  }

  /*
   * Validates if $phone is a valid U.S. phone number in the 202-555-1234 format.
   * Note that the first digit cannot be a 1 as no area code in the U.S. starts with a 1.
   */
  public function validatePhoneNo($phone) {
    if (preg_match("/^([1]-)?[0-9]{3}-[0-9]{3}-[0-9]{4}$/i", $phone)) {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

  /**
   * Checkout flow form submit.
   *
   * @param array $form
   * @param FormStateInterface $form_state
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * returns Us States list with Key value pair array
   */
  public function getUsStates() {
    $municipalities = [];
    $municipalities[''] = 'State';
    // Getting US sates for Addressing module.
    $subdivisionRepository = new SubdivisionRepository();
    // Get the subdivisions for US.
    $states = $subdivisionRepository->getAll(['US']);
    foreach ($states as $state) {
      $municipalities[$state->getCode()] = $state->getName();
    }
    return $municipalities;
  }

  /**
   * returns All Orders validation API Response.
   */
  public function allOrdersValidationApi($orderObj) {
    $apiResponse = $this->services->ecommerceApiCall(trim($this->configFactory->get('allOrderItemValidationApi')),'POST',[], $orderObj,'SHIP_ALL_ORDERS_VALIDATE_API');
    $this->services->UpdateResponseObj($apiResponse);
    return $apiResponse;
  }

  /**
   * returns Order validation API Response.
   */
  public function orderValidationApi() {
    $apiObj = $this->services->OrderValidationObjConstruct();
    if (is_array($apiObj)) {
      $apiResponse = $this->services->ecommerceApiCall(trim($this->configFactory->get('orderItemValidationApi')),'POST', [], $apiObj,'SHIP_ORDER_VALIDATE_API');
      if(!empty($apiResponse)){
        $apiResponse['tax'] = "0.00";
        $apiResponse['orderItems'][0]['tax'] = "0.00";
        $this->services->UpdateResponseObj($apiResponse);
        if(is_array($apiResponse)) {
          return $apiResponse;
        }
      }
    }
    else {
      $this->messenger->addError($this->t("Cart is empty, Please add Products to continue"));
      $response = new RedirectResponse("/cart");
      $response->send();
    }
  }
}

<?php

namespace Drupal\checkout_flow\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Implementation of Errors configs for checkout flow.
 */
class ErrorsConfigForm extends ConfigFormBase {

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs a CheckoutForm object.
   */
  public function __construct(ConfigFactoryInterface $configFactory) {
    //$this->configFactory = $configFactory->get('errors_config.checkout_flow_errors_configs');
  }

  /**
   * Editable Config Names.
   */
  protected function getEditableConfigNames() {
    return [
      'checkout_flow.checkout_flow_errors_configs',
    ];
  }

  /**
   * Services creation.
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory')
    );
  }

  /**
   * return FormId
   */
  public function getFormId() {
    return 'checkout_flow_errors_config_form';
  }

  /**
   * Implementation checkout flow Errors config form build().
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('checkout_flow.checkout_flow_errors_configs');

    $form['shippingForm'] = [
      '#type' => 'details',
      '#title' => $this->t('Shipping Form Errors Config'),
    ];

    $form['shippingForm']['fullname'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Full Name'),
      '#description' => $this->t('Full Name field error message.'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('fullname'),
    ];

    $form['shippingForm']['addressline1'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Address Line 1'),
      '#description' => $this->t('Address Line 1 field error message.'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('addressline1'),
    ];

    $form['shippingForm']['addressline2'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Address Line 2'),
      '#description' => $this->t('Address Line 2 field error message.'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('addressline2'),
    ];

    $form['shippingForm']['city'] = [
      '#type' => 'textfield',
      '#title' => $this->t('City'),
      '#description' => $this->t('City field error message.'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('city'),
    ];

    $form['shippingForm']['state'] = [
      '#type' => 'textfield',
      '#title' => $this->t('State'),
      '#description' => $this->t('State field error message.'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('state'),
    ];

    $form['shippingForm']['zipcode'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Zip Code'),
      '#description' => $this->t('Zip Code field error message.'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('zipcode'),
    ];

    $form['shippingForm']['phone'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Phone'),
      '#description' => $this->t('Phone field error message.'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('phone'),
    ];

    $form['shippingForm']['email'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Email'),
      '#description' => $this->t('Email field error message.'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('email'),
    ];

    // Billing Form Errors Config.

    $form['billingForm'] = [
      '#type' => 'details',
      '#title' => $this->t('Billing Form Errors Config'),
    ];

    $form['billingForm']['termsandconditions'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Terms and Condition'),
      '#description' => $this->t('Terms and Condition field error message.'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('termsandconditions'),
    ];

    $form['billingForm']['billfullname'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Full Name'),
      '#description' => $this->t('Full Name field error message.'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('billfullname'),
    ];

    $form['billingForm']['billaddressline1'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Address Line 1'),
      '#description' => $this->t('Address Line 1 field error message.'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('billaddressline1'),
    ];

    $form['billingForm']['billaddressline2'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Address Line 2'),
      '#description' => $this->t('Address Line 2 field error message.'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('billaddressline2'),
    ];

    $form['billingForm']['billcity'] = [
      '#type' => 'textfield',
      '#title' => $this->t('City'),
      '#description' => $this->t('City field error message.'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('billcity'),
    ];

    $form['billingForm']['billstate'] = [
      '#type' => 'textfield',
      '#title' => $this->t('State'),
      '#description' => $this->t('State field error message.'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('billstate'),
    ];

    $form['billingForm']['billzipcode'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Zip Code'),
      '#description' => $this->t('Zip Code field error message.'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('billzipcode'),
    ];

    // Order Status Errors Config

    $form['orderstatus'] = [
      '#type' => 'details',
      '#title' => $this->t('Order Status'),
    ];

    $form['orderstatus']['orderno'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Order Number, Email, Phone Number'),
      '#description' => $this->t('Order Number, Email, Phone Number fields error message.'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('orderno'),
    ];

    $form['orderstatus']['orderzipcode'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Order status Form Zip Code'),
      '#description' => $this->t('Zip Code error message.'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('orderzipcode'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * Checkout flow Errors Config Form Submit.
   * @param $form
   * @param $form_state
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $this->config('checkout_flow.checkout_flow_errors_configs')
      ->set('fullname', $form_state->getValue('fullname'))
      ->set('addressline1', $form_state->getValue('addressline1'))
      ->set('addressline2', $form_state->getValue('addressline2'))
      ->set('city', $form_state->getValue('city'))
      ->set('state', $form_state->getValue('state'))
      ->set('zipcode', $form_state->getValue('zipcode'))
      ->set('phone', $form_state->getValue('phone'))
      ->set('email', $form_state->getValue('email'))
      ->set('termsandconditions', $form_state->getValue('termsandconditions'))
      ->set('billfullname', $form_state->getValue('billfullname'))
      ->set('billaddressline1', $form_state->getValue('billaddressline1'))
      ->set('billaddressline2', $form_state->getValue('billaddressline2'))
      ->set('billcity', $form_state->getValue('billcity'))
      ->set('billstate', $form_state->getValue('billstate'))
      ->set('billzipcode', $form_state->getValue('billzipcode'))
      ->set('orderno', $form_state->getValue('orderno'))
      ->set('orderzipcode', $form_state->getValue('orderzipcode'))
      ->save();
  }
}
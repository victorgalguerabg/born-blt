<?php
namespace Drupal\checkout_flow\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use CommerceGuys\Addressing\Subdivision\SubdivisionRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\RedirectCommand;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\checkout_services\CheckoutServices;
use Drupal\user_account_mgmt\UserAccountManagement;
use Drupal\paymetric\PaymetricTokenServiceInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Drupal\Core\Ajax\HtmlCommand;

/**
 * Implementation of Checkout flow multi step Form.
 */
class BillingForm extends FormBase {
  /**
   * @var \Drupal\user\PrivateTempStoreFactory
   */
  protected $tempStoreFactory;
  /**
   * @var \Drupal\user\PrivateTempStore
   */
  protected $store;
  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;
  /**
   * Symfony\Component\HttpFoundation\RequestStack definition.
   */
  protected $requestStack;
  /**
   * @var \Drupal\checkout_services\CheckoutServices
   */
  protected $services;
  /**
   * Drupal\paymetric\PaymetricTokenServiceInterface definition.
   *
   * @var \Drupal\paymetric\PaymetricTokenServiceInterface
   */
  protected $paymetric_token_service;
  protected $access_token;
  protected $errorsConfig;
  /**
   * @var \Drupal\user_account_mgmt\UserAccountManagement
   */
  protected $userAccountMgmt;
  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $userAccountFactory;
  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $currentUser;
  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;
  /**
   * Constructs a CheckoutForm object.
   */
  public function __construct(PrivateTempStoreFactory $temp_store_factory, ConfigFactoryInterface $configFactory, RequestStack $requestStack, CheckoutServices $checkoutServices, PaymetricTokenServiceInterface $paymetric_token_service, UserAccountManagement $usermanagement, AccountProxyInterface $currentUser, MessengerInterface $messenger) {
    $this->tempStoreFactory = $temp_store_factory;
    $this->store = $this->tempStoreFactory->get('checkout_form_data');
    $this->configFactory = $configFactory->get('checkout_services.checkout_configs');
    $this->errorsConfig = $configFactory->get('checkout_flow.checkout_flow_errors_configs');
    $this->userAccountFactory = $configFactory->get('user_account_mgmt.user_account_mgmt_config');
    $this->requestStack = $requestStack;
    $this->services = $checkoutServices;
    $this->paymetric_token_service = $paymetric_token_service;
    $this->userAccountMgmt = $usermanagement;
    $this->currentUser = $currentUser;
    $this->messenger = $messenger;
  }
  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('tempstore.private'),
      $container->get('config.factory'),
      $container->get('request_stack'),
      $container->get('checkout_services.checkoutflow'),
      $container->get('paymetric.token'),
      $container->get('user_account_mgmt.user_account_management'),
      $container->get('current_user'),
      $container->get('messenger')
    );
  }
  /**
   * return FormId
   */
  public function getFormId() {
    return 'checkout_flow_billing_form';
  }
  /**
   * Implementation checkout flow form build().
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = [];
    // Step 3 Payment Form.
    $currentObj = json_decode($this->services->gerOrderObj(), TRUE);
    if(!empty($currentObj)) {
      $values = $form_state->getValues();
      $form = $this->stepThree($values);

      $listOrderItems = $currentObj;

      // Ground Display name
      if($listOrderItems['shippingServiceLevel'] == 'CGR') {
        $listOrderItems['shippingServiceLevel'] = $this->configFactory->get('cgr');
      }

      //Priority Display name
      if($listOrderItems['shippingServiceLevel'] == 'CND') {
        $listOrderItems['shippingServiceLevel'] = $this->configFactory->get('cnd');
      }

      // For Refurbished Products
      if(!empty($listOrderItems['orderItems'])){
        foreach ($listOrderItems['orderItems'] as $key => $value) {
          $listPrice = $this->services->getProductListPrice($value['skuCode']);
          $listOrderItems['orderItems'][$key]['collection'] = $listPrice['collection'];
          if(!empty($listPrice['listprice'])) {
            $listOrderItems['orderItems'][$key]['strikePrice'] = $listPrice['listprice'];
          }
          else {
            $listOrderItems['orderItems'][$key]['strikePrice'] = "";
          }

          if(!empty($listPrice['recertified'])) {
            $listOrderItems['orderItems'][$key]['recertified'] = $listPrice['recertified'];
          }
          else {
            $listOrderItems['orderItems'][$key]['recertified'] = "";
          }
        }
      }
      $form['noticeHead'] = trim($this->configFactory->get('noticeHead'));
      $form['noticeMsg'] = trim($this->configFactory->get('noticeMsg'));
      $form['ship_response'] = $listOrderItems;
      return $form;
    }
    else {
      $response = new RedirectResponse("/cart");
      $response->send();
    }
    return $form;
  }
  /**
   * Implementation checkout flow payment form build().
   * @param $values
   *
   * @return $form
   */
  protected function stepThree($values) {
    // paymetric API call JS
    $form['#attached']['library'][] = 'checkout_flow/checkout_flow';
    $this->access_token = !empty($values['access_token']) ? $values['access_token'] : $this->access_token = $this->paymetric_token_service->getAccessToken();
    $form['hidden']['access_token'] = [
      '#type' => 'hidden',
      '#title' => $this->t('Access Token'),
      '#value' => $this->access_token,
      '#attributes' => [
        'disabled' => 'disabled',
        'id' => 'edit-access-token',
      ]
    ];
    $iframe_url = $this->paymetric_token_service->getEndpoint() . '/diecomm/View/Iframe/' . $this->paymetric_token_service->getGuid() . '/' . $this->access_token . '/True';
    $iframe_html = '<iframe name="dieCommFrame" src="' . $iframe_url . '" style="width: 100%; overflow: hidden; height: 400px;" scrolling="no" frameborder="0" id="IFrame" class="payment_card"></iframe>';
    $form['render']['iframe'] = [
      '#type' => 'markup',
      '#markup' => $iframe_html,
      '#allowed_tags' => ['iframe'],
      '#suffix' => '<br />',
      '#attributes' => [
        'class' => ['payment_card'],
      ],
    ];
    $form['paymentBillingInfo']['additionalInfoDes'] = [
      '#type' => 'item',
      '#markup' => $this->t('Billing Information'),
      '#attributes' => [
        'class' => 'additional_info_des',
      ],
    ];
    $form['paymentBillingInfo']['billShipAddressSame'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('My Billing address is the same as my Shipping Address'),
      '#attributes' => [
        'class' => ['custom-input__native'],
      ],
      '#label_attributes' => [
        'class' => ['custom-input__label'],
      ],
      '#prefix' => "<div class='custom-input--checkbox'>",
      '#suffix' => "</div>",
    ];
    //Billing Form fields
    $form['paymentInfo'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => 'paymentInfo',
      ],
    ];
    $form['paymentInfo']['paymentFullName'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Full Name*'),
      '#default_value' => $this->store->get('paymentFullName') ? $this->store->get('paymentFullName') : '',
      '#attributes' => [
        'class' => ['form-item__textfield form-item__textfield--required required validate-name'],
      ],
    ];
    $form['paymentInfo']['paymentAddressLine'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Address Line 1*'),
      '#default_value' => $this->store->get('paymentAddressLine') ? $this->store->get('paymentAddressLine') : '',
      '#attributes' => [
        'class' => ['form-item__textfield form-item__textfield--required required validate-address'],
      ],
    ];
    $form['paymentInfo']['paymentAddressLineOptional'] = [
      '#type' => 'textfield',
      '#title' => t('Address Line 2 (Optional)'),
      '#default_value' => $this->store->get('paymentAddressLineOptional') ? $this->store->get('paymentAddressLineOptional') : '',
      '#attributes' => [
        'class' => ['form-item__textfield form-item__textfield--required'],
      ],
    ];
    $form['paymentInfo']['paymentCity'] = [
      '#type' => 'textfield',
      '#title' => $this->t('City*'),
      '#default_value' => $this->store->get('paymentCity') ? $this->store->get('paymentCity') : '',
      '#attributes' => [
        'class' => ['form-item__textfield form-item__textfield--required required validate-city'],
      ],
    ];
    $form['paymentInfo']['paymentState'] = [
      '#type' => 'select',
      '#title' => $this->t('State*'),
      '#options' => $this->getUsStates(),
    ];
    $form['paymentInfo']['paymentZipcode'] = [
      '#type' => 'number',
      '#title' => $this->t('Zip'),
      '#min' => 5,
      '#default_value' => $this->store->get('paymentZipcode') ? $this->store->get('paymentZipcode') : '',
      '#attributes' => [
        'class' => ['form-item__textfield form-item__textfield--required required validate-zipcode'],
      ],
    ];
    $form['accesstokenXml'] = [
      '#type' => 'hidden',
      '#title' => $this->t('Access Token Xml'),
      '#value' => "cardresponse",
      '#attributes' => [
        'id' => 'xml-access-token',
      ]
    ];
    $form['accept_order_details'] = [
      '#type' => 'checkbox',
      '#title' => "By choosing to place your order, you are agreeing to the items, quantities, pricing and additional charges,
      where applicable, as set forth in the order above.  This order is subject to our return policy and our standard sales terms and conditions as in effect from time to time,
      which are incorporated herein by reference and available at <a class='red1 link' href='/ecommerce/return-policy'>Return Policy</a>
      <a class='red1 link' href='/ecommerce/standard-sales-terms-and-conditions'>Terms and Conditions</a>",
      '#attributes' => [
        'class' => ['custom-input__native required'],
      ],
      '#label_attributes' => [
        'class' => ['custom-input__label'],
      ],
      '#prefix' => "<div class='custom-input--checkbox'>",
      '#suffix' => "</div>",
    ];
    $form['actions']['#type'] = 'actions';

    $form['submit'] = [
      '#type' => 'html_tag',
      '#tag' => 'input',
      '#attributes' => [
        'type' => 'button',
        'value' => $this->t("PLACE ORDER"),
        'class' => 'button button--primary button--lg',
        'name' => 'payment-submit-button',
        'id' => 'payment-submit-button'
      ],
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('PLACE ORDER'),
      '#button_type' => 'primary',
      '#attributes' => [
        'class' => ['ordersubmitbtn'],
      ],
      '#ajax' => [
        'callback' => '::ordersubmit',
        'event' => 'click',
        'wrapper' => 'edit-output',
      ],
    ];
    $form['#theme'] = 'form_billing';
    return $form;
  }
  /**
   * Checkout Flow form Validation based on steps.
   *
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }
  /* Validates if $phone is a valid U.S. phone number in the 202-555-1234 format.
   * Note that the first digit cannot be a 1 as no area code in the U.S. starts with a 1.
   */
  public function validatePhoneNo($phone) {
    if (preg_match("/^([1]-)?[0-9]{3}-[0-9]{3}-[0-9]{4}$/i", $phone)) {
      return true;
    }
    else {
      return false;
    }
  }
  /**
   * Checkout flow form submit.
   * @param array $form
   * @param FormStateInterface $form_state
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }
  /**
   * Billing Form submit and Temp storage
   */
  public function ordersubmit(array &$form, FormStateInterface $form_state) {

    $tempaddress = $form_state->getValue('billShipAddressSame');
    $orderObject = json_decode($this->services->gerOrderObj(), TRUE);
    $propertyAccessor = PropertyAccess::createPropertyAccessor();
    if($form_state->getValue('billShipAddressSame') == 0) {
      $billingAddress = [
        "addressNumber" => NULL,
        "fullName" => $form_state->getValue('paymentFullName'),
        "address1" => $form_state->getValue('paymentAddressLine'),
        "address2" => $form_state->getValue('paymentAddressLineOptional'),
        "city" => $form_state->getValue('paymentCity'),
        "state" => $form_state->getValue('paymentState'),
        "zip5" => $form_state->getValue('paymentZipcode'),
        "zip4" => NULL,
        "country" => NULL,
        "valid" => false,
        "messages" => NULL,
      ];
    } else {
      $billingAddress = $propertyAccessor->getValue($orderObject, '[shippingAddress]');
    }
    //Card details from paymetric API
    $form_data =  $form_state->getUserInput();
    $cardDetails = explode("|", $form_data['accesstokenXml']);
    if(count($cardDetails) > 1) {
      if($cardDetails[0] == "vi") {
        $ccType = "VISA";
      }
      if($cardDetails[0] == "mc") {
        $ccType = "MC";
      }
      $propertyAccessor->setValue($orderObject, '[ccNumber]', $cardDetails[3]);
      $propertyAccessor->setValue($orderObject, '[ccConfirmationCode]', $cardDetails[5]);
      $propertyAccessor->setValue($orderObject, '[ccType]', $ccType);
      $propertyAccessor->setValue($orderObject, '[ccName]', $cardDetails[4]);
      $propertyAccessor->setValue($orderObject, '[ccExpireMonth]', $cardDetails[1]);
      $propertyAccessor->setValue($orderObject, '[ccExpireYear]', $cardDetails[2]);
    }

    $propertyAccessor->setValue($orderObject, '[billingAddress]', $billingAddress);
    //payment API Call
    $apiResponse = $this->services->ecommerceApiCall(trim($this->configFactory->get('paymentAuthorizeApi')),'POST',[], $orderObject,'PAYMENT_API');
    $this->services->UpdateResponseObj($apiResponse);
    if(!empty($apiResponse['errors'])) {
      $payErrMsg = "";
      foreach ($apiResponse['errors'] as $payErrorMsg) {
        $payErrMsg .= $payErrorMsg['message']."</br>";
      }
      $payMsg = '<div class="alert alert--error form-item--error-message">'.$payErrMsg.'<i class="icon-icon_filter-hide-small"></i></div>';
      return (new AjaxResponse())->addCommand(
        new HtmlCommand('.error-on-paymetric', "$payMsg")
      );
    }
    else {
      // Order conformation API Call
      $orderConfObject = json_decode($this->services->gerOrderObj(), TRUE);
      $apiConResponse = $this->services->ecommerceApiCall(trim($this->configFactory->get('orderConformationApi')),'POST',[],$orderConfObject,'ORDER_CNF_API');
      if(!empty($apiConResponse['errors'])) {
        $errMsg = "";
        foreach ($apiConResponse['errors'] as $errorMsg) {
          $errMsg .= $errorMsg['message']."</br>";
        }
        $msg = '<div class="alert alert--error form-item--error-message">'.$errMsg.'<i class="icon-icon_filter-hide-small"></i></div>';
        return (new AjaxResponse())->addCommand(
          new HtmlCommand('.error-on-paymetric', "$msg")
        );
      }
      else {
        $this->services->UpdateResponseObj($apiConResponse);
        return (new AjaxResponse())->addCommand(
          new RedirectCommand('/order-confirmation')
        );
      }
    }
  }

  /**
   * returns Us States list with Key value pair array
   */
  public function getUsStates() {
    $municipalities = [];
    $municipalities[''] = 'State';
    // Getting US sates for Addressing module.
    $subdivisionRepository = new SubdivisionRepository();
    // Get the subdivisions for US.
    $states = $subdivisionRepository->getAll(['US']);
    foreach ($states as $state) {
      $municipalities[$state->getCode()] = $state->getName();
    }
    return $municipalities;
  }
}

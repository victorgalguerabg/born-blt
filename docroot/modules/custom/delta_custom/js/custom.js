(function($, Drupal, drupalSettings) {
  Drupal.behaviors.delta_custom = {
    attach: function(context, settings) {
    var formSelectors = "form.media-library-add-form, form.media-image-edit-form, form.media-image-add-form, form.editor-image-dialog, form#entity-browser-media-browser-form, form#entity-browser-image-browser-form, form#entity-browser-ckeditor-media-browser-form";   
      if($(formSelectors).length){
        var file_name = '';
        var medUrlAlias = '';
        var imgUrlAlias = '';
        var curUID = '';
        if($("span.file--mime-image-jpeg a").length){
          var file_name = $("span.file--mime-image-jpeg a").text();
        }
        if($("span.file--mime-image-jpg a").length){
          var file_name = $("span.file--mime-image-jpg a").text();
        }
        if($("span.file--mime-image-png a").length){
          var file_name = $("span.file--mime-image-png a").text();
        }
        if(file_name != ''){
          //This is will remove the extension from the file name
          var removeExt = file_name.split('.').slice(0, -1).join('.');
          var file_name = removeExt.replace(/_|-/gi, " ");          
          if($("form.editor-image-dialog").length > 0 ){
            //Special for - CKEditor - image icon
            $("form.editor-image-dialog input[type=text]").val(file_name);
          }else if($("form.media-library-add-form").length > 0 ){
            //Special for - Filed Type: Media, Form display widget : Media Library
            if($("form.media-library-add-form input[name*='media[0][fields][name][0][value]']").length){
              $("form.media-library-add-form input[name*='media[0][fields][name][0][value]']").val(file_name);
            }            
            if($("form.media-library-add-form input[name*='media[0][fields][image][0][title]").length){
              $("form.media-library-add-form input[name*='media[0][fields][image][0][title]").val(file_name);
            }
            if($("form.media-library-add-form input[name*='media[0][fields][image][0][alt]']").length){
              $("form.media-library-add-form input[name*='media[0][fields][image][0][alt]']").val(file_name);
            }   
            //Brizo special - field_media_image
            if($("form.media-library-add-form input[name*='media[0][fields][field_media_image][0][title]").length){
              $("form.media-library-add-form input[name*='media[0][fields][field_media_image][0][title]").val(file_name);
            }
            if($("form.media-library-add-form input[name*='media[0][fields][field_media_image][0][alt]").length){
              $("form.media-library-add-form input[name*='media[0][fields][field_media_image][0][alt]").val(file_name);
            }        
          
          }else{
            //Image , media fields, ckedit media plugins
            $("input[name*='entity[name][0][value]']").val(file_name);            
            if($("input[name*='path[0][alias]']").length){
              var medUrlAlias = $("input[name*='path[0][alias]']").val();
            }
            if($("input[name*='entity[path][0][alias]']").length){
              var imgUrlAlias = $("input[name*='entity[path][0][alias]']").val();
            }
            if($("input[name*='entity[uid][0][target_id]']").length){
              var curUID = $("input[name*='entity[uid][0][target_id]']").val();
            }   
            $("input[type=text]").val(file_name);
            if($("input[name*='entity[path][0][alias]']").length){
              $("input[name*='entity[path][0][alias]']").val(imgUrlAlias);
            }
            if($("input[name*='path[0][alias]']").length){
              $("input[name*='path[0][alias]']").val(medUrlAlias);
            }
            if($("input[name*='entity[uid][0][target_id]']").length){
              $("input[name*='entity[uid][0][target_id]']").val(curUID);
            }
          }
          
                   
          // Auto submit - Future purpose
          /*
          if($("form#entity-browser-image-browser-form input.is-entity-browser-submit").length){
            $("form#entity-browser-image-browser-form input.is-entity-browser-submit").trigger("click");
          }
          if($("form#entity-browser-media-browser-form input.is-entity-browser-submit").length){
            $("form#entity-browser-media-browser-form input.is-entity-browser-submit").trigger("click");
          }          
          if($("form#entity-browser-ckeditor-media-browser-form input.is-entity-browser-submit").length){
            $("form#entity-browser-ckeditor-media-browser-form input.is-entity-browser-submit").trigger("click");
          }
          if($('div.ui-dialog-buttonpane div.ui-dialog-buttonset button[type="button"]').length){
            $('div.ui-dialog-buttonpane div.ui-dialog-buttonset button[type="button"]').trigger('click');
          } 
          */            
        }       
      }

      $('input[data-drupal-selector = edit-field-banner-region-content-0-subform-field-content-max-width-0-value]').keyup(function(){
        if ($(this).val() > 100){
          $(this).val('100');
        } 
      })


      $('.field--name-field-content-max-width input').on('focus', function(){
        $(this).on('keyup', function(){
          if ($(this).val() > 100){
            $(this).val('100');
          } 
        });
        
      })
    }
    
  }
})(jQuery, Drupal, drupalSettings);

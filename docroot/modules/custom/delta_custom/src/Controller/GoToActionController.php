<?php

namespace Drupal\delta_custom\Controller;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\file\Entity\File;
use Drupal\media\Entity\Media;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\delta_services\DeltaService;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\delta_cache\DeltaCacheService;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use \Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


/**
 * Controller routines for user routes.
 */
class GoToActionController extends ControllerBase {

  /**
   * To get path alias.
   *
   * @var service
   */
  public $coreService;
  /**
   * Drupal\sync_product\ProductImportController definition.
   *
   * @var \Drupal\sync_product\ProductImportInterface
   */
  protected $service;
  /**
   * Drupal\delta_services\DeltaService definition.
   *
   * @var \Drupal\delta_services\DeltaService
   */
  protected $deltaservice;
  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Drupal\Core\Messenger\MessengerInterface definition.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;
  /**
   * Symfony\Component\HttpFoundation\RequestStack definition.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Drupal\delta_services\DeltaService definition.
   *
   * @var \Drupal\delta_cache\DeltaCache
   */
  protected $deltacache;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;


  /**
   * Class constructor.
   */
  public function __construct(ConfigFactoryInterface $config_factory,
                              MessengerInterface $messenger,
                              DeltaService $deltaservice,
                              RequestStack $requestStack,
                              DeltaCacheService $deltacache,
                              EntityTypeManagerInterface $entity_type_manager,
                              ContainerInterface $coreService) {
    $this->configFactory = $config_factory;
    $this->messenger = $messenger;
    $this->deltaservice = $deltaservice;
    $this->requestStack = $requestStack;
    $this->deltacache = $deltacache;
    $this->pdpCacheSlug = 'PDP_';
    $this->entityTypeManager = $entity_type_manager;
    $this->coreService = $coreService;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('messenger'),
      $container->get("delta_services.service"),
      $container->get('request_stack'),
      $container->get('delta_cache.cache_service'),
      $container->get('entity_type.manager'),
      $container->get('service_container')
    );
  }


  /**
   * Get Product Sku Details.
   */
  public function doExecute($type, $sku) {

    //Get host name
    $site = $this->requestStack->getCurrentRequest()->getSchemeAndHttpHost();
    //Get site name ex: delta, peerless ..
    $config = $this->configFactory->get('delta_services.deltaservicesconfig');
    $site_name = $config->get('global_site_name');
    // Find fucntional

    if ($type === 'function') {
      // param passing are site and category name, which redirect to category page kitchen or bath.
      // goto/function/kitchen or goto/function/bath
      $category = $sku;
      $this->findFunction($site, $category, $site_name);
    }



    // Fetch products.
    $products = $this->fetchProduct($sku);

    if (!empty($products)) {
      switch ($type) {
        case 'techdoc':
        case 'product':
          //Lets create product and redirect to PDP.
          if (is_array($products) && array_key_exists('create', $products)) {
            switch ($site_name) {
              case 'peerless':
                $redirect = $site . "/pdp/" . $sku;
                break;
              case 'delta':
              case 'brizo':
                $redirect = $site . "/product-detail/" . $sku;
                break;
              default:
                throw new NotFoundHttpException();
            }
            $response = new RedirectResponse($redirect);
            $response->send();
          }
          //Lets redirect to PDP.
          $this->findProduct($site, $products, $site_name);
          break;
        case 'large-image':
        case 'detail-image':
        case 'thumbnail-image':
        case 'thumbnail':
          $redirect = $this->findImage($type, $products);
          if (!empty($redirect)) {
            return new TrustedRedirectResponse($redirect);
          }
          break;
        default:
          throw new NotFoundHttpException();
          break;
      }
    }
    else {
      throw new NotFoundHttpException();
    }
  }

  // Redirect to image page.
  public function findImage($type, $products) {
    $imgTypeArr = [
      'large-image' => 'heroImageLarge',
      'thumbnail-image' => 'heroImageSmall',
      'detail-image' => 'heroImage',
      'thumbnail' => 'heroImageSmall'
    ];
    if (array_key_exists($type, $imgTypeArr)) {
      if (null != $products && (!empty($products))) {
        if (is_array($products) && array_key_exists($imgTypeArr[$type], $products)) {
          $getImgKeyName = $imgTypeArr[$type];
          return $products[$getImgKeyName];
        }
      }
    }
  }

  /**
   * Redirect to product page.
   */
  public function findProduct($site, $products, $site_name) {
    $section = "";
    switch ($site_name) {
      case 'delta':
        $section = "#specsTab";
        break;
      case 'peerless':
        $section = "#product-features-and-support";
        break;
      case 'brizo':
        $section = "";
        break;
    }
    if (null != $products && (!empty($products))) {
      $sku = $products['name'];
      if (is_array($products) && array_key_exists('categories', $products)) {
        $categories = $products['categories'];
        if (count($categories) > 0) {
          foreach ($categories as $categorie) {
            if (strpos(strtolower($categorie), 'kitchen') !== false) {
              $area = "kitchen";
              $redirect = $site . "/" . $area . "/product/" . $sku . $section;
              if ($site_name == 'delta' && is_array($products) && array_key_exists('values', $products) && array_key_exists('RepairPart', $products['values']) && $products['values']['RepairPart'] == 'T') {
                $redirect = $site . "/parts-product-detail?modelNumber=" . $sku;
              }
              $response = new RedirectResponse($redirect);
              $response->send();
            }
            if (strpos(strtolower($categorie), 'bath') !== false) {
              switch ($site_name) {
                case 'delta':
                  $area = "bathroom";
                  break;
                case 'peerless':
                case 'brizo':
                  $area = "bath";
                  break;
                default:
                  $area = "bath";
                  break;
              }
              $redirect = $site . "/" . $area . "/product/" . $sku . $section;
              if ($site_name == 'delta' && is_array($products) && array_key_exists('values', $products) && array_key_exists('RepairPart', $products['values']) && $products['values']['RepairPart'] == 'T') {
                $redirect = $site . "/parts-product-detail?modelNumber=" . $sku;
              }
              $response = new RedirectResponse($redirect);
              $response->send();
            }
            $redirect = $this->redirectPDP($site, $sku, $site_name);
            $response = new RedirectResponse($redirect);
            $response->send();
          }
        }
        else {
          $redirect = $this->redirectPDP($site, $sku, $site_name);
          $response = new RedirectResponse($redirect);
          $response->send();
        }
      }
    }
    return [];
  }

  /**
   * Generate Product URL based on site, sku and site name.
   */
  function redirectPDP($site, $sku, $site_name) {
    switch ($site_name) {
      case 'peerless':
        $redirect = $site . "/pdp/" . $sku;
        break;
      case 'delta':
      case 'brizo':
        $redirect = $site . "/product-detail/" . $sku;
        break;
      default:
        $redirect = $site . "/product-detail/" . $sku;
        break;
    }
    return $redirect;
  }

  /**
   * Redirect to category page.
   */
  public function findFunction($sitePath, $category, $site_name) {
    $redirect = '';
    if (strpos(strtolower($category), 'kitchen') !== FALSE) {
      $redirect = $sitePath . "/kitchen";
    }
    if (strpos(strtolower($category), 'bath') !== FALSE) {
      switch ($site_name) {
        case 'delta':
          $redirect = $sitePath . "/bathroom";
          break;
        case 'peerless':
        case 'brizo':
          $redirect = $sitePath . "/bath";
          break;
        default:
          $redirect = $sitePath . "/bath";
          break;
      }
    }
    if ($redirect) {
      $response = new RedirectResponse($redirect);
      $response->send();
    }
    else {
      throw new NotFoundHttpException();
    }
  }

  /**
   * Get Product Sku Details.
   */
  public function fetchProduct($sku) {
    $config = $this->configFactory->get('delta_services.deltaservicesconfig');
    $site = $config->get('global_site_name');
    switch ($site) {
      case 'peerless':
        $cid = 'peer_pdp_' . $sku;
        break;
      default:
        $cid = $this->pdpCacheSlug . $sku;
        break;
    }
    $cachedResultObj = $this->deltacache->deltaGetCache($cid);

    // Check data exist in cache.
    if (!empty($cachedResultObj)) {
      return json_decode(json_encode($cachedResultObj), TRUE);
    }



    //If data not exist in cache, local DB, hit API Service, Create product, Redirect to mentioned URL.
    $domain = $config->get('api_base_path');
    $pdpUrl = $domain . $config->get('global_product_api_single');
    $url = str_replace("{productSku}", strtoupper($sku), $pdpUrl);
    $result = $this->deltaservice->apiCall($url, "GET", [], "", "PRODUCTGOTO");

    if ($result['status'] == 'ERROR') {
      $this->messenger()->addMessage("Product [ $sku ] which you are trying is not exist in our system", 'error');
      return [];
    }
    if ($result['status'] == 'SUCCESS' && (!empty($result['response'])) && ($result['response'] != 'null')) {

      $this->deltacache->deltaSetCache($cid, json_decode($result['response'], FALSE));
      $content = Json::decode($result['response']);
      //This FLAG, To create the product.
      $content['create'] = '1';
      return $content;
    }

    return [];
  }
}

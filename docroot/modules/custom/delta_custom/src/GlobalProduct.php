<?php

namespace Drupal\delta_custom;

use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\PropertyAccess\PropertyAccess;

/**
 * Class GlobalProduct.
 */
class GlobalProduct {

  /**
   * Drupal\Core\Messenger\MessengerInterface definition.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Drupal\Core\Logger\LoggerChannelFactoryInterface definition.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;



  /**
   * Constructs a new GlobalProduct object.
   */
  public function __construct(MessengerInterface $messenger,
                              LoggerChannelFactoryInterface $logger_factory,
                              ConfigFactoryInterface $config_factory) {
    $this->messenger = $messenger;
    $this->loggerFactory = $logger_factory;
    $this->configFactory = $config_factory;
  }


   /**
   * Show finish based on foundational, AvailableToOrderDate, AvailableToShipDate attribute value.
   * Argument: Product response object.
   */
  public function getFoundationalOrderShipAttr($getDataObj){
    $config = $this->configFactory->get('delta_services.deltaservicesconfig');
    $validateOrderShipDate = $config->get('order_ship_date');  
    $foundAttr = $config->get('foundational_attribute');   
    $getDataArr = json_decode(json_encode($getDataObj), TRUE);
    $showFinish = TRUE;
    $validateOrderShipFlag = TRUE;
    $foundationalAttrFlag = TRUE;

    if((!empty($getDataArr)) && is_array($getDataArr)){
       $productData = $getDataArr['values'];
       $AvailableToOrderDate = array_key_exists('AvailableToOrderDate', $productData) ? $productData['AvailableToOrderDate'] : '';
       $AvailableToShipDate  = array_key_exists('AvailableToShipDate', $productData) ? $productData['AvailableToShipDate'] : '';
      if($AvailableToOrderDate === $validateOrderShipDate || $AvailableToShipDate === $validateOrderShipDate)
      {
        $showFinish = FALSE;
        $validateOrderShipFlag = FALSE;
      }
      $foundationalAttr = "T";
      if(array_key_exists($foundAttr, $productData)){
        $foundationalAttr = $productData[$foundAttr];
      }
      if($foundationalAttr == "F"){
        $showFinish = FALSE;
        $foundationalAttrFlag = FALSE;        
      }
      //if WebsiteUS or WebsitePeerlessUS is T and If availableToOrderDate or availabeToshipDate is 9999-12-31, Corresponding finish swatch should not appear
      if($foundationalAttrFlag == TRUE && $validateOrderShipFlag == FALSE){
        $showFinish = FALSE;
      }
    }
    return $showFinish;
  }
}

<?php

namespace Drupal\delta_custom\TwigExtension;

use Drupal\Core\Render\Element;
use Drupal\Core\Render\Markup;

/**
 * Class CustomHtmlEntities.
 */
class CustomHtmlEntities extends \Twig_Extension {

  /**
   * {@inheritdoc}
   */
  public function getTokenParsers() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getNodeVisitors() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getFilters() {
    return [
      new \Twig_SimpleFilter('html_entities', [$this, 'customHtmlEntities']),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getTests() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getFunctions() {
    return array(
      new \Twig_SimpleFunction('render_string', array($this, 'render_string'), array('is_safe' => array('html'))),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getOperators() {
    return [];
  }

  /**
   * Convert title from given a parameters.
   *
   * @param string $string
   *   The string
   * @return string $string
   *   Converted html string
   */
  public function customHtmlEntities($string) {
    $render_string = Markup::create($string);
    return html_entity_decode($render_string);
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return 'delta_custom.twig.extension';
  }

}

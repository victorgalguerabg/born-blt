<?php

namespace Drupal\global_purge_content\Form;

use Drupal\commerce_order\Entity\Order;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\ConfigFormBase;

/**
 * Configure settings for this site.
 */
class DeltaPurgeContentForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'global_purge_content';
  }


  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['range'] = [
      '#type' => 'select',
      '#title' => $this->t('Select range'),
      '#description' => $this->t('Number of records will be deleted.'),
      '#required' => TRUE,
      '#options' => [
        '10' => '10',
        '100' => '100',
        '200' => '200',
        '300' => '300',
        '400' => '400',
        '500' => '500',
        '600' => '600',
        '700' => '700',
        '800' => '800',
        '900' => '900',
        '1000' => '1000',
        'all' => 'All',
      ],
    ];

    $form['entity_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Select Entity Type'),
      '#required' => TRUE,
      '#options' => [
        'commerce_order' => 'Orders',
        //'user' => 'Users',
        //'content' => 'Content',
        //'vocabulary' => 'Vocabulary',
      ],
    ];

    $form['order_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Select orders type'),
      '#options' => [
        'completed' => 'Completed Orders',
        'cart' => 'Cart Orders',
      ],
      '#states' => [
        'visible' => [
          ':input[name="entity_type"]' => ['value' => 'commerce_order'],
        ],
      ],
    ];


    $userRole = [];
    $roles = \Drupal\user\Entity\Role::loadMultiple();
    foreach ($roles as $i => $role) {
      $userRole[$i] = $role->get('label');
      unset($userRole['administrator']);
      unset($userRole['authenticated']);
    }

    $form['user_role'] = [
      '#type' => 'select',
      '#title' => $this->t('Select user role'),
      '#options' => $userRole,
      '#states' => [
        'visible' => [
          ':input[name="entity_type"]' => ['value' => 'user'],
        ],
      ],
    ];

    $contentType = [];
    $node_types = \Drupal\node\Entity\NodeType::loadMultiple();
    foreach ($node_types as $i => $types) {
      $contentType[$i] = $types->get('name');
    }
    $form['content_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Select content type'),
      '#options' => $contentType,
      '#states' => [
        'visible' => [
          ':input[name="entity_type"]' => ['value' => 'content'],
        ],
      ],
    ];

    $vocabularyOptions = [];
    $vocabulary = \Drupal\taxonomy\Entity\Vocabulary::loadMultiple();
    foreach ($vocabulary as $i => $taxonomy) {
      $vocabularyOptions[$i] = $taxonomy->get('name');
    }
    $form['vocabulary_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Select vocabulary'),
      '#options' => $vocabularyOptions,
      '#states' => [
        'visible' => [
          ':input[name="entity_type"]' => ['value' => 'vocabulary'],
        ],
      ],
    ];


    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => t('Delete'),
    ];
    return $form;


  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $entity = $values['entity_type'];
    $range = $values['range'];
    $operations = [];
    $chunk = 50;


    switch ($entity) {
      case 'commerce_order':
        $orderType = $values['order_type'];
        $query = \Drupal::entityQuery('commerce_order');
        if($orderType == 'completed') {
          $query->condition('state', 'completed');
        }
        if($orderType == 'cart') {
          $query->condition('cart', TRUE);
        }
        $query->sort('created', 'DESC');
        if($range != 'all') {
          $query->range(0, $range);
        }
        $output = $query->execute();
        $results = array_chunk($output, $chunk);
        foreach ($results as $result) {
          $operations[] = [
            '\Drupal\global_purge_content\DeleteEntity::deleteOrderEntity',
            [$result]
          ];
        }
        break;
      case 'user':
        $userRole = $values['user_role'];
        $query = \Drupal::entityQuery('user');
        $query->condition('roles', $userRole);
        if($range != 'all') {
          $query->range(0, $range);
        }
        $output = $query->execute();
        $results = array_chunk($output, $chunk);
        foreach ($results as $result) {
          $operations[] = [
            '\Drupal\global_purge_content\DeleteEntity::deleteUserEntity',
            [$result]
          ];
        }
        break;
      case 'content':
        $entityType = $values['content_type'];
        $query = \Drupal::entityQuery('node');
        $query->condition('type', $entityType);
        if($range != 'all') {
          $query->range(0, $range);
        }
        $query->sort('created', 'DESC');
        $output = $query->execute();
        $results = array_chunk($output, $chunk);
        foreach ($results as $result) {
          $operations[] = [
            '\Drupal\global_purge_content\DeleteEntity::deleteNodeEntity',
            [$result]
          ];
        }
        break;
      case 'vocabulary':
        $output = [];
        $entityType = $values['vocabulary_type'];
        try {
          $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($entityType);
        } catch (\Exception $error) {
          $logger = \Drupal::service('logger.factory');
          $messenger = \Drupal::service('messenger');
          $logger->get('global_purge_content')->alert(t('@err', ['@err' => $error->__toString()]));
          $messenger->addWarning(t('Unable to proceed. Please contact an administrator. Error code: C898F7B0-@line-@time', ['@line' => __LINE__, '@time' => time()]));
        }
        foreach ($terms as $term) {
          $output[$term->tid] = $term->tid;
        }
        $results = array_chunk($output, $chunk);
        foreach ($results as $result) {
          $operations[] = [
            '\Drupal\global_purge_content\DeleteEntity::deleteTermEntity',
            [$result]
          ];
        }
        break;
    }

    $batch = array(
      'title' => t('Deleting...'),
      'init_message' => t('Executing delete entities...'),
      'operations' => $operations,
      'finished' => '\Drupal\global_purge_content\DeleteEntity::deleteEntityFinishedCallback',
    );
    batch_set($batch);
  }

}

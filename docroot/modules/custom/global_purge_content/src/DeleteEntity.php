<?php

namespace Drupal\global_purge_content;


use Drupal\node\Entity\Node;
use \Drupal\commerce_order\Entity\Order;
use Drupal\taxonomy\Entity\Term;
use Drupal\user\Entity\User;

class DeleteEntity {

  public static function deleteOrderEntity($entities, &$context) {
    $message = 'Deleting order...';
    $results = [];
    foreach ($entities as $entity) {
      $order = Order::load($entity);
      $results[] = self::delete($order);
    }
    $context['message'] = $message;
    $context['results'] = $results;
  }

  public static function deleteNodeEntity($entities, &$context) {
    $message = 'Deleting node...';
    $results = [];
    foreach ($entities as $entity) {
      $node = Node::load($entity);
      $results[] = self::delete($node);
    }
    $context['message'] = $message;
    $context['results'] = $results;
  }

  public static function deleteUserEntity($entities, &$context) {
    $message = 'Deleting user...';
    $results = [];
    foreach ($entities as $entity) {
      $user = User::load($entity);
      $results[] = self::delete($user);
    }
    $context['message'] = $message;
    $context['results'] = $results;
  }

  public static function deleteTermEntity($entities, &$context) {
    $message = 'Deleting term...';
    $results = [];
    foreach ($entities as $entity) {
      $term = Term::load($entity);
      $results[] = self::delete($term);
    }
    $context['message'] = $message;
    $context['results'] = $results;
  }

  public static function deleteEntityFinishedCallback($success, $results, $operations) {
    // The 'success' parameter means no fatal PHP errors were detected. All
    // other error management should be handled using 'results'.
    if ($success) {
      $message = t('Delete operation completed.');
    }
    else {
      $message = t('Finished with an error.');
    }
    \Drupal::messenger()->addStatus($message);
    \Drupal::logger('global_purge_content')->notice($message);
  }

  public static function delete($entity) {
    try{
      return $entity->delete();
    } catch(\Exception $error) {
      $logger = \Drupal::service('logger.factory');
      $messenger = \Drupal::service('messenger');
      $logger->get('global_purge_content')->alert(t('@err', ['@err' => $error->__toString()]));
      $messenger->addWarning(t('Unable to proceed, please try again. Error code: 113655A3-@line-@time', ['@line' => $error->getLine(), '@time' => time()]));
    }
  }
}

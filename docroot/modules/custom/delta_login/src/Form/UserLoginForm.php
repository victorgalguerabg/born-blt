<?php

namespace Drupal\delta_login\Form;

use Drupal\Core\Ajax\CloseModalDialogCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\user_account_mgmt\UserAccountManagement;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\user\Entity\User;
use Drupal\Core\Ajax\HtmlCommand;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Request;
use Drupal\checkout_services\CheckoutServices;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\user\UserStorageInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;

/**
 * Implementation of Checkout flow multi step Form.
 */
class UserLoginForm extends FormBase
{

  /**
   * @var \Drupal\user_account_mgmt\UserAccountManagement
   */
  protected $userAccountMgmt;

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $userAccountFactory;

  /**
   * Symfony\Component\HttpFoundation\RequestStack definition.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * @var \Drupal\checkout_services\CheckoutServices
   */
  protected $services;

  /**
   * Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Drupal\Core\Session\AccountProxyInterface
   */
  protected $user;

  /**
   * @var \Drupal\user\PrivateTempStoreFactory
   */
  protected $tempStoreFactory;

  /**
   * @var \Drupal\Core\TempStore\PrivateTempStoreFactory
   */
  protected $userSession;

  /**
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $logger;

  /**
   * Constructs a CheckoutForm object.
   */
  public function __construct(ConfigFactoryInterface $configFactory, UserAccountManagement $usermanagement, MessengerInterface $messenger, RequestStack $request_stack, CheckoutServices $checkoutServices, AccountProxyInterface $currentUser, UserStorageInterface $user, PrivateTempStoreFactory $temp_store_factory, LoggerChannelFactoryInterface $logger)
  {
    $this->requestStack = $request_stack;
    $this->userAccountFactory = $configFactory->get('user_account_mgmt.user_account_mgmt_config');
    $this->userAccountMgmt = $usermanagement;
    $this->messenger = $messenger;
    $this->services = $checkoutServices;
    $this->currentUser = $currentUser;
    $this->user = $user;
    $this->tempStoreFactory = $temp_store_factory;
    $this->userSession = $this->tempStoreFactory->get('loginData');
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container)
  {
    return new static(
      $container->get('config.factory'),
      $container->get('user_account_mgmt.user_account_management'),
      $container->get('messenger'),
      $container->get('request_stack'),
      $container->get('checkout_services.checkoutflow'),
      $container->get('current_user'),
      $container->get('entity_type.manager')->getStorage('user'),
      $container->get('tempstore.private'),
      $container->get('logger.factory')
    );
  }

  /**
   * return FormId
   */
  public function getFormId()
  {
    return 'delta_login_form';
  }

  /**
   * Implementation checkout flow form build().
   */
  public function buildForm(array $form, FormStateInterface $form_state)
  {
    $form['username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Username'),
      '#default_value' =>  "",
      '#attributes' => [
        'class' => ['form-item__textfield form-item__textfield--required'],
      ],
    ];

    $form['password'] = [
      '#type' => 'password',
      '#title' => $this->t('Password'),
      '#default_value' => '',
      '#field_suffix' => '<a role="button" class="form-item__pwd-toggle" aria-label="Show Password">' . $this->t('Show Password') . '</a>',
      '#attributes' => [
        'class' => ['form-item__textfield form-item__textfield--password form-item__textfield--required'],
        'autocomplete' => 'off',
      ],
    ];

    $form['persistent_login'] = [
      '#type' => 'checkbox',
      '#title' => \Drupal::config('persistent_login.settings')->get('login_form.field_label'),
      '#cache' => [
        'tags' => ['config:persistent_login.settings'],
      ],
      '#attributes' => [
        'class' => ['custom-input__native'],
      ],
      '#label_attributes' => ["class" =>  ["custom-input__label"]],
    ];

    $form['path'] = [
      '#type' => 'url',
      '#title' => $this->t('Redirect Path'),
      '#default_value' => trim($this->userAccountFactory->get('userregisterurl')),
    ];

    $form['actions']['login'] = [
      '#type' => 'submit',
      '#value' => $this->t('SIGN IN'),
      '#button_type' => 'primary',
      '#attributes' => [
        'class' => ['checkout-login button--lg'],
      ],
      '#ajax' => [
        'callback' => '::validateUser',
        'event' => 'click',
        'wrapper' => 'edit-output',
      ],
    ];
    $form['forgot_password'] = [
      '#markup' => trim($this->userAccountFactory->get('professionalForgotUrl')),
    ];
    $form['#theme'] = "form_user_login_popup";
    return $form;
  }

  /**
   * Checkout Flow form Validation based on steps.
   *
   */
  public function validateForm(array &$form, FormStateInterface $form_state)
  {
  }

  /**
   * Checkout flow form submit.
   * @param array $form
   * @param FormStateInterface $form_state
   */
  public function submitForm(array &$form, FormStateInterface $form_state)
  {
  }

  /**
   *
   * User Validation through API.
   * @param array $form
   * @param FormStateInterface $form_state
   */
  public function validateUser(array &$form, FormStateInterface $form_state)
  {
    $form_data =  $form_state->getUserInput();
    $errCnt = 0;
    $errorMsg = "";
    if (empty($form_data['username'])) {
      $errCnt++;
      $errorMsg .= "<label class='login-error'>" . $this->t("Please enter username") . "</label>";
    }

    if (empty($form_data['password'])) {
      $errCnt++;
      $errorMsg .= "<label class='login-error'>" . $this->t("Please enter password") . "</label>";
    }
    if ($errCnt == 0) {
      $userData = ["username" => $form_data['username'], "password" => $form_data['password']];
      $authResponse = $this->userAccountMgmt->userValidationApiCall(trim($this->userAccountFactory->get('userAuthApi')), "POST", $userData, [], 'VALIDATE_USR');
      $errorString = $this->userAccountFactory->get('validationErrorMessage');
      if (strlen(trim($errorString)) == 0) {
        $errorString = 'Invalid username/password';
      }
      if (isset($authResponse['success']) && !empty($authResponse['success'])) {

        $request = Request::createFromGlobals();
        $headers = $request->server->getHeaders();
        $eLearningToken = $this->userAccountMgmt->userValidationApiCall(trim($this->userAccountFactory->get('eLearningAuthApi')), 'POST', $userData, [], 'ELEARN_USR_AUTHEN');
        if (isset($eLearningToken)) {
          $request = \Drupal::request();
          $session = $request->getSession();
          $session->set('training_token', $eLearningToken['token']);
          $elearning_redirection_url = $this->userAccountFactory->get('elearning_redirection_url');
          $user_current_path =  $elearning_redirection_url;
        } else {
          $user_current_path =  $this->requestStack->getCurrentRequest()->server->get('HTTP_REFERER');
        }
        $account = user_load_by_name($form_data['username']);
        if ($account) {
          user_login_finalize($account);
          $moduleHandler = \Drupal::service('module_handler');
          if ($moduleHandler->moduleExists('persistent_login')) {
            if ($form_state->getValue('persistent_login', FALSE)) {
              \Drupal::service('persistent_login.token_handler')->setNewSessionToken(\Drupal::service('current_user')->id());
            }
          }
          // Update user description for existing user
          $userDescription = $account->field_group_description->value;
          $userId = $account->field_user_id->value;

          if ($userDescription == '' || $userId == '') {
            $userDetailsurl = trim($this->userAccountFactory->get('userValidateApi')) . $form_data['username'];
            $userDetailsRequest = $this->userAccountMgmt->userValidationApiCall($userDetailsurl, 'GET', '', [], 'ELEARN_ACCT_USR_DETAILS');
            if (isset($userDetailsRequest['userID'])) {
              $account->set('field_group_description', $userDetailsRequest['groupDescription']);
              $account->set('field_user_id', $userDetailsRequest['userID']);
              $account->save();
            }
          }

          // User Based session storage
          try {
            $this->userSession->set('currentUserGRPDescription', $userDescription);
          } catch (\Exception $error) {
            $this->logger->get('delta_login')->alert(t('@err', ['@err' => $error->__toString()]));
            $this->messenger->addWarning(t('Unable to proceed. Please try again later. If problem persists please contact an administrator. Error code: 8874925B-@line-@time', ['@line' => __LINE__, '@time' => time()]));
          }

          try {
            $this->userSession->set('currentUserId', $userId);
          } catch (\Exception $error) {
            $this->logger->get('delta_login')->alert(t('@err', ['@err' => $error->__toString()]));
            $this->messenger->addWarning(t('Unable to proceed. Please try again later. If problem persists please contact an administrator. Error code: 8874925B-@line-@time', ['@line' => __LINE__, '@time' => time()]));
          }


          // Check Existing Roles.
          $userExistsDetailsurl = trim($this->userAccountFactory->get('userValidateApi')) . $form_data['username'];
          $userExistsDetailsRequest = $this->userAccountMgmt->userValidationApiCall($userExistsDetailsurl, 'GET', '', [], 'ELEARN_GET_USR_ROLES');
          if (isset($userExistsDetailsRequest['userID'])) {
            try {
              // Generate roles.
              $existsRoles = $this->userAccountMgmt->generateRoles($userExistsDetailsRequest['roles']);
            } catch (\Exception $error) {
              $this->logger->get('delta_login')->alert(t('@err', ['@err' => $error->__toString()]));
              $this->messenger->addWarning(t('Unable to proceed. Please try again later. If problem persists please contact an administrator. Error code: 8874925B-@line-@time', ['@line' => __LINE__, '@time' => time()]));
            }

            // Get the roles to be assigned to the user.
            $existsGetUserRoles = [];
            if ($existsRoles) {
              $existsGetUserRoles = $this->userAccountMgmt->getRoles($userExistsDetailsRequest['roles']);
              $userLoad = $this->user->load($this->currentUser->id());
              foreach ($existsGetUserRoles as $roleName) {
                if ($roleName != 'authenticated') {
                  $userLoad->addRole($roleName);
                  try {
                    // Generate roles.
                    $userLoad->save();
                  } catch (\Exception $error) {
                    $this->logger->get('delta_login')->alert(t('@err', ['@err' => $error->__toString()]));
                    $this->messenger->addWarning(t('Unable to proceed. Please try again later. If problem persists please contact an administrator. Error code: 8874925B-@line-@time', ['@line' => __LINE__, '@time' => time()]));
                  }
                }
              }
              $mappedRoles = $this->currentUser->getRoles();
              foreach ($mappedRoles as $existingRoleNames) {
                if (!in_array($existingRoleNames, $existsGetUserRoles)) {
                  $userLoad->removeRole($existingRoleNames);
                  try {
                    // Generate roles.
                    $userLoad->save();
                  } catch (\Exception $error) {
                    $this->logger->get('delta_login')->alert(t('@err', ['@err' => $error->__toString()]));
                    $this->messenger->addWarning(t('Unable to proceed. Please try again later. If problem persists please contact an administrator. Error code: 8874925B-@line-@time', ['@line' => __LINE__, '@time' => time()]));
                  }
                }
              }
            }
          }

          //$this->services->combineUserCarts();
          $response = new AjaxResponse();
          $selector = '.login_popup';
          $persist = FALSE;
          $response->addCommand(
            new CloseModalDialogCommand($selector, $persist)
          );
          $random = substr(md5(mt_rand()), 0, 10);
          $response->addCommand(
            new RedirectCommand($user_current_path . '?' . $random . '&user=' . $form_data['username'])
          );
          return $response;
        } else {
          $userDetailsurl = trim($this->userAccountFactory->get('userValidateApi')) . $form_data['username'];
          $userDetailsRequest = $this->userAccountMgmt->userValidationApiCall($userDetailsurl, 'GET', '', [], 'ELEARN_ACCTNOTEXIST_GETUSR_DETAILS');
          if (isset($userDetailsRequest['userID'])) {
            try {
              // Generate roles.
              $roles = $this->userAccountMgmt->generateRoles($userDetailsRequest['roles']);
            } catch (\Exception $error) {
              $this->logger->get('delta_login')->alert(t('@err', ['@err' => $error->__toString()]));
              $this->messenger->addWarning(t('Unable to proceed. Please try again later. If problem persists please contact an administrator. Error code: 8874925B-@line-@time', ['@line' => __LINE__, '@time' => time()]));
            }

            // Get the roles to be assigned to the user.
            $getUserRoles = [];
            if ($roles) {
              $getUserRoles = $this->userAccountMgmt->getRoles($userDetailsRequest['roles']);
            }

            $userCreate = User::create([
              'name' => $form_data['username'],
              'password' => rand(1, 10000),
              'mail' => $userDetailsRequest['email'],
              'roles' => $getUserRoles,
              'status' => 1,
            ]);
            $userCreate->set('field_first_name', $userDetailsRequest['firstName']);
            $userCreate->set('field_last_name', $userDetailsRequest['lastName']);
            $userCreate->set('field_user_id', $userDetailsRequest['userID']);
            $userCreate->set('field_group_description', $userDetailsRequest['groupDescription']);
            try {
              $savedUser = $userCreate->save();
            } catch (\Exception $error) {
              $this->logger->get('delta_login')->alert(t('@err', ['@err' => $error->__toString()]));
              $this->messenger->addWarning(t('Unable to proceed. Please try again later. If problem persists please contact an administrator. Error code: 8874925B-@line-@time', ['@line' => __LINE__, '@time' => time()]));
            }

            if ($savedUser) {
              $userAccount = user_load_by_name($form_data['username']);
              user_login_finalize($userAccount);

              // User Based session storage
              try {
                $this->userSession->set('currentUserGRPDescription', $userDetailsRequest['groupDescription']);
              } catch (\Exception $error) {
                $this->logger->get('delta_login')->alert(t('@err', ['@err' => $error->__toString()]));
                $this->messenger->addWarning(t('Unable to proceed. Please try again later. If problem persists please contact an administrator. Error code: 8874925B-@line-@time', ['@line' => __LINE__, '@time' => time()]));
              }
              try {
                $this->userSession->set('currentUserId', $userDetailsRequest['userID']);
              } catch (\Exception $error) {
                $this->logger->get('delta_login')->alert(t('@err', ['@err' => $error->__toString()]));
                $this->messenger->addWarning(t('Unable to proceed. Please try again later. If problem persists please contact an administrator. Error code: 8874925B-@line-@time', ['@line' => __LINE__, '@time' => time()]));
              }
              $moduleHandler = \Drupal::service('module_handler');
              if ($moduleHandler->moduleExists('persistent_login')) {
                if ($form_state->getValue('persistent_login', FALSE)) {
                  \Drupal::service('persistent_login.token_handler')->setNewSessionToken(\Drupal::service('current_user')->id());
                }
              }
              //$this->services->combineUserCarts();
              //$current_path =  $this->requestStack->getCurrentRequest()->server->get('HTTP_REFERER');
              $response = new AjaxResponse();
              $selector = '.login_popup';
              $persist = FALSE;
              $response->addCommand(
                new CloseModalDialogCommand($selector, $persist)
              );
              $random = substr(md5(mt_rand()), 0, 10);
              $response->addCommand(
                new RedirectCommand($user_current_path . '?' . $random . '&user=' . $form_data['username'])
              );
              return $response;
            }
          } else {
            $msg = "<label class='login-error'>" . $errorString . "</label>";
            return (new AjaxResponse())->addCommand(
              new HtmlCommand('.user-login-error', "$msg")
            );
          }
        }
      } else {

        $msg = "<label class='login-error'>" . $errorString . "</label>";
        return (new AjaxResponse())->addCommand(
          new HtmlCommand('.user-login-error', "$msg")
        );
      }
    } else {
      return (new AjaxResponse())->addCommand(
        new HtmlCommand('.user-login-error', $errorMsg)
      );
    }
  }
}

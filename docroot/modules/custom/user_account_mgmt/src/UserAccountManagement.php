<?php

namespace Drupal\user_account_mgmt;

use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use GuzzleHttp\ClientInterface;
use Drupal\user\Entity\Role;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\RequestException;


class UserAccountManagement {

  protected $entityTypeManager;

  protected $accountId;

  protected $currentUser;

  protected $priorityAcces;

  /**
   * Logger Factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactory
   */
  protected $loggerFactory;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $customEntityTypeManager;

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $userConfigFactory;

  protected $entityManager;

  /**
   * Guzzle\Client instance.
   *
   * @var \Guzzle\Client
   */
  protected $httpClient;


  /**
   * {@inheritdoc}
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, AccountProxyInterface $currentUser, LoggerChannelFactory $loggerFactory, ConfigFactoryInterface $configFactory, ClientInterface $httpClient, MessengerInterface $messenger) {
    $this->current_user = $currentUser;
    $this->customEntityTypeManager = $entityTypeManager;
    $this->loggerFactory = $loggerFactory->get('checkout_flow');
    $this->configFactory = $configFactory->get('delta_services.deltaservicesconfig');
    $this->userConfigFactory = $configFactory->get('user_account_mgmt.user_account_mgmt_config');
    $this->entityManager = $entityTypeManager;
    $this->httpClient = $httpClient;
    $this->messenger = $messenger;
    try {
      $this->entityTypeManager = $entityTypeManager->getStorage('user_role');
    } catch (\Exception $error) {
      $this->logger->get('user_account_mgmt')->alert(t('@err', ['@err' => $error->__toString()]));
      $this->messenger->addWarning(t('Unable to proceed. Please try again leter. Please contact an administrator. Error code: AC7FB864-@line-@time', ['@line' => __LINE__, '@time' => time()]));
    }
  }

  /**
   * Http Client function to call Ecommerce API.
   * Outdated function, soonly removed from code base.
   */
   public function userValidationApiCallOutdated($url, $method,array $MoreHeaders, $obj, $requested_from='USER_API') {

    // Check to Ecommerce API.
    $this->loggerFactory->notice('Ecommerce-REQ-@message', [
      '@message' => $url,
    ]);
    $this->loggerFactory->notice('Ecommerce-REQ-OBJ-@message', [
      '@message' => json_encode($obj),
    ]);

    $headers['FORWARDED'] = NULL;
    $headers = ['Content-Type' => 'application/json'];
    $options = [];
    if (count($headers) > 0) {
      $options['headers'] = $headers;
    }
    $options['verify'] = FALSE;
    if (count($obj) > 0) {
      $options['body'] = json_encode($obj);
    }
    $options['http_errors'] = FALSE;
    $options['auth'] = [$this->configFactory->get('delta_api_username'),$this->configFactory->get('delta_api_password') ];

    $request = $this->httpClient->$method($url, $options);

    $response = json_decode($request->getBody(),TRUE);

    $this->loggerFactory->notice('Ecommerce-RES- @message', [
      '@message' => json_encode($response),
    ]);

    return $response;
  }


  /**
   * Http Client function to call Ecommerce API.
   */
  public function userValidationApiCall($url, $method, $obj, $MoreHeaders = [], $requested_from = 'USER_API') {
    $headers = [];
    if((is_array($MoreHeaders)) && (!empty($MoreHeaders))){
      $headers = array_merge($headers, $MoreHeaders);
    }
    // Check to Ecommerce API.
    $this->loggerFactory->notice('Ecommerce_REQ: @message', [
      '@message' => $url,
    ]);

    $headers['FORWARDED'] = NULL;
    $headers['Content-Type' ] = 'application/json';
    $options = [];
    if (count($headers) > 0) {
      $options['headers'] = $headers;
    }
    $options['verify'] = FALSE;
    if (count($obj) > 0) {
      $options['body'] = json_encode($obj);
    }

    if($requested_from != 'VALIDATE_USR'){
      //Dont record password on the log
      if(is_array($obj) && array_key_exists('password', $obj)){
        $obj['password'] = 'xxxxxx';
      }
      $this->loggerFactory->notice('Ecommerce_REQ_OBJ: @message', [
        '@message' => json_encode($obj),
      ]);
    }

    $options['http_errors'] = FALSE;
    $options['auth'] = [$this->userConfigFactory->get('user_api_username'),$this->userConfigFactory->get('user_api_password') ];
    try {
        $request = $this->httpClient->$method($url, $options);
        $response = json_decode($request->getBody(),TRUE);
          $this->loggerFactory->notice('Ecommerce_RES_@message', [
            '@message' => json_encode($response),
          ]);
      } catch (ConnectException $e) {
        $this->loggerFactory->notice('USR_VPN_Connection_ERR_@message', [
          '@message' => $requested_from . ' Connection Error',
        ]);
        return FALSE;
      }
       if (NULL === $request) {
        $responseData = ['Web service connectivity issue'];
        $this->loggerFactory->notice('USR_VPN_Connection_ERR_@message', [
          '@message' => $requested_from . ' Connection Error',
        ]);
      }
    return $response;
  }

  /**
   * Function to generate roles based on the API response.
   *
   * @param array $roles
   *   List of roles returened from API.
   *
   * @return bool
   *   return TRUE or FALSE
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function generateRoles( $roles = []) {
    if (!empty($roles)) {
      $roleManager = $this->customEntityTypeManager->getStorage('user_role');
      if (is_array($roles)) {
        foreach ($roles as $roleName) {
          $checkRole = $this->checkRoleExist($roleName);
          if ($checkRole == 'T') {
            $role = $roleManager->loadByProperties(['label' => $roleName]);

            if (!(array_shift($role) instanceof Role)) {
              $roleId = str_replace(" ", "_", $roleName);
              $createRole = $roleManager->create([
                'id' => strtolower($roleId),
                'label' => $roleName,
              ]);
              $createRole->save();
            }
          }
        }
      }
      else {
        $role = $roleManager->loadByProperties(['label' => $roles]);
        $checkRoles = $this->checkRoleExist($roles);
        if ($checkRoles == 'T') {
          if (!(array_shift($role) instanceof Role)) {
            $roleId = str_replace(" ", "_", $roles);
            $createRole = $roleManager->create([
              'id' => strtolower($roleId),
              'label' => $roles,
            ]);
            $createRole->save();
          }
        }
      }
    }
    return TRUE;
  }

  /**
   * Function to generate user roles to assign in user create.
   *
   * @param array $roles
   *   List of roles sent from the API.
   *
   * @return array
   *   reutrn array of roles.
   */
  public function getRoles( $roles = []) {
    $userRoles = [];
    $userRoles[] = 'authenticated';
    if (!empty($roles)) {
      if(is_array($roles)) {
        foreach ($roles as $roleName) {
          $checkRoles = $this->checkRoleExist($roleName);
          if ($checkRoles == 'T') {
            $userRoles[] = strtolower(str_replace(" ", "_", $roleName));
          }
        }
      }
      else {
        $checkRolesOne = $this->checkRoleExist($roles);
        if ($checkRolesOne == 'T') {
          $userRoles[] = strtolower(str_replace(" ", "_", $roles));
        }
      }
    }
    return $userRoles;
  }

  /**
   * Function to Send email to registered Email Address.
   *
   * @param $email
   *
   * @return array
   *   return array of response.
   */
  public function userForgotPassword( $email ) {

    $url = $this->userConfigFactory->get('forgotApi')."/".$email;
    $response = $this->userValidationApiCall($url,'POST', $email, [],'FORGOT_PASS');
    return $response;
  }

  /**
   * Function to Return value exists or not.
   *
   * @param $apiRole
   *
   * @return string
   *   return string value T or F.
   */
  public function checkRoleExist($apiRole) {
    $rolesConfigured = $this->userConfigFactory->get('allowedRolesCreate');
    if (!empty($rolesConfigured)) {
      $splitRoles = explode(",", $rolesConfigured);
      if (is_array($splitRoles) && count($splitRoles) > 0) {
        if (in_array($apiRole,$splitRoles)) {
          return "T";
        }
        else {
          return "F";
        }
      }
      else {
        return "F";
      }
    }
    else {
      return "F";
    }
  }
}

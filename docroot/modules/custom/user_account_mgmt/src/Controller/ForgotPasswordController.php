<?php

namespace Drupal\user_account_mgmt\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\user_account_mgmt\UserAccountManagement;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * An Order Conformation controller.
 */
class ForgotPasswordController extends ControllerBase {

  /**
   * @var \Drupal\user_account_mgmt\UserAccountManagement
   */
  protected $userAccountMgmt;

  /**
   * Object initiative.
   */
  public function __construct(UserAccountManagement $usermanagement) {
    $this->userAccountMgmt = $usermanagement;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('user_account_mgmt.user_account_management')
    );
  }

  /**
   * Returns a render-able array for a Order Status page.
   */
  public function forgotPassword() {
    $apiResponse = [];
    if(isset($_POST['email'])) {
      $apiResponse = $this->userAccountMgmt->userForgotPassword(trim($_POST['email']));
    }
    return new JsonResponse($apiResponse);
  }
}

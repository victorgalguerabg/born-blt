<?php

namespace Drupal\user_account_mgmt\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user_account_mgmt\UserAccountManagement;
use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Implementation of Forgot Password Form.
 */
class ForgotpwdForm extends FormBase {

  /**
   * @var \Drupal\user_account_mgmt\UserAccountManagement
   */
  protected $userAccountMgmt;

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $userAccountFactory;

  /**
   * Constructs a Forgot password Form object.
   */
  public function __construct(UserAccountManagement $usermanagement, MessengerInterface $messenger, ConfigFactoryInterface $configFactory) {
    $this->userAccountMgmt = $usermanagement;
    $this->messenger = $messenger;
    $this->userAccountFactory = $configFactory->get('user_account_mgmt.user_account_mgmt_config');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('user_account_mgmt.user_account_management'),
      $container->get('messenger'),
      $container->get('config.factory')
    );
  }

  /**
   * Implementation Forgot password getFormId().
   *
   */
  public function getFormId() {
    return 'checkout_forgot_password';
  }

  /**
   * Implementation checkout flow form build().
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['#attached']['library'][] = 'user_account_mgmt/forgot_password';
    $form['#attached']['drupalSettings']['errorConfigs'] = $this->getErrorsConfig();

    $form['forgotPassEmail'] = [
      '#type' => 'email',
      '#title' => $this->t('Email'),
      '#required' => TRUE,
      '#attributes' => [
        'class' => ['form-item__textfield form-item__textfield--required'],
      ],
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
      '#button_type' => 'primary',
      '#attributes' => [
        'class' => ['forgot-password-btn'],
      ]
    ];
    $form['forgot_password'] = [
      '#markup' => trim($this->userAccountFactory->get('professionalForgotUrl')),
    ];
    $form['#theme'] = 'form_forgot_password';

    return $form;
  }

  /**
   * Forgot password Validation based on steps.
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * Forgot password form submit.
   * @param array $form
   * @param FormStateInterface $form_state
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    
  }

  /**
   * Return error config messages.
   */
  public function getErrorsConfig() {
    return array(
      "emptyError" => $this->t("Please enter your email address"),
      "validError" => $this->t("Enter a valid email address."),
      "apiErrorMsg" => $this->t("Sorry, that email address does not exist in our systems."),
      "apiSuccessMsg" => $this->t("Please check your email for the reset password link!"),
    );
  }
}

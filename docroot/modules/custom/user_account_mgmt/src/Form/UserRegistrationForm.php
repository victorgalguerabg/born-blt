<?php

namespace Drupal\user_account_mgmt\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user_account_mgmt\UserAccountManagement;
use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Implementation of User Registration Form.
 */
class UserRegistrationForm extends FormBase {
  /**
   * @var \Drupal\user_account_mgmt\UserAccountManagement
   */
  protected $userAccountMgmt;

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $generalConfig;

  /**
   * Constructs a User Registration Form object.
   */
  public function __construct( UserAccountManagement $usermanagement, MessengerInterface $messenger,  ConfigFactoryInterface $configFactory) {
    $this->userAccountMgmt = $usermanagement;
    $this->messenger = $messenger;
    $this->configFactory = $configFactory->get('user_account_mgmt.user_account_mgmt_config');
    $this->generalConfig = $configFactory->get('delta_services.deltaservicesconfig');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('user_account_mgmt.user_account_management'),
      $container->get('messenger'),
      $container->get('config.factory')
    );
  }

  /**
   * Implementation User Registration getFormId().
   * @return string
   */
  public function getFormId() {
    return 'user_registration_form';
  }

  /**
   * Implementation User Registration form build().
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $disable_captcha = $this->generalConfig->get('captcha_disable');
    $form['firstname'] = [
      '#type' => 'textfield',
      '#title' => $this->t('First Name*'),
      '#default_value' => '',
      '#attributes' => [
        'class' => ['form-item__textfield form-item__textfield--required required validate-name'],
      ],
    ];

    $form['lastname'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Last Name*'),
      '#default_value' => '',
      '#attributes' => [
        'class' => ['form-item__textfield form-item__textfield--required required validate-name'],
      ],
    ];

    $form['email'] = [
      '#type' => 'email',
      '#title' => $this->t('Email*'),
      '#default_value' => '',
      '#attributes' => [
        'class' => ['form-item__textfield form-item__textfield--required required validate-email'],
      ],
    ];

    $form['password'] = [
      '#type' => 'password',
      '#title' => $this->t('Password*'),
      '#size' => 25,
      '#attributes' => [
        'class' => ['form-item__textfield form-item__textfield--required required new_pass password'],
      ],
    ];

    $form['retypepassword'] = [
      '#type' => 'password',
      '#title' => $this->t('Re-type Password*'),
      '#size' => 25,
      '#attributes' => [
        'class' => ['form-item__textfield form-item__textfield--required required check__confirm_pass password'],
      ],
    ];

    $form['notification'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('I\'d like to receive offers, news, and information.'),
      '#attributes' => ['class' => ['custom-input custom-input__native']],
      '#label_attributes' => [
        'class' => ['custom-input__label option form-item__label'],
      ],
      '#default_value' => FALSE,
      '#prefix' => '<div class=" custom-input--checkbox">',
      '#suffix' => '</div>'
    ];
    if ($disable_captcha !== 1) {
      $form['captcha'] = [
        '#type' => 'captcha',
        '#captcha_type' => 'recaptcha/reCAPTCHA',
        '#attributes' => [
          'class' => ['captcha'],
        ],
      ];

      $form['hdn_captcha'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Re-Captcha'),
        '#attributes' => [
          'class' => ['required captcha-validate'],
        ],
      ];
    }

    $form['registerproduct'] = [
      '#type' => 'submit',
      '#value' => $this->t('Register Account'),
      '#button_type' => 'primary',
      '#attributes' => [
        'class' => ['user-register-btn button--lg'],
      ],
      '#ajax' => [
        'callback' => '::userRegister',
        'event' => 'click',
        'wrapper' => 'edit-output',
      ],
    ];

    $form['reset'] = [
      '#type' => 'button',
      '#value' => $this->t('Reset'),
      '#button_type' => 'primary',
      '#attributes' => [
        'class' => ['ship-continue-btn'],
      ]
    ];

    $form['#theme'] = 'form_user_registration';

    return $form;
  }

  /**
   * User Registration Validation based on steps.
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * User Registration form submit.
   * @param array $form
   * @param FormStateInterface $form_state
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

  }

  public function userRegister(array &$form, FormStateInterface $form_state) {
    $disable_captcha = $this->generalConfig->get('captcha_disable');
    if(!empty($form_state->getValue('firstname')) && !empty($form_state->getValue('lastname')) && !empty($form_state->getValue('email')) && !empty($form_state->getValue('password')) && (!empty($form_state->getValue('hdn_captcha')) || $disable_captcha == 1)) {
      $password = $form_state->getValue('password');
      $uppercase = preg_match('@[A-Z]@', $password);
      $lowercase = preg_match('@[a-z]@', $password);
      $number    = preg_match('@[0-9]@', $password);
      $specialChars = preg_match('@[^\w]@', $password);
      if(!$uppercase || !$lowercase || !$number || !$specialChars || strlen($password) < 8) {
        return (new AjaxResponse())->addCommand(
          new HtmlCommand('.global-user-register-err-empty', "")
        );
      }
      else {
        if($form_state->getValue('password') == $form_state->getValue('retypepassword')) {
          $firstName = $form_state->getValue('firstname');
          $lastname = $form_state->getValue('lastname');
          $email = $form_state->getValue('email');
          $password = $form_state->getValue('password');
          $notification = $form_state->getValue('notification');
          $obj = [
            "email" => $email,
            "firstName" => $firstName,
            "lastName" => $lastname,
            "password" => $password,
            "optedInNewProducts" => "1",
            "optedInNewsletter" => $notification,
          ];
          $response = $this->userAccountMgmt->userValidationApiCall(trim($this->configFactory->get('userRegistration')),"POST", $obj, [],'USR_REGISTER');
          if($response === NULL || $response === 'null' || empty($response)){
            $error = 'We are unable to process your request at this time. Please try again later.';
            $msg = '<div class="alert alert--error">'.$error.'<i class="icon-icon_filter-hide-small"></i></div>';
              return (new AjaxResponse())->addCommand(
                new HtmlCommand('.global-user-register-err', $msg)
              );
          }
          if (isset($response['userID'])) {
            $response = new AjaxResponse();
            $response->addCommand(new HtmlCommand(
                '.user_registration',
                '')
            );
            $successMsg = '<div class="alert alert--success">'.$this->t("Registration Successful, Click to ").'<a href="/user-login" class="utility-menu-nav__link use-ajax" data-dialog-options="{&quot;width&quot;:800, &quot;dialogClass&quot;:&quot;login_popup&quot;}" data-dialog-type="modal" data-drupal-link-system-path="user-login">Log in</a> Here <i class="icon-icon_filter-hide-small"></i></div>';
            $response->addCommand(new HtmlCommand(
                '.global-user-register-err',
                $successMsg)
            );
            return $response;
          }
          else {
            if (isset($response['errors'])) {
              $error = "";
              if (isset($response['errors']['message'])) {
                $error .= $response['errors']['message'];
              }
              else {
                foreach ($response['errors'] as $key => $val) {
                  $error .= $val['message']."</br>";
                }
              }
              $msg = '<div class="alert alert--error">'.$error.'<i class="icon-icon_filter-hide-small"></i></div>';
              return (new AjaxResponse())->addCommand(
                new HtmlCommand('.global-user-register-err', $msg)
              );
            }
          }
        }
        else {
          return (new AjaxResponse())->addCommand(
            new HtmlCommand('.global-user-register-err-empty', "")
          );
        }
      }

    }
    else {
      return (new AjaxResponse())->addCommand(
        new HtmlCommand('.global-user-register-err-empty', "")
      );
    }
  }
}

<?php
namespace Drupal\user_account_mgmt\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * An User management API Config.
 */
class UserAccountMgmtForm extends ConfigFormBase
{

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs a new User Account ApiSettingsForm object.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    // parent::__construct($config_factory);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'user_account_mgmt.user_account_mgmt_config',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'user_account_management';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('user_account_mgmt.user_account_mgmt_config');

    $form['userAccountValidate'] = [
      '#type' => 'details',
      '#title' => $this->t('User Account Validate'),
    ];

    $form['userAccountValidate']['userAuthApi'] = [
      '#type' => 'textfield',
      '#title' => $this->t('User Authentication validate API'),
      '#description' => $this->t('User Authentication validate API.'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('userAuthApi'),
    ];

    $form['userAccountValidate']['userValidateApi'] = [
      '#type' => 'textfield',
      '#title' => $this->t('User Details API'),
      '#description' => $this->t('User Details API.'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('userValidateApi'),
    ];

    $form['userAccountValidate']['userRegistration'] = [
      '#type' => 'textfield',
      '#title' => $this->t('User Registration API'),
      '#description' => $this->t('User Registration API.'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('userRegistration'),
    ];

    $form['userAccountValidate']['allowedRolesCreate'] = [
      '#type' => 'textarea',
      '#title' => $this->t('User Allowed Roles to Create'),
      '#description' => $this->t('User Allowed Roles to Create.'),
      '#default_value' => $config->get('allowedRolesCreate'),
      '#resizable' => 'both',
    ];

    $form['userAccountValidate']['validationErrorMessage'] = [
      '#type' => 'textarea',
      '#title' => $this->t('User Validation Failure Message'),
      '#description' => $this->t('Text to be shown on failure attempt of User Login.'),
      '#default_value' => $config->get('validationErrorMessage'),
      '#resizable' => 'both',
    ];


    $form['user_api_credentials'] = [
      '#type' => 'details',
      '#title' => $this->t('User API Credentials'),
    ];

    $form['user_api_credentials']['user_api_username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Username'),
      '#description' => $this->t('Enter User API Authentication Username.'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('user_api_username'),
    ];
    $form['user_api_credentials']['user_api_password'] = [
      '#type' => 'password',
      '#title' => $this->t('Password'),
      '#description' => $this->t('Enter User API Authentication Password.'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('user_api_password'),
      '#description' => 'Password for user API service authentication. If you have already entered your password before,
      you should leave this field blank, unless you want to change the stored password.
      Please note that this password will be stored as plain-text inside Drupal\'s core configuration variables.',
    ];

    $form['forgotPassword'] = [
      '#type' => 'details',
      '#title' => $this->t('Forgot Password API Details'),
    ];

    $form['forgotPassword']['forgotApi'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Forgot Password API'),
      '#description' => $this->t('Forgot Password API.'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('forgotApi'),
    ];

    $form['forgotPassword']['professionalForgotUrl'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Professional Account Forgot Password URL'),
      '#description' => $this->t('Professional Account Forgot Password URL.'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('professionalForgotUrl'),
    ];
    $form['userAccountValidate']['eLearningAuthApi'] = [
      '#type' => 'textfield',
      '#title' => $this->t('User eLearning authentication API'),
      '#description' => $this->t('User eLearning authentication API.'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('eLearningAuthApi'),
    ];

    $form['userAccountValidate']['userregisterurl'] = [
      '#type' => 'textfield',
      '#title' => $this->t('User registration URL'),
      '#description' => $this->t('User registration URL.'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('userregisterurl'),
    ];

    $form['elearning'] = [
      '#type' => 'details',
      '#title' => $this->t("Elearning Details"),
    ];
    $form['elearning']['elearning_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Elearning URL'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('elearning_url'),
    ];
    $form['elearning']['ouid'] = [
      '#type' => 'textfield',
      '#title' => $this->t('OUID'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('ouid'),
    ];
    $form['elearning']['elearning_redirection_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Elearning Redirection URL'),
      '#description' => $this->t('Field uses for url redirection after logging in.'),
      '#default_value' =>  $config->get('elearning_redirection_url'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $this->config('user_account_mgmt.user_account_mgmt_config')
      ->set('userValidateApi', $form_state->getValue('userValidateApi'))
      ->set('userAuthApi', $form_state->getValue('userAuthApi'))
      ->set('forgotApi', $form_state->getValue('forgotApi'))
      ->set('user_api_username', $form_state->getValue('user_api_username'))
      ->set('professionalForgotUrl', $form_state->getValue('professionalForgotUrl'))
      ->set('userRegistration', $form_state->getValue('userRegistration'))
      ->set('allowedRolesCreate', $form_state->getValue('allowedRolesCreate'))
      ->set('eLearningAuthApi', $form_state->getValue('eLearningAuthApi'))
      ->set('elearning_url', $form_state->getValue('elearning_url'))
      ->set('validationErrorMessage', $form_state->getValue('validationErrorMessage'))
      ->set('userregisterurl', $form_state->getValue('userregisterurl'))
      ->set('ouid', $form_state->getValue('ouid'))
      ->set('elearning_redirection_url', $form_state->getValue('elearning_redirection_url'))
      ->save();

    if (strlen(trim($form_state->getValue('user_api_password'))) > 0) {
      $this->config('user_account_mgmt.user_account_mgmt_config')
        ->set('user_api_password', $form_state->getValue('user_api_password'))
        ->save();
    }
  }
}

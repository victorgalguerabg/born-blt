<?php

/**
 * @file
 * Contains \Drupal\user_account_mgmt\Plugin\Block\UserRegistrationBlock.
 */

namespace Drupal\user_account_mgmt\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormBuilderInterface;


/**
 * Provides a 'User Registration' Block.
 *
 * @Block(
 *   id = "user_registration_block",
 *   admin_label = @Translation("User Registration Block"),
 *   category = @Translation("User Registration Block"),
 * )
 */
class UserRegistrationBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The form builder.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface.
   */
  protected $formBuilder;

  /**
   * Constructs a new UserPasswordBlock plugin.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Form\FormBuilderInterface $formBuilder
   *   The form builder.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, FormBuilderInterface $formBuilder) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->formBuilder = $formBuilder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('form_builder')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];
    $build['form'] = $this->formBuilder->getForm('Drupal\user_account_mgmt\Form\UserRegistrationForm');
    return $build;
  }
}

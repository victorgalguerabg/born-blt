(function ($, Drupal, drupalSettings) {

    Drupal.behaviors.shippingFormValidate = {
        attach: function (context, settings) {
            $(".forgot-password-btn").once().on("click", function (e) {
                e.preventDefault();

                $(".validation-error").hide();
                var hasError = false;
                var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
                var emailaddressVal = $("#edit-forgotpassemail").val();
                if(emailaddressVal == '') {
                    $("#edit-forgotpassemail").after('<span class="validation-error">'+drupalSettings.errorConfigs.emptyError+'</span>');
                    hasError = true;
                }
                else if(!emailReg.test(emailaddressVal)) {
                    $("#edit-forgotpassemail").after('<span class="validation-error">'+drupalSettings.errorConfigs.validError+'</span>');
                    hasError = true;
                }
                if(hasError == false) {
                    $.ajax({
                        url: "/forgot-password-reset",
                        type: 'POST',
                        data: { "email": emailaddressVal },
                        async: false,
                        success: function (result) {
                            $(".forgot-pass-form-section").hide();
                            if (result['success'] == true) {
                                $(".forgot-password-desc").after('<div class="alert alert--success">' + drupalSettings.errorConfigs.apiSuccessMsg + '<i class="icon-icon_filter-hide-small"></i></div>');
                            }
                            else {
                                $(".forgot-password-desc").after('<div class="alert alert--error">'+drupalSettings.errorConfigs.apiErrorMsg+'<i class="icon-icon_filter-hide-small"></i></div>');
                            }
                        },
                        error: function (result) {
                        }
                    });
                }
            });
        }
    };
})(jQuery, Drupal, drupalSettings);

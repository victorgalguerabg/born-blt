Delta Service API Usage Example
--------------------------------

<?php

use Drupal\delta_services\DeltaService;


/**
 * Class MyController.
 */
class MyController extends ControllerBase{

	/**
	* Drupal\delta_services\DeltaService definition.
	*
	* @var \Drupal\delta_services\DeltaService
	*/
	protected $deltaservice;

  	public function __construct(DeltaService $deltaservice) {
		$this->deltaservice = $deltaservice;
	}

	/**
	  * {@inheritdoc}
	  */
	public static function create(ContainerInterface $container) {
	  // Instantiates this form class.
	  return new static(
		// Load the service required to construct this class.
	    $container->get('delta_services.service')
	   );
	}

	public myFunction(){
		....
		$response = $this->deltaservice->apiCall($url, $method, $headers, $data, 'CATEGORY-SYNC');
	}
?>

<?php

namespace Drupal\delta_services;

use CURLFile;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\ClientInterface;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Drupal\path_alias\AliasManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Database\Connection;

/**
 * Class DeltaService.
 */
class DeltaService {

  /**
   * Drupal\Core\Messenger\MessengerInterface definition.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Drupal\Core\Logger\LoggerChannelFactoryInterface definition.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Guzzle Http Client.
   *
   * @var GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * An alias manager to find the alias for the current system path.
   *
   * @var \Drupal\path_alias\AliasManagerInterface
   */
  protected $aliasManager;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * DB connection resource.
   *
   * @var connection
   */
  protected $connection;

  /**
   * Constructs a new DeltaService object.
   */
  public function __construct(MessengerInterface $messenger,
                              LoggerChannelFactoryInterface $logger_factory,
                              ConfigFactoryInterface $config_factory,
                              ClientInterface $http_client,
                              AliasManagerInterface $alias_manager,
                              EntityTypeManagerInterface $entityManager,
                              Connection $connection) {
    $this->messenger = $messenger;
    $this->loggerFactory = $logger_factory;
    $this->configFactory = $config_factory;
    $this->httpClient = $http_client;
    $this->aliasManager = $alias_manager;
    $this->prop_config = $this->configFactory->get('product_details.settings');
    $this->comingsoon = $this->prop_config->get('rest_api_settings.comming_soon_ribbontext');
    $this->discontinuedText = $this->prop_config->get('rest_api_settings.discontinue_ribbontext');
    $this->entityTypeManager = $entityManager;
    $this->connection = $connection;
  }

  /**
   * Function to call webservice API Call.
   *
   * @param string $url
   *   webServices API URL.
   * @param string $method
   *   WebServices method. It can be GET, POST, PUT, DELETE.
   * @param array $headers
   *   webServices headers.
   * @param string $data
   *   Process with data if required.
   * @param string $requested_from
   *   Specifiy the request type. This will used to track the logging.
   *
   * @return array
   *   A render response array.
   *
   * @throws exception
   *   Throws exception if find any errors.
   */
  public function apiCall($url, $method, array $headers, $data, $requested_from = "GENERAL") {
    $logName = $requested_from . '-API';
    $this->loggerFactory->get($logName)->info($url);
    $wcsResponse = [];
    $headers['FORWARDED'] = NULL;
    try {
      $client = $this->httpClient;
      $options = [];
      if (count($headers) > 0) {
        $options['headers'] = $headers;
      }
      $options['headers'] = ['Content-Type' => 'application/json'];
      $options['verify'] = FALSE;
      if (!empty($data) && count($data) > 0) {
        /**
         * @Todo
         */
        if ($requested_from == "Contact-us") {
          $options["form_params"] = $data;
        }
        else {
          $options['body'] = json_encode($data);
        }
      }
      $options['http_errors'] = FALSE;
      $config = $this->configFactory->get('delta_services.deltaservicesconfig');
      $username = $config->get('delta_api_username');
      $password = $config->get('delta_api_password');
      if ($username == '' || $password == '') {
        $this->messenger->addWarning('API credentials missing, please add');
        $this->loggerFactory->get('API Credentials missing')
          ->notice('API Cred');
        return FALSE;
      }
      $options['auth'] = [
        $username,
        $password,
      ];
      try {
        $request = $client->$method($url, $options);
      } catch (ConnectException $e) {
        $this->messenger->addError('Connection error');
        $this->loggerFactory->get('VPN Connection error')
          ->notice($requested_from . "Connection Error");
        return FALSE;
      }

      if (NULL === $request) {
        $responseData = ['Web service connectivity issue'];
        $this->loggerFactory->get('SYNC Error ')
          ->notice('SYNC Error ' . $requested_from . json_encode($responseData));
      }
      $response = $request->getBody()->getContents();
      if (!empty($response) && preg_match("/en_us\\/product/i", $url)) {
        $resultData = json_decode($response);
        if (!empty($resultData) && !is_null($resultData)) {
          $responseData = self::dataTranslations($resultData);
        } else {
          $responseData = $resultData;
        }
      }
      else {
        $responseData = json_decode($response);
      }

      //$this->loggerFactory->get('delta_services ' . $requested_from)->info(print_r($responseData, TRUE));
      if (!empty($responseData->errors)) {
        switch ($responseData->errors[0]->type) {
          case 'UnknownIdentifierError':
            $this->loggerFactory->get('SYNC Error')
              ->notice($requested_from . json_encode($responseData));
            $wcsResponse = [
              'status' => 'ERROR',
              'response' => $responseData,
            ];
        }
      }
      else {
        $wcsResponse = [
          'status' => 'SUCCESS',
          'response' => json_encode($responseData),
        ];
      }
    } catch (RequestException $e) {
      if (method_exists($e, 'getResponse')) {
        $response = $e->getResponse();
        $this->loggerFactory->get('Request Exception')
          ->notice($requested_from . $response);
        if (method_exists($e, 'getBody') && method_exists($e, 'getContents')) {
          $responseBodyAsString = $response->getBody()->getContents();
          $decodedResponse = json_decode($responseBodyAsString);
          $wcsResponse = [
            'status' => 'ERROR',
            'response' => $decodedResponse['errors'],
          ];
        }
        else {
          $wcsResponse = [
            'status' => 'ERROR',
            'response' => 'error',
          ];
        }
      }
      else {
        $wcsResponse = [
          'status' => 'ERROR',
          'response' => 'error',
        ];
      }
    } catch (ClientException $clientException) {
      $response = $clientException->getResponse();
      $this->loggerFactory->get('Request')->notice($requested_from . $response);
    }
    return $wcsResponse;
  }

  /**
   * Function to call webservice API Call.
   *
   * @param string $url
   *   webServices API URL.
   * @param string $method
   *   WebServices method. It can be GET, POST, PUT, DELETE.
   * @param array $headers
   *   webServices headers.
   * @param string $data
   *   Process with data if required.
   * @param string $requested_from
   *   Specifiy the request type. This will used to track the logging.
   *
   * @return array
   *   A render response array.
   *
   * @throws exception
   *   Throws exception if find any errors.
   */
  public function apiSfCall($url, $method, array $headers, $data, $requested_from = "GENERAL") {

    $wcsResponse = [];
    try {
      $client = $this->httpClient;
      $options = [];
      $options['headers'] = ['Content-Type' => 'application/json'];
      if (count($headers) > 0) {
        $options['headers'] = array_merge(['Content-Type' => 'application/json'], $headers);
      }

      $options['verify'] = FALSE;
      if (!empty($data) && count($data) > 0) {
        /**
         * @Todo
         */
        if ($requested_from == "Contact-us") {
          $options["form_params"] = $data;
        }
        else {
          $options['body'] = json_encode($data);
        }
      }
      $options['http_errors'] = FALSE;

      try {
        $request = $client->$method($url, $options);
      } catch (ConnectException $e) {
        $this->messenger->addError('SF Connection error');
        $this->loggerFactory->get('SF Connection error')
          ->notice($requested_from . "Connection Error");
        return FALSE;
      }

      if (NULL === $request) {
        $responseData = ['Web service connectivity issue'];
        $this->loggerFactory->get('SYNC Error ')
          ->notice('SYNC Error ' . $requested_from . json_encode($responseData));
      }
      $response = $request->getBody()->getContents();
      if (!empty($response) && preg_match("/en_us\\/product/i", $url)) {
        $resultData = json_decode($response);
        if (!empty($resultData) && !is_null($resultData)) {
          $responseData = self::dataTranslations($resultData);
        } else {
          $responseData = $resultData;
        }
      }
      else {
        $responseData = json_decode($response);
      }

      //$this->loggerFactory->get('delta_services ' . $requested_from)->info(print_r($responseData, TRUE));
      if (!empty($responseData->errors)) {
        switch ($responseData->errors[0]->type) {
          case 'UnknownIdentifierError':
            $this->loggerFactory->get('SYNC Error')
              ->notice($requested_from . json_encode($responseData));
            $wcsResponse = [
              'status' => 'ERROR',
              'response' => $responseData,
            ];
        }
      }
      else {
        $wcsResponse = [
          'status' => 'SUCCESS',
          'response' => json_encode($responseData),
        ];
      }
    } catch (RequestException $e) {
      if (method_exists($e, 'getResponse')) {
        $response = $e->getResponse();
        $this->loggerFactory->get('Request Exception')
          ->notice($requested_from . $response);
        if (method_exists($e, 'getBody') && method_exists($e, 'getContents')) {
          $responseBodyAsString = $response->getBody()->getContents();
          $decodedResponse = json_decode($responseBodyAsString);
          $wcsResponse = [
            'status' => 'ERROR',
            'response' => $decodedResponse['errors'],
          ];
        }
        else {
          $wcsResponse = [
            'status' => 'ERROR',
            'response' => 'error',
          ];
        }
      }
      else {
        $wcsResponse = [
          'status' => 'ERROR',
          'response' => 'error',
        ];
      }
    } catch (ClientException $clientException) {
      $response = $clientException->getResponse();
      $this->loggerFactory->get('Request')->notice($requested_from . $response);
    }
    return $wcsResponse;
  }

  /**
   *Implementation of new_curlfile_lib
   */
  public function new_curlfile_lib($filename, $mimetype, $path) {
    // Create a CURLFile object / oop method.
    $cfile = new CURLFile($path, $mimetype, $filename);
    return $cfile;
  }

  /**
   * Implementation of web_service_api_call_curl.
   */
  public function web_service_api_call_curl($url, $method, $headers, array $data, $requested_from = "GENERAL") {
    $this->loggerFactory->get('delta_services API Info  Contact Us')
      ->notice($requested_from);
    $this->loggerFactory->get('delta_services API Info Contact Us ' . $requested_from)
      ->notice(json_encode($data));
    $config = $this->configFactory->get('delta_services.deltaservicesconfig');
    $login = $config->get('delta_api_username');
    $password = $config->get('delta_api_password');
    $authorization = $config->get('api_authentication');
    $ch = curl_init();
    // Set method.
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($ch, CURLOPT_USERPWD, "$login:$password");

    // Set headers.
    curl_setopt($ch, CURLOPT_HTTPHEADER, [
      'Authorization: Basic ' . $authorization,
      'Content-Type: multipart/form-data; charset=utf-8; boundary=__X_PAW_BOUNDARY__',
      'Cookie: BNI_persistence=0000000000000000000000005d6f7e0a00005000',
    ]);
    // Added specifically for ubuntu.
    $specialHeader[] = "Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7";
    curl_setopt($ch, CURLOPT_HTTPHEADER, $specialHeader);
    // Set body.
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    // curl_setopt($ch, CURLOPT_POSTFIELDS, $data);.
    $response = curl_exec($ch);

    $this->loggerFactory->get('delta_services API Info Contact Us execu resp ')
      ->notice($requested_from . json_encode($response));

    // Check the HTTP Status code.
    $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    $response_msg = '';
    if (200 !== $httpCode) {
      $response = "$httpCode: ";
      switch ($httpCode) {
        case 200:
          $response_msg .= "Valid API";
          break;

        case 404:
          $response_msg .= "API Not found";
          break;

        case 401:
        case 405:
          $response_msg .= "API Error: Check Authentication and SSL certificate";
          break;

        case 500:
          $response_msg .= "Our servers replied with an error.";
          break;

        case 502:
          $response_msg .= "Our servers may be down or being upgraded. Hopefully they will be OK soon!";
          break;

        case 503:
          $response_msg .= "Mail service ; unavailable. Hopefully it will be OK soon!";
          break;

        default:
          $response_msg = 'Undocumented error: ' . $httpCode . ' : ' . curl_error($ch);
          break;
      }
      $this->loggerFactory->get('delta_services Curl Response ' . $requested_from)
        ->notice(json_encode($response));
      $this->loggerFactory->get('delta_services Error ' . $requested_from)
        ->notice($response_msg);

    }
    curl_close($ch);
    return $response;
  }

  /**
   * Implementation of data cleansing method.
   */
  public function dataTranslations(&$resultData) {
   // $accessor = PropertyAccess::createPropertyAccessor();
    $accessor = PropertyAccess::createPropertyAccessorBuilder()
      ->enableMagicCall()
      ->getPropertyAccessor();
    if ($accessor->isReadable($resultData, 'content')) {
      $contentData = $accessor->getValue($resultData, 'content');
      foreach ($contentData as $key => $contentValue) {
        self::setCollectionURL($contentValue, $accessor);
        self::setDescription($contentValue, $accessor);
        self::setDefaultRefCollection($contentValue, $accessor);
        self::setCollection($contentValue, $accessor);
        self::setRibbonText($contentValue, $accessor);
      }
    }
    else {
      self::setCollectionURL($resultData, $accessor);
      self::setDescription($resultData, $accessor);
      self::setDefaultRefCollection($resultData, $accessor);
      self::setCollection($resultData, $accessor);
      self::setRibbonText($resultData, $accessor);
    }
    return $resultData;
  }

  /**
   * Implementation of Set Description.
   */
  public function setDescription(&$contentValue, $accessor) {
    $itemData = self::translationsCode($accessor->getValue($contentValue, 'description'));
    $accessor->setValue($contentValue, 'description', $itemData);
  }


  /**
   * Implementation of Set Collection URL
   */
  public function setCollectionURL(&$contentValue, $accessor) {
    $vid = 'collections';
    $repairPart ="F";
    $viewAllCollectionLink = '#';
    if ($accessor->isReadable($contentValue, 'values.Collection')) {
      $term_name = $accessor->getValue($contentValue, 'values.Collection');

      $termData = taxonomy_term_load_multiple_by_name($term_name, $vid);
      $term = reset($termData);

      if ($accessor->isReadable($contentValue, 'categories')) {
        $categories = $accessor->getValue($contentValue, 'categories');
      }
      $category = '';

      if ($accessor->isReadable($contentValue, 'values.RepairPart')) {
        $repairPart = $accessor->getValue($contentValue, 'values.RepairPart');
      }

      $kitchenResult = preg_grep('/Kitchen(\w+)/', $categories);
      if (count($kitchenResult) > 0) {
        $category = 'kitchen';
      }

      $bathroomResult = preg_grep('/Bathroom(\w+)/', $categories);
      if (count($bathroomResult) > 0) {
        $category = 'bathroom';
      }
      if(!empty($term)){
        if ($repairPart == "F" && $category == 'kitchen') {
          $collectionLink = property_exists($term, "field_kitchen_collection_url") ? $term->field_kitchen_collection_url->value : '';
          $fullLink = '/kitchen/collections/' . $collectionLink;
        }
        elseif ($repairPart == "F" && $category == 'bathroom') {
           $collectionLink = property_exists($term, "field_bathroom_collection_url") ? $term->field_bathroom_collection_url->value : '';
          $fullLink = '/bathroom/collections/' . $collectionLink;
        }
        elseif ($repairPart == "F" && empty($categories)) {
          $collectionLink = property_exists($term, "field_kitchen_collection_url") ?  $term->field_kitchen_collection_url->value: '';
          $fullLink = '/kitchen/collections/' . $collectionLink;
        }
        if (!empty($fullLink)) {
          $viewAllCollectionLink = $this->aliasManager->getAliasByPath($fullLink);
        }
      }
    }

    if(empty($contentValue->values)){
      $contentValue->values = new \stdClass();
    }
    $contentValue->values->viewAllCollectionLink = $viewAllCollectionLink;
  }

  /**
   * Implementation of Set Description.
   */
  public function setRibbonText(&$contentValue, $accessor) {
    $availableToOrderDate = '';
    $availableToShipDate = '';
    $orderDate = $shipDate = '';
    $currentDate = strtotime(date('Y-m-d', time()));

    if ($accessor->isReadable($contentValue, 'values.AvailableToOrderDate')) {
      $availableToOrderDate = $accessor->getValue($contentValue, 'values.AvailableToOrderDate');
      $orderDate = strtotime($availableToOrderDate);
    }

    if ($accessor->isReadable($contentValue, 'values.AvailableToShipDate')) {
      $availableToShipDate = $accessor->getValue($contentValue, 'values.AvailableToShipDate');
      $shipDate = strtotime($availableToShipDate);
    }

    if (empty($orderDate) || empty($shipDate)) {
      $shipDate = $currentDate - (60 * 60 * 24);
    }

    if(empty($contentValue->values)){
      $contentValue->values = new \stdClass();
    }
    $contentValue->values->AvailableToOrderDateTimestamp = $orderDate;
    $contentValue->values->AvailableToShipDateTimestamp = $shipDate;
    $contentValue->values->currentDate = $currentDate;
    $contentValue->values->comingSoonText =  $this->comingsoon;
    $materialStatus = isset($contentValue->values->MaterialStatus) ? $contentValue->values->MaterialStatus : "";
    $contentValue->values->discontinued = ($materialStatus >= 50) ? TRUE : FALSE;
    $contentValue->values->discontinueText = $this->discontinuedText;
  }

  /**
   * Implementation of Set Collection.
   */
  public function setCollection(&$contentValue, $accessor) {
    $collectionName = '';
    if ($accessor->isReadable($contentValue, 'values.Collection')) {
      $collectionName = $accessor->getValue($contentValue, 'values.Collection');
      $accessor->setValue($contentValue, 'values.Collection', self::translationsCode($collectionName));
    }
    if ($accessor->isReadable($contentValue, 'values.defaultCollection')) {
      $defaultCollectionName = $accessor->getValue($contentValue, 'values.defaultCollection');
      $accessor->setValue($contentValue, 'values.defaultCollection', self::translationsCode($defaultCollectionName));
      }
    }

  /**
   * Implementation of Set default Collection.
   */
  public function setDefaultRefCollection(&$contentValue, $accessor) {
    $collectionName = '';

    if ($accessor->isReadable($contentValue, 'values.defaultCollection')) {
      if (empty($collectionName)) {
        $collectionName = $accessor->getValue($contentValue, 'values.defaultCollection');
      }
      $contentValue->values->defaultRefCollection = self::translationsCode($collectionName);
    }
  }


  /**
   * Implementation of data cleansing method.
   */
  public function translationsCode($data) {
    $patterns = [];
    $patterns[0] = '/2O\\\u00AE/';
    $patterns[1] = '/20\\\u00AE/';
    $patterns[2] = '/2O\(R\)/';
    $patterns[3] = '/20\(R\)/';
    $patterns[4] = '/2O\(TM\)/';
    $patterns[5] = '/20\(TM\)/';
    $patterns[6] = '/2O.xt/';
    $patterns[7] = '/20.xt/';
    $patterns[8] = '/Touch2O/';
    $patterns[9] = '/Touch20/';
    $patterns[10] = '/.xt®/';
    $patterns[11] = '/®/';
    $patterns[12] = '/\\\u00AE/';
    $patterns[13] = '/\(R\)/';
    $patterns[14] = '/™/';
    $patterns[15] = '/\(TM\)/';
    $patterns[16] = '/\\\u2122/';
    $replacements = [];
    $replacements[0] = '<sub>2</sub>O<sup>&reg;</sup>';
    $replacements[1] = '<sub>2</sub>O<sup>&reg;</sup>';
    $replacements[2] = '<sub>2</sub>O<sup>&reg;</sup>';
    $replacements[3] = '<sub>2</sub>O<sup>&reg;</sup>';
    $replacements[4] = '<sub>2</sub>O<sup>&trade;</sup>';
    $replacements[5] = '<sub>2</sub>O<sup>&trade;</sup>';
    $replacements[6] = '<sub>2</sub>O.<sub>xt</sub>';
    $replacements[7] = '<sub>2</sub>O.<sub>xt</sub>';
    $replacements[8] = 'Touch<sub>2</sub>O';
    $replacements[9] = 'Touch<sub>2</sub>O';
    $replacements[10] = '.xt<sup>&reg;</sup>';
    $replacements[11] = '<sup>&reg;</sup>';
    $replacements[12] = '<sup>&reg;</sup>';
    $replacements[13] = '<sup>&reg;</sup>';
    $replacements[14] = '<sup>&trade;</sup>';
    $replacements[15] = '<sup>&trade;</sup>';
    $replacements[16] = '<sup>&trade;</sup>';
    return preg_replace($patterns, $replacements, $data);

  }

  /**
   * Implementation of data encryption.
   */

  public function encrypt($plain_text) {
    $cryptedText  = $this->doCrypt("ENCRYPT" , $plain_text);
    if ( $cryptedText != '') {
      return $cryptedText;
    }
  }

  /**
   * Implementation of data decryption.
   */

  public function decrypt($plain_text) {
    $cryptedText  = $this->doCrypt("DECRYPT" , $plain_text);
    if ( $cryptedText != '') {
      return $cryptedText;
    }
  }

  public function doCrypt($crypt_type, $text ) {

    $config = $this->configFactory->get('delta_services.deltaservicesconfig');
    $key = $config->get('df_encryption_key');
    $iv = $config->get('df_vector_key');
    $cipher = $config->get('df_encryption_method');
    $secret_key = hash('sha256', $key);
    $secret_iv = substr(hash('sha256', $iv), 0, 16);
    if( $crypt_type == "ENCRYPT" ) {
      $cryptedText = openssl_encrypt($text, $cipher, $secret_key, 0, $secret_iv);
      $cryptedText = base64_encode($cryptedText);
    }
    if( $crypt_type == "DECRYPT" ) {
      $text = base64_decode($text);
      $cryptedText = openssl_decrypt($text, $cipher, $secret_key, 0, $secret_iv);
    }
    return $cryptedText;
  }
  /**
   * Get the multiple products.
   */
  public function getMultipleProductsDelta($recommendedSkus) {
    $sync_config = $this->configFactory->get('delta_services.deltaservicesconfig');
    $domain = $sync_config->get('api_base_path');
    $method = 'GET';
    $headers = add_headers(FALSE);
    $productSkus = '';
    foreach ($recommendedSkus as $recommendedSku) {
      $productSkus .= "&" . http_build_query(['name' => trim($recommendedSku)]);
    }
    $referenceUrl = $domain . $sync_config->get('load_multiple_products');
    $referenceUrl .= "?" . $productSkus;
    $response = self::apiCall($referenceUrl, $method, $headers, '', 'Reference');
    if ($response['status'] == 'SUCCESS') {
      $data = json_decode($response['response']);
      $referenceContents = $data->content;
      $result = [];
      foreach ($referenceContents as $referenceContent) {
        $gtm_categories = '';
        $collection = (property_exists($referenceContent->values, 'Collection')) ? $referenceContent->values->Collection: '';
        $categories = $referenceContent->categories;
        if(!empty($categories)) {
          foreach ($categories as $urlalias) {
            $urlalias_seperate = explode("_", $urlalias);
            if ($urlalias_seperate[0] == "Delta") {
              $gtm_categories = str_replace("_", "/", $urlalias);
              break;
            }
          }
        }
        $result[] = [
          'finish' => property_exists($referenceContent->values, 'Finish') ? $referenceContent->values->Finish : '',
          'gtm_categories' => $gtm_categories,
          'productUrl' => self::getProductUrlDelta($referenceContent->name),
          'description' => $referenceContent->description,
          'image' => $referenceContent->heroImageSmall,
          'type' => $collection,
          'modelNumber' => $referenceContent->name,
          'listPrice' => isset($referenceContent->values->ListPrice) ? $referenceContent->values->ListPrice : '',
        ];
      }
      return $result;
    }
  }
  /**
   * Fetch the product url by using product sku.
   */
  public function getProductUrlDelta($sku) {
    if (!empty($sku)) {
      $products = self::deltaProductExists($sku);
      $variation = key($products);
      if (!empty($variation) && is_numeric($variation)) {
        return $this->aliasManager->getAliasByPath(
          '/product/' . $variation
        );
      }
      else {
        return $this->aliasManager->getAliasByPath(
          '/product-detail/' . $sku
        );
      }
    }
    else {
      return "#";
    }

  }

  /**
   * {@inheritdoc}
   */
  public function deltaProductExists($modelNumber) {
    try {
      $variation = $this->entityTypeManager
        ->getStorage('commerce_product_variation')
        ->loadByProperties(['sku' => $modelNumber]);
    } catch (\Exception $error) {
      $this->loggerFactory->get('delta_services')->alert(t('@err', ['@err' => $error->__toString()]));
      $this->messenger->addWarning(t('Unable to proceed. Please contact an administrator. Error code: 2F7A386B-@line-@time', ['@line' => __LINE__, '@time' => time()]));
    }
    if(!empty($variation)) {
      $variationObj = reset($variation);
      $productId = $variationObj->getProductId();
      return [$productId => current($variation)];
    }

  }

  /**
   * Fetch the product url by using product sku.
   */
  public function getProductNid($sku) {
    $database = $this->connection->select('node__field_product_sku', 'ns');
    $nid = $database
      ->fields('ns', ['entity_id'])
      ->condition('field_product_sku_value', $sku)
      ->condition('bundle', 'commerce_product')
      ->execute()
      ->fetchField();
    if (!empty($nid)) {
      try {
        $node = $this->entityTypeManager->getStorage("node")->load($nid);
      } catch (\Exception $error) {
        $this->loggerFactory->get('delta_services')->alert(t('@err', ['@err' => $error->__toString()]));
        $this->messenger->addWarning(t('Unable to proceed. Please contact an administrator. Error code: 2F7A386B-@line-@time', ['@line' => __LINE__, '@time' => time()]));
      }
      return $node;
    }
    else {
      return '';
    }

  }

  /**
   * Fetch the product url by using product sku.
   */
  public function getProductUrl($sku) {
    $database = $this->connection->select('node__field_product_sku', 'ns');
    $result = $database
      ->fields('ns', ['entity_id'])
      ->condition('field_product_sku_value', $sku)
      ->condition('bundle', 'commerce_product')
      ->execute()
      ->fetchField();
    if (!empty($result)) {
      return $this->aliasManager->getAliasByPath('/node/' . $result);
    }
    else {
      return "#";
    }

  }

  //Remove multiple & from API.
  public function clenupMultipleAmp($api) {
      if(!empty($api)){
        // Lets cleanup unwanted &amp from url
        $tmpQueryRawArr = explode("&", $api);
        if(is_array($tmpQueryRawArr) && count($tmpQueryRawArr)){
          // Lets remove empty values from array
          $queryArr = array_filter($tmpQueryRawArr);
          // Lets re-assemble url query parts with &amp
          $api = implode('&', $queryArr);
        }
    }
    return $api;
  }

  /***exclude products from recommerce category **/
  public function excludeRecommerce($data){
    // List of Recommerce Categories. Configured in /admin/config/delta_services/recommerce-config
    $recommerceConfig = $this->configFactory->get('delta_component_formatter.recommerce_pdp_config_form');
    $recommerceCategories = array_map('trim', explode("\n", $recommerceConfig->get('recommerce_category_list')));
    // List of Recommerce Categories
    $json_arr = [];

    $json_arr = json_decode($data['response'], true);
    $arr_index = array();
    if (!empty($json_arr)) {
      foreach ($json_arr['content'] as $key => $value) {
        if (count(array_intersect($recommerceCategories, $value['categories']))) {
          unset($json_arr['content'][$key]);
          $json_arr['totalElements'] = $json_arr['totalElements'] - 1;
        }
      }
    }
    return json_encode($json_arr);
  }

  /**
   * Function to call webservice API Call.
   *
   * @param string $url
   *   webServices API URL.
   * @param string $method
   *   WebServices method. It can be GET, POST, PUT, DELETE.
   * @param array $headers
   *   webServices headers.
   * @param string $data
   *   Process with data if required.
   * @param string $requested_from
   *   Specifiy the request type. This will used to track the logging.
   *
   * @return array
   *   A render response array.
   *
   * @throws exception
   *   Throws exception if find any errors.
   */
  public function sfccApiCall($url) {
    $wcsResponse = [];
    try {
      $client = $this->httpClient;
      $options = [];
      $method = "GET";
      $request = $client->$method($url, $options);

      if (NULL === $request) {
        $responseData = ['Web service connectivity issue'];
        $this->loggerFactory->get('SYNC Error ')
          ->notice('SYNC Error ' . $requested_from . json_encode($responseData));
      }
      $apiResponse = $request->getBody()->getContents();
      $jsonResponse = json_decode($apiResponse,TRUE);
      $wcsResponse = [
        'status' => 'SUCCESS',
        'response' => json_encode($jsonResponse),
      ];
    }
    catch (RequestException $e) {
      $wcsResponse = [
        'status' => 'ERROR',
        'response' => 'error',
      ];
    }
    catch (ClientException $clientException) {
      $response = $clientException->getResponse();
      $this->loggerFactory->get('Request')->notice($requested_from . $response);
    }
    return $wcsResponse;
  }

  /**
   * Method to get list price of recommerce product and associate to the product response.
   * @param $result
   * @param $plp_product_list_price
   * @return false|string
   */
  public function getListPrice($data, $plp_product_list_price) {
    $skuStringArray = explode(',', $plp_product_list_price);
    $skuReferrer = [];
    if(!empty($skuStringArray)) {
      foreach($skuStringArray as $key => $skuArray) {
        $productSkuArray  = explode('|', $skuArray);
        if(!empty($productSkuArray)) {
            $skuReferrer[$productSkuArray[0]] = $productSkuArray[1];
        }
      }
    }
    $listingData = json_decode($data['response'], true);
    foreach ($listingData['content'] as $key => $value) {
      $listingData['content'][$key]['values']['SalePrice'] = $skuReferrer[$listingData['content'][$key]['name']];
    }
    $data['response'] = json_encode($listingData);
    return $data;
  }

  //bimsmith service.
  public function bimSmithAPICall($bimsmithAPI){
    $readRawBimSmithData = file_get_contents($bimsmithAPI);
    $readBimSmithData = preg_replace('/[\x00-\x1F\x7F-\xFF]/', '', $readRawBimSmithData);
    $bimSmithDataArr = json_decode($readBimSmithData, TRUE);
    $bimSmithResponse = [];
    if(is_array($bimSmithDataArr) && count($bimSmithDataArr) > 0){
      $bimSmithResponse = [
          'status' => 'SUCCESS',
          'response' => json_encode($bimSmithDataArr),
      ];
    }
    return $bimSmithResponse;
  }

  //Bimsmith implementation
  public function getBimsmithData($bimsmithAPI){
    $bimsmithResponse = ['status' => FALSE];
    $response = self::bimSmithAPICall($bimsmithAPI);
    if(is_array($response) && array_key_exists('status', $response) && $response['status'] == 'SUCCESS'){
      $bimsmithArrResponse = json_decode($response['response'], true);
      if(is_array($bimsmithArrResponse) && count($bimsmithArrResponse) > 0){
        $bimsmithResponse = ['status' => TRUE, 'id' => $bimsmithArrResponse['id'], 'name' => $bimsmithArrResponse['name']];
      }
    }
    return $bimsmithResponse;
  }
}

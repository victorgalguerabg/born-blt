<?php

namespace Drupal\delta_services\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class DeltaServicesConfigForm.
 */
class DeltaServicesConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'delta_services.deltaservicesconfig',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'delta_services_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('delta_services.deltaservicesconfig');
    $form['api_base_path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API base path'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('api_base_path'),
    ];
    $form['api_credentials'] = [
      '#type' => 'details',
      '#title' => $this->t("API Credentials"),
    ];
    $form['api_credentials']['delta_api_username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Product API - Username'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('delta_api_username'),
      '#description' => 'Username for product API service authentication',
    ];
    $form['api_credentials']['delta_api_password'] = [
      '#type' => 'password',
      '#title' => $this->t('Product API - Password'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('delta_api_password'),
      '#description' => 'Password for product API service authentication, API Authentication password. If you have already entered your password before,
      you should leave this field blank, unless you want to change the stored password.
      Please note that this password will be stored as plain-text inside Drupal\'s core configuration variables.',
    ];

    $form['salesforce_path'] = [
      '#type' => 'details',
      '#title' => $this->t("Salesforce Details"),
    ];
    $form['salesforce_path']['df_environment'] = [
      '#type' => 'select',
      '#title' => $this->t('Select Salesforce Environment'),
      '#options' => [
        '' => 'Salesforce QA',
        '_dev' => 'Salesforce Stage',
        '_prod' => 'Salesforce Prod',
      ],
      '#default_value' => $config->get('df_environment'),
    ];
    $form['salesforce_path']['sf_login_url'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Salesforce QA user login'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('sf_login_url'),
      '#states' => [
    	'visible' => [
      	  ':input[name="df_environment"]' => ['value' => ''],
    	],
      ],
    ];
    $form['salesforce_path']['sf_client_id'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Salesforce QA Client ID'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('sf_client_id'),
      '#states' => [
    	'visible' => [
      	  ':input[name="df_environment"]' => ['value' => ''],
    	],
      ],
    ];
    $form['salesforce_path']['sf_client_secret'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Salesforce QA Client Secret'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('sf_client_secret'),
      '#states' => [
    	'visible' => [
      	  ':input[name="df_environment"]' => ['value' => ''],
    	],
      ],      
    ];
    $form['salesforce_path']['sf_username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Salesforce QA username'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('sf_username'),
      '#states' => [
    	'visible' => [
      	  ':input[name="df_environment"]' => ['value' => ''],
    	],
      ],      
    ];
    $form['salesforce_path']['sf_password'] = [
      '#type' => 'password',
      '#title' => $this->t('Salesforce QA password'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('sf_password'),
      '#states' => [
    	'visible' => [
      	  ':input[name="df_environment"]' => ['value' => ''],
    	],
      ],      
    ];
    $form['salesforce_path']['notify_to'] = [
      '#type' => 'textfield',
      '#title' => $this->t('QA Email to Address'),
      '#description' => $this->t('Send email notification, when application unable to create case'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('notify_to'),
      '#states' => [
    	'visible' => [
      	  ':input[name="df_environment"]' => ['value' => ''],
    	],
      ],      
    ];
    $form['salesforce_path']['sf_login_url_prod'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Salesforce prod user login'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('sf_login_url_prod'),
      '#states' => [
    	'visible' => [
      	  ':input[name="df_environment"]' => ['value' => '_prod'],
    	],
      ],      
    ];
    $form['salesforce_path']['sf_client_id_prod'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Salesforce prod Client ID'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('sf_client_id_prod'),
      '#states' => [
    	'visible' => [
      	  ':input[name="df_environment"]' => ['value' => '_prod'],
    	],
      ],      
    ];
    $form['salesforce_path']['sf_client_secret_prod'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Salesforce prod Client Secret'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('sf_client_secret_prod'),
      '#states' => [
    	'visible' => [
      	  ':input[name="df_environment"]' => ['value' => '_prod'],
    	],
      ],      
    ];
    $form['salesforce_path']['sf_username_prod'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Salesforce prod username'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('sf_username_prod'),
      '#states' => [
    	'visible' => [
      	  ':input[name="df_environment"]' => ['value' => '_prod'],
    	],
      ],      
    ];
    $form['salesforce_path']['sf_password_prod'] = [
      '#type' => 'password',
      '#title' => $this->t('Salesforce prod password'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('sf_password_prod'),
      '#states' => [
    	'visible' => [
      	  ':input[name="df_environment"]' => ['value' => '_prod'],
    	],
      ],      
    ];
    $form['salesforce_path']['notify_to_prod'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Prod Email to Address'),
      '#description' => $this->t('Send email notification, when application unable to create case'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('notify_to_prod'),
      '#states' => [
    	'visible' => [
      	  ':input[name="df_environment"]' => ['value' => '_prod'],
    	],
      ],      
    ];
    $form['salesforce_path']['sf_login_url_dev'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Salesforce stage user login'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('sf_login_url_dev'),
      '#states' => [
    	'visible' => [
      	  ':input[name="df_environment"]' => ['value' => '_dev'],
    	],
      ],      
    ];
    $form['salesforce_path']['sf_client_id_dev'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Salesforce stage Client ID'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('sf_client_id_dev'),
      '#states' => [
    	'visible' => [
      	  ':input[name="df_environment"]' => ['value' => '_dev'],
    	],
      ],      
    ];
    $form['salesforce_path']['sf_client_secret_dev'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Salesforce stage Client Secret'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('sf_client_secret_dev'),
      '#states' => [
    	'visible' => [
      	  ':input[name="df_environment"]' => ['value' => '_dev'],
    	],
      ],      
    ];
    $form['salesforce_path']['sf_username_dev'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Salesforce stage username'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('sf_username_dev'),
      '#states' => [
    	'visible' => [
      	  ':input[name="df_environment"]' => ['value' => '_dev'],
    	],
      ],      
    ];
    $form['salesforce_path']['sf_password_dev'] = [
      '#type' => 'password',
      '#title' => $this->t('Salesforce stage password'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('sf_password_dev'),
      '#states' => [
    	'visible' => [
      	  ':input[name="df_environment"]' => ['value' => '_dev'],
    	],
      ],      
    ];
    $form['salesforce_path']['notify_to_dev'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Stage Email to Address'),
      '#description' => $this->t('Send email notification, when application unable to create case'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('notify_to_dev'),
      '#states' => [
    	'visible' => [
      	  ':input[name="df_environment"]' => ['value' => '_dev'],
    	],
      ],      
    ];
    $form['salesforce_path']['sf_create_case'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Salesforce create case API'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('sf_create_case'),
    ];
    $form['salesforce_path']['sf_create_cont_ver'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Salesforce content version API'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('sf_create_cont_ver'),
    ];
    $form['salesforce_path']['sf_doc_link'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Salesforce document link API'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('sf_doc_link'),
    ];
    $form['salesforce_path']['sf_doc_link_webtocase'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Salesforce document to webcase API'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('sf_doc_link_webtocase'),
    ];
    $form['salesforce_path']['sf_sku_search_api'] = [
      '#type' => 'textarea',
      '#title' => $this->t('SKU search'),
      '#description' => $this->t('For autocomplete integration on the model number textbox'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('sf_sku_search_api'),
    ];
    $form['salesforce_path']['sf_mapping_fields'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Salesforce mapping fields'),
      '#description' => $this->t('Salesforce mapping fields like FirstName__c, LastName__c'),
      '#default_value' => $config->get('sf_mapping_fields'),
    ];
    $form['salesforce_path']['sf_images_keys'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Salesforce, machine name for multi images'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('sf_images_keys'),
    ];
    $form['salesforce_path']['notify_flag'] = [
      '#type' => 'select',
      '#title' => $this->t('Turn off general notification'),
      '#description' => $this->t('Stop notifications: Which stops sending mail when Application sending invalid data to salesforce, unable to create case in salesforce'),
      '#default_value' => $config->get('notify_flag'),
      '#options' => ['off' => 'Off', 'on' => 'On'],
    ];
    $form['salesforce_path']['notify_flag_adv'] = [
      '#type' => 'select',
      '#title' => $this->t('Turn off token generator'),
      '#description' => $this->t('System unable to generate token to connect salesforce'),
      '#default_value' => $config->get('notify_flag_adv'),
      '#options' => ['off' => 'Off', 'on' => 'On'],
    ];
    $form['salesforce_path']['notify_subj_token'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Email body reason 1'),
      '#description' => $this->t('Reason:- System unable to generate token to connect salesforce'),
      '#default_value' => $config->get('notify_subj_token'),
    ];
    $form['salesforce_path']['notify_case_fail_subj'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Email body reason 2'),
      '#description' => $this->t('Reason:- System unable to create case in salesforce'),
      '#default_value' => $config->get('notify_case_fail_subj'),
    ];
    $form['salesforce_path']['notify_subj'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Email body reason 3'),
      '#description' => $this->t('Reason:- Application receives invalid data format'),
      '#default_value' => $config->get('notify_subj'),
    ];
    $form['salesforce_path']['email_body'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Email body'),
      '#description' => $this->t('Use token and design email template. available tokens : Error received variable {error} , Holds JSON submitted data variable {data}, Current date {date}'),
      '#maxlength' => 255,
      '#rows' => 7,
      '#default_value' => $config->get('email_body'),
    ];
    $form['salesforce_path']['api_contact_us'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Contact Us API base path'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('api_contact_us'),
    ];
    $form['salesforce_path']['api_product_registration'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Product Registration API base path'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('api_product_registration'),
    ];
    $form['salesforce_path']['webform_min_date'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Date Minimum'),
      '#description' => $this->t('Specifies the minimum date.') . '<br /><br />' . $this->t('Accepts any date in any <a href="https://www.gnu.org/software/tar/manual/html_chapter/tar_7.html#Date-input-formats">GNU Date Input Format</a>. Strings such as today, -2 months or +2 months [ Example of few allowed values: -1 years, +1 day, +1 week ] and Dec 9 2004 are all valid.'. '<br />' . ' This date minimum fields accessed in the following places, webform product registration.' . '<br />' . 'Please keep this field as empty, if you dont want to set the minimum date.'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('webform_min_date'),
    ];
    $form['salesforce_path']['webform_max_date'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Date Maximum'),
      '#description' => $this->t('Specifies the maximum date.') . '<br /><br />' . $this->t('Accepts any date in any <a href="https://www.gnu.org/software/tar/manual/html_chapter/tar_7.html#Date-input-formats">GNU Date Input Format</a>. Strings such as today, -2 months or +2 months [ Example of few allowed values: -1 years, +1 day, +1 week ] and Dec 9 2004 are all valid.'. '<br />' . ' This date maximum fields accessed in  the following places, webform product registration.' . '<br />' . 'Please keep this field as empty, if you dont want to set the maximum date.'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('webform_max_date'),
    ];
    $form['captcha_disable'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Disable CAPTCHA field in all the forms.'),
      '#default_value' => $config->get('captcha_disable'),
    ];

    $form['encryption_keys'] = [
      '#type' => 'details',
      '#title' => 'Encryption Details',
    ];
    $form['encryption_keys']['df_encryption_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Encryption Key'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('df_encryption_key'),
    ];
    $form['encryption_keys']['df_encryption_method'] = [
      '#type' => 'select',
      '#title' => $this->t('Select element'),
      '#options' => [
        'AES-128-CBC' => 'AES-128-CBC',
        'AES-128-CFB' => 'AES-128-CFB',
        'AES-192-CBC' => 'AES-192-CBC',
        'AES-256-CFB8' => 'AES-256-CFB8',
      ],
      '#default_value' => $config->get('df_encryption_method'),
    ];
    $form['encryption_keys']['df_vector_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Vector Key'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('df_vector_key'),
    ];

    $form['global_api'] = [
      '#type' => 'details',
      '#title' => 'Global API Details',
    ];
     $form['global_api']['global_site_name'] = [
      '#type' => 'select',
      '#title' => $this->t('Site Name'),
      '#options' => [
        '' => '--Select--',
        'delta' => 'Delta',
        'peerless' => 'Peerless',
        'brizo' => 'Brizo',
        'firstwave' => 'Firstwave',
      ],
      '#description' =>  $this->t('Please select site name. For common functionalaity, this will be used in the common functions.'),
      '#default_value' => $config->get('global_site_name'),
    ];

    $form['global_api']['global_product_api_single'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Product API URI'),
      '#default_value' => $config->get('global_product_api_single'),
      '#size' => 200,
    ];
    $form['global_api']['foundational_attribute'] = [
      '#type' => 'select',
      '#title' => $this->t('Foundational Attribute'),
      '#options' => [
        '' => '--Select--',
        'WebsiteUS' => 'Delta - WebsiteUS',
        'WebsitePeerlessUS' => 'Peerless - WebsitePeerlessUS',
        'WebsiteBrizoUS' => 'Brizo - WebsiteBrizoUS',
        'WebsiteBrizoCA' => 'Brizo - WebsiteBrizoCA',
        'WebsiteFirstWave' => 'FirstWave - WebsiteFirstWave'
      ],
      '#description' =>  $this->t('Please select site foundation attribute, For ex: peerless should be  WebsitePeerlessUS.'),
      '#default_value' => $config->get('foundational_attribute'),
    ];

    $form['global_api']['order_ship_date'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Product Visibility Date Exclusion'),
      '#description' => $this->t('The Delta API has a future date value (availableToOrderDate or availabeToshipDate) which is used to hide products that are not yet available. (Example: 9999-12-31)'),
      '#default_value' =>  $config->get('order_ship_date') ?: '9999-12-31',
      '#size' => 200,
    ];
    $form['global_api']['marketing_cloud_collect'] = [
      '#type' => 'textfield',
      '#title' => $this->t("Marketing Cloud's Collect Tracking Code MID"),
      '#description' => $this->t("Marketing Cloud's Collect Tracking Code within Drupal pages and were Marketing Cloud
'Member ID'differs based on environment."),
      '#default_value' =>  $config->get('marketing_cloud_collect') ?: '000000000',
      '#size' => 20,
    ];
    $form['global_api']['marketing_cloud_collect_switch'] = [
      '#type' => 'select',
      '#title' => $this->t("Marketing Cloud's Collect Tracking Code Enable/Disable"),
      '#options' => [
        '0' => 'Disable',
        '1' => 'Enable',
      ],
      '#description' =>  $this->t("Marketing Cloud's Collect Tracking Code within Drupal pages"),
      '#default_value' => $config->get('marketing_cloud_collect_switch'),
    ];
    $form['pdp_api_details'] = array(
      '#type' => 'details',
      '#title' => $this->t('Product Details Page'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE
    );

    $form['pdp_api_details']['pdp_api_url'] = [
       '#type' => 'textfield',
       '#title' => $this->t('PDP API URL'),
       '#default_value' => $config->get('pdp_api_url'),
     ];

     $form['pdp_api_details']['pdp_related_product_api_url'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Related Product API URL'),
        '#default_value' => $config->get('pdp_related_product_api_url'),
        '#maxlength' => 255,
      ];

      $form['pdp_api_details']['bimsmith'] = [
        '#type' => 'textarea',
        '#title' => $this->t('Bimsmith API'),
        '#description' => $this->t('BIMsmith - This is external API, Which fetch document and display on the PDP, '.'<br />'.' For testing purpose, Find {bmSKU} replace with RP81438'),
        '#default_value' => $config->get('bimsmith'),
        '#maxlength' => 255,
      ];
    $form['global_api']['load_multiple_products'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Multiple Product Path'),
      '#default_value' => $config->get('load_multiple_products'),
      '#required' => TRUE,
    ];
    $form['global_api']['site_commerce'] = [
      '#type' => 'select',
      '#title' => $this->t('Site commerce type'),
      '#options' => [
        '' => '--Select--',
        'product' => 'Commerce Products',
        'node' => 'Node',
      ],
      '#description' =>  $this->t('Please select site commerce type, which will be used in getting product values.'),
      '#default_value' => $config->get('site_commerce'),
      '#required' => TRUE,
    ];
    $form['global_api']['auto_filename'] = [
      '#type' => 'select',
      '#title' => $this->t('Image upload name auto complete'),
      '#options' => [
        '0' => 'Disable',
        '1' => 'Enable',
      ],
      '#description' =>  $this->t('While image upload, Image name fill on the name field automatically'),
      '#default_value' => $config->get('auto_filename'),
      '#required' => TRUE,
    ];
    $form['product_cache_update'] = [
      '#type' => 'details',
      '#title' => $this->t('Product Cache Update'),
      '#open' => FALSE,
      '#weight' => 3,
    ];
    $form['product_cache_update']['product_cache_update_cron_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Product cache cron key'),
      '#default_value' => $config->get('product_cache_update_cron_key'),
      '#size' => 64,
    ];
    $lastCron = '';
    if ($config->get('cache_update_last_cron')) {
      $mil = $config->get('cache_update_last_cron');
      $seconds = $mil / 1000;
      $lastCron = date("d-m-Y H:i:s", $seconds);
    }
    $form['product_cache_update']['cache_update_last_cron'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Product cache update last cron time'),
      '#description' => 'Last cron run date time is '.$lastCron,
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('cache_update_last_cron'),
    ];
    $form['product_cache_update']['cache_product_update_api'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Updated product feed API'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('cache_product_update_api'),
    ];
    $form['product_cache_update']['cache_product_iteration'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Updated product Iteration'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('cache_product_iteration'),
    ];
    $form['product_cache_update']['plp_cache_pages'] = [
      '#type' => 'textfield',
      '#title' => $this->t('PLP Cache pages'),
      '#maxlength' => 25,
      '#size' => 25,
      '#default_value' => $config->get('plp_cache_pages'),
    ];
    $form['sfcc_homepage'] = [
      '#type' => 'details',
      '#title' => $this->t('SFCC Home Page'),
      '#open' => FALSE,
      '#weight' => 3,
    ];
    $form['sfcc_homepage']['disable_date_folder_image'] = [
      '#type' => 'select',
      '#title' => $this->t('Turn on / off date folder image for SFCC home page'),
      '#default_value' => $config->get('disable_date_folder_image'),
      '#options' => ['off' => 'Off', 'on' => 'On'],
      '#description' => $this->t("Note:<br /> 1. Please clear cache if required."),
    ];
    $form['sfcc_homepage']['sfcc_skip_media_type'] = [
      '#type' => 'textarea',
      '#size' => 25,
      '#title' => $this->t('Please enter media type, which you created for SFFCC Home page.'),
      '#default_value' => $config->get('sfcc_skip_media_type'),
      '#description' => $this->t("Note:<br />1. This media selection limitation applicable only for the SFCC Home Page [Machine name: sfcc_home_page] <br/> 2. All media type will be disabled except above mentioned list. <br /> 3. Enter multiple media type seprate by comma ex:sfcc_home_image, sfcc_home_svg"),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Can add validation if required.
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('delta_services.deltaservicesconfig')
      ->set('api_base_path', $form_state->getValue('api_base_path'))
      ->set('delta_api_username', $form_state->getValue('delta_api_username'))
      ->set('df_environment', $form_state->getValue('df_environment'))      
       ->set('sf_login_url', $form_state->getValue('sf_login_url'))
      ->set('sf_client_id', $form_state->getValue('sf_client_id'))
      ->set('sf_client_secret', $form_state->getValue('sf_client_secret'))
      ->set('sf_username', $form_state->getValue('sf_username'))
      ->set('notify_to', $form_state->getValue('notify_to'))
       ->set('sf_login_url_prod', $form_state->getValue('sf_login_url_prod'))
      ->set('sf_client_id_prod', $form_state->getValue('sf_client_id_prod'))
      ->set('sf_client_secret_prod', $form_state->getValue('sf_client_secret_prod'))
      ->set('sf_username_prod', $form_state->getValue('sf_username_prod'))
      ->set('notify_to_prod', $form_state->getValue('notify_to_prod'))
       ->set('sf_login_url_dev', $form_state->getValue('sf_login_url_dev'))
      ->set('sf_client_id_dev', $form_state->getValue('sf_client_id_dev'))
      ->set('sf_client_secret_dev', $form_state->getValue('sf_client_secret_dev'))
      ->set('sf_username_dev', $form_state->getValue('sf_username_dev'))
      ->set('notify_to_dev', $form_state->getValue('notify_to_dev'))                    
      ->set('sf_sku_search_api', $form_state->getValue('sf_sku_search_api'))
      ->set('sf_create_case', $form_state->getValue('sf_create_case'))
      ->set('sf_create_cont_ver', $form_state->getValue('sf_create_cont_ver'))
      ->set('sf_doc_link', $form_state->getValue('sf_doc_link'))
      ->set('sf_doc_link_webtocase', $form_state->getValue('sf_doc_link_webtocase'))
      ->set('sf_mapping_fields', $form_state->getValue('sf_mapping_fields'))
      ->set('sf_images_keys', $form_state->getValue('sf_images_keys'))
      ->set('api_contact_us', $form_state->getValue('api_contact_us'))
      ->set('notify_flag', $form_state->getValue('notify_flag'))
      ->set('notify_flag_adv', $form_state->getValue('notify_flag_adv'))
      ->set('notify_subj', $form_state->getValue('notify_subj'))
      ->set('notify_subj_token', $form_state->getValue('notify_subj_token'))
      ->set('notify_case_fail_subj', $form_state->getValue('notify_case_fail_subj'))
      ->set('email_body', $form_state->getValue('email_body'))
      ->set('api_product_registration', $form_state->getValue('api_product_registration'))
      ->set('bimsmith', $form_state->getValue('bimsmith'))
      ->set('webform_min_date', $form_state->getValue('webform_min_date'))
      ->set('webform_max_date', $form_state->getValue('webform_max_date'))
      ->set('captcha_disable', $form_state->getValue('captcha_disable'))
      ->set('df_encryption_key', $form_state->getValue('df_encryption_key'))
      ->set('df_encryption_method', $form_state->getValue('df_encryption_method'))
      ->set('df_vector_key', $form_state->getValue('df_vector_key'))
      ->set('global_site_name', $form_state->getValue('global_site_name'))
      ->set('global_product_api_single', $form_state->getValue('global_product_api_single'))
      ->set('order_ship_date', $form_state->getValue('order_ship_date'))
      ->set('marketing_cloud_collect', $form_state->getValue('marketing_cloud_collect'))
      ->set('marketing_cloud_collect_switch', $form_state->getValue('marketing_cloud_collect_switch'))
      ->set('foundational_attribute', $form_state->getValue('foundational_attribute'))
      ->set('pdp_api_url', $form_state->getValue('pdp_api_url'))
      ->set('pdp_related_product_api_url', $form_state->getValue('pdp_related_product_api_url'))
      ->set('pdp_sync_api_url', $form_state->getValue('pdp_sync_api_url'))
      ->set('load_multiple_products', $form_state->getValue('load_multiple_products'))
      ->set('site_commerce', $form_state->getValue('site_commerce'))
      ->set('auto_filename', $form_state->getValue('auto_filename'))
      ->set('product_cache_update_cron_key', $form_state->getValue('product_cache_update_cron_key'))
      ->set('cache_update_last_cron', $form_state->getValue('cache_update_last_cron'))
      ->set('cache_product_update_api', $form_state->getValue('cache_product_update_api'))
      ->set('cache_product_iteration', $form_state->getValue('cache_product_iteration'))
      ->set('plp_cache_pages', $form_state->getValue('plp_cache_pages'))
      ->set('disable_date_folder_image', $form_state->getValue('disable_date_folder_image'))
      ->set('sfcc_skip_media_type', $form_state->getValue('sfcc_skip_media_type'))
      ->save();
    if (strlen(trim($form_state->getValue('delta_api_password'))) > 0) {
      $this->config('delta_services.deltaservicesconfig')
        ->set('delta_api_password', $form_state->getValue('delta_api_password'))
        ->save();
    }
    if (strlen(trim($form_state->getValue('sf_password'))) > 0) {
      $this->config('delta_services.deltaservicesconfig')
        ->set('sf_password', $form_state->getValue('sf_password'))
        ->save();
    }
    if (strlen(trim($form_state->getValue('sf_password_prod'))) > 0) {
      $this->config('delta_services.deltaservicesconfig')
        ->set('sf_password_prod', $form_state->getValue('sf_password_prod'))
        ->save();
    }
    if (strlen(trim($form_state->getValue('sf_password_dev'))) > 0) {
      $this->config('delta_services.deltaservicesconfig')
        ->set('sf_password_dev', $form_state->getValue('sf_password_dev'))
        ->save();
    }        
  }

}

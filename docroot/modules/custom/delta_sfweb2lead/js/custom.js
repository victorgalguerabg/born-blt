(function($, Drupal) {
  Drupal.behaviors.delta_sfweb2lead = {
    attach: function(context, settings) {
      var xhr;

      $(window).on('load',function() {
        var removebutton = $('.webform-multiple-table .parent_product_sku_purchase_date_table_remove .image-button');
        if(removebutton.length === 1) {
          $('.webform-multiple-table .parent_product_sku_purchase_date_table_remove .image-button').addClass('removecrossbutton');
        }
        var removebutton = $('.webform-multiple-table .parent_product_upc_sku_date_table_remove');
        if(removebutton.length === 1) {
          $('.webform-multiple-table .parent_product_upc_sku_date_table_remove').addClass('removecrossbutton');
        }

        var addbutton = $('.webform-multiple-table .webform-multiple-add');
        if(removebutton.length === 1) {
          $('.webform-multiple-table .webform-multiple-add').addClass('addbutton');
        }
      });
      //Add wrappers to add or remove button
      if($("#product_upc_sku_date_table input[type=image], #product_sku_purchase_date_table input[type=image]").length){
        $("#product_upc_sku_date_table input[type=image], #product_sku_purchase_date_table input[type=image]").once('#product_sku_purchase_date_table, #product_upc_sku_date_table').each(function(e){
          if($(this).attr('type') === 'image'){
            var getImgBtnName = $(this).attr('name');
            var getImgBtnName = getImgBtnName.slice(0,-2);
            $(this).addClass(getImgBtnName).wrap( "<div></div>" ).parent().addClass('parent_'+getImgBtnName);
            var getElemAlt = $(this).attr('alt');
            $(this).attr('type','button').attr('value',getElemAlt).removeAttr('src').addClass('button button--primary');
          }
        });
      }


      //For multiple field sku auto complete.
      $('.ac-model-number input[type=text]').each(function(e) {
        $('.ac-model-number input[type=text]').addClass('form-item__textfield form-autocomplete ui-autocomplete-input');
        $(this).on('input', function(event) {
          var getElemParentID = this.id;
          if($("div#"+getElemParentID).length == 0 ){
            $(this).parent().parent().attr('id',getElemParentID);
            $(this).parent().prev().attr('for',getElemParentID);
          }
          var target = $(event.target);
          var wrapperID = target.attr("id");
          //This is for autocomplete suggest container
          var autoSuggestSelector = 'div#' + wrapperID + ' .auto-suggest';
          if ($(autoSuggestSelector).length) {
            $(autoSuggestSelector).remove();
          }
          var model_no = $('input#' + wrapperID).val();
          $(".ac-model-number").append("<div class='auto-suggest'></div>");
          $(autoSuggestSelector).hide();
          // SKU Search
          search_sku(xhr, model_no, autoSuggestSelector, '#'+wrapperID);
          //Removing the dropdown on clicking outside of the form
          $(document).click(function() {
            $(autoSuggestSelector).hide();
          });
          //Appending the value of the li to the input value
          $(autoSuggestSelector).on('click', '.row-sku', function() {
            var selectedSku = $(this).html();
            $('input#' + wrapperID).val(selectedSku);
            $(autoSuggestSelector).hide();
          });
           event.stopImmediatePropagation();
        });
      });
      //For single field sku auto complete.
      $('.ac-model-number-single input[type=text]').on('keyup', function() {
        if ($(".auto-suggest").length) {
          $(".auto-suggest").remove();
        }
        var model_no = $(".ac-model-number-single input[type=text]").val();
        $(".ac-model-number-single").append("<div class='auto-suggest'></div>");
        $(".auto-suggest").hide();
        // SKU Search
        search_sku(xhr, model_no, '.auto-suggest', '.ac-model-number-single');
        //Removing the dropdown on clicking outside of the form
        $(document).click(function() {
          $(".auto-suggest").hide();
        });
      });

      //Appending the value of the li to the input value
      $('.ac-model-number-single').on('click', '.auto-suggest .row-sku', function() {
        var selectedSku = $(this).html();
        $('.ac-model-number-single input[type=text]').val(selectedSku);
      });

      //AJAX Call for sku search
      var search_sku = function(xhr, model_no, autoSuggestSelector, wrapperID) {
        if (xhr) xhr.abort();
        xhr = $.ajax({
          url: '/sku-autocomplete',
          type: "GET",
          data: {
            q: model_no,
          },
          contentType: "application/json; charset=utf-8",
          dataType: "json",
          success: function(skuResponse) {
            $(autoSuggestSelector).show();
            var eachRow = '';
            var entireResult = '';
            $.each(skuResponse, function(key, value) {
              eachRow = eachRow + '<li class="row-sku">' + value.label + '</li>';
            });
            entireResult = '<ul class="model-num-list">' + eachRow + '</ul>';
            $(autoSuggestSelector).html(entireResult);
            if ($('input' + wrapperID).val() === "") {
              $(autoSuggestSelector).remove();
            }
          },
        });
      }
    }
  }
})(jQuery, Drupal);

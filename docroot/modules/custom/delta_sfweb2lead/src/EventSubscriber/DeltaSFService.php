<?php
namespace Drupal\delta_sfweb2lead\EventSubscriber;

use Drupal\sfweb2lead_webform\Event\Sfweb2leadWebformEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\delta_services\DeltaService;
use Drupal\file\Entity\File;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Url;

/**
 * Class DeltaSFService
 *
 */
class DeltaSFService implements EventSubscriberInterface
{
  //mailer
  protected $mail_manager;
  protected $language_manager;

  /**
   * Drupal\Core\Logger\LoggerChannelFactoryInterface definition.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */

  protected $loggerChannelFactory;


  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  protected $deltaservice;

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  public function __construct(DeltaService $deltaservice, ConfigFactoryInterface $config_factory, LoggerChannelFactoryInterface $loggerChannelFactory, EntityTypeManagerInterface $entity_type_manager, MailManagerInterface $mail_manager, LanguageManagerInterface $language_manager,MessengerInterface $messenger){
    $this->deltaservice = $deltaservice;
    $this->configFactory = $config_factory;
    $this->loggerChannelFactory = $loggerChannelFactory;
    $this->entityTypeManager = $entity_type_manager;
    $this->mailManager = $mail_manager;
    $this->languageManager = $language_manager;
    $this->messenger = $messenger;
   }

  public static function getSubscribedEvents()
  {
    $events[Sfweb2leadWebformEvent::SUBMIT][] = ['doHookAlter', 800];
    return $events;
  }

  public function doHookAlter(Sfweb2leadWebformEvent $event)
  {
    $config = $this->configFactory->get('delta_services.deltaservicesconfig');
    $imageMachineNameList = $config->get('sf_images_keys');
    $multiImagesArr = [];
    if(!empty($imageMachineNameList)){
      $imageMachineNameList = str_replace(' ', '', $imageMachineNameList);
      $multiImagesArr = explode(',', $imageMachineNameList);
    }
    $data = $event->getData();

    $source = (isset($data['delta_sfweb2lead_source'])) ? $data['delta_sfweb2lead_source'] : '';
       //To concatinate multiple field values save in helpdescrption sf field
    // in config need add index|value HelpDescription__1 | HelpDescription
      //test data
      //$data['HelpDescription__1'] = ['a','b'];
      //$data['HelpDescription__1'] = 'a';
    if(array_key_exists('HelpDescription__c', $data)){
     for($i = 1; $i <= 10; $i++){
      $constructIndex = "HelpDescription__$i";
      if(array_key_exists($constructIndex, $data)){
        // if checkbox with multi select means then it will array.
        if(is_array($data[$constructIndex]) && (count($data[$constructIndex]))){
          foreach ($data[$constructIndex] as $getChildKey => $getChildVal) {
            array_unshift($data['HelpDescription__c'] , $getChildVal);
          }
        }else{
          // if radio or dropdown  means  it will be value.
          array_unshift($data['HelpDescription__c'], $data[$constructIndex]);
        }
      }
     }
   }

    //newsletter subscription
    $this->subscription($data, 'Subscriber_Status__c');

    $modelNumberMachineKey = 'product_model_number';
    $purchaseDateMachineKey = 'product_purchase_date';
    $productImageMachineKey = 'product_image_upload';
    foreach ($multiImagesArr as $prod_img_fid_key => $prod_img_fid){
      if(!empty($data[$prod_img_fid])){
        $data[$productImageMachineKey][] = $data[$prod_img_fid];
        unset($data[$prod_img_fid]);
      }
    }

    //Lets reset all key except salesforce API Keys
    $webFormData = $data;
    if (count($webFormData))
    {
      foreach ($webFormData as $key => $value)
      {
        if (substr($key, -3) != '__c')
        {
          unset($webFormData[$key]);
        }
      }
    }
    // webFormData contains sf required fields and its data
    // data contains sf + extra fields like oi,delta_sfweb2lead_source values.
    // so While create case, we need to use webFormData only.

    $gettokenArr = [];
    $gettokenArr = $this->generateAccessToken();

    if(empty($gettokenArr)){
      $source = $data['RecordType__c'];
      if(empty($source)){
        $source = 'Contact_Us';
      }
      $source = str_replace('_', ' ',$source);
      $source = strtolower($source);
      $notify_flag = $config->get('notify_flag_adv');
      $subject = "Failed $source submission from ";
      $reason = $config->get('notify_subj_token');
      $body = [
        'data' => $data,
        'response' => json_encode($gettokenArr),
        'reason' => $reason
      ];
      //Send notification
      $this->sendMail($body, $subject, $notify_flag);
      $this->loggerChannelFactory->get('delta_salesforce')->warning($reason);
      return false;
    }
    if (is_array($gettokenArr) && array_key_exists('accessToken', $gettokenArr))
    {
      $tokenType = $gettokenArr['tokenType'];
      $accessToken = $gettokenArr['accessToken'];
      $instanceUrl = $gettokenArr['instanceUrl'];
      $header = ['Authorization' => $tokenType . ' ' . $accessToken, 'content-type' => 'application/json', ];
    }

    // For single field, product sku and date selection.
    if($source == 'contact_us'){
      $data['sku_date'][] = [
        $modelNumberMachineKey => isset($data['ModelNo__c']) ? $data['ModelNo__c'] : '',
        $purchaseDateMachineKey => isset($data['PurchaseDate__c']) ? $data['PurchaseDate__c'] : '',
      ];
    }
    // Flag - When data does not have sku_date.
    $createCaseFlag = 0;
    // For multi field, product sku and date selection.
    if(is_array($data) &&  array_key_exists('sku_upc_date', $data) && count($data['sku_upc_date'])){
      foreach ($data['sku_upc_date'] as $key => $value) {
        //Data having sku_date keys
        $createCaseFlag = 1;
        if(array_key_exists('upc_number', $value) && count($value['upc_number'])){
          $webFormData['UPC__c'] = $value['upc_number'];
        }
        if(array_key_exists('store_name', $value) && count($value['store_name'])){
          $webFormData['PurchasedFrom__c'] = $value['store_name'];
        }
        $product_model_number = $value[$modelNumberMachineKey];
        $webFormData['ModelNo__c'] = $product_model_number;
        if(array_key_exists($purchaseDateMachineKey, $value) && count($value[$purchaseDateMachineKey])){
          $purchaseDate = $value[$purchaseDateMachineKey];
          $purchaseDate = date("Y-m-d", strtotime($purchaseDate));
          $webFormData['PurchaseDate__c'] = $purchaseDate;
        }

        $createCaseResp = $this->createCase($instanceUrl, $header, $webFormData);
        //Case creation
        $case_id = "";
        if (is_array($createCaseResp) && count($createCaseResp))
        {
          $case_id = $createCaseResp[0];
          $this->loggerChannelFactory->get('sf_case_submission')->warning('Case created ID ' . $case_id);
        }
        //Attachment creation
        if(!empty($data[$productImageMachineKey])){
          $uploadedStatus = $this->sfUploadImage($instanceUrl, $header, $data, $case_id);
        }
      }
    }
    // For multi field, product sku and date selection.
    if(is_array($data) &&  array_key_exists('ModelNo__c', $data) && count($data['ModelNo__c'])){
      foreach ($data['ModelNo__c'] as $key => $value) {
        //Data having sku_date keys
        $createCaseFlag = 1;
        $webFormData['ModelNo__c'] = $value;
        $purchaseDate = $data['PurchaseDate__c'][$key];
	//Stripping time and using only date.
        $purchaseDate = date("Y-m-d", strtotime($purchaseDate));
        $webFormData['PurchaseDate__c'] = $purchaseDate;
        $createCaseResp = $this->createCase($instanceUrl, $header, $webFormData);
        //Case creation
        $case_id = "";
        if (is_array($createCaseResp) && count($createCaseResp))
        {
          $case_id = $createCaseResp[0];
          $this->loggerChannelFactory->get('sf_case_submission')->warning('Case created ID ' . $case_id);
        }
        //Attachment creation
        if(!empty($data[$productImageMachineKey])){
          $uploadedStatus = $this->sfUploadImage($instanceUrl, $header, $data, $case_id);
        }
      }
    }
    // when data does not have sku then
    // below will create the case.
    if($createCaseFlag == 0){
      //This will concatinate multi select checkbox values.
      $sfKeys = $this->getSFConfiguredKeys();
      if(count($sfKeys)){
        foreach ($sfKeys as $key => $value) {
          $webFormData = $this->readConcatData($webFormData,$key);
        }
      }
      $createCaseResp = $this->createCase($instanceUrl, $header, $webFormData);
      //Case creation
      $case_id = "";
      if (is_array($createCaseResp) && count($createCaseResp))
        {
          $case_id = $createCaseResp[0];
          $this->loggerChannelFactory->get('sf_case_submission')->warning('Case created ID ' . $case_id);
      	  if(!empty($data[$productImageMachineKey])) {
            $uploadedStatus = $this->sfUploadImage($instanceUrl, $header, $data, $case_id);
          }
        }else{
          $this->loggerChannelFactory->get('sf_create_case_Failed')->warning('Unable to create case in salesforce');
        }
    }
  }


  /**
  * Subscriber Status update in SF
  */
  public function subscription(&$data,$sfKey){
    if(is_array($data) && array_key_exists($sfKey, $data) && count($data[$sfKey]) > 0 ){
      if(is_array($data[$sfKey])){
        $data[$sfKey] = current($data[$sfKey]);
      }else {
        $data[$sfKey] = !empty($data[$sfKey]) ? $data[$sfKey] : '';
      }
    }else{
      unset($data[$sfKey]);
    }
  }
  /**
  * concat data
  */
  public function readConcatData($data, $arrKey){
    if(array_key_exists($arrKey, $data) && is_array($data[$arrKey]) && count($data[$arrKey])){
      $concatData = '';
      foreach ($data[$arrKey] as $key => $value) {
        $concatData =  $concatData . $value . ';';
      }
      $data[$arrKey] = rtrim($concatData,';');
    }
    return $data;
  }

  /*
  * Get configured keys
  */
  public function getSFConfiguredKeys(){
    $sfFieldsList = $this->configFactory->get('delta_services.deltaservicesconfig')->get('sf_mapping_fields');
    $sfKeys = [];
    $getSFFields = explode("\n", $sfFieldsList);
    if(is_array($getSFFields) && count($getSFFields)){
      foreach ($getSFFields as  $getSFFieldDelimiter) {
        if(!empty($getSFFieldDelimiter)){
          list($sf_key, $sf_title) = explode('|', $getSFFieldDelimiter);
          $sfKeys[$sf_key] = $sf_title;
        }
      }
    }
    return $sfKeys;
  }

  /**
   * Create case
   */
  public function createCase($instanceUrl, $header, $data)
  {
    $id = [];
    $config = $this->configFactory->get('delta_services.deltaservicesconfig');
    $createCaseAPI = $config->get('sf_create_case');
    $contactusAPI = $instanceUrl . $createCaseAPI;
    $createCaseResp = $this->deltaservice->apiSfCall($contactusAPI, "POST", $header, $data, "Create case");
    if ((!empty($createCaseResp['response']) && ($createCaseResp['response'] != 'null')) && $createCaseResp['status'] == 'SUCCESS')
    {
      $responseJson = $createCaseResp['response'];
      $responseArr = json_decode($responseJson, true);
      if(isset($responseArr['id'])){
        $id = $responseArr['id'];
        return [$id];
      }else{
        $notify_flag = $config->get('notify_flag');
        $source = $data['RecordType__c'];
        if(empty($source)){
          $source = 'Contact_Us';
        }
        $source = str_replace('_', ' ',$source);
        $source = strtolower($source);
        $notify_flag = $config->get('notify_flag_adv');
        $subject = "Failed $source submission from ";
        $reason = $config->get('notify_subj');
        $body = [
          'data' => $data,
          'response' => json_encode($responseArr),
          'reason' => $reason
        ];
        //Send notification
        $this->sendMail($body, $subject, $notify_flag);
        $this->loggerChannelFactory->get('sf_invalid_data_passed')
          ->notice(json_encode($responseArr));
        return [];
      }
    }
    else
    {
      $notify_flag = $config->get('notify_flag');
      $reason = $config->get('notify_case_fail_subj');
      $source = $data['RecordType__c'];
      if(empty($source)){
        $source = 'Contact_Us';
      }
      $source = str_replace('_', ' ',$source);
      $source = strtolower($source);
      $notify_flag = $config->get('notify_flag_adv');
      $subject = "Failed $source submission from ";
      $body = [
        'data' => $data,
        'response' => json_encode($createCaseResp),
        'reason' => $reason
      ];
      //Send notification
      $this->sendMail($body, $subject, $notify_flag);
      $this->loggerChannelFactory->get('sf_submited_err_case')
          ->notice(json_encode($data));
      $this->loggerChannelFactory->get('sf_submited_err_resp')
          ->notice(json_encode($createCaseResp));
    }
    return [];
  }
  /**
   * Generate Access token.
   */

  public function generateAccessToken()
  {

    $config = $this->configFactory->get('delta_services.deltaservicesconfig');
    $sf_login_env = $config->get('df_environment');
    $loginURL = $config->get('sf_login_url'.$sf_login_env);
    $clientID = $config->get('sf_client_id'.$sf_login_env);
    $clientSecret = $config->get('sf_client_secret'.$sf_login_env);
    $sfUsername = $config->get('sf_username'.$sf_login_env);
    $sfPassword = $config->get('sf_password'.$sf_login_env);
    $acUrl = $loginURL . $clientID . '&client_secret=' . $clientSecret . '&username='. $sfUsername .'&password='. $sfPassword;
    // Step 1 - Authentication between salesforce and drupal application.
    $getAccessToken = $this->deltaservice->apiCall($acUrl, "POST", [], "", "SF Access Token");
    $tokenType = 'Bearer';
    $accessToken = '';
    $instanceUrl = '';
    $gettokenArr = [];
    if ((!empty($getAccessToken['response']) && ($getAccessToken['response'] != 'null')) && $getAccessToken['status'] == 'SUCCESS')
    {
      $accessTokenArr = json_decode($getAccessToken['response'], true);
      if (array_key_exists('token_type', $accessTokenArr))
      {
        $gettokenArr['tokenType'] = $accessTokenArr['token_type'];
      }
      if (array_key_exists('access_token', $accessTokenArr))
      {
        $gettokenArr['accessToken'] = $accessTokenArr['access_token'];
      }
      if (array_key_exists('instance_url', $accessTokenArr))
      {
        $gettokenArr['instanceUrl'] = $accessTokenArr['instance_url'];
      }
    }
    else
    {
      //System fail to generate token
      //system will return false as response.
      return [];
    }
    return $gettokenArr;
  }

  public function image_base64_encode($data)
  {
    $contentArr = [];
    if (is_array($data))
    {
      foreach ($data as $key => $fid)
      {
        $base64Resp = $this->generateBase64($fid);
        if (!empty($base64Resp))
        {
          $absolute_path = $base64Resp['filePath'];
          $base64data = $base64Resp['base64'];
          $getFileName = basename($absolute_path);
          $contentArr[] = ['Title' => $getFileName, 'PathOnClient' => $getFileName, 'VersionData' => $base64data];
        }
      }
      return $contentArr;
    }
    else
    {
      $fid = $data;
      $base64Resp = $this->generateBase64($fid);
      if (!empty($base64Resp))
      {
        $absolute_path = $base64Resp['filePath'];
        $base64data = $base64Resp['base64'];
        $getFileName = basename($absolute_path);
        $contentArr[] = ['Title' => $getFileName, 'PathOnClient' => $getFileName, 'VersionData' => $base64data];
        return $contentArr;
      }
    }
  }

  // Lets upload image into sf.
  public function sfUploadImage($instanceUrl, $header, $data, $id)
  {
    $config = $this->configFactory->get('delta_services.deltaservicesconfig');
    $createContentVersAPI = $config->get('sf_create_cont_ver');
    $createDocLinkAPI = $config->get('sf_doc_link');
    $createwebtocaseAPI = $config->get('sf_doc_link_webtocase');
    $base64 = '';
    $productImageMachineKey = 'product_image_upload';
    $getAllImagesArr = $this->image_base64_encode($data[$productImageMachineKey]);
    if (count($getAllImagesArr))
    {
      foreach ($getAllImagesArr as $key => $contentArr)
      {
        // Step 3: Attachment Create a content version
        $contentAPI = $instanceUrl . $createContentVersAPI;
        $createContentImgResp = $this->deltaservice->apiSfCall($contentAPI, "POST", $header, $contentArr, "Create content version");
        if ((!empty($createContentImgResp['response']) && ($createContentImgResp['response'] != 'null')) && $createContentImgResp['status'] == 'SUCCESS')
        {
          $getCntCreatedArr = json_decode($createContentImgResp['response'], true);
          $cntVersionID = $getCntCreatedArr['id'];
          if (!empty($cntVersionID))
          {
            $sfQuery = 'SELECT ContentDocumentId FROM ContentVersion WHERE Id=\'' . $cntVersionID . '\'';
            $docLinkAPI = $instanceUrl . $createDocLinkAPI . $sfQuery;
            $getDocumentLink = $this->deltaservice->apiSfCall($docLinkAPI, "GET", $header, [], "Create document Link SF");
            if ((!empty($getDocumentLink['response']) && ($getDocumentLink['response'] != 'null')) && $getDocumentLink['status'] == 'SUCCESS')
            {
              $getDocumentLinkRes = $getDocumentLink['response'];
              $ContentDocumentRes = json_decode($getDocumentLinkRes, true);
              $ContentDocumentId = '';
              if (array_key_exists('records', $ContentDocumentRes) && count($ContentDocumentRes['records']))
              {
                $ContentDocumentId = $ContentDocumentRes['records'][0]['ContentDocumentId'];
              }

              if (!empty($ContentDocumentId))
              {
                $documentLinkApi = $instanceUrl . $createwebtocaseAPI;
                $docCreateCaseArr = ['ContentDocumentId' => $ContentDocumentId, 'LinkedEntityId' => $id, 'ShareType' => 'V'];
                $documentLinkCreateResp = $this->deltaservice->apiSfCall($documentLinkApi, "POST", $header, $docCreateCaseArr, "Create document link case SF");
                if ((!empty($documentLinkCreateResp['response']) && ($documentLinkCreateResp['response'] != 'null')) && $documentLinkCreateResp['status'] == 'SUCCESS')
                {
                   $this->loggerChannelFactory->get('ContentDocumentLink')->warning('created document link to web to case.');

                }
                else
                {
                   $this->loggerChannelFactory->get('ContentDocumentLinkErr')->warning('Error - While creating document link to web to case.');
                }
              }
              else
              {
                 $this->loggerChannelFactory->get('ContentDocumentId')->warning('Error - Empty ContentDocumentId');
              }
            }
            else
            {
              $this->loggerChannelFactory->get('getDocumentLink')->warning('Error -  Get document link id');
            }
          }
        }
        else
        {
          $this->loggerChannelFactory->get('attachImageAsstErr')->warning('Error -  Create a content version');
        }

      }
    }
  }

  // Lets generate base64.

  public function generateBase64($fid)
  {
    try {
      $file = $this->entityTypeManager->getStorage('file')->load($fid);
    } catch (\Exception $error) {
      $this->loggerChannelFactory->get('delta_sfweb2lead')->alert(t('@err', ['@err' => $error->__toString()]));
      $this->messenger->addWarning(t('Unable to proceed. If the problem persists, please contact an administrator. Error code: CE3AEB8D-@line-@time', ['@line' => $error->getLine(), '@time' => time()]));
    }
    if(!empty($file)){
      try {
        //$absolute_path = $file->toUrl();
        if ($file->hasLinkTemplate('canonical')) {
          $absolute_path = $file->toUrl()->setAbsolute(TRUE);
        }
        elseif ($file->getEntityTypeId() === 'file' && $file->access('download')) {
          $absolute_path = Url::fromUri(file_create_url($file->getFileUri()))->toString();
        }
      } catch (\Exception $error) {
        $this->loggerChannelFactory->get('delta_sfweb2lead')->alert(t('@err', ['@err' => $error->__toString()]));
        $this->messenger->addWarning(t('Unable to proceed. If the problem persists, please contact an administrator. Error code: CE3AEB8D-@line-@time', ['@line' => $error->getLine(), '@time' => time()]));
      }
      $image_type = image_type_to_mime_type(exif_imagetype($absolute_path));
      $image_file = file_get_contents($absolute_path);
      $base_64_image = base64_encode($image_file);
      $fileName = $absolute_path;
      return ['base64' => $base_64_image, 'filePath' => $absolute_path];
    }
    return [];
  }

  //Send notification to delta team
  public function sendMail($body, $subject, $sendFlag){
    $config = $this->configFactory->get('delta_services.deltaservicesconfig');
    $sf_login_env = $config->get('df_environment');
    //Turn off mail notification
    if($sendFlag == 'off'){
      return FALSE;
    }
    $to = $config->get('notify_to'.$sf_login_env);
    //dont send mail if TO email is empty.
    if (empty($to)) {
      return FALSE;
    }
    $submissionData = '';
    if(is_array($body) && array_key_exists('data', $body) && (!empty($body['data']))){
      $submissionData = json_encode($body['data']);
    }
    $responseData = '';
    if(is_array($body) && array_key_exists('response', $body) && (!empty($body['response']))){
      $responseData = $body['response'];
    }

    $whereOccured = '';
    if(is_array($body) && array_key_exists('reason', $body) && (!empty($body['reason']))){
      $whereOccured = $body['reason'];
    }
    $from = 'no-reply@deltafaucet.com';
    $body = $config->get('email_body');

    $currentDate = date("Y-m-d H:i");

    if(empty($body)){
      $body = 'System fail to create case in salesforce';
    }
    $body = strtr($body, ['{data}' => $submissionData, '{error}' => $responseData, '{brokenAt}' => $whereOccured, '{date}' => $currentDate]);
    $params = [
      'headers' => [
        'Content-Type' => 'text/html; charset=UTF-8;',
        'Content-Transfer-Encoding' => '8Bit',
      ],
      'id' => 'sf_notify',
      'from' => $from,
      'subject' => $subject,
      'langcode' => $this->languageManager->getCurrentLanguage()->getId(),
      // The body will be rendered in commerce_mail(), because that's what
      // MailManager expects. The correct theme and render context aren't
      // setup until then.
      'message' => $body,
    ];
    $send = true;
    $result = $this->mailManager->mail('delta_sfweb2lead', $params['id'], $to, $params['langcode'], $params, NULL, $send);
    //When email system fail to send email.
    if ($result['result'] != true) {
      $message = t('Application failed to create case in salesforce, Also application unable to send mail notification to @email.', array('@email' => $to));
      $this->loggerChannelFactory->get('sf-error-log')->critical($message);
      return;
    }
    return;
  }

}

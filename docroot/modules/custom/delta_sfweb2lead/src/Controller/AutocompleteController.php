<?php

namespace Drupal\delta_sfweb2lead\Controller;

/**
 * Implementation of autocomplete controller.
 */

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Component\Utility\Tags;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\delta_services\DeltaService;

/**
 * Defines a route controller for entity autocomplete form elements.
 */
class AutocompleteController extends ControllerBase {
  /**
   * Drupal\delta_services\DeltaService definition.
   *
   * @var \Drupal\delta_services\DeltaService
   */
  protected $deltaservice;

  /**
   * Class constructor.
   */
  public function __construct(DeltaService $deltaservice) {
    $this->deltaservice = $deltaservice;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
    // Load the service required to construct this class.
        $container->get('delta_services.service')
    );
  }

  /**
   * Handler for autocomplete request.
   */
  public function handleAutocomplete(Request $request) {
    $results = [];

    // Get the typed string from the URL, if it exists.
    if ($input = $request->query->get('q')) {
      $typed_string = Tags::explode($input);
      $typed_string = strtolower(array_pop($typed_string));
      $results = $this->getSkuList($typed_string);
    }
    return new JsonResponse($results);
  }

  /**
   * Mplementation of autocomplete from product api.
   */
  public function getSkuList($searchKey) {
    $results = [];
    $config = $this->config('delta_services.deltaservicesconfig');
    $base_url = $config->get('api_base_path');
    $autocompleteAPI = $config->get('sf_sku_search_api');
    $searchPartsUrl = $base_url . $autocompleteAPI;
    $url = str_replace("{searchKey}", $searchKey, $searchPartsUrl);
    $productRequest = $this->deltaservice->apiCall($url, "GET", [], "", "SKU Search");
    if ($productRequest['status'] == 'SUCCESS') {
      $response = $productRequest['response'];
      $responseProdData = json_decode($response, TRUE);
      $results = [];
      if(array_key_exists('content', $responseProdData)){
        foreach ($responseProdData['content'] as $readSku) {
        if(array_key_exists('name', $readSku)){
           $results[] = [
            'value' => $readSku['name'],
            'label' => $readSku['name'],
          ];
        }
       }
      }      
      return $results;
    }
    else {
      return ['Something went wrong, please try again later.'];
    }
  }

}

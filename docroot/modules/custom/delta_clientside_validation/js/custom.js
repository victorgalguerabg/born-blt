//Defining object

(function ($) {
  Drupal.behaviors.commonvalidationfunction = {};
  Drupal.behaviors.commonvalidationfunction.usernameregex = function () {
    return /^[a-zA-ZÀ-ÿ\u00f1\u00d1~`\s\\\\';\"\\.,]*$/i;
  };
  Drupal.behaviors.commonvalidationfunction.emailidregex = function () {
    return /^([a-zA-ZÀ-ÿ\u00f1\u00d10-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{1,})+$/;
  };
  Drupal.behaviors.commonvalidationfunction.phonenumberregex = function () {
    return /^(\+\d{1,2}\s?)?1?\-?\.?\s?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$/i;
  };

  Drupal.behaviors.commonvalidationfunction.addressreg = function () {
    return /^[a-zA-ZÀ-ÿ\u00f1\u00d10-9@~`\s!@#$%^&*()_\\\\';:\"\\/.,-]*$/i;
  };

  Drupal.behaviors.commonvalidationfunction.cityvalidation = function () {
    return /^[a-zA-ZÀ-ÿ\u00f1\u00d1\-._~,\s]+$/;
  };

  Drupal.behaviors.commonvalidationfunction.zipcodevalidation = function () {
    return /^(\d{5}(-\d{4})?|[A-Za-z]\d[A-Za-z] ?\s\d[A-Za-z]\d)$/;
  };

  Drupal.behaviors.commonvalidationfunction.modelnumber = function () {
    return /^((-{1,2}))?(?:[A-Za-z0-9](-{1,2})?)*$/i;

  };

  Drupal.behaviors.commonvalidationfunction.searchEmail = function (emailID, element) {
    //console.log(emailID)
    $.ajax({
      url: '/data-validation',
      type: "POST",
      data: {
        email: emailID,
      },
      dataType: "json",
      error: function(jqXHR, textStatus, errorThrown){
        console.log(jqXHR);
        console.log(textStatus);
        console.log(errorThrown);
        console.log('Application failed to connect the api system');
        return 'api-error';
      },
      success: function(dataResponse) {
        //console.log(dataResponse);
        
        if(jQuery.type(dataResponse) === 'array' && dataResponse.length > 0){
          var currentURL = window.location.host;
          if(currentURL.indexOf("peerless") > 0){
            //console.log(currentURL);
            $.each(dataResponse, function(key, label) {
              element.next('p.form-error').remove();
              element.after("<p class='form-error'> " + label.msg + "</p>");
              element.addClass("error");
            });
          } else{
            $.each(dataResponse, function(key, label) {
              element.next('p.form-item--error-message').remove();
              element.after("<p class='form-item--error-message'> " + label.msg + "</p>");
              element.parent().addClass("form-item--error");
            });
          }
                   
        } 
      },
    });

  };
  //For debug purpose.
  //Drupal.behaviors.commonvalidationfunction.searchEmail("alp#habet@mail.com","test");
})(jQuery);

<?php

namespace Drupal\delta_cache;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\delta_services\DeltaService;
use Drupal\views\Plugin\views\field\Boolean;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\path_alias\AliasManagerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Core\Url;

/**
 * Class DeltacacheService.
 */
class DeltaCacheService {
  /**
   * Drupal\Core\Logger\LoggerChannelFactoryInterface definition.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;
  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;
  /**
   * The cache backend service.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cacheBackend;

  /**
   * Drupal\delta_services\DeltaService definition.
   *
   * @var \Drupal\delta_services\DeltaService
   */
  protected $deltaservice;

  /**
   * To get path alias.
   *
   * @var service
   */
  public $coreService;

  /**
   * An alias manager to find the alias for the current system path.
   *
   * @var \Drupal\path_alias\AliasManagerInterface
   */
  protected $alias_manager;


  /**
   * The Cache Render.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cacheRender;

  /**
   * Symfony\Component\HttpFoundation\RequestStack definition.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Drupal\Core\Messenger\MessengerInterface definition.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new DeltaCacheService object.
   */
  public function __construct(LoggerChannelFactoryInterface $logger_factory,
                              ConfigFactoryInterface $config_factory,
                              CacheBackendInterface $cache_backend,
                              DeltaService $deltaservice,
                              ContainerInterface $coreService,
                              CacheBackendInterface $cache_render,
                              RequestStack $requestStack,
                              MessengerInterface $messenger,
                              EntityTypeManagerInterface $entity,
                              AliasManagerInterface $alias_manager) {
    $this->loggerFactory = $logger_factory;
    $this->configFactory = $config_factory;
    $this->cacheBackend = $cache_backend;
    $this->deltaservice = $deltaservice;
    $this->coreService = $coreService;
    $this->cacheRender = $cache_render;
    $this->requestStack = $requestStack;
    $this->messenger = $messenger;
    $this->entityTypeManager = $entity;
    $this->aliasManager = $alias_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('delta_cache.cache_service')
    );
  }
  /*
   * Function to get cache
   * @param string $key
   *   Name given to the cached data.
   *
   * @return data
   *   The cache value.
   */
  public function deltaGetCache($key){
    $cid = 'delta_cache:'. $key;
    $cache = $this->cacheBackend->get($cid);
    if (!$cache || (time() > $cache->expire)) {
      return FALSE;
    }
    else {
      $data = $cache->data;
      return $data;
    }

  }

  /*
   * Function to set cache
   * @param string $key
   *   Name given to the cached data.
   * @param string $data
   *   The cache value to be set.
   * @param string $expireTime
   *   The cache expire time to be set.
   */
  public function deltaSetCache($key, $data, $expireTime = ''){
    // Use the default expiry time from config if expiry time is not passed.
    if ($expireTime == '') {
      $config = $this->configFactory->get('delta_cache.deltageneralsettings');
      $expireTime = $config->get('cache_expiry_time');
    }
    // set the new cache.
    $cid = 'delta_cache:'. $key;
    $this->cacheBackend->set($cid, $data, time() + $expireTime);
  }

  /**
   * Method to delete cache set by cid.
   * @param $keyArray
   *  key value which is used to appropriate cid.
   */
  public function deltaDeleteCacheId($keyArray) {
    if(!empty($keyArray)) {
      $cids = [];
      foreach($keyArray as $key){
        $cids[] = 'delta_cache:'.$key;
      }
      foreach($cids as $cid){
        $this->cacheBackend->delete($cid);
      }
    }
  }
  /**
   * Method to delete cache set by cid.
   * @param $skuArray
   *  SKU value which is used to appropriate cid.
   */
  public function deltaDeleteCache($skuArray) {
    if(!empty($skuArray)) {
      $cids = [];
      foreach($skuArray as $sku){
        $cids[] = 'delta_cache:PDP_'.$sku;
        $cids[] = 'peer_pdp_'.$sku;
      }
      foreach($cids as $cid){
        $this->cacheBackend->delete($cid);
      }
    }
  }

  /**
   * Method to clear cache for given SKU / Path
   * @param null $skuArray
   * @param null $pathArray
   * @return Boolean
   */
  public function clearCache($skuArray=NULL, $pathArray=NULL) {
    if(!empty($pathArray)) {
      foreach($pathArray as $path) {
        $cacheTags = [];
        if ($path=='/' || $path=='/home-page') {
          $front_url = $this->configFactory->get('system.site')->get('page.front', 'node');
          $front_url = trim($front_url, '/');
          $front = explode('/', $front_url);
          if( $front[0]=='node' && ctype_digit($front[1]) ) {
            $front_nid = $front[1];
          }
          $cacheTags[] = "node:".$front_nid;
          $this->loggerFactory->get('Request')->notice('delta_cache: cache homepage node tags-@message', [
            '@message' => print_r($cacheTags, TRUE),
          ]);
          Cache::invalidateTags($cacheTags);
          $this->loggerFactory->get('Request')->notice('Homepage Purge completed');
        } else {
          $result = $this->aliasManager->getAliasByPath($path);
          $alias_path = $this->aliasManager->getPathByAlias($result);
          $params = Url::fromUri("internal:" . $alias_path)->getRouteParameters();
          $entity_type = key($params);
          try {
            $storage = $this->entityTypeManager->getStorage($entity_type);
            $node = $storage->load($params[$entity_type]);
          } catch(\Exception $error) {
            $this->loggerFactory->get('checkout_cache')->alert(t('@err', ['@err' => $error->__toString()]));
            $this->messenger->addWarning(t('Unable to proceed. Please contact an administrator. Error code: C9A00B67-@line-@time', ['@line' => __LINE__, '@time' => time()]));
          }
          if (!empty($node)) {
            if ($entity_type == 'commerce_product') {
              $cacheTags[] = "product:".$node->id();
              $cacheTags[] = "commerce_product:".$node->id();
            } else {
              $cacheTags[] = "node:".$node->id();
            }
            Cache::invalidateTags($cacheTags);
            $this->loggerFactory->get('Request')->notice('Path Purge completed');
          }
        }
      }
      $this->cacheRender->deleteMultiple($pathArray);
      $this->messenger->addStatus('Purge completed');
    } elseif(!empty($skuArray)) {
      $cacheTags = [];
      self::deltaDeleteCache($skuArray);
      $sync_config = $this->configFactory->get('delta_services.deltaservicesconfig');
      $commerceType = $sync_config->get('site_commerce');
      $site = $sync_config->get('global_site_name');
      if($commerceType == 'product') {
        $productIds = [];
        try {
          $storage = $this->entityTypeManager
            ->getStorage('commerce_product_variation');
        } catch(\Exception $error) {
          $this->loggerFactory->get('checkout_cache')->alert(t('@err', ['@err' => $error->__toString()]));
          $this->messenger->addWarning(t('Unable to proceed. Please contact an administrator. Error code: C9A00B67-@line-@time', ['@line' => __LINE__, '@time' => time()]));
        }
        $variations = $storage->loadByProperties(['sku' => $skuArray]);
        if(!empty($variations)) {
          foreach($variations as $variation) {
            $productIds[] = $variation->getProductId();
            if ($site == 'brizo') {
              $productId = $variation->getProductId();
              try {
                $variation->delete();
              } catch (\Exception $error) {
                $this->loggerFactory->get('checkout_cache')->alert(t('@err', ['@err' => $error->__toString()]));
                $this->messenger->addWarning(t('Unable to proceed. Please contact an administrator. Error code: C9A00B67-@line-@time', ['@line' => __LINE__, '@time' => time()]));
              }
              //cleanup product alias.
              $aliasRawPattern = '/product/' . $productId;
              $getProductAliasPath = $this->aliasManager->getAliasByPath($aliasRawPattern);
              if($aliasRawPattern !== $getProductAliasPath){
                try {
                  $pathStorage = $this->entityTypeManager->getStorage('path_alias');
                } catch (\Exception $error) {
                  $this->loggerFactory->get('checkout_cache')->alert(t('@err', ['@err' => $error->__toString()]));
                  $this->messenger->addWarning(t('Unable to proceed. Please contact an administrator. Error code: C9A00B67-@line-@time', ['@line' => __LINE__, '@time' => time()]));
                }
                $paths = $pathStorage->loadByProperties([
                  'alias' => $getProductAliasPath,
                ]);
                if (!empty($paths)) {
                  foreach ($paths as $key => $path) {
                    try {
                      $path->delete();
                    } catch (\Exception $error) {
                      $this->loggerFactory->get('checkout_cache')->alert(t('@err', ['@err' => $error->__toString()]));
                      $this->messenger->addWarning(t('Unable to proceed. Please contact an administrator. Error code: C9A00B67-@line-@time', ['@line' => __LINE__, '@time' => time()]));
                    }
                  }
                }
              }
              try {
                $storage = $this->entityTypeManager->getStorage('commerce_product');
                $storage->load($productId)->delete();
              } catch (\Exception $error) {
                $this->loggerFactory->get('checkout_cache')->alert(t('@err', ['@err' => $error->__toString()]));
                $this->messenger->addWarning(t('Unable to proceed. Please contact an administrator. Error code: C9A00B67-@line-@time', ['@line' => __LINE__, '@time' => time()]));
              }
            }
          }
        }
        if(!empty($productIds)) {
          $this->loggerFactory->get('Request')->notice('delta_cache: Product array-@message', [
            '@message' => print_r($productIds, TRUE),
          ]);
          $this->loggerFactory->get('Request')->notice('delta_cache: Updating product count-@message', [
            '@message' => print_r(count($productIds), TRUE),
          ]);
          foreach($productIds as $productId) {
            $pathArray[] = '/product/'.$productId;
            $cacheTags[] = "product:".$productId;
            $cacheTags[] = "commerce_product:".$productId;
          }
          $this->loggerFactory->get('Request')->notice('delta_cache: cache product tags-@message', [
            '@message' => print_r($cacheTags, TRUE),
          ]);
          $this->loggerFactory->get('Request')->notice('Purge completed');
        } else {
          $this->loggerFactory->get('Request')->notice('delta_cache: No matching products available in CMS');
        }
      }
      if($commerceType == 'node') {
        if (!empty($skuArray)) {
          foreach($skuArray as $sku) {
            $cids[] = 'peer_pdp_' . $sku;
            $node = $this->deltaservice->getProductNid($sku);
            if ($node) {
              $this->loggerFactory->get('Request')->notice('delta_cache: node id-@message', [
                '@message' => print_r($node->get('nid')->value,TRUE),
              ]);
              $cacheTags[] = "node:".$node->get('nid')->value;
            }
            $path = $this->deltaservice->getProductUrl($sku);
            if ($path) {
              $this->loggerFactory->get('Request')->notice('delta_cache: node path-@message', [
                '@message' => $path,
              ]);
              $pathArray[] = $path;
            }
          }
					if (!empty($cids)) {
						$this->cacheRender->deleteMultiple($cids);
					}
      	}
        $this->loggerFactory->get('Request')->notice('delta_cache: cache node tags-@message', [
          '@message' => print_r($cacheTags, TRUE),
        ]);
        $this->loggerFactory->get('Request')->notice('delta_cache: Updating product count-@message', [
          '@message' => print_r(count($cacheTags), TRUE),
        ]);
        $this->loggerFactory->get('Request')->notice('Purge completed');
      }
      if (!empty($cacheTags)) {
        Cache::invalidateTags($cacheTags);
        $this->cacheRender->deleteMultiple($pathArray);
        return TRUE;
      } else {
        return FALSE;
      }
    }
  }
  /*
  * Method to get product deetails of the given sku and re-create.
  */
  public function product_cache_purge($sku, $form_submit=FALSE) {
    $sync_config = $this->configFactory->get('delta_services.deltaservicesconfig');
    $commerceType = $sync_config->get('site_commerce');
    $site = $sync_config->get('global_site_name');
    if($commerceType == 'product') {
      try {
        $variations = $this->entityTypeManager
          ->getStorage('commerce_product_variation')
          ->loadByProperties(['sku' => $sku]);
      } catch (\Exception $error) {
        $this->loggerFactory->get('checkout_cache')->alert(t('@err', ['@err' => $error->__toString()]));
        $this->messenger->addWarning(t('Unable to proceed. Please contact an administrator. Error code: C9A00B67-@line-@time', ['@line' => __LINE__, '@time' => time()]));
      }
      if(!empty($variations)) {
        foreach($variations as $variation) {
          $productId = $variation->getProductId();
          try {
            $variation->delete();
          } catch (\Exception $error) {
            $this->loggerFactory->get('checkout_cache')->alert(t('@err', ['@err' => $error->__toString()]));
            $this->messenger->addWarning(t('Unable to proceed. Please contact an administrator. Error code: C9A00B67-@line-@time', ['@line' => __LINE__, '@time' => time()]));
          }
          try {
            $this->entityTypeManager->getStorage('commerce_product')->load($productId)->delete();
          } catch (\Exception $error) {
            $this->loggerFactory->get('checkout_cache')->alert(t('@err', ['@err' => $error->__toString()]));
            $this->messenger->addWarning(t('Unable to proceed. Please contact an administrator. Error code: C9A00B67-@line-@time', ['@line' => __LINE__, '@time' => time()]));
          }
          $this->loggerFactory->get('Request')->notice('delta_cache: Deleted Product -@message', [
            '@message' => print_r($productId, TRUE),
          ]);
        };
        if($form_submit) {
          $path = "/product-detail/" . $sku;
          return $path;
        }
      } else {
        return FALSE;
      }
    }
    if($commerceType == 'node') {
      if (!empty($sku)) {
        $node = $this->deltaservice->getProductNid($sku);
        if ($node) {
          try {
            $node->delete();
          } catch (\Exception $error) {
            $this->loggerFactory->get('checkout_cache')->alert(t('@err', ['@err' => $error->__toString()]));
            $this->messenger->addWarning(t('Unable to proceed. Please contact an administrator. Error code: C9A00B67-@line-@time', ['@line' => __LINE__, '@time' => time()]));
          }
          $this->loggerFactory->get('Request')->notice('delta_cache: Deleted Product node id-@message', [
            '@message' => print_r($node->get('nid')->value,TRUE),
          ]);
          if($form_submit) {
            $path = "/pdp/" . $sku;
            return $path;
          }
        } else {
          return FALSE;
        }
      }
    }
  }
}

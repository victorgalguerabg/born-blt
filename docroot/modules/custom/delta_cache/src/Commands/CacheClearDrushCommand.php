<?php
namespace Drupal\delta_cache\Commands;

use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drush\Commands\DrushCommands;
use Drupal\Core\Cache\Cache;

class CacheClearDrushCommand extends DrushCommands {
  /**
   * Logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * To log the process.
   */
  public function __construct(LoggerChannelFactoryInterface $logger_factory) {
    $this->loggerFactory = $logger_factory;
  }

  /**
   * Clear all cache including varnish cache.
   *
   * @command clear:all-cache
   *
   * @usage drush clear:all-cache
   *   Clear all cache.
   *
   * @aliases clear_cache_all
   */
  public function clearAllCache() {
    drupal_flush_all_caches();
    Cache::invalidateTags(['rendered']);
    $this->loggerFactory->get('Request')->notice("All cache cleared");
  }
}
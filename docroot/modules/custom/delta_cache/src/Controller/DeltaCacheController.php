<?php

namespace Drupal\delta_cache\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\delta_services\DeltaService;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Component\Serialization\Json;
use Drupal\delta_cache\DeltaCacheService;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;

/**
 * Controller routines for Delta cache routes.
 */
class DeltaCacheController extends ControllerBase {
  /**
   * Drupal\delta_services\DeltaService definition.
   *
   * @var \Drupal\delta_services\DeltaService
   */
  protected $deltaservice;
  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Drupal\Core\Messenger\MessengerInterface definition.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * @var DeltaCacheService
   */
  protected $deltacache;

  /**
   * Drupal\Core\Logger\LoggerChannelFactoryInterface definition.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * Class constructor.
   */
  public function __construct(ConfigFactoryInterface $config_factory,
                              MessengerInterface $messenger,
                              DeltaService $deltaservice,
                              DeltaCacheService $deltacache,
                              LoggerChannelFactoryInterface $logger_factory) {
    $this->configFactory = $config_factory;
    $this->messenger = $messenger;
    $this->deltaservice = $deltaservice;
    $this->deltacache = $deltacache;
    $this->loggerFactory = $logger_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('messenger'),
      $container->get("delta_services.service"),
      $container->get('delta_cache.cache_service'),
      $container->get('logger.factory')
    );
  }
  /**
   * Implementation of product cache clear callback.
   */
  public function updateProductCache($key = NULL) {
    $milliseconds = round(microtime(true) * 1000);
    $config = $this->configFactory->getEditable('delta_services.deltaservicesconfig');
    $cron_key = $config->get('product_cache_update_cron_key');
    $last_cron = ($config->get('cache_update_last_cron'))? $config->get('cache_update_last_cron') : $milliseconds;
    $cron_end_time = $milliseconds;
    if ($cron_key == $key) {
      $domain = $config->get('api_base_path');
      $iterationLimit = $config->get('cache_product_iteration');
      for ($i=0;$i<$iterationLimit;$i++) {
        $skuArray = [];
        $Url = $domain . $config->get('cache_product_update_api');
        $Url = str_replace("{lastCron}", $last_cron, $Url);
        $Url = str_replace("{timeNow}", $cron_end_time, $Url);
        $Url = str_replace("{pageNumber}", $i, $Url);
        $this->loggerFactory->get('Request')->notice('delta_cache: API URL to fetch updated product for page - '.$i.'-@message', [
          '@message' => print_r($Url, TRUE),
        ]);
        $result = $this->deltaservice->apiCall($Url, "GET", [], "", "Cron-product-purge");
        if ($result['status'] == 'SUCCESS' && (!empty($result['response'])) && ($result['response'] != 'null') ) {
          $content = Json::decode($result['response']);
          if (isset($content['content'])) {
            foreach ($content['content'] as $products) {
              $skuArray[] = $products['name'];
              $lastProductModified = $products['modified'];
            }
          }
          if (count($skuArray) > 0) {
            $this->loggerFactory->get('Request')->notice('delta_cache: Product(s) modified since last cron - '.$i.' -@message to current time-@current', [
              '@message' => print_r($last_cron, TRUE),
              '@current' => print_r($cron_end_time, TRUE),
            ]);
            $this->loggerFactory->get('Request')->notice('delta_cache: Clearing cache for modified product(s) - '.$i.'-@message', [
              '@message' => print_r($skuArray, TRUE),
            ]);
            $action = $this->deltacache->clearCache($skuArray);
            if ($action) {
              $nextCronStart = (!empty($lastProductModified))? $lastProductModified : $cron_end_time;
              $config->set('cache_update_last_cron', $nextCronStart);
              $config->save(TRUE);
            } else {
              $this->loggerFactory->get('Request')->notice('delta_cache: Cache clear failed for product(s) request - '.$i.' failed-@message', [
                '@message' => print_r($skuArray, TRUE),
              ]);
            }

          } else {
            $this->loggerFactory->get('Request')->notice('delta_cache: No products have been modified since last cache clear.');
          }
          if($content['totalElements'] == 0 || $content['totalPages'] == $i) {
            break;
          }
        } else {
          $this->loggerFactory->get('Request')->notice('delta_cache: API issue please try again later');
        }
      }
    } else {
      $this->loggerFactory->get('Request')->notice('delta_cache: Key @message mismatch with CMS key please contact administrator.', [
        '@message' => print_r($key, TRUE),
      ]);
    }
    return [];
  }
  /*
  * Method to get API details for the given sku.
  */
  public function getProductDetails($sku=NULL) {
    $config = $this->configFactory->getEditable('delta_services.deltaservicesconfig');
    $domain = $config->get('api_base_path');
    $url = $domain . '/product/approved/en_us/product/' . $sku;
    $result = $this->deltaservice->apiCall($url, 'GET', [], [], 'PRODUCT_DETAIL');
    $content = Json::decode($result['response']);
    $build['output'] = [
      '#theme' => 'product_detail_api',
      '#data' => $content,
    ];
    return $build;
  }
}
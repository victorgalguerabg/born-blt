<?php

namespace Drupal\delta_cache\Form;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\delta_cache\DeltaCacheService;
use Drupal\Core\Cache\CacheTagsInvalidatorInterface;
use Drupal\delta_services\DeltaService;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class DeltaServicesConfigForm.
 */
class DeltaCachePurgeForm extends ConfigFormBase {

  /**
   * Drupal\delta_services\DeltaService definition.
   *
   * @var \Drupal\delta_services\DeltaService
   */
  protected $deltaservice;

  /**
   * @var DeltaCacheService
   */
  protected $deltacache;

  /**
   * The Cache Render.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cacheRender;

  /**
   * To get path alias.
   *
   * @var service
   */
  public $coreService;

  /**
   * The cache tags invalidator.
   *
   * @var \Drupal\Core\Cache\CacheTagsInvalidatorInterface
   */
  protected $cacheTagsInvalidator;

  /**
   * Drupal\Core\Logger\LoggerChannelFactoryInterface definition.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Symfony\Component\HttpFoundation\RequestStack definition.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  public function __construct(DeltaService $deltaservice,
                              DeltaCacheService $deltacache,
                              CacheBackendInterface $cache_render,
                              ContainerInterface $coreService,
                              CacheTagsInvalidatorInterface $cache_tags_invalidator,
                              LoggerChannelFactory $loggerFactory,
                              ConfigFactoryInterface $config_factory,
                              RequestStack $requestStack) {
    $this->deltaservice = $deltaservice;
    $this->deltacache = $deltacache;
    $this->cacheRender = $cache_render;
    $this->coreService = $coreService;
    $this->cacheTagsInvalidator = $cache_tags_invalidator;
    $this->loggerFactory = $loggerFactory->get('checkout_flow');
    $this->configFactory = $config_factory;
    $this->requestStack = $requestStack;
  }
  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('delta_services.service'),
      $container->get('delta_cache.cache_service'),
      $container->get('cache.render'),
      $container->get('service_container'),
      $container->get('cache_tags.invalidator'),
      $container->get('logger.factory'),
      $container->get('config.factory'),
      $container->get('request_stack')
    );
  }
  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'delta_cache.delta_cache_purge_settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'delta_cache_purge_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['purge_select'] = [
      '#type' => 'radios',
      '#title' => $this->t('Select cache clear type'),
      '#options' => [
        'sku' => $this->t('Sku Based'),
        'path' => $this->t('Path Based'),
        'tag' => $this->t('Cache Tag')
      ],
      '#attributes' => [
        'name' => 'field_select_purge',
      ],
    ];
    $form['purge_sku'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Cache Clear SKU'),
      '#description' => $this->t('Enter multiple SKU in newline'),
      '#states' => [
        //show this textfield only if the radio 'sku' is selected above
        'visible' => [
          ':input[name="field_select_purge"]' => ['value' => 'sku'],
        ],
      ],
    ];
    $form['purge_path'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Cache Clear Path'),
      '#description' => $this->t('Enter multiple path in newline along with preceding "/". Use "/" or "/home-page" to clear Homepage cache.'),
      '#states' => [
        //show this textfield only if the radio 'path' is selected above
        'visible' => [
          ':input[name="field_select_purge"]' => ['value' => 'path'],
        ],
      ],
    ];
    $form['cache_tags'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Clear Cache Tags'),
      '#description' => $this->t('Enter multiple Tags in newline along with preceding'),
      '#states' => [
        //show this textfield only if the radio 'tag' is selected above
        'visible' => [
          ':input[name="field_select_purge"]' => ['value' => 'tag'],
        ],
      ],
    ];
    $form['submit_purge'] = [
      '#type' => 'submit',
      '#value' => $this->t('Purge'),
      '#submit' => array([$this, 'submit_purge']),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submit_purge(array &$form, FormStateInterface $form_state) {
    $pathAll = $form_state->getValue('purge_path');
    $skuAll = $form_state->getValue('purge_sku');
    $tagsAll= $form_state->getValue('cache_tags');
    if(!empty($pathAll)) {
      $paths = explode(PHP_EOL, $pathAll);
      $paths = array_map('trim', $paths);
      $this->deltacache->clearCache(NULL, $paths);
    } elseif(!empty($skuAll)) {
      $skuArray = explode(PHP_EOL, $skuAll);
      $skuArray = array_map('trim', $skuArray);
      $action = $this->deltacache->clearCache($skuArray);
      if ($action) {
        $this->messenger()->addStatus($this->t('Purge completed'));
      } else {
        $this->messenger()->addStatus($this->t('API issue please try again later!'));
      }
    } elseif (!empty($tagsAll)){
      $tags = explode(PHP_EOL, $tagsAll);
      $cacheTags = array_map('trim', $tags);
      $this->loggerFactory->notice('delta_cache: cache tags-@message', [
        '@message' => print_r($cacheTags, TRUE),
      ]);
      $this->deltacache->deltaDeleteCacheId($cacheTags);
      $this->messenger()->addStatus($this->t('Purge completed'));
    }
    else {
      $this->messenger()->addWarning($this->t('Please enter any Cache ID (cid)'));
    }
  }
}

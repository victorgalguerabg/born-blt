<?php

namespace Drupal\delta_cache\Form;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\delta_cache\DeltaCacheService;
use Drupal\Core\Cache\CacheTagsInvalidatorInterface;
use Drupal\delta_services\DeltaService;
use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Messenger\MessengerInterface;

/**
 * Class DeltaPlpCachePurgeForm.
 */
class DeltaPlpCachePurgeForm extends ConfigFormBase {

  /**
   * Drupal\delta_services\DeltaService definition.
   *
   * @var \Drupal\delta_services\DeltaService
   */
  protected $deltaservice;

  /**
   * @var DeltaCacheService
   */
  protected $deltacache;

  /**
   * The Cache Render.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cacheRender;

  /**
   * To get path alias.
   *
   * @var service
   */
  public $coreService;

  /**
   * The cache tags invalidator.
   *
   * @var \Drupal\Core\Cache\CacheTagsInvalidatorInterface
   */
  protected $cacheTagsInvalidator;

  /**
   * Drupal\Core\Logger\LoggerChannelFactoryInterface definition.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Symfony\Component\HttpFoundation\RequestStack definition.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  public function __construct(DeltaService $deltaservice,
                              DeltaCacheService $deltacache,
                              CacheBackendInterface $cache_render,
                              ContainerInterface $coreService,
                              CacheTagsInvalidatorInterface $cache_tags_invalidator,
                              LoggerChannelFactory $loggerFactory,
                              ConfigFactoryInterface $config_factory,
                              RequestStack $requestStack,
                              EntityTypeManagerInterface $entity,
                              MessengerInterface $messenger) {
    $this->deltaservice = $deltaservice;
    $this->deltacache = $deltacache;
    $this->cacheRender = $cache_render;
    $this->coreService = $coreService;
    $this->cacheTagsInvalidator = $cache_tags_invalidator;
    $this->loggerFactory = $loggerFactory->get('Request');
    $this->configFactory = $config_factory;
    $this->requestStack = $requestStack;
    $this->entityTypeManager = $entity;
    $this->messenger = $messenger;
  }
  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('delta_services.service'),
      $container->get('delta_cache.cache_service'),
      $container->get('cache.render'),
      $container->get('service_container'),
      $container->get('cache_tags.invalidator'),
      $container->get('logger.factory'),
      $container->get('config.factory'),
      $container->get('request_stack'),
      $container->get('entity_type.manager'),
      $container->get('messenger')
    );
  }
  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'delta_cache.delta_plp_cache_purge',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'delta_plp_cache_purge_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['plp_pages'] = [
      '#type' => 'select',
      '#title' => $this->t('PLP page*'),
      '#options' => $this->getPlpPages(),
    ];
    $form['submit_purge'] = [
      '#type' => 'submit',
      '#value' => $this->t('Purge'),
      '#submit' => array([$this, 'submit_purge']),
    ];
    return $form;
  }
  /**
   * returns Us States list with Key value pair array
   */
  public function getPlpPages() {
    $sync_config = $this->configFactory->get('delta_services.deltaservicesconfig');
    $site = $sync_config->get('global_site_name');
    switch($site) {
      case 'peerless':
        $bundles = 'landing_page';
        break;
      default:
        $bundles = 'plp';
        break;

    }
    $entity_ids = [];
    try {
      $entity_storage = $this->entityTypeManager->getStorage('node');
    } catch (\Exception $error) {
      $this->loggerFactory->get('checkout_cache')->alert(t('@err', ['@err' => $error->__toString()]));
      $this->messenger->addWarning(t('Unable to proceed. Please contact an administrator. Error code: 8FBDC239-@line-@time', ['@line' => __LINE__, '@time' => time()]));
    }
    $query = $entity_storage->getQuery();
    $query->condition('status',1); // only published nodes
    $query->condition('type',$bundles); // of a certain content type
    $entity_ids = array_merge($entity_ids, $query->execute());
    $options = array();
    $options[0] = 'Select PLP page';
    foreach($entity_ids as $id) {
      try {
        $entity = $this->entityTypeManager->getStorage('node')->load($id);
      } catch (\Exception $error) {
        $this->loggerFactory->get('checkout_cache')->alert(t('@err', ['@err' => $error->__toString()]));
        $this->messenger->addWarning(t('Unable to proceed. Please contact an administrator. Error code: 8FBDC239-@line-@time', ['@line' => __LINE__, '@time' => time()]));
      }
      if($entity->hasField('field_cache_key')) {
        $cache_key = $entity->get('field_cache_key')->value;
        $options[$cache_key] = $entity->label();
      }
    }
    return $options;
  }
  /**
   * {@inheritdoc}
   */
  public function submit_purge(array &$form, FormStateInterface $form_state) {
    $plp = $form_state->getValue('plp_pages');
    if(!empty($plp)) {
      $config = $this->configFactory->get('delta_services.deltaservicesconfig');
      $pageCount = $config->get('plp_cache_pages');
      if($pageCount) {
        for($i=0;$i<$pageCount;$i++){
          $tags[] = 'plp_filters_'.$plp."_".$i;
          $tags[] = $plp."_".$i;
        }
      }
      $cacheTags = array_map('trim', $tags);
      $this->loggerFactory->notice('delta_cache: PLP cache tags-@message', [
        '@message' => print_r($cacheTags, TRUE),
      ]);
      $this->deltacache->deltaDeleteCacheId($cacheTags);
      $this->messenger()->addStatus($this->t('PLP Purge completed'));
    } else {
      $this->messenger()->addWarning($this->t('Please select a valid plp page.'));
    }
  }
}

<?php

namespace Drupal\delta_cache\Form;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\delta_cache\DeltaCacheService;
use Drupal\Core\Cache\CacheTagsInvalidatorInterface;
use Drupal\delta_services\DeltaService;
use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\Core\Url;

/**
 * Class DeltaServicesConfigForm.
 */
class DeltaProductPurgeForm extends ConfigFormBase {

  /**
   * Drupal\delta_services\DeltaService definition.
   *
   * @var \Drupal\delta_services\DeltaService
   */
  protected $deltaservice;

  /**
   * @var DeltaCacheService
   */
  protected $deltacache;

  /**
   * The Cache Render.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cacheRender;

  /**
   * To get path alias.
   *
   * @var service
   */
  public $coreService;

  /**
   * The cache tags invalidator.
   *
   * @var \Drupal\Core\Cache\CacheTagsInvalidatorInterface
   */
  protected $cacheTagsInvalidator;

  /**
   * Drupal\Core\Logger\LoggerChannelFactoryInterface definition.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Symfony\Component\HttpFoundation\RequestStack definition.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  public function __construct(DeltaService $deltaservice,
                              DeltaCacheService $deltacache,
                              CacheBackendInterface $cache_render,
                              ContainerInterface $coreService,
                              CacheTagsInvalidatorInterface $cache_tags_invalidator,
                              LoggerChannelFactory $loggerFactory,
                              ConfigFactoryInterface $config_factory,
                              RequestStack $requestStack) {
    $this->deltaservice = $deltaservice;
    $this->deltacache = $deltacache;
    $this->cacheRender = $cache_render;
    $this->coreService = $coreService;
    $this->cacheTagsInvalidator = $cache_tags_invalidator;
    $this->loggerFactory = $loggerFactory->get('checkout_flow');
    $this->configFactory = $config_factory;
    $this->requestStack = $requestStack;
  }
  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('delta_services.service'),
      $container->get('delta_cache.cache_service'),
      $container->get('cache.render'),
      $container->get('service_container'),
      $container->get('cache_tags.invalidator'),
      $container->get('logger.factory'),
      $container->get('config.factory'),
      $container->get('request_stack')
    );
  }
  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'delta_cache.delta_product_purge_settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'delta_product_purge_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['purge_sku'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Product Clear SKU'),
      '#description' => $this->t('Enter Product SKU which has to be deleted and re-created'),
    ];
    $form['submit_purge'] = [
      '#type' => 'submit',
      '#value' => $this->t('Purge'),
      '#submit' => array([$this, 'submit_purge']),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submit_purge(array &$form, FormStateInterface $form_state) {
    $sku = $form_state->getValue('purge_sku');
    if(!empty($sku)) {
      $action = $this->deltacache->product_cache_purge($sku, TRUE);
      if ($action) {
        $link = '/admin/config/delta_settings/product-details-api/ '. $sku;
        $this->messenger()->addStatus($this->t('Product Purged and Re-created. <a target="_blank" href=":url">Product details from API</a>', array(':url' => $link)));
        $url = Url::fromUri('internal:' . $action);
        $form_state->setRedirectUrl($url);
      } else {
        $this->messenger()->addStatus($this->t('No product found for the given SKU!'));
      }
    } else {
      $this->messenger()->addWarning($this->t('Please enter a valid SKU'));
    }
  }
}
<?php

namespace Drupal\delta_cache\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class DeltaServicesConfigForm.
 */
class DeltaGeneralSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'delta_cache.deltageneralsettings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'delta_general_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('delta_cache.deltageneralsettings');
    $form['cache_expiry_time'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Cache Expiry Time'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('cache_expiry_time'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Can add validation if required.
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('delta_cache.deltageneralsettings')
      ->set('cache_expiry_time', $form_state->getValue('cache_expiry_time'))
      ->save();
  }

}

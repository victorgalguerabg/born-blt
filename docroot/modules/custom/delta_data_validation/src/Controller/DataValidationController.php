<?php

namespace Drupal\delta_data_validation\Controller;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;

/**
 * Implementation of data validation controller.
 */

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Defines a route controller data validation .
 */
class DataValidationController extends ControllerBase {
  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Drupal\Core\Logger\LoggerChannelFactoryInterface definition.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */

  public static $logger;

    /**
    * Symfony\Component\HttpFoundation\RequestStack definition.
    *
    * @var Symfony\Component\HttpFoundation\RequestStack
    */
    protected $requestStack;
  /**
   * Class constructor.
   */
  public function __construct(ConfigFactoryInterface $config_factory, LoggerChannelFactoryInterface $loggerChannel, RequestStack $requestStack) {
    $this->configFactory = $config_factory;
    $this->logger = $loggerChannel;
    $this->requestStack = $requestStack;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
    // Load the service required to construct this class.
        $container->get('config.factory'),
        $container->get('logger.factory'),
        $container->get('request_stack')
    );
  }

  /**
   * Handler for melissa email validation
   */
  public function dataValidation() {
    $results = [];
    $typed_string = $this->requestStack->getCurrentRequest()->get('email');
    $config = $this->config('delta_data_validation.config');
    $melissa_api = $config->get('melissa_api');
    if(empty($melissa_api)){
      return new JsonResponse($results);
    }
    $melissa_code = $config->get('melissa_code');
    $melissa_code_arr = explode(",",$melissa_code);
    $mel_resp_code_list = array_map('trim', $melissa_code_arr);
    $melissa_message = $config->get('melissa_message');
    $readDataARR = $this->validate_inputdata($melissa_api, $typed_string);
    if(is_array($readDataARR) && count($readDataARR)){
      if(array_key_exists('Records', $readDataARR) && count($readDataARR['Records'])){
        $data_resp_code = $readDataARR['Records'][0]['Results'];
        $single_multi_resp_code = explode(",", $data_resp_code);
        if(is_array($single_multi_resp_code) && gettype($single_multi_resp_code) == 'array'){
          foreach ($single_multi_resp_code as $key => $value) {
            if(in_array($value, $mel_resp_code_list)){
               $results[] = ['code' => $value];
            }
          }
        }
      }
    }
    $resp = [];
    if(count($results) > 0){
      $resp[]  = ['msg' => $melissa_message];
    }
    return new JsonResponse($resp);
  }

  //Melissa Email Validation
  public function validate_inputdata($melissa_api, $email){
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    $endpoint = $melissa_api.$email;
    curl_setopt($curl, CURLOPT_URL,$endpoint);
    $this->logger->get('melissa_req_resp')->notice($curl);
    $result = json_decode(curl_exec($curl), true);
    $this->logger->get('melissa_req_resp')->notice(json_encode($result));
    return $result;
  }
}

<?php

namespace Drupal\delta_data_validation\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class DeltaDataTestingConfigForm.
 */
class DeltaIntegrationConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'delta_data_validation.config',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'delta_integration_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('delta_data_validation.config');
    $form['delta_integration_melinssa'] = [
      '#type' => 'details',
      '#title' => $this->t('Melinssa'),
      '#open' => FALSE,
      '#weight' => 3,
    ];
    $form['delta_integration_melinssa']['melissa_api'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Please enter melissa api'),
      '#default_value' => $config->get('melissa_api'),
      '#description' => $this->t("Please update melissa api with license key"),
    ];
    $form['delta_integration_melinssa']['melissa_message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Please enter melissa message'),
      '#default_value' => $config->get('melissa_message'),
      '#description' => $this->t("Please enter melissa message which will display on the web forms"),
    ];
    $form['delta_integration_melinssa']['melissa_code'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Please enter melissa response codes'),
      '#default_value' => $config->get('melissa_code'),
      '#description' => $this->t("Please display error message, when these response code received from API"),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $this->config('delta_data_validation.config')
    ->set('melissa_api', $form_state->getValue('melissa_api'))
    ->set('melissa_message', $form_state->getValue('melissa_message'))
    ->set('melissa_code', $form_state->getValue('melissa_code'))
    ->save();
  }

}

<?php

namespace Drupal\checkout_services;

use Drupal\commerce_cart\CartManagerInterface;
use Drupal\commerce_cart\CartProviderInterface;
use Drupal\commerce_product\Entity\Product;
use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\commerce_order\Entity\Order;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\Core\Config\ConfigFactoryInterface;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ConnectException;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;

class CheckoutServices {
   /**
   * Drupal\Core\Logger\LoggerChannelFactoryInterface definition.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  protected $cartManager;

  protected $cartProvider;

  protected $entityTypeManager;

  protected $accountId;

  protected $currentUser;

  protected $priorityAcces;

  protected $cart_provider;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $customEntityTypeManager;

  /**
   * The tempstore service.
   *
   * @var \Drupal\user\PrivateTempStoreFactory
   */
  protected $tempStoreFactory;

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  protected $entityManager;

  /**
   * Drupal\Core\Entity\EntityTypeManager; definition.
   *
   * @var Drupal\Core\Entity\EntityTypeManager;
   */
  protected $entityTermManager;

  /**
   * Guzzle\Client instance.
   *
   * @var \Guzzle\Client
   */
  protected $httpClient;

  /**
   * Recommerce Config Variable.
   *
   */
  protected $recommerceConfig;

  /**
   * @var \Drupal\commerce_cart\CartManagerInterface
   */
  protected $cart_manager;

  /**
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $current_user;

  /**
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;


  /**
   * {@inheritdoc}
   */
  public function __construct(CartManagerInterface $cartManager, CartProviderInterface $cartProvider, EntityTypeManagerInterface $entityTypeManager, AccountProxyInterface $currentUser, EntityTypeManagerInterface $customEntityTypeManager,  PrivateTempStoreFactory $privateTempStoreFactory, ConfigFactoryInterface $configFactory, ClientInterface $httpClient, LoggerChannelFactoryInterface $logger_factory) {
    $this->cart_manager = $cartManager;
    $this->cart_provider = $cartProvider;
    $this->current_user = $currentUser;
    $this->customEntityTypeManager = $customEntityTypeManager;
    $this->tempStoreFactory = $privateTempStoreFactory->get('checkout_form_data');
    $this->configFactory = $configFactory->get('checkout_services.checkout_configs');
    $this->recommerceConfig = $configFactory->get('delta.max_product_qty');
    $this->entityManager = $entityTypeManager;
    $this->httpClient = $httpClient;
    $this->loggerFactory = $logger_factory;
    try {
      $this->entityTypeManager = $entityTypeManager->getStorage('commerce_order');
      $this->entityTermManager = $entityTypeManager->getStorage('taxonomy_term');
    } catch (\Exception $error) {
      $this->loggerFactory->get('checkout_services')->alert(t('@err', ['@err' => $error->__toString()]));
      $this->messenger->addWarning(t('Unable to proceed, please try again. If the problem persists, please contact an administrator. Error code: E4DC38E1-@line-@time', ['@line' => $error->getLine(), '@time' => time()]));
    }
  }

  /**
   * Http Client function to call Ecommerce API.
   * Outdated function, Soonly will be removed from code base.
   */
  public function ecommerceApiCallOutdated($url, $obj) {

    $request = $this->httpClient->post($url, [
      'json' => $obj,
      'auth' => [$this->configFactory->get('delta_api_username'),$this->configFactory->get('delta_api_password')],
      'headers' => [],
    ]);

    $response = json_decode($request->getBody(),true);
    return $response;
  }

   /**
   * Function to call ecommerce webservice API Call.
   *
   * @param string $url
   *   webServices API URL.
   * @param string $method
   *   WebServices method. It can be GET, POST, PUT, DELETE.
   * @param array $headers
   *   webServices headers.
   * @param string $data
   *   Process with data if required.
   * @param string $requested_from
   *   Specifiy the request type. This will used to track the logging.
   *
   * @return array
   *   A render response array.
   *
   * @throws exception
   *   Throws exception if find any errors.
   */
  public function ecommerceApiCall($url,$method,array $MoreHeaders, $data, $requested_from = "COMMERCE_API") {
    $headers = [];
    if((is_array($MoreHeaders)) && (!empty($MoreHeaders))){
      $headers = array_merge($headers, $MoreHeaders);
    }
    $headers['FORWARDED'] = NULL;
    try {
      $client = $this->httpClient;
      $options = [];
      $options['headers']  = $headers;
      $options['verify'] = FALSE;
      if (!empty($data)) {
        $options['json'] = $data;
      }
      $options['http_errors'] = FALSE;

      $username = $this->configFactory->get('delta_api_username');
      $password = $this->configFactory->get('delta_api_password');
      if ($username == '' || $password == '') {
        $this->loggerFactory->get('APICredentialsMissing')
          ->notice('Please update API Credentials');
        return FALSE;
      }
      $options['auth'] = [
        $username,
        $password,
      ];
      try {
        $request = $client->$method($url, $options);
      } catch (ConnectException $e) {
        $this->loggerFactory->get('ECOM_VPN_Connection_ERR')
          ->notice($requested_from . " Connection Error");
        return FALSE;
      }

      if (NULL === $request) {
        $responseData = ['Web service connectivity issue'];
        $this->loggerFactory->get('SYNC Error ')
          ->notice('SYNC Error ' . $requested_from . json_encode($responseData));
      }
      $responseData = json_decode($request->getBody(),true);

      if (!empty($responseData->errors)) {
        switch ($responseData->errors[0]->type) {
          case 'UnknownIdentifierError':
            $this->loggerFactory->get('SYNC Error')
              ->notice($requested_from . json_encode($responseData));
            return $responseData;
        }
      }
      else {
        return $responseData;
      }
    }catch (ClientException $clientException) {
      $responseData = $clientException->getResponse();
      $this->loggerFactory->get('Request')->notice($requested_from . $responseData);
    }
    return $responseData;
  }

  /**
   * Check cart empty or not.
   */
  public function cartCheck() {
     $currentCartCount = $this->cart_provider->getCartIds();
     if(count($currentCartCount) > 0) {
       return $currentCartCount;
     }
     else {
       return;
     }
  }

  /**
   * Function will return cart list products API object.
   */
  protected function innerObjCons() {
    $currentCartId = $this->cart_provider->getCartIds();
    $currentOrders = Order::load($currentCartId[0]);
    $cart = ($currentOrders) ? $currentOrders->toArray() : NULL;
    if (count($cart) <= 0) {
      return;
    }
    else {
      $total_items = $currentOrders->getItems();
      $i = 1;
      $total_unitPrice = 0;
      $orderWeight = 0;
      $order_item_construction = [];
      foreach ($total_items as $val) {
        $order_list = $val->toArray();
        $Variation = ProductVariation::load($order_list['purchased_entity'][0]['target_id']);
        if($Variation) {
          $productVariation = $Variation->toArray();
          $product = Product::load($productVariation['product_id'][0]['target_id'])->toArray();
          $total_unitPrice = $total_unitPrice + $order_list['total_price'][0]['number'];
          $unit_price = $this->getPriceFormat($order_list['unit_price'][0]['number']);
          $product_weight = $productVariation['field_product_weight'][0]['value'] * $order_list['quantity'][0]['value'];
          $orderWeight = $orderWeight + $product_weight;
          $prdImg = "";
          if (count($product['field_image_url']) > 0) {
            $prdImg = $product['field_image_url'][0]['value'];
          }

          $order_item_construction[] =
            [
              "lineNumber" => $i,
              "skuCode" => $productVariation['sku'][0]['value'],
              "description" => $product['title'][0]['value'],
              "listPrice" => $unit_price,
              "unitPrice" => $unit_price,
              "extendedPrice" => $unit_price,
              "externalPrice" => NULL,
              "offBottomDiscPct" => NULL,
              "quantity" => (int)$order_list['quantity'][0]['value'],
              "tax" => 0,
              "taxPercent" => 0,
              "taxCode" => 0,
              "orderStatus" => NULL,
              "trackingNumber" => NULL,
              "lineTotal" => 0,
              "carrierWebsite" => NULL,
              "carrier" => $this->configFactory->get('carrier'),
              "branchPlant" => NULL,
              "onHandQuantity" => "0",
              "availableQuantity" => "0",
              "availableQuantityFGW" => NULL,
              "committedQuantity" => "0",
              "onReceiptQuantity" => "0",
              "promoCode" => "",
              "activityNumber" => "",
              "estShipDate" => NULL,
              "requestShipDate" => NULL,
              "customerItemNumber" => NULL,
              "shipDate" => NULL,
              "qtyOrdered" => NULL,
              "qtyShipped" => "0",
              "qtyBackOrdered" => "0",
              "productWeight" => $product_weight,
              "netPrice" => NULL,
              "imageFilename" => $prdImg,
              "orderWeight" => $product_weight
            ];
        }
        $i++;
      }
      return ["obj" => $order_item_construction, "total_unitPrice" => $total_unitPrice,"orderWeight" => $orderWeight];
    }
  }

  /**
   * Order Validation API call Object Construction.
   */
  public function OrderValidationObjConstruct() {

    $innerObj = $this->innerObjCons();
    if(!is_array($innerObj)) {
      return;
    }

    $lenthArray = [8,4,4,4,12];

    $orderValidation = [
      "cartID" => $this->randomNumber($lenthArray),
      "customerID" => $this->getCurrentUsername(),
      "orderNumber" => "",
      "companyNumber" => $this->configFactory->get('companyNumber'),
      "documentType" => $this->configFactory->get('documentType'),
      "customerOrderNumber" => "",
      "quoteID" => NULL,
      "orderItems" => $innerObj['obj'],
      "phone" => NULL,
      "email" => NULL,
      "futureRequestDate" => NULL,
      "minimumOrderCharge" => 0,
      "orderTradeAllowance" => 0,
      "shippingAddress" => NULL,
      "billingAddress" => NULL,
      "shippingOriginAddress" => NULL,
      "shippingServiceLevel" => NULL,
      "shippingCharge" => NULL,
      "shippingTax" => NULL,
      "shippingTaxPercent" => 0,
      "tax" => 0,
      "ccNumber" => NULL,
      "ccConfirmationCode" => NULL,
      "ccType" => NULL,
      "ccName" => NULL,
      "ccLastFour" => NULL,
      "ccExpireMonth" => NULL,
      "ccExpireYear" => NULL,
      "coupon" => NULL,
      "couponDiscount" => NULL,
      "orderSubTotal" => $this->getPriceFormat($innerObj['total_unitPrice']),
      "orderTotal" => $this->getPriceFormat($innerObj['total_unitPrice']),
      "orderPlaced" => NULL,
      "snappayAuthorizationID" => NULL,
      "maxLeadTime" => -1,
      "priorityAccessCode" => NULL,
      "shiptoNumber" => $this->configFactory->get('shiptoNumber'),
      "billtoNumber" => NULL,
      "source" => $this->configFactory->get('source'),
      "writeOrder" => NULL,
      "quickShip" => false,
      "activityCode" => NULL,
      "dropShip" => false,
      "dropShipAddress" => NULL,
      "paymetricAuthorizationCode" => NULL,
      "paymetricAuthorizationDate" => NULL,
      "paymetricTransactionId" => NULL,
      "paymetricAuthorizationTime" => NULL,
      "paymetricMessage" => NULL,
      "paymetricAVSAddress" => NULL,
      "paymetricAVSCode" => NULL,
      "paymetricAVSZipCode" => NULL,
      "paymetricResponseCode" => NULL,
      "paymetricAuthorizeAmount" => NULL,
      "valid" => false,
      "errors" => [],
      "orderWeight" => $innerObj['orderWeight'],
    ];
    return $orderValidation;
  }

  /**
   * Function to return the decimal numbers.
   */
  public function getPriceFormat($price) {
    return number_format((float) $price, 2, '.', '');
  }

  /**
   * Function to return the random number.
   */
  public function randomNumber($lenthArray) {
    $randomNumber = "";
    foreach ($lenthArray as $value) {
      $randomNumber .= $this->generateRandomNumber($value)."-";
    }
    return rtrim($randomNumber,"-");
  }

  /**
   * Function to return the unique number with 40 character.
   */
  public function generateRandomNumber($length) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $randomString = '';

    for ($i = 0; $i < $length; $i++) {
      $index = rand(0, strlen($characters) - 1);
      $randomString .= $characters[$index];
    }

    return $randomString;
  }

  /**
   * Returns current username
   */
  public function getCurrentUsername() {
    $currentUser = $this->current_user->getAccountName();
    if (!empty($currentUser)) {
      return $currentUser;
    }
    else {
      return "guest";
    }
  }

  /**
   * Function to return Order Status.
   */
  public function orderStstusWebCall($orderDetails) {

    // Check to Ecommerce Order Status API.
    $options = [];
    $options['http_errors'] = FALSE;
    $options['auth'] = [$this->configFactory->get('delta_api_username'), $this->configFactory->get('delta_api_password')];
    $apiUrl = trim($this->configFactory->get('orderStatus'))."?orderNumber=".$orderDetails['orderno']."&phoneNumber=&email=".$orderDetails['email']."&zipCode=".$orderDetails['zip']."&source";
    try{
      $request = $this->httpClient->request('GET', $apiUrl, $options);
    } catch(\Throwable $error) {
      $this->loggerFactory->get('checkout_services')->alert(t('@err', ['@err' => $error->__toString()]));
      $this->messenger->addWarning(t('Unable to proceed, please try again. If the problem persists, please contact an administrator. Error code: E4DC38E1-@line-@time', ['@line' => $error->getLine(), '@time' => time()]));
    }
    $response = json_decode($request->getBody(),true);
    return $response;
  }

  /**
   * Function to return Order Details.
   */
  public function orderDetailsWebCall($orderDetails) {

    // Check to Ecommerce Order Details API.
    $options = [];
    $options['http_errors'] = FALSE;
    $options['auth'] = [$this->configFactory->get('delta_api_username'), $this->configFactory->get('delta_api_password')];
    $apiUrl = trim($this->configFactory->get('orderDetails'))."?orderNumber=".$orderDetails['orderNo']."&companyNumber=".$orderDetails['customerNo']."&documentType=".$orderDetails['docType'];
    try{
      $request = $this->httpClient->request('GET', $apiUrl, $options);
    } catch(\Throwable $error) {
      $this->loggerFactory->get('checkout_services')->alert(t('@err', ['@err' => $error->__toString()]));
      $this->messenger->addWarning(t('Unable to proceed, please try again. If the problem persists, please contact an administrator. Error code: E4DC38E1-@line-@time', ['@line' => $error->getLine(), '@time' => time()]));
    }
    $response = json_decode($request->getBody(),true);
    return $response;
  }

  /**
   * Function to Empty current cart.
   */
  public function emptyCurrentCart() {
    $currentCartCount = $this->cart_provider->getCartIds();
    if(!empty($currentCartCount)) {
      $currentOrders = Order::load($currentCartCount[0]);
      $this->cart_manager->emptyCart($currentOrders);
    }
    else {
      return "";
    }
  }

  /**
   * Function to Delete Cart.
   * @param $cartId
   */
  public function deleteCart($cartId) {
    $currentOrders = Order::load($cartId);
    $this->cart_manager->emptyCart($currentOrders);
  }

  /**
   * Function to Get Current Cart Id.
   * @return array cart Id
   */
  public function getCurrentCartId() {
    $currentCartId = $this->cart_provider->getCartIds();
    if (!empty($currentCartId)) {
      return $currentCartId[0];
    }
  }

  /**
   * Function to Add response Object to order Object.
   */
  public function UpdateResponseObj($object) {
    $currentCartId = $this->cart_provider->getCartIds();
    if (!empty($currentCartId)) {
      $currentOrders = Order::load($currentCartId[0]);
      $order = $this->entityTypeManager->load($currentOrders->order_id->value);
      $order->field_order_object->value = json_encode($object);
      try{
        $order->save();
      } catch(\Exception $error) {
        $this->loggerFactory->get('checkout_services')->alert(t('@err', ['@err' => $error->__toString()]));
        $this->messenger->addWarning(t('Unable to proceed, please try again. If the problem persists, please contact an administrator. Error code: E4DC38E1-@line-@time', ['@line' => $error->getLine(), '@time' => time()]));
      }
    }
  }

  /**
   * Function to Email subscription Option enabling.
   */
  public function emailSubscription($email) {
    $currentCartId = $this->cart_provider->getCartIds();
    if (!empty($currentCartId)) {
      $currentOrders = Order::load($currentCartId[0]);
      $order = $this->entityTypeManager->load($currentOrders->order_id->value)  ;
      $order->field_email_subscription->value = ($email) ? $email : 0;
      try {
        $order->save();
      } catch (\Exception $error) {
        $this->loggerFactory->get('checkout_services')->alert(t('@err', ['@err' => $error->__toString()]));
        $this->messenger->addWarning(t('Unable to proceed, please try again. If the problem persists, please contact an administrator. Error code: E4DC38E1-@line-@time', ['@line' => __LINE__, '@time' => time()]));
      }
    }
  }

  /**
   * Function to get Order Object.
   */
  public function gerOrderconfirmationObj() {
    $currentCartId = $this->cart_provider->getCartIds();
    if(!empty($currentCartId)) {
      $currentOrders = Order::load($currentCartId[0]);
      $order = $this->entityTypeManager->load($currentOrders->order_id->value);
      if($order) {
        $orderObj = $order->toArray();
        return json_encode($orderObj);
      }
      else {
        return "";
      }
    }
    else {
      return "";
    }
  }


  /**
   * Function to get Order Object.
   */
  public function gerOrderObj($email = FALSE) {
    $currentCartId = $this->cart_provider->getCartIds();
    if(!empty($currentCartId)) {
      $currentOrders = Order::load($currentCartId[0]);
      $order = $this->entityTypeManager->load($currentOrders->order_id->value);
      if($order) {
        $orderObj = $order->toArray();
        if($email == TRUE) {
          return json_encode($orderObj);
        }
        return $orderObj['field_order_object'][0]['value'];
      }
      else {
        return "";
      }
    }
    else {
      return "";
    }
  }

  /**
   * Function to change cart state.
   */
  public function changeCartState() {
    $currentCartId = $this->cart_provider->getCartIds();
    if(!empty($currentCartId)) {
      $currentOrders = Order::load($currentCartId[0]);
      $order = $this->entityTypeManager->load($currentOrders->order_id->value);
      $order->cart->value = '0';
      try{
        $order->save();
      } catch(\Exception $error) {
        $this->loggerFactory->get('checkout_services')->alert(t('@err', ['@err' => $error->__toString()]));
        $this->messenger->addWarning(t('Unable to proceed, please try again. If the problem persists, please contact an administrator. Error code: E4DC38E1-@line-@time', ['@line' => $error->getLine(), '@time' => time()]));
      }
      return 1;
    }
  }

  /**
   * Function to change Complete state.
   */
  public function changeOrderState() {
    $currentCartId = $this->cart_provider->getCartIds();
    if(!empty($currentCartId)) {
      $currentOrders = Order::load($currentCartId[0]);
      $order = $this->entityTypeManager->load($currentOrders->order_id->value);
      $order->setEmail('Anonymous');
      $order->setCustomerId(0);
      $order->setIpAddress('NULL');
      $order->cart->value = '0';
      $order->state->value = 'completed';
      try{
        $order->save();
      } catch(\Exception $error) {
        $this->loggerFactory->get('checkout_services')->alert(t('@err', ['@err' => $error->__toString()]));
      }
    }
  }

  /**
   * Function to Get Current Cart Id.
   */
  public function getCurrentCartObj() {
    $currentCartId = $this->cart_provider->getCartIds();
    if(!empty($currentCartId)) {
      $currentOrders = Order::load($currentCartId[0]);
      return $currentOrders;
    }
    else {
      return "";
    }
  }

  /**
   * Function to Update Cart Id to Current User.
   */
  public function updateCartToCurrentUser($cartItems) {
    $order = $this->entityTypeManager->load($cartItems->order_id->value);
    $order->setCustomer($this->current_user);
    try {
      $order->save();
    } catch (\Exception $error) {
      $this->loggerFactory->get('checkout_services')->alert(t('@err', ['@err' => $error->__toString()]));
      $this->messenger->addWarning(t('Unable to proceed, please try again. If the problem persists, please contact an administrator. Error code: E4DC38E1-@line-@time', ['@line' => __LINE__, '@time' => time()]));
    }
  }

  /**
   * Combines all of a user's carts into their main cart.
   *
   * @param \Drupal\user\UserInterface $user
   *   The user.
   */
  public function combineUserCarts() {
    $currentCartId = $this->cart_provider->getCartIds();
    if(count($currentCartId) == 2) {
      $this->combineCarts($currentCartId[0], $currentCartId[1], TRUE);
    }
  }

  /**
   * Combines another cart into the main cart and optionally deletes the other
   * cart.
   * @param $currentCartId
   * @param $previouscartId
   * @param $delete
   */
  public function combineCarts($currentCartId, $previouscartId, $delete = FALSE) {
    if ($currentCartId !== $previouscartId) {
      $main_cart = Order::load($currentCartId);
      $other_cart = Order::load($previouscartId);
      $mainCartItems = $main_cart->getItems();
      foreach ($other_cart->getItems() as $item) {
        $itemList = $item->toArray();
        $updatedCount = 0;
        foreach ($mainCartItems as $mainitem) {
          $mainOrderItem = $mainitem->toArray();
          $Variation = ProductVariation::load($mainOrderItem['purchased_entity'][0]['target_id']);
          if($Variation) {
            $productVariation = $Variation->toArray();
            $product = Product::load($productVariation['product_id'][0]['target_id'])->toArray();
            $configMaxcount = 0;
            if(!empty($product['field_refurbished_product']) && $product['field_refurbished_product'][0]['value'] == 1) {
              $configMaxcount = $this->recommerceConfig->get('max_qty_recommerce');
            }
            else {
              $configMaxcount = $this->recommerceConfig->get('max_qty_delta');
            }
            if($mainOrderItem['purchased_entity'][0]['target_id'] ==  $itemList['purchased_entity'][0]['target_id']) {

              if((int)$itemList['quantity'][0]['value'] < $configMaxcount) { // case 1
                $maxQuantity = $configMaxcount - (int)$itemList['quantity'][0]['value'];

                if($maxQuantity != 0) {
                  $currentCartCnt = (int)$mainOrderItem['quantity'][0]['value'] + (int)$itemList['quantity'][0]['value'];
                  $confCount = $configMaxcount - $currentCartCnt;
                  if($confCount >= 0) {
                    $updatedCount = 1;
                    if($confCount == 0) {
                      $mainitem->setQuantity((int)$itemList['quantity'][0]['value']);
                      $other_cart->removeItem($item);
                      $this->cart_manager->addOrderItem($main_cart, $mainitem, TRUE);
                    }
                    else {
                      $other_cart->removeItem($item);
                      $item->get('order_id')->entity = $main_cart;
                      $this->cart_manager->addOrderItem($main_cart, $item, TRUE);
                    }
                  }
                  else {
                    $mcartCount = $configMaxcount - (int)$mainOrderItem['quantity'][0]['value'];
                    if($mcartCount != 0) {
                      $updatedCount = 1;
                      $item->setQuantity($mcartCount);
                      $other_cart->removeItem($item);
                      $item->get('order_id')->entity = $main_cart;
                      $this->cart_manager->addOrderItem($main_cart, $item, TRUE);
                    }
                    else {
                      $rcartCount = $configMaxcount - (int)$itemList['quantity'][0]['value'];
                      $updatedCount = 1;
                      $item->setQuantity($rcartCount);
                      $other_cart->removeItem($item);
                      $item->get('order_id')->entity = $main_cart;
                      $this->cart_manager->addOrderItem($main_cart, $item, TRUE);
                    }
                  }
                }
                else {
                  $updatedCount = 1;
                  $other_cart->removeItem($item);
                }
              }
              else {
                $existingCount = (int)$itemList['quantity'][0]['value'] - (int)$mainOrderItem['quantity'][0]['value'];
                if($existingCount != 0) {
                  $updatedCount = 1;
                  $other_cart->removeItem($item);
                  $item->setQuantity($existingCount);
                  $item->get('order_id')->entity = $main_cart;
                  $this->cart_manager->addOrderItem($main_cart, $item, TRUE);
                }
                else {
                  $updatedCount = 1;
                }
              }
            }
          }
        }
        if($updatedCount == 0) {
          $other_cart->removeItem($item);
          $item->get('order_id')->entity = $main_cart;
          $this->cart_manager->addOrderItem($main_cart, $item, TRUE);
        }
      }
      try{
        $main_cart->save();
      } catch(\Exception $error) {
        $this->loggerFactory->get('checkout_flow_checkout_form')->alert(t('@err', ['@err' => $error->__toString()]));
        $this->messenger->addWarning(t('Unable to proceed, please try again. If the problem persists, please contact an administrator. Error code: E4DC38E1-@line-@time', ['@line' => __LINE__, '@time' => time()]));
      }

      try {
        if ($delete) {
          $other_cart->delete();
        } else {
          $other_cart->save();
        }
      } catch (\Exception $error) {
        $this->loggerFactory->get('checkout_flow_checkout_form')->alert(t('@err', ['@err' => $error->__toString()]));
        $this->messenger->addWarning(t('Unable to proceed, please try again. If the problem persists, please contact an administrator. Error code: E4DC38E1-@line-@time', ['@line' => __LINE__, '@time' => time()]));
      }
    }
  }

  /**
   * Get List price based on SKU
   *
   * @param $modelNumber
   *
   * @return array
   */
  public function getProductListPrice($modelNumber) {
    $listPriceDetails = [];
    $termCollectionName = "";
    try {
      $variation = $this->customEntityTypeManager
        ->getStorage('commerce_product_variation')
        ->loadByProperties(['sku' => $modelNumber]);
    } catch (\Exception $error) {
      $this->loggerFactory->get('checkout_flow_checkout_form')->alert(t('@err', ['@err' => $error->__toString()]));
      $this->messenger->addWarning(t('Unable to proceed, please try again. If the problem persists, please contact an administrator. Error code: E4DC38E1-@line-@time', ['@line' => __LINE__, '@time' => time()]));
    }
    \Drupal::logger('shipping-variation-load')->notice(json_encode($variation));
    if(!empty($variation)) {
      foreach ($variation as $key => $value) {
        $variationDetails = $value->toArray();
        if(!empty($variationDetails['product_id'][0]['target_id'])) {
          $productDetails = Product::load($variationDetails['product_id'][0]['target_id'])->toArray();
          \Drupal::logger('shipping-product-load')->notice(json_encode($variation));

          if(!empty($productDetails['field_collection'][0]['target_id'])) {
            $termCollectionName = $this->entityTermManager->load($productDetails['field_collection'][0]['target_id'])->getName();
            \Drupal::logger('shipping-term-load')->notice(json_encode($variation));
          }
          $listPriceDetails['recertified'] = $productDetails['field_refurbished_product'][0]['value'];
          $listPriceDetails['collection'] = $termCollectionName;
        }
      }
      if(!empty($variationDetails)) {
        $listPriceDetails['listprice'] = ($variationDetails['list_price'][0]['number']) ? $variationDetails['list_price'][0]['number'] : "";
      }
    }
    return $listPriceDetails;
  }
}

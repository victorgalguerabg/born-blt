<?php

namespace Drupal\checkout_services\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * An API Config controller.
 */
class CheckoutServicesApiConfigForm extends ConfigFormBase {

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs a new DFApiSettingsForm object.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    // parent::__construct($config_factory);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'checkout_services.checkout_configs',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'checkout_flow_configs';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('checkout_services.checkout_configs');

    $form['domain_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Domain Url'),
      '#description' => $this->t('Domain URL of the API server.'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('domain_url'),
    ];

    $form['site_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Site Name'),
      '#description' => $this->t('List the products based on the Site Name.'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('site_name'),
    ];

    $form['delta_api_username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Service Account Username '),
      '#description' => $this->t('Enter the Delta API username for the Drupal Service Account.'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('delta_api_username'),
    ];
    $form['delta_api_password'] = [
      '#type' => 'password',
      '#title' => $this->t('API Service Account Password'),
      '#description' => $this->t(' Password for user API service authentication. If you have already entered your password before,
      you should leave this field blank, unless you want to change the stored password.
      Please note that this password will be stored as plain-text inside Drupal\'s core configuration variables.'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('delta_api_password'),
    ];

    $form['source'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Source Name'),
      '#description' => $this->t('Source Name.'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('source'),
    ];

    $form['store_type'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Store Type'),
      '#description' => $this->t('Store Type Name'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('store_type'),
    ];

    $form['companyNumber'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Company Number'),
      '#description' => $this->t('Company Number.'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('companyNumber'),
    ];

    $form['carrier'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Carrier'),
      '#description' => $this->t('Carrier.'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('carrier'),
    ];

    $form['shiptoNumber'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Ship to Number'),
      '#description' => $this->t('Ship to Number.'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('shiptoNumber'),
    ];

    $form['documentType'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Document Type'),
      '#description' => $this->t('Document Type for API Call.'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('documentType'),
    ];

    $form['noticeHead'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Notice Heading'),
      '#description' => $this->t('Notice Heading'),
      '#default_value' => $config->get('noticeHead'),
    ];

    $form['noticeMsg'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Notice Message'),
      '#description' => $this->t('Notice Message'),
      '#default_value' => $config->get('noticeMsg'),
    ];

    $form['emailSubscriptionConfigs'] = [
      '#type' => 'details',
      '#title' => $this->t('Email Subscription Configs to Salesforce'),
    ];

    $form['emailSubscriptionConfigs']['recordType'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Record Type'),
      '#description' => $this->t('Record Type.'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('recordType'),
    ];

    $form['emailSubscriptionConfigs']['emailOrigin'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Email Origin'),
      '#description' => $this->t('Email Origin'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('emailOrigin'),
    ];

    $form['emailSubscriptionConfigs']['subscriberStatus'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Subscriber Status'),
      '#description' => $this->t('Subscriber Status'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('subscriberStatus'),
    ];

    $form['emailSubscriptionConfigs']['agency'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Agency'),
      '#description' => $this->t('Agency'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('agency'),
    ];

    $form['shippingOptions'] = [
      '#type' => 'details',
      '#title' => $this->t('Shipping Options'),
    ];

    $form['shippingOptions']['ground'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Ground Price'),
      '#description' => $this->t('Ground'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('ground'),
    ];

    $form['shippingOptions']['priority'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Priority price'),
      '#description' => $this->t('Priority.'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('priority'),
    ];

    $form['shippingOptions']['shipping_info'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Shipping Information'),
      '#description' => $this->t('Shipping Address Information'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('shippingInfo'),
    ];

    $form['shippingOptions']['cgr'] = [
      '#type' => 'textfield',
      '#title' => $this->t('CGR(Ground)'),
      '#description' => $this->t('CGR display name in checkout payment page'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('cgr'),
    ];

    $form['shippingOptions']['cnd'] = [
      '#type' => 'textfield',
      '#title' => $this->t('CND(Priority)'),
      '#description' => $this->t('CND display name in checkout payment page'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('cnd'),
    ];

    $form['shippingApi'] = [
      '#type' => 'details',
      '#title' => $this->t('Shipping APIs'),
    ];

    $form['shippingApi']['orderItemValidationApi'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Order Item Validation API'),
      '#description' => $this->t('Order Item Validation API.'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('orderItemValidationApi'),
    ];

    $form['shippingApi']['allOrderItemValidationApi'] = [
      '#type' => 'textfield',
      '#title' => $this->t('All Order Item Validation API'),
      '#description' => $this->t('All Order Item Validation API.'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('allOrderItemValidationApi'),
    ];

    $form['paymentApi'] = [
      '#type' => 'details',
      '#title' => $this->t('Payment API'),
    ];

    $form['paymentApi']['paymentAuthorizeApi'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Payment Authorize API'),
      '#description' => $this->t('Payment Authorize API.'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('paymentAuthorizeApi'),
    ];

    $form['paymentApi']['orderConformationApi'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Order Conformation API'),
      '#description' => $this->t('Order Conformation API.'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('orderConformationApi'),
    ];

    $form['orderStatus'] = [
      '#type' => 'details',
      '#title' => $this->t('Order Status'),
    ];

    $form['orderStatus']['orderStatus'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Order Status API'),
      '#description' => $this->t('Order Status API.'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('orderStatus'),
    ];

    $form['orderStatus']['orderDetails'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Order Details API'),
      '#description' => $this->t('Order Details API.'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('orderDetails'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $this->config('checkout_services.checkout_configs')
      ->set('domain_url', $form_state->getValue('domain_url'))
      ->set('site_name', $form_state->getValue('site_name'))
      ->set('delta_api_username', $form_state->getValue('delta_api_username'))
      ->set('companyNumber', $form_state->getValue('companyNumber'))
      ->set('carrier', $form_state->getValue('carrier'))
      ->set('shiptoNumber', $form_state->getValue('shiptoNumber'))
      ->set('store_type', $form_state->getValue('store_type'))
      ->set('documentType', $form_state->getValue('documentType'))
      ->set('noticeMsg', $form_state->getValue('noticeMsg'))
      ->set('noticeHead', $form_state->getValue('noticeHead'))
      ->set('source', $form_state->getValue('source'))
      ->set('orderItemValidationApi', $form_state->getValue('orderItemValidationApi'))
      ->set('allOrderItemValidationApi', $form_state->getValue('allOrderItemValidationApi'))
      ->set('paymentAuthorizeApi', $form_state->getValue('paymentAuthorizeApi'))
      ->set('orderConformationApi', $form_state->getValue('orderConformationApi'))
      ->set('orderStatus', $form_state->getValue('orderStatus'))
      ->set('orderDetails', $form_state->getValue('orderDetails'))
      ->set('ground', $form_state->getValue('ground'))
      ->set('priority', $form_state->getValue('priority'))
      ->set('shippingInfo', $form_state->getValue('shipping_info'))
      ->set('recordType', $form_state->getValue('recordType'))
      ->set('emailOrigin', $form_state->getValue('emailOrigin'))
      ->set('subscriberStatus', $form_state->getValue('subscriberStatus'))
      ->set('agency', $form_state->getValue('agency'))
      ->set('cgr', $form_state->getValue('cgr'))
      ->set('cnd', $form_state->getValue('cnd'))
      ->save();

      if (strlen(trim($form_state->getValue('delta_api_password'))) > 0) {
        $this->config('checkout_services.checkout_configs')
          ->set('delta_api_password', $form_state->getValue('delta_api_password'))
          ->save();
      }
  }
}

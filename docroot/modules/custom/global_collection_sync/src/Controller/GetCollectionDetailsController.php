<?php

namespace Drupal\global_collection_sync\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\delta_services\DeltaService;
use Drupal\delta_cache\DeltaCacheService;
use Drupal\Component\Serialization\Json;

/**
 * Class GetCollectionDetailsController.
 */
class GetCollectionDetailsController extends ControllerBase {

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Symfony\Component\HttpFoundation\RequestStack definition.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The default cache bin.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * Drupal\delta_services\DeltaService definition.
   *
   * @var \Drupal\delta_services\DeltaService
   */
  protected $deltaservice;

  /**
   * Drupal\delta_cache\DeltaCacheService definition.
   *
   * @var \Drupal\delta_cache\DeltaCacheService
   */
  protected $deltaCache;

  /**
   * Constructs a new GetCollectionDetailsController object.
   *
   * @param Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   The default cache bin.
   * @param \Drupal\delta_services\DeltaService $deltaservice
   *   The deltaservice to make API calls.
   */
  public function __construct(ConfigFactoryInterface $config_factory,
                              RequestStack $request_stack,
                              DeltaService $deltaservice,
                              DeltaCacheService $deltaCache) {
    $this->configFactory = $config_factory;
    $this->requestStack = $request_stack;
    $this->deltaservice = $deltaservice;
    $this->deltaCache = $deltaCache;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('request_stack'),
      $container->get('delta_services.service'),
      $container->get('delta_cache.cache_service')
    );
  }

  /**
   * Get collection details from api based on Category, Collection Name, size and pagenumber.
   *
   * @return JSON
   *   A properly formatted json response from api.
   */
  public function getCollectionDetails() {
    $response = new Response();
    $sort = '';
    $finishFacet = '';
    $featureFacet = '';
    $flowRateFacet = '';
    $showerSys = '';
    $cdpConfig = $this->configFactory->get('global_sync.api_settings');
    $prop_config = $this->configFactory->get('product_details.settings');
    $discontinue_text = $prop_config->get('rest_api_settings.discontinue_ribbontext');
    $coming_soon_text = $prop_config->get('rest_api_settings.comming_soon_ribbontext');
    $queryString = $this->requestStack->getCurrentRequest()->query->all();
    if (array_key_exists('sub_category', $queryString)) {
      $collectionSubCategory = $queryString['sub_category'];
    }
    else{
      $collectionSubCategory = '';
    }
     $collectionCategory = $queryString['category'];
     $collectionName  = $queryString['collection'];
     $setDefaultModelFlag = 1;
     $apiBaseDetails = $this->configFactory->get('delta_services.deltaservicesconfig');
     $brand = $apiBaseDetails->get('global_site_name');
     $haystackStrPieces = $cdpConfig->get('skip_default_model');
     //dbrizo-34
     //SKIP Appending defaultModel=true for specific CDPs
     //$haystackStrPieces = $this->testSkipDefaultModelAttr();
     //Fix for brizo only
     if((!empty($haystackStrPieces)) && strtolower($brand) == 'brizo'){
       $haystackRaw = explode(',', $haystackStrPieces);
       if(is_array($haystackRaw) && count($haystackRaw) > 0){
        $haystackLRTrim = array_map('trim', $haystackRaw);
        $haystack = array_map('strtolower', $haystackLRTrim);
        //concatinate category params and collection param ,Seprator:_
        //brizo_bath_jason wu for brizo™
        $needle = strtolower($collectionCategory . '_' .  $collectionName); 
        if(is_array($haystack) && in_array($needle, $haystack)){
          $setDefaultModelFlag = 0;
        }
       }
     }
    $pagenumber = $queryString['pagenumber'];
    $size = $queryString['size'];
    if (!$size) {
      $size = $cdpConfig->get('collection_detail_items_per_page');
    }
    $variables['#attached']['drupalSettings']['collectionProductsPerPage'] = $size;
    $cache = $queryString['cache'];


    if (array_key_exists('facets', $queryString)) {
      $finishFacet = $queryString['facets'];
    }
    if (array_key_exists('features', $queryString)) {
      $featureFacet = $queryString['features'];
    }
    if (array_key_exists('flowrates', $queryString)) {
      $flowRateFacet = $queryString['flowrates'];
    }
    if (array_key_exists('showersys', $queryString)) {
      $showerSys = $queryString['showersys'];
    }


    if(array_key_exists('sort', $queryString)){
      $sort =  $queryString['sort'] ;
    }
    $cacheId = $this->getCid($queryString);
    if ($cache == 'T') {
      $cachedResult = $this->deltaCache->deltaGetCache($cacheId);
      if ($cachedResult) {
        $result = $cachedResult;
        $response->setContent($result['response']);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
      }
    }

    $placeholders = ['{category}', '{collection}','{pagenumber}', '{size}'];
    $replacements = [$collectionCategory, $collectionName, $pagenumber, $size];

    $collectionDetailApiArgs = str_replace(
      $placeholders,
      $replacements,
      $cdpConfig->get('collection_detail_api')
    );
    if (!array_key_exists('narrow_result', $queryString) || $queryString['narrow_result'] == '') {
      $collectionDetailApiArgs = str_replace(
        '&includeObsolete=true',
        '',
        $collectionDetailApiArgs
      );
    }
    $collectionDetailAPIUrl = $apiBaseDetails->get('api_base_path');
    if(is_array($collectionSubCategory)){
      foreach ($collectionSubCategory as $filterCat){
        $collectionDetailApiArgs .= '&or_categories=' . rawurlencode($filterCat);
      }

    }elseif (is_string($collectionSubCategory) && $collectionSubCategory != ''){
      $collectionDetailApiArgs .= '&and_categories=' . $collectionSubCategory;
    }
    if ($finishFacet){
      if(is_array($finishFacet)){
        foreach ($finishFacet as $singleFacet){
          $collectionDetailApiArgs .= '&facetFinish=' . rawurlencode($singleFacet);
        }
      }else{

        $collectionDetailApiArgs .= '&facetFinish=' . $finishFacet;
      }
    }
    if ($featureFacet){
     // $collectionDetailAPIUrl .= "facet_facetFeature=true&";
      if(is_array($featureFacet)){
        foreach ($featureFacet as $singleFacet){
          $collectionDetailApiArgs .= '&facetFeature=' . rawurlencode($singleFacet);
        }
      }else{
        $collectionDetailApiArgs .= '&facetFeature=' . $featureFacet;
      }
    }
    if ($flowRateFacet){
       if(is_array($flowRateFacet)){
         foreach ($flowRateFacet as $singleFacet){
           $collectionDetailApiArgs .= '&FlowRate=' . rawurlencode($singleFacet);
         }
       }else{
         $collectionDetailApiArgs .= '&FlowRate=' . $flowRateFacet;
       }
     }
    if ($showerSys){
      if(is_array($showerSys)){
        foreach ($showerSys as $singleFacet){
          //$collectionDetailApiArgs .= '&FlowRate=' . rawurlencode($singleFacet);
          $collectionDetailApiArgs .= '&' . str_replace("_", "=", $singleFacet);
        }
      } else {
        $collectionDetailApiArgs .= '&' . str_replace("_", "=", $showerSys);
      }
    }
    if(empty($finishFacet) && $setDefaultModelFlag == 1){
      $collectionDetailApiArgs .= '&defaultModel=true';
    }
    $collectionDetailAPIUrl .= $collectionDetailApiArgs . '&' . $sort;
   //facet_collections=true&facet_facetNarrowResults=true&facet_facetBrizoFeature=true&facet_facetFinish=true&facet_categories=true&categories=Brizo_Bath_Tub/Shower Packages_Pressure Balance Shower Only&&facetBrizoFeature=ADA&
    $result = $this->deltaservice->apiCall($collectionDetailAPIUrl, "GET", [], [], "CDP");
    if ($result['status'] == 'SUCCESS') {
      $this->deltaCache->deltaSetCache($cacheId, $result, 3600);
      $readJsonArr = json_decode($result['response'], TRUE);
      if (count($readJsonArr)) {

        $result = ['response' => json_encode($readJsonArr)];
      }
      $response->setContent($result['response']);
      $response->headers->set('Content-Type', 'application/json');
    }
    else {
      $response->setContent('API Response - ERROR');
    }
    return $response;
  }
  
  // Sample data for testing purpose.
  public function testSkipDefaultModelAttr(){
    return 'brizo_bath_Jason Wu for Brizo™, brizo_kitchen_Jason Wu for Brizo™';
  }

  /**
   * Get collection finish facets from api based on Category, Collection Name, size and pagenumber.
   *
   * @return JSON
   *   A properly formatted json response from api.
   */
  function getCollectionFinishFacets(){
    $response = new Response();
    $cdpConfig = $this->configFactory->get('global_sync.api_settings');
    $queryString = $this->requestStack->getCurrentRequest()->query->all();
    if ($queryString['sub_category']) {
      $collectionSubCategory = $queryString['sub_category'];
    }
    else{
      $collectionSubCategory = '';
    }
    $collectionCategory = $queryString['category'];
    $collectionName  = $queryString['collection'];
    $pagenumber = $queryString['pagenumber'];
    $size = $cdpConfig->get('collection_detail_items_per_page');
    $variables['#attached']['drupalSettings']['collectionProductsPerPage'] = $size;
    $cache = $queryString['cache'];
    if(array_key_exists('sort', $queryString)){
      $sort =  $queryString['sort'] ;
    }

    $cacheId = $this->getCid($queryString) . '&finishfacets';
    /*if ($cache == 'T') {
      $cachedResult = $this->deltaCache->deltaGetCache($cacheId);
      if ($cachedResult) {
        $result = $cachedResult;
        $response->setContent($result['response']);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
      }
    }*/

    $placeholders = ['{category}', '{collection}', '{size}'];
    $replacements = [$collectionCategory, $collectionName, $size];
    $collectionDetailApiArgs = str_replace(
      $placeholders,
      $replacements,
      $cdpConfig->get('collection_detail_finish_api')
    );
    $apiBaseDetails = $this->configFactory
     ->get('delta_services.deltaservicesconfig');
    $collectionFacetAPIUrl = $apiBaseDetails->get('api_base_path');
    if(is_array($collectionSubCategory)){
      foreach ($collectionSubCategory as $filterCat){
        $collectionFacetAPIUrl .= $collectionDetailApiArgs . '&or_categories=' . rawurlencode($filterCat);
      }

    }elseif (is_string($collectionSubCategory) && $collectionSubCategory != ''){
      $collectionFacetAPIUrl .= $collectionDetailApiArgs . '&and_categories=' . $collectionSubCategory;
    }else{
      $collectionFacetAPIUrl .= $collectionDetailApiArgs . '&' . $sort;
    }
    //print $collectionFacetAPIUrl;die;
    $result = $this->deltaservice->apiCall($collectionFacetAPIUrl, "GET", [], [], "CDP");
    $this->deltaCache->deltaSetCache($cacheId, $result, 3600);
    if ($result['status'] == 'SUCCESS') {
      $response->setContent($result['response']);
      $response->headers->set('Content-Type', 'application/json');
    }
    else {
      $response->setContent('API Response - ERROR');
    }
    return $response;

  }
  /**
   * {@inheritdoc}
   */
  public function getCid($queryString = []) {
    $str = '';
    foreach ($queryString as $key => $value){
      if(is_array($value)){
        $str .= implode('&',$value);
      }else{
        $str .= '&' . $value;
      }
    }
    return $str;
  }

}

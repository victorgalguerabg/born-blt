<?php

namespace Drupal\global_collection_sync\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\global_collection_sync\CollectionImportService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class CollectionImportForm.
 */
class CollectionSyncForm extends FormBase {

  /**
   * Drupal\delta_collection\CollectionImportService definition.
   *
   * @var \Drupal\global_collection_sync\CollectionImportService
   */
  protected $collectionService;

  /**
   * Class constructor.
   */
  public function __construct(CollectionImportService $collectionService) {
    $this->collectionService = $collectionService;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('global_collection_sync.import')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'collection_import_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $nid = '') {

    $config = $this->config('general.deltaservicesconfig');
    $delta_config = $this->configFactory->get('delta_services.deltaservicesconfig');
    $api = $delta_config->get('api_base_path');
    $form['choose'] = [
      '#type' => 'select',
      '#title' => $this->t('Import Collection'),
      '#required' => TRUE,
      '#options' => [
        'insert' => 'Import Collection',
      ],
      '#default_value' => ['insert'],
      '#prefix' => t('<strong>Delta API Collection Sync</strong>
       <br> This form is used to import collection information from the Delta API.
       <br> Current API Endpoint: ') . $api . t('
       <br> Select "Import Collection" to import collection information into the terms in
       <a href="/admin/structure/taxonomy/manage/collections/overview">Collections vocabulary</a>.
        This will add collection data if they don\'t exist. This will not remove any collection.'),
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#id' => 'import_category',
      '#value' => $this->t('Execute'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $action = $form_state->getValue('choose');
    if ($action == 'insert') {
      $this->collectionService->importCollection();
    }
  }

}

<?php

namespace Drupal\delta_collection_sync\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class CDPConfigForm.
 */
class CDPConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'delta_services.cdpconfig',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'cdp_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('delta_services.cdpconfig');
    $form['collection_detail_finish_api'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Collection Detail page Finish facet API'),
      '#description' => $this->t('Collection Detail API details for Finish excluding basepath.'),
      '#default_value' => $config->get('collection_detail_finish_api'),
    ];
    $form['collection_detail_api'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Collection Detail API'),
      '#description' => $this->t('Collection Detail API details excluding basepath.'),
      '#default_value' => $config->get('collection_detail_api'),
    ];
    $form['collection_detail_items_per_page'] = [
      '#type' => 'number',
      '#title' => $this->t('Items per Page'),
      '#description' => $this->t('Number of items per page in collection detail page.'),
      '#default_value' => $config->get('collection_detail_items_per_page'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('delta_services.cdpconfig')
    ->set('collection_detail_finish_api', $form_state->getValue('collection_detail_finish_api'))
      ->set('collection_detail_api', $form_state->getValue('collection_detail_api'))
      ->set('collection_detail_items_per_page', $form_state->getValue('collection_detail_items_per_page'))
      ->save();
  }

}

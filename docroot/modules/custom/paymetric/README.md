# Paymetric Module Configuration

## Integration Guide

```/modules/custom/paymetric/assets/XiIntercept_eCommerce_Integration_Guide.pdf```

## Settings

<b>Path</b>

`````/admin/config/commerc/paymetric`````

#### Required Values
The following values are required for the Access Token and Response Packet requests

<b>MerchantGuid</b><br />
Specific to project

<b>SessionRequestType</b><br />
"1" for iFrame

<b>MerchantDevelopmentEnvironment</b><br />
"php" for a drupal installation

<b>Shared key</b>
<br />Used to sign the Packet XML to obtain the Access Token and then to sign the Access Token to
the Response Packet.

<b>Endpoint</b>

Dev/QA<br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;https://qaapp02.XiSecurenet.com/DIeComm/AccessToken<br />
Production<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;https://prdapp02.XiSecurenet.com/DIeComm/AccessToken


<b>Host URL</b>
The host name of the machine calling the paymetric api

<b>Packet XML</b>

See API documentation for more specifics.  Here's an example of a packet being used. For best results, the packet should be compressed.

```<?xml version="1.0" encoding="UTF-8"?><merchantHtmlPacketModel xmlns="Paymetric:XiIntercept:MerchantHtmlPacketModel" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><iFramePacket><hostUri>https://paymetric.dd/</hostUri><cssUri>https://qaapp02.xisecurenet.com/diecomm/Content/IFrameStyleSheet.css</cssUri></iFramePacket><merchantHtml><htmlSection class="merchant_paycontent"><cardDropdownSection><tag name="div"><label for="cardType" text="card type" /><ddlCardType id="cd"><items><item for="mastercard" /><item for="visa" /></items></ddlCardType></tag><tag name="div"><label for="cardholderName" text="Name on card"/><tboxCardHolderName class="xi-long-text"/><validationMsg for="cardholderName" class="valmsg"/></tag><tag name="div"><label for="cardNumber" text="card number" /><tboxCardNumber tokenize="true" /><validationMsg for="cardNumber" class="valmsg" /></tag><tag name="div"><label for="startMonth" text="start date" /><ddlStartMonth default-text="month" display-format="MMM" class="merchant_combos" required="false" /><ddlStartYear default-text="year" class="merchant_combos" years-to-display="10" required="false" start-date="true" /><validationMsg for="startYear" class="valmsg" /></tag><tag name="div"><label for="expMonth" text="exp date" /><ddlExpMonth default-text="month" class="merchant_combos" required="false" /><ddlExpYear default-text="year" class="merchant_combos" years-to-display="10" required="false" exp-date="true" /><validationMsg for="expYear" class="valmsg" /></tag><tag name="div"><label for="cvv" text="Card cvv:"/><tboxCvv class="xi-short-text"/><validationMsg for="cvv" class="valmsg"/></tag></cardDropdownSection></htmlSection></merchantHtml></merchantHtmlPacketModel>```


Here's a formatted version of the same packet.

```/modules/custom/paymetric/assets/sample_packet.xml```
   
   
## Checkout Sample Page

```/paymetric/checkout```

## Paymetric API Drupal service

```paymetric.token```

## Access Token REST Service
Returns the Access Token in a json string

```/paymetric/access_token```

```
{
     "access_token": "{access_token}"
}
```

## Response Packet REST Service
Returns the Response Packet as XML

```/paymetric/response_packet/{access_token}```




<?php
namespace Drupal\paymetric;
use Drupal\Core\Config\ConfigFactory;
/**
 * Class PaymetricTokenService.
 */
class PaymetricTokenService implements PaymetricTokenServiceInterface {
  private $guid;
  private $session_request_type;
  private $merchant_development_environment;
  private $shared_key;
  private $endpoint;
  private $host_url;
  private $packet_xml;
  /**
   * Constructs a new PaymetricTokenService object.
   */
  public function __construct(ConfigFactory $configFactory) {
    $paymetric_config = $configFactory->get('paymetric.settings');
    $this->guid = $paymetric_config->get('paymetric_guid');
    $this->session_request_type = $paymetric_config->get('paymetric_session_request_type');
    $this->merchant_development_environment = $paymetric_config->get('paymetric_merchant_development_environment');
    $this->shared_key = $paymetric_config->get('paymetric_shared_key');
    $this->endpoint = $paymetric_config->get('paymetric_endpoint');
    $this->host_url = $paymetric_config->get('paymetric_host_url');
    $this->packet_xml = $paymetric_config->get('paymetric_packet_xml');
  }
  public function getEndpoint() {
    return $this->endpoint;
  }
  public function getGuid() {
    return $this->guid;
  }
  private function signPacket($packet) {
    return base64_encode(hash_hmac('sha256', $packet, $this->shared_key,true));
  }
  public function getAccessToken() {
    $params = $this->getAccessTokenParams();
    $token_request = $this->submitAccessTokenRequest($params);
    $access_token = array_key_exists('AccessToken', $token_request) ? $token_request['AccessToken'] : '';
    return $access_token;
  }
  private function getAccessTokenParams() {
    return [
      "MerchantGuid" => $this->guid,
      "SessionRequestType" => $this->session_request_type,
      "Packet" => $this->packet_xml,
      "MerchantDevelopmentEnvironment" => $this->merchant_development_environment,
      "Signature" => $this->signPacket($this->packet_xml),
    ];
  }
  private function submitAccessTokenRequest($params) {
    $options = [
      'http' => [
        'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
        'method'  => 'POST',
        'content' => http_build_query($params)
      ]
    ];
    $context = stream_context_create($options);
    $result = file_get_contents($this->endpoint . '/DIeComm/AccessToken', false, $context);
    $parsed = $this->parseAccessTokenResult($result);
    return $parsed;
  }
  private function parseAccesstokenResult($result) {
    $p = xml_parser_create();
    xml_parse_into_struct($p, $result, $vals, $index);
    if (!array_key_exists('REQUESTERROR',$index)) {
      if (array_key_exists('ACCESSTOKEN',$index)) {
        $access_token_index = $vals[$index['ACCESSTOKEN'][0]]['value'];
        $signature_index = $vals[$index['SIGNATURE'][0]]['value'];
        return [
          'MerchantGuid' => $this->guid,
          'AccessToken' => $access_token_index,
          'Signature' => $signature_index,
        ];
      }
    } else {
      $error_message = $vals[$index['MESSAGE'][0]];
      return [
        "ErrorMessage" => $error_message,
      ];
    }
    return null;
  }
  public function getResponsePacket($access_token) {
    $params = $this->getResponsePacketParams($access_token);
    $a=1;
    $response_packet = $this->submitResponsePacketRequest($params);
    return $response_packet;
  }
  private function getResponsePacketParams($access_token) {
    return [
      "MerchantGUID" => $this->guid,
      "Signature" => $this->signPacket($access_token),
      "AccessToken" => $access_token,
    ];
  }
  private function submitResponsePacketRequest($params) {
    $url = $this->endpoint . "/DIeComm/ResponsePacket?MerchantGUID=" . $params['MerchantGUID'] . "&Signature=" . urlencode($params['Signature']) . "&AccessToken=" .  $params['AccessToken'];
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, [
      'Content-Type: application/x-www-form-urlencoded',
      'Cookie: PMPRSTTCKT=!N1KnhgZ29eZD9UF0zm3oec8PLsnWciQfwWWvYy/K7aTVbzNrkuM+6+V24TmjJ6pc2at+oYq7nyYFr9k=',
    ]);
    $body = [];
    $body = http_build_query($body);
    // set body
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
    $response = curl_exec($ch);
    curl_close($ch);
    //$result = file_get_contents($url);
    return $response;
  }
}
<?php

namespace Drupal\paymetric\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\paymetric\PaymetricTokenServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;
/**
 * Defines HelloController class.
 */
class PaymetricResponsePacketController extends ControllerBase {

  /**
   * Drupal\paymetric\PaymetricTokenServiceInterface definition.
   *
   * @var \Drupal\paymetric\PaymetricTokenServiceInterface
   */

  protected $paymetric_token_service;

  /**
   * Constructs a new PaymetricCheckoutForm object.
   */
  public function __construct(
    PaymetricTokenServiceInterface $paymetric_token_service
  ) {
    $this->paymetric_token_service = $paymetric_token_service;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('paymetric.token')
    );
  }


  /**
   * Display the markup.
   *
   * @return array
   *   Return markup array.
   */
  public function getResponsePacket($access_token) {
    $response_packet = $this->paymetric_token_service->getResponsePacket($access_token);
    $response = new Response();
    $response->setContent($response_packet);
    $response->headers->set('Content-Type', 'text/xml');

    return $response;

  }
}
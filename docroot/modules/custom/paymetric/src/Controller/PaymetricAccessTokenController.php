<?php

namespace Drupal\paymetric\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\paymetric\PaymetricTokenServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Defines HelloController class.
 */
class PaymetricAccessTokenController extends ControllerBase {

  /**
   * Drupal\paymetric\PaymetricTokenServiceInterface definition.
   *
   * @var \Drupal\paymetric\PaymetricTokenServiceInterface
   */

  protected $paymetric_token_service;

  /**
   * Constructs a new PaymetricCheckoutForm object.
   */
  public function __construct(
    PaymetricTokenServiceInterface $paymetric_token_service
  ) {
    $this->paymetric_token_service = $paymetric_token_service;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('paymetric.token')
    );
  }


  /**
   * Display the markup.
   *
   * @return array
   *   Return markup array.
   */
  public function getAccessToken() {

    $access_token = $this->paymetric_token_service->getAccessToken();

    $json_array = ['access_token' => $access_token];

    return new JsonResponse($json_array);

  }
}
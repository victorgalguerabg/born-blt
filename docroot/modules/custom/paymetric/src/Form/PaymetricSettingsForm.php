<?php

namespace Drupal\paymetric\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class PaymetricSettingsForm.
 */
class PaymetricSettingsForm extends ConfigFormBase {
  /** @var string Config settings */
  const SETTINGS = 'paymetric.settings';

  /** @var MessengerInterface Config settings */
  protected $messenger;

  public function __construct(
    MessengerInterface $messenger
  ) {
    $this->messenger = $messenger;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'paymetric_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);

    $form['paymetric_guid'] = [
      '#type' => 'textfield',
      '#title' => $this->t('MerchantGuid'),
      '#description' => $this->t('The Global Unique Identifier assigned by Paymetric.'),
      '#default_value' => $config->get('paymetric_guid'),
    ];

    $form['paymetric_session_request_type'] = [
      '#type' => 'textfield',
      '#title' => $this->t('SessionRequestType'),
      '#description' => $this->t('Indicates the integration type.'),
      '#default_value' => $config->get('paymetric_session_request_type'),
    ];

    $form['paymetric_merchant_development_environment'] = [
      '#type' => 'textfield',
      '#title' => $this->t('MerchantDevelopmentEnvironment'),
      '#description' => $this->t('Indicate the language in which you are coding your integration to XiIntercept eComm, for example, asp.net or php.'),
      '#default_value' => $config->get('paymetric_merchant_development_environment'),
    ];

    $form['paymetric_shared_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Shared key'),
      '#description' => $this->t('Indicate the language in which you are coding your integration to XiIntercept eComm, for example, asp.net or php.'),
      '#default_value' => $config->get('paymetric_shared_key'),
    ];

    $form['paymetric_endpoint'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Endpoint'),
      '#description' => $this->t('Endpoint for submitting api requests'),
      '#default_value' => $config->get('paymetric_endpoint'),
    ];

    $form['paymetric_host_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Host URL'),
      '#description' => $this->t('URL for host sending request.'),
      '#default_value' => $config->get('paymetric_host_url'),
    ];

    $form['paymetric_packet_xml'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Packet'),
      '#default_value' => $config->get('paymetric_packet_xml'),
    ];

    $form['test'] = [
      '#type' => 'button',
      '#value' => $this->t('Test'),
      '#attributes' => [
        'onclick' => 'return false;'
      ],
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Retrieve the configuration
    $this->configFactory->getEditable(static::SETTINGS)
      // Set the submitted configuration setting
      ->set('paymetric_guid', $form_state->getValue('paymetric_guid'))
      ->set('paymetric_session_request_type', $form_state->getValue('paymetric_session_request_type'))
      ->set('paymetric_merchant_development_environment', $form_state->getValue('paymetric_merchant_development_environment'))
      ->set('paymetric_shared_key', $form_state->getValue('paymetric_shared_key'))
      ->set('paymetric_endpoint', $form_state->getValue('paymetric_endpoint'))
      ->set('paymetric_host_url', $form_state->getValue('paymetric_host_url'))
      ->set('paymetric_packet_xml', $form_state->getValue('paymetric_packet_xml'))
      ->save();

    parent::submitForm($form, $form_state);
    // Display result.
    foreach ($form_state->getValues() as $key => $value) {
      $this->messenger->addStatus($key . ': ' . $value);
    }

  }

}

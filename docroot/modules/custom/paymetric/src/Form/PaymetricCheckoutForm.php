<?php

namespace Drupal\paymetric\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\paymetric\PaymetricTokenServiceInterface;
use Drupal\Core\Messenger\MessengerInterface;

/**
 * Class PaymetricCheckoutForm.
 */
class PaymetricCheckoutForm extends FormBase {

  /**
   * Drupal\paymetric\PaymetricTokenServiceInterface definition.
   *
   * @var \Drupal\paymetric\PaymetricTokenServiceInterface
   */

  protected $paymetric_token_service;
  protected $access_token;

  /**
   * Drupal\Core\Messenger\MessengerInterface definition.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Constructs a new PaymetricCheckoutForm object.
   */
  public function __construct(
    PaymetricTokenServiceInterface $paymetric_token_service, MessengerInterface $messenger
  ) {
    $this->paymetric_token_service = $paymetric_token_service;
    $this->messenger = $messenger;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('paymetric.token'),
      $container->get('messenger')
    );
  }


  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'paymetric_checkout_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['#attached']['library'][] = 'paymetric/paymetric';

    $values = $form_state->getValues();

    $this->access_token = !empty($values['access_token']) ? $values['access_token'] : $this->access_token = $this->paymetric_token_service->getAccessToken();

    $form['hidden']['access_token'] = [
      '#type' => 'hidden',
      '#title' => $this->t('Access Token'),
      '#value' => $this->access_token,
      '#attributes' => [
        'disabled' => 'disabled',
        'id' => 'edit-access-token',
      ]
    ];

    $title = 'Token Details:';
    $form['token'] = [
      '#type' => 'fieldset',
      '#title' => $title,
    ];

    $table_prefix = "<table>";
    $table_suffix = "</table>";
    $iframe_url = $this->paymetric_token_service->getEndpoint() . '/diecomm/View/Iframe/' . $this->paymetric_token_service->getGuid() . '/' . $this->access_token . '/True';
    $iframe_html = '<iframe name="dieCommFrame" src="' . $iframe_url . '" style="width: 100%; overflow: hidden; height: 200px;" scrolling="no" frameborder="0" id="IFrame"></iframe>';

    $token_output = '';
    $token_output .= "<tr><td/><b>Accesstoken" . ":</b></td><td>" .  $this->access_token . "</td></tr>";
    $token_output .= "<tr><td/><b>iFrame URL" . ":</b></td><td>" .  $iframe_url . "</td></tr>";
    $token_output .= "<tr><td/><b>iFrame HTML" . ":</b></td><td>" .  htmlspecialchars($iframe_html) . "</td></tr>";
    if (!empty($token_output)) {
      $token_output = $table_prefix . $token_output . $table_suffix;
    }

    $form['token']['render'] = [
      '#type' => 'markup',
      '#markup' => $token_output,
    ];

    $form['render']['iframe'] = [
      '#type' => 'markup',
      '#markup' => $iframe_html,
      '#allowed_tags' => ['iframe'],
      '#suffix' => '<br />'
    ];

    $form['submit'] = [
      '#type' => 'html_tag',
      '#tag' => 'input',
      '#attributes' => [
        'type' => 'button',
        'value' => t("Submit"),
        'class' => 'button',
        'name' => 'edit-submit-button',
        'id' => 'edit-submit-button'
      ],
    ];


    return $form;
  }
  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state-> setRebuild();
  }

}

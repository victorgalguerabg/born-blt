<?php

namespace Drupal\paymetric;

/**
 * Interface PaymetricTokenServiceInterface.
 */
interface PaymetricTokenServiceInterface {

  public function getGuid();

  public function getAccessToken();

  public function getEndpoint();

  public function getResponsePacket($access_token);

}

Drupal.behaviors.paymetric_validate = {
  attach: function (context, settings) {

    // Attach a click listener to the clear button.
    var submitBtn = document.getElementById('edit-submit-button');
    submitBtn.addEventListener('click', function() {
      jQuery(document).ready(function($){
        var iframeUrl = $('#IFrame').attr('src');
        var access_token = $('#edit-access-token').val();
        console.log('targetUrl: ' + iframeUrl );
        console.log('Access Token: ' + access_token);
        if (iframeUrl) {
          $XIFrame.submit({
            autosizeheight: true,
            iFrameId: 'dieCommFrame',
            targetUrl: iframeUrl,
            onSuccess: function(msg) {
              var message = JSON.parse(msg);
              if (message && message.data.HasPassed) {
                // all good, send the form:
                $.ajax({
                  url: "/paymetric/response_packet/" + access_token,
                  contentType: "application/xml",
                  dataType: 'xml',
                  success: function(result){
                    console.log(result);
                  },
                  error: function(result){
                    console.log(result);
                  }
                });
              }
            },
            onError: function(msg) {
              // triggered by an error on paymetric's end
            }

          });
        }
      });
    }, false);
  }
};
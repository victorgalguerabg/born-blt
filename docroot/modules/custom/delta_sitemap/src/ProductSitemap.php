<?php
namespace Drupal\delta_sitemap;

use Drupal\Core\Entity;
use Drupal\node\Entity\Node;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Render;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Drupal\Component\Utility\Html;
use Drupal\simple_sitemap\Queue\QueueWorker;
use Drupal\delta_services\DeltaService;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Queue\QueueFactory;
use Drush\Commands\DrushCommands;



/**
 * Class ProductSitemap.
 *
 * @package Drupal\delta_sitemap
 */
class ProductSitemap {

  /**
   * @var \Drupal\delta_sitemap\Queue\QueueWorker
   */
  protected $queueWorker;


  protected $queueFactory;

  /**
   * Drupal\Core\Logger\LoggerChannelFactoryInterface definition.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */

  public static $loggerChannelFactory;


  /**
   * Simplesitemap constructor.
   *
   * @param \Drupal\delta_sitemap\Queue\QueueWorker $queue_worker
   */

   /**
    * Constructs a new GetProductsController object.
    *
    * @param \Drupal\delta_services\DeltaService $deltaservice
    *   The deltaservice to make API calls.
    */

    /**
     * Drupal\Core\Config\ConfigFactoryInterface definition.
     *
     * @var \Drupal\Core\Config\ConfigFactoryInterface
     */
    protected $configFactory;

  public function __construct(DeltaService $deltaservice, ConfigFactoryInterface $config_factory, QueueFactory $queue, LoggerChannelFactoryInterface $loggerChannel) {
    $this->deltaservice = $deltaservice;
    $this->configFactory = $config_factory;
    $this->queueFactory = $queue;
    self::$loggerChannelFactory = $loggerChannel;
  }

  /**
   * {@inheritdoc}
   */
  public function generate($form = 'queue') {
    $module_path = drupal_get_path('module', 'delta_sitemap');
    $filePath = \Drupal::service('file_system')->realpath(\Drupal::config('system.file')->get('default_scheme') . "://");
    $xmlFile = $filePath . '/product-sitemap.xml';
    $xml = simplexml_load_file($module_path . '/xsl/product-sitemap.xml');
    $xml->asXML($filePath. '/product-sitemap.xml');
    if($form == 'queue')
    {
      return $this->generateSiteMap();
    }
    else {
      $process = self::doBatchOperation();
      return $process;
    }

  }

  /**
   * {@inheritdoc}
   */

  public function generateSiteMap() {
    $apiBaseDetails = $this->configFactory->get('delta_services.deltaservicesconfig');
    $productAPIUrl = $apiBaseDetails->get('api_base_path');
    $sitemap_config = $this->configFactory->get('delta_sitemap.settings');
    $productAPIUrl = $productAPIUrl . $sitemap_config->get('delta_siemap_api_url');
    $size = $sitemap_config->get('delta_siemap_api_size');
    $productAPIUrl = str_replace('[size]' , $size, $productAPIUrl);
    $productAPIUrl = str_replace('[page]' , 0, $productAPIUrl);
    $productRequest = $this->deltaservice->apiCall($productAPIUrl, "GET", [], "", "ProductSitemap");
    if ($productRequest['status'] == 'SUCCESS' ) {
      $encodedResponse = json_decode($productRequest['response']);
      $queue = $this->queueFactory->get('generate_delta_sitemap');
      $totalItemsBefore = $queue->numberOfItems();
      $productItems = $encodedResponse->content;
      foreach ($productItems as $element) {
      // Create new queue item.
        $queue->createItem($element);
      }
      $totalPages = $encodedResponse->totalPages;
      for($page = 1 ; $page <= $totalPages ; $page++) {
        $productAPIUrl = $apiBaseDetails->get('api_base_path');
        $sitemap_config = $this->configFactory->get('delta_sitemap.settings');
        $productAPIUrl = $productAPIUrl . $sitemap_config->get('delta_siemap_api_url');
        $productAPIUrl = str_replace('[size]' , $size, $productAPIUrl);
        $productAPIUrl = str_replace('[page]' , $page, $productAPIUrl);
        $productRequest = $this->deltaservice->apiCall($productAPIUrl, "GET", [], "", "ProductSitemap");
        if ($productRequest['status'] == 'SUCCESS' ) {
          $encodedResponse = json_decode($productRequest['response']);
          $productItems = $encodedResponse->content;
          foreach ($productItems as $element) {
             // Create new queue item.
             $queue->createItem($element);
         }
        }
      }
      return $totalItemsAfter = $queue->numberOfItems();
    }
  }

  public static function doBatchOperation() {

    $process = self::getProducts();
    return $process;

  }
  public static function generateXML($items) {

    $host = \Drupal::request()->getSchemeAndHttpHost();
    $filePath = \Drupal::service('file_system')->realpath(\Drupal::config('system.file')->get('default_scheme') . "://");
    $module_path = drupal_get_path('module', 'delta_sitemap');
    $xmlFile = $filePath . '/product-sitemap.xml';
    $xml = simplexml_load_file($xmlFile);
    foreach($items as $product){
      $product_name = $product->name;
      $basecategory = [];
      $categories = $product->categories;
      $repairpart = $product->values->RepairPart;
      $sitefromcategory = [];
      $basecategory = [];
      $maincategoryfinal = "";
      $maincategoryurl = "";
      foreach ($categories as $urlalias) {
        $urlalias_seperate = explode("_", $urlalias);
        $sitefromcategory[] = $urlalias_seperate[0];
        if ($urlalias_seperate[0] == "Delta") {
          $basecategory[] = strtolower($urlalias_seperate[1]);
        }
      }
      if(!empty($sitefromcategory) && in_array("Delta", $sitefromcategory)){
        $basecategoryfinal = array_unique($basecategory);
        if ($repairpart == "T" && in_array("parts", $basecategoryfinal)) {
          $maincategoryfinal = "parts";
          $maincategoryurl = "parts-product-detail?modelNumber=";
        }
        elseif (in_array('kitchen', $basecategoryfinal)) {
          $maincategoryfinal = "kitchen";
          $maincategoryurl = "kitchen/product/";
        }
        elseif(in_array('bathroom', $basecategoryfinal)) {
          $maincategoryfinal =  "bathroom";
          $maincategoryurl = "bathroom/product/";
        }
      }
      elseif(empty($categories) && $repairpart == "T"){
        $maincategoryfinal = "parts";
        $maincategoryurl = "parts-product-detail?modelNumber=";
      }
      elseif (empty($categories) && $repairpart == "F"){
        $maincategoryfinal = "kitchen";
        $maincategoryurl = "kitchen/product/";
      }
      if($maincategoryurl != '') {
        $product_url = $host . '/' . $maincategoryurl . $product_name;
        $url = $xml->addChild('url');
        $url->addChild('loc',$product_url);
      }
    }
    $xml->asXML($filePath. '/product-sitemap.xml');
    return 1;
  }

  // Generate xml for brizo

  public static function generateBrizoXML($items) {

    $host = \Drupal::request()->getSchemeAndHttpHost();
    $filePath = \Drupal::service('file_system')->realpath(\Drupal::config('system.file')->get('default_scheme') . "://");
    $module_path = drupal_get_path('module', 'delta_sitemap');
    $xmlFile = $filePath . '/product-sitemap.xml';
    $xml = simplexml_load_file($xmlFile);
    foreach($items as $product){
      $product_name = $product->name;
      $basecategory = [];
      $categories = $product->categories;
      $sitefromcategory = [];
      $basecategory = [];
      $maincategoryfinal = "";
      $maincategoryurl = "";      
      foreach ($categories as $urlalias) {
        $urlalias_seperate = explode("_", $urlalias);
        $sitefromcategory[] = $urlalias_seperate[0];
        if (($urlalias_seperate[0] == "Brizo" || $urlalias_seperate[0] == "Brizo-Old") && array_key_exists(1, $urlalias_seperate)) {
          $basecategory[] = strtolower($urlalias_seperate[1]);
        }
      }
      if((!empty($sitefromcategory) && in_array("Brizo", $sitefromcategory))
       || (!empty($sitefromcategory) && in_array("Brizo-Old", $sitefromcategory))){
        $basecategoryfinal = array_unique($basecategory);
        if ( in_array("parts", $basecategoryfinal)) {
         $maincategoryfinal = "parts";
        }
        elseif (in_array('kitchen', $basecategoryfinal)) {
          $maincategoryfinal = "kitchen";
        }
        elseif(in_array('bath', $basecategoryfinal)) {
          $maincategoryfinal =  "bath";
        }
      }
      elseif(empty($categories)){
        $maincategoryfinal = "kitchen";
      }

      $mainCategory = $maincategoryfinal;
      $config = \Drupal::config('brizo_pdp.settings');
      $urlPattern  = $config->get('pdp_url_patterns');
      $parts_url_pattern = $config->get('pdp_parts_url_patterns');
      if( $maincategoryfinal != 'parts') {
        $path = str_replace('{CATEGORY}', $mainCategory, $urlPattern);
        $maincategoryurl = str_replace('{sku}', $product->name, $path);
      }
      else {
        $maincategoryurl = str_replace('{sku}', $product->name, $parts_url_pattern);
      }
      if($maincategoryurl != '') {
        $product_url = $host  . $maincategoryurl;
        $url = $xml->addChild('url');
        $url->addChild('loc',$product_url);
      }
    }
    $xml->asXML($filePath. '/product-sitemap.xml');
    return 1;
  }

  public static function getProducts() {
    $products = [];
    $operations = [];
    $config = \Drupal::config('delta_services.deltaservicesconfig');
    $site_name = $config->get('global_site_name');
    $productAPIUrl = $config->get('api_base_path');
    $sitemap_config = \Drupal::config('delta_sitemap.settings');
    $size = $sitemap_config->get('delta_siemap_api_size');
    $productAPIUrl = $productAPIUrl . $sitemap_config->get('delta_siemap_api_url');
    $productAPIUrl = str_replace('[size]' , $size, $productAPIUrl);
    $productAPIUrl = str_replace('[page]' , 0, $productAPIUrl);
    $productRequest = \Drupal::service('delta_services.service')->apiCall($productAPIUrl, "GET", [], "", "ProductSitemap");
    if ($productRequest['status'] == 'SUCCESS' ) {
      $encodedResponse = json_decode($productRequest['response']);
      $totalPages = $encodedResponse->totalPages;
      $products = $encodedResponse->content;
      switch ($site_name) {
        case 'delta':
          $generateXML = self::generateXML($products);
          $subRoutine  = '\Drupal\delta_sitemap\ProductSitemap::generateXML';
          break;
        case 'brizo':
          $generateXML = self::generateBrizoXML($products);
          $subRoutine  = '\Drupal\delta_sitemap\ProductSitemap::generateBrizoXML';
          break;        
        default:
          $generateXML = self::generateXML($products);
          $subRoutine  = '\Drupal\delta_sitemap\ProductSitemap::generateXML';
          break;
      } 
      for($page = 1 ; $page <= $totalPages; $page++) {
        $productAPIUrl = $config->get('api_base_path');
        $productAPIUrl = $productAPIUrl . $sitemap_config->get('delta_siemap_api_url');
        $productAPIUrl = str_replace('[size]' , $size, $productAPIUrl);
        $productAPIUrl = str_replace('[page]' , $page, $productAPIUrl);
        $productRequest = \Drupal::service('delta_services.service')->apiCall($productAPIUrl, "GET", [], "", "ProductSitemap");
        if ($productRequest['status'] == 'SUCCESS' ) {
          $encodedResponse = json_decode($productRequest['response']);
          $productItems = $encodedResponse->content;          
          $operations[] = [ $subRoutine , [$productItems] ];
         }

      }
      return $operations;
    }
    else {
      return 0;
    }
  }
}

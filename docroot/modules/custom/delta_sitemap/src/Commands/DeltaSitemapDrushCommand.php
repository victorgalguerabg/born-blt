<?php

namespace Drupal\delta_sitemap\Commands;

use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drush\Commands\DrushCommands;
use Drupal\delta_sitemap\ProductSitemap;

/**
 * Class DeltaSitemapDrushCommand
 * @package Drupal\delta_sitemap\Commands
 */

class DeltaSitemapDrushCommand extends DrushCommands {

  /**
   * Logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  private $loggerChannelFactory;
  /**
   * Constructs a new ProductSitemapGenerate object.
   *
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerChannelFactory
   *   Logger service.
   */

   /**
    * Drupal\delta_services\DeltaService definition.
    *
    * @var \Drupal\delta_sitemap\ProductSitemap
    */
   protected $sitemap_service;


  public function __construct(LoggerChannelFactoryInterface $loggerChannelFactory, ProductSitemap $product_sitemap) {
    $this->loggerChannelFactory = $loggerChannelFactory;
    $this->sitemap_service = $product_sitemap;
  }

  /**
   * Regenerate the XML sitemaps according to the module settings.
   *
   * @command delta-sitemap:products
   *
   * @usage drush delta-sitemap:products
   *   Regenerate the XML sitemaps according to the module settings.
   *
   * @validate-module-enabled delta_sitemap
   *
   * @aliases dsp, delta-sitemap-generate
   */

  public function generate() {

    $this->loggerChannelFactory->get('delta_sitemap')->info('Delta Product sitemap process initiated');
    $operations = $this->sitemap_service->generate('batch');
    if(count($operations) > 0) {
        $this->loggerChannelFactory->get('delta_sitemap')->info('Generate sitemap batch operations start.');
        $batch = [
          'title' => t('Generate sitemap'),
          'operations' => $operations,
          'finished' => ['\Drupal\delta_sitemap\MapgenerateController::LogbatchMessage()'],
        ];
        batch_set($batch);
        drush_backend_batch_process();
        $this->loggerChannelFactory->get('delta_sitemap')->info('Generate batch operations end.');
    }
  }
}

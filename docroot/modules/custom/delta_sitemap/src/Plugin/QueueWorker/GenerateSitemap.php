<?php
namespace Drupal\delta_sitemap\Plugin\QueueWorker;

use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\Core\File\FileSystem;
use \Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Save queue item in a node.
 *
 * To process the queue items whenever Cron is run,
 * we need a QueueWorker plugin with an annotation witch defines
 * to witch queue it applied.
 *
 * @QueueWorker(
 *   id = "generate_delta_sitemap",
 *   title = @Translation("Gernerate Product sitemap"),
 *   cron = {"time" = 60}
 * )
 */
class GenerateSitemap extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * Drupal\Core\Logger\LoggerChannelFactoryInterface definition.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  private $loggerChannelFactory;

  /**
   * @var \Drupal\Core\File\FileSystem
   */
  protected $fileSystem;

  /**
   * @var Symfony\Component\HttpFoundation\RequestStack
   */
  protected $request;

  /**
   * {@inheritdoc}
   */

  public function __construct(LoggerChannelFactoryInterface $loggerChannelFactory, FileSystem $file_system, RequestStack $requestStack) {
    $this->loggerChannelFactory = $loggerChannelFactory;
    $this->fileSystem = $file_system;
    $this->request = $requestStack;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $container->get('logger.factory'),
      $container->get('file_system'),
      $container->get('request_stack')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($product) {
    // Save the queue item in a node
      //$host = $this->request->getCurrentRequest()->getSchemeAndHttpHost();
      $host = Url::fromRoute('<front>', [], ['absolute' => TRUE])->toString();
      $filePath = $this->fileSystem->realpath(\Drupal::config('system.file')->get('default_scheme') . "://");
      $module_path = drupal_get_path('module', 'delta_sitemap');
      $xmlFile = $filePath . '/product-sitemap.xml';
      $xml = simplexml_load_file($xmlFile);
      $product_name = $product->name;$basecategory = [];
      $categories = $product->categories;
      // Get product url-location
      $sitefromcategory = [];
      $basecategory = [];
      $maincategoryfinal = "";
      $maincategoryurl = "";
      $repairpart = $product->values->RepairPart;
      foreach ($categories as $urlalias) {
        $urlalias_seperate = explode("_", $urlalias);
        $sitefromcategory[] = $urlalias_seperate[0];
        if ($urlalias_seperate[0] == "Delta") {
          $basecategory[] = strtolower($urlalias_seperate[1]);
        }
      }
      if(!empty($sitefromcategory) && in_array("Delta", $sitefromcategory)){
        $basecategoryfinal = array_unique($basecategory);
        if ($repairpart == "T" && in_array("parts", $basecategoryfinal)) {
          $maincategoryfinal = "parts";
          $maincategoryurl = "parts-product-detail?modelNumber=";
        }
        elseif (in_array('kitchen', $basecategoryfinal)) {
          $maincategoryfinal = "kitchen";
          $maincategoryurl = "kitchen/product/";
        }
        elseif(in_array('bathroom', $basecategoryfinal)) {
          $maincategoryfinal =  "bathroom";
          $maincategoryurl = "bathroom/product/";
        }
      }
      elseif(empty($categories) && $repairpart == "T"){
        $maincategoryfinal = "parts";
        $maincategoryurl = "parts-product-detail?modelNumber=";
      }
      elseif (empty($categories) && $repairpart == "F"){
        $maincategoryfinal = "kitchen";
        $maincategoryurl = "kitchen/product/";
      }
      if($maincategoryurl != '') {
        $product_url = $host . '/' . $maincategoryurl . $product_name;
        $url = $xml->addChild('url');
        $url->addChild('loc',$product_url);
      }


      $xml->asXML($filePath. '/product-sitemap.xml');
      $this->loggerChannelFactory->get('debug')->debug('Create sitemap from queue');
    }
}

<?php
namespace Drupal\delta_sitemap\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\simple_sitemap\Simplesitemap;
use Symfony\Component\HttpFoundation\Response;
use Drupal\Core\File\FileSystem;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;

/**
 * Class MapgenerateController.
 */

class MapgenerateController extends ControllerBase {


  /**
   * @var \Drupal\simple_sitemap\Simplesitemap
   */
  protected $generator;

  /**
   * @var \Drupal\Core\File\FileSystem
   */
  protected $fileSystem;

  /**
   * @var Symfony\Component\HttpFoundation\RequestStack
   */
  protected $request;

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Drupal\Core\Logger\LoggerChannelFactoryInterface definition.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */

  public static $loggerChannelFactory;

  /**
   * SimplesitemapController constructor.
   * @param \Drupal\simple_sitemap\Simplesitemap $generator
   */
    public function __construct(Simplesitemap $generator, FileSystem $file_system, RequestStack $requestStack, ConfigFactoryInterface $config_factory, LoggerChannelFactoryInterface $loggerChannel) {

      $this->generator = $generator;
      $this->fileSystem = $file_system;
      $this->request = $requestStack;
      $this->configFactory = $config_factory;
      $this->loggerChannelFactory = $loggerChannel;
    }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('simple_sitemap.generator'),
      $container->get('file_system'),
      $container->get('request_stack'),
      $container->get('config.factory'),
      $container->get('logger.factory')
    );
  }


  /**
   * Generates sitemap.
   *
   * @return array
   *   A simple renderable array.
   */

  public static function generateSitemapBatch() {
    $sitemap = \Drupal::service('delta_sitemap.generator');
    $operations = $sitemap->generate('batch');
    if(count($operations) > 0) {
        \Drupal::logger('delta_sitemap')->info('Generate sitemap batch operations start.');
        $batch = [
          'title' => t('Generate sitemap'),
          'operations' => $operations,
          'finished' => ['\Drupal\delta_sitemap\MapgenerateController::LogbatchMessage()'],
        ];
        batch_set($batch);
        return batch_process('user');
    }
  }


  public function generate() {
    $sitemap = \Drupal::service('delta_sitemap.generator');
    $totalQueue = $sitemap->generate();
    $element = array(
      '#markup' => "Total Item queued $totalQueue.",
    );
    return $element;
  }

  /**
   * Returns the XML stylesheet for the sitemap.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   */
  public function getSitemapXsl() {

    // Read the XSL content from the file.
     $module_path = drupal_get_path('module', 'delta_sitemap');
     $xsl_content = file_get_contents($module_path . '/xsl/sitemap.xsl');
     $simple_sitemap_module_path = drupal_get_path('module', 'simple_sitemap');
     $host = $this->request->getCurrentRequest()->getSchemeAndHttpHost();
     $sitemap_config = $this->configFactory->get('delta_sitemap.settings');
    // Replace custom tokens in the XSL content with appropriate values.
    $replacements = [
      '[CMS-SITEMAP]' => $host . $sitemap_config->get('delta_siemap_content_sitemap_url'),
      '[PRODUCT-SITEMAP]' => $host . '/product-sitemap.xml',
      '[SFCC-PRODUCT-SITEMAP]' => $host . '/sfcc-product-sitemap.xml',
      '[IMG-SITEMAP]' => $host . $sitemap_config->get('delta_siemap_img_sitemap_url'),
    ];
    if ( ($imgSiteMap = $sitemap_config->get('delta_siemap_img_sitemap_url')) != '') {
      $replacements['[IMG-SITEMAP]'] = '<sitemap><loc>' . $host . $imgSiteMap .'</loc></sitemap>';
    }
    else {
        $replacements['[IMG-SITEMAP]'] = '';
    }

    // Output the XSL content.
    return new Response(strtr($xsl_content, $replacements), Response::HTTP_OK, [
      'Content-type' => 'application/xml; charset=utf-8',
      'X-Robots-Tag' => 'noindex, nofollow',
    ]);
  }

  /**
   * Returns the XML stylesheet for the sitemap.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   */

  public function getProductSitemapXsl() {
    $filePath = $this->fileSystem->realpath(\Drupal::config('system.file')->get('default_scheme') . "://");
    $xmlFile = $filePath . '/product-sitemap.xml';
    $xsl_content = file_get_contents($xmlFile);
    $replacements = [];
    // Output the XSL content.
    return new Response(strtr($xsl_content, $replacements), Response::HTTP_OK, [
    'Content-type' => 'application/xml; charset=utf-8',
    'X-Robots-Tag' => 'noindex, nofollow',
    ]);
  }

  /**
   * Returns the XML stylesheet for the sfcc products sitemap.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   */

  public function getSfccProductSitemapXsl() {
    $filePath = $this->fileSystem->realpath(\Drupal::config('system.file')->get('default_scheme') . "://");
    $xmlFile = $filePath . '/sfcc-product-sitemap.xml';
    $xsl_content = file_get_contents($xmlFile);
    $replacements = [];
    // Output the XSL content.
    return new Response(strtr($xsl_content, $replacements), Response::HTTP_OK, [
      'Content-type' => 'application/xml; charset=utf-8',
      'X-Robots-Tag' => 'noindex, nofollow',
    ]);
  }

  public static function LogbatchMessage() {
    \Drupal::logger('delta_sitemap')->info('Generate sitemap batch operations End');
  }

}

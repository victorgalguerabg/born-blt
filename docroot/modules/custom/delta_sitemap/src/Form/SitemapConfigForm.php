<?php

/**
 * @file
 * Contains Drupal\delta_sitemap\Form\SitemapConfigForm.
 */

namespace Drupal\delta_sitemap\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\delta_sitemap\ProductSitemap;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;


/**
 * Class SettingsForm.
 *
 * @package Drupal\delta_sitemap\Form
 */
class SitemapConfigForm extends ConfigFormBase {


  /**
   * Drupal\delta_services\DeltaService definition.
   *
   * @var \Drupal\delta_sitemap\ProductSitemap
   */
  protected $sitemap_service;

  /**
   * Logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  private $loggerChannelFactory;
  public function __construct(ProductSitemap $product_sitemap, LoggerChannelFactoryInterface $loggerChannelFactory){
    $this->loggerChannelFactory = $loggerChannelFactory;
    $this->sitemap_service = $product_sitemap;
  }

  /**
   * {@inheritdoc}
   */

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('delta_sitemap.generator'),
      $container->get('logger.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'delta_sitemap.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'delta_sitemap_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('delta_sitemap.settings');
    $form['delta_siemap_corn_time'] = array(
      '#type' => 'select',
      '#title' => $this->t('Run cron every'),
      '#options' => [
        '0' => $this->t('Never'),
        '10' => $this->t('10 Seconds'),
        '604800' => $this->t('1 Week'),
        '1,296,000' => $this->t('15 Days'),
        '2,592,000' => $this->t('30 Days')
        ],
      '#default_value' => $config->get('delta_siemap_corn_time'),
    );
    $form['delta_siemap_api_url'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Enter the API url'),
      '#default_value' => $config->get('delta_siemap_api_url'),
    );
    $form['delta_siemap_api_size'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Enter the page size'),
      '#default_value' => $config->get('delta_siemap_api_size'),
    );
    $form['delta_siemap_content_sitemap_url'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Enter content sitemap url'),
      '#default_value' => $config->get('delta_siemap_content_sitemap_url'),
    );
    $form['delta_siemap_img_sitemap_url'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Enter image sitemap url'),
      '#default_value' => $config->get('delta_siemap_img_sitemap_url'),
    );
    $form['delta_sitemap'] = array(
      '#type' => 'fieldset',
      '#title' => $this->t('Sitmap'),
    );
    $form['delta_sitemap']['generate_product_sitemap'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Generate'),
      '#name' => 'generate_product_sitemap',
    );


      return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $triggering_element = $form_state->getTriggeringElement();
    $button_name = $triggering_element['#name'];
    if( $button_name == 'generate_product_sitemap' ) {
    // $batch = array(
    //   'title' => t('Generating sitemap...'),
    //   'error_message' => $this->t('error'),
    //   'operations' => [
    //     ['\Drupal\delta_sitemap\ProductSitemap::generate', ['batch']],
    //   ],
    // );
    // batch_set($batch);
    $operations = $this->sitemap_service->generate('batch');
      if(count($operations) > 0) {
          $this->loggerChannelFactory->get('delta_sitemap')->info('Generate sitemap batch operations start.');
          $batch = [
            'title' => t('Generate sitemap'),
            'operations' => $operations,
            'finished' => ['\Drupal\delta_sitemap\MapgenerateController::LogbatchMessage()'],
          ];
          batch_set($batch);
          //return batch_process('user');
          $this->loggerChannelFactory->get('delta_sitemap')->info('Generate batch operations end.');
      }
    }
    else {
      $this->config('delta_sitemap.settings')
        ->set('delta_siemap_corn_time', $form_state->getValue('delta_siemap_corn_time'))
        ->set('delta_siemap_api_url', $form_state->getValue('delta_siemap_api_url'))
        ->set('delta_siemap_api_size', $form_state->getValue('delta_siemap_api_size'))
        ->set('delta_siemap_content_sitemap_url', $form_state->getValue('delta_siemap_content_sitemap_url'))
        ->set('delta_siemap_img_sitemap_url', $form_state->getValue('delta_siemap_img_sitemap_url'))
        ->save();
    }
  }

}

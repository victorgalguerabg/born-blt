<?php
/**
 * @file Contains the code to generate the custom drush commands.
 */

/**
 * Implements hook_drush_command().
 */
function delta_sitemap_drush_command() {

  $items['delta-sitemap-generate'] = [
    'description' => 'Generate product sitemap.',
    'aliases' => ['delta-sitemap:products'],
    'callback' => 'delta_sitemap_generate',
  ];
  return $items;
}

/**
 * Call back function drush_custom_drush_command_say_hello()
 * The call back function name in the  following format
 *  drush_modulename_[COMMAND_NAME]().
 */
function drush_delta_delta_sitemap_generate() {
  $batch = [
    'title' => t('Generate sitemap'),
    'operations' => [
      ['\Drupal\delta_sitemap\ProductSitemap::generate', ['batch']],
    ],
  ];
  batch_set($batch);
  drush_backend_batch_process();
}

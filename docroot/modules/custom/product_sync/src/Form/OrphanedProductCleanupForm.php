<?php
namespace Drupal\product_sync\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\delta_services\DeltaService;
use Drupal\product_sync\ProductImport;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;


/**
 * Defines a form that pdp configures forms module settings.
 */

 class OrphanedProductCleanupForm extends FormBase {


   /**
    * Drupal\Core\Config\ConfigFactoryInterface definition.
    *
    * @var \Drupal\Core\Config\ConfigFactoryInterface
    */
   protected $configFactory;


   /**
    * Constructs a new GetProductsController object.
    *
    * @param \Drupal\delta_services\DeltaService $deltaservice
    *   The deltaservice to make API calls.
    */

    protected $deltaservice;

    /**
     *
     * @param \Drupal\product_sync\ProductImport $productImport
     *   The deltaservice to make API calls.
     */

     protected $productImport;

       /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
   protected $messenger;

     /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

    /**
     * {@inheritdoc}
     */

    public function __construct(DeltaService $delta_service, ConfigFactoryInterface $config_factory, ProductImport $product_import, MessengerInterface $messenger, EntityTypeManagerInterface $entity_type_manager) {
     $this->deltaservice = $delta_service;
     $this->configFactory = $config_factory;
     $this->productImport = $product_import;
     $this->messenger = $messenger;
     $this->entityTypeManager = $entity_type_manager;
    }

    /**
     * {@inheritdoc}
     */

    public static function create(ContainerInterface $container) {
      return new static(
        $container->get('delta_services.service'),
        $container->get('config.factory'),
        $container->get('product_sync.import'),
        $container->get('messenger'),
        $container->get('entity_type.manager')
      );
    }
  /**
   * {@inheritdoc}
   */

  public function getFormId() {
  return 'product_sync_form';
  }

  /**
   * {@inheritdoc}
   */

   public function buildForm(array $form, FormStateInterface $form_state) {

    $nullPIDS = $this->productImport->getNullProducts();
    $skus = '';
    if(is_array($nullPIDS) && count($nullPIDS) > 0 ){
      foreach ($nullPIDS as $key => $value) {
        $skus .= $value['sku'];
        $skus .= "<br />";
      }
      $OrphanedFlag = true;
    }else{
      $skus = 'No orphaned products exist';
      $OrphanedFlag = false;
    }

    $form['orphaned_products_cleanup'] = array(
       '#type' => 'fieldset',
       '#title' => $this->t('Orphaned Products Cleanup'),
     );

    $form['orphaned_products_cleanup']['product_sync_import'] = array(
       '#type' => 'submit',
       '#value' => $this->t('Cleanup orphaned skus'),
       '#name' => 'orphaned_products_cleanup',
    );
    $form['orphaned_products_cleanup']['Orphaned'] = array(
       '#type' => 'details',
       '#title' => $this->t('Orphaned products List'),
       '#open' => $OrphanedFlag,
     );

    $form['orphaned_products_cleanup']['Orphaned']['product_sync_intro'] = array(
        '#type' => 'html_tag',
        '#tag' => 'p',
        '#value' => $skus ,
      );

    // For duplicate product
    $dupPIDS = $this->productImport->getDupProducts();
    $dupSkus = '';
    if(is_array($dupPIDS) && count($dupPIDS) > 0 ){
      foreach ($dupPIDS as $dupsku => $dupvalue) {
        $dupSkus .= 'SKU:' . $dupsku . ', Total number of duplicate ' . count($dupvalue);
        $dupSkus .= "<br />";
      }
      $duplicateFlag = true;
    }else{
      $dupSkus = 'No duplicate products exist';
      $duplicateFlag = false;
    }

    $form['duplicate_products_cleanup'] = array(
       '#type' => 'fieldset',
       '#title' => $this->t('Duplicate Products Cleanup'),
     );
    $form['duplicate_products_cleanup']['product_sync_import'] = array(
       '#type' => 'submit',
       '#value' => $this->t('Cleanup duplicate skus'),
       '#name' => 'duplicate_products_cleanup',
    );

    $form['duplicate_products_cleanup']['duplicate'] = array(
       '#type' => 'details',
       '#title' => $this->t('Duplicate products List'),
       '#open' => $duplicateFlag,
     );

    $form['duplicate_products_cleanup']['duplicate']['product_sync_intro'] = array(
        '#type' => 'html_tag',
        '#tag' => 'p',
        '#value' => $dupSkus ,
      );

    return $form;

  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */

  public function submitForm(array &$form, FormStateInterface $form_state) {

      $triggering_element = $form_state->getTriggeringElement();
      $button_name = $triggering_element['#name'];
      if( $button_name == 'orphaned_products_cleanup' ) {
      //  $products = $this->testGetNullProducts();
        $products = $this->productImport->getNullProducts();
      $batch = array(
        'title' => t('Orphaned products cleanup ...'),
        'operations' => array(
          array(
            '\Drupal\product_sync\ProductImport::cleanupBulkOrphanedProducts',
            array($products)
          ),
        ),
        'finished' => '\Drupal\product_sync\ProductImport::cleanupBulkOrphanedProductsFinish',
      );
      batch_set($batch);
    }
    if( $button_name == 'duplicate_products_cleanup' ) {
      // $products = $this->testGetDupProducts();
      $products = $this->productImport->getDupProducts();
      $batch = array(
        'title' => t('Duplicate products cleanup ...'),
        'operations' => array(
          array(
            '\Drupal\product_sync\ProductImport::cleanupDuplicateProducts',
            array($products)
          ),
        ),
        'finished' => '\Drupal\product_sync\ProductImport::cleanupDuplicateProductsFinish',
      );
      batch_set($batch);
    }
  }

  // For testing purpose created few test data, product ids are NULL
  public function testGetNullProducts(){
    return [
      '0' => ['sku' =>'68135-GL','variation_id'=>'765', 'product_id' => NULL],
      '1' => ['sku' =>'RP49587PC','variation_id'=>'1935', 'product_id' => NULL],
      '2' => ['sku' =>'T67305-BNLHP','variation_id'=>'4020', 'product_id' => NULL],
      '3' => ['sku' =>'RP70908PC','variation_id'=>'4037', 'product_id' => NULL]
    ];
  }

  // For testing purpose created few test data, product ids are NULL
  public function testGetDupProducts(){
    return [
      '0' => ['sku' =>'68135-GL','variation_id'=>'765', 'product_id' => 200],
      '1' => ['sku' =>'RP49587PC','variation_id'=>'1935', 'product_id' => 765],
      '2' => ['sku' =>'T67305-BNLHP','variation_id'=>'4020', 'product_id' => 767],
      '3' => ['sku' =>'RP70908PC','variation_id'=>'4037', 'product_id' => 768]
    ];
  }

 }

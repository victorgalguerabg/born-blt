<?php
namespace Drupal\product_sync\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\delta_services\DeltaService;
use Drupal\product_sync\ProductImport;
use Symfony\Component\DependencyInjection\ContainerInterface;


/**
 * Defines a form that pdp configures forms module settings.
 */

 class ProductSyncForm extends FormBase {


   /**
    * Drupal\Core\Config\ConfigFactoryInterface definition.
    *
    * @var \Drupal\Core\Config\ConfigFactoryInterface
    */
   protected $configFactory;


   /**
    * Constructs a new GetProductsController object.
    *
    * @param \Drupal\delta_services\DeltaService $deltaservice
    *   The deltaservice to make API calls.
    */

    protected $deltaservice;

    /**
     *
     * @param \Drupal\product_sync\ProductImport $productImport
     *   The deltaservice to make API calls.
     */

     protected $productImport;

    /**
     * {@inheritdoc}
     */

    public function __construct(DeltaService $delta_service, ConfigFactoryInterface $config_factory, ProductImport $product_import ) {
     $this->deltaservice = $delta_service;
     $this->configFactory = $config_factory;
     $this->productImport = $product_import;
    }

    /**
     * {@inheritdoc}
     */

    public static function create(ContainerInterface $container) {
      return new static(
        $container->get('delta_services.service'),
        $container->get('config.factory'),
        $container->get('product_sync.import')
      );
    }
  /**
   * {@inheritdoc}
   */

  public function getFormId() {
  return 'product_sync_form';
  }

  /**
   * {@inheritdoc}
   */

   public function buildForm(array $form, FormStateInterface $form_state) {

     $config = $this->configFactory->get('delta_services.deltaservicesconfig');
     $config_sync = $this->configFactory->get('global_sync.api_settings');
     $endpoint  = $config_sync->get('pdp_sync_api_url');
     $basePath  = $config->get('api_base_path');
     $enpointUrl = $basePath . $endpoint;

    $description = "Current API endpoint <i>". $enpointUrl . "</i>";

    $form['product_sync'] = array(
       '#type' => 'fieldset',
       '#title' => $this->t('Product Sync'),
     );

    $form['product_sync']['product_sync_intro'] = array(
        '#type' => 'html_tag',
        '#tag' => 'p',
        '#value' => $description,
      );

    $form['product_sync']['product_sync_import'] = array(
       '#type' => 'submit',
       '#value' => $this->t('Sync Product'),
       '#name' => 'product_import_sync',
    );

    return $form;

  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */

  public function submitForm(array &$form, FormStateInterface $form_state) {

      $triggering_element = $form_state->getTriggeringElement();
      $button_name = $triggering_element['#name'];
      if( $button_name == 'product_import_sync' ) {
          $this->productImport->importBulkProduct();
      }



  }


 }

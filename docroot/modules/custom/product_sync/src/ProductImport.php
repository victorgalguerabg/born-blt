<?php
namespace Drupal\product_sync;

use Drupal\delta_services\DeltaService;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\Component\Serialization\Json;
use Drupal\commerce_price\Price;
use Drupal\Core\Database\Connection;
use Drupal\path_alias\Entity\PathAlias;

/**
 * Class ProductImport.
 */
class ProductImport {

  /**
   * Drupal\Core\Logger\LoggerChannelFactoryInterface definition.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerChannelFactory;

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Drupal\path_alias\AliasManagerInterface definition.
   *
   * @var \Drupal\path_alias\AliasManagerInterface
   */
  protected $pathAliasStorage;

    /**
   * DB connection resource.
   *
   * @var connection
   */
  protected $connection;

  /**
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * @var \Drupal\delta_services\DeltaService
   */
  protected $deltaservice;

  /**
   * @param \Drupal\delta_services\DeltaService $deltaservice
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerChannel
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   * @param \Drupal\Core\Database\Connection $connection
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   */
  public function __construct(DeltaService $deltaservice, ConfigFactoryInterface $config_factory, LoggerChannelFactoryInterface $loggerChannel, EntityTypeManagerInterface $entity_type_manager, Connection $connection, MessengerInterface $messenger) {
    $this->deltaservice = $deltaservice;
    $this->configFactory = $config_factory;
    $this->loggerChannelFactory = $loggerChannel;
    $this->entityTypeManager = $entity_type_manager;
    $this->connection = $connection;
    $this->messenger = $messenger;

    try {
      $this->pathAliasStorage = $this->entityTypeManager->getStorage('path_alias');
    } catch (\Exception $error) {
      $this->loggerChannelFactory->get('product_sync')->alert(t('@err', ['@err' => $error->__toString()]));
      $this->messenger->addWarning(t('Unable to proceed. Please contact an administrator. Error code: 6AEA99C7-@line-@time', ['@line' => __LINE__, '@time' => time()]));
    }
  }

  /**
   * @param $sku
   *
   * @return mixed
   */
  public function getCheckProductExist($sku) {
    $results = $this->connection->select('commerce_product_variation_field_data', 'i')->fields('i', ['sku', 'variation_id','product_id'])->condition('i.sku', $sku,'=')->execute()->fetchAll();

    return  json_decode(json_encode($results), TRUE);
  }

  /**
   * Get all products in the commerce bundle.
   *
   * @return mixed
   */
  public function getAllProducts() {
    $results = $this->connection->select('commerce_product_variation_field_data', 'i')->fields('i', ['sku', 'variation_id','product_id'])->execute()->fetchAll();

    return  json_decode(json_encode($results), TRUE);
  }

  /**
   * Product variations that have no referenced product.
   *
   * @param null $sku
   *
   * @return array
   */
  public function getNullProducts($sku = NULL) {
    if ($sku == NULL) {
      $getNullProducts = $this->getAllProducts();
    } else {
      $getNullProducts = $this->getCheckProductExist($sku);
    }

    $nullProductsList = [];
    if (count($getNullProducts)) {
      foreach ($getNullProducts as $key => $variation) {
        if ((is_array($variation)) && empty($variation['product_id'])) {
          $nullProductsList[] = $variation;
        }
      }
    }

    return $nullProductsList;
  }

  /**
   * Get duplicate products in array
   *
   * @param null $sku
   *
   * @return array
   */
  public function getDupProducts($sku = NULL){
    if ($sku == NULL) {
      $getDupProducts = $this->getAllProducts();
    } else {
      $getDupProducts = $this->getCheckProductExist($sku);
    }

    $new_arr = [];
    foreach ($getDupProducts as $k => $v) {
      $new_arr[$v['sku']][]=$v;
    }

    $duplicateProductSkus = [];
    foreach ($new_arr as $sku => $value) {
      if (count($value) > 1) {
        $duplicateProductSkus[$sku] = $value;
      }
    }

    return $duplicateProductSkus;
  }

  /**
   * Cleanup duplicate products
   *
   * @param $products
   */
  public static function cleanupDuplicateProducts($products){
    $message = 'Duplicate products cleanup in progress...';
    $results = [];

    $deletedSKUS = [];
    if (is_array($products) && count($products) > 0 ) {
      foreach ($products as $sku => $product_arr) {
        if (is_array($product_arr) && count($product_arr) > 0) {
          foreach ($product_arr as $key => $products) {
            $product_id = $products['product_id'];

            //delete products except product is missing rows
            if (!empty($product_id)) {
              try {
                $product = \Drupal::entityTypeManager()->getStorage('commerce_product')->load($product_id);
                if(!empty($product)) {
                  $product->delete();
                }
              } catch (\Exception $error) {
                $messenger = \Drupal::messenger();
                $logger = \Drupal::logger('product_sync');
                $logger->alert(t('@err', ['@err' => $error->__toString()]));
                $messenger->addWarning(t('Unable to proceed. Please contact an administrator. Error code: 6AEA99C7-@line-@time', ['@line' => __LINE__, '@time' => time()]));
              }

              $deletedSKUS[$products['sku']] = $products['sku'];
              $results[] = $products['sku'];
            }
          }
        }
      }
    }

    $logMsg = 'Deleted duplicate SKUs List: ' . json_encode($deletedSKUS);
    \Drupal::logger('duplicate_skus_deleted')->info($logMsg);
    $context['message'] = $message;
    $context['results'] = $results;
  }

  /**
   * @param $success
   * @param $results
   * @param $operations
   */
  public static function cleanupDuplicateProductsFinish($success, $results, $operations) {
    // The 'success' parameter means no fatal PHP errors were detected. All
    // other error management should be handled using 'results'.
    if ($success) {
      $message = \Drupal::translation()->formatPlural(
        count($results),
        'One duplicate products processed.', 'Duplicate products removed from the system.'
      );
    } else {
      $message = t('Finished with an error.');
    }

    \Drupal::messenger()->addMessage($message);
  }

  /**
   * Cleanup Orphaned Products
   *
   * @param $variations
   */
  public static function cleanupBulkOrphanedProducts($variations) {
    $message = 'Orphaned products cleanup in progress...';
    $results = [];
    $cleanedSkus = [];

    foreach ($variations as $variation) {
      try {
        $variations = \Drupal::entityTypeManager()->getStorage('commerce_product_variation')->load($variation['variation_id']);
      } catch (\Exception $error) {
        $messenger = \Drupal::messenger();
        $logger = \Drupal::logger('product_sync');
        $logger->alert(t('@err', ['@err' => $error->__toString()]));
        $messenger->addWarning(t('Unable to proceed. Please contact an administrator. Error code: 6AEA99C7-@line-@time', ['@line' => __LINE__, '@time' => time()]));
      }

      try {
        $variations->delete();
      } catch (\Exception $error) {
        $messenger = \Drupal::messenger();
        $logger = \Drupal::logger('product_sync');
        $logger->alert(t('@err', ['@err' => $error->__toString()]));
        $messenger->addWarning(t('Unable to proceed. Please contact an administrator. Error code: 6AEA99C7-@line-@time', ['@line' => __LINE__, '@time' => time()]));
      }

      $results[] = $variation['variation_id'];
      $cleanedSkus[$variation['sku']] = $variation;
    }

    $logMsg = json_encode($cleanedSkus);
    $logMsg = 'Following orphaned products are cleaned up, SKU List here: ' . $logMsg;
    \Drupal::logger('cleanupBulkOrphanedProducts')->info($logMsg);
    $context['message'] = $message;
    $context['results'] = $results;
  }

  /**
   * @param $success
   * @param $results
   * @param $operations
   */
  public static function cleanupBulkOrphanedProductsFinish($success, $results, $operations) {
    // The 'success' parameter means no fatal PHP errors were detected. All
    // other error management should be handled using 'results'.
    if ($success) {
      $message = \Drupal::translation()->formatPlural(
        count($results),
        'One orphaned products processed.', 'Orphaned products removed from the system.'
      );
    } else {
      $message = t('Finished with an error.');
    }

    \Drupal::messenger()->addMessage($message);
  }

  /**
   *
   */
   public function importBulkProduct() {
     $config = $this->configFactory->get('delta_services.deltaservicesconfig');
     $config_sync = $this->configFactory->get('global_sync.api_settings');
     $endpoint  = $config_sync->get('pdp_sync_api_url');
     $basePath  = $config->get('api_base_path');
     $enpointUrl = $basePath . $endpoint;

     $response = $this->deltaservice->apiCall( $enpointUrl, 'GET', [], [], "PRODUCT_IMPORT");

     if (is_array($response) && count($response) > 0) {

       if ($response['status'] == 'SUCCESS') {
        $encodedResponse = json_decode($response['response']);
        $productItems = $encodedResponse->content;
        $totalPages = $encodedResponse->totalPages;

        $totalItems = $encodedResponse->numberOfElements;
        if ($totalItems > 0) {
          foreach ($productItems as $product) {
            $product_name = $product->name;
            $isExist = $this->productExists($product_name);
            $siteName = $config->get('global_site_name');
            if (empty(key($isExist))) {
              $this->createProduct($product,$siteName);
            }
          }
       }
     }
   }
  }

  /**
   * @param $product
   * @param $siteName
   *
   * @return int|string|void|null
   */
  public function createProduct($product, $siteName) {
    $sku = $product->name;

     // Delete product product id is NULL
    $nullProducts = $this->getNullProducts($sku);

    if (is_array($nullProducts) && count($nullProducts) > 0) {
      foreach ($nullProducts as $variationsArr) {
        try {
          $variations = $this->entityTypeManager->getStorage('commerce_product_variation')->load($variationsArr['variation_id']);
          if(!empty($variations)){
            $variations->delete();
          }
        } catch (\Exception $error) {
          $this->loggerChannelFactory->get('product_sync')->alert(t('Error in commerce_product_variation @err', ['@err' => $error->__toString()]));
          $this->messenger->addWarning(t('Unable to proceed. Please contact an administrator. Error code: 6AEA99C7-@line-@time', ['@line' => __LINE__, '@time' => time()]));
        }
      }
    }

     // Check duplicate skus exist
    $duplProducts = $this->getDupProducts($sku);
    if (is_array($duplProducts) && count($duplProducts) > 0) {
      foreach ($duplProducts[$sku] as $dupVariations) {
        try {
          $variations = $this->entityTypeManager->getStorage('commerce_product_variation')->load($dupVariations['variation_id']);
          if(!empty($variations)){
            $variations->delete();
          }
        } catch (\Exception $error) {
          $this->loggerChannelFactory->get('product_sync')->alert(t('Error in duplicate products @err', ['@err' => $error->__toString()]));
          $this->messenger->addWarning(t('Unable to proceed. Please contact an administrator. Error code: 6AEA99C7-@line-@time', ['@line' => __LINE__, '@time' => time()]));
          $this->loggerChannelFactory->get('Brizo PDP')->info('PDP log- Delete duplicate product  - ' . $dupVariations['variation_id']);
        }
      }
    }
    if (is_object($product)) {
        $storeId = 1;
        $categories = $product->categories;
        $basecategory = [];
        $maincategoryfinal = "";
        $maincategoryurl = "";

        foreach ($categories as $urlalias) {
          $urlalias_seperate = explode("_", $urlalias);
          $sitefromcategory[] = $urlalias_seperate[0];

          if ($urlalias_seperate[0] == "Brizo" || $urlalias_seperate[0] == "Brizo-Old") {
            $basecategory[] = strtolower($urlalias_seperate[1]);
          }
        }

        if ((!empty($sitefromcategory) && in_array("Brizo", $sitefromcategory))
        || (!empty($sitefromcategory) && in_array("Brizo-Old", $sitefromcategory))) {
          $basecategoryfinal = array_unique($basecategory);

          if (in_array('kitchen', $basecategoryfinal)) {
            $maincategoryfinal = "kitchen";
            $maincategoryurl = "kitchen/product/";
          } elseif (in_array('bath', $basecategoryfinal)) {
            $maincategoryfinal =  "bath";
            $maincategoryurl = "bath/product/";
          } elseif (in_array('parts', $basecategoryfinal)) {
            $maincategoryfinal =  "parts";
            $maincategoryurl = "bath/product/";
          }
        }

        if (count($categories) == 0 ) {
          $maincategoryfinal =  "kitchen";
          $maincategoryurl = "kitchen/product/";
        }

        if ($maincategoryurl != "") {
          $mainCategory = $maincategoryfinal;
          try {
            $store = $this->entityTypeManager->getStorage('commerce_store')->loadDefault();
          } catch (\Exception $error) {
            $this->loggerChannelFactory->get('product_sync')->alert(t('@err', ['@err' => $error->__toString()]));
            $this->messenger->addWarning(t('Unable to proceed. Please contact an administrator. Error code: 6AEA99C7-@line-@time', ['@line' => __LINE__, '@time' => time()]));
          }

        $storeData = $store->get('store_id')->getValue();
        if (is_array($storeData) && !empty($storeData)) {
          $storeId = $storeData[0]['value'];
        }

        $termId = '';
        $collection = (isset($product->values->Collection))?$product->values->Collection:$product->values->defaultCollection;
        if (!empty($collection)) {
          $termId =  $this->getCollectionTermByName($collection);
        }

        $consumerCopy = isset($product->values->ConsumerCopy) ? $product->values->ConsumerCopy : '';
        $variationObj = self::createCommerceProductVariation($product);


        try {
          $productEntity =  $this->entityTypeManager->getStorage('commerce_product')->create([
            'title' => $product->description,
            'uid' => '1',
            'type' => 'default',
            'variations' => [$variationObj],
            'stores' => [$storeId],
            'field_image_url' => self::getImageUrls($product),
            'field_brand' => $siteName,
            'field_collection' => $termId,
            'field_product_response_data' => Json::encode($product),
            'field_category' => $maincategoryfinal,
            'body' => $consumerCopy,
          ]);
        } catch (\Exception $error) {
          $this->loggerChannelFactory->get('product_sync')->alert(t('@err', ['@err' => $error->__toString()]));
          $this->messenger->addWarning(t('Unable to proceed. Please contact an administrator. Error code: 6AEA99C7-@line-@time', ['@line' => __LINE__, '@time' => time()]));
          $this->loggerChannelFactory->get('Brizo PDP')->info('PDP log- Delete duplicate product  - ' . $dupVariations['variation_id']);
        }

        $config = $this->configFactory->get('brizo_pdp.settings');
        $urlPattern  = $config->get('pdp_url_patterns');
        $parts_url_pattern = $config->get('pdp_parts_url_patterns');

        if ($maincategoryfinal != 'parts') {
          $path = str_replace('{CATEGORY}', $mainCategory, $urlPattern);
          $path = str_replace('{sku}', $product->name, $path);
        } else {
          $path = str_replace('{sku}', $product->name, $parts_url_pattern);
        }

        //$productalias = "/".$maincategoryurl.$product->name;
        $productalias = $path;
        try {
          $productEntity->save();
          $path_alias = $this->pathAliasStorage->create([
            'path' => '/product/' . $productEntity->id(),
            'alias' => urldecode($productalias),
            'langcode' => 'en'])->save();

           // PathAlias::create(['path' => "/node/".$nid, 'alias' => urldecode($aliasPath),'langcode' => 'en'])->save();
            
        } catch (\Exception $error) {
          $this->loggerChannelFactory->get('product_sync')->alert(t('Error in saving product @err', ['@err' => $error->__toString()]));
          $this->messenger->addWarning(t('Unable to proceed. Please contact an administrator. Error code: 6AEA99C7-@line-@time', ['@line' => __LINE__, '@time' => time()]));
        }

        //new product has been created. Update product as shown field. BC-45
        $update_product = $this->constructProductShown($product, $productEntity);
        // ends BC-45
        return $productEntity->id();
        }
      }
  }

  /**
   * @param $modelNumber
   *
   * @return array
   */
  public function productExists($modelNumber) {
    $productId = [];
    try {
      $variation = $this->entityTypeManager
        ->getStorage('commerce_product_variation')
        ->loadByProperties(['sku' => $modelNumber]);
    } catch (\Exception $error) {
      $this->loggerChannelFactory->get('product_sync')->alert(t('error in product exists: @sku', ['@sku' => $modelNumber]));
      $this->messenger->addWarning(t('Unable to proceed. Please contact an administrator. Error code: 6AEA99C7-@line-@time', ['@line' => __LINE__, '@time' => time()]));
    }

    if (!empty($variation)) {
      $variationObj = reset($variation);
      $productId = $variationObj->getProductId();
      return [$productId => $productId];
    } else {
      return $productId;
    }
  }

  /**
   * @param null $product
   *
   * @return \Drupal\commerce_product\Entity\ProductVariation|\Drupal\Core\Entity\EntityBase|\Drupal\Core\Entity\EntityInterface|void
   */
  public function createCommerceProductVariation($product = NULL) {
    $content = (array)$product;

    if (!is_null($content)) {
      $key_exists = $model = NULL;
      $model = $content['name'];
      $listPrice = $content['values']->ListPrice;

      try {
        $variation = ProductVariation::create([
          'type' => 'default',
          'sku' => $model,
          'status' => 1,
          'price' => self::getPrice($listPrice),
        ]);
        $variation->save();
      } catch (\Exception $error) {
        $this->loggerChannelFactory->get('product_sync')->alert(t(' Error in createCommerceProductVariation  @err', ['@err' => $error->__toString()]));
        $this->messenger->addWarning(t('Unable to proceed. Please contact an administrator. Error code: 6AEA99C7-@line-@time', ['@line' => __LINE__, '@time' => time()]));
      }

      return $variation;
    }
  }

  /**
   * @param null $product
   *
   * @return array
   */
   public static function getImageUrls($product = NULL) {
     $content = (array)$product;
     $imageUrls = [];
     $imageUrls[]['value'] = $content['heroImage'];

     if (!empty($content['assets'])) {
       $product_assets_obj = $content['assets'];

       if (count($content['assets'])) {
         $product_assets_arr = (array)$product_assets_obj;

         foreach ($product_assets_arr as $image_assets) {
           if ($image_assets->type == 'InContextShot' || $image_assets->type == 'Video' || $image_assets->type == 'SecondaryOnWhiteShots') {
             $imageUrls[]['value'] = $image_assets->url;
           }
         }
       }
     }

     return $imageUrls;
   }

  /**
   * @param null $listPrice
   * @param string $currencyCode
   *
   * @return \Drupal\commerce_price\Price|void
   */
   public function getPrice($listPrice = NULL, $currencyCode = 'USD') {
     if (!is_null($listPrice)) {
       $price = new Price($listPrice, $currencyCode);
       return $price;
     }
   }

  /**
   * @param $term_name
   *
   * @return int|string|null
   */
   public function getCollectionTermByName($term_name){
      $term_name = trim($term_name);
      $tid = '';
      $term_name = htmlentities($term_name);
      $term_name = str_replace('&lt;sup&gt;&amp;trade;&lt;/sup&gt;', '', $term_name);
      $term_name = str_replace('&lt;sup&gt;&amp;reg;&lt;/sup&gt;', '', $term_name);

      try {
        $query = $this->entityTypeManager->getStorage('taxonomy_term')->getQuery();
      } catch (\Exception $error) {
        $this->loggerChannelFactory->get('product_sync')->alert(t('@err', ['@err' => $error->__toString()]));
        $this->messenger->addWarning(t('Unable to proceed. Please contact an administrator. Error code: 6AEA99C7-@line-@time', ['@line' => __LINE__, '@time' => time()]));
      }

      $query->condition('vid', "collections");
      $query->condition('name', "%" . $term_name . "%" , "LIKE");
      $query->range(0,1);
      $tids = $query->execute();

      if (count($tids) > 0) {
        $tid = key($tids);
      } else {
        try {
          $terms = $this->entityTypeManager->getStorage('taxonomy_term')
            ->loadTree("collections", 0, 1, TRUE);
        } catch (\Exception $error) {
          $this->loggerChannelFactory->get('product_sync')->alert(t('@err', ['@err' => $error->__toString()]));
          $this->messenger->addWarning(t('Unable to proceed. Please contact an administrator. Error code: 6AEA99C7-@line-@time', ['@line' => __LINE__, '@time' => time()]));
        }

        foreach ($terms as $term) {
          $termName = htmlentities($term->getName());
          $termName = str_replace('&lt;sup&gt;&amp;trade;&lt;/sup&gt;', '', $termName);
          $termName = str_replace('&lt;sup&gt;&amp;reg;&lt;/sup&gt;', '', $termName);
          $termName = str_replace('&reg;', '', $termName);
          $termName = str_replace('&trade;', '', $termName);

          if (strcmp($termName,$term_name) == 0) {
            $tid = $term->id();
          }
        }
      }

      return $tid;
   }

  /**
   * @param $sku
   *
   * @return array|void
   */
  public function isValidProductSKU($sku) {
    $config = $this->configFactory->get('delta_services.deltaservicesconfig');
    $endpoint  = $config->get('global_product_api_single');
    $basePath  = $config->get('api_base_path');
    $enpointUrl = $basePath . $endpoint;
    $enpointUrl = str_replace('{productSku}', $sku, $enpointUrl);
    $this->loggerChannelFactory->get('Brizo PDP')->info('PDP log- Before is valid product sku api call  - ' . $sku);
    $response = $this->deltaservice->apiCall( $enpointUrl, 'GET', [], [], "SINGLE_PRODUCT_IMPORT");
    $this->loggerChannelFactory->get('Brizo PDP')->info('PDP log- After is valid product sku api call  - ' . $sku);

    if (is_array($response) && count($response) > 0) {
      if ($response['status'] == 'SUCCESS' ) {
        $status = 1;
        $encodedResponse = json_decode($response['response']);
        $data = ['status' => $status, 'productData' => $encodedResponse];
      } else {
        $status = 0;
        $data = ['status' => $status, 'productData' => []];
      }

      return $data;
    }
  }

  /**
   * @param $data
   * @param $pid
   *
   * @return int|string|null
   */
  public function updateProductShown($data, $pid) {
    try {
      $product = $this->entityTypeManager->getStorage('commerce_product')->load($pid);
      $product->set('field_product_as_shown', json_encode($data));
      $product->save();
      return $product->id();
    } catch (\Exception $error) {
      $this->loggerChannelFactory->get('product_sync')->alert(t('@err', ['Unable to update product shown:@err' => $error->__toString()]));
      $this->messenger->addWarning(t('Unable to proceed. Please contact an administrator. Error code: 6AEA99C7-@line-@time', ['@line' => __LINE__, '@time' => time()]));
    }

    return NULL;
  }

  // Construct Product as shown and update in the product.
   /**
   * @param $data
   * @param $prodEntity
   *
   * @return null
   */
  public function constructProductShown($data,$prodEntity) {
   $this->loggerChannelFactory->get('product_import')->alert(t('In constructProductShown prd_id:@err', ['@err' => $prodEntity->id()]));
    $sku = $data->name;
    $output = [];
    if ($data->name == $data->values->BaseModel || $data->name == $data->values->ModelNumber) {
      $output['product_shown'][] = [
        'title'=> $data->description,
        'sku'=> $data->name,
        'price' =>  $data->values->ListPrice,
        'caprice' => isset($data->values->CAListPrice)?number_format($data->values->CAListPrice, 2):'',
      ];
      if (count($data->configOptions) > 0) {
        $output['product_shown'] = \Drupal::service('brizo_pdp.details')->getProductShownConfigData($output['product_shown'], $data->configOptions, $data->name, 1);
      }
      $prdshown = $output['product_shown'];
      $this->loggerChannelFactory->get('product_import')->info(t('In constructProductShown prdshown:@err', ['@err' => print_r($prdshown,1)]));
      $output['product_as_shown'] = \Drupal::service('brizo_pdp.details')->getPrdAsShown($prdshown, $sku);
      $this->loggerChannelFactory->get('product_import')->info(t('In constructProductShown  $output:@err', ['@err' => print_r($output,1)]));
    } else {
      $this->loggerChannelFactory->get('product_import')->info(t('In constructProductShown flow 2'));
      $productId = key($this->productExists($data->values->ModelNumber));
      if (!empty($productId)) {
        $title = \Drupal::service('brizo_pdp.details')->getProductTitle($productId);
        $price = \Drupal::service('brizo_pdp.details')->getProductPrice($productId);
        $caprice = \Drupal::service('brizo_pdp.details')->getProductCaprice($productId);
      } else {
        $modelNumberdata = $this->isValidProductSKU($data->values->ModelNumber);
        if ($modelNumberdata['status'] == 1) {
            $serice_config = $this->configFactory->get('delta_services.deltaservicesconfig');
            $siteName = $serice_config->get('global_site_name');
            $productId = $this->createProduct($modelNumberdata['productData'], $siteName);
            if (!empty($productId)) {
                $title = \Drupal::service('brizo_pdp.details')->getProductTitle($productId);
                $price = \Drupal::service('brizo_pdp.details')->getProductPrice($productId);
                $caprice = \Drupal::service('brizo_pdp.details')->getProductCaprice($productId);
            }
        }
      }


      $output['product_shown'][] = [
        'title'=> $title,
        'sku' => $data->values->ModelNumber,
        'price' =>  $price,
        'caprice' =>  $caprice,
        'optionLabel' => "PRD"
      ];
      if (count($data->configOptions) > 0) {
        $output['product_shown'] = \Drupal::service('brizo_pdp.details')->getProductShownConfigData($output['product_shown'], $data->configOptions, $data->name);
      }
      $output['product_as_shown'] = $output['product_shown'];
    } // else name = baseModal
    $this->loggerChannelFactory->get('product_import')->info(t('In constructProductShown product_shown:@err', ['@err' => print_r($output['product_shown'],1)]));
    $update_product = $this->updateProductShown($output['product_shown'], $prodEntity->id());
  }


}

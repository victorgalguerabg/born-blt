<?php

namespace Drupal\global_sync\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\ConfigFormBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Configure settings for this site.
 */
class SyncSettingsForm extends ConfigFormBase implements ContainerInjectionInterface {

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The Config storage.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configfactory;

  /**
   * The site path.
   *
   * @var string
   */
  protected $sitePath;


  /**
   * Constructs a new SyncSettingsForm.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   * @param $site_path
   */
  public function __construct(ConfigFactoryInterface $config_factory,$site_path) {
    $this->configfactory = $config_factory;
    $this->sitePath = $site_path;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('site.path')
    );
  }

  /**`
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'sync_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['global_sync.api_settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('global_sync.api_settings');

    $form['category_call_api'] = [
      '#type' => 'details',
      '#title' => $this->t('Category Related Calls'),
      '#open' => TRUE,
      '#weight' => 2,
    ];
    $form['category_call_api']['category_api'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Category Sync API'),
      '#default_value' => $config->get('rest_api_settings.category_api'),
      '#required' => TRUE,
      '#size' => 100,
    ];
    $form['category_call_api']['term_filter'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Name of the term to filter ex:Delta'),
      '#default_value' => empty($config->get('rest_api_settings.term_filter')) ? 'Delta' : $config->get('rest_api_settings.term_filter'),
      '#size' => 100,
    ];

    $form['category_call_api']['taxonomy_depth'] = [
      '#type' => 'select',
      '#title' => $this->t('Product category sync depth'),
      '#options' => ['1' => 1, '2' => 2, '3' => 3],
      '#default_value' => empty($config->get('rest_api_settings.taxonomy_depth')) ? 3 : $config->get('rest_api_settings.taxonomy_depth'),
      '#description' => $this->t('1:Parent Terms, 2: Child Terms, 3:Sibling Terms'),
    ];
    $form['facet_call_api'] = [
      '#type' => 'details',
      '#title' => $this->t('Facet Related Calls'),
      '#open' => TRUE,
      '#weight' => 2,
    ];
    $form['facet_call_api']['facet_api'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Facet Sync API'),
      '#default_value' => $config->get('rest_api_settings.facet_api'),
      '#required' => TRUE,
      '#size' => 100,
    ];
    $form['facet_call_api']['facet_size'] = [
      '#type' => 'number',
      '#title' => $this->t('Facet Size'),
      '#default_value' => $config->get('rest_api_settings.facet_size'),
      '#required' => TRUE,
      '#size' => 100,
    ];

    $form['product_data_sync'] = array(
      '#type' => 'details',
      '#title' => $this->t('Product Related Call'),
      '#open' => TRUE,
    );

    $form['product_data_sync']['pdp_sync_api_url'] = [
       '#type' => 'textfield',
       '#title' => $this->t('Product Sync API URL'),
       '#maxlength' => 255,
       '#default_value' => $config->get('pdp_sync_api_url'),
     ];

    $form['search_api'] = [
      '#type' => 'details',
      '#title' => $this->t('Search Related Calls'),
      '#open' => TRUE,
      '#weight' => 6,
    ];

    $form['search_api']['product_search'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Product Search API'),
      '#default_value' => $config->get('product_search'),
      '#required' => TRUE,
      '#size' => 255,
    ];

    $form['search_api']['product_per_page'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Product Per Page'),
      '#default_value' => $config->get('product_per_page'),
      '#required' => TRUE,
      '#size' => 5,
    ];

    $form['search_api']['document_search'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Document Search API'),
      '#default_value' => $config->get('document_search'),
      '#required' => TRUE,
      '#size' => 255,
    ];

    $form['search_api']['brand'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Brand'),
      '#default_value' => $config->get('brand'),
      '#required' => TRUE,
      '#size' => 255,
    ];

    $form['search_api']['document_per_page'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Document Per Page'),
      '#default_value' => $config->get('document_per_page'),
      '#required' => TRUE,
      '#size' => 255,
    ];

    $form['search_api']['global_search_products'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Search Products Autocomplete API'),
      '#default_value' => $config->get('global_search_products'),
      '#required' => TRUE,
      '#size' => 255,
    ];

    $form['search_api']['global_search_parts'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Search Parts Autocomplete'),
      '#default_value' => $config->get('global_search_parts'),
      '#required' => TRUE,
      '#size' => 255,
    ];
    $form['design_gallery'] = [
      '#type' => 'details',
      '#title' => $this->t('Design Gallery Related Calls'),
      '#open' => TRUE,
      '#weight' => 6,
    ];

    $form['design_gallery']['design_gallery_api'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Design Gallery API'),
      '#default_value' => $config->get('design_gallery_api'),
      '#required' => TRUE,
      '#size' => 255,
    ];

    $form['collection_api'] = [
      '#type' => 'details',
      '#title' => $this->t('Collection Detail Page Related Calls'),
      '#open' => TRUE,
      '#weight' => 6,
    ];

    $form['collection_api']['collection_api'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Collection Sync API'),
      '#default_value' => $config->get('collection_api'),
      '#required' => TRUE,
      '#size' => 255,
    ];
    $form['collection_api']['website'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Website'),
      '#default_value' => $config->get('website'),
      '#required' => TRUE,
      '#size' => 255,
    ];
    $form['collection_api']['category_bath'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Bath Category Text'),
      '#default_value' => $config->get('category_bath'),
      '#required' => TRUE,
      '#size' => 255,
    ];
    $form['collection_api']['category_kitchen'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Kitchen Category Text'),
      '#default_value' => $config->get('category_kitchen'),
      '#required' => TRUE,
      '#size' => 255,
    ];
    $form['collection_api']['category_bath_sinks'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Bath Sinks Category Text'),
      '#default_value' => $config->get('category_bath_sinks'),
      '#required' => TRUE,
      '#size' => 255,
    ];
    $form['collection_api']['category_kitchen_sinks'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Kitchen Sinks Category Text'),
      '#default_value' => $config->get('category_kitchen_sinks'),
      '#required' => TRUE,
      '#size' => 255,
    ];
    $form['collection_api']['collection_innovation_api'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Collection Innovation Feature API'),
      '#default_value' => $config->get('collection_innovation_api'),
      '#required' => TRUE,
      '#size' => 255,
    ];
    $form['collection_api']['collection_product_image'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Collection Product Image attribute'),
      '#default_value' => $config->get('collection_product_image'),
      '#required' => TRUE,
      '#size' => 255,
    ];
    $form['collection_api']['collection_available_finish_api'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Collection Available Finishes API'),
      '#default_value' => $config->get('collection_available_finish_api'),
      '#required' => TRUE,
      '#size' => 255,
    ];

    $form['collection_api']['collection_detail_api'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Collection Detail API'),
      '#default_value' => $config->get('collection_detail_api'),
      '#required' => TRUE
    ];

    $form['collection_api']['collection_detail_finish_api'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Collection Detail Finish Facets API'),
      '#default_value' => $config->get('collection_detail_finish_api'),
      '#required' => TRUE
    ];

    $form['collection_api']['collection_detail_items_per_page'] = [
      '#type' => 'number',
      '#title' => $this->t('Items per Page'),
      '#description' => $this->t('Number of items per page in collection detail page.'),
      '#default_value' => $config->get('collection_detail_items_per_page'),
    ];
    $form['collection_api']['skip_default_model'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Unset defaultModel api param'),
      '#description' => $this->t('For brizo specific, Page - CDP, Application will rid defaultModel eq true when the configured value matched. Sample Data:brizo_bath_jason wu for brizo™, For multiple patterns use comma separator.'),
      '#default_value' => $config->get('skip_default_model'),
    ];

    $form['product_resources'] = [
      '#type' => 'details',
      '#title' => $this->t('Product Resources Page Related Calls'),
      '#open' => TRUE,
      '#weight' => 6,
    ];

    $form['product_resources']['product_resources_api'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Product Resources API'),
      '#description' => $this->t('Product Resources API.'),
      '#default_value' => $config->get('product_resources_api'),
      '#required' => TRUE
    ];

    $form['product_resources']['product_resources_per_page'] = [
      '#type' => 'number',
      '#title' => $this->t('Items per Page'),
      '#description' => $this->t('Number of items per page.'),
      '#default_value' => $config->get('product_resources_per_page'),
      '#required' => TRUE
    ];



    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('global_sync.api_settings')
      ->set('rest_api_settings.category_api', $form_state->getValue('category_api'))
      ->set('rest_api_settings.facet_api', $form_state->getValue('facet_api'))
      ->set('rest_api_settings.facet_size', $form_state->getValue('facet_size'))
      ->set('rest_api_settings.taxonomy_depth', $form_state->getValue('taxonomy_depth'))
      ->set('rest_api_settings.product_website', $form_state->getValue('product_website'))
      ->set('rest_api_settings.term_filter', $form_state->getValue('term_filter'))
      ->set('collection_api', $form_state->getValue('collection_api'))
      ->set('website', $form_state->getValue('website'))
      ->set('brand', $form_state->getValue('brand'))
      ->set('category_kitchen', $form_state->getValue('category_kitchen'))
      ->set('category_bath', $form_state->getValue('category_bath'))
      ->set('category_kitchen_sinks', $form_state->getValue('category_kitchen_sinks'))
      ->set('category_bath_sinks', $form_state->getValue('category_bath_sinks'))
      ->set('collection_product_image', $form_state->getValue('collection_product_image'))
      ->set('collection_innovation_api', $form_state->getValue('collection_innovation_api'))
      ->set('collection_available_finish_api', $form_state->getValue('collection_available_finish_api'))
      ->set('pdp_sync_api_url', $form_state->getValue('pdp_sync_api_url'))
      ->set('product_search', $form_state->getValue('product_search'))
      ->set('product_per_page', $form_state->getValue('product_per_page'))
      ->set('document_search', $form_state->getValue('document_search'))
      ->set('document_per_page', $form_state->getValue('document_per_page'))
      ->set('global_search_products', $form_state->getValue('global_search_products'))
      ->set('global_search_parts', $form_state->getValue('global_search_parts'))
      ->set('design_gallery_api', $form_state->getValue('design_gallery_api'))
      ->set('collection_detail_finish_api', $form_state->getValue('collection_detail_finish_api'))
      ->set('collection_detail_api', $form_state->getValue('collection_detail_api'))
      ->set('collection_detail_items_per_page', $form_state->getValue('collection_detail_items_per_page'))
      ->set('product_resources_api', $form_state->getValue('product_resources_api'))
      ->set('product_resources_per_page', $form_state->getValue('product_resources_per_page'))
      ->set('skip_default_model', $form_state->getValue('skip_default_model'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}

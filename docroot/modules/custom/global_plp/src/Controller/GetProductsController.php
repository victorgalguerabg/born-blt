<?php

namespace Drupal\global_plp\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\delta_services\DeltaService;
use Drupal\delta_cache\DeltaCacheService;
use Symfony\Component\PropertyAccess\PropertyAccess;

/**
 * Class GetProductsController.
 */
class GetProductsController extends ControllerBase {

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Symfony\Component\HttpFoundation\RequestStack definition.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The default cache bin.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * Drupal\delta_services\DeltaService definition.
   *
   * @var \Drupal\delta_services\DeltaService
   */
  protected $deltaservice;

  /**
   * Drupal\delta_cache\DeltaCacheService definition.
   *
   * @var \Drupal\delta_cache\DeltaCacheService
   */
  protected $deltaCache;

  /**
   * Constructs a new GetProductsController object.
   *
   * @param Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   The default cache bin.
   * @param \Drupal\delta_services\DeltaService $deltaservice
   *   The deltaservice to make API calls.
   */
  public function __construct(ConfigFactoryInterface $config_factory,
                              RequestStack $request_stack,
                              DeltaService $deltaservice,
                              DeltaCacheService $deltaCache) {
    $this->configFactory = $config_factory;
    $this->requestStack = $request_stack;
    $this->deltaservice = $deltaservice;
    $this->deltaCache = $deltaCache;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('request_stack'),
      $container->get('delta_services.service'),
      $container->get('delta_cache.cache_service')
    );
  }

  /**
   * Get products from api based on Category, size and pagenumber.
   *
   * @return JSON
   *   A properly formatted json response from api.
   */
  public function getProducts() {
    $secondaryCategory = '';
    $categories = '';
    $queryString = $this->requestStack->getCurrentRequest()->query->all();
    $cacheKeyData = $queryString['cacheKeyData'];
    $pagenumber = is_numeric($queryString['pagenumber']) ? $queryString['pagenumber'] : 0;
    $size = $queryString['size'];
    $cache = $queryString['cache'];
    if (array_key_exists('categoryLevel', $queryString)) {
      $categoryLevel = $queryString['categoryLevel'];
    }
    $response = new Response();
    $defaultModel = (array_key_exists('defaultModel', $queryString)) ? $queryString['defaultModel'] : '';
    if($defaultModel == 'false'){
      $cacheId = 'plp_filters_'.$cacheKeyData."_".$pagenumber;
    }
    //$cacheId = $this->getCid($queryString);
    else{
      $cacheId = $cacheKeyData."_".$pagenumber;
    }
    if ($cache == 'T') {
      $cachedResult = $this->deltaCache->deltaGetCache($cacheId);
      if (isset($cachedResult)  && !empty($cachedResult)) {
        $response->setContent($cachedResult);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
      }
    }

    $plpDetails = $this->configFactory->get('delta_services.plpconfig');
    $prop_config = $this->configFactory->get('product_details.settings');
    $currentDate = strtotime(date('Y-m-d', time()));
    $baseCategory = NULL;
    $categoryCount = $queryString['categoryCount'];
    $categoryValue = (array_key_exists('category', $queryString)) ? $queryString['category'] : [];
    $productFacets = (array_key_exists('productFacets', $queryString)) ? $queryString['productFacets'] : '';
    $cacheKeyData = $queryString['cacheKeyData'];
    $defaultModel = (array_key_exists('defaultModel', $queryString)) ? $queryString['defaultModel'] : '';
    $categoryURL = '';
    $checkcategory = [];
    $recomercePlp = 0;
    for ($i = 0; $i < $categoryCount; $i++) {
      if(is_array($categoryValue) && count($categoryValue)){
        if(strpos($categoryValue[$i], "Recommerce") !== false) {
          $recomercePlp = 1;
        }
        $categoryURL .= '&or_categories=' . rawurlencode($categoryValue[$i]);
        $checkcategory[] = $categoryValue[$i];
      }
    }
    // $categories = $categoryURL;
    if (!$size) {
      $size = $plpDetails->get('product_listing_items_per_page');
    }
    $facetParams = $queryString['facets'];

    if (array_key_exists('sort', $queryString)) {
      $sort = $queryString['sort'];
    }
    if (array_key_exists('showerSystemFilter', $queryString)) {
      $showerSystemFilter = $queryString['showerSystemFilter'];
    }
    if (array_key_exists('secondaryCategory', $queryString) && !empty($queryString['secondaryCategory'])) {
      $secondaryCategory = $queryString['secondaryCategory'];
    }

    $category_seperate = explode("_", $categories);
    if ($category_seperate[0] == "Delta") {
      $baseCategory = strtolower($category_seperate[1]);
    }
    $placeholders = ['{pagenumber}', '{size}'];
    $replacements = [$pagenumber, $size];

    $plpDetails->get('product_listing_api');
    $productApiArgs = str_replace(
      $placeholders,
      $replacements,
      $plpDetails->get('product_listing_api')
    );
    if ($recomercePlp) {
      $productApiArgs = str_replace('excludeRecommerce=true','excludeRecommerce=false', $productApiArgs);
    }
    if (strpos($facetParams, 'includeFerguson=true') !== false) {
      $productApiArgs = str_replace('&FltWebExclusiveCustomerItem=F','', $productApiArgs);
    }

    $apiBaseDetails = $this->configFactory
      ->get('delta_services.deltaservicesconfig');

    $productAPIUrl = $apiBaseDetails->get('api_base_path');
    if (strpos($queryString['facets'], 'availableToTrade') !== FALSE) {
      $productApiArgs = str_replace('includeFuture=false&', "and_availableToTrade=true&", $productApiArgs);
    }

    $productAPIUrl .= $productApiArgs . '&' . $categoryURL . '&' . $facetParams . '&' . $sort;
    if($secondaryCategory != ''){
      $productAPIUrl .= '&and_categories=' . rawurlencode($secondaryCategory);
    }
    if (!empty($productFacets) && $productFacets != '') {
      $productAPIUrl .= $productFacets;
    }

    if (strpos($facetParams, 'includeFuture') == true) {
      $productAPIUrl .= '&comingsoon=false';
    }
    // For Brizo finish filters, defaultModel=true removed from the API call.
    if($defaultModel == 'false' && strpos($productAPIUrl, 'defaultModel=true') !== false){
      $productAPIUrl = str_replace('defaultModel=true', '', $productAPIUrl);
    }
    $result = $this->deltaservice->apiCall($productAPIUrl, "GET", [], [], "PLP");
    if($recomercePlp == 1) {
      $recommerceConfig = $this->configFactory->get('delta_component_formatter.recommerce_pdp_config_form');
      $plp_product_list_price = $recommerceConfig->get('plp_product_list_price');
      $addListPrice = $this->deltaservice->getListPrice($result, $plp_product_list_price);
      $encodedResponse = json_decode($addListPrice['response']);
    }
    else {
      $excludeRecommerceResult = $this->deltaservice->excludeRecommerce($result);
      $encodedResponse = json_decode($excludeRecommerceResult);
    }

    $termName = [];
    // For showing only applicable facets as per API data.
    $propertyAccessor = PropertyAccess::createPropertyAccessor();
    if ($propertyAccessor->isReadable($encodedResponse, 'facets')) {
      $facetCategories = $propertyAccessor->getValue($encodedResponse, 'facets');
      foreach ($facetCategories as $key => $value) {
        $facetName = $propertyAccessor->getValue($value, 'name');
        $termresult = [];
        if ($facetName == "categories") {
          $keyvalue = $key;
          if (!empty($checkcategory)) {
            $terms = $propertyAccessor->getValue($value, 'terms');
            foreach ($terms as $key => $termvalue) {
              $termName = $propertyAccessor->getValue($termvalue, 'term');
              // To show only top-most level of category in Brizo. Controlled by a flag in the node.
              if(!empty($categoryLevel) && $categoryLevel == 1) {
                $brizoTerm = explode("_",$termName);
                if(count($brizoTerm) <= 3) {
                  foreach ($checkcategory as $key => $checkCategoryValue) {
                    $pos = strpos($termName, $checkCategoryValue);
                    if ($pos !== FALSE && $termName != $checkCategoryValue) {
                      $termresult[] = $termvalue;
                    }
                  }
                }
              }
              else{
                //Get current site from multi site instance if it is brizo proceed.
                $site_path = \Drupal::service('site.path'); // e.g.: 'sites/default'
                $site_path = explode('/', $site_path);
                $site_name = $site_path[1];
                if($site_name == 'brizo') {
                  if(in_array($termName,$checkcategory)) {
                    $termresult[] = $termvalue;
                  }
                } else {
                  foreach ($checkcategory as $key => $checkCategoryValue) {
                    $pos = strpos($termName, $checkCategoryValue);

                    if ($pos !== FALSE && $termName != $checkCategoryValue) {
                      $termresult[] = $termvalue;
                    }
                  }
                }
              }
            }
          }
          $name = 'facets[' . $keyvalue . '].terms';
          $propertyAccessor->setValue($encodedResponse, $name, $termresult);
        }
      }
    }
    $output = json_encode($encodedResponse);
    if ($result['status'] == 'SUCCESS') {
      if($cache == 'T'){
        $this->deltaCache->deltaSetCache($cacheId, $output);
      }
      $response->setContent($output);
      $response->headers->set('Content-Type', 'application/json');
    }
    else {
      $response->setContent('API Response - ERROR');
    }
    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function getCid($queryString = []) {
    return implode('&', $queryString);
  }
}

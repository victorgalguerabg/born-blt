<?php

namespace Drupal\global_plp\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class DeltaServicesConfigForm.
 */
class PLPConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'delta_services.plpconfig',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'plp_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('delta_services.plpconfig');
    $form['product_listing_api'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Product Listing API'),
      '#description' => $this->t('Product Listing API details excluding basepath.'),
      '#default_value' => $config->get('product_listing_api'),
    ];
    $form['pdp_parts_api'] = [
      '#type' => 'textarea',
      '#title' => $this->t('PDP Parts API'),
      '#description' => $this->t('PDP Parts API'),
      '#default_value' => $config->get('pdp_parts_api'),
    ];
    $form['product_listing_items_per_page'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Products count per page'),
      '#description' => $this->t('Number of products to display in a page.'),
      '#default_value' => $config->get('product_listing_items_per_page'),
    ];
    $form['product_listing_items_per_page_options'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Products count per page drop-down options'),
      '#description' => $this->t('Enter Number of products to display in a page options as comma separated values.'),
      '#default_value' => $config->get('product_listing_items_per_page_options'),
    ];
    $form['product_listing_handle_order'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Products page filter handle sort order'),
      '#description' => $this->t('In order which handle should be listed.'),
      '#default_value' => $config->get('product_listing_handle_order'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Can add validation if required.
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('delta_services.plpconfig')
      ->set('product_listing_api', $form_state->getValue('product_listing_api'))
      ->set('pdp_parts_api', $form_state->getValue('pdp_parts_api'))
      ->set('product_listing_items_per_page', $form_state->getValue('product_listing_items_per_page'))
      ->set('product_listing_items_per_page_options', $form_state->getValue('product_listing_items_per_page_options'))
      ->set('product_listing_handle_order', $form_state->getValue('product_listing_handle_order'))
      ->save();
  }
}

<?php

namespace Drupal\delta_custom_blocks\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'Checkout Header' block.
 *
 * @Block(
 *  id = "checkout_header",
 *  admin_label = @Translation("Checkout Header block"),
 * )
 */
class CheckoutHeaderBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];
    $block = [
      '#theme' => 'block_checkout_header',
      '#attributes' => [
        'class' => ['checkout-header'],
        'id' => 'checkout-header-block',
      ],
    ];
    $build['checkout_header'] = $block;
    return $build;
  }
}
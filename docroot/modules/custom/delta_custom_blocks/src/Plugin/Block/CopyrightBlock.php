<?php

namespace Drupal\delta_custom_blocks\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'CopyrightBlock' block.
 *
 * @Block( 
 *  id = "copyright_block",
 *  admin_label = @Translation("Copyright block"),
 * )
 */
class CopyrightBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
  $build = []; 
  $year = date('Y');
  $block = [
    '#theme' => 'block_copyright',
    '#attributes' => [
        'class' => ['copyright'],
        'id' => 'copyright-block',
      ],
    '#year'  => $year,
    '#cache' => [
        'max-age' => strtotime('01/01/'.(date('Y')+1)) - time(),
      ],
    ];
    $build['copyright_block'] = $block;
    return $build;
  }
}
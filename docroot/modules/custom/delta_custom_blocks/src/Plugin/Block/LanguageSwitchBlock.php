<?php

namespace Drupal\delta_custom_blocks\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'Language Switch' block.
 *
 * @Block(
 *  id = "language_switch_block",
 *  admin_label = @Translation("Language Switch block"),
 * )
 */
class LanguageSwitchBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];
    $items = [
      ['url' => '#',
      'content' => 'USA - Eng',
      'class' => 'lng-item',
      'title' => 'Open in same window',
        ],
      ['url' => 'https://es.deltafaucet.com/',
        'content' => 'USA - Esp',
        'class' => 'lng-item',
        'title' => 'Open in new window',
      ],
      ['url' => 'http://www.deltafaucet.ca/index.html',
        'content' => 'Canada - Eng',
        'class' => 'lng-item',
        'title' => 'Open in new window',
      ],
      ['url' => 'https://fr.deltafaucet.ca/index.html',
        'content' => 'Canada-fr',
        'class' => 'lng-item',
        'title' => 'Open in new window',
      ],
      ['url' => 'https://worldwide.deltafaucet.com/index.html',
        'content' => 'Worldwide - Eng',
        'class' => 'lng-item',
        'title' => 'Open in new window',
      ],
      ['url' => 'https://www.deltafaucet.com.br/home.html',
        'content' => 'Brasil (PT)',
        'class' => 'lng-item',
        'title' => 'Open in new window',
      ],
      ['url' => 'https://www.deltafaucet.com.cn/index.html',
        'content' => 'China (中文)',
        'class' => 'lng-item',
        'title' => 'Open in new window',
      ],
      ['url' => 'https://www.deltafaucet.co.in/',
      'content' => 'India - Eng',
      'class' => 'lng-item',
      'title' => 'Open in new window',
    ],
    ];
    $block = [
      '#theme' => 'block_language_switch',
      '#block' => $items,
    ];
    $build['block_language_switch'] = $block;
    return $build;
  }
}

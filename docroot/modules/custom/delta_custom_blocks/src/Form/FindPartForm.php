<?php

namespace Drupal\delta_custom_blocks\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Url;

/**
 * Implementation Find a Part form.
 */
class FindPartForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'find_part_form';
  }

  /**
   * Implementation Find and replacement form build().
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = [];
    $form['search'] = [
      '#type' => 'search',
      '#title' => t('Enter your Product or Part number here:'),
      '#autocomplete_route_name' => 'global_search.autocomplete',
      '#autocomplete_route_parameters' => ['field_name' => 'search_input', 'count' => 3],
      '#attributes' => [
        'placeholder' => t('Search'),
        'class' => ['form-item__textfield form-item__textfield--required search-input header-search-input'],
      ],
      '#suffix' => '<div id="find-a-part-autocomplete-results"></div>',
];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => 'Search',
      '#attributes' => ['class' => ['search-icon']],
    ];

    //$form['#theme'] = 'find_replacement_parts';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Route to the search page with searched keywords.
    $filter = $form_state->getUserInput();
    $filtersArr = [];
    $searchPattern = Xss::filter($filter['search']);
    $filtersArr = ['title' => $searchPattern];
    $url = Url::fromRoute('global_search.search', $filtersArr);
    $form_state->setRedirectUrl($url);
  }

}

<?php
namespace Drupal\delta_service_and_parts\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Drupal\delta_services\DeltaService;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Defines a route controller for service and parts landing page.
 */
class ServiceAndPartsController extends ControllerBase {

  /**
   * To get current query string.
   *
   * @var Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Drupal\delta_services\DeltaService definition.
   *
   * @var \Drupal\delta_services\DeltaService
   */
  protected $deltaservice;

  /**
   * To handle response.
   *
   * @var response
   */
  public $response;

  /**
   * To handle response.
   *
   * @var response
   */
  public $domain;

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Implementation of constructor.
   */
  public function __construct(DeltaService $deltaservice, RequestStack $requestStack, ConfigFactoryInterface $config_factory) {
    $this->deltaservice = $deltaservice;
    $this->requestStack = $requestStack;
    $this->configFactory = $config_factory;
    $this->response = new Response();
    $this->domain = 'https://api.deltafaucet.com';
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
    // Load the service required to construct this class.
      $container->get('delta_services.service'),
      $container->get('request_stack'),
      $container->get('config.factory')
    );
  }

  /**
   * Method to attach library to guided search landing page.
   * @return array
   */
 public function getProductIdentification () {
    
    $response = new RedirectResponse('customer-support-parts',301);
    $response->send();
    return;
    /*return [
     '#theme' => 'service_and_parts_page',
     '#data' => [],
     '#attached' => [
       'library' =>
         [
           'delta_service_and_parts/productIdentification',
         ],
       'drupalSettings' => []
     ],
   ];*/
 }
  /**
   * Method to attach library to guided search find a part landing page.
   * @return array
   */
  public function getFindAPart () {
    return [
      '#theme' => 'service_and_parts_find_a_part',
      '#data' => [],
      '#attached' => [
        'library' =>
          [
            'delta_service_and_parts/productIdentification',
          ],
        'drupalSettings' => []
      ],
    ];
  }
  /**
   * Method to get guided search results.
   * @return Response
   */
 public function getRoomTabOutput() {
   $method = 'GET';
   $data = '';
   $request = $this->requestStack->getCurrentRequest();
   $config = $this->config('sync.api_settings');
   $apiBaseDetails = $this->configFactory
     ->get('delta_services.deltaservicesconfig');
   $domain = $apiBaseDetails->get('api_base_path');
   $searchApiUrl = $this->domain . $config->get('guided_search');
   $tab = $request->get('tab');
   $prevResponse = $request->get('prevResponse');
   $selectedResponse = $request->get('selectedResponse');
   $currentStep = $request->get('currentStep');
   switch($tab) {
     case 'product-type':
       $method = 'POST';
       $searchApiUrl = $searchApiUrl . '/step/1';
       foreach($prevResponse['steps'] as $steps_key => $steps) {
         foreach($steps['questions'] as $question_keys => $questions) {
           $prevResponse['steps'][$steps_key]['questions'][$question_keys]['answerCode'] = $selectedResponse;
         }
       }
       break;
     case 'product-description':
       $method = 'POST';
       $searchApiUrl = (($currentStep == 'STEP3' && $selectedResponse))? $searchApiUrl . '/step/3' : $searchApiUrl . '/step/2';
       foreach($prevResponse['steps'] as $steps_key => $steps) {
         if($currentStep == $steps['stepId']) {
           foreach($steps['questions'] as $question_keys => $questions) {
             if($currentStep == 'STEP3' && $selectedResponse){
               if($selectedResponse[$question_keys]){
                 $prevResponse['steps'][$steps_key]['questions'][$question_keys]['answerCode'] = $selectedResponse[$question_keys];
               }
             }
             else {
               $prevResponse['steps'][$steps_key]['questions'][$question_keys]['answerCode'] = $selectedResponse;
             }
           }
         }
       }
       break;
   }
   $guidedRequest = $this->deltaservice->apiCall(
     $searchApiUrl, $method, [], $prevResponse, "Search"
   );
   $this->response->setContent($guidedRequest['response']);
   $this->response->headers->set('Content-Type', 'application/json');
   return $this->response;
 }

  /**
   * Method to get products list.
   * @return Response
   */
 public function getGuidedSearchProducts () {
   $request = $this->requestStack->getCurrentRequest();
   $url = $request->get('url');
   $page = $request->get('page');
   if($page) {
     $url = $url . '&page='. $page;
   }

   $productRequest = $this->deltaservice->apiCall(
     $url, "GET", [], "", "Search"
   );

   $this->response->setContent($productRequest['response']);
   $this->response->headers->set('Content-Type', 'application/json');
   return $this->response;
 }

  /**
   * Method to get troubleshoot steps.
   */
 public function getGuidedSearchTroubleshoot() {
   $method = 'GET';
   $prevResponse = '';
   $request = $this->requestStack->getCurrentRequest();
   $config = $this->config('sync.api_settings');
   $apiBaseDetails = $this->configFactory
     ->get('delta_services.deltaservicesconfig');
   $domain = $apiBaseDetails->get('api_base_path');
   $troubleshootApiUrl = $this->domain . $config->get('guided_search_troubleshoot');
   $prevResponse = $request->get('prevResponse');
   $selectedResponse = $request->get('selectedResponse');
   $stepId = $request->get('stepId');
   if($selectedResponse) {
     $method = 'POST';
     $troubleshootApiUrl = $troubleshootApiUrl . '/step/1';
     foreach($prevResponse['steps'] as $steps_key => $steps) {
       foreach($steps['questions'] as $question_keys => $questions) {
         if ($stepId == 3) {
           $prevResponse['steps'][$steps_key]['questions'][1]['answerCode'] = $selectedResponse;
         }
         else {
           $prevResponse['steps'][$steps_key]['questions'][$question_keys]['answerCode'] = $selectedResponse;
         }
       }
     }
   }
   $guidedRequest = $this->deltaservice->apiCall(
     $troubleshootApiUrl, $method, [], $prevResponse, "Search"
   );
   $this->response->setContent($guidedRequest['response']);
   $this->response->headers->set('Content-Type', 'application/json');
   return $this->response;
 }
}
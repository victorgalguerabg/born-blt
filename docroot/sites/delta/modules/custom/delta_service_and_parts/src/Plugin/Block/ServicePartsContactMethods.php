<?php

namespace Drupal\delta_service_and_parts\Plugin\block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Provides my custom block.
 *
 * @Block(
 *   id = "service_contact_methods",
 *   admin_label = @Translation("Service Contact Methods"),
 *   category = @Translation("Blocks")
 * )
 */
class ServicePartsContactMethods extends BlockBase implements BlockPluginInterface, ContainerFactoryPluginInterface {


  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   *
   */
  protected $entityManager;

    /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entityManager, ConfigFactoryInterface $configFactory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entityManager;
    $this->configFactory = $configFactory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration, $plugin_id, $plugin_definition, $container->get('entity_type.manager'), $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {    
    $config = $this->configFactory->get('sync.api_settings');
    $bid = $config->get('service_contact_method');
    if(empty($bid)){
      return [];
    }
    $content = $this->renderBlock($bid);    
    if(!empty($content)){
      return [
        '#theme' => 'service_contact_methods',
        '#content' => $content
      ];
    }else{
      return [];
    }      
  }

  //render block content
  public function renderBlock($bid){
    $content = '';
    $blockObj = $this->entityTypeManager->getStorage('block_content')->load($bid);
    if (!is_null($blockObj)) {
      if($blockObj->hasField('body')){
        $getBodyArr = $blockObj->get('body')->getValue();
        $content = $getBodyArr[0]['value'];
      }
    }
    return $content;
  }
}
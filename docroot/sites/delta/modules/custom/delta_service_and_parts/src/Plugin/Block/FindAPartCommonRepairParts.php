<?php
namespace Drupal\delta_service_and_parts\Plugin\block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides my custom block.
 *
 * @Block(
 *   id = "find_a_part_common_repair_parts",
 *   admin_label = @Translation("Find a part Common repair parts finder"),
 *   category = @Translation("Blocks")
 * )
 */
class FindAPartCommonRepairParts extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#theme' => 'find_a_part_common_repair_parts',
    ];
  }
}
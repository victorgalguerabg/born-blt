<?php

namespace Drupal\delta_service_and_parts\Plugin\block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides my custom block.
 *
 * @Block(
 *   id = "product_identifier",
 *   admin_label = @Translation("Product Identifier block"),
 *   category = @Translation("Blocks")
 * )
 */
class ProductIdentifier extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build =  [
      '#theme' => 'service_and_parts_page',
      '#data' => [],
      '#attached' => [
        'library' =>
          [
            'delta_service_and_parts/productIdentification',
          ],
        'drupalSettings' => []
      ],
    ];
    return $build;
  }
}
<?php

namespace Drupal\delta_service_and_parts\Plugin\block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides my custom block.
 *
 * @Block(
 *   id = "product_identifier_tool",
 *   admin_label = @Translation("Product Identifier tool block - service and parts section"),
 *   category = @Translation("Blocks")
 * )
 */
class ProductIdentifierTool extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build =  [
      '#theme' => 'product_identifier_tool',
      '#data' => [],
      '#attached' => [
        'library' =>
          [
            'delta_service_and_parts/productIdentification',
          ],
        'drupalSettings' => []
      ],
    ];
    return $build;
  }
}

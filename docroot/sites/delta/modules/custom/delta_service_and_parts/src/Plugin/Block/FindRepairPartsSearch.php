<?php
namespace Drupal\delta_service_and_parts\Plugin\block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides my custom block.
 *
 * @Block(
 *   id = "find_a_part_common_repair_parts_search",
 *   admin_label = @Translation("Find a part Common repair parts search block"),
 *   category = @Translation("Blocks")
 * )
 */
class FindRepairPartsSearch extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#theme' => 'repair_parts_and_resources',
    ];
  }
}

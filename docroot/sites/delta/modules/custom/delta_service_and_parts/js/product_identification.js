(function ($, Drupal, drupalSettings) {
    Drupal.behaviors.ServiceAndPArts = {
        attach: function (context, settings) {
            let loadProducts = true;
            let pageNumber = 1;
            let lastPage = false;
            let finishDataIcon = drupalSettings.finishImages;
            let setFinishClass = "no-round";
            let ignoreListFinish = ['na','notapplicable'];
            let ignoreStockTypeFinish = ['Obsolete','Use Up'];
            let _finishList = [];
            let _defaultFinish = '';
            let loader = $('.ajax-progress-throbber');
            $('document').ready(function () {
                drupalSettings.tab = 'room';
                $( ".service-parts__tabpanel" ).on( "click", ".service-tab-trigger", function(e) {
                    e.preventDefault();
                    var postValues = [];
                    var tab = $(this).attr('data');
                    var selectedResponse = $(this).attr('data-value');
                    var stepId = $(this).attr('nextStep');
                    var currentStep = $(this).attr('data-stepid');
                    var postValues = {};
                    postValues['tab'] = tab;
                    postValues['selectedResponse'] = selectedResponse;
                    postValues['step'] = stepId;
                    postValues['currentStep'] = currentStep;
                    getProductIdentificationResults(postValues);
                });
                $(".service-parts__tabpanel").on("click", ".product-description-output .image-with-link-block", function(e) {
                    e.preventDefault();
                    var postValues = [];

                    if ($(this).hasClass('disabled')) {
                      return false;
                    }

                    $(this).parent(".sp-third-tab-img").parent(".accordion-def").prev(".accordion-term").find(".acc-ans-link").remove();
                    var acc_ans_link = document.createElement('a');
                    var selectedData = $(this).find('.img-link-title a').attr('data');
                    var questionNo = $(this).find('.img-link-title a').attr('question');
                    acc_ans_link.setAttribute('data', selectedData);
                    acc_ans_link.setAttribute('question', questionNo);
                    $(acc_ans_link).appendTo($(this).parent(".sp-third-tab-img").parent(".accordion-def").prev(".accordion-term").find(".acc-ans"));
                    $(".acc-ans").find("a").addClass("acc-ans-link");
                    var img_title = $(this).find(".img-link-title").text();
                    $(this).parent(".sp-third-tab-img").parent(".accordion-def").prev(".accordion-term").find("label").addClass("sp-label-msg");
                    $(this).parent(".sp-third-tab-img").parent(".accordion-def").prev(".accordion-term").find(".acc-ans").find("a").text(img_title);

                    $(this).parent(".sp-third-tab-img").parent(".accordion-def").prev(".accordion-term").removeClass("is-active");
                    $(this).parent(".sp-third-tab-img").parent(".accordion-def").next(".accordion-term").addClass("is-active");
                    var values = [];
                    $('.acc-ans-link').each(function(){
                        var selectedAnswer = $(this).attr('data');
                        var questionNo = $(this).attr('question');
                        //values.push({questionNo: selectedAnswer});
                        values[questionNo] = selectedAnswer;
                    });
                    postValues['tab'] = 'product-description';
                    postValues['currentStep'] = 'STEP3';
                    postValues['step'] = 4;
                    postValues['selectedResponse'] = values;
                    getProductIdentificationResults(postValues);
                });
                $(".service-parts__tabpanel").on("click", ".acc-ans", function() {
                    $(this).find(".acc-ans-link").remove();
                    var postValues = [];
                    $(".accordion-term").removeClass("is-active");
                    $(this).parent(".accordion-term").addClass("is-active");
                    var values = [];
                    $('.acc-ans-link').each(function(){
                        var selectedAnswer = $(this).attr('data');
                        var questionNo = $(this).attr('question');
                        //values.push({questionNo: selectedAnswer});
                        values[questionNo] = selectedAnswer;
                    });
                    postValues['tab'] = 'product-description';
                    postValues['currentStep'] = 'STEP3';
                    postValues['step'] = 4;
                    postValues['selectedResponse'] = values;
                    getProductIdentificationResults(postValues);
                });
                $('.guided-search-viewMatchingLink').click(function () {
                    var url = $(this).attr('data');
                    getProductsList(url);
                });
                $(".service-parts__tabpanel").on("click", ".tab4-issue-identifier", function(e) {
                    e.preventDefault();
                    var postValues = [];
                    drupalSettings.modelNumber = $(this).attr('data');
                    postValues['step'] = 1;
                    postValues['selectedResponse'] = 0;
                    getTroubleshoot(postValues);
                });
                $(".service-parts__tabpanel").on("click", ".troubleshoot-option-trigger", function(e) {
                    e.preventDefault();
                    var postValues = [];
                    var selectedResponse = $(this).attr('data-value');
                    var stepId = $(this).attr('data-stepid');
                    postValues['step'] = parseInt(stepId) + 1
                    postValues['selectedResponse'] = selectedResponse;
                    getTroubleshoot(postValues);
                });
                $('.troubleshoot-back').click(function () {
                    $('.issues-product-output').show();
                    $('.issues-troubleshoot', '.issues-repair').hide();
                });
            });

            var TrobuleshootView = Backbone.View.extend({
                template: _.template($("#troubleshoot-question-node").html()),
                template2: _.template($("#troubleshoot-answer-node").html()),
                initialize: function (initData, stepId) {
                    this.renderTroubleshootQuestion(initData, stepId);
                    this.renderTroubleshootAnswer(initData, stepId);
                },
                renderTroubleshootQuestion: function (initData, stepId) {
                    var _self = this;
                    _.each(initData.steps, function (stepsEntry) {
                        _.each(stepsEntry.questions, function (entry) {
                            var _render = "";
                            _question = entry.questiontext;
                            _render += _self.template({
                                question: _question,
                            });
                            $('#troubleshoot-question').html(_render);
                        });
                    });

                },
                renderTroubleshootAnswer: function (initData, stepId) {
                    var _self = this;
                    _.each(initData.steps, function (stepsEntry) {
                        _.each(stepsEntry.questions, function (answerEntry) {
                            var _render = "";
                            _.each(answerEntry.answers, function (entry) {
                                _render += _self.template2({
                                    title: entry.answerDisplayValue,
                                    thumbnail: entry.thumbnail,
                                    value: entry.key,
                                    matchingproductcount: entry.matchingproductcount,
                                    stepId: stepId,
                                });
                            });
                            $('#troubleshoot-answers').html(_render);
                        });
                    });
                }
            });


            /*
            * Method to create the dynamic elements and render them in the template.
             */
            var ProdView = Backbone.View.extend({
                template: _.template($("#output-question-node").html()),
                template2: _.template($("#output-answer-node").html()),
                template3: _.template($("#output-accordion-question").html()),
                template4: _.template($("#output-accordion-answers").html()),
                initialize: function (initData, outputTarget, stepId) {
                    stepId = parseInt(stepId);
                    switch(stepId) {
                        case 1:
                        case 2:
                            this.renderQuestion(initData, outputTarget, stepId);
                            this.renderAnswer(initData, outputTarget, stepId);
                            break;
                        case 3:
                        case 4:
                            this.renderAccordionQuestions(initData, stepId);
                            this.renderAccordionAnswers(initData);
                            break;
                    }
                },
                renderQuestion: function (initData, outputTarget, stepId) {
                    var _self = this;
                    var _render = "";
                    _.each(initData.steps, function (stepsEntry) {
                        var stepValue = "STEP"+ stepId;
                        if(stepsEntry.stepId == stepValue) {
                            _.each(stepsEntry.questions, function (entry) {
                                _question = entry.questiontext;
                            });
                        }
                    });
                    _render += _self.template({
                        question: _question,
                    });
                    $('#'+outputTarget+'-question').html(_render);
                },
                renderAnswer: function (initData, outputTarget, stepId) {
                    var _self = this;
                    var _renderAnswer = "";
                    var _nextTab = "";
                    switch(outputTarget) {
                        case 'room':
                            _nextTab= "product-type";
                            break;
                        case 'product-type':
                            _nextTab= "product-description";
                            break;
                        case 'product-description':
                            _nextTab= "issues";
                            break;
                    }
                    _.each(initData.steps, function (stepsEntry) {
                        var stepValue = "STEP"+ stepId;
                        var nextStep = parseInt(stepId) + 1;
                        if(stepsEntry.stepId == stepValue) {
                            _stepId = stepsEntry.stepId
                            _.each(stepsEntry.questions, function (answerEntry) {
                                var _render = "";
                                _.each(answerEntry.answers, function (entry) {
                                    _render += _self.template2({
                                        title: entry.answerDisplayValue,
                                        thumbnail: entry.thumbnail,
                                        value: entry.key,
                                        matchingproductcount: entry.matchingproductcount,
                                        stepId: _stepId,
                                        tab: outputTarget,
                                        nextTab: _nextTab,
                                        nextStep: nextStep
                                    });
                                });
                                _renderAnswer = _render;
                            });
                        }
                    });
                    $('#'+outputTarget+'-answers').html(_renderAnswer);

                },
                renderAccordionQuestions: function (initData, stepId) {
                    var _self = this;
                    var _render = "";
                    var matchingCount = 0;
                    _.each(initData.steps, function (stepsEntry) {
                        if(stepsEntry.stepId == 'STEP1') {
                            _.each(stepsEntry.questions, function (questionEntry) {
                                var selectedRoom = questionEntry.answerCode;
                                _.each(questionEntry.answers, function (answerquestionEntry) {
                                    if(answerquestionEntry.key == selectedRoom) {
                                        matchingCount = answerquestionEntry.matchingproductcount;
                                    }
                                });
                            });
                        }
                        if(stepsEntry.stepId == 'STEP3') {
                            var _questionNumber = 0;
                            var _active = '';
                            _.each(stepsEntry.questions, function (entry) {
                                if(_questionNumber == 0) {
                                    _active = 'is-active';
                                }
                                else {
                                    _active = '';
                                }
                                _render += _self.template3({
                                    question: entry.questiontext,
                                    questionNumber: _questionNumber,
                                    active: _active
                                });
                                _questionNumber = _questionNumber + 1;
                            });
                        }
                    });

                    var viewMatchingLink = '#';
                    if(initData._links.result.href) {
                        viewMatchingLink = initData._links.result.href;
                    }
                    //var link = '<a href="'+viewMatchingLink+'" nextstep='++' data='++' data-stepid="'++'" >'++'</a>';
                    $('.guided-search-viewMatchingLink').attr('data', viewMatchingLink);
                    $('.sps-ttl-prd-no').html(matchingCount);
                    var totalCount = parseInt(matchingCount);
                    if(!totalCount) {
                        $('.guided-search-viewMatchingLink').attr('disabled', true);
                        $('.guided-search-viewMatchingLink').removeAttr('href');
                    }
                    else {
                        $('.guided-search-viewMatchingLink').attr('disabled', false);
                        $('.guided-search-viewMatchingLink').attr('href','#issue')
                    }
                    $('#product-description-data').html(_render);

                },
                renderAccordionAnswers: function (initData) {
                    var _self = this;
                    _.each(initData.steps, function (stepsEntry) {
                        if(stepsEntry.stepId == 'STEP3') {
                            var questionNumber = 0;
                            _.each(stepsEntry.questions, function (questionentry) {
                                var _render = "";
                                _.each(questionentry.answers, function (entry) {
                                    _render += _self.template4({
                                        title: entry.answerDisplayValue,
                                        thumbnail: entry.thumbnail,
                                        value: entry.key,
                                        matchingproductcount: '',
                                        questionNumber: questionNumber
                                    });
                                });
                                $('#answer'+questionNumber).html(_render);
                                questionNumber = questionNumber + 1;
                            });
                        }
                    });
                }
            });
            if (context === document) {
                var postValues = {};
                postValues['tab'] = 'room';
                postValues['step'] = 1;
                getProductIdentificationResults(postValues);
                loader.hide();
            }
            /*
            * Method to create the dynamic elements and render them in the template.
            */
            var UpateMatchigCount = Backbone.View.extend({
                template4: _.template($("#output-accordion-answers").html()),
                initialize: function (initData) {
                    this.renderMatchingCount(initData);
                },
                renderMatchingCount: function (initData) {
                    var _self = this;
                    var matchingCount = 0;
                    _.each(initData.steps, function (stepsEntry) {
                        if (stepsEntry.stepId == 'STEP1') {
                            _.each(stepsEntry.questions, function (questionEntry) {
                                var selectedRoom = questionEntry.answerCode;
                                _.each(questionEntry.answers, function (answerquestionEntry) {
                                    if (answerquestionEntry.key == selectedRoom) {
                                        matchingCount = answerquestionEntry.matchingproductcount;
                                    }
                                });
                            });
                        }
                        if(stepsEntry.stepId == 'STEP3') {
                            var questionNumber = 0;
                            _.each(stepsEntry.questions, function (questionentry) {
                                var _render = "";
                                _.each(questionentry.answers, function (entry) {
                                    var _matchingproductcount = '';
                                    if(entry.matchingproductcount == 0) {
                                      _matchingproductcount = 'disabled';
                                    }
                                    _render += _self.template4({
                                        title: entry.answerDisplayValue,
                                        thumbnail: entry.thumbnail,
                                        value: entry.key,
                                        matchingproductcount: _matchingproductcount,
                                        questionNumber: questionNumber
                                    });
                                });
                                $('#answer'+questionNumber).html(_render);
                                questionNumber = questionNumber + 1;
                            });
                        }
                    });
                    var viewMatchingLink = '#';
                    if(initData._links.result.href) {
                        viewMatchingLink = initData._links.result.href;
                    }
                    $('.guided-search-viewMatchingLink').attr('data', viewMatchingLink);
                    $('.sps-ttl-prd-no').html(matchingCount);
                    var totalCount = parseInt(matchingCount);
                    if(!totalCount) {
                        $('.guided-search-viewMatchingLink').attr('disabled', true);
                        $('.guided-search-viewMatchingLink').removeAttr('href');
                    }
                    else {
                        $('.guided-search-viewMatchingLink').attr('disabled', false);
                        $('.guided-search-viewMatchingLink').attr('href','#issue')
                    }
                }
            });
            /*
            * Method to get the api response and call view method.
            * */
            function getProductIdentificationResults(postValues) {
                var stepId = parseInt(postValues.step);
                switch(stepId){
                    case 1:
                    case 2:
                        prevResponse = drupalSettings.step1;
                        break;
                    case 3:
                        prevResponse = drupalSettings.step2;
                        break;
                    case 4:
                        prevResponse = drupalSettings.step3;
                        break;
                }
                $.ajax({
                    type: "POST",
                    url: "/get-room-tab-output",
                    dataType: 'json',
                    async: false,
                    data: {
                        tab: postValues.tab,
                        prevResponse: prevResponse,
                        selectedResponse: postValues.selectedResponse,
                        currentStep: postValues.currentStep,
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        //showErrorMessage(XMLHttpRequest.status, '#all-tab #output');
                        //showErrorMessage(XMLHttpRequest.status, '#all-tab #output');
                    },
                    beforeSend: function () {

                    },
                    complete: function () {

                    },
                    success: function (data) {
                        var stepId = parseInt(postValues.step);
                        if(stepId == 3) {
                            var step3 = false;
                            _.each(data.steps, function (stepsEntry) {
                                if (stepsEntry.stepId == 'STEP3') {
                                    step3 = true;
                                }
                            });
                            if(!step3) {
                                postValues.step = stepId = 2;
                                postValues.tab = 'product-type';
                                $("#product-type-answers .service-tab-trigger").removeAttr('data-tab-link');
                            }
                        }
                        switch(stepId){
                            case 1:
                                drupalSettings.step1 = data;
                                break;
                            case 2:
                                drupalSettings.step2 = data;
                                break;
                            case 3:
                                drupalSettings.step3 = data;
                                break;
                        }
                        if(stepId == 4) {
                            var matchingCount = new UpateMatchigCount(data);
                        }
                        else {
                            var prodView = new ProdView(data, postValues.tab, postValues.step);
                        }
                    }
                });
            }
            var ProductView = Backbone.View.extend({
                el: "#guided-search-output-product",
                //el2: "#output-product-1",
                template: _.template($("#output-product-node").html()),
                //template2: _.template($("#output-product-node-2").html()),
                initialize: function (initData, eventName, append) {
                    this.renderProductNode(initData, eventName, append);
                    //this.renderFullProducts(initData);
                },
                renderProductNode: function (initData, eventName, append) {
                    var _self = this;
                    var _render = "";
                    if (initData.content != '') {

                        // Getting SFCC product Redirect URL from SFCC apiCall
                        var productIds = "";
                        _.each(initData.content, function (prdData) {
                          productIds += prdData.name+",";
                        });

                        var responsePrdUrls = {};
                        if(productIds != "" && productIds != undefined) {
                          $.ajax({
                            type: "POST",
                            url: "/collection-listing-producturls",
                            dataType: 'json',
                            async: false,
                            data: {
                              prdids: productIds,
                            },
                            error: function (XMLHttpRequest, textStatus, errorThrown) {
                              return false;
                            },
                            success: function (response) {
                              if(response != "" && response != undefined ){
                                _.each(response, function (result) {
                                  responsePrdUrls[result.model_number] = result.view_details_link;
                                });
                              }
                            }
                          });
                        }

                        _.each(initData.content, function (entry) {
                            var _heroImage = entry.heroImageSmall;
                            var _title = "Title";
                            let _availableToOrderDate = entry.values.AvailableToOrderDateTimestamp;
                            let _availableToShipDate = entry.values.AvailableToShipDateTimestamp;
                            let _currentDate = entry.values.currentDate;
                            let _viewAllCollectionLink = entry.values.viewAllCollectionLink;
                            let _ribbonText = '';
                            let _ribbonColor = '';
                            let _collection = '';
                            if (entry.description !== '' && entry.description !== undefined) {
                                _title = entry.description;
                            }
                            var _sku = entry.name;
                            var _url = '';

                            if(responsePrdUrls != "" && responsePrdUrls != undefined) {
                              if(responsePrdUrls[entry.name] != "" && responsePrdUrls[entry.name] != undefined ) {
                                _url = responsePrdUrls[entry.name];
                              }
                            }

                            if (entry.values.Collection !== "" && entry.values.Collection !== undefined) {
                                _collection = entry.values.Collection;
                            }
                            if(entry.values.discontinued) {
                                _ribbonText = entry.values.discontinueText;
                                _ribbonColor = "gray-30";
                            }
                            else {
                                if (_currentDate < _availableToShipDate) {
                                    if (_currentDate > _availableToOrderDate) {
                                        _ribbonText = entry.values.comingSoonText;
                                        _ribbonColor = "delta-red";
                                    }
                                }
                            }
                            var setFinishIconPath = '';
                            const getEachFinish = [];
                            new Date;
                            for (var i = 0; i <= _finishList.length; i++ ){
                              if(_finishList[i] !== undefined && typeof _finishList[i].facetFinish !== undefined && _finishList[i].modelNumber.indexOf('-IN') == '-1' && _finishList[i].modelNumber.slice(-2) != '-R' && (Date.now() > _finishList[i].availableToOrderDate)){
                                var getAPiFinishName = _finishList[i].facetFinish;
                                var getAPiFinishST = _finishList[i].stockingType;
                                ActiveClass = 'in-active finish-class';
                                if(setFinishClass == getAPiFinishName){
                                  ActiveClass = 'active finish-class';
                                  _defaultFinish = '';
                                }
                                if(_defaultFinish == getAPiFinishName){
                                  ActiveClass = 'active finish-class';
                                }
                                var setFinishIconPath = finishDataIcon[getAPiFinishName];
                                var existInIgnoreList = ignoreListFinish.indexOf(getAPiFinishName);
                                var existIgnoreStockList = ignoreStockTypeFinish.indexOf(getAPiFinishST);
                                if(setFinishIconPath !== undefined && existInIgnoreList == -1 && existIgnoreStockList == -1 && getAPiFinishST != ''){
                                   var finishModelNumber = _finishList[i].modelNumber;
                                   var imgIcon = '<a href="/product-detail/' + finishModelNumber + '" class="no-line product-action-link">' +'<img src="' + setFinishIconPath + '" alt="'+ _finishList[i].facetFinish +'" title="'+ _finishList[i].finish +'" class="'+ ActiveClass +'"  /></a>';
                                   getEachFinish.push(imgIcon);
                                }
                            }
                          }
                          _render += _self.template({
                                title: _title,
                                collectionName: _collection,
                                heroImage: _heroImage,
                                finishOptions: getEachFinish,
                                sku: _sku,
                                url: _url,
                                ribbonText: _ribbonText,
                                ribbonColor: _ribbonColor
                            });
                        });
                        $('#guided-search-product-matching-count').html(initData.totalElements);
                        if(append ==1){
                            _self.$el.append(_render);
                        }else{
                            _self.$el.html(_render);
                        }
                    }
                }
            });

            /**
             * Method to get Products and render them.
             * @param $url
             */
            function getProductsList(url) {
                $.ajax({
                    type: "POST",
                    url: "/guided-search-products",
                    dataType: 'json',
                    async: false,
                    data: {
                        url: url,
                        page:0
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        //showErrorMessage(XMLHttpRequest.status, '#all-tab #output');
                        //showErrorMessage(XMLHttpRequest.status, '#all-tab #output');
                    },
                    beforeSend: function () {

                    },
                    complete: function () {

                    },
                    success: function (data) {
                        drupalSettings.producturl = url;
                        var productView = new ProductView(data);
                    }
                });
            }
            /**
             * Method to get Products and render them.
             * @param $url
             */
            function getTroubleshoot(postValues) {
                var stepId = parseInt(postValues.step);
                switch(stepId){
                    case 1:
                    case 2:
                        prevResponse = drupalSettings.troubleshootStep1;
                        break;
                    case 3:
                        prevResponse = drupalSettings.troubleshootStep2;
                        break;
                }
                $.ajax({
                    type: "POST",
                    url: "/guided-search-troubleshoot",
                    dataType: 'json',
                    async: false,
                    data: {
                        stepId: postValues.stepId,
                        prevResponse: prevResponse,
                        selectedResponse: postValues.selectedResponse,
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        //showErrorMessage(XMLHttpRequest.status, '#all-tab #output');
                        //showErrorMessage(XMLHttpRequest.status, '#all-tab #output');
                    },
                    beforeSend: function () {

                    },
                    complete: function () {

                    },
                    success: function (data) {
                        var stepId = parseInt(postValues.step);
                        switch(stepId){
                            case 1:
                                drupalSettings.troubleshootStep1 = data;
                                break;
                            case 2:
                                drupalSettings.troubleshootStep2 = data;
                                break;
                            case 3:
                                drupalSettings.troubleshootStep3 = data;
                                break;
                        }
                        var troubleshootView = new TrobuleshootView(data, stepId);
                        $('.issues-product-output').hide();
                    }
                });
            }

            /**
             * Scroll functionality to get products on window scroll.
             */
            $(window).once().scroll(function () {
                if (($(window).scrollTop()+250 > $('#guided-search-output-product').height())
                    && loadProducts && (!lastPage) && drupalSettings.producturl) {

                    loadProducts = false;
                    loader.show();
                    $.ajax({
                        type: "POST",
                        url: "/guided-search-products",
                        dataType: 'json',
                        data: {
                            url: drupalSettings.producturl,
                            page:pageNumber ++
                        },
                        complete: function () {
                            loader.hide();
                        },
                        success: function (data) {
                            var productView = new ProductView(data, null, 1);
                            loadProducts = true;
                            lastPage = data.last;
                        }
                    });
                }
            });
        }
    };
    Drupal.behaviors.findapart = {
      attach: function (context, settings) {
        jQuery(document).mouseup(function(e) {
          var container = jQuery(".search-block-suggestion-result-parts");
          if (!container.is(e.target) && container.has(e.target).length === 0) {
            jQuery(".search-block-suggestion-result-parts").html("");
          }
        });
      }
    }
})(jQuery, Drupal, drupalSettings);

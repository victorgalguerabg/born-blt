<?php

// Declare the namespace for our own event subscriber.
namespace Drupal\sfcc_services\EventSubscriber;

// This is the interface that we are implementing.
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Drupal\sfcc_services\SfccServices;

/**
 * Event subscriptions for events dispatched by Cart Count Update.
 */
class SsoAuthenticateSubscriber implements EventSubscriberInterface {

  protected $cartCount;

  /**
   * Constructor.
   *
   * We use dependency injection get Cart Count From SFCC.
   *
   */
  public function __construct(SfccServices $cartCount) {
    $this->cartCount = $cartCount;
  }

  /**
   * Returns an array of event names this subscriber wants to listen to.
   *
   * @return array
   *   The event names to listen to
   */
  static function getSubscribedEvents() {
    $events = array();
    $events[KernelEvents::CONTROLLER][] = array('onLoad');
    return $events;
  }

  public function onLoad(FilterControllerEvent $event) {
    // SSO Login with SFCC
    // if(!isset($_COOKIE['dwsid'])) {
    //   $questUser = $this->cartCount->guestAuth();
    // }
    // else {
    //   $sessionResponse = $this->cartCount->sessionValidate($_COOKIE['dwsid']);
    // }
    $current_user = \Drupal::currentUser();
    $roles = $current_user->getRoles();
    if(in_array("administrator",$roles)) {
      setrawcookie("varnishadmin","1");
    }
  }

}

<?php

namespace Drupal\sfcc_services\Controller;

use Drupal\Core\Link;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Component\Utility\Tags;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\sfcc_services\SfccServices;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\Core\Url;
use Drupal\Core\File\FileSystemInterface;


/**
 * Defines a route controller for entity autocomplete form elements.
 */
class SearchController extends ControllerBase {
  /**
   * Drupal\delta_services\DeltaService definition.
   *
   * @var Drupal\sfcc_services\SfccServices;
   */
  protected $searchservice;

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Drupal\Core\TempStore\PrivateTempStoreFactory definition.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStoreFactory
   */
  private $tempStoreFactory;

  /**
    * Drupal\Core\Logger\LoggerChannelFactoryInterface definition.
    *
    * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
    */
  protected $loggerFactory;

  /**
    * Symfony\Component\HttpFoundation\RequestStack definition.
    *
    * @var Symfony\Component\HttpFoundation\RequestStack
    */
  protected $requestStack;

  /**
    * Drupal\Core\File\FileSystem definition.
    *
    * @var Drupal\Core\File\FileSystem
    */
  protected $fileSystem;

  /**
   * Class constructor.
   */
  public function __construct(SfccServices $searchservice,ConfigFactoryInterface $config_factory,PrivateTempStoreFactory $tempStoreFactory,LoggerChannelFactoryInterface $loggerFactory, RequestStack $requestStack, FileSystemInterface $file_system) {
    $this->searchservice = $searchservice;
    $this->configFactory = $config_factory;
    $this->sfccConfig = $this->configFactory->get('sfcc_services.sfcc_api_configs');
    $this->tempStoreFactory = $tempStoreFactory;
    $this->loggerFactory = $loggerFactory;
    $this->requestStack = $requestStack;
    $this->fileSystem = $file_system;
    $this->systemFile = $this->configFactory->get('system.file');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
    // Load the service required to construct this class.
        $container->get('sfcc_services.deltasfccservices'),
        $container->get('config.factory'),
        $container->get('tempstore.private'),
        $container->get('logger.factory'),
        $container->get('request_stack'),
        $container->get('file_system')
    );
  }

  /**
   * Handler for Search  Popup request.
   */
  public function searchSuggestion() {
    $output = [];
    $typedString = $this->requestStack->getCurrentRequest()->get('searchtxt');
    $searchUrl = $this->sfccConfig->get('searchsuggestion');
    $hostname = $this->sfccConfig->get("hostname");
    $searchProductUrl = $hostname.$searchUrl.rawurlencode($typedString)."&json=true";
    $searchResponse = $this->searchservice->apiRequest($searchProductUrl,"GET");
    $returnData = json_decode($searchResponse['body'], TRUE);
    if(!empty($returnData)) {
      if(isset($returnData['suggestions']['product']['retail']) && !empty($returnData['suggestions']['product']['retail'])) {
        foreach ($returnData['suggestions']['product']['retail'] as $content) {
          if(isset($content['price']['type'])) {
            if(isset($content['price']['min']) && isset($content['price']['max'])) {
              $output['retail'][$content['ID']]['saleprice'] = $content['price']['min']['sales']['formatted']."-".$content['price']['max']['sales']['formatted'];
            }
          }
          else {
            $output['retail'][$content['ID']]['saleprice'] = (!empty($content['price']['sales'])) ? $content['price']['sales']['formatted'] : "";
            $output['retail'][$content['ID']]['listprice'] = (!empty($content['price']['list'])) ? $content['price']['list']['formatted'] : "";
          }
          $output['retail'][$content['ID']]['name'] = $content['name'];
          $output['retail'][$content['ID']]['url'] = $content['url'];
          $output['retail'][$content['ID']]['sku'] = $content['ID'];
          $output['retail'][$content['ID']]['imageUrl'] = $content['imageUrl'];
          $output['retail'][$content['ID']]['collection'] = (!empty($content['collection'])) ? $content['collection'] : "";
          $output['retail'][$content['ID']]['variations'] = $content['swatches'];
          $output['retail'][$content['ID']]['promotionMsg'] = $content['promotionMsg'];
          $output['retail'][$content['ID']]['badge'] = $content['badge'];
        }
      }
      if (isset($returnData['suggestions']['product']['repairParts']) && !empty($returnData['suggestions']['product']['repairParts'])) {
        foreach ($returnData['suggestions']['product']['repairParts'] as $content) {
          if(isset($content['price']['type'])) {
            if(isset($content['price']['min']) && isset($content['price']['max'])) {
              $output['parts'][$content['ID']]['saleprice'] = $content['price']['min']['sales']['formatted']."-".$content['price']['max']['sales']['formatted'];
            }
          }
          else {
            $output['parts'][$content['ID']]['saleprice'] = (!empty($content['price']['sales'])) ? $content['price']['sales']['formatted'] : "";
            $output['parts'][$content['ID']]['listprice'] = (!empty($content['price']['list'])) ? $content['price']['list']['formatted'] : "";
          }
          $output['parts'][$content['ID']]['name'] = $content['name'];
          $output['parts'][$content['ID']]['url'] = $content['url'];
          $output['parts'][$content['ID']]['sku'] = $content['ID'];
          $output['parts'][$content['ID']]['imageUrl'] = $content['imageUrl'];
          $output['parts'][$content['ID']]['collection'] = (!empty($content['collection'])) ? $content['collection'] : "";
          $output['parts'][$content['ID']]['variations'] = $content['swatches'];
          $output['parts'][$content['ID']]['promotionMsg'] = $content['promotionMsg'];
          $output['parts'][$content['ID']]['badge'] = $content['badge'];
        }
      }
      if (isset($returnData['suggestions']['product']['others']) && !empty($returnData['suggestions']['product']['others'])) {
        foreach ($returnData['suggestions']['product']['others'] as $content) {
          if(isset($content['price']['type'])) {
            if(isset($content['price']['min']) && isset($content['price']['max'])) {
              $output['others'][$content['ID']]['saleprice'] = $content['price']['min']['sales']['formatted']."-".$content['price']['max']['sales']['formatted'];
            }
          }
          else {
            $output['others'][$content['ID']]['saleprice'] = (!empty($content['price']['sales'])) ? $content['price']['sales']['formatted'] : "";
            $output['others'][$content['ID']]['listprice'] = (!empty($content['price']['list'])) ? $content['price']['list']['formatted'] : "";
          }
          $output['others'][$content['ID']]['name'] = $content['name'];
          $output['others'][$content['ID']]['url'] = $content['url'];
          $output['others'][$content['ID']]['sku'] = $content['ID'];
          $output['others'][$content['ID']]['imageUrl'] = $content['imageUrl'];
          $output['others'][$content['ID']]['collection'] = (!empty($content['collection'])) ? $content['collection'] : "";
          $output['others'][$content['ID']]['variations'] = $content['swatches'];
          $output['others'][$content['ID']]['promotionMsg'] = $content['promotionMsg'];
          $output['others'][$content['ID']]['badge'] = $content['badge'];
        }
      }
      $output['links'] = $returnData['links'];
      $searchPageUrl = $this->sfccConfig->get('searchpage');
      array_unshift($returnData['suggestions']['popular'], ['value'=>"Popular Search",'url'=>""]);
      $didyoumean = "";
      if(!empty($returnData['suggestions']['product']['phrases'])) {
        foreach ($returnData['suggestions']['product']['phrases'] as $key => $value) {
          $searchUrl = (!empty($value['value'])) ? $searchPageUrl.$value['value'] : "";
          $didyoumean .= '<a href="'.$searchUrl.'">'.$value['value'].'</a> ';
        }
      }
      if(!empty($didyoumean)) {
        array_unshift($returnData['suggestions']['popular'], ['value'=>"Do you mean? <b>".$didyoumean."</b>",'url'=>"didyoumean"]);
      }
      $output['suggestions'] = (!empty($returnData['suggestions']['popular'])) ? $returnData['suggestions']['popular'] : "";
    }
    return new JsonResponse($output);
  }

  /**
   * Handler for autocomplete request.
   */
  public function autoComplete() {
    $output = [];
    $results = '';
    $typedString = $this->requestStack->getCurrentRequest()->get('q');
    if (strlen($typedString) > 2) {
      $autoSearchUrl = $this->sfccConfig->get('searchsuggestion');
      $hostname = $this->sfccConfig->get("hostname");
      $autoSearchProductUrl = $hostname.$autoSearchUrl.$typedString."&json=true";
      $autoSearchResponse = $this->searchservice->apiRequest($autoSearchProductUrl,"GET");
      $autoReturnData = json_decode($autoSearchResponse['body'], TRUE);
      if(!empty($autoReturnData)) {
        if(isset($autoReturnData['suggestions']['product']['retail']) && !empty($autoReturnData['suggestions']['product']['retail'])) {
          $output[] = [
            'label' => 'PRODUCTS',
          ];
          foreach ($autoReturnData['suggestions']['product']['retail'] as $content) {
            $productLink = [];
            $productLink['name'] = $content['ID'];
            $productLink['description'] = $content['collection']." ".$content['name'];

            $link = [
              '#theme' => 'search_item_link',
              '#data' => (Array) $productLink,
            ];

            $output[] = [
              'value' => $content['name'],
              'label' => Link::fromTextAndUrl(render($link), Url::fromUserInput($content['url'])),
            ];
          }

          if(count($output)){
            $output = array_slice($output, 0,5);
          }

          $url = Url::fromRoute('global_search.search', ['q' => $typedString, 'filter' => 'retail']);
          $viewAllLink['viewAll'] = $url;
          $link = [
            '#theme' => 'view_all_link',
            '#data' => (Array) $viewAllLink,
          ];
          $addViewAll = [
            'label' => render($link),
          ];
          $output[] = $addViewAll;
         //Build Output.
          $results = [
            '#theme' => 'search_suggestions_output',
            '#data' => $output,
          ];
        }

        $partsOutput = [];
        if (isset($autoReturnData['suggestions']['product']['repairParts']) && !empty($autoReturnData['suggestions']['product']['repairParts'])) {
          $partsOutput[] = [
            'label' => 'REPAIR PARTS',
          ];
          foreach ($autoReturnData['suggestions']['product']['repairParts'] as $content) {
            $repairProductLink = [];
            $repairProductLink['name'] = $content['ID'];
            $repairProductLink['description'] = $content['collection']." ".$content['name'];

            $repairLink = [
              '#theme' => 'search_item_link',
              '#data' => (Array) $repairProductLink,
            ];

            $partsOutput[] = [
              'value' => $content['name'],
              'label' => Link::fromTextAndUrl(render($repairLink), Url::fromUserInput($content['url'])),
            ];
          }

          if(count($partsOutput)){
            $partsOutput = array_slice($partsOutput, 0,5);
          }

          //Add view all link.
          $repairProdUrl = Url::fromRoute('global_search.search', ['q' => $typedString, 'filter' => 'repair']);
          $repairProdViewAllLink['viewAll'] = $repairProdUrl;
          $repairLink = [
            '#theme' => 'view_all_link',
            '#data' => (Array) $repairProdViewAllLink,
          ];
          $repairProdAddViewAll = [
            'label' => render($repairLink),
          ];
          $partsOutput[] = $repairProdAddViewAll;

          // Check if the file is uploaded, let combine file data in to array.
          if (is_array($partsOutput) && count($partsOutput)) {
            $output = array_merge($output, $partsOutput);
          }
        }
      }
      // Build Output.
      $results = [
        '#theme' => 'search_suggestions_output',
        '#data' => $output,
      ];
    }
    return new JsonResponse($output);
  }

  public function getBasket() {
    // Checking session authentication
    if(!isset($_COOKIE['dwsid'])) {
      $questUser = $this->searchservice->guestAuth();
    }
    else {
      $sessionResponse = $this->searchservice->sessionValidate($_COOKIE['dwsid']);
    }

    // Checking Tracking consent
    // Setting tacking consent TRUE.
    if (!isset($_COOKIE['tracking_consent'])) {
      $host = \Drupal::request()->getHost();
      setcookie("tracking_consent", "true", time() + (86400 * 30), $host);
    }

    $userDetails = [];
    $basketResults = [];
    $hostname = $this->sfccConfig->get("hostname");
    $midpath = $this->sfccConfig->get("midpath");
    $siteid = $this->sfccConfig->get("siteid");
    $clientid = $this->sfccConfig->get("clientid");
    $apiType = $this->sfccConfig->get("basket");
    $Url = $hostname.$siteid.$midpath.$apiType;
    // Get Customer ID and Token
    $tempstore = $this->tempStoreFactory->get('userInfo');
    $customerId = $tempstore->get('customerId');
    $token = $tempstore->get('token');

    // To fire GTM Event for each pages
    $userDetails['type'] = $tempstore->get('usertype');
    $userDetails['customerId'] = $tempstore->get('visitorId');
    $userDetails['visitorId'] = $tempstore->get('customerId');
    $userDetails['email'] = $tempstore->get('email');
    $basketResults['user'] = $userDetails;

    if(!empty($customerId)) {
      $basketUrl= str_replace("customerid",$customerId,$Url);
      $basketRequest = $this->searchservice->apiRequest($basketUrl,"GET",$token,"","","");
      $basketResults['cart'] = json_decode($basketRequest['body'],TRUE);
      if(!isset($basketResults['cart']['fault'])) {
        if(isset($basketResults['cart']['baskets'][0]['product_items'])) {
          $basketResults['cart']['baskets'][0]['product_items'] = array_reverse($basketResults['cart']['baskets'][0]['product_items']);
        }
        return new JsonResponse($basketResults);
      }
      else {
        $this->loggerFactory->get('Connection_ERR')
          ->notice($basketRequest['cart']['body']);
        return new JsonResponse($basketResults);
      }
    }
    else {
      $this->loggerFactory->get('Connection_ERR')
        ->notice("User customer ID is empty.");
      return new JsonResponse(["User customer ID is empty."]);
    }
  }

  public function updateQuantity() {
    $basketResults = [];
    $basketId = $this->requestStack->getCurrentRequest()->get('basketid');
    $itemId = $this->requestStack->getCurrentRequest()->get('itemid');
    $quantity = $this->requestStack->getCurrentRequest()->get('quantity');
    $hostname = $this->sfccConfig->get("hostname");
    $midpath = $this->sfccConfig->get("midpath");
    $siteid = $this->sfccConfig->get("siteid");
    $clientid = $this->sfccConfig->get("clientid");
    $apiType = $this->sfccConfig->get("updatequantity");
    $Url = $hostname.$siteid.$midpath.$apiType;
    // Get Customer ID and Token
    $tempstore = $this->tempStoreFactory->get('userInfo');
    $token = $tempstore->get('token');
    if(!empty($basketId) && !empty($itemId)) {
      $rawurl = str_replace("basketno",$basketId,$Url);
      $basketUrl= $rawurl.$itemId;
      $requestData['quantity'] = (int)$quantity;
      $updateRequest = $this->searchservice->apiRequest($basketUrl,"PATCH",$token,"",$requestData,"");
      $basketResults = json_decode($updateRequest['body'],TRUE);
      if(!isset($basketResults['fault'])) {
        return new JsonResponse($basketResults);
      }
      else {
        return new JsonResponse($basketResults);
      }
    }
    else {
      return new JsonResponse($basketResults);
    }
  }

  public function removeProduct() {
    $basketResults = [];
    $basketId = $this->requestStack->getCurrentRequest()->get('basketid');
    $itemId = $this->requestStack->getCurrentRequest()->get('itemid');
    $removeProductresponse = $this->productRemove($basketId,$itemId);
    if(!isset($removeProductresponse['fault'])) {
      return new JsonResponse($removeProductresponse);
    }
    else {
      return new JsonResponse($removeProductresponse);
    }
  }

  public function productRemove($basketId,$itemId) {
    $basketResults = [];
    $hostname = $this->sfccConfig->get("hostname");
    $midpath = $this->sfccConfig->get("midpath");
    $siteid = $this->sfccConfig->get("siteid");
    $clientid = $this->sfccConfig->get("clientid");
    $apiType = $this->sfccConfig->get("removeproduct");
    $Url = $hostname.$siteid.$midpath.$apiType;
    // Get Customer ID and Token
    $tempstore = $this->tempStoreFactory->get('userInfo');
    $token = $tempstore->get('token');
    if(!empty($basketId) && !empty($itemId)) {
      $rawurl = str_replace("basketno",$basketId,$Url);
      $basketUrl= $rawurl.$itemId;
      $removeRequest = $this->searchservice->apiRequest($basketUrl,"DELETE",$token,"","","");
      $basketResults = json_decode($removeRequest['body'],TRUE);
      return $basketResults;
    }
    else {
      return $basketResults;
    }
  }

  public function createSFL($type) {
    $bodyData = [];
    $hostname = $this->sfccConfig->get("hostname");
    $midpath = $this->sfccConfig->get("midpath");
    $siteid = $this->sfccConfig->get("siteid");
    $clientid = $this->sfccConfig->get("clientid");
    $apiType = $this->sfccConfig->get("createsfl");
    $Url = $hostname.$siteid.$midpath.$apiType;
    // Get Customer ID and Token
    $tempstore = $this->tempStoreFactory->get('userInfo');
    $customerId = $tempstore->get('customerId');
    $token = $tempstore->get('token');
    if(!empty($token) && !empty($customerId)) {
      $rawurl = str_replace("customerid",$customerId,$Url);
      $bodyData = ['type'=>$type,'public'=>true];
      $createSflRequest = $this->searchservice->apiRequest($rawurl,"POST",$token,"",$bodyData,"");
      $createSflResults = json_decode($createSflRequest['body'],TRUE);
      return $createSflResults;
    }
    else {
      return $createSflResults;
    }
  }

  public function addSFL($sflId,$productId,$quantity) {
    $addResponse = [];
    $hostname = $this->sfccConfig->get("hostname");
    $midpath = $this->sfccConfig->get("midpath");
    $siteid = $this->sfccConfig->get("siteid");
    $clientid = $this->sfccConfig->get("clientid");
    $sflUrl = $this->sfccConfig->get("addtosfl");
    $sflUrlConstruct = $hostname.$siteid.$midpath.$sflUrl;
    // Get Customer ID and Token
    $tempstore = $this->tempStoreFactory->get('userInfo');
    $customerId = $tempstore->get('customerId');
    $token = $tempstore->get('token');

    $customerIdUpdate = str_replace("customerid",$customerId,$sflUrlConstruct);
    $sflIdUpdate = str_replace("sflid",$sflId,$customerIdUpdate);
    $bodyReuestType = ['product_id'=>$productId,"public"=>true,"quantity"=>(int)$quantity,"type"=>"product"];
    $addSflRequest = $this->searchservice->apiRequest($sflIdUpdate,"POST",$token,"",$bodyReuestType,"");
    $addResponse = json_decode($addSflRequest['body'],TRUE);
    return $addResponse;
  }

  public function saveForLater() {
    $basketResults = [];
    $bodyReuestType = [];
    $basketId = $this->requestStack->getCurrentRequest()->get('basketid');
    $itemId = $this->requestStack->getCurrentRequest()->get('itemid');
    $quantity = $this->requestStack->getCurrentRequest()->get('quantity');
    $productId = $this->requestStack->getCurrentRequest()->get('productid');
    $hostname = $this->sfccConfig->get("hostname");
    $midpath = $this->sfccConfig->get("midpath");
    $siteid = $this->sfccConfig->get("siteid");
    $clientid = $this->sfccConfig->get("clientid");
    $apiType = $this->sfccConfig->get("customerprdlist");
    $Url = $hostname.$siteid.$midpath.$apiType;
    // Get Customer ID and Token
    $tempstore = $this->tempStoreFactory->get('userInfo');
    $customerId = $tempstore->get('customerId');
    $token = $tempstore->get('token');
    if(!empty($customerId)) {
      $saveUrl = str_replace("customerid",$customerId,$Url);
      $saveRequest = $this->searchservice->apiRequest($saveUrl,"GET",$token,"","","");
      $saveResults = json_decode($saveRequest['body'],TRUE);
      if(!isset($saveResults['fault'])) {
        if($saveResults['count'] != 0) {
          $count = 0;
          foreach ($saveResults['data'] as $key => $value) {
            if($value['type'] == 'custom_1') {
              $count++;
              $sflId = $value['id'];
              $addResponse = $this->addSFL($sflId,$productId,$quantity);
              if(!isset($addResponse['fault'])) {
                // Remove product from Mini Cart
                $removeproduct = $this->productRemove($basketId,$itemId);
                if(!isset($removeproduct['fault'])) {
                  return new JsonResponse($removeproduct);
                }
                else {
                  return new JsonResponse($removeproduct);
                }
              }
            }
          }
          if($count == 0) {
            $createSfl = $this->createSFL('custom_1');
            if(!isset($createSfl['fault'])) {
              $addResponse = $this->addSFL($createSfl['id'],$productId,$quantity);
              if(!isset($addResponse['fault'])) {
                $removeproduct = $this->productRemove($basketId,$itemId);
                if(!isset($removeproduct['fault'])) {
                  return new JsonResponse($removeproduct);
                }
                else {
                  return new JsonResponse($removeproduct);
                }
              }
              else {
                return new JsonResponse($addResponse);
              }
            }
            else {
              return new JsonResponse($removeproduct);
            }
          }
        }
        else {
          $createSfl = $this->createSFL('custom_1');
          if(!isset($createSfl['fault'])) {
            $addResponse = $this->addSFL($createSfl['id'],$productId,$quantity);
            if(!isset($addResponse['fault'])) {
              $removeproduct = $this->productRemove($basketId,$itemId);
              if(!isset($removeproduct['fault'])) {
                return new JsonResponse($removeproduct);
              }
              else {
                return new JsonResponse($removeproduct);
              }
            }
            else {
              return new JsonResponse($addResponse);
            }
          }
          else {
            return new JsonResponse($createSfl);
          }
        }
      }
      else {
        return new JsonResponse($saveResults);
      }
    }
    else {
      return new JsonResponse($saveResults);
    }
  }

  public function addWishlist() {
    $basketResults = [];
    $bodyReuestType = [];
    $basketId = $this->requestStack->getCurrentRequest()->get('basketid');
    $itemId = $this->requestStack->getCurrentRequest()->get('itemid');
    $quantity = $this->requestStack->getCurrentRequest()->get('quantity');
    $productId = $this->requestStack->getCurrentRequest()->get('productid');
    $type = $this->requestStack->getCurrentRequest()->get('type');
    $hostname = $this->sfccConfig->get("hostname");
    $midpath = $this->sfccConfig->get("midpath");
    $siteid = $this->sfccConfig->get("siteid");
    $clientid = $this->sfccConfig->get("clientid");
    $apiType = $this->sfccConfig->get("customerprdlist");
    $Url = $hostname.$siteid.$midpath.$apiType;
    // Get Customer ID and Token
    $tempstore = $this->tempStoreFactory->get('userInfo');
    $customerId = $tempstore->get('customerId');
    $token = $tempstore->get('token');
    if(!empty($customerId)) {
      $saveUrl = str_replace("customerid",$customerId,$Url);
      $saveRequest = $this->searchservice->apiRequest($saveUrl,"GET",$token,"","","");
      $saveResults = json_decode($saveRequest['body'],TRUE);
      if(!isset($saveResults['fault'])) {
        if($saveResults['count'] != 0) {
          $count = 0;
          foreach ($saveResults['data'] as $key => $value) {
            if($value['type'] == 'wish_list') {
              $count++;
              $sflId = $value['id'];
              $addResponse = $this->addSFL($sflId,$productId,$quantity);
              if(!isset($addResponse['fault'])) {
                // Remove product from Mini Cart
                if($type == 'cart') {
                  $removeproduct = $this->productRemove($basketId,$itemId);
                  if(!isset($removeproduct['fault'])) {
                    return new JsonResponse($removeproduct);
                  }
                  else {
                    return new JsonResponse($removeproduct);
                  }
                }
                else if($type == 'search') {
                  return new JsonResponse($addResponse);
                }
              }
            }
          }
          if($count == 0) {
            $createSfl = $this->createSFL('wish_list');
            if(!isset($createSfl['fault'])) {
              $addResponse = $this->addSFL($createSfl['id'],$productId,$quantity);
              if(!isset($addResponse['fault'])) {
                if($type == 'cart') {
                  $removeproduct = $this->productRemove($basketId,$itemId);
                  if(!isset($removeproduct['fault'])) {
                    return new JsonResponse($removeproduct);
                  }
                  else {
                    return new JsonResponse($removeproduct);
                  }
                }
                else if($type == 'search') {
                  return new JsonResponse($addResponse);
                }
              }
              else {
                return new JsonResponse($addResponse);
              }
            }
            else {
              return new JsonResponse($createSfl);
            }
          }
        }
        else {
          $createSfl = $this->createSFL('wish_list');
          if(!isset($createSfl['fault'])) {
            $addResponse = $this->addSFL($createSfl['id'],$productId,$quantity);
            if(!isset($addResponse['fault'])) {
              if($type == 'cart') {
                $removeproduct = $this->productRemove($basketId,$itemId);
                if(!isset($removeproduct['fault'])) {
                  return new JsonResponse($removeproduct);
                }
                else {
                  return new JsonResponse($removeproduct);
                }
              }
              else if($type == 'search') {
                return new JsonResponse($addResponse);
              }
            }
            else {
              return new JsonResponse($addResponse);
            }
          }
          else {
            return new JsonResponse($createSfl);
          }
        }
      }
      else {
        return new JsonResponse($saveResults);
      }
    }
    else {
      return new JsonResponse(["User customer ID is empty."]);
    }
  }

  //Get collection details page product URLS.
  public function getProductUrls(){
    $response = [];
    $productIds = $this->requestStack->getCurrentRequest()->get('prdids');
    if(!empty($productIds)) {
      $response = $this->searchservice->getProducts($productIds);
      return new JsonResponse($response);
    }
    else {
      return new JsonResponse($response);
    }
  }

  // Get wishlist Item Counts
  public function getWishListItems() {
    $addResponse = [];
    $listId = $this->requestStack->getCurrentRequest()->get('wishlistId');
    $hostname = $this->sfccConfig->get("hostname");
    $midpath = $this->sfccConfig->get("midpath");
    $siteid = $this->sfccConfig->get("siteid");
    $clientid = $this->sfccConfig->get("clientid");
    $wishlistUrl = $this->sfccConfig->get("getwishlistitems");
    $wishlistUrlConstruct = $hostname.$siteid.$midpath.$wishlistUrl;
    // Get Customer ID and Token
    $tempstore = $this->tempStoreFactory->get('userInfo');
    $customerId = $tempstore->get('customerId');
    $token = $tempstore->get('token');

    $customerIdUpdate = str_replace("customerid",$customerId,$wishlistUrlConstruct);
    $wishlistIdUpdate = str_replace("wishlistid",$listId,$customerIdUpdate);
    $wishListIdsRequest = $this->searchservice->apiRequest($wishlistIdUpdate,"GET",$token,"","","");
    $addResponse = json_decode($wishListIdsRequest['body'],TRUE);
    return new JsonResponse($addResponse);
  }

  public function wishlistRemove() {
    $addResponse = [];
    $wishlistId = $this->requestStack->getCurrentRequest()->get('wishlistId');
    $wishlistItemId = $this->requestStack->getCurrentRequest()->get('wishlistItemId');
    $hostname = $this->sfccConfig->get("hostname");
    $midpath = $this->sfccConfig->get("midpath");
    $siteid = $this->sfccConfig->get("siteid");
    $clientid = $this->sfccConfig->get("clientid");
    $wishlistUrl = $this->sfccConfig->get("removewishlist");
    $wishlistUrlConstruct = $hostname.$siteid.$midpath.$wishlistUrl;
    
    // Get Customer ID and Token
    $tempstore = $this->tempStoreFactory->get('userInfo');
    $customerId = $tempstore->get('customerId');
    $token = $tempstore->get('token');
    
    $wishlistIdUpdate = str_replace("wishlist-id",$wishlistId,$wishlistUrlConstruct);
    $wishlistItemIdUpdate = str_replace("wishlist-item-id",$wishlistItemId,$wishlistIdUpdate);
    $customerIdUpdate = str_replace("customerid",$customerId,$wishlistItemIdUpdate);



    $wishlistRemove = $this->searchservice->apiRequest(trim($customerIdUpdate),"DELETE",$token,"","","");
    $removeResponse = json_decode($wishlistRemove['body'],TRUE);
    return new JsonResponse($removeResponse);
  }

  public function getUserProductLists() {
    $addResponse = [];
    $hostname = $this->sfccConfig->get("hostname");
    $midpath = $this->sfccConfig->get("midpath");
    $siteid = $this->sfccConfig->get("siteid");
    $clientid = $this->sfccConfig->get("clientid");
    $productListUrl = $this->sfccConfig->get("customerprdlist");

    // Get Customer ID and Token
    $tempstore = $this->tempStoreFactory->get('userInfo');
    $customerId = $tempstore->get('customerId');
    $token = $tempstore->get('token');

    $productListUrlConstruct = $hostname.$siteid.$midpath.$productListUrl;
    $productListUrl = str_replace("customerid",$customerId,$productListUrlConstruct);

    $apirequest = $this->searchservice->apiRequest($productListUrl,"GET",$token,"","","");
    $apiResponse = json_decode($apirequest['body'],TRUE);
    if(isset($apiResponse['data']) && !empty($apiResponse['data'])) {
      foreach ($apiResponse['data'] as $key => $value) {
        if(!empty($value['type']) && isset($value['type']) && $value['type'] == 'wish_list') {
          $itemsApirequest = $this->searchservice->apiRequest($value['items_link']['link'],"GET",$token,"","","");
          $itemsApiResponse = json_decode($itemsApirequest['body'],TRUE);
          if(isset($itemsApiResponse['data']) && !empty($itemsApiResponse['data'])) {
            foreach ($itemsApiResponse['data'] as $keys => $values) {
              $addResponse[] = $values['product_id'];
            }
          }
        }
      }
    }

    /* whitelist API Logging Code */
    $logger_API_data = [
      'API URL' => $productListUrl,
      'API Token' => $token,
      'API Method' => 'GET',
     ];
    $this->loggerFactory->get('Connection_ERR')->notice('Product API Parameter <pre><code>' . print_r($logger_API_data, TRUE) . '</code></pre>');
    $this->loggerFactory->get('Connection_ERR')->notice('Product API response <pre><code>' . print_r($apiResponse, TRUE) . '</code></pre>');
    $product_ids = new JsonResponse($addResponse);
    $this->loggerFactory->get('Connection_ERR')->notice('Product API child response <pre><code>' . print_r($product_ids, TRUE) . '</code></pre>');
    /* whitelist API Logging Code */
    return new JsonResponse($addResponse);
  }

  public function removeUserWishLists() {
    $skuId = $this->requestStack->getCurrentRequest()->get('skuid');
    $wishList = [];
    $hostname = $this->sfccConfig->get("hostname");
    $midpath = $this->sfccConfig->get("midpath");
    $siteid = $this->sfccConfig->get("siteid");
    $clientid = $this->sfccConfig->get("clientid");
    $removeProductListUrl = $this->sfccConfig->get("customerprdlist");

    // Get Customer ID and Token
    $tempstore = $this->tempStoreFactory->get('userInfo');
    $customerId = $tempstore->get('customerId');
    $token = $tempstore->get('token');

    $productListUrlConstruct = $hostname.$siteid.$midpath.$removeProductListUrl;
    $productListUrl = str_replace("customerid",$customerId,$productListUrlConstruct);

    $apirequest = $this->searchservice->apiRequest($productListUrl,"GET",$token,"","","");
    $apiResponse = json_decode($apirequest['body'],TRUE);
    if(isset($apiResponse['data']) && !empty($apiResponse['data'])) {
      foreach ($apiResponse['data'] as $key => $value) {
        if(!empty($value['type']) && isset($value['type']) && $value['type'] == 'wish_list') {
          $link_url = $value['items_link']['link'];
          $components = parse_url($link_url);
          if (strpos($link_url, $hostname) === false) {
            $components_hostname = parse_url($hostname);
            $link_url = str_replace($components['host'], $components_hostname['host'], $link_url);
          }
          $itemsApirequest = $this->searchservice->apiRequest($link_url,"GET",$token,"","","");
          $itemsApiResponse = json_decode($itemsApirequest['body'],TRUE);

          /* whitelist API Logging Code */
          $logger_item_API_data = [
            'Value Item Link' => $value['items_link']['link'],
            'API Token' => $token,
            'API Method' => 'GET',
          ];
          $this->loggerFactory->get('Connection_ERR')->notice('whitelist API child Parameter <pre><code>' . print_r($logger_item_API_data, TRUE) . '</code></pre>');
          $this->loggerFactory->get('Connection_ERR')->notice('whitelist API child Response <pre><code>' . print_r($itemsApiResponse, TRUE) . '</code></pre>');
          /* end whitelist API Logging Code */

          if(isset($itemsApiResponse['data']) && !empty($itemsApiResponse['data'])) {
            foreach ($itemsApiResponse['data'] as $keys => $values) {
              if($values['product_id'] == $skuId) {
                $wishListProductId = $values['id'];

                $wishlistUrl = $this->sfccConfig->get("removewishlist");
                $wishlistUrlConstruct = $hostname.$siteid.$midpath.$wishlistUrl;

                $wishlistIdUpdate = str_replace("wishlist-id",$value['id'],$wishlistUrlConstruct);
                $wishlistItemIdUpdate = str_replace("wishlist-item-id",$wishListProductId,$wishlistIdUpdate);
                $customerIdUpdate = str_replace("customerid",$customerId,$wishlistItemIdUpdate);

                $removeApirequest = $this->searchservice->apiRequest(trim($customerIdUpdate),"DELETE",$token,"","","");
                $removeApiResponse = json_decode($removeApirequest['body'],TRUE);

                /* Remove API Logging Code */
                $logger_item_API_data = [
                  'White_list_URL' => $wishlistUrl,
                  'Customer_id' => $customerIdUpdate,
                  'api_token' => $token,
                  'api_method' => 'DELETE',
                ];
                $this->loggerFactory->get('Connection_ERR')->notice('Remove API Parameter <pre><code>' . print_r($logger_item_API_data, TRUE) . '</code></pre>');
                $this->loggerFactory->get('Connection_ERR')->notice('Remove API Response <pre><code>' . print_r($removeApiResponse, TRUE) . '</code></pre>');
                /* End remove API Logging Code */

                if(!isset($removeApiResponse['fault'])) {
                  $wishList[] = "success";
                }
                else {
                  $wishList[] = "fail";
                }
              }
            }
          }
          else {
            $wishList[] = "fail";
          }
        }
        else {
          $wishList[] = "fail";
        }
      }
    }
    else {
      $wishList[] = "fail";
    }
    /* whitelist API Logging Code */
    $logger_API_data = [
      'API URL' => $productListUrl,
      'API Token' => $token,
      'API Method' => 'GET',
    ];
    $this->loggerFactory->get('Connection_ERR')->notice('whitelist Parent API Parameter <pre><code>' . print_r($logger_API_data, TRUE) . '</code></pre>');
    $this->loggerFactory->get('Connection_ERR')->notice('whitelist Parent API response <pre><code>' . print_r($apiResponse, TRUE) . '</code></pre>');
    /* whitelist API Logging Code */

    return new JsonResponse($wishList);
  }

  // Appending Product Sitemap XML.
  public function getSfccProductXml() {
    $host = \Drupal::request()->getSchemeAndHttpHost();
    $path = $this->systemFile->get('default_scheme') . "://";
    $filePath = $this->fileSystem->realpath($path);
    $xmlFile = $filePath . '/sfcc-product-sitemap.xml';

    // Empty already existing Content
    $f = @fopen($xmlFile, "w+");
    if ($f !== false) {
      ftruncate($f, 0);
      $txt = '<?xml version="1.0" encoding="UTF-8"?>
      <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"></urlset>';
      fwrite($f, $txt);
      fclose($f);
    }

    $xml = simplexml_load_file($xmlFile);
    $xmlUrl = $this->sfccConfig->get("productsitemap");
    $baseXml = simplexml_load_file($xmlUrl);
    if(!empty($baseXml)) {
      if(!empty($baseXml->sitemap->loc)) {
        $productXml = simplexml_load_file($baseXml->sitemap->loc);
        if(!empty($productXml)) {
          foreach ($productXml as $key => $value) {
            $url = $xml->addChild('url');
            $url->addChild('loc',$value->loc);
          }
          $xml->asXML($filePath. '/sfcc-product-sitemap.xml');
          return new JsonResponse(['success']);
        }
        else {
          return new JsonResponse(['fail']);
        }
      }
      else {
        return new JsonResponse(['fail']);
      }
    }
    else {
      return new JsonResponse(['fail']);
    }
  }
}

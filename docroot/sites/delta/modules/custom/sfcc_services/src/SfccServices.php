<?php

namespace Drupal\sfcc_services;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ConnectException;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use GuzzleHttp\Cookie\SetCookie;
use Drupal\Core\TempStore\PrivateTempStoreFactory;

class SfccServices {
   /**
   * Drupal\Core\Logger\LoggerChannelFactoryInterface definition.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  protected $entityManager;

  /**
   * Guzzle\Client instance.
   *
   * @var \Guzzle\Client
   */
  protected $httpClient;

  /**
   * Drupal\Core\TempStore\PrivateTempStoreFactory definition.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStoreFactory
   */
  private $tempStoreFactory;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    ConfigFactoryInterface $configFactory,
    EntityTypeManagerInterface $entityManager,
    ClientInterface $httpClient,
    LoggerChannelFactoryInterface $logger_factory,
    PrivateTempStoreFactory $tempStoreFactory) {
    $this->configFactory = $configFactory->get('sfcc_services.sfcc_api_configs');
    $this->entityManager = $entityManager;
    $this->httpClient = $httpClient;
    $this->loggerFactory = $logger_factory;
    $this->tempStoreFactory = $tempStoreFactory;
  }

  public function guestAuth() {
    $hostname = $this->configFactory->get("hostname");
    $midpath = $this->configFactory->get("midpath");
    $siteid = $this->configFactory->get("siteid");
    $clientid = $this->configFactory->get("clientid");
    $apiType = $this->configFactory->get("credentialsapi");
    $requestUrl = $hostname.$siteid.$midpath.$apiType;
    if(!empty($hostname)) {
      $questRequest = $this->apiRequest($requestUrl,"POST","",$clientid,["type"=>"guest"],"");
      $requestBody = json_decode($questRequest['body'],TRUE);
      if(!isset($requestBody['fault'])) {
        $tempstore = $this->tempStoreFactory->get('userInfo');
        $tempstore->set('usertype',$requestBody['auth_type']);
        $tempstore->set('customerId',$requestBody['customer_id']);
        $tempstore->set('email',(isset($sessionResponse['email']) ? $sessionResponse['email'] : ""));
        $tempstore->set('visitorId',(isset($sessionResponse['customer_no']) ? $sessionResponse['customer_no'] : ""));
        $tempstore->set('token',$questRequest['header']['Authorization'][0]);
        $token = $questRequest['header']['Authorization'][0];
        $sessionApiUrl = $this->configFactory->get("crestesession");
        $sesstionAuthUrl = $hostname.$siteid.$midpath.$sessionApiUrl;
        $createSessionRequest = $this->apiRequest($sesstionAuthUrl,"POST",$token);
        $bodyResponse = json_decode($createSessionRequest['body'],TRUE);
        if(!isset($bodyResponse['fault'])){
          foreach ($createSessionRequest['header']['Set-Cookie'] as $key => $value) {
            if(strpos($value, "dwsid") !== false) {
              $splitedCookies = explode(";", $value);
              foreach ($splitedCookies as $cookiekey => $cookievalue) {
                if(strpos($cookievalue, "dwsid") !== false) {
                  $cookie = explode("dwsid=", $cookievalue);
                  setrawcookie("dwsid", trim($cookie[1]));
                }
              }
            }
          }
          return TRUE;
        }
        else {
          $this->loggerFactory->get('SFCC API Issue')
            ->notice($createSessionRequest['body']);
          return FALSE;
        }
      }
      else {
        $this->loggerFactory->get('SFCC API Issue')
          ->notice($questRequest['body']);
        return FALSE;
      }
    }
    else {
      return FALSE;
    }
  }

  public function sessionValidate($cookie){
    $hostname = $this->configFactory->get("hostname");
    $midpath = $this->configFactory->get("midpath");
    $siteid = $this->configFactory->get("siteid");
    $clientid = $this->configFactory->get("clientid");
    $apiType = $this->configFactory->get("credentialsapi");
    $requestUrl = $hostname.$siteid.$midpath.$apiType;
    if(!empty($hostname)) {
      $sessionValidateRequest = $this->apiRequest($requestUrl,"POST","",$clientid,["type"=>"session"],$cookie);
      $sessionResponse = json_decode($sessionValidateRequest['body'],TRUE);
      if(!isset($sessionResponse['fault'])) {
        $tempstore = $this->tempStoreFactory->get('userInfo');
        $tempstore->set('customerId',$sessionResponse['customer_id']);
        $tempstore->set('email',(isset($sessionResponse['email']) ? $sessionResponse['email'] : ""));
        $tempstore->set('visitorId',(isset($sessionResponse['customer_no']) ? $sessionResponse['customer_no'] : ""));
        if(!empty($sessionValidateRequest['header']['Authorization'][0])) {
          $tempstore->set('token',$sessionValidateRequest['header']['Authorization'][0]);
        }
        $tempstore->set('usertype',$sessionResponse['auth_type']);
        return TRUE;
      }
      else {
        $this->loggerFactory->get('Connection_ERR')
          ->notice($sessionValidateRequest['body']);
        $sessionExpired = $this->guestAuth();
        return TRUE;
      }
    }
    else {
      return FALSE;
    }

  }

  public function apiRequest($url,$method,$token = "",$clientId = "" ,$requestType = "", $cookie = "") {
    $response = [];
    $headers = [];
    $json = [];
    $options = [];

    if(!empty($token)){
      $headers['Authorization'] = $token;
    }
    if(!empty($cookie)) {
      $headers['cookie'] = "dwsid=".$cookie;
    }
    if(!empty($clientId)){
      $headers['x-dw-client-id'] = $clientId;
    }
    if(!empty($requestType)) {
      $options['json']  = $requestType;
    }
    $client = $this->httpClient;
    $options['headers']  = $headers;
    $options['http_errors'] = FALSE;
    try {
      $request = $client->$method($url, $options);
      $response['body'] = $request->getBody()->getContents();
      $response['header'] = $request->getHeaders();
    }
    catch (RequestException $e) {
      $this->loggerFactory->get('Connection_ERR')
        ->notice($requestType . "API SFCC Connection Error");
      return $response;
    }
    return $response;
  }

  public function getProducts($skus = "") {
    $productData = [];
    $hostname = $this->configFactory->get("hostname");
    $midpath = $this->configFactory->get("midpath");
    $siteid = $this->configFactory->get("siteid");
    $clientid = $this->configFactory->get("clientid");
    $apiType = $this->configFactory->get("productwidget");
    $requestUrl = $hostname.$siteid.$midpath.$apiType;
    $skuUrl= str_replace("productids",$skus,$requestUrl);
    $productWidgetUrl = $skuUrl.$clientid;
    $widgetRequest = $this->apiRequest($productWidgetUrl,"GET");
    $resultResponse = json_decode($widgetRequest['body'],TRUE);
    \Drupal::logger('sfcc_service')->notice($widgetRequest['body']);
    if(!isset($resultResponse['fault'])) {
      if($resultResponse['count'] > 0) {
        foreach ($resultResponse['data'] as $key => $value) {
          $productId = $value['id'];
          $title = $value['name'];
          $finish = $value['c_finish'];
          $pdpPath = (isset($value['c_productLink'])) ? $value['c_productLink'] : "";
          $collection = (isset($value['c_collection'])) ? $value['c_collection'] : "";
          $image = (isset($value['image_groups'][0]['images'][0]['dis_base_link'])) ? $value['image_groups'][0]['images'][0]['dis_base_link']."?sw=250&sh=250&sm=fit" : "";
          $productData[] = [
            'description' => $title,
            'finish' => $finish,
            'model_image' => $image,
            'model_number' => $productId,
            'collection_name' => "",
            'view_details_link' => $pdpPath,
            'view_docs_link' => $pdpPath. '#specsTab',
          ];
        }
      }
    }
    return $productData;
  }
}

<?php

namespace Drupal\sfcc_services\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * An API Config controller.
 */
class SfccConfigForm extends ConfigFormBase {

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs a new DFApiSettingsForm object.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    // parent::__construct($config_factory);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'sfcc_services.sfcc_api_configs',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'sfcc_configs';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('sfcc_services.sfcc_api_configs');

    $form['hostname'] = [
      '#type' => 'textfield',
      '#title' => $this->t('HostName'),
      '#description' => $this->t('Hostname of the API server.'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('hostname'),
    ];

    $form['midpath'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Mid Path'),
      '#description' => $this->t('API URL Mid URL.'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('midpath'),
    ];

    $form['siteid'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Site Id'),
      '#description' => $this->t('Site id.'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('siteid'),
    ];

    $form['clientid'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client Id'),
      '#description' => $this->t('Client Id.'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('clientid'),
    ];

    $form['credentialsapi'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Credentials API'),
      '#description' => $this->t('Credentials API.'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('credentialsapi'),
    ];

    $form['crestesession'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Create session API'),
      '#description' => $this->t('Create session API.'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('crestesession'),
    ];

    $form['basket'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Basket API URL'),
      '#description' => $this->t('Ex /customers/customerid/baskets => customerid will replaced using session'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('basket'),
    ];

    $form['updatequantity'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Update product quantity API URL'),
      '#description' => $this->t('Update product quantity API URL'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('updatequantity'),
    ];

    $form['removeproduct'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Remove product in Minicart'),
      '#description' => $this->t('Remove Product API URL'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('removeproduct'),
    ];

    $form['productwidget'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Get product widgets '),
      '#description' => $this->t('Get product widgets API'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('productwidget'),
    ];

    $form['productsitemap'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Product sitemap URL'),
      '#description' => $this->t('Product sitemap URL'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('productsitemap'),
    ];

    $form['search'] = [
      '#type' => 'details',
      '#title' => $this->t('Search Related API URLs'),
    ];

    $form['search']['searchsuggestion'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Search Suggestion API'),
      '#description' => $this->t('Search Suggestion API.'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('searchsuggestion'),
    ];

    $form['search']['searchpage'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Search Page API'),
      '#description' => $this->t('Search Page API URL.'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('searchpage'),
    ];

    $form['wishlistsavelater'] = [
      '#type' => 'details',
      '#title' => $this->t('Whishlist and Save for later'),
    ];

    $form['wishlistsavelater']['getwishlistitems'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Get Wishlist Items'),
      '#description' => $this->t('Get Wishlist Items.'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('getwishlistitems'),
    ];

    $form['wishlistsavelater']['customerprdlist'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Customer product list '),
      '#description' => $this->t('Customer product list.'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('customerprdlist'),
    ];

    $form['wishlistsavelater']['addtosfl'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Add product to SFL'),
      '#description' => $this->t('Add product to SFL.'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('addtosfl'),
    ];

    $form['wishlistsavelater']['createsfl'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Create SFL category'),
      '#description' => $this->t('Create SFL category.'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('createsfl'),
    ];

    $form['wishlistsavelater']['removewishlist'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Remove product from wishlist'),
      '#description' => $this->t('Remove product from wishlist.'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('removewishlist'),
    ];

    $form['basketvalid'] = [
      '#type' => 'details',
      '#title' => $this->t('Cart basket not valid message'),
    ];

    $form['basketvalid']['createbasketmsg'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Cart Message'),
      '#description' => $this->t('This message will display on cart when c_basket_valid SFCC response is false. '),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('createbasketmsg'),
    ];

    $form['sfcchomepageID'] = [
        '#type' => 'details',
        '#title' => $this->t('SFCC Home page ID'),
    ];

    $form['sfcchomepageID']['sfcchomeid'] = [
        '#type' => 'textfield',
        '#title' => $this->t('SFCC Home Page ID'),
        '#description' => $this->t('This ID will use for SFCC home page after importing config. '),
        '#maxlength' => 255,
        '#size' => 64,
        '#default_value' => !empty($config->get('sfcchomeid')) ? $config->get('sfcchomeid') : 8138,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $this->config('sfcc_services.sfcc_api_configs')
      ->set('hostname', $form_state->getValue('hostname'))
      ->set('midpath', $form_state->getValue('midpath'))
      ->set('siteid', $form_state->getValue('siteid'))
      ->set('clientid', $form_state->getValue('clientid'))
      ->set('credentialsapi', $form_state->getValue('credentialsapi'))
      ->set('crestesession', $form_state->getValue('crestesession'))
      ->set('searchsuggestion', $form_state->getValue('searchsuggestion'))
      ->set('searchpage', $form_state->getValue('searchpage'))
      ->set('basket', $form_state->getValue('basket'))
      ->set('updatequantity', $form_state->getValue('updatequantity'))
      ->set('removeproduct', $form_state->getValue('removeproduct'))
      ->set('customerprdlist', $form_state->getValue('customerprdlist'))
      ->set('addtosfl', $form_state->getValue('addtosfl'))
      ->set('createsfl', $form_state->getValue('createsfl'))
      ->set('productwidget', $form_state->getValue('productwidget'))
      ->set('productsitemap', $form_state->getValue('productsitemap'))
      ->set('getwishlistitems', $form_state->getValue('getwishlistitems'))
      ->set('removewishlist', $form_state->getValue('removewishlist'))
      ->set('createbasketmsg', $form_state->getValue('createbasketmsg'))
      ->set('sfcchomeid', $form_state->getValue('sfcchomeid'))
      ->save();
  }
}

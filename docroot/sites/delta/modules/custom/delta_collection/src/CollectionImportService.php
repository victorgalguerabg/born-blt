<?php

namespace Drupal\delta_collection;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\delta_cache\DeltaCacheService;
use Drupal\delta_services\DeltaService;
use stdClass;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\product_details\ProductService;

/**
 * Class CollectionImportService.
 */
class CollectionImportService {

  /**
   * Drupal\sync\DeltaService definition.
   *
   * @var \Drupal\sync\DeltaService
   */
  protected $deltaservice;

  /**
   * Implementation of EntityTypeManagerInterface.
   *
   * @var Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entitytypemanager;

  /**
   * Drupal\Core\Logger\LoggerChannelFactoryInterface definition.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactorXyInterface
   */
  protected $logger;

  /**
   * Drupal\Core\Messenger\MessengerInterface definition.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Drupal\delta_cache\DeltaCacheService definition.
   *
   * @var \Drupal\delta_cache\DeltaCacheService
   */
  protected $deltaCache;

  /**
   * Drupal\product_details\ProductService definition.
   *
   * @var \Drupal\product_details\ProductService
   */
  protected $productService;

  /**
   * Symfony\Component\HttpFoundation\RequestStack definition.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $request;

  /**
   * Drupal\Core\Config\ImmutableConfig variable.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $deltaConfig;

  /**
   * Drupal\Core\Config\ImmutableConfig variable.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $syncConfig;

  /**
   * Class constructor.
   */
  public function __construct(DeltaService $deltaservice,
                              RequestStack $request,
                              EntityTypeManagerInterface $entitytypemanager,
                              LoggerChannelFactoryInterface $logger,
                              MessengerInterface $messenger,
                              ConfigFactoryInterface $config_factory,
                              DeltaCacheService $deltaCache,
                              ProductService $productService) {
    $this->deltaservice = $deltaservice;
    $this->request = $request;
    $this->entitytypemanager = $entitytypemanager;
    $this->logger = $logger;
    $this->messenger = $messenger;
    $this->configFactory = $config_factory;
    $this->deltaCache = $deltaCache;
    $this->productService = $productService;
    $this->deltaConfig = $this->configFactory->get('delta_services.deltaservicesconfig');
    $this->syncConfig = $this->configFactory->get('sync.api_settings');
  }

  /**
   * Imports collection API data.
   */
  public function importCollection() {
    $vid = 'collections';
    $entities = $this->entitytypemanager
      ->getStorage('taxonomy_term')->loadTree($vid);

    $method = 'GET';
    $headers = add_headers(FALSE);
    $data = '';

    $domain = $this->deltaConfig->get('api_base_path');
    $imageKey = $this->syncConfig->get('collection_product_image');
    $operations = [];
    foreach ($entities as $entity) {
      $url = $domain . $this->syncConfig->get('collection_api') . $entity->name;
      $cacheId = 'collection_' . $entity->name;
      $innovationKitchen = $innovationBathroom = [];
      $collectionRaw = $collectionType = [];
      $response = $this->deltaservice
        ->apiCall($url, $method, $headers, $data, 'COLLECTION-SYNC');
      $this->deltaCache->deltaSetCache($cacheId, $response, 3600);
      $collectionRaw = json_decode($response['response']);
      $collectionObjBathroom = $collectionObjKitchen = [];
      $categories = [];

      //  if (count($collectionRaw) > 0) {

      $collectionRaw = array_shift($collectionRaw);
      $brandName = 'Others';
      $brandObj = $collectionRaw->brand;
      $discontinued = 0;
      if (isset($collectionRaw->colObsolete)) {
        $discontinued = ($collectionRaw->colObsolete == 'true')? 1 : 0;
      }
      if (is_array($brandObj)) {
        $result = preg_grep('/Delt(\w+)/', $brandObj);
        if (count($result) > 0) {
          $brandName = 'Delta';
        }
      }

      if ($collectionRaw->contemporary) {
        $collectionType[] = 'contemporary';
      }

      if ($collectionRaw->traditional) {
        $collectionType[] = 'traditional';
      }

      if ($collectionRaw->transitional) {
        $collectionType[] = 'transitional';
      }

      $collectionObjBathroom = $this->setCollectionMapping($entity, 'Delta_Bathroom', $collectionRaw, $categories, 'recommendedBathSuite', $imageKey);
      $collectionObjKitchen = $this->setCollectionMapping($entity, 'Delta_Kitchen', $collectionRaw, $categories, 'recommendedKitchenSuite', $imageKey);
      $collectionObjKitchenSinks = $this->setCollectionMapping($entity, 'Delta_Kitchen_Kitchen Sinks', $collectionRaw, $categories, 'recommendedKitchenSuite', $imageKey);
      $collectionObjBathroomSinks = $this->setCollectionMapping($entity, 'Delta_Bathroom_Bathroom Sinks', $collectionRaw, $categories, 'recommendedBathSuite', $imageKey);

      if ($brandName == 'Others') {
        $categories = []; // reset categories if brand is not Delta or null
      }
      $data = new stdClass();
      $data->tid = $entity->tid;
      //$data->field_collection_type = $collectionType;
      //$data->field_collection_category = $categories;
      $data->field_bathroom_sku = $collectionObjBathroom['sku'];
      $data->field_kitchen_sku = $collectionObjKitchen['sku'];
      $data->field_bathroom_sink_sku = $collectionObjBathroomSinks['sku'];
      $data->field_kitchen_sink_sku = $collectionObjKitchenSinks['sku'];
      $data->field_innovationfeature_bathroom = $collectionObjBathroom['references'];
      $data->field_innovationfeature_kitchen = $collectionObjKitchen['references'];
      $data->field_innovation_bath_sink = $collectionObjBathroomSinks['references'];
      $data->field_innovation_kitchen_sink = $collectionObjKitchenSinks['references'];
      $data->field_kitchen_innovation_label = $collectionObjKitchen['labels'];
      $data->field_bathroom_innovation_label = $collectionObjBathroom['labels'];
      $data->field_kitchen_sinks_innovation = $collectionObjKitchenSinks['labels'];
      $data->field_bathroom_sinks_innovation = $collectionObjBathroomSinks['labels'];
      $data->field_bathroom_collection_image = $collectionObjBathroom['image'];
      $data->field_kitchen_collection_image = $collectionObjKitchen['image'];
      $data->field_bathroom_sinks_col_image = $collectionObjBathroomSinks['image'];
      $data->field_kitchen_sinks_col_image = $collectionObjKitchenSinks['image'];
      $data->field_facet_label = $this->deltaservice->translationsCode($entity->name);
      $data->field_kitchen_discontinued = $discontinued;
      $data->field_bathroom_discontinued = $discontinued;


      $this->logger->get('delta_collection')
        ->info(
          'Collection API for ' . $entity->name
          . print_r($data, TRUE)
        );
      $operations[] = [
        '_collection_sync',
        [$data],
      ];
      // }
      //  }
      /* else {
         $this->logger->get('delta_collection')
           ->info('No API data for - ' . $entity->name);
       }*/
    }
    $batch = [
      'title' => 'Collection Sync...',
      'operations' => $operations,
      'finished' => '_collection_finishedcallback',
    ];
    batch_set($batch);
  }

  /**
   * Sets Collection mapping for Kitchen and Bathroom Category.
   */
  public function setCollectionMapping($entity, $entityName, $collectionRaw, &$category, $recommendedKey, $imageKey) {

    $collectionData = $facetObj = [];

    $facetObj = $this->getInnovationFacets(
      $entity->name,
      $entityName
    );
    $innovationLabel = $innovationTerms = $prodData = [];
    $collectionImage = $sku = '';

    $innovationLabel = $facetObj['innovationLabel'];
    $innovationTerms = $facetObj['innovationTerms'];

    if ($collectionRaw->$recommendedKey != NULL) {
      $sku = strtoupper(array_shift($collectionRaw->$recommendedKey));
    }
    else {
      $sku = $facetObj['prodName'];
    }
    // Create commerce prod for the  sku.
    $prodData = $this->productService->syncProduct($sku, TRUE);
    $categories = $prodData['categories'];
    if (is_array($categories)) {
      if ($entityName == 'Delta_Kitchen') {
        $result = preg_grep('/Kitchen(\w+)/', $categories);
        if (count($result) > 0) {
          $category[] = 'kitchen';
        }
      }
      elseif ($entityName == 'Delta_Bathroom') {
        $result = preg_grep('/Bathroom(\w+)/', $categories);
        if (count($result) > 0) {
          $category[] = 'bathroom';
        }
      }
      elseif ($entityName == 'Delta_Kitchen_Kitchen Sinks') {
              $result = preg_grep('/Kitchen_Kitchen Sinks(\w+)/', $categories);
        if (count($result) > 0) {
          $category[] = 'kitchen_sinks';
        }
      }
      elseif ($entityName == 'Delta_Kitchen_Kitchen Sinks') {
              $result = preg_grep('/Bathroom_Bathroom Sinks(\w+)/', $categories);
        if (count($result) > 0) {
          $category[] = 'bathroom_sinks';
        }
      }
    }
    $imageData = $prodData[$imageKey];
    //$collectionImage = '<img src="' . $imageData . '" alt="' . $entity->name . '"  title ="' . $entity->name . '">';
    $imageDataSrc = '/themes/custom/delta_theme/images/lazyload.png';
    $collectionImage = '<img data-source="'.$imageData.'" src="' . $imageDataSrc . '" alt="' . $entity->name . '"  title ="' . $entity->name . '">';
    $collectionData['category'] = $category;
    $collectionData['sku'] = $sku;
    $collectionData['labels'] = $innovationLabel;
    $collectionData['references'] = $innovationTerms;
    $collectionData['image'] = $collectionImage;
    return $collectionData;
  }


  /**
   * Gets innovation and feature facet values for a term.
   */
  public function getInnovationFacets($termName, $category) {
    $domain = $this->deltaConfig->get('api_base_path');
    $method = 'GET';
    $headers = add_headers(FALSE);
    $data = '';
    $innovationTerms = [];
    $innovationLabel = [];
    $facetTerms = [];
    $innovationUrl = $domain . $this->syncConfig->get('collection_innovation_api');
    $innovationUrl = str_replace('{term}', $termName, $innovationUrl);
    $innovationUrl = str_replace('{category}', $category, $innovationUrl);
    $innovation = $this->deltaservice
      ->apiCall($innovationUrl, $method, $headers, $data, 'COLLECTION-INNOVATION-SYNC');
    $innovation = json_decode($innovation['response']);
    $facetTerms['prodName'] = $innovation->content[0]->name;
    $innovationFacets = $innovation->facets;
    if (count($innovationFacets) > 0) {
      foreach ($innovationFacets as $facet) {
        $facetName = $facet->name;
        if (count($facet->terms) > 0) {
          foreach ($facet->terms as $term) {
            $termData = $this
              ->entitytypemanager
              ->getStorage('taxonomy_term')
              ->loadByProperties(
                [
                  'name' => $term->term,
                ]
              );
            $termData = array_shift($termData);
            // if ($facetName == 'facetInnovation') {
            $innovationTerms[] = ['target_id' => $termData->tid->value];
            if($termData->field_display_flag->value == 0){
              $innovationLabel[] = $this->deltaservice->translationsCode($termData->field_facet_label->value);
            }
            // }
            // else {
            // if ($facetName == 'facetFeature') {
            // $featureTerms[] = ['target_id' => $termData->tid->value];
            //}
            //}
          }
        }
      }
      $facetTerms['innovationTerms'] = $innovationTerms;
      $facetTerms['innovationLabel'] = $innovationLabel;
    }
    return $facetTerms;
  }

}

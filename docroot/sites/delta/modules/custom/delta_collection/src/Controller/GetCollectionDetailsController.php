<?php

namespace Drupal\delta_collection\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\delta_services\DeltaService;
use Drupal\delta_cache\DeltaCacheService;
use Drupal\Component\Serialization\Json;

/**
 * Class GetCollectionDetailsController.
 */
class GetCollectionDetailsController extends ControllerBase {

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Symfony\Component\HttpFoundation\RequestStack definition.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The default cache bin.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * Drupal\delta_services\DeltaService definition.
   *
   * @var \Drupal\delta_services\DeltaService
   */
  protected $deltaservice;

  /**
   * Drupal\delta_cache\DeltaCacheService definition.
   *
   * @var \Drupal\delta_cache\DeltaCacheService
   */
  protected $deltaCache;

  /**
   * Constructs a new GetCollectionDetailsController object.
   *
   * @param Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   The default cache bin.
   * @param \Drupal\delta_services\DeltaService $deltaservice
   *   The deltaservice to make API calls.
   */
  public function __construct(ConfigFactoryInterface $config_factory,
                              RequestStack $request_stack,
                              DeltaService $deltaservice,
                              DeltaCacheService $deltaCache) {
    $this->configFactory = $config_factory;
    $this->requestStack = $request_stack;
    $this->deltaservice = $deltaservice;
    $this->deltaCache = $deltaCache;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('request_stack'),
      $container->get('delta_services.service'),
      $container->get('delta_cache.cache_service')
    );
  }

  /**
   * Get collection details from api based on Category, Collection Name, size and pagenumber.
   *
   * @return JSON
   *   A properly formatted json response from api.
   */
  public function getCollectionDetails() {
    $response = new Response();
    $sort = '';
    $finishFacet = '';
    $cdpConfig = $this->configFactory->get('delta_services.cdpconfig');
    $prop_config = $this->configFactory->get('product_details.settings');
    $discontinue_text = $prop_config->get('rest_api_settings.discontinue_ribbontext');
    $coming_soon_text = $prop_config->get('rest_api_settings.comming_soon_ribbontext');
    $queryString = $this->requestStack->getCurrentRequest()->query->all();
    if ($queryString['sub_category']) {
      $collectionSubCategory = $queryString['sub_category'];
    }
    else{
      $collectionSubCategory = '';
    }
    $collectionCategory = $queryString['category'];
    $collectionName  = $queryString['collection'];
    $pagenumber = $queryString['pagenumber'];
    $size = $queryString['size'];
    if (!$size) {
      $size = $cdpConfig->get('collection_detail_items_per_page');
    }
    $variables['#attached']['drupalSettings']['collectionProductsPerPage'] = $size;
    $cache = $queryString['cache'];
    if (array_key_exists('facets', $queryString)){
      $finishFacet = $queryString['facets'];
    }
    
    if(array_key_exists('sort', $queryString)){
      $sort =  $queryString['sort'] ;
    }
    // Keep sort_ListPrice filter throughout , so as to get filter results.
    if (strpos($sort, 'sort_ListPrice') !== FALSE) {
        $sort = $queryString['sort'];
    }
    else {
        $sort = 'sort_MaterialStatus=ASC&sort_AvailableToShipDate=DESC&sort_ListPrice=DESC';
    }

    $cacheId = $this->getCid($queryString);
    if ($cache == 'T') {
      $cachedResult = $this->deltaCache->deltaGetCache($cacheId);
      if ($cachedResult) {
        $result = $cachedResult;
        $response->setContent($result['response']);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
      }
    }

    $placeholders = ['{category}', '{collection}','{pagenumber}', '{size}'];
    $replacements = [$collectionCategory, $collectionName, $pagenumber, $size];

    $collectionDetailApiArgs = str_replace(
      $placeholders,
      $replacements,
      $cdpConfig->get('collection_detail_api')
    );
    $apiBaseDetails = $this->configFactory
     ->get('delta_services.deltaservicesconfig');
    $collectionDetailAPIUrl = $apiBaseDetails->get('api_base_path');
    if ($collectionSubCategory){
      $collectionDetailApiArgs .= '&and_categories=' . $collectionSubCategory;
    }
    if ($finishFacet){
      $collectionDetailApiArgs = str_replace("&defaultModel=true","",$collectionDetailApiArgs);
      $collectionDetailApiArgs .= '&facetFinish=' . $finishFacet;
    }
    $collectionDetailAPIUrl .= $collectionDetailApiArgs . '&' . $sort .'&or_MaterialStatus=30&or_MaterialStatus=40&or_MaterialStatus=50&or_MaterialStatus=60&or_MaterialStatus=70';
    $result = $this->deltaservice->apiCall($collectionDetailAPIUrl, "GET", [], [], "CDP");
    $this->deltaCache->deltaSetCache($cacheId, $result, 3600);
    if ($result['status'] == 'SUCCESS') {
      $readJsonArr = json_decode($result['response'], TRUE);
      if (count($readJsonArr)) {
        $ribbonText = "";
        $ribboncolor = "";
        foreach ($readJsonArr['content'] as $key => $productAttributes) {
          $materialstaus = isset($productAttributes['values']['MaterialStatus']) ? $productAttributes['values']['MaterialStatus'] : "";
          $discontinued = ($materialstaus >= 50) ? TRUE : FALSE;
          $commingSoon = FALSE;
          $ribbonVal = $productAttributes['values']['WebExclusiveCustomerItem'];
          $availableToOrderDate = isset($productAttributes['values']['AvailableToOrderDate']) ? $productAttributes['values']['AvailableToOrderDate'] : '';
          $availableToShipDate = isset($productAttributes['values']['AvailableToShipDate']) ? $productAttributes['values']['AvailableToShipDate'] : '';
          $orderDate = strtotime($availableToOrderDate);
          $shipDate = strtotime($availableToShipDate);
          $currentDate = strtotime(date('Y-m-d', time()));
          if (empty($orderDate) || empty($shipDate)) {
            $shipDate = $currentDate - (60 * 60 * 24);
          }
          if ($discontinued) {
            $ribbonText = $discontinue_text;
            $ribboncolor = "gray-30";
          }
          else {
            if ($currentDate < $shipDate) {
              if ($currentDate > $orderDate) {
                $commingSoon = TRUE;
                $ribbonText = $coming_soon_text;
                $ribboncolor = "delta-red";
              }
            }else{
              $ribbonText = "";
              $ribboncolor = "";
            }
          }
          if (!empty($ribbonVal)) {
            if ($ribbonVal != 'NA' &&
              $ribbonVal != 'OEM' &&
              $ribbonVal != 'Retail' &&
              $ribbonVal != 'Warranty' &&
              $ribbonVal != 'Internal Use Only' &&
              $discontinued != TRUE &&
              $commingSoon != TRUE
            ) {
              $ribbonText = "Only at " . str_replace("_", " and ", $ribbonVal);
              $ribboncolor = "gray-30";
            }
          }
          $readJsonArr['content'][$key]['ribbontext'] = $ribbonText;
          $readJsonArr['content'][$key]['ribboncolor'] = $ribboncolor;
        }
        $result = ['response' => json_encode($readJsonArr)];
      }
      $response->setContent($result['response']);
      $response->headers->set('Content-Type', 'application/json');
    }
    else {
      $response->setContent('API Response - ERROR');
    }
    return $response;
  }

  /**
   * Get collection finish facets from api based on Category, Collection Name, size and pagenumber.
   *
   * @return JSON
   *   A properly formatted json response from api.
   */
  function getCollectionFinishFacets(){
    $sort = '';
    $response = new Response();
    $cdpConfig = $this->configFactory->get('delta_services.cdpconfig');
    $queryString = $this->requestStack->getCurrentRequest()->query->all();
    if ($queryString['sub_category']) {
      $collectionSubCategory = $queryString['sub_category'];
    }
    else{
      $collectionSubCategory = '';
    }
    $collectionCategory = $queryString['category'];
    $collectionName  = $queryString['collection'];
    $pagenumber = $queryString['pagenumber'];
    $size = $cdpConfig->get('collection_detail_items_per_page');
    $variables['#attached']['drupalSettings']['collectionProductsPerPage'] = $size;
    $cache = $queryString['cache'];
    if(array_key_exists('sort', $queryString)){
      $sort =  $queryString['sort'] ;
    }

    $cacheId = $this->getCid($queryString) . '&finishfacets';
    if ($cache == 'T') {
      $cachedResult = $this->deltaCache->deltaGetCache($cacheId);
      if ($cachedResult) {
        $result = $cachedResult;
        $response->setContent($result['response']);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
      }
    }

    $placeholders = ['{category}', '{collection}', '{size}'];
    $replacements = [$collectionCategory, $collectionName, $size];

    $collectionDetailApiArgs = str_replace(
      $placeholders,
      $replacements,
      $cdpConfig->get('collection_detail_finish_api')
    );
    $apiBaseDetails = $this->configFactory
     ->get('delta_services.deltaservicesconfig');
    $collectionFacetAPIUrl = $apiBaseDetails->get('api_base_path');
    if ($collectionSubCategory){
      $collectionFacetAPIUrl .= $collectionDetailApiArgs . '&and_categories=' . $collectionSubCategory . '&' . $sort;
    }
    else{
      $collectionFacetAPIUrl .= $collectionDetailApiArgs . '&' . $sort;
    }
    $result = $this->deltaservice->apiCall($collectionFacetAPIUrl, "GET", [], [], "CDP");
    $this->deltaCache->deltaSetCache($cacheId, $result, 3600);
    if ($result['status'] == 'SUCCESS') {
      $response->setContent($result['response']);
      $response->headers->set('Content-Type', 'application/json');
    }
    else {
      $response->setContent('API Response - ERROR');
    }
    return $response;

  }
  /**
   * {@inheritdoc}
   */
  public function getCid($queryString = []) {
    return implode('&', $queryString);
  }

}

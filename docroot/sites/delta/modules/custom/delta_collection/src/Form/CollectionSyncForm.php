<?php

namespace Drupal\delta_collection\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\delta_collection\CollectionImportService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class CollectionImportForm.
 */
class CollectionSyncForm extends FormBase {

  /**
   * Drupal\delta_collection\CollectionImportService definition.
   *
   * @var \Drupal\delta_collection\CollectionImportService
   */
  protected $collectionService;

  /**
   * Class constructor.
   */
  public function __construct(CollectionImportService $collectionService) {
    $this->collectionService = $collectionService;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('delta_collection.import')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'collection_import_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $nid = '') {

    $form['choose'] = [
      '#type' => 'select',
      '#title' => $this->t('Import Collection'),
      '#required' => TRUE,
      '#options' => [
        'insert' => 'Import Collection',
      ],
      '#default_value' => ['insert'],
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#id' => 'import_category',
      '#value' => $this->t('Import'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $action = $form_state->getValue('choose');
    if ($action == 'insert') {
      $this->collectionService->importCollection();
    }
  }

}

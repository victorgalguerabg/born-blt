<?php

namespace Drupal\delta_blogs\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\delta_blogs\BlogImportService;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Class ImportForm.
 */
class ImportForm extends FormBase {

  /**
   * Drupal\Core\Logger\LoggerChannelFactoryInterface definition.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;
  /**
   * Drupal\Core\Messenger\MessengerInterface definition.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;
  /**
   * Drupal\delta_blogs\BlogImportService definition.
   *
   * @var \Drupal\delta_blogs\BlogImportService
   */
  protected $deltaBlogsXmlimport;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new ImportForm object.
   */
  public function __construct(
    LoggerChannelFactoryInterface $logger_factory,
    MessengerInterface $messenger,
    EntityTypeManagerInterface $entity_type_manager,
    BlogImportService $delta_blogs_xmlimport
  ) {
    $this->deltaBlogsXmlimport = $delta_blogs_xmlimport;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('logger.factory'),
      $container->get('messenger'),
      $container->get('entity_type.manager'),
      $container->get('delta_blogs.xmlimport')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'import_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['xml_file'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('XML file'),
      '#description' => $this->t('XML file containing blogs.'),
      '#weight' => '0',
      '#required' => TRUE,
      '#upload_location' => 'public://blog',
      '#upload_validators' => [
        'file_validate_extensions' => ['xml'],
      ],
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $form_file = $form_state->getValue('xml_file', 0);
    if (isset($form_file[0]) && !empty($form_file[0])) {
      $id = $form_file[0];
      $file = $this->entityTypeManager
        ->getStorage('file')
        ->load($id);
      $path = file_create_url($file->getFileUri());
      $this->deltaBlogsXmlimport->getXml($path);
      return $this->redirect('<front>');
    }
  }

}

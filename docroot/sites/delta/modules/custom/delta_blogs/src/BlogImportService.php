<?php

namespace Drupal\delta_blogs;

use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Link;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Url;

/**
 * Class BlogImportService.
 */
class BlogImportService {

  /**
   * Drupal\Core\Render\RendererInterface definition.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * Drupal\Core\Logger\LoggerChannelFactoryInterface definition.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $logger;

  /**
   * Drupal\Core\File\FileSystemInterface definition.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Drupal\Core\Messenger\MessengerInterface definition.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Symfony\Component\HttpFoundation\RequestStack definition.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Constructs a new BlogImportService object.
   */
  public function __construct(RendererInterface $renderer,
                              EntityTypeManagerInterface $entity_type_manager,
                              LoggerChannelFactoryInterface $logger,
                              MessengerInterface $messenger,
                              FileSystemInterface $file_system
  ) {
    $this->renderer = $renderer;
    $this->entityTypeManager = $entity_type_manager;
    $this->logger = $logger;
    $this->messenger = $messenger;
    $this->fileSystem = $file_system;
    $this->vocabulary = 'tags';
    $this->imgPrefix = 'referenceInField_dlt_x0025_3ArichText_en_';
    $this->deltaImgPath = '';
  }

  /**
   * Getxml.
   *
   * @return string
   *   Return Hello string.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function getXml($path) {

    // $file = "/var/www/html/blogs.xml";

    stream_context_set_default(
      [
        'http' => [
          'header' => "Accept: text/xml"
        ]
      ]
    );
    $feed = file_get_contents($path);
    $xml = simplexml_load_string($feed);
    if (!$xml) {
      // error opening the file
      $error = libxml_get_errors();
      $this->logger
        ->get('delta_blogs')
        ->warning(print_r($error, TRUE));
    }
    $operations = [];

    foreach ($xml as $item) {
      $machineName = $item->getName();
      if (count($this->checkBlogExists($machineName)) == 0) {

        $namespaces = $item->getNameSpaces(TRUE);
        $metaData = (array) $item->attributes();
        $metaData = $metaData['@attributes'];
        $itemAttributes = $item->attributes($namespaces['j']);
        $itemAttributes = (array) $itemAttributes;
        $itemAttributes = $itemAttributes['@attributes'];

        $wrappedTag = $item
          ->{'article-content-area'}
          ->{'wrapped-text'};

        $wrappedTagJcr = $item->children('j', TRUE)
          ->attributes($namespaces['jcr']);

        $tagName = '';
        $tid = $wrappedTagJ = [];
        if (!is_null($item
          ->{'article-content-area'}
          ->{'wrapped-text'})) {

          unset($wrappedTagJ);
          $wrappedTagJ = (array) $item
            ->{'article-content-area'}
            ->{'wrapped-text'}
            ->attributes($namespaces['j']);
          $tagName = $wrappedTagJ['@attributes']['tagList'];

          $tid = $this->taxonomyTermCreate($tagName, $this->vocabulary);
        }

        $nodeData = (array) $wrappedTagJcr;
        $nodeData = $nodeData['@attributes'];

        if (!is_null($wrappedTagAttributes = $item
          ->{'article-content-area'}
          ->{'wrapped-text'})) {

          $wrappedTagAttributes = $item
            ->{'article-content-area'}
            ->{'wrapped-text'}
            ->children($namespaces['j']);
        }
        $wrappedTagBanner = [];
        $wrappedTagBannerImg = '';

        if (!is_null($item
          ->{'article-content-area'}
          ->{'wrapped-text'}
          )) {

          $articleRawdata = (array) $item
            ->{'article-content-area'}
            ->{'wrapped-text'}->children();
          unset($articleRawdata['vanityUrlMapping']);
          unset($articleRawdata['article']);
          $articleImgIndex = array_shift(array_keys($articleRawdata));


          if($item
            ->{'article-content-area'}
            ->{'wrapped-text'}
            ->{$articleImgIndex}){
            $wrappedTagBanner = $item
              ->{'article-content-area'}
              ->{'wrapped-text'}
              ->{$articleImgIndex}
              ->children($namespaces['j'])
              ->translation_en;
            $wrappedTagBanner = (array) $wrappedTagBanner;
            $wrappedTagBanner = $wrappedTagBanner['@attributes'];

            if ($wrappedTagBanner['node'] != NULL) {
              $wrappedTagBannerImgAlt = $wrappedTagBanner['alternateText'];
              $wrappedTagBannerImg = "<img alt='";
              $wrappedTagBannerImg .= $wrappedTagBannerImgAlt . "' ";
              $wrappedTagBannerImg .= "src='";
              $wrappedTagBannerImg .= $this->deltaImgPath;
              $wrappedTagBannerImg .= $wrappedTagBanner['node'];
              $wrappedTagBannerImg .= "'>";
              // $wrappedTagBannerImgSrc = $this->deltaImgPath;
              // $wrappedTagBannerImgSrc .= $wrappedTagBanner['node'];
              $wrappedTagBannerImgAlt = $wrappedTagBanner['alternateText'];
              $wrappedTagBannerImgName = explode('/', $wrappedTagBanner['node']);
              $wrappedTagBannerImgName = $wrappedTagBannerImgName[count($wrappedTagBannerImgName) - 1];
            }
          }

        }

        $translation_en = $item
          ->children('j', TRUE);

        $translation_en_img = $translation_en->attributes($namespaces['dlt']);
        $translation_en_img = (array) $translation_en_img;
        $translation_en_img = $translation_en_img['@attributes']['articleThumbnailImage'];
        $translation_en_data = $translation_en->attributes($namespaces['j']);
        $translation_en_data = (array) $translation_en_data;
        $translation_en_data = $translation_en_data['@attributes'];

        $translation_en_jcr = $translation_en->attributes($namespaces['jcr']);
        $translation_en_jcr = (array) $translation_en_jcr;

        $translation_en_jcr = $translation_en_jcr['@attributes'];

        $images = [];
        if (!is_null($wrappedTagAttributes)) {

          $resources = $wrappedTagAttributes;

          if (is_array($resources)) {
            array_shift($resources);
          }

          foreach ($resources as $k => $imageData) {

            $imageData = (array) $imageData;

            if (!empty($imageData['@attributes']['reference'])) {
              $images[$k] = $imageData['@attributes']['reference'];
            }
          }
        }

        $description = '';
        if (!is_null($wrappedTag
          ->{'article'})) {

          $description = $wrappedTag
            ->{'article'}
            ->attributes($namespaces['j']);
          $description = (array) $description;
          $description = $description['@attributes']['richText'];

          $subTitle = $wrappedTag->{'article'}->attributes($namespaces['jcr']);
          $subTitle = (array) $subTitle;
          $subTitle = $subTitle['@attributes'];
          $subTitle = $subTitle['title'];

        }

        $descAnchor = $this->updateHref($description, $images);

        $regex='|<a\s*href="([^"]+)"[^>]+>([^<]+)</a>|';
        preg_match_all($regex,$description,$anchorDesc,PREG_SET_ORDER);

        if (count($anchorDesc) > 0) {
          foreach ($anchorDesc as $kk => $aTag) {

            $href = $aTag[1];
            if (strpos($href, 'cms-context') !== false) {
              $ref = explode('ref:link', $aTag[1]);
              $refLink = (int) $ref[count($ref) - 1];
              $description = str_replace($aTag[1], $descAnchor[$kk], $description);
            }
          }
        }


        preg_match_all('/<img[^>]+>/i', $description, $result);
        $result = array_shift($result);



        //$description = $this->updateContainer($description);
        // Update img  src attribute.
        $descImg = $this->updateImageSrc($description, $images);
        $img = '';

        if (count($result) > 0) {
          foreach ($result as $kk => $img_tag) {

            if (isset($img)) {
              unset($img);
            }

            preg_match_all('/(src)=("[^"]*")/i', $img_tag, $img[$img_tag]);
            $imgTag = array_shift(array_keys($img));
            $description = str_replace($imgTag, $descImg[$kk], $description);
          }
        }
        //adds img wrapper
        $description = $this->updateImageTag(
          $description
        );
        //adds img-wrapper
        $description = $this->updateImageCaption(
          $description
        );
        $description = str_replace('<p></p>', '', $description);
        $description = str_replace('<p>&nbsp;</p>', '', $description);

        // $description = $wrappedTagBannerImg . $description;
        $publishedData = $item->attributes($namespaces['j']);
        $publishedData = (array) $publishedData;
        $publishedData = $publishedData['@attributes'];

        $createdData = $item->attributes($namespaces['jcr']);
        $createdData = (array) $createdData;
        $createdData = $createdData['@attributes'];

        // $publishedOn = $this->getTimestamp($publishedData, 'lastPubli shed');
        $createdOn = $this->getTimestamp($createdData, 'created');
        $modifiedOn = $this->getTimestamp($createdData, 'lastModified');

        if (!is_null($nodeData['title'])) {

          $entity_type = 'node';
          $field_tags = [];
          foreach ($tid as $termId) {
            $field_tags[] = [
              'target_id' => $termId,
            ];
          }

          $data = [
            'title' => $nodeData['title'],
            'body' => $description,
            'field_article_heading' => $subTitle,
            'field_machine_name' => $machineName,
            'uid' => 1,
            'created' => $createdOn,
            'changed' => $modifiedOn,
            'status' => ($publishedData['published']) == TRUE ? 1 : 0,
            'field_tags' => $field_tags,
            'metaTitle' => $metaData['metaTitle'],
            'metaDescription' => $nodeData['description'],
            'metaKeywords' => str_replace(
              '_x0020_',
              ' ',
              $itemAttributes['tagList']
            ),
          ];

          $operations[] = [
            '\Drupal\delta_blogs\BlogImportService::blogImport',
            [$data],
          ];

        }
        else {
          $this->logger
            ->get('delta_blogs')
            ->warning(json_encode($node, TRUE));
        }
      }

    }
    $batch = [
      'title' => 'Blog import...',
      'operations' => $operations,
      'finished' => '\Drupal\delta_blogs\BlogImportService::finishedCallback',
    ];
    // To run with batch support. uncomment.
    batch_set($batch);
    /*$this->logger
      ->get('delta_blogs created nodes')
      ->info(json_encode($savedData, TRUE));
    $this->messenger->addStatus('Blog import finished successfully');
    return TRUE;*/
  }

  /**
   * {@inheritdoc}
   */
  public function getTimestamp($data, $index) {

    $timestamp = explode('T', $data[$index]);
    $timestamp = $timestamp[0] . ' ' . $timestamp[1];
    return $timestamp = strtotime($timestamp);
  }

  /**
   * {@inheritdoc}
   */
  public function updateImageSrc($content, $images) {
    $doc = new \DOMDocument();
    $doc->loadHTML($content);
    $tags = $doc->getElementsByTagName('img');
    $data = [];
    if (count($tags) > 0) {
      foreach ($tags as $key => $tag) {
        $tag = $tags->item($key);
        $src = $tag->getAttribute('src');
        $imgRef = explode('ref:link', $src);
        $refLink = (int) $imgRef[count($imgRef) - 1];
        $imageRef = $this->imgPrefix . $refLink;
        $imageRef = $this->imgPrefix . $refLink;
        $imageSrc = $this->deltaImgPath . $images[$imageRef];
        $imageSrc = str_replace('_x0020_', ' ', $imageSrc);
        $tag->setAttribute('src', $imageSrc);
        $data[$key] = $doc->saveHTML($tag);
      }

    }
    return $data;
  }

  /**
   * {@inheritdoc}
   */
  public function updateHref($content, $anchor) {
    $doc = new \DOMDocument();
    $doc->loadHTML($content);
    $tags = $doc->getElementsByTagName('a');
    $data = [];
    if (count($tags) > 0) {
      foreach ($tags as $key => $tag) {
        $tag = $tags->item($key);
        $src = $tag->getAttribute('href');
        if (strpos($src, 'cms-context') !== false) {
          $ref = explode('ref:link', $src);
          $refLink = (int) $ref[count($ref) - 1];
          $href = $this->imgPrefix . $refLink;
          $href = str_replace('_x0020_', ' ', $href);
          $tag->setAttribute('href', $anchor[$href]);
          $data[$key] = $anchor[$href];
        }

      }
    }

    return $data;
  }

  /**
   * Implementation of taxonomyTermCreate.
   */
  public function taxonomyTermCreate($term_name, $vocabulary) {

    $entity_type = 'taxonomy_term';
    $term_name = explode(' ', $term_name);
    $term_id = [];
    foreach ($term_name as $t) {
      if ($t != '') {

        if (count($this->getTerm($t, $vocabulary)) > 0) {

          $term_id[] = $this->getTerm($t, $vocabulary);
        }
        else {
          $values = [
            'name' => $t,
            'vid' => $vocabulary,
          ];
          $this->entityTypeManager
            ->getStorage($entity_type)
            ->create($values)->save();
          $term_id[] = $this->getTerm($t, $vocabulary);
        }
      }
    }

    return $term_id;
  }

  /**
   * {@inheritdoc}
   */
  public function getTerm($t, $vocabulary) {
    $tid = '';
    $term = $this->entityTypeManager
      ->getStorage('taxonomy_term')
      ->loadByProperties(
          [
            'name' => $t,
            'vid'  => $vocabulary,
          ]
        );
    if (array_filter($term) > 0) {
      $tid = array_shift(array_keys($term));
    }
    return $tid;
  }

  /**
   * {@inheritdoc}
   */
  public function checkBlogExists($machineName) {
    $node = $this->entityTypeManager
      ->getStorage('node')
      ->loadByProperties(
        [
          'field_machine_name' => $machineName,
          'type'  => 'blog',
        ]
      );
    return $node;
  }

  /**
   * {@inheritdoc}
   */
  public function updateImageCaption($content) {
    $dom = new \DOMDocument();
    $dom->loadHTML($content);

    $new_container = $dom->createElement('figcaption');
    $new_container->setAttribute('class','figure__caption');
    $finder = new \DomXPath($dom);
    $classname = "fs-small";
    $nodes = $finder->query("//*[contains(@class, '$classname')]");
    if (count($nodes) > 0) {
      foreach ($nodes as $key => $tag) {
        $aWrapper = $new_container->cloneNode();
        $tag->parentNode->replaceChild($aWrapper, $tag);
        $aWrapper->appendChild($tag);
        $h = $dom->saveHTML();
      }
    }else{
      $h = $content;
    }

    return $h;
  }

  /**
   * {@inheritdoc}
   */
  public function updateImageTag($content) {
    $doc = new \DOMDocument();
    $doc->loadHTML($content);
    $new_container = $doc->createElement('picture');
    $new_container->setAttribute('class','figure__image');
    $aTag = $doc->createElement('a');
    $aTag->setAttribute('class','figure__link');
    $aTag->setAttribute('href','#');
    $tags = $doc->getElementsByTagName('img');
    if (count($tags) > 0) {
      foreach ($tags as $key => $tag) {

        $aWrapper = $aTag->cloneNode();
        $tag->parentNode->replaceChild($aWrapper, $tag);
        $aWrapper->appendChild($tag);

        //Clone our created container
        $new_container = $new_container->cloneNode();
        //Replace image with this wrapper
        $tag->parentNode->replaceChild($new_container, $tag);
        //Append this image to wrapper
        $new_container->appendChild($tag);
        $h = $doc->saveHTML();
      }
    }else{
      $h = $content;
    }

    return $h;
  }
  /**
   * {@inheritdoc}
   */
  public static function blogImport($data, $entity_type){

    $values = [
      'nid' => NULL,
      'type' => 'blog',
      'title' => $data['title'],
      'body' => $data['body'],
      'field_article_heading' => $data['field_article_heading'],
      'field_machine_name' => $data['field_machine_name'],
      'uid' => 1,
      'created' => $data['created'],
      'changed' => $data['changed'],
      'status' => $data['status'],
      'field_tags' => $data['field_tags'],
    ];
    $node = \Drupal::entityTypeManager()
      ->getStorage('node')
      ->create($values);
    $node->body->format = 'full_html';
    $node->set('field_meta_tags', serialize([
      'title' => $data['metaTitle'],
      'description' => $data['metaDescription'],
      'keywords' => $data['metaKeywords'],
    ]));

    $node->save();
    $str = "Created node " . $data['field_machine_name'] . ', view ' . $url;

    \Drupal::logger('delta_blogs')
      ->info($str);
  }

  /**
   * {@inheritdoc}
   */
  public static function finishedCallback($success, $results, $operations) {
    if ($success) {
      $message = t('Blog import successfully completed.');
    }
    else {
      $message = t('Finished with an error.');
    }
    \Drupal::messenger()->addStatus($message);
  }

}

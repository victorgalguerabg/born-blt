<?php

namespace Drupal\sync_product\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\sync_product\ProductImportService;

/**
 * Defines the Product sync form.
 */
class ProductSyncForm extends FormBase {

  /**
   * Drupal\sync_product\ProductImportController definition.
   *
   * @var \Drupal\sync_product\ProductImportInterface
   */
  protected $service;

  /**
   * Class constructor.
   */
  public function __construct(ProductImportService $service) {
    $this->service = $service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('sync_product.import')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'product_sync_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['sync_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Import Products'),
      '#required' => TRUE,
      '#options' => [
        'all' => 'All Products',
      ],
      '#default_value' => ['all'],
    ];

    /*$form['product_sku'] = [
    '#type' => 'textarea',
    '#title' => $this->t('Enter product sku.
    If multiple, enter comma seperated.'),
    '#states' => [
    'visible' => [
    ':input[name="sync_type"]' => ['value' => 'specific'],
    ],
    'required' => [
    ':input[name="sync_type"]' => ['value' => 'specific'],
    ],
    ],
    ];*/

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Import'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->service->createCommerceProduct();
  }

  /**
   * Implementation of getProductData.
   */
  public static function getProductData($accessor, $dataObj, $key) {

    if ($accessor->isReadable($dataObj, $key)) {
      return $accessor->getValue($dataObj, $key);
    }
    return FALSE;
  }

}

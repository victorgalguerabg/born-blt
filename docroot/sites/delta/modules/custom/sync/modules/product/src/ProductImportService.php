<?php

namespace Drupal\sync_product;

use Drupal\commerce_price\Price;
use Drupal\commerce_product\Entity\Product;
use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Component\Serialization\Json;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\delta_services\DeltaService;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class ProductImportService.
 */
class ProductImportService {

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;
  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;
  /**
   * Drupal\Core\Logger\LoggerChannelFactoryInterface definition.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactorXyInterface
   */
  protected $loggerFactory;

  /**
   * Drupal\Core\Path\AliasStorageInterface definition.
   *
   * @var \Drupal\path_alias\AliasManagerInterface
   */
  protected $pathAliasStorage;

  /**
   * Drupal\Core\Messenger\MessengerInterface definition.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;
  /**
   * Drupal\sync\DeltaService definition.
   *
   * @var \Drupal\sync\DeltaService
   */
  protected $deltaservice;

  /**
   * Constructs a new ProductImportService object.
   */
  public function __construct(ConfigFactoryInterface $config_factory,
                              EntityTypeManagerInterface $entity_type_manager,
                              LoggerChannelFactoryInterface $logger_factory,
                              MessengerInterface $messenger,
  DeltaService $deltaservice) {
    $this->configFactory = $config_factory;
    $this->entityTypeManager = $entity_type_manager;
    $this->loggerFactory = $logger_factory;
    $this->pathAliasStorage = $this->entityTypeManager->getStorage('path_alias');
    $this->messenger = $messenger;
    $this->deltaservice = $deltaservice;
  }

  /**
   * {@inheritdoc}
   */
  public static function productImportFinishedCallback($success, $results, $operations) {
    if ($success) {
      $message = t('Product sync successfully completed.');
    }
    else {
      $message = t('Finished with an error.');
    }
    /* @todo Use DI */
    // $this->messenger->addStatus($message);
    \Drupal::messenger()->addStatus($message);
  }

  /**
   * {@inheritdoc}
   */
  public function generateProductUrl() {
    $config = $this->configFactory->get('delta_services.deltaservicesconfig');
    $siteName = $config->get('site_name');

    $domainUrl = $config->get('domain_url');
    $syncProductURL = $domainUrl . $config->get('sync_api_url');

    if (!empty($siteName)) {
      $syncProductURL = $syncProductURL . '?categories=' . $siteName;
    }
    $filterWebExclusive = $config->get('filter_web_exclusive');
    if (!empty($filterWebExclusive)) {
      $syncProductURL = $syncProductURL . '&FltWebExclusiveCustomerItem=' . $filterWebExclusive;
    }

    $pagination = 10;
    if (!empty($config->get('pagination'))) {
      $pagination = $config->get('pagination');
      $syncProductURL = $syncProductURL . '&size=' . $pagination;
    }

    return $syncProductURL;

  }

  /**
   * Function to create commerce products.
   */
  public function createCommerceProduct($from = NULL, $to = NULL) {

    $sync_config = $this->configFactory->get('delta_services.deltaservicesconfig');
    $delta_config = $this->configFactory->get('sync.api_settings');
    $siteName = $delta_config->get('rest_api_settings.term_filter');

    $from = 1;
    $domain = $sync_config->get('api_base_path');
    $website_name = $delta_config->get('rest_api_settings.product_website');
    $url = $domain . $delta_config->get('rest_api_settings.product_fetch_category');
    $url .= '&' . $website_name . '=T';
    $composedUrl = $url . '&page=' . $from;

    $result = $this->deltaservice->apiCall($composedUrl, 'GET', [], [], 'PRODUCT_SYNC');
    $result_array = Json::decode($result['response']);
    $data = $result_array['content'];

    $product_count = 0;
    if (is_array($data)) {
      $product_count = count($data);
    }

    $operations = [];
    for ($i = 0; $i < $product_count; $i++) {
      $operations[] = [
        $this->importProducts(
          $product_count,
          $data[$i],
          $siteName
        ),
      ];
    }
    $batch = [
      'title' => 'Import products...',
      'operations' => $operations,
      'finished' => '\Drupal\sync_product\ProductImportService::productImportFinishedCallback',
    ];
    batch_set($batch);
  }

  /**
   * {@inheritdoc}
   */
  public function importProducts(&$context, $product_item, $siteName = NULL) {
    $this->createCommerceEntities($product_item, $siteName);
    usleep(100);
  }

  /**
   * {@inheritdoc}
   */
  public function createCommerceEntities($product_item = NULL, $siteName = NULL, $update = FALSE) {
    if ($product_item) {
      $this->createProduct($product_item, $siteName);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function createProduct($content, $siteName) {
    $categories = $content['categories'];
    $repairpart = !empty($content['values']['RepairPart']) ? $content['values']['RepairPart'] : 'F';
    $sitefromcategory = [];
    $basecategory = [];
    $maincategoryfinal = "";
    $maincategoryurl = "";
    $recertifiedurl = [];
    $recertifiedProduct = 0;
    foreach ($categories as $urlalias) {
      $urlalias_seperate = explode("_", $urlalias);
      $sitefromcategory[] = $urlalias_seperate[0];
      if ($urlalias_seperate[0] == "Delta") {
        $basecategory[] = strtolower($urlalias_seperate[1]);
      }
      $recertifiedurl[] = strtolower($urlalias_seperate[2]);
    }

    if(!empty($sitefromcategory) && in_array("Delta", $sitefromcategory)){
      $basecategoryfinal = array_unique($basecategory);
      $baserecertified = array_unique($recertifiedurl);
      if ($repairpart == "T" && in_array("parts", $basecategoryfinal)) {
        $maincategoryfinal = "parts";
        $maincategoryurl = "parts-product-detail?modelNumber=";
      }
      elseif (in_array('kitchen', $basecategoryfinal)) {
        $maincategoryfinal = "kitchen";
        $maincategoryurl = "kitchen/product/";
        if(in_array('recommerce', $basecategoryfinal)) {
          $recertifiedProduct = 1;
        }
      }
      elseif (in_array('bathroom', $basecategoryfinal)) {
        $maincategoryfinal =  "bathroom";
        $maincategoryurl = "bathroom/product/";
        if(in_array('recommerce', $basecategoryfinal)) {
          $recertifiedProduct = 1;
        }
      }
      elseif ($repairpart == "F" && in_array('parts', $basecategoryfinal)) {
        $maincategoryfinal = "parts";
        $maincategoryurl = "parts-product-detail?modelNumber=";
      }
      elseif(in_array('commercial', $basecategoryfinal)) {
        $maincategoryfinal = "commercial";
        $maincategoryurl = "parts-product-detail?modelNumber=";
      }
      elseif(in_array('recommerce', $basecategoryfinal) && in_array('kitchen', $basecategoryfinal)) {
        $maincategoryfinal = "kitchen";
        $maincategoryurl = "kitchen/product/";
        $recertifiedProduct = 1;
      }
      elseif(in_array('recommerce', $basecategoryfinal) && in_array('bathroom', $basecategoryfinal)) {
        $maincategoryfinal =  "bathroom";
        $maincategoryurl = "bathroom/product/";
        $recertifiedProduct = 1;
      }
      elseif(in_array('recommerce', $basecategoryfinal) && in_array('bathroom', $baserecertified)) {
        $maincategoryfinal =  "bathroom";
        $maincategoryurl = "bathroom/product/";
        $recertifiedProduct = 1;
      }
      elseif(in_array('recommerce', $basecategoryfinal) && in_array('kitchen', $baserecertified)) {
        $maincategoryfinal = "kitchen";
        $maincategoryurl = "kitchen/product/";
        $recertifiedProduct = 1;
      }
    }
    elseif(empty($categories) && $repairpart == "T"){
      $maincategoryfinal = "parts";
      $maincategoryurl = "parts-product-detail?modelNumber=";
    }
    elseif (empty($categories) && $repairpart == "F"){
      $maincategoryfinal = "kitchen";
      $maincategoryurl = "kitchen/product/";
    }
    else{
      throw new NotFoundHttpException();
    }

    $key_exists = $model = NULL;
    $value = NestedArray::getValue($content, ['values', 'ModelNumber'], $key_exists);
    if ($key_exists) {
      $model = $value;
    }
    $storeId = 1;
    $store = $this->entityTypeManager->getStorage('commerce_store')->loadDefault();
    $storeData = $store->get('store_id')->getValue();
    if(is_array($storeData) && !empty($storeData)){
      $storeId = $storeData[0]['value'];
    }
    $existProduct = $this->productExists($content['name']);
    if (empty(key($existProduct))) {
      $termId = '';
      if((is_array($content['values'])) && array_key_exists('Collection', $content['values'])){
        $collection = $content['values']['Collection'];
        $properties['name'] = $collection;
        $terms = $this->entityTypeManager
          ->getStorage('taxonomy_term')
          ->loadByProperties($properties);
        $termData = array_shift($terms);
        if(is_object($termData) && property_exists($termData,'tid') && property_exists($termData->tid,'value')){
          $termId = $termData->tid->value;
        }
      }
     // 'field_list_price' => $content['values']['ListPrice'],
      $productEntity = Product::create(
        [
          'title' => $content['description'],
          'uid' => '1',
          'type' => 'default',
          'variations' => [self::createCommerceProductVariation($content)],
          'stores' => [$storeId],
          'field_image_url' => self::getImageUrls($content),
          'field_brand' => $siteName,
          'field_collection' => $termId,
          'field_product_response_data' => Json::encode($content),
          'field_category' => $maincategoryfinal,
          'field_refurbished_product' => $recertifiedProduct
        ]
      );
      $productalias = "/".$maincategoryurl.$content['name'];
      $productEntity->save();
      $this->pathAliasStorage->create(['path'=>'/product/'.$productEntity->id(),'alias'=>$productalias,'langcode'=>'en'])->save();
      return $productEntity->id();
    } else {
      $message = 'Product exists - ' . $content['description'];
      $this->loggerFactory->get('sync_product')->info(
        '@message', [
          '@message' => $message,
        ]
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function productExists($modelNumber) {
    $variation = $this->entityTypeManager
      ->getStorage('commerce_product_variation')
      ->loadByProperties(['sku' => $modelNumber]);
    if(!empty($variation)) {
      $variationObj = reset($variation);
      $productId = $variationObj->getProductId();
      return [$productId => current($variation)];
    }
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function ProductListPrice($modelNumber) {
    $variation = $this->entityTypeManager
      ->getStorage('commerce_product_variation')
      ->loadByProperties(['sku' => $modelNumber]);
      if(!empty($variation)) {
        $variationDetails = "";
        foreach ($variation as $key => $value) {
          $variationDetails = $value->toArray();
        }
        if(!empty($variationDetails)) {
         return $variationDetails['list_price'][0];
        }
      }
      else {
        return [];
      }
  }

  /**
   * {@inheritdoc}
   */
  public static function getImageUrls($content = NULL) {
    $imageUrls = [];
    $imageUrls[]['value'] = $content['heroImage'];
    if (!empty($content['assets'])) {
      $product_assets_arr = $content['assets'];
      if (count($content['assets'])) {
        foreach ($product_assets_arr as $image_assets) {
           if ($image_assets['type'] == 'InContextShot'
           || $image_assets['type'] == 'Video' || $image_assets['type'] == 'SecondaryOnWhiteShots') {
          $imageUrls[]['value'] = $image_assets['url'];
           }
        }
      }
    }
    return $imageUrls;
  }

  /**
   * {@inheritdoc}
   */
  public function createCommerceProductVariation($content = NULL) {
    if (!is_null($content)) {
      $key_exists = $model = NULL;
      $model = $content['name'];
      $listPrice = NestedArray::getValue($content,
        [
          'values',
          'ListPrice',
        ],
        $key_exists
      );

      $weight = NestedArray::getValue($content,
        [
          'values',
          'ProductWeight_kg',
        ],
        $key_exists
      );

      $variation = ProductVariation::create(
        [
          'type' => 'default',
          'sku' => $model,
          'status' => 1,
          'price' => self::getPrice($listPrice),
        ]
      );
      $variation->set(
        'field_product_weight',
        $weight
      );
      $variation->save();
      return $variation;

    }
  }

  /**
   * {@inheritdoc}
   */
  public function getPrice($listPrice = NULL, $currencyCode = 'USD') {
    if (!is_null($listPrice)) {
      $price = new Price($listPrice, $currencyCode);
      return $price;
    }

  }
}

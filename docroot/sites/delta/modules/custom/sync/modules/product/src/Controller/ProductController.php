<?php

namespace Drupal\sync_product\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\sync_product\ProductImportService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Url;

/**
 * Class ProductController.
 */
class ProductController extends ControllerBase {

  /**
   * Drupal\sync_product\ProductImportController definition.
   *
   * @var \Drupal\sync_product\ProductImportInterface
   */
  protected $service;
  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Class constructor.
   */
  public function __construct(ProductImportService $service,
                              ConfigFactoryInterface $config_factory) {
    $this->service = $service;
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('sync_product.import'),
      $container->get('config.factory')
    );
  }

  /**
   * To get product details.
   */
  public function getProductDetail($sku) {

    $productExist = $this->service->productExists($sku);
    if (empty($productExist)) {
      $config = $this->configFactory->get('delta_services.deltaservicesconfig');
      $delta_config = $this->configFactory->get('sync.api_settings');
      $siteName = $delta_config->get('rest_api_settings.term_filter');
      $productContentApi = $delta_config->get('rest_api_settings.product_content_api');
      $basePath = $config->get('api_base_path');
      $composedUrl = $basePath . $productContentApi . $sku;

      $result = web_services_api_call($composedUrl, 'GET', [], [], 'PRODUCT_DETAIL');

      $content = Json::decode($result['response']);
      $productId = $this->service->createProduct($content, $siteName);
    }
    else {
      $productId = key($productExist);
    }

    $route_name = 'entity.commerce_product.canonical';
    $route_parameters = [
      'commerce_product' => $productId,
    ];

    $path = Url::fromRoute(
      $route_name,
      $route_parameters
    )->toString();
    $response = new RedirectResponse($path);
    return $response->send();
  }

}

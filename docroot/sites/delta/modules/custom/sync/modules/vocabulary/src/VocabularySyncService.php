<?php

namespace Drupal\sync_vocabulary;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\delta_services\DeltaService;
use Drupal\taxonomy\Entity\Term;
use Drupal\media\Entity\Media;
use Drupal\file\Entity\File;
use Drupal\taxonomy\TermStorage;
use Drupal\delta_cache\DeltaCacheService;
use Drupal\node\Entity\Node;


/**
 * Class VocabularySyncService.
 */
class VocabularySyncService {

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Drupal\Core\Logger\LoggerChannelFactoryInterface definition.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactorXyInterface
   */
  protected $loggerFactory;

  /**
   * Drupal\Core\Messenger\MessengerInterface definition.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Drupal\sync\DeltaService definition.
   *
   * @var \Drupal\sync\DeltaService
   */
  protected $deltaservice;

  /**
   * Drupal\delta_services\DeltaService definition.
   *
   * @var \Drupal\delta_cache\DeltaCache
   */
  protected $deltacache;

  /**
   * Constructs a new ProductImportService object.
   */
  public function __construct(ConfigFactoryInterface $config_factory,
                              EntityTypeManagerInterface $entity_type_manager,
                              LoggerChannelFactoryInterface $logger_factory,
                              MessengerInterface $messenger,
                              DeltaService $deltaservice,
                              DeltaCacheService $deltacache) {
    $this->configFactory = $config_factory;
    $this->entityTypeManager = $entity_type_manager;
    $this->loggerFactory = $logger_factory;
    $this->messenger = $messenger;
    $this->deltaservice = $deltaservice;
    $this->deltacache = $deltacache;
  }

  /**
   * {@inheritdoc}
   */
  public function getSelectedVocabulary($vid, $labelUpdate = FALSE) {
    if ($vid == "menu") {
      $vocabularyTree = \Drupal::entityTypeManager()
        ->getStorage('taxonomy_term')
        ->loadTree(
          $vid,
          0,
          NULL,
          TRUE
        );
      foreach ($vocabularyTree as $vocabularyDatum) {
        $termId = $vocabularyDatum->get('tid')->getValue()[0]['value'];
        $termData = self::faceMenuCacheBuild($termId,TRUE);
        if ($termData['check']) {
          $childMenu = $termData['childMenu'];
          foreach ($childMenu as $child) {
            if ($vid == "menu" && $labelUpdate == FALSE) {
              $facetResultObj = self::facetVocabularyCacheBuild($child['target_id'],TRUE);
            }
            elseif ($vid == "menu" && $labelUpdate == TRUE) {
              $term = Term::load($child['target_id']);
              $childvid = $term->get('field_facet_vocabulary')
                ->getValue()[0]['target_id'];
              $vocabularyTree = \Drupal::entityTypeManager()
                ->getStorage('taxonomy_term')
                ->loadTree(
                  $childvid,
                  0,
                  NULL,
                  TRUE
                );
              foreach ($vocabularyTree as $vocabularyDatum) {
                if (count($vocabularyDatum->get('field_facet_label')
                    ->getValue()) > 0) {
                  $facetLabel = $vocabularyDatum->get('field_facet_label')
                    ->getValue()[0]['value'];
                  $facetLabelTranslated = $this->deltaservice->translationsCode($facetLabel);
                  $vocabularyDatum->field_facet_label->setValue($facetLabelTranslated);
                  $vocabularyDatum->Save();
                }
              }
            }
          }
        }
      }
    }
    elseif ($vid == "syncprod") {
      $query = \Drupal::entityQuery('node')->condition('type', 'plp');
      $nids = $query->execute();
      $nodes = Node::loadMultiple($nids);
      foreach ($nodes as $n) {
        $n->save();
      }
    }
    $message = t('Cache Rebuild completed');
    \Drupal::messenger()->addStatus($message);
  }

  public function faceMenuCacheBuild($termid,$cacheRebuild = FALSE) {
    $checkCacheResult = 'facetmenu' . $termid;
    $cachedResult = $this->deltacache->deltaGetCache($checkCacheResult);
    if ($cachedResult != "" && $cacheRebuild == FALSE) {
      $response = $cachedResult;
      return $response;
    }
    else {
      $response = [];
      $response['check'] = FALSE;
      $menuTerm = Term::load($termid);
      if ($menuTerm != NULL) {
        $response['childMenu'] = $menuTerm->get('field_childmenu')->getValue();
        $response['menuName'] = $menuTerm->get('name')->getValue()[0]['value'];
        if ($menuTerm->hasField('field_collapsible_settings')) {
          $response['menuSetting'] = $menuTerm->field_collapsible_settings->value;
        }

        if ($menuTerm->hasField('field_max_filters')) {
          $response['menuMax'] = $menuTerm->field_max_filters->value;
        }

        if ($menuTerm->hasField('field_min_filters')) {
          $response['menuMin'] = $menuTerm->field_min_filters->value;
        }
        $response['check'] = TRUE;
      }
      $this->deltacache->deltaSetCache($checkCacheResult, $response);
      return $response;
    }
  }

  function _stringClean($string) {
    $string = str_replace(' ', '-', $string);
    $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string);

    return preg_replace('/-+/', '-', $string);
  }

  ////////////
  public function facetVocabularyCacheBuild($termid,$cacheRebuild = FALSE) {
    $response = [];
    $response['check'] = FALSE;
    $term = NULL;
    $checkCacheResult = 'facetterm' . $termid;
    $cachedResult = $this->deltacache->deltaGetCache($checkCacheResult);
    if ($cachedResult != "" && $cacheRebuild == FALSE) {
      $response = $cachedResult;
      return $response;
    }
    else {
      $term = Term::load($termid);
      $facetName = $term->get('name')->getValue()[0]['value'];
      $facetMachineName = $term->get('field_facet_value')
        ->getValue()[0]['value'];
      if ($term->hasField('field_facet_vocabulary')) {
        $response['check'] = TRUE;
        $vid = $term->get('field_facet_vocabulary')
          ->getValue()[0]['target_id'];

        $vocabularyTree = \Drupal::entityTypeManager()
          ->getStorage('taxonomy_term')
          ->loadTree(
            $vid,
            0,
            NULL,
            TRUE
          );
        $vocabulary = \Drupal::entityTypeManager()
          ->getStorage('taxonomy_vocabulary')
          ->load($vid);

        $facetData[$vid] = trim(
          strip_tags(
            $vocabulary->getDescription()
          )
        );
        foreach ($vocabularyTree as $vocabularyDatum) {
          $chilTid = $vocabularyDatum
            ->get('tid')
            ->getValue()[0]['value'];
          $facetTermName = $vocabularyDatum
            ->get('name')
            ->getValue()[0]['value'];
          $facetData[$facetTermName] = [];
          $facetLabel = $facetIcon = $facetHelp = '';

          if (count($vocabularyDatum
              ->get('field_facet_label')
              ->getValue()) > 0) {
            $facetLabel = $vocabularyDatum
              ->get('field_facet_label')
              ->getValue()[0]['value'];
          }
          else {
            $facetLabel = $facetTermName;
          }
          $iconUri = '';
          if ($vocabularyDatum->hasField('field_plp_filter_icon') && count($vocabularyDatum
              ->get('field_plp_filter_icon')->getValue()) > 0) {

            $icon = $vocabularyDatum
              ->get('field_plp_filter_icon')
              ->getValue()[0]['target_id'];

            if ($icon != NULL) {
              $iconData = file::load($icon);
              $facetIcon = file_create_url($iconData->getFileUri());
            }
          }

          if (count($vocabularyDatum
            ->get('field_facet_help_text')->getValue())) {
            $facetHelp = $vocabularyDatum
              ->get('field_facet_help_text')
              ->getValue()[0]['value'];
          }
          if ($vid == 'product_category') {
            $termDescription = strip_tags(
              $vocabularyDatum->get('field_category_path')->value
            );
          }
          else {
            $termDescription = strip_tags(
              $vocabularyDatum->get('description')->value
            );
          }
          $uniqueId = strtolower(
            str_ireplace(' ', '', $facetLabel)
          );
          $uniqueId = self::_stringClean($uniqueId);
          $termWeight = $vocabularyDatum->getWeight();
          $facetData[trim($termDescription)] = [
            'label' => $facetLabel,
            'icon' => $facetIcon,
            'help' => $facetHelp,
            'tid' => $uniqueId,
            'position' => $termWeight,
          ];
          if ($vid == 'facetstyle') {
            $facetData['facetstyle'] = [];
            $facetStyleData['name'] = $facetName;

            $facetStyleData['terms'][] = [
              'name' => $vocabularyDatum->getName(),
              'label' => $facetLabel,
              'icon' => $facetIcon,
              'help' => $facetHelp,
              'tid' => $uniqueId,
              'position' => $termWeight,
            ];
            $facetData['facetstyle'] = $facetStyleData;
          }
          if ($vid == 'facetnarrowresults') {
            $facetData['facetnarrowresults'] = [];
            $facetNarrowResultData['name'] = $facetName;
            $facetNarrowResultData['terms'][] = [
              'name' => $vocabularyDatum->getName(),
              'label' => $facetLabel,
              'icon' => $facetIcon,
              'help' => $facetHelp,
              'tid' => $uniqueId,
              'position' => $termWeight,
            ];
            $facetData['facetnarrowresults'] = $facetNarrowResultData;
          }
        }
        $response['facetName'] = $facetName;
        $response['facetMachineName'] = $facetMachineName;
        $response['facetData'] = $facetData;
        $this->deltacache->deltaSetCache($checkCacheResult, $response);
        return $response;
      }
      return $response;
    }
  }

}


<?php

namespace Drupal\sync_vocabulary\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\sync_vocabulary\VocabularySyncService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\taxonomy\Entity\Vocabulary;

/**
 * Class VocabularySyncForm.
 */
class VocabularySyncForm extends FormBase {

  protected $service;

  /**
   * Class constructor.
   */
  public function __construct(VocabularySyncService $service) {
    $this->service = $service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
      // Load the service required to construct this class.
        $container->get('sync_vocabulary.sync')
      );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'vocabulary_sync_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $options = [];
    $options = $this->get_vocabulary_names();
    $form['choose'] = [
      '#type' => 'select',
      '#title' => $this->t('Select Vocabulary'),
      '#required' => TRUE,
      '#options' => $options,
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#id' => 'import_category',
      '#value' => $this->t('Import'),
    ];
    return $form;
  }
  /**
   * {@inheritdoc}
   */

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $input_sel = $form_state->getUserInput();
    $input_selection = $input_sel['choose'];
    if ($input_selection == "menulabel"){
      $this->service->getSelectedVocabulary('menu',TRUE);
    } else {
      $this->service->getSelectedVocabulary($input_selection,FALSE);
    }
  }

  /**
   * Returns array of names of existing vocabularies.
   */
  function get_vocabulary_names(){
    $vocabularies = Vocabulary::loadMultiple();
    $vocabulariesList = [];
    foreach ($vocabularies as $vid => $vocablary) {
      if ($vid == "menu") {
        $vocabulariesList[$vid] = $vocablary->get('name');
      }
    }
    $vocabulariesList['syncprod'] = 'Sync Products';
    $vocabulariesList['menulabel'] = 'vocabularies Label fix';
    return $vocabulariesList;
    }
}

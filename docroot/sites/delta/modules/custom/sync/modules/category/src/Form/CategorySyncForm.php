<?php

namespace Drupal\sync_category\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\sync_category\CategoryImportService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class CategoryImportForm.
 */
class CategorySyncForm extends FormBase {
  /**
   * Drupal\sync_category\Controller\CategoryImportController definition.
   *
   * @var \Drupal\sync_category\CategoryImportInterface
   */
  protected $service;

  /**
   * Class constructor.
   */
  public function __construct(CategoryImportService $service) {
    $this->service = $service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
      // Load the service required to construct this class.
        $container->get('sync_category.import')
      );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'category_import_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $nid = '') {
    $form['#id'] = 'category-import-form';
    $form['choose'] = [
      '#type' => 'select',
      '#title' => $this->t('Import Category'),
      '#required' => TRUE,
      '#options' => [
        'insert' => 'Import Category',
        'delete' => 'Remove Category',
        'facetInsert' => 'Import Facets',
      ],
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#id' => 'import_category',
      '#value' => $this->t('Import'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $input_sel = $form_state->getUserInput();
    $input_selection = $input_sel['choose'];
    if ($input_selection == 'insert') {
      $this->service->createCommerceCategory();
    }
    elseif ($input_selection == 'delete') {
      $this->service->deleteCommerceCategory();
    }
    elseif ($input_selection == 'facetInsert') {
      $this->service->facetInsert();
    }
  }

}

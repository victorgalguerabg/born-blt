<?php

namespace Drupal\sync\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\ConfigFormBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Configure settings for this site.
 */
class SyncSettingsForm extends ConfigFormBase implements ContainerInjectionInterface {

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The Config storage.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configfactory;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configfactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
          $container->get('config.factory')
      );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'sync_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['sync.api_settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('sync.api_settings');

    $form['category_call_api'] = [
      '#type' => 'details',
      '#title' => $this->t('Category Related Calls'),
      '#open' => TRUE,
      '#weight' => 2,
    ];
    $form['category_call_api']['category_api'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Category Sync API'),
      '#default_value' => $config->get('rest_api_settings.category_api'),
      '#required' => TRUE,
      '#size' => 100,
    ];
    $form['category_call_api']['term_filter'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Name of the term to filter ex:Delta'),
      '#default_value' => empty($config->get('rest_api_settings.term_filter')) ? 'Delta' : $config->get('rest_api_settings.term_filter'),
      '#size' => 100,
    ];

    $form['category_call_api']['taxonomy_depth'] = [
      '#type' => 'select',
      '#title' => $this->t('Product category sync depth'),
      '#default_value' => $config->get('rest_api_settings.taxonomy_depth'),
      '#options' => ['1' => 1, '2' => 2, '3' => 3],
      '#default_value' => empty($config->get('rest_api_settings.taxonomy_depth')) ? 3 : $config->get('rest_api_settings.taxonomy_depth'),
      '#description' => $this->t('1:Parent Terms, 2: Child Terms, 3:Sibling Terms'),
    ];
    $form['facet_call_api'] = [
      '#type' => 'details',
      '#title' => $this->t('Facet Related Calls'),
      '#open' => TRUE,
      '#weight' => 2,
    ];
    $form['facet_call_api']['facet_api'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Facet Sync API'),
      '#default_value' => $config->get('rest_api_settings.facet_api'),
      '#required' => TRUE,
      '#size' => 100,
    ];
    $form['facet_call_api']['facet_size'] = [
      '#type' => 'number',
      '#title' => $this->t('Facet Size'),
      '#default_value' => $config->get('rest_api_settings.facet_size'),
      '#required' => TRUE,
      '#size' => 100,
    ];
    $form['product_call_api'] = [
      '#type' => 'details',
      '#title' => $this->t('Product Related Calls'),
      '#open' => TRUE,
      '#weight' => 3,
    ];
    $form['product_call_api']['product_website'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Name of the website'),
      '#default_value' => empty($config->get('rest_api_settings.product_website')) ? 'WebsiteUS' : $config->get('rest_api_settings.product_website'),
      '#size' => 100,
    ];
    $form['product_call_api']['product_fetch_category'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Product Fetch Category'),
      '#default_value' => $config->get('rest_api_settings.product_fetch_category'),
      '#required' => TRUE,
      '#size' => 200,
    ];

    $form['product_call_api']['product_by_sku'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Fetch Product By SKU'),
      '#default_value' => $config->get('rest_api_settings.product_by_sku'),
      '#required' => TRUE,
      '#size' => 200,
    ];
    $form['product_call_api']['product_collections_api'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Product Collection Path'),
      '#default_value' => $config->get('rest_api_settings.product_collections_api'),
      '#required' => TRUE,
      '#size' => 200,
    ];
    $form['product_call_api']['load_multiple_products'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Multiple Product Path'),
      '#default_value' => $config->get('rest_api_settings.load_multiple_products'),
      '#required' => TRUE,
    ];
    $form['product_call_api']['partlist_api'] = [
      '#type' => 'textarea',
      '#title' => $this->t('PDP Parts API'),
      '#default_value' => $config->get('partlist_api'),
      '#required' => TRUE,
    ];
    $form['product_call_api']['partlist_per_page'] = [
      '#type' => 'textfield',
      '#title' => $this->t('PDP Parts Per Page'),
      '#default_value' => $config->get('partlist_per_page'),
      '#required' => TRUE,
      '#size' => 5,
    ];
    $form['collection_api'] = [
      '#type' => 'details',
      '#title' => $this->t('Collection Related Calls'),
      '#open' => TRUE,
      '#weight' => 5,
    ];
    $form['collection_api']['collection_api'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Collection Sync API'),
      '#default_value' => $config->get('collection_api'),
      '#required' => TRUE,
      '#size' => 255,
    ];
    $form['collection_api']['collection_innovation_api'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Collection Innovation Feature API'),
      '#default_value' => $config->get('collection_innovation_api'),
      '#required' => TRUE,
      '#size' => 255,
    ];
    $form['collection_api']['collection_product_image'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Collection Product Image attribute'),
      '#default_value' => $config->get('collection_product_image'),
      '#required' => TRUE,
      '#size' => 255,
    ];

    $form['search_api'] = [
      '#type' => 'details',
      '#title' => $this->t('Search Related Calls'),
      '#open' => TRUE,
      '#weight' => 6,
    ];

    $form['search_api']['product_search'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Product Search API'),
      '#default_value' => $config->get('product_search'),
      '#required' => TRUE,
      '#size' => 255,
    ];

    $form['search_api']['product_per_page'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Product Per Page'),
      '#default_value' => $config->get('product_per_page'),
      '#required' => TRUE,
      '#size' => 5,
    ];

    $form['search_api']['document_search'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Document Search API'),
      '#default_value' => $config->get('document_search'),
      '#required' => TRUE,
      '#size' => 255,
    ];

    $form['search_api']['document_per_page'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Document Per Page'),
      '#default_value' => $config->get('document_per_page'),
      '#required' => TRUE,
      '#size' => 255,
    ];

    $form['search_api']['global_search_products'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Global Search Products'),
      '#default_value' => $config->get('global_search_products'),
      '#required' => TRUE,
      '#size' => 255,
    ];

    $form['search_api']['global_search_parts'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Global Search Parts'),
      '#default_value' => $config->get('global_search_parts'),
      '#required' => TRUE,
      '#size' => 255,
    ];
    $form['product_compare'] = [
      '#type' => 'details',
      '#title' => $this->t('Product Compare Related Calls'),
      '#open' => TRUE,
      '#weight' => 6,
    ];
    $form['product_compare']['compare'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Product Compare'),
      '#default_value' => $config->get('sync.compare'),
      '#required' => TRUE,
      '#size' => 200,
    ];
    $form['service_parts'] = [
      '#type' => 'details',
      '#title' => $this->t('Service and parts Product identification Related Calls'),
      '#open' => TRUE,
      '#weight' => 6,
    ];
    $form['service_parts']['guided_search'] = [
      '#type' => 'textarea',
      '#title' => $this->t('App name api'),
      '#default_value' => '/guidedsearch/deltafinder',
      '#required' => TRUE,
      '#size' => 255,
    ];
    $form['service_parts']['guided_search_troubleshoot'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Guided search troubleshoot steps api'),
      '#default_value' => $config->get('guided_search_troubleshoot'),
      '#required' => TRUE,
      '#size' => 255,
    ];
    $form['service_parts']['service_common_repair_parts'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Service and parts common repair parts finder block ID'),
      '#default_value' => $config->get('service_common_repair_parts'),
      '#size' => 64,
    ];
    $form['service_parts']['service_helpful_link'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Service and parts helpful link block ID'),
      '#default_value' => $config->get('service_helpful_link'),
      '#size' => 64,
    ];
    $form['service_parts']['service_contact_method'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Service and parts contact methods block ID'),
      '#default_value' => $config->get('service_contact_method'),
      '#size' => 64,
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('sync.api_settings')
      ->set('rest_api_settings.product_fetch_category', $form_state->getValue('product_fetch_category'))
      ->set('rest_api_settings.category_api', $form_state->getValue('category_api'))
      ->set('rest_api_settings.product_by_sku', $form_state->getValue('product_by_sku'))
      ->set('rest_api_settings.product_collections_api', $form_state->getValue('product_collections_api'))
      ->set('rest_api_settings.load_multiple_products', $form_state->getValue('load_multiple_products'))
      ->set('rest_api_settings.facet_api', $form_state->getValue('facet_api'))
      ->set('rest_api_settings.facet_size', $form_state->getValue('facet_size'))
      ->set('rest_api_settings.taxonomy_depth', $form_state->getValue('taxonomy_depth'))
      ->set('rest_api_settings.product_website', $form_state->getValue('product_website'))
      ->set('rest_api_settings.term_filter', $form_state->getValue('term_filter'))
      ->set('collection_api', $form_state->getValue('collection_api'))
      ->set('collection_innovation_api', $form_state->getValue('collection_innovation_api'))
      ->set('product_search', $form_state->getValue('product_search'))
      ->set('product_per_page', $form_state->getValue('product_per_page'))
      ->set('document_search', $form_state->getValue('document_search'))
      ->set('document_per_page', $form_state->getValue('document_per_page'))
      ->set('global_search_products', $form_state->getValue('global_search_products'))
      ->set('global_search_parts', $form_state->getValue('global_search_parts'))
      ->set('sync.compare', $form_state->getValue('compare'))
      ->set('guided_search', $form_state->getValue('guided_search'))
      ->set('guided_search_troubleshoot', $form_state->getValue('guided_search_troubleshoot'))
      ->set('service_helpful_link', $form_state->getValue('service_helpful_link'))
      ->set('service_common_repair_parts', $form_state->getValue('service_common_repair_parts'))
      ->set('service_contact_method', $form_state->getValue('service_contact_method'))
      ->set('collection_product_image', $form_state->getValue('collection_product_image'))
      ->set('partlist_api', $form_state->getValue('partlist_api'))
      ->set('partlist_per_page', $form_state->getValue('partlist_per_page'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}

setTimeout(function () {
  var findItOnlineString = '';
  jQuery(".ps-online-seller").each( function() {
    if(findItOnlineString.length > 0) findItOnlineString += ', ';
    findItOnlineString += jQuery('img', this).attr('title');
  });
  dataLayer.push({
    'findItOnline': findItOnlineString
  });
  dataLayer.push({'event': 'pageReady'});
}, 3600);

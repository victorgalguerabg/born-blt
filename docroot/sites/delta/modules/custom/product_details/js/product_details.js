(function ($, Drupal) {


  Drupal.behaviors.pdpaddtocart = {
    attach: function (context, settings) {
      var impressions = 0;
      var gtm_feature = '';
      var gtm_related = '';
      var gtm_fp_count = 0;
      var gtm_rp_count = 0;
      if(settings.product_details != undefined && settings.product_details != null){
        var pdp_detail = settings.product_details.gtm_detail;
      }
      var impressions_arr = [];

      var letterNumber = /[^a-zA-Z]/g;

      // IE 10 - replace string
      $(".numeric-stepper__input").on('keypress keyup blur', function() {
        if(!$(this).val().match(letterNumber)){
          $(this).val( $(this).val().replace(letterNumber,'') );
        }
      });

      // Qty filed validation
      $(".numeric-stepper__input").focusout(function() {
        maxLimit = parseInt($(this).attr('data-max'));
        qtyValue = $(this).val();
        // not exceed thee max value
        if ( parseInt($(this).val()) > maxLimit) {
          $(this).val(maxLimit);
        }
        else {
          $(this).val(qtyValue);
        }

        // Qty filed replace with min value if the field is empty
        if($(this).val() == '' ) {
          $(this).val(1);
        }

      });

      // After add to cart , change the buttons to disable and link to cart page.
     $(document).ajaxComplete(function(event, xhr, settings) {
       if(settings.url == "/featured-products" || settings.url == "/related-products") {
         impressions = impressions + 1;
         if(pdp_detail != undefined && impressions == 2) {
            if(gtm_feature != undefined && gtm_feature.length > 0){
              impressions_arr = impressions_arr.concat(gtm_feature);
            }
            if(gtm_related != undefined && gtm_related.length > 0){
              impressions_arr = impressions_arr.concat(gtm_related);
            }
            if (impressions_arr != undefined && impressions_arr.length > 0){
              var impressions_prods = JSON.stringify(impressions_arr);
            }
            else{
              var impressions_prods = '[]';
            }
            $('head').append('<script> dataLayer.push({\n' +
                  '            \'event\': "pageReady",\n' +
                  '            \'ecommerce\': {\n' +
                  '              \'details\': {\n' +
                  '                \'products\': '+pdp_detail+'\n' +
                  '              },\n' +
                  '              \'impressions\': '+impressions_prods+',\n' +
                  '            }\n' +
                  '          })</script>');

            dataLayer.push({
                'event': 'productDetail',
                'ecommerce': {
                    'details': {
                     'products': JSON.parse(pdp_detail)
                    },
                },
            });
           }
         }

         if ( document == context  && xhr.status == 200 && settings.extraData != undefined && settings.extraData._triggering_element_value !== 'undefined' && settings.extraData._triggering_element_value == 'Add to cart') {
             if($(".commerce-order-item-add-to-cart-form").length && settings.type == 'POST'){
                 //PDP addtocart button show and hide
                  var hideButton = 0;
                   var obj = jQuery.parseJSON( xhr.responseText );
                   if(obj.length > 0){
                     $.each(obj, function(key, val) {
                       if(val.command == "insert") {
                         $.each(val, function(keys, vals) {
                           if(keys == "settings") {
                             if(vals != null && vals != "") {
                               if(vals.hide !="" && vals.hide != undefined && vals.hide != null) {
                                 hideButton = vals.hide;
                               }
                             }
                           }
                         });
                       }
                     });
                   }

                   if(hideButton == 0) {
                     $(".pdp-btn-addcart.button--add-to-cart").remove();
                     $(".pdp-btn-addcart").html('<a class="button button--tertiary checkmark" href="#" disabled="disabled">Item Added to Cart</a><a class="button button--primary" href="/cart" >View cart and checkout</a>');
                     if(pdp_detail != undefined) {
                       var quantity = $('.field--name-quantity input').val();
                       var parsed_detail = JSON.parse(pdp_detail);
                       parsed_detail['0']['quantity'] = quantity;
                       var add = {
                         'products': parsed_detail
                       }
                       dataLayer.push({
                           'event': 'addToCart',
                           'ecommerce': {
                               'add': add
                           },
                       });
                     }
                   }
                }
            }

          });
      $('#where_to_buy').once().click(
        function () {
          $(this).hide();
          $('#local_dealer').show();
          $('#shop_online').show();
        });

      // for ajax appending product parts
      var current_path = window.location.pathname;
      var loaded = false;
      var first = null;
      var last = null;
      var total = null;
      var action = null;
      function getPartList(pageNumber, first, last, total, action){
        var pageNum = parseInt(pageNumber);
        var first = parseInt(first);
        var last = parseInt(last);
        var total = parseInt(total);
        if(!action && pageNum > 0){
          pageNum = pageNum -1;
        }
        else if (action == "prev" && pageNum > 0) {
          pageNum = pageNum - 2;
        }
        $.ajax({
          type: "POST",
          url: "/get-parts",
          data: {
            path: current_path,
            page: pageNum
          },
          beforeSend: function(){ $('.pdp-list-desc').html('<div class="ajax-progress ajax-progress-throbber"><div class="throbber">&nbsp;</div></div>'); },
          success: function(result) {
            var _currentpage = parseInt(result.pager.current_page);
            var _totalpages = parseInt(result.pager.total);
            var _first;
            // GTM
            if(result.gtm_parts != undefined && result.gtm_parts != '') {
              var gtm_parts = JSON.parse(result.gtm_parts);
              dataLayer.push({
                  'event': 'productImpression',
                  'ecommerce': {
                      'impressions': gtm_parts
                  },
              });
            }
            if (_currentpage == 0) {
              _first = 1;
            } else {
              //_first = initData.first;
            }
            if(_currentpage>=first && _currentpage<=last) {
              _first = parseInt(first);
            }
            if(action && action == 'next') {
              if((_currentpage+1)>=first && _currentpage<last) {
                _first = parseInt(first);
              } else {
                _first = parseInt(first) + 1;
              }
            }
            if(action && action == 'prev') {
              if((_currentpage+1) >= first && _currentpage<=last) {
                _first = parseInt(first);
              } else {
                _first = parseInt(first) - 1;
              }
            }
            $('.ajax-progress').remove();
            var _render = "";
            var pagination = parseInt(_first)+4;
            if(_totalpages < pagination) {
              pagination = _totalpages;
            }

            if(_currentpage > 0) {
              _render = '<input first="'+_first+'" last="'+pagination+'" total="'+ _totalpages +'" class="pager__link prev" type="button" data="" value="<">';
            }
            for(var i=_first;i<=pagination;i++) {
              var classValue = '';
              var dis = '';
              var chk = parseInt(_currentpage) + 1
              if(i === chk) {
                classValue = 'is-active';
                dis = 'disabled';
              }
              _render += '<input '+ dis +' first="'+_first+'" last="'+pagination+'" total="'+ _totalpages +'" class="pager__link '+ classValue +'" type="button" value="'+ i +'">';

            }
            if(_currentpage != _totalpages -1) {
              _render += '<input first="'+_first+'" last="'+pagination+'" total="'+ _totalpages +'" class="pager__link next" type="button" data="" value=">">';
            }
            if(_totalpages > 1){
              $('.pager__items').html(_render);
            }
            result ? $('.pdp-list-desc').html(result.content) : $('.pdp-list-desc').remove();

            // GTM push product click
            $('.pdp-list-desc div.grid div.product-tile a.product-action-link, .pdp-list-desc div.grid div.product-tile div.product-tile__body a.product-action-link',context).not(".js-add-to-compare").on("click",function(event) {
              event.stopPropagation();
              event.preventDefault();
              var url = $(this).attr('href');
              var sku = $(this).attr('data');
              var psclick;
              // GTM push ProductClick
              if(gtm_parts != undefined && gtm_parts.length > 0){
                gtm_parts.forEach(function(gtm_ps){
                  if(gtm_ps.id == sku){
                    psclick = gtm_ps;
                    dataLayer.push({
                      'event':'productClick',
                      'ecommerce': {
                        'click': {
                          'actionField': {'list': 'parts list'},
                          'products': [psclick]
                        }
                      },
                      'eventCallback': function() {
                              document.location = url;
                      }
                    });
                    return;
                  }
                });
              }
              document.location = url;
            });
          },
          error: function() {
            $('.pdp-list-desc').remove();
          }
        });
      }
      $('.pdp-tabs').on('click', '.pager__link', function(e) {
        var first = $(this).attr('first');
        var last = $(this).attr('last');
        var total = $(this).attr('total');
        var pageNumber = $(this).val();
        var action = null;
        if($(e.currentTarget).val() === '>') {
          pageNumber = $('.pager__link.is-active').val();
          action = 'next';
        }
        if($(e.currentTarget).val() === '<') {
          pageNumber = $('.pager__link.is-active').val();
          action = 'prev';
        }
        getPartList(pageNumber,first,last,total,action);
      });

      $('a[name ="partListTab"], div[name ="partListTab"]').once("click").click(function(){
        //if(loaded) return;
        //loaded = true;
        getPartList(0);
      });
      // Once when document ready related products load
      if (context == document) {

        if ($('.pdp-rp').length) {
          $.ajax({
            type: "POST",
            url: "/related-products",
            data: {path: current_path},
            success: function(result_data) {
              result_data != false ? $('.pdp-rp').html(result_data.html) : $('.pdp-rp').remove();
              if(result_data != false && result_data.gtm_related != undefined){
                gtm_related = JSON.parse(result_data.gtm_related);
              }else{
                gtm_related = '';
              }
              $('.pdp-rp div.grid div.product-tile a.product-action-link, .pdp-rp div.grid div.product-tile div.product-tile__body a.product-action-link').on("click",function(event) {
              event.stopPropagation();
              event.preventDefault();
              var url = $(this).attr('href');
              var sku = $(this).attr('data');
              var rdclick;
              // GTM push product click
              if(gtm_related != undefined && gtm_related.length > 0){
                gtm_related.forEach(function(gtm_rd){
                  if(gtm_rd.id == sku){
                    rdclick = gtm_rd;
                    dataLayer.push({
                      'event':'productClick',
                      'ecommerce': {
                        'click': {
                          'actionField': {'list': 'related products'},
                          'products': [rdclick]
                        }
                      },
                      'eventCallback': function() {
                         document.location = url;
                      }
                    });
                    return;
                  }
                });
              }

              document.location = url;
            });
            },
            error: function() {
              $('.pdp-rp').remove();
            }
          });
        }
        if ($('.pdp-fp').length) {
          $.ajax({
            type: "POST",
            url: "/featured-products",
            data: {path: current_path},
            success: function(result_data) {
              result_data != false ? $('.pdp-fp').html(result_data.html) : $('.pdp-fp').remove();
              gtm_feature = result_data != false ? JSON.parse(result_data.gtm_feature): '';
              $('.pdp-fp div.grid div.product-tile a.product-action-link, .pdp-fp div.grid div.product-tile div.product-tile__body a.product-action-link').on("click",function(event) {
                event.stopPropagation();
                event.preventDefault();
                var url = $(this).attr('href');
                var sku = $(this).attr('data');
                var ftclick;
                // GTM push ProductClick
                if(gtm_feature != undefined && gtm_feature.length > 0){
                  gtm_feature.forEach(function(gtm_ft){
                    if(gtm_ft.id == sku){
                      ftclick = gtm_ft;
                      dataLayer.push({
                        'event':'productClick',
                        'ecommerce': {
                          'click': {
                            'actionField': {'list': 'featured accessoriess'},
                            'products': [ftclick]
                          }
                        },
                        'eventCallback': function() {
                          document.location = url;
                        }
                      });
                      return;
                    }
                  });
                }

                document.location = url;
              });
            },
            error: function() {
              $('.pdp-fp').remove();
            }
          });
        }

      }

      // diable basket icon imageFilename
      $(".button--add-to-cart").click(function(){
        var cartLink = $('.cart-icon').attr("href");
        $('.cart-icon').attr('data-href', cartLink);
        $('.cart-icon').off("click").attr('href', "javascript: void(0);");
      });

        $(document).ready(function() {
            $(document).scroll(function() {
                if($('.pdp-fp').length) {
                    var hT = $('.pdp-fp').offset().top,
                        hH = $('.pdp-fp').outerHeight(),
                        wH = $(window).height(),
                        wS = $(this).scrollTop();
                    if(gtm_fp_count == 0) {
                        if (wS > (50 + hT + hH - wH)) {
                            console.log((hH+hT-wH) , wS);
                            if(gtm_feature != "" && gtm_feature != undefined) {
                                dataLayer.push({
                                    'event': 'productImpression',
                                    'ecommerce': {
                                        'impressions': gtm_feature
                                    },
                                });
                            }
                            gtm_fp_count = gtm_fp_count + 1;
                        }
                    }
                }
                if($('.pdp-rp').length) {
                    var hTrp = $('.pdp-rp').offset().top,
                        hHrp = $('.pdp-rp').outerHeight(),
                        wHrp = $(window).height(),
                        wSrp = $(this).scrollTop();
                    if(gtm_rp_count == 0) {
                        if (wSrp > (50 + hTrp + hHrp - wHrp)) {
                            console.log((hH+hT-wH) , wS);
                            if(gtm_related != "" && gtm_related != undefined) {
                                dataLayer.push({
                                    'event': 'productImpression',
                                    'ecommerce': {
                                        'impressions': gtm_related
                                    },
                                });
                            }
                            gtm_rp_count = gtm_rp_count + 1;
                        }
                    }
                }
            });
        });
    }
  };
})(jQuery, Drupal);

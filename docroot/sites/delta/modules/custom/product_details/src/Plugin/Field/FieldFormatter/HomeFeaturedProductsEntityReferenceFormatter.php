<?php

namespace Drupal\product_details\Plugin\Field\FieldFormatter;

use Drupal\product_details\Controller\ProductController;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceFormatterBase;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Plugin implementation of the 'entity reference rendered entity' formatter.
 *
 * @FieldFormatter(
 *   id = "entity_reference_home_featured_products_widget",
 *   label = @Translation("Home Page - Featured Products Widget"),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class HomeFeaturedProductsEntityReferenceFormatter extends EntityReferenceFormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $refProducts = $items->referencedEntities();
    foreach ($refProducts as $refProduct) {
      $featuredProductsSkus[] = $refProduct->get('field_product_sku')->value;
    }
    if (count($featuredProductsSkus) > 0) {
      $featuredProducts['productReferences'] = ProductController::getMultipleProducts($featuredProductsSkus);
    }
    return [
      '#theme' => 'home_feature_products',
      '#data' => $featuredProducts,
    ];
  }

}

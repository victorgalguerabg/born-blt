<?php

namespace Drupal\product_details\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\product_details\ProductService;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\delta_cache\DeltaCacheService;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\delta_services\DeltaService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\sync_product\ProductImportService;
use Drupal\commerce_product\ProductLazyBuilders;

/**
 * Controller routines for user routes.
 */
class ProductController extends ControllerBase {

  /**
   * Drupal\sync_product\ProductImportController definition.
   *
   * @var \Drupal\sync_product\ProductImportInterface
   */
  protected $service;
  /**
   * Drupal\delta_services\DeltaService definition.
   *
   * @var \Drupal\delta_services\DeltaService
   */
  protected $deltaservice;
  /**
   * Drupal\product_details\ProductService definition.
   *
   * @var \Drupal\product_details\ProductService
   */
  protected $productservice;

  /**
   * To get current query string.
   *
   * @var Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;
  /**
   * Drupal\Core\Messenger\MessengerInterface definition.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;
  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $currentRouteMatch;

  /**
   * The current route match.
   *
   * @var \Drupal\delta_cache\DeltaCacheService
   */
  protected $deltaCache;

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Drupal\Core\TempStore\PrivateTempStoreFactory definition.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStoreFactory
   */
  protected $tempStoreFactory;

  /**
   * Drupal\user\PrivateTempStoreFactory definition.
   *
   * @var \Drupal\sync_product\ProductImportservice
   */
  protected $productimportService;

  public static $page_title;

  /**
   * Drupal\commerce_product\ProductLazyBuilders definition.
   *
   */
  protected $productLazyBuilders;

  /**
   * Class constructor.
   */
  public function __construct(ProductService $productservice,
                              RequestStack $requestStack,
                              MessengerInterface $messenger,
                              EntityTypeManagerInterface $entity_type_manager,
                              RouteMatchInterface $current_route_match,
                              DeltaCacheService $deltaCache,
                              DeltaService $deltaservice,
                              ConfigFactoryInterface $configFactory,
                              PrivateTempStoreFactory $tempStoreFactory,
                              ProductImportService $productImportService,
                              ProductLazyBuilders $productLazyBuilders
                              ) {
    $this->productservice = $productservice;
    $this->requestStack = $requestStack;
    $this->messenger = $messenger;
    $this->entityTypeManager = $entity_type_manager;
    $this->currentRouteMatch = $current_route_match;
    $this->deltaCache = $deltaCache;
    $this->deltaservice = $deltaservice;
    $this->configFactory = $configFactory;
    $this->tempStoreFactory = $tempStoreFactory;
    $this->tempStore = $this->tempStoreFactory->get('product_details');
    $this->productimportService = $productImportService;
    $this->productLazyBuilders = $productLazyBuilders;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('product_details.service'),
      $container->get('request_stack'),
      $container->get('messenger'),
      $container->get('entity_type.manager'),
      $container->get('current_route_match'),
      $container->get("delta_cache.cache_service"),
      $container->get("delta_services.service"),
      $container->get("config.factory"),
      $container->get('tempstore.private'),
      $container->get('sync_product.import'),
      $container->get('commerce_product.lazy_builders')
    );
  }

  /**
   * Get Product Sku Details.
   */
  public function getProductModelNumber($sku) {
    $response = $this->productservice->getProductDetail($sku);
    return $response;
  }

  /**
   * Fetch the where to buy page.
   */
  public function whereToBuy() {
    $routeName = $this->currentRouteMatch->getRouteName();
    $requestUri = $this->requestStack->getCurrentRequest()->getRequestUri();
    $requestArray = explode('/',$requestUri);
    $sku = $requestArray[2];
    $dataOutput = [];
      if ($sku) {
        // Cache pdp information.
        $data = $this->deltaCache->deltaGetCache('PDP_' . $sku);
        if (!empty($data->data)) {
          $dataOutput = $data->data;
        }
        else {
          $products = $this->productimportService->productExists($sku);
          $productId = key($products);
          if (!empty($productId)) {
            $productDetails = $this->productservice->productDetailsPage($sku);
            $prop_config = $this->configFactory->get('product_details.settings');
            $locationUrl = $prop_config->get('find-location-url');
            $locationDisclaimer = $prop_config->get('find-location-disclaimer');
            $exclusives = [
              "Ferguson"=>"F",
              "Lowe's" =>"L",
              "Home Depot"=>"H",
              "Menards"=>"M",
              "Delta Trade"=>"T",
              "Costco"=>"C"
            ];
            $showLocation = true;
            if(isset($productDetails['output']['#data']['values']->FltWebExclusiveCustomerItem) && $productDetails['output']['#data']['values']->FltWebExclusiveCustomerItem == 'T'){
              $exclusive_array = explode('_',$productDetails['output']['#data']['values']->WebExclusiveCustomerItem);
              $exclusive = [];
              foreach($exclusive_array as $key => $exclusive_key) {
                $exclusive[] = $exclusives[$exclusive_key];
              }
              $exclusive_string = implode(',', $exclusive);
              $locationUrl = $locationUrl."?exclusive=".$exclusive_string;
              if ($productDetails['output']['#data']['values']->WebExclusiveCustomerItem == 'Online Retailers' || $productDetails['output']['#data']['values']->ExclusiveCustomerItem == 'Online Retailers' ) {
                $showLocation = false;
              }
            }
            if (!empty($productDetails) && (!empty($productDetails['output']['#data']))) {
              $dataOutput = $productDetails['output']['#data'];
              $dataOutput['locationUrl'] = $locationUrl;
              $dataOutput['locationDisclaimer'] = $locationDisclaimer;
              $dataOutput['showLocation'] = $showLocation;
            }
          }
          else {
            throw new NotFoundHttpException();
          }
        }
      }

    switch ($routeName) {
      case 'product_details.where2buy':
        $build['output'] = [];
        break;

      case 'product_details.where2buy_online':
        $build['output'] = [
          '#theme' => 'where_to_buy_online',
          '#sku' => $sku,
          '#data' => $dataOutput,
        ];
        break;
    }
    return $build;
  }

 /**
   * Fetch the Owner Quicklinks page.
   */
  public function ownerQuickLinks($sku) {
    $build = [];
    if ($sku) {
      $dataOutput = "";
      // Cache pdp information.
      $data = $this->deltaCache->deltaGetCache('PDP_' . $sku);
      if (!empty($data->data)) {
        $dataOutput = $data->data;
      }
      else {
        $products = $this->productimportService->productExists($sku);
        $productId = key($products);
        if (!empty($productId)) {
          $productDetails = $this->productservice
            ->productDetailsPage($sku);
          if (!empty($productDetails) && (!empty($productDetails['output']['#data']))) {
            $dataOutput = $productDetails['output']['#data'];
          }
        }
        else {
          $this->productservice->getProductDetail($sku);
        }
      }
      $build['output'] = [
        '#theme' => 'owner_quicklinks',
        '#sku' => $sku,
        '#data' => $dataOutput,
      ];
    }
    return $build;
  }
  /**
   * Implementation of compare products.
   */
  public function compareProducts() {
    $sku = $this->requestStack->getCurrentRequest()->get('modelnumber');

    $compareProductsRaw = $this->tempStore->get('product_compare');

    if(!empty($sku)) {
      if (!empty($compareProductsRaw)) {
        $compareProducts = explode("||", $compareProductsRaw);
        if (count($compareProducts) <= 2) {
          if (@!in_array($sku, $compareProducts)) {
            $compareProducts[] = $sku;
          }
        }
        $newSkus = implode("||", $compareProducts);
        $this->tempStore->set('product_compare', $newSkus);
      }
      else {
        $this->tempStore->set('product_compare', $sku);
      }
    }
    return new JsonResponse($this->tempStore->get('product_compare'));
  }

  /**
   * Implementation of remove compare products.
   */
  public function removeCompareProduct() {
    $sku = $this->requestStack->getCurrentRequest()->get('modelnumber');

    $compareProductsRaw = $this->tempStore->get('product_compare');
    if(!empty($sku)) {
      if (!empty($compareProductsRaw)) {
        $compareProducts = explode("||", $compareProductsRaw);
        foreach($compareProducts as $key => $compareProduct) {
          if($compareProduct == $sku) {
            unset($compareProducts[$key]);
          }
        }
        $compareProductsNew = implode("||", $compareProducts);
        $this->tempStore->set('product_compare', $compareProductsNew);
      }
    }
    return new JsonResponse($this->tempStore->get('product_compare'));
  }

  public function comparePopup(){
    $compareProductsRaw = $this->tempStore->get('product_compare');

    $config = $this->configFactory->get('delta_services.deltaservicesconfig');
    $deltaConfig = $this->configFactory->get('sync.api_settings');
    $domain = $config->get('api_base_path');
    $compareUrl = $domain . $deltaConfig->get('sync.compare');
    $prop_config = $this->configFactory->get('product_details.settings');
    $discontinue_text = $prop_config->get('rest_api_settings.discontinue_ribbontext');
    $coming_soon_text = $prop_config->get('rest_api_settings.comming_soon_ribbontext');
    $urlParts = explode("||", $compareProductsRaw);
    $compareSku = '';
    foreach($urlParts as $parts){
      $compareSku .= "modelNumber=" . $parts. '&';
    }
    $url = str_replace("{modelNumber}", $compareSku, $compareUrl);
    $numCompResp = 1;
    $request = $this->deltaservice->apiCall($url, "GET", [], "", "ProductCompare");
    $productDetails = [];
    $compareLabValArr = [];

    if ($request['status'] == 'ERROR') {
      $this->messenger()->addMessage("product compare" , 'error');
      return [];
    }
    if ($request['status'] == 'SUCCESS' && $request['response'] == 'null') {
      $this->messenger()->addMessage("product compare" , 'error');
      return [];
    }
    if ($request['status'] == 'SUCCESS' && $request['response'] != 'null') {
      $readJsonArr = json_decode($request['response'], TRUE);
      if (count($readJsonArr)) {
        foreach ($readJsonArr['products'] as $productAttributes) {
          $ribbonText = "";
          $ribboncolor = "";
          $collection = ($productAttributes['values']['Collection'])?$productAttributes['values']['Collection']:$productAttributes['values']['defaultCollection'];
          // Product Coming Soon and Discontinued logic.
          $materialstaus = isset($productAttributes['values']['MaterialStatus']) ? $productAttributes['values']['MaterialStatus'] : "";
          $discontinued = ($materialstaus >= 50) ? TRUE : FALSE;
          $commingSoon = FALSE;
          $ribbonVal = $productAttributes['values']['WebExclusiveCustomerItem'];
          $availableToOrderDate = isset($productAttributes['values']['AvailableToOrderDate']) ? $productAttributes['values']['AvailableToOrderDate'] : '';
          $availableToShipDate = isset($productAttributes['values']['AvailableToShipDate']) ? $productAttributes['values']['AvailableToShipDate'] : '';
          $orderDate = strtotime($availableToOrderDate);
          $shipDate = strtotime($availableToShipDate);
          $currentDate = strtotime(date('Y-m-d', time()));
          if (empty($orderDate) || empty($shipDate)) {
            $shipDate = $currentDate - (60 * 60 * 24);
          }
          if ($discontinued) {
            $ribbonText = $discontinue_text;
            $ribboncolor = "gray-30";
          }
          else {
            if ($currentDate < $shipDate) {
              if ($currentDate > $orderDate) {
                $commingSoon = TRUE;
                $ribbonText = $coming_soon_text;
                $ribboncolor = "delta-red";
              }
            }else{
              $ribbonText = "";
              $ribboncolor = "";
            }
          }

          $sitefromcategory = [];
          $basecategory = [];
          if(!empty($productAttributes['categories'])) {
            foreach ($productAttributes['categories'] as $urlalias) {
              $urlalias_seperate = explode("_", $urlalias);
              $sitefromcategory[] = $urlalias_seperate[0];
              if ($urlalias_seperate[0] == "Delta") {
                $basecategory[] = strtolower($urlalias_seperate[1]);
              }
            }
          }
          if(!empty($sitefromcategory)) {
            $basecategoryfinal = array_unique($basecategory);
            if(in_array('recommerce', $basecategoryfinal)) {
              $ribbonText = "Recertified";
              $ribboncolor = "gray-30";
            }
          }

          if(!in_array('recommerce', $basecategoryfinal)) {
            if (!empty($ribbonVal)) {
              if ($ribbonVal != 'NA' &&
                $ribbonVal != 'OEM' &&
                $ribbonVal != 'Retail' &&
                $ribbonVal != 'Warranty' &&
                $ribbonVal != 'Internal Use Only' &&
                $discontinued != TRUE &&
                $commingSoon != TRUE
              ) {
                $ribbonText = "Only at " . str_replace("_", " and ", $ribbonVal);
                $ribboncolor = "gray-30";
              }
            }
          }
          $productDetails[] = [
            'sku' => $productAttributes['defaultModelNumber'],
            'image' => $productAttributes['heroImageSmall'],
            'name' => $productAttributes['name'],
            'description' => $productAttributes['description'],
            'collection' => $collection,
            'price' => $productAttributes['values']['ListPrice'],
            'ribbontext' => $ribbonText,
            'ribboncolor' => $ribboncolor,
          ];
        }
      }
    }
    $output = [
      '#theme' => 'product_compare_popup',
      '#data' => $productDetails
    ];

    return $output;
  }
  public function docompare(Request $request){
    $page_path = $request->getQueryString();
    $is_ajax = $request->isXmlHttpRequest();
    // Clear the cookies
    //$this->tempStore->set('product_compare', '');
    // Clear the cookies
    $config = $this->configFactory->get('delta_services.deltaservicesconfig');
    $deltaConfig = $this->configFactory->get('sync.api_settings');
    $domain = $config->get('api_base_path');
    $compareUrl = $domain . $deltaConfig->get('sync.compare');
    $prop_config = $this->configFactory->get('product_details.settings');
    $discontinue_text = $prop_config->get('rest_api_settings.discontinue_ribbontext');
    $coming_soon_text = $prop_config->get('rest_api_settings.comming_soon_ribbontext');
    $compareProductsRaw = $this->tempStore->get('product_compare');
    if($is_ajax == 1){

      $urlParts = explode("||", $compareProductsRaw);

      $compareSku = '';
      foreach($urlParts as $parts){
        $compareSku .= "modelNumber=" . $parts. '&';
      }
      $url = str_replace("{modelNumber}", $compareSku, $compareUrl);
    }else{
      $url = str_replace("{modelNumber}", $page_path.'&', $compareUrl);
    }
    $numCompResp = 1;
    $request = $this->deltaservice->apiCall($url, "GET", [], "", "ProductCompare");
    $productDetails = [];
    $compareLabValArr = [];
    if ($request['status'] == 'ERROR') {
      $this->messenger()->addMessage("product compare" , 'error');
      return [];
    }
    if ($request['status'] == 'SUCCESS' && $request['response'] == 'null') {
      $this->messenger()->addMessage("product compare" , 'error');
      return [];
    }
    if ($request['status'] == 'SUCCESS' && $request['response'] != 'null') {
      $readJsonArr = json_decode($request['response'], TRUE);
      if (count($readJsonArr)) {
        foreach ($readJsonArr['products'] as $productAttributes) {
          // Product Coming Soon and Discontinued logic.
          $materialstaus = isset($productAttributes['values']['MaterialStatus']) ? $productAttributes['values']['MaterialStatus'] : "";
          $discontinued = ($materialstaus >= 50) ? TRUE : FALSE;
          $commingSoon = FALSE;
          $ribbonVal = $productAttributes['values']['WebExclusiveCustomerItem'];
          $availableToOrderDate = isset($productAttributes['values']['AvailableToOrderDate']) ? $productAttributes['values']['AvailableToOrderDate'] : '';
          $availableToShipDate = isset($productAttributes['values']['AvailableToShipDate']) ? $productAttributes['values']['AvailableToShipDate'] : '';
          $orderDate = strtotime($availableToOrderDate);
          $shipDate = strtotime($availableToShipDate);
          $currentDate = strtotime(date('Y-m-d', time()));
          if (empty($orderDate) || empty($shipDate)) {
            $shipDate = $currentDate - (60 * 60 * 24);
          }
          if ($discontinued) {
            $ribbonText = $discontinue_text;
            $ribboncolor = "gray-30";
          }
          else {
            if ($currentDate < $shipDate) {
              if ($currentDate > $orderDate) {
                $commingSoon = TRUE;
                $ribbonText = $coming_soon_text;
                $ribboncolor = "delta-red";
              }
            }else{
              $ribbonText = "";
              $ribboncolor = "";
            }
          }

          $sitefromcategory = [];
          $basecategory = [];
          if(!empty($productAttributes['categories'])) {
            foreach ($productAttributes['categories'] as $urlalias) {
              $urlalias_seperate = explode("_", $urlalias);
              $sitefromcategory[] = $urlalias_seperate[0];
              if ($urlalias_seperate[0] == "Delta") {
                $basecategory[] = strtolower($urlalias_seperate[1]);
              }
            }
          }
          if(!empty($sitefromcategory)) {
            $basecategoryfinal = array_unique($basecategory);
            if(in_array('recommerce', $basecategoryfinal)) {
              $ribbonText = "Recertified";
              $ribboncolor = "gray-30";
            }
          }
          if(!in_array('recommerce', $basecategoryfinal)) {
            if (!empty($ribbonVal)) {
              if ($ribbonVal != 'NA' &&
                $ribbonVal != 'OEM' &&
                $ribbonVal != 'Retail' &&
                $ribbonVal != 'Warranty' &&
                $ribbonVal != 'Internal Use Only' &&
                $discontinued != TRUE &&
                $commingSoon != TRUE
              ) {
                $ribbonText = "Only at " . str_replace("_", " and ", $ribbonVal);
                $ribboncolor = "gray-30";
              }
            }
          }
          $productDetails[$productAttributes['name']] = [
            'sku' => [
              'sku' => $productAttributes['name'],
            ],
            'image' => [
              'path' => $productAttributes['heroImageSmall'],
            ],
            'name' => [
              'name' => $productAttributes['name'],
            ],
            'description' => [
              'desc' => $productAttributes['description'],
            ],
            'collection' => [
              'collection' => (array_key_exists('Collection', $productAttributes['values'])) ? $productAttributes['values']['Collection'] : $productAttributes['values']['defaultCollection'],
            ],
            'price' => [
              'label' => 'List Price as Shown',
              'price' => $productAttributes['values']['ListPrice'],

            ],
            'ribbontext' => [
              'ribbontext' => $ribbonText,
            ],
            'ribboncolor' => [
              'ribboncolor' => $ribboncolor,
            ],
          ];
        }
      }
      foreach ($readJsonArr['products'] as $productAttributes) {
        if(array_key_exists("ListPrice", $productAttributes['values'])){
          $compareLabValArr['List Price'][] = "$ ".$productAttributes['values']['ListPrice'];
        }
        else{
          $compareLabValArr['List Price'][] = "no data avaliable";
        }
        if (array_key_exists("finishOptions", $productAttributes)) {
          if (count($productAttributes['finishOptions'])) {
            $addMore = [];
            foreach ($productAttributes['finishOptions'] as $finishList) {
              $finishProductUrl = $this->productservice->getProductUrl($finishList['modelNumber']);
              if ($finishProductUrl === '#') {
                $finishProductUrl = "/pdp/" . $finishList['modelNumber'];
              }
              $addMore[] = get_taxonomy_using_name($finishList['facetFinish'], 'facetfinish', 'icon');
            }
            $compareLabValArr['Finishes'][] = $addMore;
          }
        }
        if(array_key_exists("Finish", $productAttributes['values'])){
          $compareLabValArr['Finish Shown'][] = $productAttributes['values']['Finish'];
        }
        else{
          $compareLabValArr['Finish Shown'][] = "no data avaliable";
        }
        if(array_key_exists("numberOfHandles", $productAttributes['values'])){
          $compareLabValArr['Number Of Handles'][] = $productAttributes['values']['numberOfHandles'];
        }
        else{
          $compareLabValArr['Number Of Handles'][] = "no data avaliable";
        }
        if (array_key_exists("HolesforInstallation", $productAttributes['values'])) {
          $compareLabValArr['Number of Holes'][] = $productAttributes['values']['HolesforInstallation'];
        }
        else{
          $compareLabValArr['Number of Holes'][] = "no data avaliable";
        }
        if (array_key_exists("facetFeature", $productAttributes) && is_array($productAttributes)) {
          $facetspecialFeaturesResult = [];
          $facetFeatures = explode(" ", $productAttributes["facetFeature"]);
          foreach ($facetFeatures as $facetfeaturesname) {
              $facetspecialFeaturesResult[] = get_taxonomy_using_name($facetfeaturesname, 'facetfeature', 'text');
          }
          $compareLabValArr['Available Features'][] = $facetspecialFeaturesResult;
        }
        else{
          $compareLabValArr['Available Features'][] = "no data avaliable";
        }
        if (array_key_exists("facetInnovation", $productAttributes) && is_array($productAttributes)) {
          $facetInnovationDetails = [];
          $facetInnovaDet = explode(" ", $productAttributes["facetInnovation"]);
          foreach ($facetInnovaDet as $facetInnovation) {
            $facetInnovationDetails[] = get_taxonomy_using_name($facetInnovation, 'facetinnovation', 'text');
          }
          $compareLabValArr['Available Innovations'][] = $facetInnovationDetails;
        }
        else{
          $compareLabValArr['Available Innovations'][] = "no data avaliable";
        }
        if(!empty($productAttributes['categories'])){
          $pulldown_occurance = 0;
           foreach($productAttributes['categories'] as $categories){
            if (strpos($categories, 'Pull-Outs')) {
              $pulldown_occurance = $pulldown_occurance+1;
            }
          }
          if($pulldown_occurance > 0) {
            $compareLabValArr['Pull-out/Pull-Down'][] = "Yes";
          }else{
            $compareLabValArr['Pull-out/Pull-Down'][] = "No";
          }
        }
        if (array_key_exists("PBOptionalEscutcheon", $productAttributes['values'])) {
          $compareLabValArr['Escutcheon'][] = $productAttributes['values']['PBOptionalEscutcheon'];
        }
        else{
          $compareLabValArr['Escutcheon'][] = "no data avaliable";
        }
        if (array_key_exists("FlowRate", $productAttributes['values'])) {
          $compareLabValArr['FlowRate'][] = $productAttributes['values']['FlowRate'];
        }
        else{
          $compareLabValArr['FlowRate'][] = "no data avaliable";
        }
        if (array_key_exists("PBSpoutTotalHeight", $productAttributes['values'])) {
          $compareLabValArr['Spout Total Height'][] = $productAttributes['values']['PBSpoutTotalHeight'].'"';
        }
        else{
          $compareLabValArr['Spout Total Height'][] = "no data avaliable";
        }
        if (array_key_exists("PBSpoutLength", $productAttributes['values'])) {
          $compareLabValArr['Spout Length'][] = $productAttributes['values']['PBSpoutLength'].'"';
        }
        else{
          $compareLabValArr['Spout Length'][] = "no data avaliable";
        }
        if (array_key_exists("PBSupplyLines", $productAttributes['values'])) {
          $compareLabValArr['Supply Lines'][] = $productAttributes['values']['PBSupplyLines'];
        }
        else{
          $compareLabValArr['Supply Lines'][] = "no data avaliable";
        }
        $draintypeOccurance = 0;
        if (array_key_exists("DrainType", $productAttributes['values'])) {
          $draintypeOccurance = $draintypeOccurance + 1;
        }
        if($draintypeOccurance > 0){
          if (array_key_exists("DrainType", $productAttributes['values'])) {
            $compareLabValArr['Pop-Up Type'][] = $productAttributes['values']['DrainType'];
          }
        else{
            $compareLabValArr['Pop-Up Type'][] = "no data avaliable";
          }
        }
        if (array_key_exists("ConnectionType", $productAttributes['values'])) {
          $compareLabValArr['Fittings Type'][] = $productAttributes['values']['ConnectionType'];
        }
        else{
          $compareLabValArr['Fittings Type'][] = "no data avaliable";
        }
        if (array_key_exists("Over All Height", $productAttributes['values'])) {
          $compareLabValArr['Spout Height Deck to Aerator'][] = $productAttributes['values']['Over All Height'].'"';
        }
        else{
          $compareLabValArr['Spout Height Deck to Aerator'][] = "no data avaliable";
        }
        if (array_key_exists("PBMaxDeckThickness_decimal", $productAttributes['values'])) {
          $compareLabValArr['Deck Thickness'][] = $productAttributes['values']['PBMaxDeckThickness_decimal'].'"';
        }
        else{
          $compareLabValArr['Deck Thickness'][] = "no data avaliable";
        }
        if (array_key_exists("ValveType", $productAttributes['values'])) {
          $compareLabValArr['Valve Type'][] = $productAttributes['values']['ValveType'];
        }
        else{
          $compareLabValArr['Valve Type'][] = "no data avaliable";
        }
      }
    }
    $listing = [];
    $listingorder = ['List Price','Finishes','Finish Shown','Number Of Handles','Number of Holes','Available Features','Available Innovations','Pull-out/Pull-Down','Escutcheon','FlowRate','Spout Total Height','Spout Length','Supply Lines', 'Pop-Up Type','Fittings Type','Spout Height Deck to Aerator','Deck Thickness','Valve Type'];
    foreach($listingorder as $order){
      $listing[$order] = $compareLabValArr[$order];
    }
    $output = [
      '#theme' => 'product_compare_results',
      '#items' => $productDetails,
      '#compareItems' => $listing,
    ];

    return $output;
  }

  public function is_multi($geeks) {
    $rv = array_filter($geeks, 'is_array');
    if (count($rv) > 0) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Fetch all available parts for the particular product
   */
  public function getParts(Request $request) {
    $page_path = $request->request->get('path');
    $pageNumber = $request->request->get('page');
    $page_path = explode('/', $page_path);
    $propertyAccessor = PropertyAccess::createPropertyAccessor();
    $partlist_coll = [];
    $sku = end($page_path);
    $sync_config = $this->configFactory->get('delta_services.deltaservicesconfig');
    $product_config = $this->configFactory->get('sync.api_settings');
    $domain = $sync_config->get('api_base_path');
    $partsPerPage = $product_config->get('partlist_per_page');
    $partsApi = $domain . $product_config->get('partlist_api');
    $partsApi = str_replace('{size}', $partsPerPage, $partsApi);
    $parts_cache = $this->deltaCache->deltaGetCache('PDP_PARTS' . $sku);
    if (!empty($parts_cache)) {
      $partsAPI = $parts_cache['api'];
    }
    else{
      $response = $this->productservice->productBaseDetails($sku);
      if($response) {
        if (!empty($response->productReferences)) {
          $partlist = $response->productReferences;
          $appendModelNumber = '';
          foreach ($partlist as $productpartlist) {
            $modelNumber = $propertyAccessor->getValue($productpartlist, 'modelNumber');
            if(!empty($modelNumber)){
              $appendModelNumber .= '&or_name=' . $modelNumber;
            }
          }
          $partsAPI = $partsApi . $appendModelNumber;
        }
      }
    }
    $partsAPI .= "&page=" . $pageNumber;
    $cid = "partlist-" . $sku . $pageNumber;
    $data = $this->deltaCache->deltaGetCache($cid);
    $gtm_parts = [];
    if (!empty($data)) {
      $output = $data;
    }
    else {
      // Parts API Call
      $method = 'GET';
      $headers = add_headers(FALSE);
      $partsAPI = $this->deltaservice->clenupMultipleAmp($partsAPI);
      $partsAPIresponse = $this->deltaservice->apiCall($partsAPI, $method, $headers, '', 'PDP Parts');
      if($partsAPIresponse['status'] == 'SUCCESS' && $partsAPIresponse['response'] !='error'){
        $partsResponse = json_decode($partsAPIresponse['response']);
        $partsResponseContent = $propertyAccessor->getValue($partsResponse, 'content');
        $position = $pageNumber ? ($pageNumber*12) +1 : 1;
        foreach ($partsResponseContent as $partsResponseContentPart) {
          $partsResponseChkFlag = $propertyAccessor->getValue($partsResponseContentPart, 'values.RepairPart');
          if($partsResponseChkFlag  === 'T'){
            $partsSKU = $propertyAccessor->getValue($partsResponseContentPart, 'name');
            $PartslistPrice = number_format((float) $partsResponseContentPart->values->ListPrice, 2, '.', '');
            $collection = (!empty($partsResponseContentPart->values->Collection)) ? $partsResponseContentPart->values->Collection :  $partsResponseContentPart->values->Brand;
            $gtmCollection = (!empty($partsResponseContentPart->values->Collection)) ? $partsResponseContentPart->values->Collection :  'Delta - No Collection';
            $model = $partsResponseContentPart->values->ModelNumber;
            $description = $partsResponseContentPart->description;
            $categories = property_exists($partsResponseContentPart, 'categories') ? $partsResponseContentPart->categories : '';
            $gtm_categories = '';
            if(!empty($categories)) {
              foreach ($categories as $urlalias) {
                $urlalias_seperate = explode("_", $urlalias);
                if ($urlalias_seperate[0] == "Delta") {
                  $gtm_categories = str_replace("_", "/", $urlalias);
                  break;
                }
              }
            }
            $Partlist_orderDate = '';
            if(is_object($partsResponseContentPart->values) && property_exists($partsResponseContentPart->values, 'AvailableToOrderDate')){
              $Partlist_orderDate = $partsResponseContentPart->values->AvailableToOrderDate;
            }

            $getFinishOptFromRespArr = $this->productservice->setFinishOptions($partsResponseContentPart);

            $partlist_coll[] = [
              'Partlist_finish' => property_exists($partsResponseContentPart->values, 'Finish') ? $partsResponseContentPart->values->Finish : '',
              'Partlist_defaultFacetFinish' => property_exists($partsResponseContentPart, 'facetFinish') ? $partsResponseContentPart->facetFinish : '',
              'Partlist_finishOptions' => $getFinishOptFromRespArr,
              'Partlist_collection' => $collection,
              'Partlist_id' => $propertyAccessor->getValue($partsResponseContentPart, 'id'),
              'Partlist_model_number' => $model,
              'Partlist_description' => $description,
              'Partlist_type' => $partsResponseContentPart->values->Type,
              'Partlist_orderDate' => $Partlist_orderDate,
              'Partlist_stockingType' => (!(empty($partsResponseContentPart->values->stockingType))) ? $partsResponseContentPart->values->stockingType : '',
              'Partlist_image' => $partsResponseContentPart->heroImageSmall,
              'Partlist_listprice' => '$'.$PartslistPrice,
              'Partlist_productLink' => $this->productservice->getProductUrl($partsResponseContentPart->values->ModelNumber),
            ];
            $gtm_parts[] = [
                    "brand" => strip_tags($gtmCollection),
                    "category" => $gtm_categories,
                    "id" => $model,
                    "list" => "parts list",
                    "name" => strip_tags($description),
                    "position" => $position,
                    "price" => '$'.$PartslistPrice,
                    "variant" => property_exists($partsResponseContentPart->values, 'Finish') ? $partsResponseContentPart->values->Finish : '',
            ];
            $position++;
          }
        }
      }
      $output['partlist_products'] = $partlist_coll;
      $output['gtm_parts'] = $gtm_parts;

      if(is_object($partsResponse) && isset($partsResponse->totalElements)) {
        $parts = $propertyAccessor->getValue($partsResponse, 'totalElements');

      if($parts == $partsPerPage){
        $pages = 1;
      }else{
        $pages = ceil($parts/$partsPerPage);
      }
      $output['pages'] = $pages;
      $output['current_page'] = $pageNumber;
      $output['total'] = $pages;
      }
      $this->deltaCache->deltaSetCache($cid, $output);
    }

    if (!empty($output)) {
      $render = [
        '#theme' => 'commerce_product_parts',
        '#data' => $output
      ];
      $json_data = [
        'gtm_parts' => $output['gtm_parts'] ? json_encode($output['gtm_parts']) : '',
        'pager' => $output,
        'content' => render($render)
      ];
    }
    else {
      $json_data = false;
    }
    return new JsonResponse ($json_data);
  }

  /**
   * Fetch the related products of the sku
   */
  public function getFeatureProducts() {
    $page_path = isset($_POST['path']) ? explode('/', $_POST['path']) : '';
    $sku = end($page_path);
    $fp_details = $this->deltaCache->deltaGetCache('PDP_featured' . $sku);
    if(empty($fp_details)){
         $fp_cache = $this->deltaCache->deltaGetCache('PDP_FP' . $sku);
      if(!empty($fp_cache)){
        $fp_details = $this->productservice->getProductInfoMultiple($fp_cache);
        $this->deltaCache->deltaSetCache('PDP_featured' . $sku, $fp_details);
      }else {
        $response = $this->productservice->productBaseDetails($sku);
        if ($response) {
          foreach ($response->configOptions as $feature) {
            if ($feature->optionType == "addon") {
              foreach ($feature->optionProducts as $featureProduct) {
                $features_details[] = $featureProduct->modelNumber;
              }
            }
          }
          if (!empty($features_details)) {
            $fp_details = $this->productservice->getProductInfoMultiple($features_details);
            $this->deltaCache->deltaSetCache('PDP_featured' . $sku, $fp_details);
          }
        }
      }
    }
    $output['featureproductdetails'] = $fp_details;
    if(!empty($fp_details)){
      $gtm_feature = [];
      $position = 1;
      foreach ($fp_details as $fp_detail) {
        $brand = '';
        if(is_array($fp_detail) && array_key_exists('brand', $fp_detail)){
          $brand = strip_tags($fp_detail['brand']);
        }
        $variant = '';
        if(is_array($fp_detail) && array_key_exists('variant', $fp_detail)){
          $variant = strip_tags($fp_detail['variant']);
        }
        $gtm_categories = '';
        if(is_array($fp_detail) && array_key_exists('gtm_categories', $fp_detail)){
          $gtm_categories = $fp_detail['gtm_categories'];
        }
        $gtm_feature[] = [
          "brand" =>  $brand,
          "category" => $gtm_categories,
          "id" => $fp_detail['model_number'],
          "list" => "featured accessories",
          "name" => strip_tags($fp_detail['description']),
          "position" => $position,
          "price" => $fp_detail['list_price'],
          "variant" => $variant,
        ];
        $position++;
      }
      $render = [
        '#theme' => 'commerce_product_featured',
        '#data' => $output
      ];
      $json_data['html'] = render($render);
      $json_data['gtm_feature'] = json_encode($gtm_feature);
    }
    else{
      $json_data = false;
    }
    return new JsonResponse ($json_data);
  }

  /**
   * Fetch the related products of the sku
   */
  public function getRelated() {
    $page_path = isset($_POST['path']) ? explode('/', $_POST['path']) : '';
    $sku = end($page_path);
    $type_cache = $this->deltaCache->deltaGetCache('PDP_RP'.$sku);
    if(is_object($type_cache)) {
      $type_cache = (array)$type_cache;
    }
    if (!empty($type_cache) && !empty($type_cache['collection'])) {
      $productReferences = $this->productservice->getProductReference(
        html_entity_decode(strip_tags($type_cache['collection'])),
        $type_cache['producttype']
      );
      $output['productReferences'] = $productReferences;
      $output['viewAllCollectionLink'] = $type_cache['viewAllCollectionLink'];
    }
    else{
      $response = $this->productservice->productBaseDetails($sku);
      if($response){
        $categories = $response->categories;
        foreach ($response->categories as $urlalias){
          $urlalias_seperate = explode("_", $urlalias);
          if($urlalias_seperate[0] == "Delta"){
            $baseCategory = strtolower($urlalias_seperate[1]);
            break;
          }
        }
        $producttype = !empty($baseCategory) ? str_replace('room', '', $baseCategory) : 'bath';
        $collection = !empty($response->values->Collection) ? $response->values->Collection : $response->values->defaultCollection;
        if(!empty($collection)) {
          $collection = html_entity_decode(strip_tags($collection));
          $productReferences = $this->productservice->getProductReference(
            $collection,
            $producttype
          );
          $output['productReferences'] = $productReferences;
          $viewAllCollectionLink = $response->values->viewAllCollectionLink;
          $output['viewAllCollectionLink'] =  $viewAllCollectionLink && $viewAllCollectionLink !== '#' ? $viewAllCollectionLink : '' ;
        }
      }
    }
    if(!empty($productReferences)){
      $gtm_related = [];
      $position = 1;
      foreach ($productReferences as $productReference) {
        $gtm_related[] = [
          "brand" => strip_tags($productReference['type']),
          "category" => $productReference['gtm_categories'],
          "id" => $productReference['modelNumber'],
          "list" => "related products",
          "name" => strip_tags($productReference['description']),
          "position" => $position,
          "price" => '$'.$productReference['listPrice'],
          "variant" => $productReference['finish'],
        ];
        $position++;
      }
      $render = [
        '#theme' => 'commerce_product_related',
        '#data' => $output
      ];
      $json_data['html'] = render($render);
      $json_data['gtm_related'] = json_encode($gtm_related);
    }
    else {
      $json_data = false;
    }

    return new JsonResponse ($json_data);

  }

  /**
   * Repair Parts products URL Redirecting.
   */
  public function repairPartsPdp(){

    $sku = $this->requestStack->getCurrentRequest()->get('modelNumber');
    $dataOutput = [];
    if ($sku) {
      // Cache pdp information.
      $data = $this->deltaCache->deltaGetCache('PDP_' . $sku);
      if (!empty($data->data)) {
        $dataOutput = $data->data;
        self::$page_title = $dataOutput['name']['#markup'];
      }
      else {
        $products = $this->productimportService->productExists($sku);
        $productId = key($products);
        if (!empty($productId)) {
          $productDetails = $this->productservice->productDetailsPage($sku);
          if (!empty($productDetails) && (!empty($productDetails['output']['#data']))) {
              $dataOutput = $productDetails['output']['#data'];
              self::$page_title = $dataOutput['name']['#markup'];
            $cart_array = $this->productLazyBuilders->addToCartForm($productId, 'teaser', true, 'en');
            $dataOutput['variations'] = $cart_array;
          }
        }
        else {
          $syncProductId = $this->productservice->syncProduct($sku);
          if (!empty($syncProductId)) {
            $syncProductDetails = $this->productservice->productDetailsPage($sku);
            if (!empty($syncProductDetails) && (!empty($syncProductDetails['output']['#data']))) {
              $dataOutput = $syncProductDetails['output']['#data'];
              self::$page_title = $dataOutput['name']['#markup'];
              $synCart_array = $this->productLazyBuilders->addToCartForm($syncProductId, 'teaser', true, 'en');
              $dataOutput['variations'] = $synCart_array;
            }
          }
        }
      }
    }

    if(!empty($dataOutput)) {
      $gtm_categories = '';
      // GTM dataLayer variables
      if(isset($dataOutput['categories']) && !empty($dataOutput['categories'])) {
        foreach ($dataOutput['categories'] as $urlalias) {
          $urlalias_seperate = explode("_", $urlalias);
          if ($urlalias_seperate[0] == "Delta") {
            $baseCategory = strtolower($urlalias_seperate[1]);
            $gtm_categories = str_replace("_", "/", $urlalias);
            break;
          }
        }
      }
      $gtmCollectionName = (!empty($dataOutput['collections']) )? $dataOutput['collections'] : 'Delta - No Collection';
      $gtm_detail[] = array(
        "brand" => strip_tags($gtmCollectionName),
        "category" => strip_tags($gtm_categories),
        "id" => $dataOutput['model'],
        "name" => strip_tags($dataOutput['descriptionWithCollection']),
        "price" => '$'.$dataOutput['listPrice'],
        "variant" => strip_tags($dataOutput['finish'])
      );
      $output = [
        '#theme' => 'commerce_product_rp',
        '#data' => $dataOutput,
        '#attached' => [
          'drupalSettings' => [
            'product_details' => [
              'gtm_detail' => json_encode($gtm_detail),
            ],
          ],
        ]
      ];
    }
    else {
      $output = [
        '#theme' => 'commerce_product_rp',
        '#data' => "",
      ];
    }
    return $output;
  }

  /**
   * Bulk Products Delete.
   */
  public function bulkDeleteProducts() {
    $response = $this->productservice->deleteAllInEntity();
    $response = new RedirectResponse("/admin/commerce/products");
    return $response->send();
  }

  /**
   * Bulk Products Delete.
   */
  public function bulkDeleteProductswithoutCategory() {
    $response = $this->productservice->deleteAllwithoutCategories();
    $response = new RedirectResponse("/admin/commerce/products");
    return $response->send();
  }


  /**
   * Get Dynamic page title.
   */
  public function getPageTitle() {
    return self::$page_title;
  }

  /**
   * Get Product List Price.
   */
  public function getProductListPrice() {
    $queryString = $this->requestStack->getCurrentRequest()->query->all();
    if(!empty($queryString['sku'])) {
      $response = $this->productservice->getListPrice($queryString['sku']);
      if(!empty($response['number'])) {
        $list_price = number_format($response['number'],2);
      }
      else {
        $list_price = $response['number'];
      }
      return new JsonResponse($list_price);
    }
  }
}

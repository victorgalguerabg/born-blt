<?php

namespace Drupal\product_details;
use Drupal\commerce_product\Entity\Product;
use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\path_alias\AliasManagerInterface;
use Drupal\Core\Url;
use Drupal\delta_services\DeltaService;
use Drupal\sync_product\ProductImportService;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\delta_cache\DeltaCacheService;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Drupal\delta_custom\GlobalProduct;

/**
 * Provides service for Product Description Page.
 */
class ProductService {

    /**
  * Drupal\global_product\GlobalProduct definition.
  *
  * @var \Drupal\global_product\GlobalProduct
  */
  protected $globalproduct;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entity;

  /**
   * Drupal\sync_product\ProductImportController definition.
   *
   * @var \Drupal\sync_product\ProductImportInterface
   */
  protected $service;

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Drupal\Core\Messenger\MessengerInterface definition.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Drupal\delta_services\DeltaService definition.
   *
   * @var \Drupal\delta_services\DeltaService
   */
  protected $deltaservice;

  /**
   * An alias manager to find the alias for the current system path.
   *
   * @var \Drupal\path_alias\AliasManagerInterface
   */
  protected $aliasManager;

  /**
   * Drupal\delta_services\DeltaService definition.
   *
   * @var \Drupal\delta_cache\DeltaCache
   */
  protected $deltacache;

  /**
   * Class constructor.
   */
  public function __construct(ProductImportService $service,
                              ConfigFactoryInterface $configFactory,
                              MessengerInterface $messenger,
                              EntityTypeManagerInterface $entity,
                              DeltaService $deltaservice,
                              AliasManagerInterface $alias_manager,
                              DeltaCacheService $deltacache,
                              GlobalProduct $globalproduct) {
    $this->service = $service;
    $this->configFactory = $configFactory;
    $this->messenger = $messenger;
    $this->entity = $entity;
    $this->deltaservice = $deltaservice;
    $this->aliasManager = $alias_manager;
    $this->deltacache = $deltacache;
    $this->pdpCacheSlug = "PDP_";
    $this->globalproduct = $globalproduct;
  }

  public function begnWith($str, $begnString) {
    $len = strlen($begnString);
    return (substr($str, 0, $len) === $begnString);
  }

  public function endsWith($haystack, $needle) {
    $length = strlen($needle);
    if ($length == 0) {
      return TRUE;
    }

    return (substr($haystack, -$length) === $needle);
  }

  /**
   * Common Method to load the Product Details.
   */
  public function productBaseDetails($sku) {
    $sku = strtoupper(strtolower($sku));
    $config = $this->configFactory->get('delta_services.deltaservicesconfig');
    $deltaConfig = $this->configFactory->get('sync.api_settings');
    $domain = $config->get('api_base_path');
    $pdpUrl = $domain . $deltaConfig->get('rest_api_settings.product_by_sku');
    $pdpUrl = str_replace("{productSku}", $sku, $pdpUrl);
    $checkCacheResult = $this->pdpCacheSlug . $sku;
    $cachedResult = $this->deltacache->deltaGetCache($checkCacheResult);
    if ($cachedResult != "") {
      $response = $cachedResult;
    }
    else {
      $method = 'GET';
      $headers = add_headers(FALSE);
      $rawResponse = $this->deltaservice->apiCall(
        $pdpUrl, $method, $headers, '', "PDP"
      );
      $response = json_decode($rawResponse['response']);
      $this->deltacache->deltaSetCache(
        $checkCacheResult,
        $response
      );
    }
    return $response;
  }

  public function basePartDetail($baseModel) {
    $propertyAccessor = PropertyAccess::createPropertyAccessor();
    $basepartsCollection = '';
    $config = $this->configFactory->get('delta_services.deltaservicesconfig');
    $deltaConfig = $this->configFactory->get('sync.api_settings');
    $domain = $config->get('api_base_path');
    $pdpUrl = $domain . $deltaConfig->get('rest_api_settings.product_by_sku');
    $pdpUrl = str_replace("{productSku}", $baseModel, $pdpUrl);
    $method = 'GET';
    $headers = add_headers(FALSE);
    $baseparts = $this->deltaservice->apiCall($pdpUrl, $method, $headers, '', "PDP");
    $basepartsdata = json_decode($baseparts['response']);
    $basepartimage = $propertyAccessor->getValue($basepartsdata, 'heroImage');
    if(!empty($basepartsdata->values->Collection)) {
      $basepartsCollection = $propertyAccessor->getValue($basepartsdata, 'values.Collection');
    }
    $basepartsdefaultCollection = !empty($propertyAccessor->getValue($basepartsdata, 'values.defaultCollection')) ? $propertyAccessor->getValue($basepartsdata, 'values.defaultCollection') : '';
    $basepartsModelnumber = $propertyAccessor->getValue($basepartsdata, 'name');
    $basepartsDescription = $propertyAccessor->getValue($basepartsdata, 'description');
    $basepartlistprice = $propertyAccessor->getValue($basepartsdata, 'values.ListPrice');
    $basepart[] = [
      "image" => $basepartimage,
      "collection" => !empty($basepartsCollection) ? $basepartsCollection : $basepartsdefaultCollection,
      "modelnumber" => $basepartsModelnumber,
      "description" => $basepartsDescription,
      "listPrice" => number_format((float) $basepartlistprice, 2, '.', ''),
    ];
    return $basepart;
  }


  public function objecttoArray($propertyAccessor, $inputArray, $price = 0, $modelNumber = '') {
    $returnArray = [];
    $returnArray['collection'] = '';
    $returnArray['modelnumber'] = $modelNumber . '' . $propertyAccessor->getValue($inputArray, 'modelNumber');
    $returnArray['description'] = $propertyAccessor->getValue($inputArray, 'description');
    $configprice = $propertyAccessor->getValue($inputArray, 'listPrice');
    $configprice = (float) $configprice + (float) $price;
    $returnArray['listPrice'] = number_format((float) $configprice, 2, '.', '');
    $returnArray['image'] = $propertyAccessor->getValue($inputArray, 'image');
    $returnArray['imageAlt'] = $propertyAccessor->getValue($inputArray, 'description');
    return $returnArray;
  }


  /**
   * Fetch the product detail page.
   */
  public function productDetailsPage($sku) {
    $propertyAccessor = PropertyAccess::createPropertyAccessor();
    $output = [];
    $build = [];
    $bazaarvoiceconfig = $this->configFactory->get('bazaarvoice.api_settings');
    $cloudKey = $bazaarvoiceconfig->get('bazaarvoice_api_settings.cloud_key');
    $siteEnvironment = $bazaarvoiceconfig->get('bazaarvoice_api_settings.site_environment');
    $siteId = $bazaarvoiceconfig->get('bazaarvoice_api_settings.site_id');
    $siteLocale = $bazaarvoiceconfig->get('bazaarvoice_api_settings.site_locale');
    $prop_config = $this->configFactory->get('product_details.settings');
    $finish_stocking_type = $prop_config->get('finish_stocking_type');
    $prop_warning_msg = $prop_config->get('rest_api_settings.prop_65_warning_label');
    $prop_help_text = $prop_config->get('rest_api_settings.prop_65_help_text');
    $prop_help_tex_notes = $prop_config->get('rest_api_settings.prop_65_help_text_notes');
    $prop_help_tex_notes_replace = $prop_config->get('rest_api_settings.prop_65_help_text_notes_replace');
    $handle_or_accent_text = $prop_config->get('handle_or_accent');
    $rough_valve_text = $prop_config->get('rough_valve');
    $base_part_text = $prop_config->get('base_part');
    $recommended_replacement_text = $prop_config->get('recommended_replacement');
    $configuration_part_text = $prop_config->get('configuration_part');
    $localDealerButtonText = $prop_config->get('local_dealer_button_text') ?? 'Find Local Dealers';
    $shopOnlineButtonText = $prop_config->get('shop_online_button_text') ?? 'Shop Online';
    $sync_config = $this->configFactory->get('delta_services.deltaservicesconfig');
    $bimSmithAPI =  $sync_config->get('bimsmith');
    $validateOrderShipDate = $sync_config->get('order_ship_date');
    $output['video_thump'] = '';
    $infoImageArr = $prop_config->get('pdp_video_thumb');
    $fid = $infoImageArr[0];
    if(!empty($fid) && is_numeric($fid)) {
      if (is_object($this->entity->getStorage('file')->load($fid))) {
        $imagePathArr = $this->entity->getStorage('file')->load($fid)->toArray();
        $output['video_thump'] = file_create_url($imagePathArr['uri'][0]['value']);
      }
    }
    $domain = $sync_config->get('api_base_path');
    $product_config = $this->configFactory->get('sync.api_settings');
    $partsPerPage = $product_config->get('partlist_per_page');
    $partsApi = $domain . $product_config->get('partlist_api');
    $partsApi = str_replace('{size}', $partsPerPage, $partsApi);

    $response = $this->productBaseDetails($sku);

    $productId = $this->syncProduct($sku);
    $productObject = $this->entity->getStorage('commerce_product')
        ->load((int) $productId);
    if ($response) {
      $data = $response;
      //Redirect to 404, when the AvailableToOrderDate /  AvailableToShipDate is 9999-12-31
      if((!(empty($data->values->AvailableToOrderDate)) && ($data->values->AvailableToOrderDate === $validateOrderShipDate)) || ((!(empty($data->values->AvailableToShipDate))) && ($data->values->AvailableToShipDate === $validateOrderShipDate)))
      {
        throw new NotFoundHttpException();
      }

      if(property_exists($data->values, 'WebsiteUS')){
        if(((empty($data->values->WebsiteUS)) || ($data->values->WebsiteUS !== 'T'))){
              throw new NotFoundHttpException();
         }
       } else {
            throw new NotFoundHttpException();
      }


      $isRecomProduct = ($productObject->field_refurbished_product->value)? $productObject->field_refurbished_product->value : FALSE;
      $output['warranty_block'] = '';
      $output['showSalePrice'] = FALSE;
      $output['salePrice'] = 0;
      if($isRecomProduct){
        $isRecomProduct = TRUE;
        $recommerceConfig = $this->configFactory->get('delta_recommerce.recommerce_pdp_config_form');
        $output['recomBlockId'] = $recommerceConfig->get('installation_block');
        $output['warranty_block'] = $recommerceConfig->get('warranty_block');
        $warranty_fid = $recommerceConfig->get('warranty_image');
        $path = '';
        if($warranty_fid){
          $file = $this->entity
          ->getStorage('file')
          ->load($warranty_fid);
            if($file) {
              $path = file_create_url($file->getFileUri());
            }
        }

        $output['warranty_image'] = $path;
        $product_variation = $this->entity->getStorage('commerce_product_variation')->load((int)$productObject->getVariationIds()[0]);
        $list_price = $product_variation->get('list_price')->getValue()[0]['number'];
        $price = $product_variation->get('price')->getValue()[0]['number'];
        if($list_price <> $price && $list_price != 0){
          $output['salePrice'] = number_format($list_price, 2);
          $output['showSalePrice'] = TRUE;
        }
      }

      $output['isRecomProduct'] = $isRecomProduct;
      $output['bazaarvoicecloudKey'] = $cloudKey;
      $output['bazaarvoicesiteEnvironment'] = $siteEnvironment;
      $output['bazaarvoicesiteId'] = $siteId;
      $output['bazarvoicesiteLocale'] = $siteLocale;
      $output['handle_or_accent_text'] = $handle_or_accent_text;
      $output['rough_valve_text'] = $rough_valve_text;
      $output['base_part_text'] = $base_part_text;
      $output['configuration_part_text'] = $configuration_part_text;
      $output['recommended_replacement_text'] = $recommended_replacement_text;
      $description = $data->description;
      $output['function'] = strtolower($data->values->Function);
      $output['unconfigured'] = isset($data->unconfigured) ? $data->unconfigured : '';
      $output['name']['#markup'] = $description;
      $listPrice = isset($data->values->ListPrice) ? $data->values->ListPrice : '';
      $output['listPrice'] = number_format($listPrice, 2);
      $modelName = $propertyAccessor->getValue($data, 'name');
      $ModelNumber = $propertyAccessor->getValue($data, 'name');
      $output['model'] = $modelName;

      $output['facetfinish'] = $propertyAccessor->getValue($data, 'facetFinish');
      if (!empty($data->values->ConsumerCopy)) {
        $output['productcontentdetails'] = $propertyAccessor->getValue($data, 'values.ConsumerCopy');
      }
      $collection = (property_exists($data->values, 'Collection')) ? $data->values->Collection: '';
      if (!empty($collection)) {
        $output['collections'] = strtoupper($collection);
      }
      $output['descriptionWithCollection'] = $data->description;
      $categories = $data->categories;
      $getCollectionName = '';
      $gtm_categories = '';
      foreach ($data->categories as $urlalias) {
        $urlalias_seperate = explode("_", $urlalias);
        if ($urlalias_seperate[0] == "Delta") {
          $baseCategory = strtolower($urlalias_seperate[1]);
          if (strtolower($urlalias_seperate[2]) == 'kitchen sinks' || strtolower($urlalias_seperate[2]) == 'bathroom sinks') {
            $baseCategory = str_replace(" ", "-", strtolower($urlalias_seperate[2]));
          }
          $gtm_categories = str_replace("_", "/", $urlalias);
          $getCollectionName = '/' . $baseCategory . '/collections/collectionName-'. $baseCategory . '-collection';
          break;
        }
      }
      $output['basecategory'] = !empty($baseCategory) ? ucfirst($baseCategory) : '';
      if ($collection != "" && $baseCategory != "") {
        $basecollectionpath = $collection . $baseCategory;
        $output['basecollectionpath'] = $basecollectionpath;
      }
      $i = 1;
      $finishstocking = explode(",", $finish_stocking_type);
      if (!empty($data->values->Finish)) {
        $output['finish'] = $data->values->Finish;
      }

      //Recommerece Fix - Hide all finish chip except current sku finish chip.
      if($isRecomProduct == TRUE){
        $this->getOnlyRecertFinishes($data->name, $data);
      }
      // List of Recommerce Categories. Configured in /admin/config/delta_services/recommerce-config
      $recommerceConfig = $this->configFactory->get('delta_recommerce.recommerce_pdp_config_form');
      $recommerceCategories = array_map('trim', explode("\n", $recommerceConfig->get('recommerce_category_list')));
      // List of Recommerce Categories
     if (count($data->finishOptions) > 0) {
        foreach ($data->finishOptions as $finish) {
          //Update Website Logic for PDP/Finish SwatcH
          $getDataObj = $this->productBaseDetails($finish->modelNumber);
          if($isRecomProduct == FALSE) {
            $json_arr = (array) $getDataObj;
            if (count(array_intersect($recommerceCategories,$json_arr['categories']))) {
              continue;
            }
          }
          $getFinishSkuData = $this->globalproduct->getFoundationalOrderShipAttr($getDataObj);
          if($getFinishSkuData == TRUE){
            $now = time();
            if (!in_array($finish->stockingType, $finishstocking) && ($now > $finish->availableToOrderDate || !empty($finish->availableToOrderDate))) {
              $finish->productUrl = self::getProductUrl($finish->modelNumber);
              $finish->productIcon = get_taxonomy_using_name($finish->facetFinish, 'facetfinish', 'icon');
              if ($finish->modelNumber == $data->name) {
                $output['finishOptions'][0] = $finish;
              }
              else {
                $output['finishOptions'][$i] = $finish;
                $i++;
              }
            }
          }
        }
      }
      if (isset($data->facetInnovation)) {
        $facetInnovaDet = explode(" ", $data->facetInnovation);
        if (($key = array_search('FltElectronicFaucet', $facetInnovaDet)) !== false) {
          unset($facetInnovaDet[$key]);
        }
        foreach ($facetInnovaDet as $facetInnovation) {
          $facetInnovationDetails[] = get_taxonomy_details($facetInnovation, 'facetinnovation', 'all');
        }
        $output['facetinnovation'] = $facetInnovationDetails;
      }
      if (isset($data->values->WatersenseCert) && $data->values->WatersenseCert == 'T') {
        if (isset($data->values->WatersenseLogo)) {
          $watersense_logo = $data->values->WatersenseLogo;
          $facetFeatureResult = get_taxonomy_using_name($watersense_logo, 'watersense');
          $output['watersense'] = $facetFeatureResult;
          $path_info = pathinfo($watersense_logo);
          $watersense_label = basename($watersense_logo, '.' . $path_info['extension']);
          $watersense_label = str_replace('_', ' ', $watersense_label);
          $output['watersense_alt'] = $watersense_label;
        }
      }
      $facetspecialFeaturesResult = [];
      if (!empty($data->facetFeature)) {
        $facetFeatures = explode(" ", $data->facetFeature);
        foreach ($facetFeatures as $facetfeaturesname) {
          if ($facetfeaturesname != "WatersenseCert") {
            $facetspecialFeaturesResult[] = get_taxonomy_details($facetfeaturesname, 'facetfeature', 'all');
          }
        }
        $output['facetspecialfeatures'] = $facetspecialFeaturesResult;
      }
      if (count($data->configOptions) > 0) {
        $features = [];
        $rough_Valve = [];
        $features_details = [];
        $handle_Accent = [];
        $configurationParts = [];
        $configPartsDetails = [];
        $roughValveDetails = [];
        $handleAccentDetails = [];
        $rough_Valve = [];
        $basepart = [];

        if ($propertyAccessor->isReadable($data, 'configOptions')) {
          $configOptions = $propertyAccessor->getValue($data, 'configOptions');
          $baseCnt = 0;
          foreach ($configOptions as $key => $value) {
            $label = $propertyAccessor->getValue($value, 'optionLabel');
            $type = $propertyAccessor->getValue($value, 'optionType');
            $optionProducts = $propertyAccessor->getValue($value, 'optionProducts');

            if ((strpos($label, 'Default') === FALSE) && (strpos($label, 'Featured') === FALSE) && (stripos($label, 'Rough_Valve') === FALSE) && $type != 'alternative') {
              foreach ($optionProducts as $handleAccent) {
                $getModelNumber = $propertyAccessor->getValue($handleAccent, 'modelNumber');
                $handleAccentDetails[$getModelNumber] = $this->objecttoArray($propertyAccessor, $handleAccent, 0);
              }
              // sorting based on the model number
              ksort($handleAccentDetails);
              $baseCnt++;
            }
            if (strpos($label, 'Default_Rough_Valve') !== FALSE) {
              foreach ($optionProducts as $key => $value) {
                $getModelNumber = $propertyAccessor->getValue($value, 'modelNumber');
                $rough_Valve[$getModelNumber] = $this->objecttoArray($propertyAccessor, $value);
              }
              $baseCnt++;
            }
            if ($this->begnWith($label, 'Rough_Valve')) {
              $optionProducts = $propertyAccessor->getValue($value, 'optionProducts');
              foreach ($optionProducts as $key => $value1) {
                $getModelNumber1 = $propertyAccessor->getValue($value1, 'modelNumber');
                $rough_Valve[$getModelNumber1] = $this->objecttoArray($propertyAccessor, $value1);
                $configurationOptions = $propertyAccessor->getValue($value1, 'configurationOptions');
                if (count($configurationOptions) > 0) {
                  foreach ($propertyAccessor->getValue($value1, 'configurationOptions[0].optionProducts') as $key => $value) {
                    $getModelNumber = $propertyAccessor->getValue($value1, 'modelNumber');
                    $configprice = $propertyAccessor->getValue($value1, 'listPrice');
                    $rough_Valve[$getModelNumber] = $this->objecttoArray($propertyAccessor, $value, $configprice, $getModelNumber);
                  }
                }
              }
              $baseCnt++;
            }

            if ((strpos($label, 'Default') === FALSE) && (strpos($label, 'Featured') === FALSE) && $type != 'alternative') {
              foreach ($optionProducts as $key => $value) {
                $name = $propertyAccessor->getValue($value, 'modelNumber');
                $dashModelNumberEnd = "--" . $name;
                $dashModelNumber = "--" . $name . "--";
                if ((strpos($ModelNumber, $dashModelNumber) !== FALSE) || $this->endsWith($ModelNumber, $dashModelNumberEnd)) {
                  $configPartsDetails[$name] = $this->objecttoArray($propertyAccessor, $value);
                }
              }
            }

          }
          if ($baseCnt > 0) {
            $baseModel = $propertyAccessor->getValue($data, 'values.BaseModel');
            if ($modelName == $baseModel) {
              $basepart[] = [
                "image" => $propertyAccessor->getValue($data, 'heroImage'),
                "modelnumber" => $modelName,
                "description" => $description,
                "listPrice" => $listPrice,
              ];
            }
            else {
              $basepart = $this->basePartDetail($propertyAccessor->getValue($data, 'values.ModelNumber'));
            }
          }
        }
        // feature products
        $fp = $this->deltacache->deltaGetCache('PDP_FP' . $sku);
        if(!$fp){
          foreach ($data->configOptions as $feature) {
            if ($feature->optionType == "addon") {
              foreach ($feature->optionProducts as $featureProduct) {
                $features_details[] =  $featureProduct->modelNumber;
              }
              $features[] = [
                'featureOptionType' => $feature->optionType,
                'featureProductdetails' => $features_details,
              ];
            }
          }
          $this->deltacache->deltaSetCache('PDP_FP' . $sku, $features_details);
        }
        $output['baseparts'] = $basepart;
        $output['features'] = $features_details;
        $output['rough_valve'] = $rough_Valve;
        $output['handle_Accent'] = $handleAccentDetails;
        $output['configurationParts'] = $configPartsDetails;
      }

      if (!empty($data->productReferences)) {
        $partlist_coll = [];
        $partlist = $data->productReferences;
        $appendModelNumber = '';
        foreach ($partlist as $productpartlist) {
          $modelNumber = $propertyAccessor->getValue($productpartlist, 'modelNumber');
          $appendModelNumber .= '&or_name=' . $modelNumber;
        }
        $partsAPI = $partsApi . $appendModelNumber;
        // cache
        $data_cached = $this->deltacache->deltaGetCache('PDP_PARTS' . $sku);
        if (!$data_cached) {
          $parts_cache['property'] = $propertyAccessor;
          $parts_cache['api'] = $partsAPI;
          $data_cached = $this->deltacache->deltaSetCache('PDP_PARTS' . $sku, $parts_cache);
        }
        $output['partlist_title'] = "Part List";
        $output['partlist_products'] = TRUE;
      }
      $avaliable_to_trade = isset($data->values->availableToTrade) ? $data->values->availableToTrade : "";
      if ($avaliable_to_trade == "true") {
        $output['avaliable_to_trade'] = $data->values->availableToTrade;
      }
      if (!empty($data->values->SalientBullet03)) {
        $salientBullet_03 = $data->values->SalientBullet03;
        $output['salientBullet03'] = $salientBullet_03;
      }
      if (!empty($data->values->SalientBullet02)) {
        $salientBullet_02 = $data->values->SalientBullet02;
        $output['salientBullet02'] = $salientBullet_02;
      }
      if (!empty($data->values->SalientBullet01)) {
        $salientBullet_01 = $data->values->SalientBullet01;
        $output['salientBullet01'] = $salientBullet_01;
      }
      if (!empty($data->values->RepairPart) && $data->values->RepairPart == "T") {
        $output['product_list'] = "T";
        $output['product_list_title'] = "Product List";
      }

      $output['B2C'] = $data->values->B2C;
      if (property_exists($data->values, 'Prop65WarningLabel') && $data->values->Prop65WarningLabel == 'T') {
        if (property_exists($data->values, 'Prop65Warning') && ($data->values->Prop65Warning != 'null' && $data->values->Prop65Warning != '')) {
          $protocols = ['http', 'https', 'mail'];
          $attributes = ['target' => '_blank'];
          $prop_help_text = $this->makeClickableLinks($data->values->Prop65Warning, $protocols, $attributes);
        }
        if ((!empty($prop_help_tex_notes)) && (!empty($prop_help_tex_notes_replace))) {
          $prop_help_text = str_replace($prop_help_tex_notes, $prop_help_tex_notes_replace, $prop_help_text);
        }
        $output['Prop65WarningLabel'] = $prop_warning_msg;
        $output['Prop65HelpMessage'] = $prop_help_text;
      }
      if (!empty($data->values->NonExclusiveDeepLinking) && $data->values->NonExclusiveDeepLinking == "T") {
        $output['NonExclusiveDeepLinking'] = $data->values->NonExclusiveDeepLinking;
      }
      // Product Coming Soon and Discontinued logic.
      $materialstaus = isset($data->values->MaterialStatus) ? $data->values->MaterialStatus : "";
      $discontinued = $data->values->discontinued;
      $discontinue_text = isset($data->values->discontinueText) ? $data->values->discontinueText : '';
      $coming_soon_text = isset($data->values->comingSoonText) ? $data->values->comingSoonText : '';
      $collectionName = '#';
      if(property_exists($data->values, 'Collection') && $data->values->Collection !== 'Delta'){
        $getCollNameRaw = html_entity_decode($data->values->Collection, ENT_QUOTES);
        $getCollNameRaw = strtolower($getCollNameRaw);
        $getCollNameRaw = strip_tags($getCollNameRaw);
        $getCollNameRaw = str_replace([' '], ['-'], $getCollNameRaw);
        $collectionName = str_replace(['™','®'], [''], $getCollNameRaw);
        $collectionName = str_replace('collectionName-', $collectionName .'-' , $getCollectionName);
      }
      //if collection not present in local term then collection link will not display
      if(isset($data->values->viewAllCollectionLink) && $data->values->viewAllCollectionLink == '#'){
        $collectionName = '#';
      }
      $output['viewAllCollectionLink'] = $collectionName;
      $commingSoon = FALSE;
      $ribbonVal = $data->values->WebExclusiveCustomerItem;
      $availableToOrderDate = isset($data->values->AvailableToOrderDate) ? $data->values->AvailableToOrderDate : '';
      $availableToShipDate = isset($data->values->AvailableToShipDate) ? $data->values->AvailableToShipDate : '';
      $orderDate = strtotime($availableToOrderDate);
      $shipDate = strtotime($availableToShipDate);
      $currentDate = strtotime(date('Y-m-d', time()));
      if (empty($orderDate) || empty($shipDate)) {
        $shipDate = $currentDate - (60 * 60 * 24);
      }
      $output['ownersLink'] = TRUE;
      $recommendedlist_path = [];
      if ($discontinued) {
        $ribbonText = $discontinue_text;
        $ribboncolor = "gray-30";
        $recommendedreplacement = isset($data->values->ObsoleteReplaceItemNum) ? $data->values->ObsoleteReplaceItemNum : "";
        if ($recommendedreplacement != "") {
          $possibilities = $prop_config->get('replacement_possibilities');
          $seperator = str_replace(explode(',', $possibilities), '$', $recommendedreplacement);
          $recommandedrepalce = explode('$', $seperator);
          $rpSkus = array_map('trim', $recommandedrepalce);
          $rpProducts = self::getProductInfoMultiple($rpSkus);
          $output['recommendedReplacementNum'] = $rpProducts;
        }
      }
      else {
        if ($currentDate < $shipDate) {
          if ($currentDate > $orderDate) {
            $commingSoon = TRUE;
            $ribbonText = $coming_soon_text;
            $ribboncolor = "delta-red";
            $output['ownersLink'] = "";
          }
        }
      }
      if (!empty($ribbonVal)) {
        if ($ribbonVal != 'NA' &&
          $ribbonVal != 'OEM' &&
          $ribbonVal != 'Retail' &&
          $ribbonVal != 'Warranty' &&
          $ribbonVal != 'Internal Use Only' &&
          $discontinued != TRUE &&
          $commingSoon != TRUE
        ) {
          $ribbonText = "Only at " . str_replace("_", " and ", $ribbonVal);
          $ribboncolor = "gray-30";
        }
      }
      if($isRecomProduct){
        $ribbonText = "Recertified";
        $ribboncolor = "gray-30 banner-recertified";
      }
      $output['modelNumber'] = $data->values->ModelNumber;
      $output['heroImage'] = $data->heroImage;
      $output['localDealerButtonText'] = $localDealerButtonText;
      $output['shopOnlineButtonText'] = $shopOnlineButtonText;
      $output['priceSpiderDeepLink'] = $data->values->PriceSpiderDeepLink;
      $output['faucetDirectPartsDeeplink'] = $data->values->FaucetDirectPartsDeeplink;
      $output['categories'] = $data->categories;
      $output['discontinued'] = $discontinued;
      $disclaimerConfig = $prop_config->get('disclaimer_category');
      $disclaimer = FALSE;
      $productFlowrate = 0;
      if (!empty($disclaimerConfig)) {
        $disclaimerText = explode(',', $disclaimerConfig);
        foreach ($disclaimerText as $disclaimerItem) {
          $disclaimerRaw = explode('|', $disclaimerItem);
          $disclaimerCategory = array_shift($disclaimerRaw);
          if (count($disclaimerRaw)) {
            $disclaimerValue = $disclaimerRaw[count($disclaimerRaw) - 1];
          }
          $productFlowrate = isset($data->values->FlowRateCert) ? (float) $data->values->FlowRateCert : 0;
          foreach ($output['categories'] as $itemCategory) {
            if (strpos($itemCategory, $disclaimerCategory) !== FALSE
              && $productFlowrate > $disclaimerValue) {
              $disclaimer = TRUE;
              break;
            }
          }
          if ($disclaimer == TRUE) {
            break;
          }
        }
      }
      $output['disclaimer'] = $disclaimer;
      $disclaimerText = $prop_config->get('disclaimer_message');
      $disclaimerLink = $prop_config->get('disclaimer_link');
      $output['disclaimerText'] = $disclaimerText;
      $output['disclaimerLink'] = $disclaimerLink;
      // Ribbon Text.
      if (isset($ribbonText)) {
        $output['ribbonText'] = $ribbonText;
        $output['ribboncolor'] = $ribboncolor;
      }
      $faturesBenefitsResult = self::getProductFeature($data);
      if (!empty($faturesBenefitsResult)) {
        $featuresTranslated = $this->deltaservice->translationsCode($faturesBenefitsResult);
        $output['faturesBenefits'] = $featuresTranslated;
      }
      if (!empty($data->facetFeature)) {
      }
      $productDetails = self::getProductDetails($data);
      if (!empty($productDetails)) {
        $output['productDetails'] = $productDetails['#items'];
      }
      $productCertification = self::getProductCertification($data);
      if (!empty($productCertification)) {
        $output['productCertification'] = $productCertification;
      }
      if (isset($data->heroImageSmall)) {
        $output['assets']['images']['url'] = $data->heroImage;
        $heroImageString = explode("/", $data->heroImage);
        $heroImageFilename = $heroImageString[count($heroImageString) - 1];
        $output['assets']['images']['heroImageFileName'] = $heroImageFilename;
      }
      if (count($data->assets) > 0) {
        foreach ($data->assets as $assets) {
          if (!empty($assets->smallUrl) && ($assets->type == 'InContextShot')) {
            $assets->mediaType = 'image';
            $output['assets']['images'][] = $assets;
          }
          if (!empty($assets->smallUrl) && ($assets->type == 'SecondaryOnWhiteShots')) {
            $assets->mediaType = 'image';
            $output['assets']['images'][] = $assets;
          }
          if (!empty($assets->url) && ($assets->type == 'Video')) {
            $assets->mediaType = 'video';
            $output['assets']['images'][] = $assets;
          }
          if ($assets->type == 'MandI') {
            $output['assets']['installation'][] = $assets;
            $output['assets']['installation']['label'] = $prop_config->get('maintenance_installation_sheet_label');
          }
          if ($assets->type == 'SpecSheet') {
            $output['assets']['specsheet'][] = $assets;
            $output['assets']['specsheet']['label'] = $prop_config->get('technical_specs_sheet_label');
          }
          if ($assets->type == 'PartsDiagram') {
            $output['assets']['diagram'][] = $assets;
            $output['assets']['diagram']['label'] = $prop_config->get('parts_diagram_sheet_label');
          }
          if (!empty($assets->url) && ($assets->type == 'CAD')) {
            $cadFilename = $assets->filename;
            $cadArray = explode(".", $cadFilename);
            $cadFileExtension = end($cadArray);
            $cadAssetDetails[] = [
              'cadFileExtension' => '.' . strtoupper($cadFileExtension),
              'filename' => $cadFilename,
              'fileUrl' => $assets->url,
            ];
            $output['assets']['cadAssets'] = $cadAssetDetails;
          }
        }
      }
      $output['values'] = $data->values;

      if ($data->facetHoles == 'NA' || $data->facetHoles == 'na') {
        $holesforInstallation = '';
      }
      else {
        $holesforInstallation = get_taxonomy_using_name($data->facetHoles, 'facetholes', 'both');
      }
      $output['holes4installation'] = $holesforInstallation;
      // Breadcrumb
      $breadcrumb = self::create_breadcrumb($baseCategory, $data->values->Series);
      if (!empty($breadcrumb['back_to_results'])) {
        $output['back_to_results'] = $breadcrumb['back_to_results'];
      }
      else {
        $output['breadcrumb'] = $breadcrumb;
      }

      $request = \Drupal::request();
      $referer = $request->headers->get('referer');

      $backtoresults = [];
      if (strpos($referer, 'search-results') !== false) {
        $backtoresults['back_to_results'] = [
          'label' => 'Back to search results',
          'title' => $output['modelNumber'],
          'path' => $referer,
        ];
        $output['back_to_results'] = $backtoresults;
      }

      // Product References.
      $productType = (!empty($baseCategory)) ? str_replace('room', '', $baseCategory) : 'bath';
      $type_cache = $this->deltacache->deltaGetCache('PDP_RP' . $sku);
      if (!$type_cache) {
        $type_cache['collection'] = !empty($data->values->Collection) ? $data->values->Collection : $data->values->defaultCollection;
        $type_cache['producttype'] = $productType;
        $type_cache['viewAllCollectionLink'] = $output['viewAllCollectionLink'];
        $this->deltacache->deltaSetCache('PDP_RP' . $sku, $type_cache);
      }

      $gtmCollectionName = (!empty($collection) )? $collection : 'Delta - No Collection';
      // GTM dataLayer variables
      $gtm_detail[] = array(
      "brand" => strip_tags($gtmCollectionName),
      "category" => strip_tags($gtm_categories),
      "id" => $output['modelNumber'],
      "name" => strip_tags($description),
      "price" => '$'.$output['listPrice'],
      "variant" => strip_tags($output['finish'])
      );
      $build['gtm_detail'] = json_encode($gtm_detail);
      if(is_object($data) && property_exists($data, 'defaultModelNumber')){
        $getBaseModelNo = $data->defaultModelNumber;
        $output['defaultModelNumber'] = $data->defaultModelNumber;
      }
      //Start - BMSmith External JS
      $bimsmithResponse = ['status' => FALSE ];
      if(strlen(trim($bimSmithAPI)) > 0){
        $getBaseModelNo = 0;
        if(is_object($data) && property_exists($data, 'defaultModelNumber')){
          $getBaseModelNo = $data->defaultModelNumber;
        }
        if($getBaseModelNo == 0){
          if(is_array($output) && array_key_exists('values',$output)){
            if(is_countable($output['values']) && count($output['values']) > 0){
              $getBMNObj = $output['values'];
              if(is_object($getBMNObj) && property_exists($getBMNObj, 'BaseModel')){
                $getBaseModelNo = $getBMNObj->BaseModel;
              }
            }
          }
        }
        $bimSmithAPI = str_replace('{bmSKU}', $getBaseModelNo, $bimSmithAPI);
        $bimsmithResponse = $this->deltaservice->getBimsmithData($bimSmithAPI);
      }
      $output['bimsmith'] = $bimsmithResponse;
      //End - BMSmith External JS
      $build['output'] = [
        '#theme' => 'commerce_product',
        '#data' => $output,
      ];
    }

    return $build;
  }
  //Remove all model numbers from the finishOptions data object except current pdp model number.
  //Logc For:Recommerce only
  public function getOnlyRecertFinishes($modelNumber, &$data){
    if((!empty($data)) && property_exists($data, 'finishOptions')){
      $finishOptions = $data->finishOptions;
      $key = array_search($modelNumber, array_column($finishOptions, 'modelNumber'));
      if(!empty($key)) {
        $data->finishOptions = [$finishOptions[$key]];
      }
    }
  }

  /**
   * Fetch the product url by using product sku.
   */
  public function getProductUrl($sku) {
    if (!empty($sku)) {
      $products = $this->service->productExists($sku);
      $variation =(is_array($products) && count($products) > 0) ? key($products) : '';
      if (!empty($variation) && is_numeric($variation)) {
        return $this->aliasManager->getAliasByPath(
          '/product/' . $variation
        );
      }
      else {
        return $this->aliasManager->getAliasByPath(
          '/product-detail/' . $sku
        );
      }
    }
    else {
      return "#";
    }
  }

  /**
   * Get the facet feature values.
   */
  public function getProductFeature($data) {
    $faturesBenefits = [];
    $featuresBenefitsResult = [];
    foreach ($data->values as $key => $value) {
      if (preg_match('/^BrandBaseProdBullet/', $key, $matches)) {
        $bulletkeys = preg_split('/^BrandBaseProdBullet/', $key);
        $featuresBenefitsResult[$bulletkeys[1]] = $value;
      }
      ksort($featuresBenefitsResult);
    }
    return $featuresBenefitsResult;
  }

  /**
   * Get the taxanomy Image Path values.
   */
  public function getImagePath($facetFeatures) {
    $term_obj = $this->entity->getStorage('taxonomy_term')
      ->loadByProperties(['name' => $facetFeatures]);
    $term_id = array_keys($term_obj);
    $final_term = array_pop($term_id);
    $file = $term_obj[$final_term]->get('field_facet_icon')->entity;
    $watersensecert_label = "";
    $watersensecert_label = $term_obj[$final_term]->get('field_facet_label')->value;
    $watersense_image = "";
    $fileUri = $file->getFileUri();
    $watersense_image = file_create_url($fileUri);
    $facetFeatureResult = [];
    $facetFeatureResult = [
      "watersenselogo" => $watersense_image,
      "watersenselabel" => $watersensecert_label,
    ];
    return $facetFeatureResult;
  }

  /**
     * Turn all URLs in clickable links.
     *
     * @param string $value
     * @param array  $protocols  http/https, ftp, mail, twitter
     * @param array  $attributes
     * @return string
     */
    public function makeClickableLinks($value, $protocols = array('http', 'mail'), array $attributes = array())
    {
        // Link attributes
        $attr = '';
        foreach ($attributes as $key => $val) {
            $attr .= ' ' . $key . '="' . htmlentities($val) . '"';
        }

        $links = array();

        // Extract existing links and tags
        $value = preg_replace_callback('~(<a .*?>.*?</a>|<.*?>)~i', function ($match) use (&$links) { return '<' . array_push($links, $match[1]) . '>'; }, $value);

        // Extract text links for each protocol
        foreach ((array)$protocols as $protocol) {
            switch ($protocol) {
                case 'http':
                case 'https':
                  $value = preg_replace_callback('~(?:(https?)://([^\s<]+)|(www\.[^\s<]+?\.[^\s<]+))(?<![\.,:])~i', function ($match) use ($protocol, &$links, $attr) { if ($match[1]) $protocol = $match[1]; $link = $match[2] ?: $match[3]; return '<' . array_push($links, "<a $attr href=\"$protocol://$link\">$link</a>") . '>'; }, $value);
                  break;
                case 'mail':
                  $value = preg_replace_callback('~([^\s<]+?@[^\s<]+?\.[^\s<]+)(?<![\.,:])~', function ($match) use (&$links, $attr) { return '<' . array_push($links, "<a $attr href=\"mailto:{$match[1]}\">{$match[1]}</a>") . '>'; }, $value);
                  break;
                default:
                  $value = preg_replace_callback('~' . preg_quote($protocol, '~') . '://([^\s<]+?)(?<![\.,:])~i', function ($match) use ($protocol, &$links, $attr) { return '<' . array_push($links, "<a $attr href=\"$protocol://{$match[1]}\">{$match[1]}</a>") . '>'; }, $value);
                  break;
            }
        }

        // Insert all link
        return preg_replace_callback('/<(\d+)>/', function ($match) use (&$links) { return $links[$match[1] - 1]; }, $value);
    }


  /**
   * Get the facet feature values.
   */
  public function getFacetFeature($facetFeatures) {
    $faturesBenefitsValues = [];
    $facetFeatureResult = '';
    $vid = strtolower('facetFeature');
    $terms = $this->entity->getStorage('taxonomy_term')
      ->loadTree($vid, 0, 1, TRUE);;
    foreach ($terms as $term) {
      $termName = $term->getName();
      $iconUri = '';
      if ($term->hasField('field_facet_label')) {
        $labelVal = $term->get('field_facet_label')->getValue();
      }
      if ($term->hasField('field_facet_icon')) {
        $iconVal = $term->get('field_facet_icon')->getValue();
        // $iconvalfileuri = $term->get('field_facet_icon')->entity->uri->value;
      }
      $label = (!empty($labelVal)) ? $labelVal[0]['value'] : $termName;
      if (!empty($iconVal)) {
      }
      $faturesBenefits[$termName] = [
        '#theme' => 'icon_text',
        '#data' => ['text' => $label, 'icon' => $iconUri],
      ];
    }
    foreach ($facetFeatures as $facetFeature) {
      if (!empty($faturesBenefits[$facetFeature])) {
        $faturesBenefitsValues[] = $faturesBenefits[$facetFeature];
      }
    }
    if (count($faturesBenefitsValues) > 0) {
      $facetFeatureResult = [
        '#theme' => 'item_list',
        '#items' => $faturesBenefitsValues,
        '#attributes' => [
          'class' => [
            'facet--feature list-unstyled',
          ],
        ],
      ];
    }
    return $facetFeatureResult;
  }

  /**
   * Get the product details.
   */
  public function getProductDetails($data) {
    $propconfig = $this->configFactory->get('product_details.settings');
    $spray_settings_label = $propconfig->get('spray_settings_label');
    $FltAdjustableFullBody = $propconfig->get('FltAdjustableFullBody');
    $FltAdjH2OkineticSpray = $propconfig->get('FltAdjH2OkineticSpray');
    $FltH2OkineticSpray = $propconfig->get('FltH2OkineticSpray');
    $FltH2OkineticSpraywMassage = $propconfig->get('FltH2OkineticSpraywMassage');
    $FltCrossingFull = $propconfig->get('FltCrossingFull');
    $FltCrossingFullSpywMssg = $propconfig->get('FltCrossingFullSpywMssg');
    $FltFullBody = $propconfig->get('FltFullBody');
    $FltFullSpywMssg = $propconfig->get('FltFullSpywMssg');
    $FltFastMssg = $propconfig->get('FltFastMssg');
    $FltMassaging = $propconfig->get('FltMassaging');
    $FltSlowMssg = $propconfig->get('FltSlowMssg');
    $FltPause = $propconfig->get('FltPause');
    $FltHighEfficiency = $propconfig->get('FltHighEfficiency');
    $FltSoftRain = $propconfig->get('FltSoftRain');
    $FltSoftDrench = $propconfig->get('FltSoftDrench');
    $FltSoftDrenchwFull = $propconfig->get('FltSoftDrenchwFull');
    $FltSoftDrenchwMssg = $propconfig->get('FltSoftDrenchwMssg');
    $FLTDrenchingMist = $propconfig->get('FLTDrenchingMist');
    $FltSoftFull = $propconfig->get('FltSoftFull');
    $FltShampooRinsing = $propconfig->get('FltShampooRinsing');
    $FltShampooRinsingwMssg = $propconfig->get('FltShampooRinsingwMssg');
    $escutcheonlabel = $propconfig->get('escutcheon_label');
    $flowratelabel = $propconfig->get('flow_rate_label');
    $spoutheightlabel = $propconfig->get('spout_height_label');
    $spoutlengthlabel = $propconfig->get('spout_length_label');
    $decktooutletlabel = $propconfig->get('deck_to_outlet_label');
    $fittingstypelabel = $propconfig->get('fittings_type_label');
    $draintypelabel = $propconfig->get('drain_type_label');
    $deckthicknesslabel = $propconfig->get('deck_thickness_label');
    $supplylineslabel = $propconfig->get('supply_lines_label');
    $valvetypelabel = $propconfig->get('valve_type_label');
    $values = $data->values;
    $productDetailsValues = [];
    $productDetailsResult = '';
    $spraysettings = "";
    if (!empty($values->PBOptionalEscutcheon) && $values->PBOptionalEscutcheon != "F") {
      $productDetailsValues['escutcheon']['label'] = $escutcheonlabel;
      $productDetailsValues['escutcheon']['value'] = $values->PBOptionalEscutcheon;
    }
    if (!empty($values->FlowRate)) {
      $productDetailsValues['flowrate']['label'] = $flowratelabel;
      $productDetailsValues['flowrate']['value'] = $values->FlowRate;
    }
    $heightText = "Over All Height";
    if (!empty($values->PBSpoutTotalHeight)) {
      $productDetailsValues['spout_height']['label'] = $spoutheightlabel;
      $productDetailsValues['spout_height']['value'] = $values->PBSpoutTotalHeight.'"';
    }
    if (!empty($values->PBSpoutLength)) {
      $productDetailsValues['spout_length']['label'] = $spoutlengthlabel;
      $productDetailsValues['spout_length']['value'] = $values->PBSpoutLength.'"';
    }
    if (!empty($values->PBSpoutHeightDecktoAerator)) {
      $productDetailsValues['deck_to_outlet']['label'] = $decktooutletlabel;
      $productDetailsValues['deck_to_outlet']['value'] = $values->PBSpoutHeightDecktoAerator.'"';
    }
    if (!empty($values->ConnectionType) && $values->ConnectionType != "NA") {
      $productDetailsValues['fittings_type']['label'] = $fittingstypelabel;
      $productDetailsValues['fittings_type']['value'] = $values->ConnectionType;
    }
    if (!empty($values->DrainType) && $values->ConnectionType != "NA") {
      $productDetailsValues['drain_type']['label'] = $draintypelabel;
      $productDetailsValues['drain_type']['value'] = $values->DrainType;
    }
    if (!empty($values->PBMaxDeckThickness_decimal)) {
      $productDetailsValues['deck_thickness']['label'] = $deckthicknesslabel;
      $productDetailsValues['deck_thickness']['value'] = $values->PBMaxDeckThickness_decimal.'"';
    }
    if (!empty($values->PBSupplyLines) && $values->PBSupplyLines != "NA") {
      $productDetailsValues['supply_lines']['label'] = $supplylineslabel;
      $productDetailsValues['supply_lines']['value'] = $values->PBSupplyLines;
    }
    if (!empty($values->ValveType) && $values->ValveType != "NA") {
      $productDetailsValues['valve']['label'] = $valvetypelabel;
      $productDetailsValues['valve']['value'] = $values->ValveType;
    }
    if (!empty($values->FltAdjustableFullBody) && $values->FltAdjustableFullBody != "F") {
      $spraysettings .= $FltAdjustableFullBody . ",";
    }
    if (!empty($values->FltAdjH2OkineticSpray) && $values->FltAdjH2OkineticSpray != "F") {
      $spraysettings .= $FltAdjH2OkineticSpray . ",";
    }
    if (!empty($values->FltH2OkineticSpray) && $values->FltH2OkineticSpray != "F") {
      $spraysettings .= $FltH2OkineticSpray . ",";
    }
    if (!empty($values->FltH2OkineticSpraywMassage) && $values->FltH2OkineticSpraywMassage != "F") {
      $spraysettings .= $FltH2OkineticSpraywMassage . ",";
    }
    if (!empty($values->FltCrossingFull) && $values->FltCrossingFull != "F") {
      $spraysettings .= $FltCrossingFull . ",";
    }
    if (!empty($values->FltCrossingFullSpywMssg) && $values->FltCrossingFullSpywMssg != "F") {
      $spraysettings .= $FltCrossingFullSpywMssg . ",";
    }
    if (!empty($values->FltFullBody) && $values->FltFullBody != "F") {
      $spraysettings .= $FltFullBody . ",";
    }
    if (!empty($values->FltFullSpywMssg) && $values->FltFullSpywMssg != "F") {
      $spraysettings .= $FltFullSpywMssg . ",";
    }
    if (!empty($values->FltFastMssg) && $values->FltFastMssg != "F") {
      $spraysettings .= $FltFastMssg . ",";
    }
    if (!empty($values->FltMassaging) && $values->FltMassaging != "F") {
      $spraysettings .= $FltMassaging . ",";
    }
    if (!empty($values->FltSlowMssg) && $values->FltSlowMssg != "F") {
      $spraysettings .= $FltSlowMssg . ",";
    }
    if (!empty($values->FltHighEfficiency) && $values->FltHighEfficiency != "F") {
      $spraysettings .= $FltHighEfficiency . ",";
    }
    if (!empty($values->FltPause) && $values->FltPause != "F") {
      $spraysettings .= $FltPause . ",";
    }
    if (!empty($values->FltSoftRain) && $values->FltSoftRain != "F") {
      $spraysettings .= $FltSoftRain . ",";
    }
    if (!empty($values->FltSoftDrench) && $values->FltSoftDrench != "F") {
      $spraysettings .= $FltSoftDrench . ",";
    }
    if (!empty($values->FltSoftDrenchwFull) && $values->FltSoftDrenchwFull != "F") {
      $spraysettings .= $FltSoftDrenchwFull . ",";
    }
    if (!empty($values->FltSoftDrenchwMssg) && $values->FltSoftDrenchwMssg != "F") {
      $spraysettings .= $FltSoftDrenchwMssg . ",";
    }
    if (!empty($values->FLTDrenchingMist) && $values->FLTDrenchingMist != "F") {
      $spraysettings .= $FLTDrenchingMist . ",";
    }
    if (!empty($values->FltSoftFull) && $values->FltSoftFull != "F") {
      $spraysettings .= $FltSoftFull . ",";
    }
    if (!empty($values->FltShampooRinsing) && $values->FltShampooRinsing != "F") {
      $spraysettings .= $FltShampooRinsing . ",";
    }
    if (!empty($values->FltShampooRinsingwMssg) && $values->FltShampooRinsingwMssg != "F") {
      $spraysettings .= $FltShampooRinsingwMssg . ",";
    }
    if (!empty($spraysettings)) {
      $productDetailsValues['spary']['label'] = $spray_settings_label;
      $productDetailsValues['spary']['value'] = rtrim($spraysettings, ",");
    }
    // Tall High and Pull Out.
    foreach ($data->categories as $categories) {
      if (strpos($categories, 'Tall/High-Arc')) {
        $productDetailsValues['tall_high']['label'] = 'Tall/High Arc';
        $productDetailsValues['tall_high']['value'] = 'Yes';
      }
      if (strpos($categories, 'Pull-Outs')) {
        $productDetailsValues['pull_out_down']['label'] = 'Pull-out/Pull-down';
        $productDetailsValues['pull_out_down']['value'] = 'Yes';
      }
    }
    if (count($productDetailsValues) > 0) {
      $productDetailsResult = [
        '#theme' => 'item_list',
        '#items' => $productDetailsValues,
        '#attributes' => [
        ],
      ];
    }
    return $productDetailsResult;
  }

  /**
   * Get the product certification.
   */
  public function getProductCertification($data) {
    $values = (array) $data->values;
    $productCertResult = '';
    $modulePath = drupal_get_path('module', 'product_details');
    $productCertValues = [];
    if (array_key_exists('CSACert', $values) && $values['CSACert'] == 'T' && $values['WatersenseCert'] == 'T') {
      $productCertValues[] = [
        '#theme' => 'icon_text',
        '#data' => [
          'text' => '',
          'icon' => '/' . $modulePath . '/assets/csaws.png',
        ],
      ];
    }
    if (array_key_exists('IAPMOCert', $values) && $values['IAPMOCert'] == 'T' && $values['WatersenseCert'] == 'T') {
      $productCertValues[] = [
        '#theme' => 'icon_text',
        '#data' => [
          'text' => '',
          'icon' => '/' . $modulePath . '/assets/iapmows.png',
        ],
      ];
    }
    if (count($productCertValues) > 0) {
      $productCertResult = [
        '#theme' => 'item_list',
        '#items' => $productCertValues,
        '#attributes' => [
          'class' => [
            'product--certification list-unstyled',
          ],
        ],
      ];
    }
    return $productCertResult;
  }

  /**
   * Get the product reference.
   */
  public function getProductReference($collection, $productType = 'kitchen') {
    $sync_config = $this->configFactory->get('delta_services.deltaservicesconfig');
    $deltaConfig = $this->configFactory->get('sync.api_settings');
    $domain = $sync_config->get('api_base_path');
    $collectionUrl = $domain . $deltaConfig->get('rest_api_settings.product_collections_api');
    $collectionUrl .= "?" . http_build_query(['name' => trim($collection)]);
    $method = 'GET';
    $headers = add_headers(FALSE);
    $response = $this->deltaservice->apiCall($collectionUrl, $method, $headers, '', 'Collection');
    if ($response['status'] == 'SUCCESS') {
      $data = json_decode($response['response']);
      $output = reset($data);
      $recommendedSkusText = ($productType == 'bath') ? 'recommendedBathSuite' : 'recommendedKitchenSuite';
      if((is_object($output)) && property_exists($output, $recommendedSkusText)){
        $recommendedSkus = $output->$recommendedSkusText;
        if (isset($recommendedSkus) && count($recommendedSkus) > 0) {
          return self::getMultipleProducts($recommendedSkus);
        }
      }
    }
    return FALSE;
  }

  /**
   * Get the multiple products.
   */
  public function getMultipleProducts($recommendedSkus) {
    $sync_config = $this->configFactory->get('delta_services.deltaservicesconfig');
    $deltaConfig = $this->configFactory->get('sync.api_settings');
    $domain = $sync_config->get('api_base_path');
    $method = 'GET';
    $headers = add_headers(FALSE);
    $productSkus = '';
    foreach ($recommendedSkus as $recommendedSku) {
      $productSkus .= "&" . http_build_query(['name' => trim($recommendedSku)]);
    }
    $referenceUrl = $domain . $deltaConfig->get('rest_api_settings.load_multiple_products');
    $referenceUrl .= "?" . $productSkus;
    $response = $this->deltaservice->apiCall($referenceUrl, $method, $headers, '', 'Reference');
    if ($response['status'] == 'SUCCESS') {
      $data = json_decode($response['response']);
      $referenceContents = $data->content;
      $result = [];
      foreach ($referenceContents as $referenceContent) {
        $collection = (property_exists($referenceContent->values, 'Collection')) ? $referenceContent->values->Collection: '';
        $categories = $referenceContent->categories;
        $gtm_categories = '';
        if(!empty($categories)) {
          foreach ($categories as $urlalias) {
            $urlalias_seperate = explode("_", $urlalias);
            if ($urlalias_seperate[0] == "Delta") {
              $gtm_categories = str_replace("_", "/", $urlalias);
              break;
            }
          }
        }
        $productId = $this->syncProduct($referenceContent->name);
        $productObject = $this->entity
          ->getStorage('commerce_product')
          ->load($productId);
        $refurbishedProduct = 0;
        if($productObject){
          $refurbishedProduct = ($productObject->field_refurbished_product->value)? $productObject->field_refurbished_product->value : 0;
          $showSalePrice = FALSE;
          $salePrice = 0;
          if(is_array($productObject->getVariationIds())){
            $variation = $productObject->getVariationIds()[0];
            $product_variation = $this->entity->getStorage('commerce_product_variation')->load($variation);
            $list_price = $product_variation->get('list_price')->getValue()[0]['number'];
            $price = $product_variation->get('price')->getValue()[0]['number'];
            if($list_price <> $price && $list_price != 0){
              $salePrice = number_format($list_price, 2);
              $showSalePrice = TRUE;
            }
          }
        }
        $getFinishOptFromRespArr = $this->setFinishOptions($referenceContent);
        $result[] = [
          'finish' => property_exists($referenceContent->values, 'Finish') ? $referenceContent->values->Finish : '',
          'defaultFacetFinish' => property_exists($referenceContent, 'facetFinish') ? $referenceContent->facetFinish : '',
          'finishOptions' => $getFinishOptFromRespArr,
          'gtm_categories' => $gtm_categories,
          'productUrl' => self::getProductUrl($referenceContent->name),
          'description' => $referenceContent->description,
          'image' => $referenceContent->heroImageSmall,
          'type' => $collection,
          'modelNumber' => $referenceContent->name,
          'listPrice' => isset($referenceContent->values->ListPrice) ? $referenceContent->values->ListPrice : '',
          'discountPrice' => $salePrice,
          'showSalePrice' => $showSalePrice,
          'refurbishedProduct' => $refurbishedProduct
        ];
      }
      return $result;
    }
  }

  public function setFinishOptions($referenceContent){
    $getFinishOptFromRespArr = [];
    if(is_object($referenceContent) && property_exists($referenceContent,'finishOptions')){
      $getFinishOptFromRespObj = $referenceContent->finishOptions;
      $getFinishOptFromRespArr = json_decode(json_encode($getFinishOptFromRespObj), TRUE);
      //Lets remove internation product with -IN keyword
      array_walk($getFinishOptFromRespArr,array($this, 'removeProduct'));
      $getFinishOptFromRespArr = array_filter($getFinishOptFromRespArr);
      //lets copy the facetFinish data into facetFinishIcon.
      array_walk($getFinishOptFromRespArr,array($this, 'mirrorFinishKey'));
      //lets update finish icon into facetFinishIcon key
      array_walk($getFinishOptFromRespArr,array($this, 'updateFinishPath'));
    }
    return $getFinishOptFromRespArr;
  }

  // This will remove the international productions from the arrau, if prod contains -INT.
  public function removeProduct(&$value,$key){
    //Remove product from array - when product SKU contains -IN String
    if(array_key_exists('modelNumber', $value)){
      if(strpos($value['modelNumber'], '-IN') !== FALSE){
       $value = [];
      }
    }
    //Remove product from array - when product last string is -R
    if(array_key_exists('modelNumber', $value)){
      if(substr($value['modelNumber'], -2) === '-R'){
       $value = [];
      }
    }
    //Remove product from array - when availableToOrderDate > current timestamp
    if(array_key_exists('availableToOrderDate', $value)){
      $milliseconds = intval(microtime(true) * 1000);
      if($value['availableToOrderDate'] > $milliseconds){
       $value = [];
      }
    }
    //Remove product when stock type is Obsolete and Use Up
    $prop_config = $this->configFactory->get('product_details.settings');
    $finish_stocking_type = $prop_config->get('finish_stocking_type');
    $stockingType = explode(",", $finish_stocking_type);
    if(array_key_exists('stockingType', $value)){
      if(in_array($value['stockingType'], $stockingType) || strlen($value['stockingType']) == 0){
       $value = [];
      }
    }
  }
  // Duplicate the facetFinish value into facetFinishIcon
  public function mirrorFinishKey(&$value,$key) {
    if(array_key_exists('facetFinish', $value)){
      $value['facetFinishIcon'] = $value['facetFinish'];
    }
  }
  // Do reverse lookup, add icon path in to the facetFinishIcon
  public function updateFinishPath(&$value,$key) {
     array_walk($value,array($this, 'getFinishIcon'));
  }

  public function getFinishIcon(&$value,$key) {
    if(isset($key) && $key == 'facetFinishIcon'){
      $finishImages = getFacetFinish('facetfinish');
      $value = $finishImages[$value];
    }
  }

  /**
   * To Get Image URLS.
   */
  public function getImageUrls($content = NULL) {
    $imageUrls = [];
    if (!empty($content['assets'])) {
      $product_assets_arr = $content['assets'];
      if (count($content['assets'])) {
        foreach ($product_assets_arr as $image_assets) {
          // If ($image_assets['type'] == 'InContextShot'
          // || $image_assets->type == 'Video') {.
          $imageUrls[]['value'] = $image_assets['url'];
          // }
        }
      }
    }
    return $imageUrls;
  }

  /**
   * Checks for a SKU and create if product not available.
   */
  public function syncProduct($sku, $Apidata = FALSE) {
    $productId = '';
    if ($Apidata === TRUE) {
      $config = $this->configFactory->get('delta_services.deltaservicesconfig');
      $delta_config = $this->configFactory->get('sync.api_settings');
      $siteName = $delta_config->get('rest_api_settings.term_filter');
      $productContentApi = $delta_config->get('rest_api_settings.product_by_sku');
      $productContentApi = str_replace("{productSku}", $sku, $productContentApi);
      $basePath = $config->get('api_base_path');
      $composedUrl = $basePath . $productContentApi;
      $result = $this->deltaservice->apiCall($composedUrl, 'GET', [], [], 'PRODUCT_DETAIL');
      $content = Json::decode($result['response']);
      return $content;
    }
    else {
      $productExist = $this->service->productExists($sku);
      if (is_array($productExist) && empty(key($productExist))) {
        $config = $this->configFactory->get('delta_services.deltaservicesconfig');
        $delta_config = $this->configFactory->get('sync.api_settings');
        $siteName = $delta_config->get('rest_api_settings.term_filter');
        $productContentApi = $delta_config->get('rest_api_settings.product_by_sku');
        $productContentApi = str_replace("{productSku}", $sku, $productContentApi);
        $basePath = $config->get('api_base_path');
        $composedUrl = $basePath . $productContentApi;

        $result = $this->deltaservice->apiCall($composedUrl, 'GET', [], [], 'PRODUCT_DETAIL');
        $content = Json::decode($result['response']);
        if (is_array($content)) {
          $productId = $this->service->createProduct($content, $siteName);
        }
      }
      else {
        $productId = key($productExist);
      }
      return $productId;
    }
  }

  /**
   * To get product details.
   */
  public function getProductDetail($sku) {
    $productId = $this->syncProduct($sku);
    $route_name = 'entity.commerce_product.canonical';
    $route_parameters = [
      'commerce_product' => $productId,
    ];
    if (!empty($productId)) {
      $productdata = Product::load($productId);
      $prdDetails = $productdata->toArray();

      if(!empty($prdDetails['field_category'][0]['value']) && $prdDetails['field_category'][0]['value'] == 'parts' || $prdDetails['field_category'][0]['value'] == 'commercial' ) {
        $variations = ProductVariation::load($prdDetails['variations'][0]['target_id']);
      $path = "/parts-product-detail?modelNumber=".$variations->sku->value;
      }
      else {
        $path = Url::fromRoute(
          $route_name,
          $route_parameters
        )->toString();
      }
      $response = new RedirectResponse($path);
      return $response->send();
    }
    else {
      throw new NotFoundHttpException();
    }
  }

  /**
   * To get product details from API.
   *
   * @param array $sku
   *
   * @return array
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getProductInfoMultiple($sku = []) {
    $returnArray = $excludedArray = $widget = [];
    foreach ($sku as $item) {
      //check if exist in cache
      $checkCache = $this->pdpCacheSlug . $item;
      $cachedResult = $this->deltacache->deltaGetCache($checkCache);
      if ($cachedResult) {
        $parseData = $this->buildProductWidget(
          $cachedResult
        );
        if(is_array($parseData) && array_key_exists('finishOptions',  $parseData)){
          $productFinishObj = new \stdClass();
          $productFinishObj->finishOptions = (object) $parseData['finishOptions'];
          $parseData['finishOptions'] = $this->setFinishOptions($productFinishObj);
        }
        $returnArray[$item] = $parseData;
      }
      else {

        // check if product exists, else populate sku to get API info.
        $productExist = $this->service->productExists($item);
        if ((is_array($productExist)) && !empty(key($productExist))) {
          $productVariation = current($productExist);

          $parseData = $this->buildProductWidget(
            $productVariation,
            FALSE
          );
          $productDetails = $this->productBaseDetails($item);
          $brand = '';
          if(is_object($productDetails->values) && property_exists($productDetails->values, 'Brand')){
            $brand = $productDetails->values->Brand;
          }

          if(is_array($productDetails->collections) && count($productDetails->collections)){
            $brand = $productDetails->collections[0];
          }
          $variant = property_exists($productDetails, 'facetFinish')  ? $productDetails->facetFinish : "";
          $finish =  property_exists($productDetails->values, 'Finish') ? $productDetails->values->Finish : '';
          $parseData['brand'] = $brand;
          $parseData['variant'] = $variant;
          $parseData['finish'] = $finish;
          $parseData['defaultFacetFinish'] = $variant;
          $parseData['finishOptions'] = $this->setFinishOptions($productDetails);
          $returnArray[$item] = $parseData;
        }
        else {
          $excludedArray[] = $item;
        }
      }
    }

    $sync = [];
    //get excluded products API info
    if (count($excludedArray) > 0) {
      $sync = $this->syncMultipleSku($excludedArray);
      $widget = array_merge($returnArray, $sync);
      return $widget;
    }
    return $returnArray;
  }

  /**
   * To get product details from API.
   *
   * @param array $sku
   *
   * @return array
   */
  public function syncMultipleSku($sku = []) {
    $query = '';
    $productData = [];
    foreach ($sku as $model) {
      $query .= "&" . http_build_query(['name' => trim($model)]);
    }
    $sync_config = $this->configFactory
      ->get('delta_services.deltaservicesconfig');
    $deltaConfig = $this->configFactory
      ->get('sync.api_settings');
    $siteName = $deltaConfig->get('rest_api_settings.term_filter');
    $domain = $sync_config->get('api_base_path');
    $pdpUrl = $domain . $deltaConfig->get('rest_api_settings.product_by_sku');

    $api = $domain . $deltaConfig->get('rest_api_settings.load_multiple_products');
    $api .= '?';
    $endpoint = $api . $query;
    $response = $this->deltaservice->apiCall(
      $endpoint,
      'GET',
      [],
      [],
      "syncMultipleSku"
    );

    if ($response['status'] == 'SUCCESS' && !empty($response['response'])) {
      $result = json_decode($response['response']);
      $productItem = $result->content;
      if (is_array($productItem)) {

        foreach ($productItem as $item) {
          $sku = $item->name;
          // cache individual
          $setcache = $this->pdpCacheSlug . $sku;
          $this->deltacache->deltaSetCache($setcache, $item);
          // Create Commerce product
          $decoded = (json_decode(json_encode($item), TRUE));
          $this->service->createProduct($decoded, $siteName);
          \Drupal::logger('outlet-parse-new-product')->notice(json_encode($item));
          $recertifiedBanner = $bannerModifier = '';
          $recertifiedPrd = $this->isRecertifiedPrd($item->categories);
          if($recertifiedPrd == 1) {
            $recertifiedBanner = "Recertified";
            $bannerModifier = "gray-30 banner-recertified";
          }
          $title = $collection = '';
          $title = $item->description;
          $collection = $item->values->Collection ?? $item->values->defaultCollection;
          $image = $item->heroImage;
          $price = $item->values->ListPrice;
          $pdpPath = Url::fromRoute(
            'product_details.product_detail',
            [
              'sku' => $sku,
            ]
          )->toString();
          $pdpLink = $this->getProductUrl($sku);
          $productFinish = '';
          $finishOptions = [];
          if((!empty($item)) && property_exists($item, 'finishOptions')){
            $finishOptions = $this->setFinishOptions($item);
          }
          $defaultFacetFinish = '';
          if((!empty($item)) && property_exists($item, 'facetFinish')){
              $defaultFacetFinish = $item->facetFinish;
          }

          if((!empty($item)) && property_exists($item, 'values')){
            if((is_object($item->values)) && property_exists($item->values, 'Finish')){
              $productFinish = $item->values->Finish;
            }
          }

          $productData[$sku] = [
            'collection_name' => $collection,
            'list_price' => "$" . $price,
            'strikePrice' => "",
            'product_tile_flag_text' => $recertifiedBanner,
            'product_tile_flag_modifiers' => [$bannerModifier],
            'refurbishedProduct' => $recertifiedPrd,
            'model_image' => str_replace('/md/', '/sm/', $image),
            'model_number' => $sku,
            'description' => $title,
            'view_details_link' => $pdpPath,
            'view_docs_link' => $pdpPath. '#specsTab',
            'pdp_path' => $pdpLink,
            'finish' => $productFinish,
            'finishOptions' => $finishOptions,
            'defaultFacetFinish' => $defaultFacetFinish,
          ];
        }
      }
    }
    return $productData;
  }

  /**
   * @param $categories
   *
   * @return 1 or 0
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function isRecertifiedPrd($categories = []) {
    $sitefromcategory = [];
    $basecategory = [];
    $recertifiedurl = [];
    $recertifiedProduct = 0;
    foreach ($categories as $urlalias) {
      $urlalias_seperate = explode("_", $urlalias);
      $sitefromcategory[] = $urlalias_seperate[0];
      if ($urlalias_seperate[0] == "Delta") {
        $basecategory[] = strtolower($urlalias_seperate[1]);
      }
      $recertifiedurl[] = strtolower($urlalias_seperate[2]);
    }
    if(in_array('recommerce', $basecategory)) {
      $recertifiedProduct = 1;
    }
    return $recertifiedProduct;
  }

  /**
   * @param $data
   * @param bool $fromApi
   *
   * @return array
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function buildProductWidget($data, $fromApi = TRUE) {
    $collection = $sku = $price = $title = $image = $gtm_categories = '';
    $discount_price = $recertifiedBanner = $bannerModifier = "";
    $refurbishedProduct = 0;
    $productData = [];
    $productFinish = '';
    $finishOptions = [];
    $defaultFacetFinish = '';
    $skulistPrice = '';
    if ($fromApi) {
      $title = $data->description;
      if (property_exists($data->values, 'Collection')) {
        $collection = $data->values->Collection;
      }
      else {
        $collection = $data->values->defaultCollection;
      }
      $sku = $data->name;
      $image = $data->heroImage;
      $price = $data->values->ListPrice;

      $refurbishedProduct = $this->isRecertifiedPrd($data->categories);
      if($refurbishedProduct == 1) {
        $recertifiedBanner = "Recertified";
        $bannerModifier = "gray-30 banner-recertified";

        $productExist = $this->service->productExists($sku);
        if (!empty(key($productExist))) {
          $productVariation = current($productExist);
          $skulistPrice = $productVariation->get('list_price')
              ->getValue()[0]['number'];
        }
      }

      if((!empty($data)) && property_exists($data, 'finishOptions')){
        $finishOptions = $data->finishOptions;
      }
      if((!empty($data)) && property_exists($data, 'facetFinish')){
          $defaultFacetFinish = $data->facetFinish;
      }
      if((!empty($data)) && property_exists($data, 'values')){
        if((is_object($data->values)) && property_exists($data->values, 'Finish')){
          $productFinish = $data->values->Finish;
        }
      }
    }
    else {
      $sku = $data->get('sku')->getValue()[0]['value'];
      $price = $data->get('price')->getValue()[0]['number'];
      $price = (float) $price;
      $productId = $data->get('product_id')
        ->getValue()[0]['target_id'];
      $skulistPrice = $data->get('list_price')
          ->getValue()[0]['number'];
      $productObject = $this->entity->getStorage('commerce_product')
        ->load((int) $productId);

      if($productObject) {
        if ($productObject->hasField('field_collection')) {
          $collection_entity_array = $productObject->get('field_collection')->referencedEntities();
          $collection_entity = (isset($collection_entity_array[0]))? $collection_entity_array[0] : NULL;
          if ($collection_entity) {
            $collection = $collection_entity->get('name')->value;
          }
        }
        if($productObject->hasField('field_product_response_data')){
          $prodRespObj =  $productObject->get('field_product_response_data')->value;
          if(!empty($prodRespObj)){
            $responseDataObj = json_decode($prodRespObj);

            //if above collection var is empty, then get it from product response.
            if($collection == "" && is_object($responseDataObj) && property_exists($responseDataObj->values, 'Collection')){
              $collection =  $responseDataObj->values->Collection;
            }
            //if collection property is empty check defaultCollection
            if($collection == "" && is_object($responseDataObj) && property_exists($responseDataObj->values, 'defaultCollection')){
              $collection =  $responseDataObj->values->defaultCollection;
            }

            if((!empty($responseDataObj)) && property_exists($responseDataObj, 'finishOptions')){
              $finishOptions = $responseDataObj->finishOptions;
            }

            if((!empty($responseDataObj)) && property_exists($responseDataObj, 'facetFinish')){
                $defaultFacetFinish = $responseDataObj->facetFinish;
            }

            if((!empty($responseDataObj)) && property_exists($responseDataObj, 'values')){
              if((is_object($responseDataObj->values)) && property_exists($responseDataObj->values, 'Finish')){
                $productFinish = $responseDataObj->values->Finish;
              }
            }
          }
        }

        $response_data = ($productObject->field_product_response_data->value) ? json_decode($productObject->field_product_response_data->value) : '';
        $categories = property_exists($response_data, 'categories') ? $response_data->categories : '';
        if(!empty($categories)) {
          foreach ($categories as $urlalias) {
            $urlalias_seperate = explode("_", $urlalias);
            if ($urlalias_seperate[0] == "Delta") {
              $gtm_categories = str_replace("_", "/", $urlalias);
              break;
            }
          }
        }
        // Re-certified products List price field and banner fields.
        $refurbishedProduct = ($productObject->field_refurbished_product->value)? $productObject->field_refurbished_product->value : '';
        if($refurbishedProduct == 1) {
          $recertifiedBanner = ($refurbishedProduct ) ? "Recertified" : "";
          $bannerModifier = ($refurbishedProduct) ? "gray-30 banner-recertified" : "";
        }

        $title = ($productObject->title->value)? $productObject->title->value : '';
        $image = ($productObject->field_image_url->value)? str_replace('/md/', '/sm/', $productObject->field_image_url->value) : '';
      }
    }
    $pdpPath = self::getProductUrl($sku);
    $productData = [
      'description' => $title,
      'list_price' => "$" . $price,
      'strikePrice' => $skulistPrice,
      'product_tile_flag_text' => $recertifiedBanner,
      'product_tile_flag_modifiers' => [$bannerModifier],
      'refurbishedProduct' => $refurbishedProduct,
      'model_image' => $image,
      'model_number' => $sku,
      'collection_name' => !empty($collection) ? $collection : '',
      'view_details_link' => $pdpPath,
      'view_docs_link' => $pdpPath. '#specsTab',
      'gtm_categories' => $gtm_categories,
      'finish' => $productFinish,
      'finishOptions' => $finishOptions,
      'defaultFacetFinish' => $defaultFacetFinish,
    ];
    return $productData;
  }

  /**
   * Deletes all entries in a given entity,type.
   *
   * @param $entity
   * @param $bundle
   *
   * @return bool
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  function deleteAllInEntity() {
    $pid = $this->entity->getStorage("commerce_product")->getQuery()
      ->condition('type', "default")
      ->execute();
    $storage_handler = $this->entity
      ->getStorage("commerce_product");
    $entities = $storage_handler->loadMultiple($pid);
    $storage_handler->delete($entities);
    return TRUE;
  }


  /**
   * Deletes all entries in a given entity,type and without category.
   *
   * @param $entity
   * @param $bundle
   *
   * @return bool
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  function deleteAllwithoutCategories() {
    $pid = \Drupal::entityQuery("commerce_product")
      ->condition('type', "default")
      ->notExists('field_category')
      ->execute();
    $storage_handler = $this->entity
      ->getStorage("commerce_product");
    $entities = $storage_handler->loadMultiple($pid);
    $storage_handler->delete($entities);
    return $pid;
  }

  /**
   * Create breadcrumb based on the referer
   */
  public function create_breadcrumb($category = '', $series = '') {
    $output = $component = [];
    $link = '';
    $request = Request::createFromGlobals();
    $headers = $request->server->getHeaders();
    $referer_url = isset($headers['REFERER']) ? $headers['REFERER'] : '';
    if ($referer_url) {
      $path_alias = parse_url($referer_url);
      $args = explode('/', $path_alias['path']);
      if (stripos($path_alias['path'], 'search')) {
        // from search results
        parse_str($path_alias['query'], $params);
        $output['back_to_results'] = [
          'label' => 'Back to search results',
          'title' => $params['title'],
          'path' => $path_alias['path'],
        ];
      }
      elseif (stripos($path_alias['path'], 'collections')) {
        // from collections page
        foreach ($args as $arg) {
          if (!empty($arg)) {
            $label = ucwords(str_replace('-', ' ', $arg));
            $link = $link . '/' . $arg;
            $component['text'] = $label;
            $component['url'] = $link;
            $output[] = $component;
          }
        }
      }
      elseif ((stripos($path_alias['path'], 'kitchen') || stripos($path_alias['path'], 'bathroom')) && !stripos($path_alias['path'], 'product')) {
        // from category page
        foreach ($args as $arg) {
          if (!empty($arg)) {
            $label = ucwords(str_replace('-', ' ', $arg));
            $link = $link . '/' . $arg;
            $component['text'] = $label;
            $component['url'] = $link;
            $output[] = $component;
          }
        }
      }
    }
    return $output;
  }

  /**
   * Get Product List Price.
   *
   * @param $sku
   *
   * @return $list price
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function getListPrice($sku) {
    if(!empty($sku)) {
      return $this->service->ProductListPrice($sku);
    }
  }
}

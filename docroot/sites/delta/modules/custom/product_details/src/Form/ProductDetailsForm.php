<?php

namespace Drupal\product_details\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\ConfigFormBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\file\Entity\File;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Configure settings for this site.
 */
class ProductDetailsForm extends ConfigFormBase implements ContainerInjectionInterface {

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The Config storage.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configfactory;

    /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entityManager) {
    $this->configfactory = $config_factory;
    $this->entityTypeManager = $entityManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
          $container->get('config.factory'),
          $container->get('entity_type.manager')
      );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'product_details_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['product_details.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('product_details.settings');

    $form['product_details_config'] = [
      '#type' => 'details',
      '#title' => $this->t('Product Details Configurations'),
      '#open' => TRUE,
      '#weight' => 2,
    ];
    $form['product_details_config']['prop_65_warning_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Prop 65 Warning Label'),
      '#default_value' => $config->get('rest_api_settings.prop_65_warning_label'),
      '#required' => TRUE,
      '#size' => 100,
    ];
    $form['product_details_config']['prop_65_help_text'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Prop 65 Help Text'),
      '#default_value' => $config->get('rest_api_settings.prop_65_help_text'),
      '#required' => TRUE,
      '#size' => 100,
    ];
    $form['product_details_config']['prop_65_help_text_notes'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Prop65Warning - Last sentense'),
      '#description' => $this->t('Prop65Warning - Update Last sentense in this text box and this is case sensitive'),
      '#default_value' => $config->get('rest_api_settings.prop_65_help_text_notes'),
      '#size' => 100,
    ];
    $form['product_details_config']['prop_65_help_text_notes_replace'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Prop65Warning - Last sentense - Insert basic html tags'),
      '#description' => $this->t('Copy of Prop65Warning - Last sentense with minimal html appending to bring better screen design.'),
      '#default_value' => $config->get('rest_api_settings.prop_65_help_text_notes_replace'),
      '#size' => 100,
    ];
    $form['product_details_config']['discontinue_ribbontext'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Discontinue Product Ribbon Text'),
      '#default_value' => $config->get('rest_api_settings.discontinue_ribbontext'),
      '#required' => TRUE,
      '#size' => 100,
    ];
    $form['product_details_config']['comming_soon_ribbontext'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Coming Soon Ribbon Text'),
      '#default_value' => $config->get('rest_api_settings.comming_soon_ribbontext'),
      '#required' => TRUE,
      '#size' => 100,
    ];
    $form['product_details_config']['finish_stocking_type'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Finish Stocking Type'),
      '#default_value' => $config->get('finish_stocking_type'),
      '#required' => TRUE,
      '#size' => 100,
    ];
    $form['product_details_config']['handle_or_accent'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Handle Or Accent Label'),
      '#default_value' => $config->get('handle_or_accent'),
      '#required' => TRUE,
      '#size' => 100,
    ];
    $form['product_details_config']['rough_valve'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Rough Valve Label'),
      '#default_value' => $config->get('rough_valve'),
      '#required' => TRUE,
      '#size' => 100,
    ];
    $form['product_details_config']['base_part'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Base Part Label'),
      '#default_value' => $config->get('base_part'),
      '#required' => TRUE,
      '#size' => 100,
    ];
    $form['product_details_config']['configuration_part'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Configurable Parts Label'),
      '#default_value' => $config->get('configuration_part'),
      '#required' => TRUE,
      '#size' => 100,
    ];
    $form['product_details_config']['recommended_replacement'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Recommended Replacement Label'),
      '#default_value' => $config->get('recommended_replacement'),
      '#required' => TRUE,
      '#size' => 100,
    ];
    $form['product_details_config']['disclaimer_category'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Disclaimer Category and FlowRateCert Value'),
      '#default_value' => $config->get('disclaimer_category'),
      '#required' => TRUE,
    ];
    $form['product_details_config']['disclaimer_message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Disclaimer Message'),
      '#default_value' => $config->get('disclaimer_message'),
      '#required' => TRUE,
    ];
    $form['product_details_config']['disclaimer_link'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Disclaimer Link'),
      '#default_value' => $config->get('disclaimer_link'),
      '#required' => TRUE,
    ];    
    $form['product_details_config']['pdp_video_thumb'] = [
       '#type' => 'managed_file',
       '#title' => $this->t('PDP - Video Image'),
       '#default_value' => $config->get('pdp_video_thumb'),
       '#size' => 100,
       '#upload_validators' => [
         'file_validate_is_image' => [],
         'file_validate_extensions' => ['gif png jpg jpeg'],
         'file_validate_size' => [25600000]
       ],
       '#upload_location' => 'public://product',
       '#attributes' => ['class' => ['photo-upload']],
     ]; 
     $form['product_details_config']['replacement_possibilities'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Replacement possibilities'),
      '#description' => 'Enter comma seperated possibilities for the replacement seperation in API. e.g. w/,+,& ',
      '#default_value' => $config->get('replacement_possibilities'),
      '#required' => TRUE,
    ];
    $form['pdp_wheretobuy'] = [
      '#type' => 'details',
      '#title' => $this->t('PDP Where To Buy Configurations'),
      '#open' => TRUE,
      '#weight' => 2,
    ];
    $form['pdp_wheretobuy']['local_dealer_button_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Local Dealers Button Text'),
      '#default_value' => $config->get('local_dealer_button_text'),
      '#required' => TRUE,
      '#size' => 100,
    ];
    $form['pdp_wheretobuy']['shop_online_button_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Shop Online Button Text'),
      '#default_value' => $config->get('shop_online_button_text'),
      '#required' => TRUE,
      '#size' => 50,
      '#maxlength' => 50,
    ];
    $form['pdp_wheretobuy']['ps-account'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Price Spider Account ID'),
      '#default_value' => $config->get('ps-account'),
      '#required' => TRUE,
      '#size' => 10,
      '#maxlength' => 10,
    ];
    $form['pdp_wheretobuy']['ps-config'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Price Spider Config'),
      '#default_value' => $config->get('ps-config'),
      '#required' => TRUE,
      '#size' => 50,
      '#maxlength' => 50,
    ];
    $form['pdp_wheretobuy']['ps-country'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Price Spider Country'),
      '#default_value' => $config->get('ps-country') ?? 'US',
      '#required' => TRUE,
      '#size' => 10,
      '#maxlength' => 10,
    ];
    $form['pdp_wheretobuy']['ps-language'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Price Spider Language'),
      '#default_value' => $config->get('ps-language') ?? 'en',
      '#required' => TRUE,
      '#size' => 10,
      '#maxlength' => 10,
    ];
    $form['pdp_wheretobuy']['find-location-url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Find Location URL'),
      '#default_value' => $config->get('find-location-url'),
      '#required' => TRUE,
      '#size' => 100,
      '#maxlength' => 100,
    ];
    $form['pdp_wheretobuy']['find-location-disclaimer'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Find Location Disclaimer'),
      '#default_value' => $config->get('find-location-disclaimer'),
      '#required' => TRUE,
      '#size' => 100,
      '#maxlength' => 100,
    ];
    $form['product_toolkit_config'] = [
      '#type' => 'details',
      '#title' => $this->t('Product Ownership Toolkit Configuration'),
      '#open' => TRUE,
      '#weight' => 2,
    ];
    $form['product_toolkit_config']['maintenance_installation_sheet_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Maintenace and Installation Sheet Label'),
      '#default_value' => $config->get('maintenance_installation_sheet_label'),
      '#required' => TRUE,
      '#size' => 100,
    ];
    $form['product_toolkit_config']['technical_specs_sheet_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Technical Specification Sheet Label'),
      '#default_value' => $config->get('technical_specs_sheet_label'),
      '#required' => TRUE,
      '#size' => 100,
    ];
    $form['product_toolkit_config']['parts_diagram_sheet_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Parts and Accessories Sheet Label'),
      '#default_value' => $config->get('parts_diagram_sheet_label'),
      '#required' => TRUE,
      '#size' => 100,
    ];
    $form['installation_information'] = [
      '#type' => 'details',
      '#title' => $this->t('Installation Information'),
      '#open' => TRUE,
      '#weight' => 2,
    ];
    $form['installation_information']['escutcheon_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Escutcheon Label'),
      '#default_value' => $config->get('escutcheon_label'),
      '#required' => TRUE,
      '#size' => 100,
    ];
    $form['installation_information']['flow_rate_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Flow Rate Label'),
      '#default_value' => $config->get('flow_rate_label'),
      '#required' => TRUE,
      '#size' => 100,
    ];
    $form['installation_information']['spout_height_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Spout Height Label'),
      '#default_value' => $config->get('spout_height_label'),
      '#required' => TRUE,
      '#size' => 100,
    ];
    $form['installation_information']['spout_length_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Spout Length Label'),
      '#default_value' => $config->get('spout_length_label'),
      '#required' => TRUE,
      '#size' => 100,
    ];
    $form['installation_information']['deck_to_outlet_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Deck To Outlet Label'),
      '#default_value' => $config->get('deck_to_outlet_label'),
      '#required' => TRUE,
      '#size' => 100,
    ];
    $form['installation_information']['fittings_type_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Fittings Type Label'),
      '#default_value' => $config->get('fittings_type_label'),
      '#required' => TRUE,
      '#size' => 100,
    ];
    $form['installation_information']['drain_type_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Drain Type Label'),
      '#default_value' => $config->get('drain_type_label'),
      '#required' => TRUE,
      '#size' => 100,
    ];
    $form['installation_information']['deck_thickness_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Deck Thickness Label'),
      '#default_value' => $config->get('deck_thickness_label'),
      '#required' => TRUE,
      '#size' => 100,
    ];
    $form['installation_information']['supply_lines_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Supply Lines Label'),
      '#default_value' => $config->get('supply_lines_label'),
      '#required' => TRUE,
      '#size' => 100,
    ];
    $form['installation_information']['valve_type_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Valve Type Label'),
      '#default_value' => $config->get('valve_type_label'),
      '#required' => TRUE,
      '#size' => 100,
    ];
    $form['installation_information']['spray_settings_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Spray Settings Label'),
      '#default_value' => $config->get('spray_settings_label'),
      '#required' => TRUE,
      '#size' => 100,
    ];
    $form['spray_settings_value'] = [
      '#type' => 'details',
      '#title' => $this->t('Sprayers Label Value'),
      '#open' => TRUE,
      '#weight' => 2,
    ];
    $form['spray_settings_value']['FltAdjustableFullBody'] = [
      '#type' => 'textfield',
      '#title' => $this->t('FltAdjustableFullBody Label'),
      '#default_value' => $config->get('FltAdjustableFullBody'),
      '#required' => TRUE,
      '#size' => 100,
    ];
    $form['spray_settings_value']['FltAdjH2OkineticSpray'] = [
      '#type' => 'textfield',
      '#title' => $this->t('FltAdjH2OkineticSpray Label'),
      '#default_value' => $config->get('FltAdjH2OkineticSpray'),
      '#required' => TRUE,
      '#size' => 100,
    ];
    $form['spray_settings_value']['FltH2OkineticSpray'] = [
      '#type' => 'textfield',
      '#title' => $this->t('FltH2OkineticSpray Label'),
      '#default_value' => $config->get('FltH2OkineticSpray'),
      '#required' => TRUE,
      '#size' => 100,
    ];
    $form['spray_settings_value']['FltH2OkineticSpraywMassage'] = [
      '#type' => 'textfield',
      '#title' => $this->t('FltH2OkineticSpraywMassage Label'),
      '#default_value' => $config->get('FltH2OkineticSpraywMassage'),
      '#required' => TRUE,
      '#size' => 100,
    ];
    $form['spray_settings_value']['FltCrossingFull'] = [
      '#type' => 'textfield',
      '#title' => $this->t('FltCrossingFull Label'),
      '#default_value' => $config->get('FltCrossingFull'),
      '#required' => TRUE,
      '#size' => 100,
    ];
    $form['spray_settings_value']['FltCrossingFullSpywMssg'] = [
      '#type' => 'textfield',
      '#title' => $this->t('FltCrossingFullSpywMssg Label'),
      '#default_value' => $config->get('FltCrossingFullSpywMssg'),
      '#required' => TRUE,
      '#size' => 100,
    ];
    $form['spray_settings_value']['FltFullBody'] = [
      '#type' => 'textfield',
      '#title' => $this->t('FltFullBody Label'),
      '#default_value' => $config->get('FltFullBody'),
      '#required' => TRUE,
      '#size' => 100,
    ];
    $form['spray_settings_value']['FltFullSpywMssg'] = [
      '#type' => 'textfield',
      '#title' => $this->t('FltFullSpywMssg Label'),
      '#default_value' => $config->get('FltFullSpywMssg'),
      '#required' => TRUE,
      '#size' => 100,
    ];
    $form['spray_settings_value']['FltFastMssg'] = [
      '#type' => 'textfield',
      '#title' => $this->t('FltFastMssg Label'),
      '#default_value' => $config->get('FltFastMssg'),
      '#required' => TRUE,
      '#size' => 100,
    ];
    $form['spray_settings_value']['FltMassaging'] = [
      '#type' => 'textfield',
      '#title' => $this->t('FltMassaging Label'),
      '#default_value' => $config->get('FltMassaging'),
      '#required' => TRUE,
      '#size' => 100,
    ];
    $form['spray_settings_value']['FltSlowMssg'] = [
      '#type' => 'textfield',
      '#title' => $this->t('FltSlowMssg Label'),
      '#default_value' => $config->get('FltSlowMssg'),
      '#required' => TRUE,
      '#size' => 100,
    ];
    $form['spray_settings_value']['FltHighEfficiency'] = [
      '#type' => 'textfield',
      '#title' => $this->t('FltHighEfficiency Label'),
      '#default_value' => $config->get('FltHighEfficiency'),
      '#required' => TRUE,
      '#size' => 100,
    ];
    $form['spray_settings_value']['FltPause'] = [
      '#type' => 'textfield',
      '#title' => $this->t('FltPause Label'),
      '#default_value' => $config->get('FltPause'),
      '#required' => TRUE,
      '#size' => 100,
    ];
    $form['spray_settings_value']['FltSoftRain'] = [
      '#type' => 'textfield',
      '#title' => $this->t('FltSoftRain Label'),
      '#default_value' => $config->get('FltSoftRain'),
      '#required' => TRUE,
      '#size' => 100,
    ];
    $form['spray_settings_value']['FltSoftDrench'] = [
      '#type' => 'textfield',
      '#title' => $this->t('FltSoftDrench Label'),
      '#default_value' => $config->get('FltSoftDrench'),
      '#required' => TRUE,
      '#size' => 100,
    ];
    $form['spray_settings_value']['FltSoftDrenchwFull'] = [
      '#type' => 'textfield',
      '#title' => $this->t('FltSoftDrenchwFull Label'),
      '#default_value' => $config->get('FltSoftDrenchwFull'),
      '#required' => TRUE,
      '#size' => 100,
    ];
    $form['spray_settings_value']['FltSoftDrenchwMssg'] = [
      '#type' => 'textfield',
      '#title' => $this->t('FltSoftDrenchwMssg Label'),
      '#default_value' => $config->get('FltSoftDrenchwMssg'),
      '#required' => TRUE,
      '#size' => 100,
    ];
    $form['spray_settings_value']['FLTDrenchingMist'] = [
      '#type' => 'textfield',
      '#title' => $this->t('FLTDrenchingMist Label'),
      '#default_value' => $config->get('FLTDrenchingMist'),
      '#required' => TRUE,
      '#size' => 100,
    ];
    $form['spray_settings_value']['FltSoftFull'] = [
      '#type' => 'textfield',
      '#title' => $this->t('FltSoftFull Label'),
      '#default_value' => $config->get('FltSoftFull'),
      '#required' => TRUE,
      '#size' => 100,
    ];
    $form['spray_settings_value']['FltShampooRinsing'] = [
      '#type' => 'textfield',
      '#title' => $this->t('FltShampooRinsing Label'),
      '#default_value' => $config->get('FltShampooRinsing'),
      '#required' => TRUE,
      '#size' => 100,
    ];
    $form['spray_settings_value']['FltShampooRinsingwMssg'] = [
      '#type' => 'textfield',
      '#title' => $this->t('FltShampooRinsingwMssg Label'),
      '#default_value' => $config->get('FltShampooRinsingwMssg'),
      '#required' => TRUE,
      '#size' => 100,
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $getUserInput = $form_state->getUserInput();
    if (!empty($getUserInput['pdp_video_thumb'])) {
      $image = $getUserInput['pdp_video_thumb'];
      if (!empty($image['fids'])) {
        $file = $this->entityTypeManager->getStorage('file')->load($image['fids']);
        $file->setPermanent();
        $file->save();
      }
    }
    $this->config('product_details.settings')
      ->set('rest_api_settings.prop_65_warning_label', $form_state->getValue('prop_65_warning_label'))
      ->set('rest_api_settings.prop_65_help_text', $form_state->getValue('prop_65_help_text'))
      ->set('rest_api_settings.prop_65_help_text_notes', $form_state->getValue('prop_65_help_text_notes'))
      ->set('rest_api_settings.prop_65_help_text_notes_replace', $form_state->getValue('prop_65_help_text_notes_replace'))
      ->set('rest_api_settings.discontinue_ribbontext', $form_state->getValue('discontinue_ribbontext'))
      ->set('rest_api_settings.comming_soon_ribbontext', $form_state->getValue('comming_soon_ribbontext'))
      ->set('local_dealer_button_text', $form_state->getValue('local_dealer_button_text'))
      ->set('finish_stocking_type' , $form_state->getValue('finish_stocking_type'))
      ->set('handle_or_accent' , $form_state->getValue('handle_or_accent'))
      ->set('rough_valve' , $form_state->getValue('rough_valve'))
      ->set('base_part' , $form_state->getValue('base_part'))
      ->set('configuration_part' , $form_state->getValue('configuration_part'))
      ->set('recommended_replacement' , $form_state->getValue('recommended_replacement'))
      ->set('disclaimer_category', $form_state->getValue('disclaimer_category'))
      ->set('disclaimer_message', $form_state->getValue('disclaimer_message'))
      ->set('disclaimer_link', $form_state->getValue('disclaimer_link'))
      ->set('replacement_possibilities', $form_state->getValue('replacement_possibilities'))
      ->set('pdp_video_thumb', $form_state->getValue('pdp_video_thumb'))      
      ->set('shop_online_button_text', $form_state->getValue('shop_online_button_text'))
      ->set('ps-account', $form_state->getValue('ps-account'))
      ->set('ps-config', $form_state->getValue('ps-config'))
      ->set('ps-country', $form_state->getValue('ps-country'))
      ->set('ps-language', $form_state->getValue('ps-language'))
      ->set('find-location-url', $form_state->getValue('find-location-url'))
      ->set('find-location-disclaimer', $form_state->getValue('find-location-disclaimer'))
      ->set('maintenance_installation_sheet_label', $form_state->getValue('maintenance_installation_sheet_label'))
      ->set('technical_specs_sheet_label', $form_state->getValue('technical_specs_sheet_label'))
      ->set('parts_diagram_sheet_label', $form_state->getValue('parts_diagram_sheet_label'))
      ->set('escutcheon_label', $form_state->getValue('escutcheon_label'))
      ->set('flow_rate_label', $form_state->getValue('flow_rate_label'))
      ->set('spout_height_label', $form_state->getValue('spout_height_label'))
      ->set('spout_length_label', $form_state->getValue('spout_length_label'))
      ->set('deck_to_outlet_label', $form_state->getValue('deck_to_outlet_label'))
      ->set('fittings_type_label', $form_state->getValue('fittings_type_label'))
      ->set('drain_type_label', $form_state->getValue('drain_type_label'))
      ->set('deck_thickness_label', $form_state->getValue('deck_thickness_label'))
      ->set('supply_lines_label', $form_state->getValue('supply_lines_label'))
      ->set('valve_type_label', $form_state->getValue('valve_type_label'))
      ->set('spray_settings_label', $form_state->getValue('spray_settings_label'))
      ->set('FltAdjustableFullBody', $form_state->getValue('FltAdjustableFullBody'))
      ->set('FltAdjH2OkineticSpray', $form_state->getValue('FltAdjH2OkineticSpray'))
      ->set('FltH2OkineticSpray', $form_state->getValue('FltH2OkineticSpray'))
      ->set('FltH2OkineticSpraywMassage', $form_state->getValue('FltH2OkineticSpraywMassage'))
      ->set('FltCrossingFull', $form_state->getValue('FltCrossingFull'))
      ->set('FltCrossingFullSpywMssg', $form_state->getValue('FltCrossingFullSpywMssg'))
      ->set('FltFullBody', $form_state->getValue('FltFullBody'))
      ->set('FltFullSpywMssg', $form_state->getValue('FltFullSpywMssg'))
      ->set('FltFastMssg', $form_state->getValue('FltFastMssg'))
      ->set('FltMassaging', $form_state->getValue('FltMassaging'))
      ->set('FltSlowMssg', $form_state->getValue('FltSlowMssg'))
      ->set('FltHighEfficiency', $form_state->getValue('FltHighEfficiency'))
      ->set('FltPause', $form_state->getValue('FltPause'))
      ->set('FltSoftRain', $form_state->getValue('FltSoftRain'))
      ->set('FltSoftDrench', $form_state->getValue('FltSoftDrench'))
      ->set('FltSoftDrenchwFull', $form_state->getValue('FltSoftDrenchwFull'))
      ->set('FltSoftDrenchwMssg', $form_state->getValue('FltSoftDrenchwMssg'))
      ->set('FLTDrenchingMist', $form_state->getValue('FLTDrenchingMist'))
      ->set('FltSoftFull', $form_state->getValue('FltSoftFull'))
      ->set('FltShampooRinsing', $form_state->getValue('FltShampooRinsing'))
      ->set('FltShampooRinsingwMssg', $form_state->getValue('FltShampooRinsingwMssg'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}

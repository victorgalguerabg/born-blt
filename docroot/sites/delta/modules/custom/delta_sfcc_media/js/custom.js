(function($, Drupal, drupalSettings) {
  Drupal.behaviors.delta_sfcc_media_popup = {
    attach: function(context, settings) {
      var formSelectors =
        "form#entity-browser-media-browser-form, form#entity-browser-image-browser-form, form#entity-browser-ckeditor-media-browser-form";
      if ($(formSelectors).length) {
        /*
          form#entity-browser-media-browser-form,
          form#entity-browser-image-browser-form,
          form#entity-browser-ckeditor-media-browser-form
        */
        if (drupalSettings.sfcc_home_page_flag !== undefined && drupalSettings.sfcc_home_page_flag ===
          'on') {
          $("form#entity-browser-media-browser-form select[name='bundle'] option[value], form#entity-browser-ckeditor-media-browser-form select[name='bundle'] option[value], form#entity-browser-image-browser-form select[name='bundle'] option[value]")
            .attr("disabled", "disabled");
          var readMediaTypeAsRaw = drupalSettings.sfcc_skip_media_type;
          var enableOptValArr = [];
          if (readMediaTypeAsRaw != "") {
            var enableOptValArrRawSpace = readMediaTypeAsRaw.split(",");
            var enableOptValArr = enableOptValArrRawSpace.maprec(String.prototype.trim)
          }
          var isObjEmpty = jQuery.isEmptyObject(enableOptValArr);
          if (isObjEmpty === false) {
            for (var i = 0; i < enableOptValArr.length; i++) {
              var getmediaType = enableOptValArr[i];
              var enableOptValmediaBrowser =
                "form#entity-browser-media-browser-form select[name='bundle'] option[value='" +
                getmediaType + "']";
              if ($(enableOptValmediaBrowser).length) {
                $(enableOptValmediaBrowser).attr("disabled", false);
              }

              var enableOptValckeditor =
                "form#entity-browser-ckeditor-media-browser-form select[name='bundle'] option[value='" +
                getmediaType + "']";
              if ($(enableOptValckeditor).length) {
                $(enableOptValckeditor).attr("disabled", false);
              }
              var enableOptValBrowser =
                "form#entity-browser-image-browser-form select[name='bundle'] option[value='" +
                getmediaType + "']";
              if ($(enableOptValBrowser).length) {
                $(enableOptValBrowser).attr("disabled", false);
              }
            }
          }
        }
      }
    }
  }
  Object.defineProperty(Array.prototype, "maprec", {
    value: function(cb) {
      return this.map(function(v) {
        return cb.call(v)
      })
    }
  })
})(jQuery, Drupal, drupalSettings);

<?php

namespace Drupal\imagepin_extend\Plugin\imagepin\Widget;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Markup;
use Drupal\imagepin\Plugin\WidgetBase;
use Drupal\node\Entity\Node;

/**
 * The Hotspot content widget plugin.
 *
 * @Widget(
 *   id = "hotspot_content",
 *   label = @Translation("Hotspot content"),
 * )
 */
class HotspotWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formNewElement(array &$form, FormStateInterface $form_state) {
    $element = [];

    // TODO Required fields currently don't work.
    // Form API documentation lacks here, again.
    $element['referred_content'] = [
      '#type' => 'entity_autocomplete',
      '#target_type' => 'node',
      '#title' => t('Hotspot content'),
      '#selection_handler' => 'default',
      '#selection_settings' => array(
        'target_bundles' => array('hotspot_content'),
      ),
    ];
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function previewContent($value) {
    $nid = $value['referred_content'];
    if(!empty($nid) && is_numeric($nid)) {
      $node = Node::load($nid);
      $textarea['#markup'] = (isset($node->body->value)) ? $node->body->value:'';
      $hero_image = $node->field_hs_hero_image->target_id ? $node->get('field_hs_hero_image')->referencedEntities()[0] : '';
      if(!empty($hero_image)){
        $hero_image_url = file_create_url($hero_image->get('uri')->value);
      }
    }
    $output = array ('hero_image' => $hero_image_url, 'body' => $textarea);
    return [
      '#theme' => 'imagepin_hotspot_content_preview',
      '#data' => $output,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function viewContent($value) {
    $nid = $value['referred_content'];
    if(!empty($nid) && is_numeric($nid)) {
      $node = Node::load($nid);
      $textarea['#markup'] = (isset($node->body->value)) ? $node->body->value:'';
      $hero_image = $node->field_hs_hero_image->target_id ? $node->get('field_hs_hero_image')->referencedEntities()[0] : '';
      if(!empty($hero_image)){
        $hero_image_url = file_create_url($hero_image->get('uri')->value);
      }
    }
    $output = array ('hero_image' => $hero_image_url, 'body' => $textarea);
    return [
      '#theme' => 'imagepin_hotspot_content_view',
      '#data' => $output,
    ];
  }

}

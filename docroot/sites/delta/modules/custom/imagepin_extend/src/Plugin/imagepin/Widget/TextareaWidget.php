<?php

namespace Drupal\imagepin_extend\Plugin\imagepin\Widget;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Markup;
use Drupal\imagepin\Plugin\WidgetBase;

/**
 * The textarea widget plugin.
 *
 * @Widget(
 *   id = "textarea",
 *   label = @Translation("Textarea"),
 * )
 */
class TextareaWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formNewElement(array &$form, FormStateInterface $form_state) {
    $element = [];

    // TODO Required fields currently don't work.
    // Form API documentation lacks here, again.
    $element['textarea'] = [
      '#type' => 'textarea',
      '#title' => t('Textarea'),
      '#description' => 'Use HTML tags in this teatarea',
      '#format' => 'full_html',
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function previewContent($value) {
    $textarea['#markup'] = (isset($value['textarea']))?$value['textarea']:'';
    $output = array ('textarea' => $textarea);
    return [
      '#theme' => 'imagepin_hotspot_preview',
      '#data' => $output,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function viewContent($value) {
    $textarea['#markup'] = (isset($value['textarea']))?$value['textarea']:'';
    $output = array ('textarea' => $textarea);
    return [
      '#theme' => 'imagepin_hotspot_view',
      '#data' => $output,
    ];
  }

}

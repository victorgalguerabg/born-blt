<?php
namespace Drupal\delta_component_formatter\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\media\Entity\Media;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\file\Entity\File;

/**
 * Configure example settings for this site.
 */
class RecommercePdpConfigForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'delta_component_formatter.recommerce_pdp_config_form';

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

   /**
   * Class constructor.
   */
  public function __construct(EntityTypeManagerInterface $entityManager) {
    $this->entityTypeManager = $entityManager;
  }

   /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
      // Load the service required to construct this class.
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'recommerce_pdp_config_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);

    $form['warranty_block'] = [
      '#type' => 'text_format',
      '#format'=> 'full_html',
      '#title' => $this->t('Warranty Block Text'),
      '#required' => TRUE,
      '#default_value' => $config->get('warranty_block')
    ];

    //$media = Media::load($id);
    $form['warranty_image'] = [
       '#type' => 'managed_file',
       '#title' => t('Warranty Image'),
       '#upload_validators' => array(
           'file_validate_extensions' => array('gif png jpg jpeg svg'),
           'file_validate_size' => array(25600000),
       ),
       '#upload_location' => 'public://recommerce/',
       '#default_value' => [$config->get('warranty_image')],
       '#required' => TRUE,
    ];
    $str2 = $str = '';


    $pdp_id = $config->get('installation_block');
    $entity = $this->entityTypeManager
    ->getStorage('block_content')
    ->load($pdp_id);
    if($pdp_id) {
      $str = "<p> <a href='/block/". $pdp_id ."'>Click Here</a> To edit this block</p>";
    }
    $form['installation_block'] = array(
      '#title' => 'Type in Block Name (PDP)',
      '#type' => 'entity_autocomplete',
      '#target_type' => 'block_content',
      '#default_value' => $entity,
      '#description' => $str
    );

    $plp_id = $config->get('plp_block');
    $plp_entity = $this->entityTypeManager
    ->getStorage('block_content')
    ->load($plp_id);
    if($plp_id) {
      $str2 = "<p>To edit this block <a href='/block/". $plp_id ."'>Click Here</a></p>";
    }
    $form['plp_block'] = array(
      '#title' => 'Type in Block Name (PLP)',
      '#type' => 'entity_autocomplete',
      '#target_type' => 'block_content',
      '#default_value' => $plp_entity,
      '#description' => $str2
    );
    $form['plp_product_list_price'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Product list price against SKU'),
      '#description' => $this->t('To store list price of a product against its sku e.g(989-AREA-DST|558.25,B2418LF-DSP|348.25,21966LF|404.50)'),
      '#default_value' => $config->get('plp_product_list_price'),
    ];
    $form['recommerce_category_list'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Recommerce Category List'),
      '#description' => $this->t('Provide list of Recommerce Category. Ex: Delta_Recommerce, Delta_Recommerce_Kitchen'),
      '#default_value' => $config->get('recommerce_category_list'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if(!empty($form_state->getValue('warranty_image')[0])) {
      $image = $form_state->getValue('warranty_image')[0];
      $file = File::load( $image );
      $file->setPermanent();
      $file->save();
    }
    // Retrieve the configuration.
    $this->configFactory->getEditable(static::SETTINGS)
      // Set the submitted configuration setting.
      ->set('installation_block', $form_state->getValue('installation_block'))
      ->set('plp_block', $form_state->getValue('plp_block'))
      ->set('warranty_block', $form_state->getValue('warranty_block')['value'])
      ->set('warranty_image', $form_state->getValue('warranty_image')[0])
      ->set('plp_product_list_price', $form_state->getValue('plp_product_list_price'))
      ->set('recommerce_category_list', $form_state->getValue('recommerce_category_list'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}

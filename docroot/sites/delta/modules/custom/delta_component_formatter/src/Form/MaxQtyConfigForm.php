<?php
namespace Drupal\delta_component_formatter\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\media\Entity\Media;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure example settings for this site.
 */
class MaxQtyConfigForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'delta_component_formatter.max_product_qty';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'recommerce_pdp_config_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);

    $form['max_qty_delta'] = [
      '#type' => 'number',
      '#title' => $this->t('Maximum Quantity Allowed (Delta)'),
      '#required' => TRUE,
      '#default_value' => $config->get('max_qty_delta')
    ];

    $form['qty_delta_error_msg'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Error Meaasge (Delta)'),
      '#required' => TRUE,
      '#default_value' => $config->get('qty_delta_error_msg')
    ];

    $form['max_qty_recommerce'] = [
      '#type' => 'number',
      '#title' => $this->t('Maximum Quantity Allowed (Delta - Recommerce)'),
      '#required' => TRUE,
      '#default_value' => $config->get('max_qty_recommerce')
    ];

    $form['qty_recommerce_error_msg'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Error Meaasge (Delta - Recommerce)'),
      '#required' => TRUE,
      '#default_value' => $config->get('qty_recommerce_error_msg')
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    // Retrieve the configuration.
    $this->configFactory->getEditable(static::SETTINGS)
      // Set the submitted configuration setting.
      ->set('max_qty_delta', $form_state->getValue('max_qty_delta'))
      ->set('max_qty_recommerce', $form_state->getValue('max_qty_recommerce'))
      ->set('qty_delta_error_msg', $form_state->getValue('qty_delta_error_msg'))
      ->set('qty_recommerce_error_msg', $form_state->getValue('qty_recommerce_error_msg'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}

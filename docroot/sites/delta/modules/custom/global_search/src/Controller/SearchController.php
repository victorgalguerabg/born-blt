<?php

namespace Drupal\global_search\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Implementation of search controller.
 */
class SearchController extends ControllerBase {

  /**
   * To get current query string.
   *
   * @var Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Implementation of constructor.
   */
  public function __construct(RequestStack $requestStack) {
    $this->requestStack = $requestStack;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
      // Load the service required to construct this class.
      $container->get('request_stack')
    );
  }

  /**
   * Implementation of search results.
   */
  public function getSearchResults() {
    $searchKey = $this->requestStack->getCurrentRequest()->get('title');
    $searchType = $this->requestStack->getCurrentRequest()->get('type');
    $searchFilter = ($this->requestStack->getCurrentRequest()->get('filter')) ? $this->requestStack->getCurrentRequest()->get('filter') : 0;
    return [
      '#theme' => 'search_results_page',
      '#data' => ['term' => $searchKey, 'filter' => $searchFilter, 'categories' => $searchType],
      '#attached' => [
        'library' =>
        [
          'global_search/searchResult',
        ],
      ],
    ];
  }

  /**
   * Implementation of support search results.
   */
  public function getSupportSearchResults() {

    return [
      '#theme' => 'support_search_results_page',
      '#attached' => [
        'library' =>
          [
            'global_search/customerSupport',
          ],
      ],
    ];
  }

}

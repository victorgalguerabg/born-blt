<?php

namespace Drupal\global_search\Controller;

use Drupal\Core\Link;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Component\Utility\Tags;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\delta_services\DeltaService;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Defines a route controller for entity autocomplete form elements.
 */
class AutocompleteController extends ControllerBase {

  /**
   * Drupal\delta_services\DeltaService definition.
   *
   * @var \Drupal\delta_services\DeltaService
   */
  protected $deltaservice;

  /**
   * Implementation of constructor.
   */
  public function __construct(DeltaService $deltaservice) {
    $this->deltaservice = $deltaservice;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
    // Load the service required to construct this class.
      $container->get('delta_services.service')
    );
  }

  /**
   * Handler for autocomplete request.
   */
  public function handleAutocomplete(Request $request) {
    $results = [];
    $output = '';
    $response = new Response();
    $inputQs = $request->query->get('q');
    //find -140 from sku and remove it
    $getLastFourChar = substr($inputQs, -4);
    if($getLastFourChar == '-140'){
      $inputQs = str_replace($getLastFourChar, '', $inputQs);
    }
    // Get the typed string from the URL, if it exists.
    $input = trim($inputQs);
    if ($input) {
      $typedString = Tags::explode($input);
      $typedString = trim(array_pop($typedString));
      if (strlen($typedString) > 1) {
        $result = self::productSearchAutocomplete($typedString);
        $totalRecords = 0;
        if (is_array($result) && (!empty($result['products']))) {
          $totalRecords = count($result['products']);
        }

        if ($totalRecords > 0) {
          $results[] = [
            'label' => 'PRODUCTS',
          ];
          foreach ($result['products'] as $product) {
            $results[] = [
              'value' => $product['description'],
              'label' => Link::fromTextAndUrl($product['productText'], Url::fromUserInput($product['url'])),
            ];
          }
          if(count($results)){
            $results = array_slice($results, 0,5);
          }
          $url = Url::fromRoute('global_search.search', ['title' => $typedString, 'type' => 'Delta_Products']);
          $viewAllLink['viewAll'] = $url;
          $link = [
            '#theme' => 'view_all_link',
            '#data' => (Array) $viewAllLink,
          ];
          $addViewAll = [
            'label' => render($link),
          ];
          $results[] = $addViewAll;
          // Build Output.
          $output = [
            '#theme' => 'search_suggestions_output',
            '#data' => $results,
          ];
        }
        $totalRecords = 0;
        if (!empty($result['parts'])) {
          $totalRecords = count($result['parts']);
        }
        if ($totalRecords > 0) {
          $resultsr[] = [
            'label' => 'REPAIR PARTS',
          ];
          foreach ($result['parts'] as $product) {
            $resultsr[] = [
              'value' => $product['description'],
              'label' => Link::fromTextAndUrl($product['productText'], Url::fromUserInput($product['url'])),
            ];
          }

          if(count($resultsr)){
            $resultsr = array_slice($resultsr, 0,5);
          }
          // Check if the file is uploaded, let combine file data in to array.
          if (is_array($resultsr) && count($resultsr)) {
            $results = array_merge($results, $resultsr);
          }


          // Add view all link.
          $repairProdUrl = Url::fromRoute('global_search.search', ['title' => $typedString, 'type' => 'Delta_Parts']);
          $repairProdViewAllLink['viewAll'] = $repairProdUrl;
          $repairLink = [
            '#theme' => 'view_all_link',
            '#data' => (Array) $repairProdViewAllLink,
          ];
          $repairProdAddViewAll = [
            'label' => render($repairLink),
          ];
          $results[] = $repairProdAddViewAll;
          // Build Output.
          $output = [
            '#theme' => 'search_suggestions_output',
            '#data' => $results,
          ];
        }
      }
    }
    return new JsonResponse($results);
  }

  /**
   * Handler for autocomplete request.
   */
  public function handleAutocompleteParts(Request $request) {
    $results = [];
    $output = '';
    $response = new Response();
    $inputQs = $request->query->get('q');
    //find -140 from sku and remove it
    $getLastFourChar = substr($inputQs, -4);
    if($getLastFourChar == '-140'){
      $inputQs = str_replace($getLastFourChar, '', $inputQs);
    }
    // Get the typed string from the URL, if it exists.
    $input = trim($inputQs);
    if ($input) {
      $typedString = Tags::explode($input);
      $typedString = trim(array_pop($typedString));
      if (strlen($typedString) > 1) {
        $result = self::productSearchAutocomplete($typedString);
        $totalRecords = 0;
        if (is_array($result) && (!empty($result['products']))) {
          $totalRecords = count($result['products']);
        }

        if ($totalRecords > 0) {
          $results['products'][] = [
            'label' => 'PRODUCTS',
            'title' => "section",
          ];
          foreach ($result['products'] as $product) {
            $results['products'][] = [
              'value' => $product['description'],
              'url' => $product['url'],
              'name' => $product['name'],
            ];
          }
          if(count($results)){
            $results = array_slice($results, 0,5);
          }
          $url = Url::fromRoute('global_search.search', ['q' => $typedString, 'type' => 'Delta_Products']);
          $viewAllLink['viewAll'] = $url;
          $link = [
            '#theme' => 'view_all_link_parts',
            '#data' => (Array) $viewAllLink,
          ];
          $addViewAll = [
            'label' => render($link),
            'title' => "viewall",
          ];
          $results['products'][] = $addViewAll;
          // Build Output.
          $output = [
            '#theme' => 'search_suggestions_output',
            '#data' => $results,
          ];
        }
        $totalRecords = 0;
        if (!empty($result['parts'])) {
          $totalRecords = count($result['parts']);
        }
        if ($totalRecords > 0) {
          $resultsr['parts'][] = [
            'label' => 'REPAIR PARTS',
            'title' => "section",
          ];
          foreach ($result['parts'] as $product) {
            $resultsr['parts'][] = [
              'value' => $product['description'],
              'url' => $product['url'],
              'name' => $product['name'],
            ];
          }

          if(count($resultsr)){
            $resultsr = array_slice($resultsr, 0,5);
          }
          // Check if the file is uploaded, let combine file data in to array.
          if (is_array($resultsr) && count($resultsr)) {
            $results = array_merge($results, $resultsr);
          }

          // Add view all link.
          $repairProdUrl = Url::fromRoute('global_search.search', ['q' => $typedString, 'type' => 'Delta_Parts']);
          $repairProdViewAllLink['viewAll'] = $repairProdUrl;
          $repairLink = [
            '#theme' => 'view_all_link_parts',
            '#data' => (Array) $repairProdViewAllLink,
          ];
          $repairProdAddViewAll = [
            'label' => render($repairLink),
            'title' => "viewall",
          ];
          $results['parts'][] = $repairProdAddViewAll;
          // Build Output.
          $output = [
            '#theme' => 'search_suggestions_output',
            '#data' => $results,
          ];
        }
      }
    }
    return new JsonResponse($results);
  }

  /**
   * Fetch the search result for auto complete.
   */
  public function productSearchAutocomplete($typedString) {
    $config = $this->config('sync.api_settings');
    $apiBaseDetails = $this->configFactory
      ->get('delta_services.deltaservicesconfig');
    $domain = $apiBaseDetails->get('api_base_path');
    $output = [];
    $method = 'GET';
    $headers = add_headers(TRUE);

    // Search Products.
    $searchProductUrl = $domain . $config->get('global_search_products');
    $searchProductUrl = str_replace("{typedString}", $typedString, $searchProductUrl);
    $response = $this->deltaservice->apiCall(
      $searchProductUrl,
      "GET",
      [],
      "",
      "Search"
    );
    if ($response['status'] == 'SUCCESS') {
      $responseJson = $this->deltaservice->excludeRecommerce($response);
      $response['status'] = 'SUCCESS';
      $response['response'] = $responseJson;
    }
    if ($response['status'] == 'SUCCESS') {
      $data = json_decode($response['response']);
      if (count($data->content) > 0) {
        foreach ($data->content as $content) {
          $output['products'][$content->name]['name'] = $content->name;
          $output['products'][$content->name]['description'] = $content->descriptionWithCollection;
          $output['products'][$content->name]['url'] = '/product-detail/' . $content->name;
          //When user entered actual sku then redirect to PDP.
          if (strtolower($output['products'][$content->name]['name']) === strtolower($typedString)) {
            $pdpUrl = $output['products'][$content->name]['url'];
            //$this->cache()->set('search_' . strtolower($typedString), $pdpUrl, $cidExpTime);
          }
          $content->description = preg_replace("#(" . $typedString . ")#i", "<b>$1</b>", $content->descriptionWithCollection);
          $link = [
            '#theme' => 'search_item_link',
            '#data' => (Array) $content,
          ];
          $output['products'][$content->name]['productText'] = render($link);
        }
      }
    }

    // Search Parts.
    $searchPartsUrl = $domain . $config->get('global_search_parts');
    $searchPartsUrl = str_replace("{typedString}", $typedString, $searchPartsUrl);
    $response = $this->deltaservice->apiCall(
      $searchPartsUrl,
      "GET",
      [],
      "",
      "Search"
    );
    if ($response['status'] == 'SUCCESS') {
      $data = json_decode($response['response']);
      if (count($data->content) > 0) {
        foreach ($data->content as $content) {
          $output['parts'][$content->name]['name'] = $content->name;
          $output['parts'][$content->name]['description'] = $content->descriptionWithCollection;
          $output['parts'][$content->name]['url'] = '/product-detail/' . $content->name;
          $content->description = preg_replace("#(" . $typedString . ")#i", "<b>$1</b>", $content->descriptionWithCollection);
          $link = [
            '#theme' => 'search_item_link',
            '#data' => (Array) $content,
          ];
          $output['parts'][$content->name]['productText'] = render($link);
        }
      }
    }
    return $output;
  }
}

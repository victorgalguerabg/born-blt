<?php

namespace Drupal\global_search\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\delta_cache\DeltaCacheService;
use Drupal\delta_services\DeltaService;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Implementation of search controller.
 */
class SearchOutputController extends ControllerBase {

  /**
   * To get current query string.
   *
   * @var Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;
  /**
   * To handle search term.
   *
   * @var searchKey
   */
  public $searchKey;
  /**
   * Flag to dedect advance or normal search.
   *
   * @var advSearchKey
   */
  public $advSearchKey;
  /**
   * To capture source key.
   *
   * @var sourceKey
   */
  public $sourceKey;
  /**
   * To handle response.
   *
   * @var response
   */
  public $response;
  /**
   * To handle page number.
   *
   * @var pageNum
   */
  public $pageNum;
  /**
   * To get current current url.
   *
   * @var getCurUrl
   */
  public $getCurUrl;
  /**
   * Drupal\delta_services\DeltaService definition.
   *
   * @var \Drupal\delta_services\DeltaService
   */
  protected $deltaservice;

  /**
   * Implementation of constructor.
   */
  public function __construct(RequestStack $requestStack,
                              DeltaService $deltaservice) {
    $this->deltaservice = $deltaservice;
    $this->requestStack = $requestStack;
    $this->getCurUrl = $this->requestStack->getCurrentRequest()->getRequestUri();
    $this->searchKey = $this->requestStack->getCurrentRequest()->get('title');
    $this->sourceKey = $this->requestStack->getCurrentRequest()->get('source');
    $this->advSearchKey = $this->requestStack->getCurrentRequest()->get('');
    $this->response = new Response();
    $this->pageNum = 0;
    if (!empty($this->requestStack->getCurrentRequest()->get('page'))) {
      $this->pageNum = $this->requestStack->getCurrentRequest()->get('page');
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
    // Load the service required to construct this class.
      $container->get('request_stack'),
      $container->get('delta_services.service')
    );
  }

  /**
   * Implementation of search output.
   */
  public function getSearchOutput() {
    //find -140 from sku and remove it
    $getLastFourChar = substr($this->searchKey, -4);
    if($getLastFourChar == '-140'){
      $this->searchKey = str_replace($getLastFourChar, '', $this->searchKey);
    }
    $request = $this->requestStack->getCurrentRequest();
    $includeObsolete = '&includeObsolete=true';
    if (isset($this->sourceKey) && $this->sourceKey === 'prof') {
      $includeObsolete = '&includeObsolete=false';
    }
    $config = $this->config('sync.api_settings');
    $apiBaseDetails = $this->configFactory
      ->get('delta_services.deltaservicesconfig');
    $domain = $apiBaseDetails->get('api_base_path');
    $prop_config = $this->configFactory->get('product_details.settings');
    $pagerSize = $config->get('product_per_page');
    $searchApiUrl = $domain . $config->get('product_search');
    $url = $searchApiUrl . $this->searchKey . $includeObsolete ;
    $url .= '&WebsiteUS=T&page=' . $this->pageNum . '&size=' . $pagerSize;
    if($request->get('facet') != ''){
      $url .= '&facetFinish=' .$request->get('facet');
    }
    if ($request->get('categories') == 'Delta_Parts' ) {
        $url .= "&categories=" . $request->get('categories');
    }
    if ($request->get('categories') == 'Delta_Products' ) {
        $url .= "&or_categories=Delta_Kitchen&or_categories=Delta_Bathroom";
    }
    if($request->get('sort') != ''){
      $url .= "&" . $request->get('sort');
    }


    $productRequest = $this->deltaservice->apiCall(
      $url, "GET", [], "", "Search"
    );

    $content = $this->deltaservice->excludeRecommerce(
      $productRequest
    );

    $this->response->setContent($content);
    $this->response->headers->set('Content-Type', 'application/json');
    return $this->response;

  }

  /**
   * Implementation of search document output.
   */
  public function getDocumentSearchOutput() {
    //find -140 from sku and remove it
    $getLastFourChar = substr($this->searchKey, -4);
    if($getLastFourChar == '-140'){
      $this->searchKey = str_replace($getLastFourChar, '', $this->searchKey);
    }
    $config = $this->config('sync.api_settings');
    $apiBaseDetails = $this->configFactory
      ->get('delta_services.deltaservicesconfig');
    $domain = $apiBaseDetails->get('api_base_path');
    $documentSearchApi = $domain . $config->get('document_search');
    $pagerSize = $config->get('document_per_page');
    $url = $documentSearchApi . $this->searchKey . '&size=' . $pagerSize;
    $url .= '&brand=delta&from=' . $this->pageNum;
    if (!empty($this->advSearchKey)) {
      $filterKey = '';
      if ($this->advSearchKey == 'ts') {
        $filterKey = '&documentType=SpecSheet';
      }
      else {
        if ($this->advSearchKey == 'mi') {
          $filterKey = '&documentType=mandi';
        }
        else {
          if ($this->advSearchKey == 'pd') {
            $filterKey = '&documentType=partsdiagram';
          }
        }
      }
      $url = $documentSearchApi . $this->searchKey . '&size=' . $pagerSize;
      $url .= $pagerSize . $filterKey . '&from=' . $this->pageNum;
      $url .=  '&brand=delta';
    }

    $documentRequest = $this->deltaservice->apiCall(
      $url,
      "GET",
      [],
      "",
      "Search"
    );
    if ($documentRequest['status'] == 'SUCCESS') {
      $this->response->setContent($documentRequest['response']);
      $this->response->headers->set('Content-Type', 'application/json');
    }
    return $this->response;

  }

}

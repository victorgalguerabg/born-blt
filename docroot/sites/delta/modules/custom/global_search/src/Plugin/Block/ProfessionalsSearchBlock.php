<?php

namespace Drupal\global_search\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Render\Renderer;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Provides my custom block.
 *
 * @Block(
 *   id = "professionals_search_block",
 *   admin_label = @Translation("Professionals Search Block"),
 *   category = @Translation("Blocks")
 * )
 */
class ProfessionalsSearchBlock extends BlockBase implements BlockPluginInterface, ContainerFactoryPluginInterface {

  /**
   * To get current query string.
   *
   * @var Symfony\Component\HttpFoundation\RequestStack
   */
  protected $request;

  /**
   * {@inheritdoc}
   */
  protected $formBuilder;

  /**
   * {@inheritdoc}
   */
  protected $renderer;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration, $plugin_id, $plugin_definition, $container->get('form_builder'), $container->get('renderer'), $container->get('request_stack')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, FormBuilderInterface $formBuilder, Renderer $renderer, RequestStack $request) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->form_builder = $formBuilder;
    $this->renderer = $renderer;
    $this->requestStack = $request;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $form = $this->form_builder->getForm('Drupal\global_search\Form\ProfessionalsSearchForm');
    return [
      '#type' => 'markup',
      '#markup' => render($form),
      '#cache' => [
        'max-age' => 0,
      ],
    ];
  }

}

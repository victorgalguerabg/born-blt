<?php

namespace Drupal\global_search\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Implementation of global search form.
 */
class GlobalSearchOnResultsForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'global_search_on_results_form';
  }

  /**
   * Implementation of global search on content region form build().
   */
  public function buildForm(array $form, FormStateInterface $form_state, $arg = NULL) {
    $searchPattern = $arg[1];
    if (count($arg) > 2) {
      $searchPattern = $arg[2];
    }
    $form = [];
    $form['name'] = [
      '#type' => 'textfield',
      '#value' => urldecode($searchPattern),
      '#attributes' => [
        'class' => [
          'text-input search-results',
        ],
      ],
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => 'search',
      '#attributes' => [
        'class' => [
          'btn btn-default search-results-submit',
        ],
      ],
    ];
    $form['#theme'] = 'search_on_results_block';
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $filter = $form_state->getUserInput();
    $filterKey = '';
    $filtersArr = [];
    $searchPattern = $filter['name'];
    $filtersArr = ['title' => $searchPattern];
    if (!empty($filter['filter_option'])) {
      $filterKey = $filter['filter_option'];
      $searchPattern = $filter['search_box'];
      $filtersArr = ['title' => $searchPattern, 'documentLookFor' => $filterKey];
    }
    $url = Url::fromRoute('global_search.search', $filtersArr);
    $form_state->setRedirectUrl($url);
  }

}

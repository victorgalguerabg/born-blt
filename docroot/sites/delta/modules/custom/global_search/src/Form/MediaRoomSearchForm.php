<?php
namespace Drupal\global_search\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\global_search\Ajax\SearchOnlyMediaRoomCommand;
use Drupal\Core\Ajax\CommandInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Implementation of global search form.
 */
class MediaRoomSearchForm extends FormBase {

  /**
   * To access pdp methods using services.
   *
   * @var pdpServices
   */
  protected $pdpServices;


  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'media_room_search_form';
  }

  /**
   * Implementation fof Media room search form build().
   */
  public function buildForm(array $form, FormStateInterface $form_state, $arg = null) {
    $form = [];
    $form['search_input'] = [
      '#type' => 'search',
      '#placeholder' => $this->t('Search'),
      '#attributes' => [
        'class' => ['form-item__textfield form-item__textfield--required serach-input'],
      ],
    ];
    $form['filter_option'] = [
      '#type' => 'radios',
      '#title' => '',
      '#options' => [
        'media-tab' => 'Search Only Media Room',
        'entire-site' => 'Search Entire Site',
      ],
      '#default_value' => 'media-tab',
      '#attributes' => [
        'class' => ['radio'],
      ],
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('search'),
      '#button_type' => 'primary',
      '#attributes' => [
        'class' => ['media-room-search-button button--lg hide'],
      ],
      '#ajax' => [
        'callback' => '::searchMediaRoom',
        'event' => 'click',
        'wrapper' => 'edit-output',
      ],
    ];
    $form['#theme'] = 'media_room_search_block';
    $form['#attached']['library'][] = 'global_search/searchResult';
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }
  /**
   *
   * Check search option and act.
   * @param array $form
   * @param FormStateInterface $form_state
   */
  public function searchMediaRoom(array &$form, FormStateInterface $form_state, Request $request)
  {

    $form_data = $form_state->getUserInput();
    $searchPattern = $form_data['search_input'];
    $searchFilter = $form_data['filter_option'];
    if($searchFilter == 'entire-site') {
      return (new AjaxResponse())->addCommand(
        new RedirectCommand(Url::fromRoute('global_search.search')->toString().'?q='.$searchPattern)
      );
    } else {
      $request = Request::createFromGlobals();
      $headers = $request->server->getHeaders();
      $referer_url = isset($headers['REFERER']) ? $headers['REFERER'] : '';
      if($referer_url){
        $current_url = explode('/',$referer_url);
        $array_count = count($current_url);
        if($array_count > 0) {
          $array_last_key = $array_count-1;
          $current_page = $current_url[$array_last_key];
          $content_type = '';
          if($current_page == 'media-room' || $current_page == 'MediaRoom' || $current_page == 'mediaroom') {
            $content_type = 'media-tab';
          }
          if(in_array('press-releases', $current_url)){
            $content_type = 'press-releases';
          }
          if(in_array('in-the-news', $current_url)){
            $content_type = 'in-the-news';
          }
          $form_data['content-type'] =  $content_type;
        }
      }
      $response = new AjaxResponse();
      $response->addCommand( new SearchOnlyMediaRoomCommand($form_data));
      return $response;

    }
  }
  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * If you put model number, the page  automatically take you to the PDP page.
   */
  public function productSearch($typedString) {
    $config = $this->config('sync.api_settings');
    $domain = $config->get('sync.wcs_rest_api');
    $output = [];
    $method = 'GET';
    $headers = add_headers(TRUE);

    // Search Products.
    $searchProductUrl = $domain . $config->get('sync.search_products');
    $searchProductUrl = str_replace("{typedString}", $typedString, $searchProductUrl);
    $response = web_services_api_call($searchProductUrl, $method, $headers, '', 'SearchProducts');
    $pdpUrl = '';
    if ($response['status'] == 'SUCCESS') {
      $data = json_decode($response['response']);
      if (count($data->content) > 0) {
        foreach ($data->content as $content) {
          $output['products'][$content->name]['name'] = $content->name;
          $output['products'][$content->name]['description'] = $content->descriptionWithCollection;
          // $output['products'][$content->name]['url'] = ($this->pdpServices->getProductUrl($content->name) == '#') ? '/pdp/' . $content->name : $this->pdpServices->getProductUrl($content->name);
          //When user entered actual sku then redirect to PDP.
          if (strtolower($output['products'][$content->name]['name']) === strtolower($typedString)) {
            $pdpUrl = $output['products'][$content->name]['url'];
          }
        }
      }
    }
    return $pdpUrl;
  }

}

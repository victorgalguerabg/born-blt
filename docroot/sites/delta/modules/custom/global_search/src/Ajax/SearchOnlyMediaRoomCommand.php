<?php
namespace Drupal\global_search\Ajax;
use Drupal\Core\Ajax\CommandInterface;

/**
 * AJAX command to rerender a formatted text field without any transformation
 * filters.
 */
class SearchOnlyMediaRoomCommand implements CommandInterface {

  protected $search;
  // Constructs a ReadMessageCommand object.
  public function __construct($search) {
    $this->search = $search;
  }
  // Implements Drupal\Core\Ajax\CommandInterface:render().
  public function render() {
    return array(
      'command' => 'searchMediaRoom',
      'title' => $this->search['search_input'],
      'type' => $this->search['content-type'],
    );
  }
}
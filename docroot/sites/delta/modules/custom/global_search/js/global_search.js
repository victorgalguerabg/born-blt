(function ($, Drupal, drupalSettings) {

    Drupal.behaviors.globalSearch = {
        attach: function (context, settings) {
            if($(".inner-link").length){
                $(".inner-link").click(function(){
                    var get_target_selector = $(this).attr("rel");
                    $("#"+get_target_selector).parent().show();
                });
            }
            if ($('.btn-support-browse').length) {
                $('.btn-support-browse').click(function () {
                    if ($("#edit-product-type").val() === '') {
                        $("#edit-product-type").addClass('error');
                        return false;
                    }
                });
            }

            var xhr;
            $('form#global-search-form input.autocomplete').keyup(function (event) {
                var searchText = $(this).val();
                if (searchText.length > 2) {
                    fn(searchText);
                    $('.autocomplete-container--inner .input-close').addClass('hide');
                    $('.autocomplete-container--inner .input-loader').removeClass('hide');
                } else {
                    if (xhr) {
                        xhr.abort();
                    }
                    $("#search-block-suggestion-result").empty();
                    $('.autocomplete-container--inner .input-loader, .autocomplete-container--inner .input-close').addClass('hide');
                }
            });

            $(document).on('click', '#search-block-suggestion-result li.record', function () {
                var itemValue = $(this).attr('data-item-value');
                $('form#global-search-form input.autocomplete').val(itemValue);
                $("#search-block-suggestion-result").empty();
            });

            $('.autocomplete-container--inner .input-close').click(function () {
                $('form#global-search-form input.autocomplete').val('');
                $("#search-block-suggestion-result").empty();
                $('.autocomplete-container--inner .input-close').addClass('hide');
                $('.autocomplete-container--inner input.autocomplete').focus();
            });
            var fn = function (searchText) {
                if (xhr && xhr.readyState != 4) {
                    xhr.abort();
                }
                var output = '';
                $('.autocomplete-container--inner .input-loader').removeClass('hide');
                var searchUrl = drupalSettings.path.baseUrl + "global-search-autocomplete";
                xhr = $.get(searchUrl, {text: searchText});
                xhr.done(function (data) {
                    if (data == '') {
                        output = "<div class=\"pad-aa text-align-center\">No Results Found!</div>";
                    } else {
                        output = data;
                    }
                    $("#search-block-suggestion-result").html(output);
                    $('.autocomplete-container--inner .input-loader').addClass('hide');
                    $('.autocomplete-container--inner .input-close').removeClass('hide');
                });
            };
            if ($(".get-support").length) {
                $('.get-support select').change(function () {
                    window.location.href = this.value;
                });
            }
            //set as go-back as class, which will redirect to previous page.
            if ($('a.back').length) {
                $('a.back').click(function () {
                    parent.history.back();
                    return false;
                });
            }

            /**
              Parts page auto autocomplete Results.
            */
            var searchxhr;
            $('input#global-search-text').keyup(function (event) {
              var searchSuggestion = $(this).val();
              if (searchSuggestion.length > 2) {
                searchfn(searchSuggestion);
              }
              else {
                if (searchxhr) {
                  searchxhr.abort();
                }
                $("#search-block-suggestion-result").empty();
                $('.autocomplete-container--inner .input-loader, .autocomplete-container--inner .input-close').addClass('hide');
              }
            });

            var searchfn = function (searchSuggestion) {
              if (searchxhr && searchxhr.readyState != 4) {
                  searchxhr.abort();
              }
              var searchResults = "<div class='auto-search-result'>";
              var searchUrl = drupalSettings.path.baseUrl + "global-search-autocomplete-parts";
              searchxhr = $.get(searchUrl, {q: searchSuggestion});
              searchxhr.done(function (data) {
                  if (data == '') {
                    searchResults += "<div class='pad-aa text-align-center'>No Results Found!</div>";
                  }
                  else {
                    var sectionCounter = 0;
                    $.each(data, function(key,val) {
                      if(key == 'products') {
                        $.each(val, function(productKey,productVal) {
                          if(productVal['title'] == 'section') {
                            searchResults += '<div class="as-block">';
                            searchResults += '<div class="as-title"><h3 class="h3">'+productVal['label']+'</h3></div><div class="as-desc">';
                          }
                          else if(productVal['title'] == 'viewall') {
                            searchResults += '</div><div class="as-view">'+productVal['label']+'</div>';
                          }
                          else {
                            if(productVal['value'] != "") {
                              searchResults += `<div class="as-desc-block"><a href="${productVal['url']}"><div class="as-item">
                                                <p class="paragraph">${productVal['value']}</p>
                                                </div>
                                                <div class="as-item-no"><p class="paragraph">${productVal['name']}</p>
                                                </div>
                                                </a></div>`;
                            }
                          }
                        });
                        searchResults += '</div>';
                      }
                      if(key == 'parts') {
                        $.each(val, function(partsKey,partsVal) {
                          if(partsVal['title'] == 'section') {
                            searchResults += '<div class="as-block">';
                            searchResults += '<div class="as-title"><h3 class="h3">'+partsVal['label']+'</h3></div><div class="as-desc">';
                          }
                          else if(partsVal['title'] == 'viewall') {
                            searchResults += '</div><div class="as-view">'+partsVal['label']+'</div>';
                          }
                          else {
                            if(partsVal['value'] != "") {
                              searchResults += `<div class="as-desc-block"><a href="${partsVal['url']}"><div class="as-item">
                                                <p class="paragraph">${partsVal['value']}</p>
                                                </div>
                                                <div class="as-item-no"><p class="paragraph">${partsVal['name']}</p>
                                                </div>
                                                </a></div>`;
                            }
                          }
                        });
                        searchResults += '</div>';
                      }
                    });
                  }
                  searchResults += "</div>";
                  $(".search-block-suggestion-result-parts").html(searchResults);
              });
            };

            /* Parts page Form Submit */
            $(".parts-form-submit").on("click", function(e){
              e.preventDefault();
              var base_url = window.location.origin;
              var searchtext = $(".global-search-text").val();
              var searchUrl = base_url+"/search-results?q="+searchtext;
              window.location.href = searchUrl;
            });

            /**
              Get search value from querystring.
            */
            function getSearchParams(k) {
                var p = {};
                location.search.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (s, k, v) {
                    p[k] = v
                })
                var response = k ? p[k] : p;
                var decodedResponse = decodeURIComponent(response);
                var sanitizedResponse = removeHTMLTags(decodedResponse);
                return sanitizedResponse;
            }

            /*
                 Function to remove HTML tags.
            */

            function removeHTMLTags(decodedResponse) {
                paramText = decodedResponse;
                paramText = paramText.replace(/<script.*>[\w\W]{1,}(.*?)[\w\W]{1,}<\/script>/gi, "");
                paramText = paramText.replace(/%3Cscript.*%3E[\w\W]{1,}(.*?)[\w\W]{1,}%3C\/script%3E/gi, "");
                paramText = paramText.replace(/<style.*>[\w\W]{1,}(.*?)[\w\W]{1,}<\/style>/gi, "");
                paramText = paramText.replace(/&nbsp;/gi, " ");
                paramText = paramText.replace(/&amp;/gi, "&");
                paramText = paramText.replace(/&quot;/gi, '"');
                paramText = paramText.replace(/&lt;/gi, '<');
                paramText = paramText.replace(/&gt;/gi, '>');
                paramText = paramText.replace(/svg\/onload%3D/gi, '');
                paramText = paramText.replace(/--%21/gi, '');
                paramText = paramText.replace(/svg\/onload%3D/gi, '');
                paramText = paramText.replace(/--%21/gi, '');
                paramText = paramText.replace(/%28confirm%29/gi, '')
                paramText = paramText.replace(/%3C/gi, '');
                paramText = paramText.replace(/%3E/gi, '');
                paramText = paramText.replace(/<(?:.|\s)*?>/g, "");
                return paramText;

            }

        }
    };
})(jQuery, Drupal, drupalSettings);

(function ($, Drupal, drupalSettings, Backbone) {
    Drupal.behaviors.plpBehaviour = {
        attach: function (context, settings) {
            let gtm_search = [];
            let gtm_search_click = [];
            let loadProducts = true;
            let pageNumber = 0;
            let lastPage = false;
            let totalResult = 0;
            let loader = $('.ajax-progress-throbber');
            let finishDataIcon = drupalSettings.finishImages;
            let setFinishClass = "no-round";
            let _finishList = [];
            let _defaultFinish = '';
            let ignoreListFinish = ['na','notapplicable'];
            let ignoreStockTypeFinish = ['Obsolete','Use Up'];
            $('document').ready(function () {

                // Retain search text in global search form.
                var vars = [], hash;
                var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
                var pathArray = window.location.pathname.split( '/' );
                for(var i = 0; i < hashes.length; i++)
                {
                    hash = hashes[i].split('=');
                    vars.push(hash[0]);
                    vars[hash[0]] = hash[1];
                }
                var globalSearchValue = vars['title'];
                if(pathArray[1] == 'search' && typeof globalSearchValue != 'undefined') {
                    $("#global-search-text").val(globalSearchValue);
                }

                // Add advanced class when the url having the filter as query string.
                var _filter = getSearchParams('documentLookFor');
                if (_filter !== '' && _filter !== undefined) {
                    $('#plp-wrapper').addClass('advance-search');
                } else {
                    if ($('#plp-wrapper advance-search').length) {
                        $('#plp-wrapper').removeClass('advanced');
                    }
                }

                $('.product-see-all').click(function () {
                    $('ul.nav-tabs li.active').removeClass('active');
                    $('#productTabLink').parent('li').addClass('active');
                });
                $('.document-see-all').click(function () {
                    $('ul.nav-tabs li.active').removeClass('active');
                    $('#documentTabLink').parent('li').addClass('active');

                    $('html, body').stop().animate({
                        scrollTop: $('.nav-tabs').offset().top
                    }, 'slow');

                });
                $('.content-see-all').click(function () {
                    $('ul.nav-tabs li.active').removeClass('active');
                    $('#contentTabLink').parent('li').addClass('active');

                    $('html, body').stop().animate({
                        scrollTop: $('.nav-tabs').offset().top
                    }, 'slow');

                });
            })

            var _totalCount = 0;
            var docView = Backbone.View.extend({
                el: ".documentoutput",
                template: _.template($("#document-output").html()),
                initialize: function (initData) {

                    this.render(initData);
                    this.afterRender(initData);
                },
                render: function (initData) {
                    var typedKeyword = getSearchParams('title');
                    //var noResultsMessage = "<div class='noResults no-rows-doc'>We're sorry, we didn't find any results for " + decodeURIComponent(typedKeyword) + ". We recommend using different words in your search.</div>";
                    if (initData === null) {
                        $("#document-tab div.row h3, .paging-compare-bar").hide();
                        $("#document-tab .documentoutput").html(noResultsMessage);
                        $("#document-tab-link").hide();
                        $("#search-document-title").hide();
                        $('.documentResult').hide();
                        $(".document-see-all").hide();
                        $('#doc-count-tab').attr('count', 0);
                         return false;
                    }
                    var _self = this;
                    var _render = "";
                    var content = initData.content;
                    if (content.length > 0) {
                      _.each(initData.content, function (entry) {
                          var _filePath = entry.filePath;
                            var _fileName = entry.fileName;
                            var _docType = entry.documentType;
                            _render += _self.template({
                                fileName: _fileName,
                                fileLinks: entry.modelNumber.slice(0, 5),
                                filePath: _filePath,
                                docType: _docType,
                            });
                            _self.$el.html(_render);
                            $('.preloader').remove();
                            $('body').removeClass('loader-active');
                            $('.tab-content-title-doc').show();
                        });
                        $('.document-all-search-result').html(_render);
                    }

                  var _documentCount = initData.totalElements ? initData.totalElements : 0;
                    if(_documentCount === 0) {
                      $(".all-document").hide();
                      $('.documentResult').hide();
                      $("#document-tab-link").hide();
                      $("li#document-list").remove();
                    }
                  _totalCount = parseInt(_totalCount) + parseInt(_documentCount);
                  totalResult = totalResult + _documentCount;
                  $('#doc-count').html(_documentCount);
                  $('#doc-count-tab,#document-mbl-cnt').html('(' + _documentCount + ')');
                  $('#doc-count-tab,#document-mbl-cnt').attr('count', _documentCount);
                  recalculate();
                  return this;
                },
                afterRender: function (initData) {
                    if (initData != null) {
                        var _documentCount = initData.totalElements;
                        if (_documentCount > 0) {
                            var _filter = getSearchParams('documentLookFor');
                            //lets select document tab as default when user go with advanced search.
                            if (_filter !== "" && _filter !== undefined && (_filter === 'ts' || _filter === 'mi' || _filter === 'pd')) {
                                $("#documentTabLink").trigger('click');
                            }
                        }

                    }

                }
            });

            // Process content data and assemble the page.
            var contView = Backbone.View.extend({
                el: ".contentoutput",
                el1: ".contentoutputfull",
                template: _.template($("#content-output").html()),
                template1: _.template($("#full-content-output").html()),
                initialize: function (content, _sno) {
                    if (content.currentpage === 1) {
                        this.renderAllsectionContent(content, _sno);
                    }
                    this.renderContentSection(content, _sno);
                },
                renderAllsectionContent: function (content, _sno) {
                    var _self = this;
                    var _render = "";
                    if (content.totalmatches > 0) {
                        var matches = content.matches.slice(0, 8);
                        var counter = _sno * 20;
                        var _incrCnter = counter + 1;
                        _.each(matches, function (entry) {
                            var _title = entry.title;
                            var _body = entry.search_api_excerpt;
                            var _url = entry.url;
                            _render += _self.template({
                                searchContentBody: _body,
                                searchContentUrl: _url,
                                searchContentTitle: _title,
                                searchSno: _incrCnter,
                            });
                            _self.$el.html(_render);
                            _incrCnter++;
                        });
                    }
                    $('.tab-content-title-cont').show();
                    recalculate();
                    return this;
                },
                renderContentSection: function (content, _sno) {
                    var _sno = parseInt(_sno);
                    var _self = this;
                    var _render = "";
                    if (content.totalmatches > 0) {
                        var matches = content.matches;
                        var counter = _sno * 20;
                        var _incrCnter = counter + 1;
                        _.each(matches, function (entry) {
                            var _title = entry.title;
                            var _body = entry.search_api_excerpt;
                            var _url = entry.url;
                            _render += _self.template1({
                                searchContentBody: _body,
                                searchContentUrl: _url,
                                searchContentTitle: _title,
                                searchSno: _incrCnter,
                            });
                            $('.preloader').remove();
                            $('body').removeClass('loader-active');
                            _incrCnter++;
                        });
                        $('#fullcontentoutput').html(_render);
                        $('.content-all-search-result').html(_render);
                    }
                    return this;
                }
            });


            function facetView(data, className){
              let facets = data[0].terms;
              let elem = '.'  + className;
              $.each(facets, function(i, d) {
                $(elem).append('<option value="' +
                  d.term +
                  '">' + d.term +
                  '</option>');
              });
              $(elem).show();

            }

            var ProdView = Backbone.View.extend({

                el: "#output",
                el2: "#output-1",
                template: _.template($("#output-node").html()),
                template2: _.template($("#output-node-1").html()),

                initialize: function (initData, eventName, append) {
                  this.render(initData, eventName,append);
                  this.renderFullProducts(initData);
                },

                render: function (initData, eventName, append) {
                    if (!$('.preloader').length) {
                        $('#plp-wrapper').prepend('<div class="preloader"><i class="input-loader"></i></div>');
                    }

                    var _self = this;
                    var _render = "";
                    var _i = 0;
                    if (initData.content != null && initData.content != '') {
                        _.each(initData.content, function (entry) {
                            var search_position = 0;
                            var _price = 0;
                            let _ribbonColor = '';
                            let search_product = new Object();
                            search_position = search_position + 1;
                            search_product.list = "Search Results";
                            if(entry.collections[0] != undefined) {
                                search_product.brand = entry.collections[0];
                            }
                            else {
                                search_product.brand = "";
                            }
                            search_product.name = entry.description;
                            search_product.position = search_position;
                            search_product.id = entry.name;
                            search_product.variant = entry.values.Finish;
                            if(entry.categories != undefined && entry.categories.length > 0) {
                                entry.categories.forEach(function(gtm_cat){
                                    if (gtm_cat.startsWith("Delta_") == true){
                                        search_product.category = gtm_cat.replace("_", "/");
                                    }
                                });
                            }

                            var _filter = getSearchParams('documentLookFor');
                            if (_filter !== '' || _filter !== undefined) {
                                var _price = entry.values.ListPrice;
                            }
                            var _heroImage = entry.heroImageSmall;
                            let _ribbonText = '';
                            if (entry.values.MaterialStatus !== undefined && entry.values.MaterialStatus >= 50) {
                              _ribbonText = 'discontinued';
                              _ribbonColor = "gray-30";
                            }

                            var _title = "Title";
                            if (entry.description !== '' && entry.description !== undefined) {
                                _title = entry.description;
                            }

                            var _collection = "";
                            //if(_filter === ''){
                            if (entry.values.Collection !== "" && entry.values.Collection !== undefined) {
                                _collection = entry.values.Collection;
                            }
                            //}


                          let _currentDate = entry.values.currentDate;
                          let _availableToShipDate = entry.values.AvailableToShipDateTimestamp;
                          let _availableToOrderDate = entry.values.AvailableToOrderDateTimestamp;
                          if (_currentDate < _availableToShipDate) {
                            if (_currentDate > _availableToOrderDate) {
                              _ribbonText = entry.values.comingSoonText;
                              _ribbonColor = "delta-red";
                            }
                          }
                          let _viewAllCollectionLink = entry.values.viewAllCollectionLink;
                            var _getCategories = entry.categories;
                            var _sku = entry.name;
                            var urlAliasObject = drupalSettings.urlAliasObject;
                            var _url = '';
                            /*if (typeof urlAliasObject[_sku] != 'undefined') {
                                _url = urlAliasObject[_sku];
                            }*/
                            if (_url === '' || _url === undefined) {
                                _url = '/product-detail/' + _sku;
                            }
                            let _finishList = entry.finishOptions;
                            let _defaultFinish = entry.facetFinish;
                           _price = '$ ' + _price;
                            gtm_search.push(search_product);
                            gtm_search_click[entry.name] = search_product;

                            var getAPiFinishName = '';
                            var setFinishIconPath = '';
                            const getEachFinish = [];
                            new Date;
                            for (var i = 0; i <= _finishList.length; i++ ){
                              if(_finishList[i] !== undefined && typeof _finishList[i].facetFinish !== 'undefined' && _finishList[i].modelNumber.indexOf('-IN') == '-1' && _finishList[i].modelNumber.slice(-2) != '-R' && (Date.now() > _finishList[i].availableToOrderDate)){
                                var getAPiFinishName = _finishList[i].facetFinish;
                                var getAPiFinishST = _finishList[i].stockingType;
                                ActiveClass = 'in-active finish-class';
                                if(setFinishClass == getAPiFinishName){
                                  ActiveClass = 'active finish-class';
                                  _defaultFinish = '';
                                }
                                if(_defaultFinish == getAPiFinishName){
                                  ActiveClass = 'active finish-class';
                                }
                                var setFinishIconPath = finishDataIcon[getAPiFinishName];
                                var existInIgnoreList = ignoreListFinish.indexOf(getAPiFinishName);
                                var existIgnoreStockList = ignoreStockTypeFinish.indexOf(getAPiFinishST);
                                if(setFinishIconPath !== undefined && existInIgnoreList == -1 && existIgnoreStockList == -1 && getAPiFinishST != ''){
                                   var finishModelNumber = _finishList[i].modelNumber;
                                   var imgIcon = '<a data="'+ _sku +'" href="/product-detail/' + finishModelNumber + '" class="no-line product-action-link">' +'<img src="' + setFinishIconPath + '" alt="'+ _finishList[i].facetFinish +'" title="'+ _finishList[i].finish +'" class="'+ ActiveClass +'"  /></a>';
                                   getEachFinish.push(imgIcon);
                                }
                              }
                            }
                            _render += _self.template({
                                title: _title,
                                collectionName: _collection,
                                heroImage: _heroImage,
                                finishOptions: getEachFinish,
                                sku: _sku,
                                price: _price,
                                url: _url,
                                ribbonText: _ribbonText,
                                ribbonColor: _ribbonColor,
                                viewAllCollectionLink:  _viewAllCollectionLink
                            });

                            $('.tab-content-title-prod').show();
                            $('.preloader').remove();
                            $('body').removeClass('loader-active');
                            _i++;
                        });
                        if (gtm_search != undefined && gtm_search.length > 0) {
                            dataLayer.push({
                                'event':'pageReady',
                                'ecommerce': {
                                    'impressions': gtm_search
                                },
                            });
                            dataLayer.push({
                                'event':'productImpression',
                                'ecommerce': {
                                    'impressions': gtm_search
                                },
                            });
                            gtm_search = [];
                        }
                        if(append ==1){
                            _self.$el.append(_render);
                        }else{
                            _self.$el.html(_render);
                            $('.product-all-search-result').html(_render);
                        }
                    } else {
                        var noResultsMessage = '<div class ="error-message" role="contentinfo" aria-label="Error message"><h2 class="visually-hidden">Status message</h2>Oops!!! Nothing Found. Please refine your search</div>';
                        $('#output').html(noResultsMessage);
                        $('.preloader').remove();
                        $('body').removeClass('loader-active');
                    }
                    var _productCount = initData.totalElements;
                    totalResult = totalResult + _productCount;
                    var allTabCount = totalResult;
                    $('.product-count').html(_productCount);
                    $('.product-count-tab,#product-mbl-cnt').html('('+ _productCount + ')');
                    $('.product-count-tab,#product-mbl-cnt').attr('count', _productCount);
                    recalculate();

                    // Product Click GA Event
                    if(document == context){
                      $("a.product-action-link",context).once('global_search').on("click", function (event) {
                          event.stopPropagation();
                          event.preventDefault();
                          var currentUrl = $(this).attr("href");
                          var spitedUrl = currentUrl.split("/");
                          var parentClass = $(this).parent().attr("class");
                          //When user click finishes, which is not an current sku.
                          if(parentClass == 'prod-finishes' && gtm_search_click[spitedUrl[2]] == undefined){
                            spitedUrl[2] = $(this).attr('data');
                          }
                          if(gtm_search_click[spitedUrl[2]] != undefined) {
                              delete gtm_search_click[spitedUrl[2]]["list"];
                              dataLayer.push({
                                  'event':'productClick',
                                  'ecommerce': {
                                      'click': {
                                          'actionField': {'list': 'Search Results'},
                                          'products': [gtm_search_click[spitedUrl[2]]]
                                      }
                                  },
                                  'eventCallback': function() {
                                      document.location = currentUrl;
                                  }
                              });
                          }
                          document.location = currentUrl;
                      });
                      return this;
                    }

                },
                renderFullProducts: function (initData) {
                    var typedKeyword = getSearchParams('title');
                    // var noResultsMessage = "<div class='noResults no-rows-pro'>We're sorry, we didn't find any results for " + decodeURIComponent(typedKeyword) + ". We recommend using different words in your search.</div>";
                    if (initData.content.length === 0) {
                        $("#output-1").html(noResultsMessage);
                        $("#productTabLink").hide();
                    }
                    var _self = this;
                    var _render = "";
                    if (initData.content != '') {
                        _.each(initData.content, function (entry) {
                            var _price = entry.values.ListPrice;
                            var _heroImage = entry.heroImageSmall;
                            var _SyndigoActive = '';
                            if (entry.values.MaterialStatus !== undefined && entry.values.MaterialStatus >= 50) {
                                var _SyndigoActive = 'discontinued';
                            }
                            var orderDate = (entry.values.AvailableToOrderDate !== '' || entry.values.AvailableToOrderDate !== undefined) ? DateTotimeStamp(entry.values.AvailableToOrderDate) : '';
                            var shipDate = (entry.values.AvailableToShipDate !== '' || entry.values.AvailableToShipDate !== undefined) ? DateTotimeStamp(entry.values.AvailableToShipDate) : '';
                            var currentDate = DateTotimeStamp(getCurrentDate());
                            if (orderDate === '' || shipDate === '') {
                                shipDate = currentDate - (60 * 60 * 24);
                            }
                            if (currentDate < shipDate) {
                                if (currentDate > orderDate) {
                                    var _SyndigoActive = 'coming soon';
                                }
                            }
                            var _title = "Title";
                            if (entry.description != '') {
                                _title = entry.description;
                            }
                            var _collection = "";
                            if (entry.values.Collection !== "" && entry.values.Collection !== undefined) {
                                _collection = entry.values.Collection;
                            }
                            var _sku = entry.name;
                            var urlAliasObject = drupalSettings.urlAliasObject;
                            var _url = '';
                            if (typeof urlAliasObject != 'undefined') {
                                _url = urlAliasObject[_sku];
                            }
                            if (_url === '' || _url === undefined) {
                                _url = '/pdp/' + _sku;
                            }
                            _render += _self.template2({
                                title: _title,
                                collectionName: _collection,
                                heroImage: _heroImage,
                                sku: _sku,
                                price: _price,
                                url: _url,
                                prodOutDated: _SyndigoActive,
                            });
                            //$('#output-1').html(_render);
                            //  $('.preloader').remove();
                            // $('body').removeClass('loader-active');
                        });
                    } else {
                        var typedKeyword = getSearchParams('title');
                        $("#product-tab div.row h3, .paging-compare-bar").hide();
                        $('.preloader').remove();
                        $("#productTabLink").hide();
                        $('body').removeClass('loader-active');
                    }
                    var _productCount = initData.totalElements;
                    if(_productCount === 0){
                      $(".all-product").hide();
                      $("#product-tab-link").hide();
                      $(".productResult").remove();
                      $(".productResult").removeClass('is-active,active-tab-title');

                      if($('.tab-accordion-title').hasClass('active-tab-title')) {
                        $(this).removeClass('active-tab-title,productResult');
                      }
                      $('.product_container, #product-list, #product-mbl-tab').remove();

                      setTimeout(function() {
                        if($("#content-count-tab").attr("count") > 0){
                          $('.content_container, #content-tab-link, .contentResult').addClass('is-active');
                          $('.tab-accordion-title.contentResult').addClass('active-tab-title');

                        }else if ($('#doc-count-tab').attr('count') > 0) {
                            $('.documentResult, #document-tab-link').addClass('is-active');
                            $('.tab-accordion-title.documentResult').addClass('active-tab-title');
                            $('.content_container, #content-tab-link, .contentResult').removeClass('is-active');
                        }else{
                          $('.content_container, #content-list, #content-mbl-tab, .contentResult').remove();
                          $('.allResult, #all-tab-link').addClass('is-active');
                          $('.content_container, #content-tab-link').removeClass('is-active');
                          $('.documentResult, #document-tab-link').removeClass('is-active');
                        }
                      },500);

                      $('#all-tab-link').on('click',function() {
                        if($('#all-tab-link .tabs__link').hasClass('is-active')) {
                          $('.allResult').addClass('is-active');
                          $('.documentResult, #document-tab-link').removeClass('is-active');
                          $('.allResult.is-active .all-tab-dropdown').css('display','none');
                          $('.allResult.is-active .srp-results__header').css('padding-bottom','20px');
                        }
                      });

                      $('.tab-accordion-title.allResult').on('click',function() {
                        if($('.tab-accordion-title.allResult').hasClass('active-tab-title')) {
                          $('.allResult.is-active .all-tab-dropdown').css('display','none');
                          $('.allResult.is-active .srp-results__header').css('padding-bottom','20px');
                          $('.allResult.is-active .srp-result-summary').css('text-align','left');
                          $('.allResult.is-active .srp-result-summary + div').css('text-align','right');
                        }
                      })


                       $('#document-list').on('click',function() {
                          if($('#doc-count-tab').attr('count') > 0) {
                            $('.documentResult, #document-tab-link').addClass('is-active');
                            $('.contentResult, #content-tab-link').removeClass('is-active');
                          }
                        });

                        $('#content-list').on('click',function() {
                           if($('#content-count-tab').attr('count') > 0) {
                             $('.contentResult, #content-tab-link').addClass('is-active');
                           }
                         });


                    }
                    $('#productTabLink span').html('Products (' + _productCount + ')');
                    return this;
                }

            });

            var paginationView = Backbone.View.extend({
                el: ".pagination-output",
                template: _.template($("#pagination").html()),
                initialize: function (initData) {
                    this.render(initData);
                },
                events: {
                    'click [type="button"]': 'pagination'
                },
                pagination: function (e) {
                    var params = [];
                    params['pageNum'] = $(e.currentTarget).val();
                    applyPLPFacets(params, 'pager');
                },
                render: function (initData) {
                    var _self = this;
                    var _render = "";
                    //stopLoop value
                    var _nextPage = parseInt(initData.number) + 1;
                    var _previousPage = parseInt(initData.number) - 1;
                    _render += _self.template({
                        totalPager: initData.totalPages,
                        currentPage: _nextPage,
                        nextPage: _nextPage,
                        previousPage: _previousPage,
                        totalRecordset: initData.totalElements
                    });
                    _self.$el.html(_render);
                }
            });

            var docpaginationView = Backbone.View.extend({
                el: ".document-pagination-output",
                template: _.template($("#document-pagination").html()),
                initialize: function (initData) {
                    this.render(initData);
                },
                events: {
                    'click [type="button"]': 'docpagination'
                },
                docpagination: function (e) {
                    var params = [];
                    params['pageNum'] = $(e.currentTarget).val();
                    params['first'] = $(e.currentTarget).attr('first');
                    params['last'] = $(e.currentTarget).attr('last');
                    if($(e.currentTarget).val() === '>') {
                      params['pageNum'] = $('.pager__link.is-active').val();
                      params['action'] = 'next';
                    }
                    if($(e.currentTarget).val() === '>>') {
                      var total = $(e.currentTarget).attr('total');
                      params['pageNum'] = parseInt(total);
                      params['first'] = parseInt(total)-4;
                      params['last'] = parseInt(total);
                    }
                    if($(e.currentTarget).val() === '<') {
                      params['pageNum'] = $('.pager__link.is-active').val();
                      params['action'] = 'prev';
                    }
                    if($(e.currentTarget).val() === '<<') {
                      params['pageNum'] = 1;
                      params['first'] = 1;
                      params['last'] = 5;
                    }
                    applydocFacets(params);
                },
                render: function (initData) {
                  if (initData === null) {
                    // $('.documentoutput').html('<h4>No results at this time.</h4>');
                    return false;
                  }
                  var cont_disp_cnt = 20;//drupalSettings.cont_disp_cnt;
                  if (cont_disp_cnt == null) {
                    cont_disp_cnt = 20;
                  }
                  var _self = this;
                  var _render = "";
                  var _nextPage = (initData === null) ? 0 : parseInt(initData.number) + 1;
                  var _currentpage = initData.currentpage;  // Current page
                  var _totalmatches = initData.totalElements;
                  var _totalpages = Math.ceil(_totalmatches / cont_disp_cnt);
                  var _nextPage;
                  var _first;
                  if (_currentpage < _totalpages) {
                    _nextPage = _currentpage + 1;
                  } else {
                    _nextPage = _currentpage;
                  }
                  var _previousPage;
                  if (_currentpage === 1) {
                    _previousPage = 1;
                    _first = 1;
                  } else {
                    _previousPage = _currentpage - 1;
                    _first = initData.first;
                  }
                  if(_currentpage>=initData.first && _currentpage<=initData.last) {
                    _first = parseInt(initData.first);
                  }
                  if(initData.action && initData.action == 'next') {
                    if(_currentpage>=initData.first && _currentpage<initData.last) {
                      _first = parseInt(initData.first);
                    } else {
                      _first = parseInt(initData.first) + 1;
                    }
                    _currentpage = parseInt(_currentpage) + 1;
                  }
                  if(initData.action && initData.action == 'prev') {
                    if(_currentpage>initData.first && _currentpage<=initData.last) {
                      _first = parseInt(initData.first);
                    } else {
                      _first = parseInt(initData.first) - 1;
                    }
                    _currentpage = parseInt(_currentpage) - 1;
                  }
                  var pagination = parseInt(_first)+4;
                  if(_totalpages < pagination) {
                    pagination = _totalpages;
                  }
                  if(_currentpage != 1) {
                    _render += '<input total="'+_totalpages+'" class="pager__link first" type="button" data="" value="<<">';
                    _render += '<input first="'+_first+'" last="'+pagination+'" class="pager__link prev" type="button" data="" value="<">';
                  }
                  for(var i=_first;i<=pagination;i++) {
                    /*_render += _self.template({
                      pageNumber: i,
                      currentPage: _currentpage,
                      totalRecordset: initData.totalElements
                    });*/
                    var classValue = '';
                    if(i === parseInt(_currentpage)) {
                      classValue = 'is-active';
                    }

                    _render += '<input first="'+_first+'" last="'+pagination+'" class="pager__link '+ classValue +'" type="button" value="'+ i +'">';

                  }
                  if(_currentpage != _totalpages) {
                    _render += '<input first="'+_first+'" last="'+pagination+'" class="pager__link next" type="button" data="" value=">">';
                    _render += '<input total="'+_totalpages+'" class="pager__link last" type="button" data="" value=">>">';
                  }
                var start_value = (_currentpage-1)*20;
                if(_currentpage ==1) {
                    start_value = 1;
                }
                var endvalue = _currentpage*20;
                if(start_value == 1 && endvalue > initData.totalElements){
                    // no pager
                }
                else if(endvalue > initData.totalElements){
                endvalue = initData.totalElements;
                $('.srp-result-doc').html('<span class="srp-qty">Results ' + start_value + '-' + endvalue  + ' of ' + initData.totalElements +'</span>').attr('data-content-cnt', initData.totalElements);
                }
                else{
                $('.srp-result-doc').html('<span class="srp-qty">Results ' + start_value + '-' + endvalue  + ' of ' + initData.totalElements +'</span>').attr('data-content-cnt', initData.totalElements);
                }
                  _self.$el.html(_render);
                }
            });


            // Pagination fot content section.
            var contpaginationView = Backbone.View.extend({
                el: ".content-pagination-output",
                template: _.template($("#content-pagination").html()),
                initialize: function (initData) {
                    this.render(initData);
                },
                events: {
                    'click [type="button"]': 'contentpagination'
                },
                contentpagination: function (e) {
                    var params = [];
                    params['pageNum'] = parseInt($(e.currentTarget).val()); // Current
                    params['first'] = $(e.currentTarget).attr('first');
                    params['last'] = $(e.currentTarget).attr('last');
                    if($(e.currentTarget).val() === '>') {
                        params['pageNum'] = $('.pager__link.is-active').val();
                        params['action'] = 'next';
                    }
                    if($(e.currentTarget).val() === '>>') {
                        var total = $(e.currentTarget).attr('total');
                        params['pageNum'] = parseInt(total);
                        params['first'] = parseInt(total)-4;
                        params['last'] = parseInt(total);
                    }
                    if($(e.currentTarget).val() === '<') {
                        params['pageNum'] = $('.pager__link.is-active').val();
                        params['action'] = 'prev';
                    }
                    if($(e.currentTarget).val() === '<<') {
                        params['pageNum'] = 1;
                        params['first'] = 1;
                        params['last'] = 5;
                    }

                    applycontFacets(params);
                },
                render: function (initData) {
                    if (initData === null) {
                        $('.documentoutput').html('<h4>No results at this time.</h4>');
                        return false;
                    }
                    var cont_disp_cnt = 20;//drupalSettings.cont_disp_cnt;
                    if (cont_disp_cnt == null) {
                        cont_disp_cnt = 10;
                    }
                    var _self = this;
                    var _render = "";
                    var _currentpage = initData.currentpage;  // Current page
                    var _totalmatches = initData.totalmatches;
                    var _totalpages = Math.ceil(_totalmatches / cont_disp_cnt);
                    var _nextPage;
                    var _first;
                    if (_currentpage < _totalpages) {
                        _nextPage = _currentpage + 1;
                    } else {
                        _nextPage = _currentpage;
                    }
                    var _previousPage;
                  if (_currentpage === 1) {
                    _previousPage = 1;
                    _first = 1;
                  } else {
                    _previousPage = _currentpage - 1;
                    _first = initData.first;
                  }
                  if(_currentpage>=initData.first && _currentpage<=initData.last) {
                    _first = parseInt(initData.first);
                  }
                  if(initData.action && initData.action == 'next') {
                    if(_currentpage>=initData.first && _currentpage<initData.last) {
                      _first = parseInt(initData.first);
                    } else {
                      _first = parseInt(initData.first) + 1;
                    }
                    _currentpage = parseInt(_currentpage) + 1;
                  }
                  if(initData.action && initData.action == 'prev') {
                    if(_currentpage>initData.first && _currentpage<=initData.last) {
                      _first = parseInt(initData.first);
                    } else {
                      _first = parseInt(initData.first) - 1;
                    }
                    _currentpage = parseInt(_currentpage) - 1;
                  }
                  var pagination = parseInt(_first)+4;
                  if(_totalpages < pagination) {
                    pagination = _totalpages;
                  }
                  if(_currentpage != 1) {
                    _render += '<input total="'+_totalpages+'" class="pager__link first" type="button" data="" value="<<">';
                    _render += '<input first="'+_first+'" last="'+pagination+'" class="pager__link prev" type="button" data="" value="<">';
                  }
                  for(var i=_first;i<=pagination;i++) {
                    /*_render += _self.template({
                      pageNumber: i,
                      currentPage: _currentpage,
                      totalRecordset: initData.totalElements
                    });*/
                    var classValue = '';
                    if(i === parseInt(_currentpage)) {
                      classValue = 'is-active';
                    }

                    _render += '<input first="'+_first+'" last="'+pagination+'" class="pager__link '+ classValue +'" type="button" value="'+ i +'">';

                  }
                  if(_currentpage != _totalpages) {
                    _render += '<input first="'+_first+'" last="'+pagination+'" class="pager__link next" type="button" data="" value=">">';
                    _render += '<input total="'+_totalpages+'" class="pager__link last" type="button" data="" value=">>">';
                  }
                    if (_totalpages > 1 ) {
                      _self.$el.html(_render);
                    }
                }
            });
            if (context === document) {
                var pathArray = window.location.pathname.split('/');
                if (pathArray[1] === 'replacement-parts-search-results') {
                    var _param = getSearchParams('title');
                    var _type = getSearchParams('type');
                    var _filter = getSearchParams('documentLookFor');
                    $.ajax({
                        type: "GET",
                        url: "/search-doc-output",
                        dataType: 'json',
                        data: {
                            title: _param,
                            categories: _type,
                            // documentLookFor: _filter,
                        },
                        complete: function () {
                            recalculate();
                        },
                        success: function (data) {
                            var DocView = new docView(data);
                            var DocpaginationView = new docpaginationView(data);
                        }
                    });

                } else {
                    var _param = getSearchParams('title');
                    var _type = getSearchParams('type');
                    var _mediaroomFilter = getSearchParams('filter');
                    var _filter = getSearchParams('documentLookFor');
                    if (window.performance && window.performance.navigation.type == 2) {
                      var searchValue = (new URL(location.href)).searchParams.get('title');
                      var newQuery = "?title="+searchValue+"&searchFilter=true"
                      window.history.replaceState(null, null, newQuery);
                    }
                  let _defaultSort = '';
                  let _defaultFacet = '';
                  if(window.location.href.indexOf("searchFilter") == -1) {
                    _defaultSort = $('.srp-result-dropdowns #sortProduct').find('option:selected').val();
                    _defaultFacet = $('.srp-result-dropdowns .facetFinish').find('option:selected').val();
                    localStorage.setItem("selected-sort", _defaultSort);
                    localStorage.setItem("selected-facet", _defaultFacet);
                  }
                  let selectedSort = localStorage.getItem("selected-sort");
                  let selectedFacet = localStorage.getItem("selected-facet");
                  if((selectedSort || selectedFacet) && (window.location.href.indexOf("searchFilter") > -1)) {
                      _defaultSort = selectedSort;
                      _defaultFacet = selectedFacet;
                  }
                    $.ajax({
                        type: "GET",
                        url: "/search-output",
                        dataType: 'json',
                        data: {
                            title: _param,
                            categories: _type,
                            sort: _defaultSort,
                            facet: _defaultFacet,
                            // documentLookFor: _filter,
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            showErrorMessage(XMLHttpRequest.status, '#all-tab #output');
                            showErrorMessage(XMLHttpRequest.status, '#all-tab #output');

                        },
                        beforeSend: function () {
                            $('#all-tab #output').addClass('loader-active');
                            if ($('#all-tab .product-cal').length === 0) {
                                $('#plp-wrapper').prepend('<div class="product-call"><i class="input-loader"></i></div>');
                            }
                        },
                        complete: function () {
                            $('.product-call').remove();
                            $('#all-tab #output').removeClass('loader-active');
                            // Manage tag records
                            recalculate();
                            $('.srp-result-dropdowns .facetFinish').val(_defaultFacet);
                            $('.srp-result-dropdowns #sortProductAll').val(_defaultSort);
                            $('.srp-result-dropdowns #sortProduct').val(_defaultSort);

                        },
                        success: function (data) {
                            var prodView = new ProdView(data);
                            facetView(data.facets, 'facetFinish');
                            loadProducts = 1;
                            var PaginationView = new paginationView(data);

                        }
                    });
                    $.ajax({
                        type: "GET",
                        url: "/search-doc-output",
                        dataType: 'json',
                        data: {
                            title: _param,
                            categories: _type,
                            documentLookFor: _filter,
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            showErrorMessage(XMLHttpRequest.status, '#all-tab #output');
                        },
                        beforeSend: function () {
                            $('#all-tab #output').addClass('loader-active');
                            if ($('#all-tab .product-cal').length === 0) {
                                $('#plp-wrapper').prepend('<div class="product-call"><i class="input-loader"></i></div>');
                            }
                        },
                        complete: function () {
                            $('.product-call').remove();
                            $('#all-tab #output').removeClass('loader-active');
                            recalculate();
                        },
                        success: function (data) {
                            var DocView = new docView(data);
                            if(data){
                              data.currentpage = 1;
                            }
                            var DocpaginationView = new docpaginationView(data);
                            //var DocpaginationView = new docpaginationView(data);
                        }
                    });
                    // Matching records for content being fetched and processed based on
                    // matching record count.
                    var page = [];
                    page['pageNum'] = 1;
                    var content_matchings = getContentSearchResults(_param, _mediaroomFilter, page);
                    if (content_matchings.totalmatches !== undefined) {
                        _totalCount = content_matchings.totalmatches;
                    }
                    totalResult = totalResult + content_matchings.totalmatches;
                    //$('#contentTabLink span').html('Content (' + _totalCount + ')');
                    $('#content-count-tab,#content-mbl-cnt').html('(' + _totalCount + ')');
                    $('#content-count-tab,#content-mbl-cnt').attr('count', _totalCount);
                    $('#srp-result-content').html('<span class="srp-qty">' + _totalCount + '</span> Contents').attr('data-content-cnt', _totalCount);
                    if (_totalCount > 0) {
                        var ContView = new contView(content_matchings, 0);
                        var ContpaginationView = new contpaginationView(content_matchings);
                    } else {
                        $('#content-tab .row').html("<b>Nothing found. Please refine your search.</b>");
                        var typedKeyword = getSearchParams('title');
                        var noResultsMessage = "<div class='noResults'>We're sorry, we didn't find any results for " + decodeURIComponent(typedKeyword) + ". We recommend using different words in your search.</div>";
                        // $('#content-tab .row').html(noResultsMessage);
                        $("#contentTabLink").hide();
                        $(".contentResult").hide();
                        $("#search-content-title").hide();
                        $(".content-see-all").hide();
                    }
                }
                //lets select document tab as default when user go with advanced search.
                if (_filter !== "" && _filter !== undefined && (_filter === 'ts' || _filter === 'mi' || _filter === 'pd')) {
                    $("#documentTabLink").trigger('click');
                }

            }


            var showErrorMessage = function (status, wrapper) {
                switch (status) {
                    case 400:
                        $(wrapper).html("<div class='sorry-msg'>Server understood the request, but request content was invalid., Please try with valid information.</div>");
                        break;
                    case 401:
                        $(wrapper).html("<div class='sorry-msg'>Unauthorized access.</div>");
                        break;
                    case 403:
                        $(wrapper).html("<div class='sorry-msg'>We are unable to process your request at this time, Please try again later.</div>");
                        break;
                    case 500:
                        $(wrapper).html("<div class='sorry-msg'>We are into the system upgrade process, Please try again later.</div>");
                        break;
                    case 401:
                        $(wrapper).html("<div class='sorry-msg'>Service unavailable. Please try again later.</div>");
                        break;
                    default:
                        $(wrapper).html("<div class='sorry-msg'>Oops something went wrong, Please try again later.</div>");
                }
            }

            /**
             * Recalculate tab total counts of records
             */
             function recalculate() {
               var grandTotal = 0;
               var data_document_cnt = 0;
               var data_product_cnt = 0;
               var data_content_cnt = 0;
               var data_all_cnt = 0;

               if($(".product-count-tab").attr("count")) {
                 data_product_cnt = $(".product-count-tab").attr("count");
                 data_product_cnt = parseInt(data_product_cnt);
               }
               if($("#content-count-tab").attr("count")) {
                 data_content_cnt = $("#content-count-tab").attr("count");
                 data_content_cnt = parseInt(data_content_cnt);
               }
               if($("#doc-count-tab").attr("count")){
                 data_document_cnt = $("#doc-count-tab").attr("count");
                 data_document_cnt = parseInt(data_document_cnt);
               }
               if($("#search-all-count-tab").attr("count")){
                 data_all_cnt = $("#search-all-count-tab").attr("count");
                 data_all_cnt = parseInt(data_all_cnt);
               }

               grandTotal = data_document_cnt + data_product_cnt + data_content_cnt;
               if(!$.isNumeric(grandTotal)) {
                 grandTotal = 0;
               }
               $('#grand_total').html(grandTotal);
               $('#search-all-count-tab,#all-mbl-cnt').html('('+ grandTotal + ')');
               if (grandTotal === 0) {
                 $("#emptyResults").show();
                 $('.search-container').hide();


                 $('.tab-content-title-doc').hide();
                 $('.tab-content-title-cont').hide();
                 $('.tab-content-title-prod').hide();

                 var typedKeyword = getSearchParams('title');
                 //var noResultsMessage = "<div class='noResultsFound'>We're sorry, we didn't find any results for " + decodeURIComponent(typedKeyword) + ". We recommend using different words in your search.</div>";
                 //$(".no-rows-pro").html(noResultsMessage).show();
               }else{
                 $("#emptyResults").hide();
                 $('.search-container').show();
               }
             }



            /**
             *  Convert date to timestamp - 2015-07-23
             * @param dates
             * @returns {number}
             * @constructor
             */
            function DateTotimeStamp(dates) {
                if (dates !== undefined) {
                    var dates1 = dates.split("-");
                    var newDate = dates1[0] + "/" + dates1[1] + "/" + dates1[2];
                    return new Date(newDate).getTime();
                }
            }

            /**
             * Get current time.
             * @returns {string}
             */

            function getCurrentDate() {
                var d = new Date($.now());
                return d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate();
            }

            /* Define functin to find and replace specified term with replacement string */
            function replaceAll(str, term, replacement) {
                return str.replace(new RegExp(escapeRegExp(term), 'g'), replacement);
            }


            /*
       Get search value from querystring.
       */
            function getSearchParams(k) {
                var p = {};
                location.search.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (s, k, v) {
                    p[k] = v
                })
                var response = k ? p[k] : p;
                var decodedResponse = decodeURIComponent(response);
                var sanitizedResponse = removeHTMLTags(decodedResponse);
                return sanitizedResponse;
            }

            /*
                 Function to remove HTML tags.
            */

            function removeHTMLTags(decodedResponse) {
                paramText = decodedResponse;
                paramText = paramText.replace(/<script.*>[\w\W]{1,}(.*?)[\w\W]{1,}<\/script>/gi, "");
                paramText = paramText.replace(/%3Cscript.*%3E[\w\W]{1,}(.*?)[\w\W]{1,}%3C\/script%3E/gi, "");
                paramText = paramText.replace(/<style.*>[\w\W]{1,}(.*?)[\w\W]{1,}<\/style>/gi, "");
                paramText = paramText.replace(/&nbsp;/gi, " ");
                paramText = paramText.replace(/&amp;/gi, "&");
                paramText = paramText.replace(/&quot;/gi, '"');
                paramText = paramText.replace(/&lt;/gi, '<');
                paramText = paramText.replace(/&gt;/gi, '>');
                paramText = paramText.replace(/svg\/onload%3D/gi, '');
                paramText = paramText.replace(/--%21/gi, '');
                paramText = paramText.replace(/svg\/onload%3D/gi, '');
                paramText = paramText.replace(/--%21/gi, '');
                paramText = paramText.replace(/%28confirm%29/gi, '')
                paramText = paramText.replace(/%3C/gi, '');
                paramText = paramText.replace(/%3E/gi, '');
                paramText = paramText.replace(/<(?:.|\s)*?>/g, "");
                return paramText;

            }


            /*
            To return content search results based on current search value and requesting page based on previous or next button hits in pagination.
            @params: search value, requesting page
            @return: search result
             */
            function getContentSearchResults(params, mediaroomFilter, arguments) {
                var search_result = [];
                requestingpage = parseInt(arguments['pageNum']);
                //search_result.currentpage = requestingpage;
                if(arguments['pageNum']) {
                  search_result.currentpage = arguments['pageNum'];
                  search_result.first = arguments['first'];
                  search_result.last = arguments['last'];
                }
                if(arguments['action']) {
                  search_result.action = arguments['action'];
                }
                var matching_count = 0;
                if(_mediaroomFilter == 'media-tab') {
                    contentUrl = "rest/export/search_content_media?_format=json";
                    contentCountUrl = "rest/export/matching_records_count_media?_format=json";
                } else {
                    contentUrl = "/rest/export/search_content?_format=json";
                    contentCountUrl = "/rest/export/matching_records_count?_format=json";
                }
                $.ajax({
                    type: "GET",
                    url: contentUrl,
                    dataType: 'json',
                    async: false,
                    data: {
                        title: _param,
                        categories: _type,
                        page: requestingpage - 1,  // Since views counts from 0
                        categories: _type,
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        //showErrorMessage(XMLHttpRequest.status, '#document-tab');
                    },
                    beforeSend: function () {
                        $('body').addClass('loader-active');
                        if ($('.preloader').length === 0) {
                            $('#plp-wrapper').prepend('<div class="preloader"><i class="input-loader"></i></div>');
                        }
                    },
                    complete: function () {
                        $('.preloader').remove();
                        $('body').removeClass('loader-active');
                    },
                    success: function (data) {
                        var matches = [];
                        $.each(data, function (key, value) {
                            matches.push(value);
                        });
                        search_result.matches = matches;

                        $.ajax({
                            type: "GET",
                            url: contentCountUrl,
                            dataType: 'json',
                            async: false,
                            data: {
                                title: _param,
                                categories: _type,
                            },
                            success: function (data) {
                               matching_count = data.length;
                            }
                        });

                        search_result.totalmatches = matching_count;
                        if(matching_count == 0){
                          $(".all-content").hide();
                          $("#content-tab-link").hide();
                          $('.content_container, #content-tab-link , #content-mbl-tab').remove();
                          if($('#product-tab-link').is(":hidden")){
                            $('.document_container, #document-tab-link').addClass('is-active');
                          }
                        }
                    }
                });
                var start_value = (requestingpage-1)*20;
                if(requestingpage ==1) {
                    start_value = 1;
                }
                var endvalue = requestingpage*20;
                if(start_value == 1 && endvalue > search_result.totalmatches){
                    // no pager
                }
                else if(endvalue > search_result.totalmatches){
                  endvalue = search_result.totalmatches;
                  $('.srp-result-content').html('<span class="srp-qty">Results ' + start_value + '-' + endvalue  + ' of ' + search_result.totalmatches +'</span>').attr('data-content-cnt', search_result.totalmatches);
                }
                else{
                  $('.srp-result-content').html('<span class="srp-qty">Results ' + start_value + '-' + endvalue  + ' of ' + search_result.totalmatches +'</span>').attr('data-content-cnt', search_result.totalmatches);
                }
                return search_result;
            }

            function applyPLPFacets(params, eventName) {
                $('body').addClass('loader-active');
                if (!$('.preloader').length) {
                    $('#plp-wrapper').prepend(
                      '<div class="preloader">' +
                        '<i class="input-loader"></i>' +
                      '</div>');
                }
                $('html, body').stop().animate({
                    scrollTop: $('.nav-tabs').offset().top
                }, 'slow');
                loader.show();

                $.ajax({
                    type: "GET",
                    url: "/search-output",
                    dataType: 'json',
                    data: {
                        title: _param,
                        categories: _type,
                        params: params['facets'],
                        size: "20",
                        page: params['pageNum'],
                    },

                    success: function (data) {

                        var prodView = new ProdView(data, eventName);
                      loader.hide();
                      loadProducts = 1;
                      var PaginationView = new paginationView(data);
                      PaginationView.undelegateEvents();

                    }
                });
            }

            function applydocFacets(params) {
                $('body').addClass('loader-active');
                if (!$('.preloader').length) {
                    $('#plp-wrapper').prepend('<div class="preloader"><i class="input-loader"></i></div>');
                }
                $('html, body').stop().animate({
                    scrollTop: $('#plp-wrapper').offset().top
                }, 'slow');
                $.ajax({
                    type: "GET",
                    url: "/search-doc-output",
                    dataType: 'json',
                    data: {
                        title: _param,
                        categories: _type,
                        page: params['pageNum'],
                    },
                    success: function (data) {
                        var DocView = new docView(data);
                        if(params['pageNum'] && data) {
                          data.currentpage = params['pageNum'];
                          data.first = params['first'];
                          data.last = params['last'];
                        }
                        if(params['action'] && data) {
                          data.action = params['action'];
                        }
                        var PaginationView = new docpaginationView(data);
                        PaginationView.undelegateEvents();

                        $('body').removeClass('loader-active');
                        $('body').removeClass('preloader');

                    }
                });
            }

            /*
            This function will be called when user hits on certain page(previous or next) on pagination.
            Fetches contents based on current search value and page number requested
            @params: pagenumber
             */
            function applycontFacets(params) {
                $('body').addClass('loader-active');
                if (!$('.preloader').length) {
                    $('.search-result-page').prepend('<div class="preloader"><i class="input-loader"></i></div>');
                }
                $('html, body').stop().animate({
                    scrollTop: $('.search-result-page').offset().top
                }, 'slow');
                var content_matchings = getContentSearchResults(_param, _mediaroomFilter, params);
                var ContView = new contView(content_matchings, (params['pageNum'] - 1));
                var ContpaginationView = new contpaginationView(content_matchings);
                ContpaginationView.undelegateEvents();
                $('body').removeClass('loader-active');
                $('body').removeClass('preloader');
            }


            $('.form-item__select').once().change(function(){
              let facet = $('#facetFinish').val();
              setFinishClass = facet;
              let sort  = $('#sortProduct').val();
              localStorage.setItem("selected-sort", sort);
              localStorage.setItem("selected-facet", facet);
              pageNumber = 0;
              loader.show();
              $.ajax({
                type: "GET",
                url: "/search-output",
                dataType: 'json',
                data: {
                  title: _param,
                  categories: _type,
                  facet: facet,
                  sort : sort,
                  page: pageNumber
                  //documentLookFor: _filter,
                },
                complete: function () {
                  $('.srp-result-dropdowns #facetFinishAll').val(facet);
                  $('.srp-result-dropdowns #sortProductAll').val(sort);
                },
                success: function (data) {
                  var prodView = new ProdView(data);
                  facetView(data.facets, 'facetFinish');

                  loadProducts = 1;
                  var PaginationView = new paginationView(data);
                  loader.hide();
                }
              });

            })
            $(window).once().scroll(function () {
            if (($(window).scrollTop() + $(window).height() > $('#plp-wrapper').height())
              && loadProducts && (!lastPage)) {
              loadProducts = false;
              totalResult = 0;
              let facet = $('#facetFinish').val();
              let sort  = $('#sortProduct').val();
              // On Scroll to achieve lazyload functionality.
              var _param = getSearchParams('title');
              var _type = getSearchParams('type');

              // var _filter = getSearchParams('documentLookFor');
              loader.show();

              pageNumber = pageNumber + 1;
              $.ajax({
                type: "GET",
                url: "/search-output",
                dataType: 'json',
                data: {
                  title: _param,
                  categories: _type,
                  facet: facet,
                  sort : sort,
                  page:pageNumber
                },
                success: function (data) {
                  if(data.totalElements > data.size) {
                    var prodView = new ProdView(data, null, 1);
                  }
                  loadProducts = true;
                  lastPage = data.last;
                  loader.hide();
                }
              });
            }
          });

          var tab = null;
          var tabTrigger = function(tab) {
              var offset = jQuery('#tabs').offset().top - 100;
              jQuery('html, body').animate({ scrollTop: offset });
              jQuery("a[href='"+tab+"']")[0].click();

          }
          var tabsmobile = null;
          var tabTriggerMobile = function(tabsmobile) {
            if ($(window).width() < 768) {
                var offsetmobile = jQuery('.tab-accordion-title').offset().top - 60;
                jQuery('html, body').animate({ scrollTop: offsetmobile });
            }
          }

          $('.tabActivate').click(function(){
            tab = $(this).attr('href');
            tabTrigger(tab);
            tabTriggerMobile(tabsmobile);
          });

          $('#facetFinishAll').change(function(){
            var facetFilter = $(this).val();
            tabTrigger('#tab-0');
            $('#facetFinish').val($(this).val());
            $('#facetFinish').trigger('change');
          });

          $('#sortProductAll').change(function(){
            var sortFilter = $(this).val();
            tabTrigger('#tab-0');
            $('#sortProduct').val($(this).val());
            setTimeout(function(){
              $('#sortProduct').trigger('change');
            }, 1500);
          });

          $(".slp_tabs li").click(function(event) {
            if($('#product-tab-link').length == 0) {

              $(".slp_tabs li a").removeClass("is-active");
              $(this).find("a").addClass("is-active");
              $(".tabs__tab").removeClass("is-active");
              $(".tab-accordion-title").removeClass('active-tab-title');
              $(".tabs__tab").eq($(this).index()).addClass("is-active");
              $(".tabs__tab").eq($(this).index()).prev().addClass("active-tab-title");
              return false;
            }
          });

          setTimeout(function() {
            if($('.path-search-results .tabs__nav li').length == 2) {
              $('.path-search-results .tabs__nav li .tabs__link').addClass('is-active');
              $('.allResult.is-active .all-tab-dropdown').css('display','none');
              $('.allResult.is-active .srp-results__header').css('padding-bottom','20px');
              $(".tab-accordion-title.allResult").addClass("active-tab-title");
              $('.allResult.is-active .srp-result-summary').css('text-align','left');
              $('.allResult.is-active .srp-result-summary + div').css('text-align','right');
            }
          },2000);

          setTimeout(function() {
          if($('#document-tab-link .count').attr('count') == 0) {
            $('#document-tab-link').remove();
          }
        },2000);

        }

    };
})(jQuery, Drupal, drupalSettings, Backbone);

(function($, Drupal, Backbone) {
  /**
   * Add new command for reading a message.
   */
  Drupal.AjaxCommands.prototype.searchMediaRoom = function(ajax, response, status){
    let totalResult = 0;
    _mediaroomFilter = response.type;
    _param = response.title;
    var page = [];
    page['pageNum'] = 1;

    /*************************/
    // Pagination fot content section.
    var contpaginationView = Backbone.View.extend({
      el: ".content-pagination-output",
      template: _.template($("#content-pagination").html()),
      initialize: function (initData) {
        this.render(initData);
      },
      events: {
        'click [type="button"]': 'contentpagination'
      },
      contentpagination: function (e) {
        var params = [];
        params['pageNum'] = parseInt($(e.currentTarget).val()); // Current
        params['first'] = $(e.currentTarget).attr('first');
        params['last'] = $(e.currentTarget).attr('last');
        if($(e.currentTarget).val() === '>') {
          params['pageNum'] = $('.pager__link.is-active').val();
          params['action'] = 'next';
        }
        if($(e.currentTarget).val() === '>>') {
          var total = $(e.currentTarget).attr('total');
          params['pageNum'] = parseInt(total);
          params['first'] = parseInt(total)-4;
          if(parseInt(total) == 4){
            params['first'] = 1;
          }
          params['last'] = parseInt(total);
        }
        if($(e.currentTarget).val() === '<') {
          params['pageNum'] = $('.pager__link.is-active').val();
          params['action'] = 'prev';
        }
        if($(e.currentTarget).val() === '<<') {
          params['pageNum'] = 1;
          params['first'] = 1;
          params['last'] = 5;
        }

        applycontFacets(params);
      },
      render: function (initData) {
        if (initData === null) {
          $('.documentoutput').html('<h4>No results at this time.</h4>');
          return false;
        }
        var cont_disp_cnt = 20;//drupalSettings.cont_disp_cnt;
        if (cont_disp_cnt == null) {
          cont_disp_cnt = 10;
        }
        var _self = this;
        var _render = "";
        var _currentpage = initData.currentpage;  // Current page
        var _totalmatches = initData.totalmatches;
        var _totalpages = Math.ceil(_totalmatches / cont_disp_cnt);
        var _nextPage;
        var _first;
        if (_currentpage < _totalpages) {
          _nextPage = _currentpage + 1;
        } else {
          _nextPage = _currentpage;
        }
        var _previousPage;
        if (_currentpage === 1) {
          _previousPage = 1;
          _first = 1;
        } else {
          _previousPage = _currentpage - 1;
          _first = initData.first;
        }
        if(_currentpage>=initData.first && _currentpage<=initData.last) {
          _first = parseInt(initData.first);
        }
        if(initData.action && initData.action == 'next') {
          if(_currentpage>=initData.first && _currentpage<initData.last) {
            _first = parseInt(initData.first);
          } else {
            _first = parseInt(initData.first) + 1;
          }
          _currentpage = parseInt(_currentpage) + 1;
        }
        if(initData.action && initData.action == 'prev') {
          if(_currentpage>initData.first && _currentpage<=initData.last) {
            _first = parseInt(initData.first);
          } else {
            _first = parseInt(initData.first) - 1;
          }
          _currentpage = parseInt(_currentpage) - 1;
        }
        var pagination = parseInt(_first)+4;
        if(_totalpages < pagination) {
          pagination = _totalpages;
        }
        if(_currentpage != 1 && _totalpages > 4) {
          _render += '<input total="'+_totalpages+'" class="pager__link first" type="button" data="" value="<<">';
          _render += '<input first="'+_first+'" last="'+pagination+'" class="pager__link prev" type="button" data="" value="<">';
        }
        for(var i=_first;i<=pagination;i++) {
          /*_render += _self.template({
            pageNumber: i,
            currentPage: _currentpage,
            totalRecordset: initData.totalElements
          });*/
          var classValue = '';
          if(i === parseInt(_currentpage)) {
            classValue = 'is-active';
          }

          _render += '<input first="'+_first+'" last="'+pagination+'" class="pager__link '+ classValue +'" type="button" value="'+ i +'">';

        }
        if(_currentpage != _totalpages && _totalpages > 4) {
          _render += '<input first="'+_first+'" last="'+pagination+'" class="pager__link next" type="button" data="" value=">">';
          _render += '<input total="'+_totalpages+'" class="pager__link last" type="button" data="" value=">>">';
        }
        if (_totalpages > 1 ) {
          _self.$el.html(_render);
        }
      }
    });

    // Process content data and assemble the page.
    var contView = Backbone.View.extend({
      el: ".contentoutput",
      el1: ".contentoutputfull",
      template: _.template($("#content-output").html()),
      template1: _.template($("#full-content-output").html()),
      initialize: function (content, _sno) {
        if (content.currentpage === 1) {
          this.renderAllsectionContent(content, _sno);
        }
        this.renderContentSection(content, _sno);
      },
      renderAllsectionContent: function (content, _sno) {
        var _self = this;
        var _render = "";
        if (content.totalmatches > 0) {
          var matches = content.matches.slice(0, 8);
          var counter = _sno * 20;
          var _incrCnter = counter + 1;
          _.each(matches, function (entry) {
            var _title = entry.title;
            var _body = entry.search_api_excerpt;
            var _url = entry.url;
            _render += _self.template({
              searchContentBody: _body,
              searchContentUrl: _url,
              searchContentTitle: _title,
              searchSno: _incrCnter,
              searchPage: _mediaroomFilter,
            });
            _self.$el.html(_render);
            _incrCnter++;
          });
        }
        $('.tab-content-title-cont').show();
        recalculate();
        return this;
      },
      renderContentSection: function (content, _sno) {
        var _sno = parseInt(_sno);
        var _self = this;
        var _render = "";
        if (content.totalmatches > 0) {
          var matches = content.matches;
          var counter = _sno * 20;
          var _incrCnter = counter + 1;
          _.each(matches, function (entry) {
            var _title = entry.title;
            var _body = entry.search_api_excerpt;
            var _url = entry.url;
            _render += _self.template1({
              searchContentBody: _body,
              searchContentUrl: _url,
              searchContentTitle: _title,
              searchSno: _incrCnter,
              searchPage: _mediaroomFilter,
            });
            $('.preloader').remove();
            $('body').removeClass('loader-active');
            _incrCnter++;
          });
          $('#fullcontentoutput').html(_render);
          $('.content-all-search-result').html(_render);
        }
        return this;
      }
    });

    /*************************/
    // Javascript method called from drupal form submit
    var content_matchings = getContentSearchResults(_param, _mediaroomFilter, page);
    if (content_matchings.totalmatches !== undefined) {
      _totalCount = content_matchings.totalmatches;
    }
    $('#content-count-tab,#content-mbl-cnt').html('(' + _totalCount + ')');
    $('#content-count-tab,#content-mbl-cnt').attr('count', _totalCount);
    $('#srp-result-content').html('<span class="srp-qty">' + _totalCount + '</span> Contents').attr('data-content-cnt', _totalCount);
    if (_totalCount > 0) {
      var ContView = new contView(content_matchings, 0);
      var ContpaginationView = new contpaginationView(content_matchings);
      $('.media-room-default-content-block').hide();
    } else {
      $('#content-tab .row').html("<b>Nothing found. Please refine your search.</b>");
      var typedKeyword = response.title;
      var noResultsMessage = "<div class='noResults'>We're sorry, we didn't find any results for '" + decodeURIComponent(typedKeyword) + "'. We recommend using different words in your search.</div>";
      $('#fullcontentoutput').html(noResultsMessage);
      $('.media-room-default-content-block').hide();
      $('.content-pagination-output,.srp-result-content').html('');
      $('.srp-result-content').removeAttr('data-content-cnt');
      $("#contentTabLink").hide();
      $(".contentResult").hide();
      $("#search-content-title").hide();
      $(".content-see-all").hide();
    }

    /*
    To return content search results based on current search value and requesting page based on previous or next button hits in pagination.
    @params: search value, requesting page
    @return: search result
     */
    function getContentSearchResults(params, mediaroomFilter, arguments) {
      var search_result = [];
      requestingpage = parseInt(arguments['pageNum']);
      //search_result.currentpage = requestingpage;
      if(arguments['pageNum']) {
        search_result.currentpage = arguments['pageNum'];
        search_result.first = arguments['first'];
        search_result.last = arguments['last'];
      }
      if(arguments['action']) {
        search_result.action = arguments['action'];
      }
      var matching_count = 0;
      switch(mediaroomFilter) {
        case 'media-tab':
          contentUrl = "/rest/export/search_content_media?_format=json";
          contentCountUrl = "/rest/export/matching_records_count_media?_format=json";
          break;
        case 'press-releases':
          contentUrl = "/rest/export/search_press_release?_format=json";
          contentCountUrl = "/rest/export/matching_records_count_media_press?_format=json";
          break;
        case 'in-the-news':
          contentUrl = "/rest/export/search_in_news?_format=json";
          contentCountUrl = "/rest/export/matching_records_count_media_news?_format=json";
          break;
      }
      $.ajax({
        type: "GET",
        url: contentUrl,
        dataType: 'json',
        async: false,
        data: {
          title: _param,
          page: requestingpage - 1,  // Since views counts from 0
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
          //showErrorMessage(XMLHttpRequest.status, '#document-tab');
        },
        beforeSend: function () {
          $('body').addClass('loader-active');
          if ($('.preloader').length === 0) {
            $('#plp-wrapper').prepend('<div class="preloader"><i class="input-loader"></i></div>');
          }
        },
        complete: function () {
          $('.preloader').remove();
          $('body').removeClass('loader-active');
        },
        success: function (data) {
          var matches = [];
          $.each(data, function (key, value) {
            matches.push(value);
          });
          search_result.matches = matches;

          $.ajax({
            type: "GET",
            url: contentCountUrl,
            dataType: 'json',
            async: false,
            data: {
              title: _param,
            },
            success: function (data) {
              matching_count = data.length;
            }
          });

          search_result.totalmatches = matching_count;
          if(matching_count == 0){
            $(".all-content").hide();
            $("#content-tab-link").hide();
            $('.content_container, #content-tab-link , #content-mbl-tab').remove();
            if($('#product-tab-link').is(":hidden")){
              $('.document_container, #document-tab-link').addClass('is-active');
            }
          }
        }
      });
      var start_value = (requestingpage-1)*20;
      if(requestingpage ==1) {
        start_value = 1;
      }
      var endvalue = requestingpage*20;
      if(start_value == 1 && endvalue > search_result.totalmatches){
        // no pager
      }
      else if(endvalue > search_result.totalmatches){
        endvalue = search_result.totalmatches;
        $('.srp-result-content').html('<span class="srp-qty">Results ' + start_value + '-' + endvalue  + ' of ' + search_result.totalmatches +'</span>').attr('data-content-cnt', search_result.totalmatches);
      }
      else{
        $('.srp-result-content').html('<span class="srp-qty">Results ' + start_value + '-' + endvalue  + ' of ' + search_result.totalmatches +'</span>').attr('data-content-cnt', search_result.totalmatches);
      }
      return search_result;
    }
    /**
     * Recalculate tab total counts of records
     */
    function recalculate() {
      var grandTotal = 0;
      var data_document_cnt = 0;
      var data_product_cnt = 0;
      var data_content_cnt = 0;
      var data_all_cnt = 0;

      if($(".product-count-tab").attr("count")) {
        data_product_cnt = $(".product-count-tab").attr("count");
        data_product_cnt = parseInt(data_product_cnt);
      }
      if($("#content-count-tab").attr("count")) {
        data_content_cnt = $("#content-count-tab").attr("count");
        data_content_cnt = parseInt(data_content_cnt);
      }
      if($("#doc-count-tab").attr("count")){
        data_document_cnt = $("#doc-count-tab").attr("count");
        data_document_cnt = parseInt(data_document_cnt);
      }
      if($("#search-all-count-tab").attr("count")){
        data_all_cnt = $("#search-all-count-tab").attr("count");
        data_all_cnt = parseInt(data_all_cnt);
      }

      grandTotal = data_document_cnt + data_product_cnt + data_content_cnt;
      if(!$.isNumeric(grandTotal)) {
        grandTotal = 0;
      }
      $('#grand_total').html(grandTotal);
      $('#search-all-count-tab,#all-mbl-cnt').html('('+ grandTotal + ')');
      if (grandTotal === 0) {
        $("#emptyResults").show();
        $('.search-container').hide();

        $('.tab-content-title-doc').hide();
        $('.tab-content-title-cont').hide();
        $('.tab-content-title-prod').hide();

      }else{
        $("#emptyResults").hide();
        $('.search-container').show();
      }
    }
    /*
    This function will be called when user hits on certain page(previous or next) on pagination.
    Fetches contents based on current search value and page number requested
    @params: pagenumber
     */
    function applycontFacets(params) {
      $('body').addClass('loader-active');
      if (!$('.preloader').length) {
        $('.search-result-page').prepend('<div class="preloader"><i class="input-loader"></i></div>');
      }
      $('html, body').stop().animate({
        scrollTop: $('.media-room-pagination-root').offset().top-60
      }, 'slow');
      var content_matchings = getContentSearchResults(_param, _mediaroomFilter, params);
      var ContView = new contView(content_matchings, (params['pageNum'] - 1));
      var ContpaginationView = new contpaginationView(content_matchings);
      ContpaginationView.undelegateEvents();
      $('body').removeClass('loader-active');
      $('body').removeClass('preloader');
    }
  }
})(jQuery, Drupal, Backbone);

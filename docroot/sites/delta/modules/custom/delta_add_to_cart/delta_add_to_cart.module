<?php

/**
 * @file
 * Contains delta_add_to_cart.module.
 */

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\delta_add_to_cart\Helper\AjaxCartHelper;
use Drupal\commerce_cart\Form\AddToCartFormInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\commerce_product\Entity\Product;
use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\commerce_cart\CartManagerInterface;
use Drupal\commerce_cart\CartProviderInterface;
use Drupal\commerce_order\Entity\Order;
use Drupal\Core\Ajax\AppendCommand;
use Drupal\Core\Ajax\RemoveCommand;
/**
 * Implements hook_help().
 */
function delta_add_to_cart_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the delta_add_to_cart module.
    case 'help.page.delta_add_to_cart':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Module to place items to cart with ajax functionality.') . '</p>';
      return $output;

    default:
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function delta_add_to_cart_module_implements_alter(&$implementations, $hook) {
  switch ($hook) {
    case 'form_alter':
      unset($implementations['ajax_add_to_cart']);
      break;
  }
}

/**
 * Define changes for standard forms.
 */
function delta_add_to_cart_form_alter(&$form, &$form_state, $form_id) {
  // Check if the form builder implements the AddToCartFormInterface.
  if ($form_state->getBuildInfo()['callback_object'] instanceof AddToCartFormInterface) {
    // Using getInstanace method to create Object.
    $object = AjaxCartHelper::getInstance();
    $object->ajaxAddToCartAjaxForm($form_id, $form);
  }
  if (strpos($form_id, 'commerce_order_item_add_to_cart_form_commerce_product') !== FALSE) {
    $form['#validate'][] = 'maximim_quantity_check';

    $form['actions']['submit']['#prefix'] = "<div class='pdp-btn-addcart'>";
    $form['actions']['submit']['#suffix'] = "</div>";
    if(isset($form['quantity'])) {
      $recommerce = "";
      $config = \Drupal::config('delta.max_product_qty');
      $current_path = \Drupal::service('path.current')->getPath();
      $params = \Drupal\Core\Url::fromUserInput($current_path)->getRouteParameters();
      $entity_type = key($params);
      if($entity_type){

        $entity = \Drupal::entityTypeManager()
          ->getStorage($entity_type)
          ->load($params[$entity_type]);
        $max_product_qty = $config->get('max_qty_delta');
        if($entity->hasField('field_refurbished_product') &&
        $entity->get('field_refurbished_product')->value == 1){
          $max_product_qty = $config->get('max_qty_recommerce');
          $recommerce = "recommerce";
        }
      }
      $form['quantity']['widget'][0]['value']['#attributes']['class'][] = "numeric-stepper__input";
      $form['quantity']['widget'][0]['value']['#attributes']['data-min'] = "0";
      $form['quantity']['widget'][0]['value']['#attributes']['data-max'] = $max_product_qty;
      $form['quantity']['widget'][0]['value']['#attributes']['aria-label'] = "Select Quantity";
      $form['quantity']['#prefix'] = '<div class="numeric-stepper '.$recommerce.'"><div class="btn-increment-decrement numeric-stepper__button js-numeric-decrement" aria-hidden="true">–</div>';
      $form['quantity']['#suffix'] = '<div class="btn-increment-decrement numeric-stepper__button js-numeric-increment" aria-hidden="true">+</div></div>';
    }
  }
}

/**
 * PDP Addtocart quantity validation.
 */
function maximim_quantity_check($form, &$form_state) {

  $formFields = $form_state->getValues();
  $sku = "";
  $recommerce = 0;
  //Getting PDP form Fields.
  $productQty = $formFields['quantity'][0]['value'];
  if(!empty($formFields['purchased_entity'][0]['variation'])) {
    $pageVariation = ProductVariation::load($formFields['purchased_entity'][0]['variation']);
    $sku = $pageVariation->sku->value;
  }

  //Getting configuration max quantity and Error Msg
  $config = \Drupal::config('delta.max_product_qty');
  $maxCommerceQty = $config->get('max_qty_delta');
  $maxCommerceErr = $config->get('qty_delta_error_msg');
  $maxReCommerceQty = $config->get('max_qty_recommerce');
  $maxReCommerceErr = $config->get('qty_recommerce_error_msg');

 // Checking recertified produts
  $current_path = \Drupal::service('path.current')->getPath();
  $params = \Drupal\Core\Url::fromUserInput($current_path)->getRouteParameters();
  $entity_type = key($params);
  if($entity_type){
    $entity = \Drupal::entityTypeManager()->getStorage($entity_type)->load($params[$entity_type]);
    if($entity->hasField('field_refurbished_product') &&
    $entity->get('field_refurbished_product')->value == 1){
      $recommerce = 1;
    }
  }

  //Loadind current cart and checking with existing PDP page Quantity
  $msg = "";
  $cartIds = \Drupal::service('commerce_cart.cart_provider')->getCartIds();
  if(!empty($cartIds)) {
    $currentOrders = Order::load($cartIds[0]);
    $orderList = $currentOrders->getItems();
    foreach ($orderList as $key => $value) {
      $cartQuantity = (int)$value->quantity->value;
      if(!empty($value->purchased_entity->target_id)) {
        $cartVariation = ProductVariation::load($value->purchased_entity->target_id);
        if($cartVariation->sku->value == $sku) {
          // Checking recertified product max quantity
          if($recommerce == 1) {
            if($maxReCommerceQty > $cartQuantity) {
              $availableLimit = $maxReCommerceQty-$cartQuantity;
              if($availableLimit != 0) {
                if($availableLimit != $productQty) {
                  $msg = $maxReCommerceErr."-"."1";
                }
              }
              else {
                $msg = $maxReCommerceErr."-"."0";
              }
            }
            else {
              $msg = $maxReCommerceErr."-"."0";
            }
          }
          else {
            // Checking non recertified product max quantity
            if($maxCommerceQty >  $cartQuantity) {
              $availableLimit = $maxCommerceQty-$cartQuantity;
              if($availableLimit != 0) {
                if($availableLimit != $productQty) {
                  $msg = $maxCommerceErr."-"."1";
                }
              }
              else {
                $msg = $maxCommerceErr."-"."0";
              }
            }
            else {
              $msg = $maxCommerceErr."-"."0";
            }
          }
        }
      }
    }
  }

 // Setting Error message
  if(!(empty($msg))) {
    $form_state->setError($form['quantity'], $msg);
  }
}

/**
 * Define a validation for forms.
 */
function delta_add_to_cart_ajax_validate(&$form, $form_state) {
  $errorMsg = $form_state->getErrors();
  $response = new AjaxResponse();
  $form_id = $form_state->getUserInput()['form_id'];
  if ($form_state->hasAnyErrors()) {
    $errors = explode("-",$errorMsg['quantity_wrapper']);
    // If form is having errors, rebuild form.
    $Selector = '.numeric-stepper';
    $Content = "<span class='quantity-err'>".$errors[0]."</span>";
    $Settings = ["hide"=>$errors[1]];
    $response->addCommand(new RemoveCommand(".quantity-err"));
    $response->addCommand(new AppendCommand($Selector, $Content, $Settings));
    //$response->addCommand(new ReplaceCommand('#modal_ajax_form_' . $form_id, $form));
  }
  else {
    // Using getInstanace method to create Object.
    $object = AjaxCartHelper::getInstance();
    // If validated successfully submit form.
    $object->ajaxAddToCartAjaxResponse($form_id, $response);
  }
  \Drupal::service('messenger')->deleteByType('status');
  return $response;
}

<?php

namespace Drupal\general\Controller;

/**
 * Implementation of Header and Footer controller.
 */

use Drupal\Core\Controller\ControllerBase;

/**
 * Defines a route controller for entity autocomplete form elements.
 */
class GlobalHeaderFooterController extends ControllerBase {
  /**
  * Returns a Global Header page.
  *
  * @return array
  *   A simple renderable array.
  */
   public function globalHeader() {
     return [
       '#markup' => 'global Header content',
     ];
   }

   /**
   * Returns a Global Header page.
   *
   * @return array
   *   A simple renderable array.
   */
    public function globalFooter() {
      return [
        '#markup' => 'global Footer content',
      ];
    }

    /**
    * Returns a Global Css page.
    *
    * @return array
    *   A simple renderable array.
    */
     public function globalCss() {
       return [
         '#markup' => 'global Css content',
       ];
     }
}

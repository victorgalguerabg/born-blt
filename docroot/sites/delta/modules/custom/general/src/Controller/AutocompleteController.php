<?php

namespace Drupal\general\Controller;

/**
 * Implementation of autocomplete controller.
 */

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Component\Utility\Tags;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\delta_services\DeltaService;

/**
 * Defines a route controller for entity autocomplete form elements.
 */
class AutocompleteController extends ControllerBase {
  /**
   * Drupal\delta_services\DeltaService definition.
   *
   * @var \Drupal\delta_services\DeltaService
   */
  protected $deltaservice;

  /**
   * Class constructor.
   */
  public function __construct(DeltaService $deltaservice) {
    $this->deltaservice = $deltaservice;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
    // Load the service required to construct this class.
        $container->get('delta_services.service')
    );
  }

  /**
   * Handler for autocomplete request.
   */
  public function handleAutocomplete(Request $request, $field_name) {
    $results = [];

    // Get the typed string from the URL, if it exists.
    if ($input = $request->query->get('q')) {
      $typed_string = Tags::explode($input);
      $typed_string = strtolower(array_pop($typed_string));
      $results = $this->getSkuList($typed_string);
    }
    return new JsonResponse($results);
  }

  /**
   * Mplementation of autocomplete from product api.
   */
  public function getSkuList($searchKey) {
    $base_config = $this->config('delta_services.deltaservicesconfig');
    $base_url = $base_config->get('api_base_path');
    $config = $this->config('general.deltaservicesconfig');
    $domain = $config->get('source_site');
    $searchUrl = $config->get('api_contact_us_autocomplete');
    $searchPartsUrl = $base_url . $searchUrl;
    $url = str_replace("{searchKey}", $searchKey, $searchPartsUrl);
    $productRequest = $this->deltaservice->apiCall($url, "GET", [], "", "SKU Search");
    if ($productRequest['status'] == 'SUCCESS') {
      $response = $productRequest['response'];
      $responseProdData = json_decode($response, TRUE);
      foreach ($responseProdData['content'] as $readSku) {
        $items[] = $readSku['values']['ModelNumber'];
        $results[] = [
          'value' => $readSku['values']['ModelNumber'],
          'label' => $readSku['values']['ModelNumber'],
        ];
      }
      return $results;
    }
    else {
      return ['Something went wrong, please try again later.'];
    }
  }

}

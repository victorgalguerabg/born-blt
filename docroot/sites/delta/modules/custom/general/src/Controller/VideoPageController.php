<?php

namespace Drupal\general\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Database\Connection;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\Response;

/**
 * Controller routines for user routes.
 */
class VideoPageController extends ControllerBase {


  /**
   * DB connection resource.
   *
   * @var connection
   */
  protected $connection;

  /**
   * To get path alias.
   *
   * @var service
   */
  public $service;

  /**
   * To get current query string.
   *
   * @var Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * To handle response.
   *
   * @var response
   */
  public $response;

  /**
   * Class constructor.
   */
  public function __construct(RequestStack $requestStack, Connection $connection, ContainerInterface $service) {
    $this->requestStack = $requestStack;
    $this->connection = $connection;
    $this->service = $service;
    $this->response = new Response();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
    // Load the service required to construct this class.
      $container->get('request_stack'),
      $container->get('database'),
      $container->get('service_container')
    );
  }

  /**
   * Fetch the video id and redirect to video page.
   */
  public function videoPage() {
    $GetVideoId = $this->requestStack->getCurrentRequest()->get('videoId');
    $videoId = strip_tags($GetVideoId);
    $database = $this->connection->select('node__field_video_id', 'video');
    $nid = $database
      ->fields('video', ['entity_id'])
       ->condition('field_video_id_value', $videoId)
      ->condition('bundle', 'delta_video')
      ->execute()
      ->fetchField();
    if (!empty($nid)) {
      $alias_path = $this->service->get('path_alias.manager')->getAliasByPath('/node/' . $nid);
      $response = new RedirectResponse($alias_path);
      return $response->send();
    } else{
       throw new NotFoundHttpException();
    }
  }

  public function Professionalvideo() {
    $nodeUrl = $this->requestStack->getCurrentRequest()->get('url');
    $alias_path = $this->service->get('path_alias.manager')->getPathByAlias($nodeUrl);
    $params = Url::fromUri("internal:" . $alias_path)->getRouteParameters();
    $entity_type = key($params);
    $node = \Drupal::entityTypeManager()->getStorage($entity_type)->load($params[$entity_type]);
    $student = \Drupal::entityTypeManager()
      ->getViewBuilder('node')
      ->view($node);
    $node_object = render($student);
    $data['result'] = $node_object;
    $result = json_encode($data);
    $this->response->setContent($result);
    $this->response->headers->set('Content-Type', 'application/json');
    return $this->response;
  }
}

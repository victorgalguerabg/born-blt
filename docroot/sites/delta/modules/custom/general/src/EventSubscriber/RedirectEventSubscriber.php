<?php
namespace Drupal\general\EventSubscriber;

use Drupal\Core\Url;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;

class RedirectEventSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::REQUEST][] = ['redirectExternalPath', 100];
    return $events;
  }

  public function redirectExternalPath(GetResponseEvent $event) {
    $skuNumber = '';
    $currentPath = \Drupal::service('path.current')->getPath();
    if(strpos($currentPath, 'own/')) {
      $urlParts = explode("/", $currentPath);
      $totalPaths = count($urlParts);
      $skuNumber = $urlParts[$totalPaths-1];
    }
    if($skuNumber) {
      $url = '/product-detail/'.$skuNumber;
      $event->setResponse(new RedirectResponse($url));
    }
  }
}

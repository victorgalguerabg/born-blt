<?php

namespace Drupal\general\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Session\AccountProxy;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Archiver\Zip;
use Drupal\file\Entity\File;
use Drupal\media\Entity\Media;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Link;
use Drupal\Core\Extension\ModuleHandlerInterface;
use \Drupal\node\Entity\Node;
use Drupal\pathauto\PathautoState;

/**
 * Implementation of contact us form.
 */
class DesignFileUploadForm extends Formbase {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The 'file_system' service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The 'language_manager' service.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $language_manager;

  /**
   * The 'current_user' service.
   *
   * @var \Drupal\Core\Session\AccountProxy
   */
  protected $current_user;

  /**
   * The 'messenger' service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The 'module_handler' service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $module_hander;

  /**
   * Class constructor.
   */
  public function __construct(EntityTypeManagerInterface $entitytypemanager, FileSystemInterface $file_system, LanguageManagerInterface $language_manager, AccountProxy $current_user, MessengerInterface $messenger, ModuleHandlerInterface $module_hander) {
    $this->entityTypeManager = $entitytypemanager;
    $this->fileSystem = $file_system;
    $this->languageManager = $language_manager;
    $this->currentUser = $current_user;
    $this->messenger = $messenger;
    $this->moduleHandler = $module_hander;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
    // Load the service required to construct this class.
      $container->get('entity_type.manager'),
      $container->get('file_system'),
      $container->get('language_manager'),
      $container->get('current_user'),
      $container->get('messenger'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'design_file_upload_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config_file_path = '/' . $this->moduleHandler->getModule('general')->getPath() . '/config.csv';
    $url = Url::fromUserInput($config_file_path, ['attributes' => ['target' => '_blank']]);
    $link = Link::fromTextAndUrl($this->t('Download'), $url)->toString()->getGeneratedLink();

    $form = array(
      '#attributes' => array('enctype' => 'multipart/form-data'),
    );
    $validators = array(
      'file_validate_extensions' => array('zip'),
    );
    $form['zip_file'] = [
      '#type' => 'managed_file',
      '#name' => 'zip_file',
      '#title' => t('File *'),
      '#size' => 20,
      '#description' => t('Zip format only'),
      '#upload_validators' => $validators,
      '#upload_location' => 'public://design-files/',
    ];
    $form['config_template'] = [
      '#markup' => "<div>$link template for field mapping. We need to zip the files not the folder. Design file name on the config.csv should be same on files zipped. Please don't change the config.csv file header.</div>"
    ];
    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Upload'),
      '#button_type' => 'primary',
    );
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if ($form_state->getValue('zip_file') == NULL) {
      $form_state->setErrorByName('zip_file', $this->t('File.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // In this variable you will have file entity
    $file = $this->entityTypeManager->getStorage('file')->load($form_state->getValue('zip_file')[0]);
    $file->status = FILE_STATUS_PERMANENT;
    $file->save();
    $absolute_path = $this->fileSystem->realpath($file->getFileUri());
    $zip = new Zip($absolute_path);

    //create folder with parent zip file name to avoid naming problem if we upload file with same name
    $path_info = pathinfo($file->getFileUri());

    $folder_name = basename($file->getFileName(), '.' . $path_info['extension']);

    //Extracting files from zip archive
    $folder_uri = 'public://design-files/' . $folder_name;
    $zip->extract($folder_uri);
    $realpath = $this->fileSystem->realpath($folder_uri);
    $config_files = $this->fileSystem->scanDirectory($realpath, '/config.csv$/');
    $config = reset($config_files);
    $row_count = 0;
    $fp = fopen($config->uri, 'r');
    $head = fgetcsv($fp, 4096, ',');
    $terms = $this->entityTypeManager->getStorage("taxonomy_term")->loadTree('product_design_file_tabs');
    foreach ($terms as $term) {
      $term_data[$term->name] = $term->tid;
    }
    $row = 0;
    while($column = fgetcsv($fp, 4096)){

      $column = array_combine($head, $column);

      if (isset($column['Design File'])) {
        if (isset($column['File Category'])) {
          $category = explode(',', $column['File Category']);
          $tids = [];
          foreach ($category as $key => $value) {
            $name = trim($value);
            $tids[] = $term_data[$name];
          }
        }

        $paragraph_items[$column['Design File']] = [
          'Admin title' => $column['Admin title'],
          'File Category' => !empty($tids) ? $tids : ''
        ];
      }
    }

    if (!empty($zip->listContents())) {
      // Create media entity with saved file.
      $search_key = explode('_', $folder_name);
      $search_key = end($search_key);
      if ($folder_name != $search_key) {
        $search_key = rtrim($folder_name, '_' . $search_key);
      }
      $node_name = $search_key;
      $search_key = str_replace(' ', '-', $search_key);
      $search_key = strtolower($search_key);
      $file_media = Media::create([
        'bundle' => 'design_files',
        'name' => $node_name,
        'uid' => $this->currentUser->id(),
        'langcode' => $this->languageManager->getDefaultLanguage()->getId(),
        'status' => 1,
        'field_design_file_search_key' => $search_key,
       ]);

      foreach ($zip->listContents() as $key => $file_name) {
        $file_uri = 'public://design-files/' . $folder_name . '/' . $file_name;

        if ($file_name != 'config.csv') {
          // Create file object with extracted zip files uri
          $file = File::create([
            'uid' => 1,
            'filename' => $file_name,
            'uri' => $file_uri,
            'status' => 1,
          ]);
          $file->save();
          // Create paragraph item
          $paragraph = Paragraph::create([
            'type' => 'product_design_files',
            'status' => TRUE,
          ]);
          $admin_title = isset($paragraph_items[$file_name]['Admin title']) ? $paragraph_items[$file_name]['Admin title'] : '';

          $file_category = isset($paragraph_items[$file_name]['File Category']) ? $paragraph_items[$file_name]['File Category'] : '';

          $paragraph->set('admin_title', $admin_title);
          $paragraph->set('field_file_category', $file_category);
          $paragraph->set('field_design_file', $file->id());
          $paragraph->isNew();
          $paragraph->save();

          $file_media->field_design_files_mapping[] = [
            'target_id' => $paragraph->id(),
            'target_revision_id' => $paragraph->getRevisionId()
          ];
        }
      }
      $file_media->save();
      // Create node object with attached file.
      $paragraph = Paragraph::create([
        'type' => 'delta_banner',
        'status' => TRUE,
      ]);
      $paragraph->set('admin_title', $node_name);
      $paragraph->set('field_page_title_formatted', $node_name);
      $paragraph->isNew();
      $paragraph->save();
      $node = Node::create([
        'type' => 'page',
        'title' => $node_name,
        'status' => TRUE,
        'path' => [
          'alias' => '/for-professionals/product-resources/product-design-files/' . $file_media->field_design_file_search_key->value,
          'pathauto' => PathautoState::SKIP,
        ]
      ]);
      $node->field_banner_region_content[] = [
        'target_id' => $paragraph->id(),
        'target_revision_id' => $paragraph->getRevisionId()
      ];
      $node->save();

      $url = Url::fromRoute('entity.media.edit_form',['media' => $file_media->id()], ['attributes' => ['target' => '_blank']]);
      $link = Link::fromTextAndUrl($this->t('Click here'), $url)->toString()->getGeneratedLink();

      $link = $this->t("Design file uploaded successfully. $link to edit the uploaded design file");

      $this->messenger->addMessage($link);
    }
  }
}

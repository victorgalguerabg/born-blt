
(function ($, Drupal) {
    'use strict';
    Drupal.behaviors.contactus = {
        attach: function (context, settings) {
           	$('.ac-model-number-single').hide();	
           	$("#edit-select-message-topic").change(function(){
           		var topicOption = $("#edit-select-message-topic").val();
           		if(topicOption == 'replacement-parts'
           			|| topicOption == 'cleaning-and-care'
           			|| topicOption == 'finish' ) {
	      			$('.ac-model-number-single').show();	
	      		}else{
	      			$('.ac-model-number-single').hide();
	      		}	
           	});
           	$('#edit-state').change(function(){
           		$("[for=edit-country]").parent().hide();
           	});
           	
        }
    };
})(jQuery, Drupal, drupalSettings);
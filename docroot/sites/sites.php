<?php

// @codingStandardsIgnoreFile

/**
 * @file
 * Configuration file for multi-site support and directory aliasing feature.
 *
 * This file is required for multi-site support and also allows you to define a
 * set of aliases that map hostnames, ports, and pathnames to configuration
 * directories in the sites directory. These aliases are loaded prior to
 * scanning for directories, and they are exempt from the normal discovery
 * rules. See default.settings.php to view how Drupal discovers the
 * configuration directory when no alias is found.
 *
 * Aliases are useful on development servers, where the domain name may not be
 * the same as the domain of the live server. Since Drupal stores file paths in
 * the database (files, system table, etc.) this will ensure the paths are
 * correct when the site is deployed to a live server.
 *
 * To activate this feature, copy and rename it such that its path plus
 * filename is 'sites/sites.php'.
 *
 * Aliases are defined in an associative array named $sites. The array is
 * written in the format: '<port>.<domain>.<path>' => 'directory'. As an
 * example, to map https://www.drupal.org:8080/mysite/test to the configuration
 * directory sites/example.com, the array should be defined as:
 * @code
 * $sites = [
 *   '8080.www.drupal.org.mysite.test' => 'example.com',
 * ];
 * @endcode
 * The URL, https://www.drupal.org:8080/mysite/test/, could be a symbolic link
 * or an Apache Alias directive that points to the Drupal root containing
 * index.php. An alias could also be created for a subdomain. See the
 * @link https://www.drupal.org/documentation/install online Drupal installation guide @endlink
 * for more information on setting up domains, subdomains, and subdirectories.
 *
 * The following examples look for a site configuration in sites/example.com:
 * @code
 * URL: http://dev.drupal.org
 * $sites['dev.drupal.org'] = 'example.com';
 *
 * URL: http://localhost/example
 * $sites['localhost.example'] = 'example.com';
 *
 * URL: http://localhost:8080/example
 * $sites['8080.localhost.example'] = 'example.com';
 *
 * URL: https://www.drupal.org:8080/mysite/test/
 * $sites['8080.www.drupal.org.mysite.test'] = 'example.com';
 * @endcode
 *
 * @see default.settings.php
 * @see \Drupal\Core\DrupalKernel::getSitePath()
 * @see https://www.drupal.org/documentation/install/multi-site
 */

// Lando Local sites
$sites['delta.lndo.site'] = 'delta';
$sites['peerless.lndo.site'] = 'peerless';
$sites['brizo.lndo.site'] = 'brizo';
$sites['delta9.lndo.site'] = 'delta';
$sites['peerless9.lndo.site'] = 'peerless';
$sites['brizo9.lndo.site'] = 'brizo';
$sites['deltafaucetcompany.lndo.site'] = 'company';
$sites['brizoanddelta.lndo.site'] = 'brizodelta';
$sites['discoverdfc.lndo.site'] = 'discover';

// Docksal Local sites
$sites['delta.docksal'] = 'delta';
$sites['brizo.delta.docksal'] = 'brizo';
$sites['peerless.delta.docksal'] = 'peerless';

// Delta
$sites['local.deltafaucet.com'] = 'delta';
$sites['dev2.deltafaucet.com'] = 'delta';
$sites['dev3.deltafaucet.com'] = 'delta';
$sites['dev4.deltafaucet.com'] = 'delta';
$sites['qat2.deltafaucet.com'] = 'delta';
$sites['stg2.deltafaucet.com'] = 'delta';
$sites['www.deltafaucet.com'] = 'delta';
$sites['deltafaucet.com'] = 'delta';

// Peerless
$sites['local.peerlessfaucet.com'] = 'peerless';
$sites['dev2.peerlessfaucet.com'] = 'peerless';
$sites['dev3.peerlessfaucet.com'] = 'peerless';
$sites['qat2.peerlessfaucet.com'] = 'peerless';
$sites['stg2.peerlessfaucet.com'] = 'peerless';
$sites['www.peerlessfaucet.com'] = 'peerless';
$sites['peerlessfaucet.com'] = 'peerless';

// Brizo
$sites['local.brizo.com'] = 'brizo';
$sites['dev2.brizo.com'] = 'brizo';
$sites['dev3.brizo.com'] = 'brizo';
$sites['dev4.brizo.com'] = 'brizo';
$sites['qat2.brizo.com'] = 'brizo';
$sites['stg2.brizo.com'] = 'brizo';
$sites['www.brizo.com'] = 'brizo';
$sites['brizo.com'] = 'brizo';

// Brizo Canada
$sites['local.brizofaucet.ca'] = 'brizo';
$sites['dev2.brizofaucet.ca'] = 'brizo';
$sites['dev3.brizofaucet.ca'] = 'brizo';
$sites['dev4.brizofaucet.ca'] = 'brizo';
$sites['qat2.brizofaucet.ca'] = 'brizo';
$sites['stg2.brizofaucet.ca'] = 'brizo';
$sites['www.brizofaucet.ca'] = 'brizo';
$sites['brizofaucet.ca'] = 'brizo';

// Company
$sites['local.deltafaucetcompany.com'] = 'company';
$sites['dev2.deltafaucetcompany.com'] = 'company';
$sites['dev3.deltafaucetcompany.com'] = 'company';
$sites['qat2.deltafaucetcompany.com'] = 'company';
$sites['stg2.deltafaucetcompany.com'] = 'company';
$sites['www.deltafaucetcompany.com'] = 'company';
$sites['deltafaucetcompany.com'] = 'company';

// Discover - DiscoverDFC.com
$sites['local.discoverdfc.com'] = 'discover';
$sites['dev2.discoverdfc.com'] = 'discover';
$sites['dev3.discoverdfc.com'] = 'discover';
$sites['qat2.discoverdfc.com'] = 'discover';
$sites['stg2.discoverdfc.com'] = 'discover';
$sites['www.discoverdfc.com'] = 'discover';
$sites['discoverdfc.com'] = 'discover';

// Brizo and Delta - Brizoanddelta.com
$sites['local.brizoanddelta.com'] = 'brizodelta';
$sites['dev2.brizoanddelta.com'] = 'brizodelta';
$sites['dev3.brizoanddelta.com'] = 'brizodelta';
$sites['qat2.brizoanddelta.com'] = 'brizodelta';
$sites['stg2.brizoanddelta.com'] = 'brizodelta';
$sites['www.brizoanddelta.com'] = 'brizodelta';
$sites['brizoanddelta.com'] = 'brizodelta';

if (file_exists($app_root . '/sites/local.sites.php')) {
  include $app_root . '/sites/local.sites.php';
}

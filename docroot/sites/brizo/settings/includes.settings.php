<?php

/**
 * Add settings using full file location and name.
 *
 * It is recommended that you use the DRUPAL_ROOT and $site_dir components to
 * provide full pathing to the file in a dynamic manner.
 */
$additionalSettingsFiles = [
  //   DRUPAL_ROOT . "/sites/$site_dir/settings/foo.settings.php"
];

$host = $_SERVER['HTTP_HOST'];
$environment = '';

// If there is a simpler way of doing this, feel free to replace it.
// Basically I just want to identify which environment we're in so we can load
// conditional Drupal settings, services, etc
if (preg_match('/^(dev\d?)\..*$/', $host)) {
  $environment = 'dev';
} elseif (preg_match('/^(qat\d?)\..*$/', $host)) {
  $environment = 'qat';
} elseif (preg_match('/^(stg\d?)\..*$/', $host)) {
  $environment = 'stg';
}

$config['config_split.config_split.delta_local']['status'] = FALSE;
$config['config_split.config_split.delta_dev']['status'] = FALSE;
$config['config_split.config_split.delta_qat']['status'] = FALSE;
$config['config_split.config_split.delta_stg']['status'] = FALSE;
$config['config_split.config_split.delta_prod']['status'] = FALSE;

switch ($environment) {
case 'dev':
  $config['config_split.config_split.delta_dev']['status'] = TRUE;
  $settings['container_yamls'][] = DRUPAL_ROOT . '/sites/development.services.yml';
  break;
case 'qat':
  $config['config_split.config_split.delta_qat']['status'] = TRUE;
  break;
case 'stg':
  $config['config_split.config_split.delta_stg']['status'] = TRUE;
  break;
default:
  $config['config_split.config_split.delta_prod']['status'] = TRUE;
  break;
}

foreach ($additionalSettingsFiles as $settingsFile) {
  if (file_exists($settingsFile)) {
    require $settingsFile;
  }
}

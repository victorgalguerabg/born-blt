<?php

namespace Drupal\sync_category\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\sync_category\CategoryImportService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class CategoryImportForm.
 */
class CategorySyncForm extends FormBase {
  /**
   * Drupal\sync_category\Controller\CategoryImportController definition.
   *
   * @var \Drupal\sync_category\CategoryImportInterface
   */
  protected $service;

  /**
   * Class constructor.
   */
  public function __construct(CategoryImportService $service) {
    $this->service = $service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
      // Load the service required to construct this class.
        $container->get('sync_category.import')
      );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'category_import_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $nid = '') {
    $config = $this->config('general.deltaservicesconfig');
    $delta_config = $this->configFactory->get('delta_services.deltaservicesconfig');
    $api = $delta_config->get('api_base_path');
    $form['#id'] = 'category-import-form';
    $form['choose'] = [
      '#type' => 'select',
      '#title' => $this->t('Import Category'),
      '#required' => TRUE,
      '#options' => [
        'insert' => 'Import Category',
        'delete' => 'Remove All Categories',
        'facetInsert' => 'Import Facets',
      ],
      '#prefix' => t('<strong>Delta API Category Sync</strong><br> This form is used to import categories from the Delta API. <br> Current API Endpoint: ') . $api . t(' <br> Select "Import Category" to import categories into the <a href="/admin/structure/taxonomy/manage/product_category/overview">Product Category vocabulary</a>. This will add new categories if they don\'t exist. This will not remove any categories.<br> Select "Remove all Categories" to all od the categories in the <a href="/admin/structure/taxonomy/manage/product_category/overview">Product Category vocabulary</a> vocabulary. '),
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#id' => 'import_category',
      '#value' => $this->t('Execute'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $input_sel = $form_state->getUserInput();
    $input_selection = $input_sel['choose'];
    if ($input_selection == 'insert') {
      $this->service->createCommerceCategory();
    }
    elseif ($input_selection == 'delete') {
      $this->service->deleteCommerceCategory();
    }
    elseif ($input_selection == 'facetInsert') {
      $this->service->facetInsert();
    }
  }

}

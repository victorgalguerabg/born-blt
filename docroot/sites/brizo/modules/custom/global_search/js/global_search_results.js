(function ($, Drupal, drupalSettings, Backbone) {
    Drupal.behaviors.plpBehaviour = {
        attach: function (context, settings) {
            let gtm_search = [];
            let gtm_search_click = [];
            let loadProducts = true;
            let pageNumber = 0;
            let lastPage = false;
            let totalResult = 0;
            let loader = $('.ajax-progress-throbber');


            var _totalCount = 0;
            var docView = Backbone.View.extend({
                el: "#documents-tab",
                template: _.template($("#document-output").html()),
                initialize: function (initData) {
                    this.render(initData);
                    this.afterRender(initData);
                },
                render: function (initData) {

                    var typedKeyword = getSearchParams('title');
                    //var noResultsMessage = "<div class='noResults no-rows-doc'>We're sorry, we didn't find any results for " + decodeURIComponent(typedKeyword) + ". We recommend using different words in your search.</div>";

                    var _self = this;
                    var _render = "";
                    if(initData) {

                      var content = initData.content;

                      if (content.length > 0) {
                        _.each(initData.content, function (entry) {
                          var _filePath = entry.filePath;
                          var _fileName = entry.fileName;
                          var _docType = entry.documentType;
                          _render += _self.template({
                            fileName: _fileName,
                            fileLinks: entry.modelNumber.slice(0, 5),
                            filePath: _filePath,
                            docType: _docType,
                          });
                          _self.$el.html(_render);
                          $('.preloader').remove();
                          $('body').removeClass('loader-active');
                          $('.tab-content-title-doc').show();
                        });
                        $('.document-all-search-result').html(_render);
                      }
                    }

                  var _documentCount = 0;
                  if(initData){

                    _documentCount = initData.totalElements ? initData.totalElements : 0;
                  }
                  if(_documentCount === 0){
                    $(".docTab").remove();
                    tabHide();
                    loader.hide();
                  }
                  _totalCount = parseInt(_totalCount) + parseInt(_documentCount);
                  totalResult = totalResult + _documentCount;
                  $('.document-count').html( _documentCount);
                  $('.document-count').attr('count', _documentCount);
                  recalculate();
                  return this;
                },
                afterRender: function (initData) {
                    if (initData != null) {
                        var _documentCount = initData.totalElements;
                        if (_documentCount > 0) {
                            var _filter = getSearchParams('documentLookFor');
                            //lets select document tab as default when user go with advanced search.
                            if (_filter !== "" && _filter !== undefined && (_filter === 'ts' || _filter === 'mi' || _filter === 'pd')) {
                                $("#documentTabLink").trigger('click');
                            }
                        }

                    }

                }
            });

            // Process content data and assemble the page.
            var contView = Backbone.View.extend({
                el: "#contents-tab",
                el1: ".contentoutputfull",
                template: _.template($("#content-output").html()),
                template1: _.template($("#full-content-output").html()),
                initialize: function (content, _sno) {
                    if (content.currentpage === 1) {
                        this.renderAllsectionContent(content, _sno);
                    }
                    this.renderContentSection(content, _sno);
                },
                renderAllsectionContent: function (content, _sno) {
                    var _self = this;
                    var _render = "";
                    if (content.totalmatches > 0) {
                        var matches = content.matches;
                        var counter = _sno * 20;
                        var _incrCnter = counter + 1;
                        _.each(matches, function (entry) {
                            var _title = entry.title;
                            var _body = entry.body;
                            var _type = entry.type;
                            var _url = entry.url;
                            if(_type == 'Blog Article'){
                              var _image = entry.field_wob_teaser_image_wide;
                            }else{
                              var _image = '';
                            }
                            _render += _self.template({
                                searchContentBody: _body,
                                searchContentUrl: _url,
                                searchContentTitle: _title,
                                searchContentType: _type,
                                searchContentImage: _image,
                                searchSno: _incrCnter,
                            });
                            _self.$el.html(_render);
                            _incrCnter++;
                        });
                    }
                    $('.tab-content-title-cont').show();
                    recalculate();
                    return this;
                },
                renderContentSection: function (content, _sno) {
                    var _sno = parseInt(_sno);
                    var _self = this;
                    var _render = "";
                    if (content.totalmatches > 0) {
                        var matches = content.matches;
                        var counter = _sno * 20;
                        var _incrCnter = counter + 1;
                        _.each(matches, function (entry) {
                            var _title = entry.title;
                            var _body = entry.search_api_excerpt;
                            var _type = entry.type;
                            var _url = entry.url;
                            if(_type == 'Blog Article'){
                              var _image = entry.field_wob_teaser_image_wide;
                            }else{
                              var _image = '';
                            }
                            _render += _self.template1({
                                searchContentBody: _body,
                                searchContentUrl: _url,
                                searchContentTitle: _title,
                                searchContentType: _type,
                                searchContentImage: _image,
                                searchSno: _incrCnter,
                            });
                            $('.preloader').remove();
                            $('body').removeClass('loader-active');
                            _incrCnter++;
                        });
                        $('#fullcontentoutput').html(_render);
                        $('.content-all-search-result').html(_render);
                    }
                    return this;
                }
            });

          var innovationView = Backbone.View.extend({
            el: "#innovation-tab",
            template: _.template($("#innovation-output").html()),
            initialize: function (content, _sno) {
              this.renderContentSection(content, _sno);
            },
            renderContentSection: function (content, _sno) {
              var _sno = parseInt(_sno);
              var _self = this;
              var _render = "";
              if (content.totalmatches > 0) {
                var matches = content.matches;
                var counter = _sno * 20;
                var _incrCnter = counter + 1;
                _.each(matches, function (entry) {
                  var _title = entry.title;
                  var _body = entry.search_api_excerpt;
                  if(_body.length == 0){
                     _body = entry.body;
                  }
                  var _url = entry.url;
                  _render += _self.template({
                    searchInnBody: _body,
                    searchInnUrl: _url,
                    searchInnTitle: _title,

                  });
                  _self.$el.html(_render);
                  $('.preloader').remove();
                  $('body').removeClass('loader-active');
                  _incrCnter++;
                });
              }
              recalculate();
              return this;
            }
          });


            function facetView(data, className){
              let facets = data[0].terms;
              let elem = '.'  + className;
              let finishData = drupalSettings.finishData;
              let options = new Array();
              $.each(facets, function(i, d) {
                if($.inArray(d.term, options) == -1){
                  let finishLabel = d.term;
                  if(finishData[d.term]['label'].length > 0){
                    finishLabel = finishData[d.term]['label'];
                  }
                  options.push(d.term);
                  $(elem).append('<option value="' +
                    d.term +
                    '">' + finishLabel +
                    '</option>');
                }
              });

              var seloptions = $(elem + ' option').not(':first');
              var arr = seloptions.map(function(_, o) { return { t: $(o).text(), v: o.value }; }).get();
              arr.sort(function(o1, o2) { return o1.t > o2.t ? 1 : o1.t < o2.t ? -1 : 0; });
              seloptions.each(function(i, o) {
                o.value = arr[i].v;
                $(o).text(arr[i].t);
              });
              $(elem).show();

            }

            var ProdView = Backbone.View.extend({

                el: "#products-tab",
                template: _.template($("#product").html()),

                initialize: function (initData, eventName, append) {
                  this.render(initData, eventName,append);
                  this.renderFullProducts(initData);
                },

                render: function (initData, eventName, append) {
                    if (!$('.preloader').length) {
                        $('#plp-wrapper').prepend('<div class="preloader"><i class="input-loader"></i></div>');
                    }

                    var _self = this;
                    var _render = "";
                    var _i = 0;
                    if (initData.content != '') {
                        _.each(initData.content, function (entry) {

                            var search_position = 0;
                            var _price = 0;
                            let _ribbonColor = '';
                            let search_product = new Object();
                            search_position = search_position + 1;
                            search_product.list = "Search Results";
                            if(entry.collections[0] != undefined) {
                                search_product.brand = entry.collections[0];
                            }
                            else {
                                search_product.brand = "";
                            }
                            search_product.name = entry.description;
                            search_product.position = search_position;
                            search_product.id = entry.name;
                            search_product.variant = entry.values.Finish;

                            var _filter = getSearchParams('documentLookFor');
                            if (_filter !== '' || _filter !== undefined) {
                                var _price = entry.values.ListPrice;
                            }
                            var _heroImage = entry.heroImageSmall;
                            let _ribbonText = '';
                            if (entry.values.MaterialStatus !== undefined && entry.values.MaterialStatus >= 50) {
                              _ribbonText = 'discontinued';
                              _ribbonColor = "gray-30";
                            }

                            var _title = "Title";
                            if (entry.description !== '' && entry.description !== undefined) {
                                _title = entry.description;
                            }

                          var _handle = new Array();
                          var _handleArr = new Array();
                          let modelNo = entry.name;
                          let skuSplit = '';
                          if(entry.configOptions.length > 0){
                            let skuSplit = modelNo.split('--');
                            _.each(entry.configOptions, function (configOptions) {
                              if(configOptions.optionLabel == "Handle_or_Accent"
                                || configOptions.optionLabel == 'Rough_Valve'){
                                _.each(configOptions.optionProducts, function (optionProduct) {
                                  if($.inArray(optionProduct.modelNumber, skuSplit) !== -1 &&
                                    $.inArray(optionProduct.modelNumber, _handleArr) == -1) {
                                    _handleArr.push(optionProduct.modelNumber);
                                    let _prdName = upperCamelCase(optionProduct.description);
                                    _handle.push({sku: optionProduct.modelNumber, name: _prdName});
                                  }
                                });
                              }
                            });
                          }


                            var _collection = "";
                            //if(_filter === ''){
                            if (entry.values.Collection !== "" && entry.values.Collection !== undefined) {
                                _collection = entry.values.Collection;
                            }
                            //}


                          let _availableToOrderDate = new Date(entry.values.AvailableToOrderDate).getTime();
                          let _availableToShipDate = new Date(entry.values.AvailableToShipDate).getTime();
                          let _currentDate = new Date().getTime();
                          if (_currentDate < _availableToShipDate) {
                            if (_currentDate > _availableToOrderDate) {
                              _ribbonText = 'Coming Soon';
                              _ribbonColor = "brizo-teal";
                            }
                          }
                          if(entry.values.FltWebExclusiveCustomerItem === 'T' && entry.values.WebExclusiveCustomerItem === 'Ferguson') {
                            _ribbonText = 'Ferguson';
                            _ribbonColor = "brizo-teal";
                          }
                          let _viewAllCollectionLink = entry.values.viewAllCollectionLink;

                            var _sku = entry.name;
                            var _url = '';

                            _url = '/product-detail/' + _sku;


                           _price = '$ ' + _price;
                            gtm_search.push(search_product);
                            gtm_search_click[entry.name] = search_product;

                            _render += _self.template({
                                title: _title,
                                collectionName: _collection,
                                handle : _handle,
                                heroImage: _heroImage,
                                sku: _sku,
                                price: _price,
                                url: _url,
                                ribbonText: _ribbonText,
                                ribbonColor: _ribbonColor,
                                viewAllCollectionLink:  _viewAllCollectionLink
                            });

                            $('.tab-content-title-prod').show();
                            $('.preloader').remove();
                            $('body').removeClass('loader-active');
                            _i++;
                        });

                        if(append ==1){
                            _self.$el.append(_render);
                        }else{
                            _self.$el.html(_render);
                            $('.product-all-search-result').html(_render);
                        }
                    } else {
                        $('.prodTab').remove();
                        tabHide();
                        loader.hide();
                    }
                    if(append == null){

                      var _productCount = initData.totalElements;
                      totalResult = totalResult + _productCount;
                      var allTabCount = totalResult;
                      $('.product-count').html( _productCount);
                      $('.product-count-tab,#product-mbl-cnt').attr('count', _productCount);
                      recalculate();
                    }

                    return this;
                },
                renderFullProducts: function (initData) {
                    var typedKeyword = getSearchParams('title');
                    // var noResultsMessage = "<div class='noResults no-rows-pro'>We're sorry, we didn't find any results for " + decodeURIComponent(typedKeyword) + ". We recommend using different words in your search.</div>";
                    if (initData.content.length === 0) {
                        $("#output-1").html(noResultsMessage);
                        $("#productTabLink").hide();
                    }
                    var _self = this;
                    var _render = "";
                    if (initData.content != '') {
                        _.each(initData.content, function (entry) {
                            var _price = entry.values.ListPrice;
                            var _heroImage = entry.heroImageSmall;
                            var _SyndigoActive = '';
                            if (entry.values.MaterialStatus !== undefined && entry.values.MaterialStatus >= 50) {
                                var _SyndigoActive = 'discontinued';
                            }
                            var orderDate = (entry.values.AvailableToOrderDate !== '' || entry.values.AvailableToOrderDate !== undefined) ? DateTotimeStamp(entry.values.AvailableToOrderDate) : '';
                            var shipDate = (entry.values.AvailableToShipDate !== '' || entry.values.AvailableToShipDate !== undefined) ? DateTotimeStamp(entry.values.AvailableToShipDate) : '';
                            var currentDate = DateTotimeStamp(getCurrentDate());
                            if (orderDate === '' || shipDate === '') {
                                shipDate = currentDate - (60 * 60 * 24);
                            }
                            if (currentDate < shipDate) {
                                if (currentDate > orderDate) {
                                    var _SyndigoActive = 'coming soon';
                                }
                            }
                            var _title = "Title";
                            if (entry.description != '') {
                                _title = entry.description;
                            }
                            var _collection = "";
                            if (entry.values.Collection !== "" && entry.values.Collection !== undefined) {
                                _collection = entry.values.Collection;
                            }
                            var _sku = entry.name;
                            var urlAliasObject = drupalSettings.urlAliasObject;
                            var _url = '';
                            if (typeof urlAliasObject != 'undefined') {
                                _url = urlAliasObject[_sku];
                            }
                            if (_url === '' || _url === undefined) {
                                _url = '/pdp/' + _sku;
                            }
                        });
                    } else {
                        var typedKeyword = getSearchParams('title');
                        $("#product-tab div.row h3, .paging-compare-bar").hide();
                        $('.preloader').remove();
                        $("#productTabLink").hide();
                        $('body').removeClass('loader-active');
                    }
                    var _productCount = initData.totalElements;
                    if(_productCount === 0){
                      $(".all-product").hide();
                      $("#product-tab-link").hide();
                      $('.product_container, #product-list, #product-mbl-tab').remove();
                      if($("#content-count-tab").attr("count") > 0){
                        $('.content_container, #content-tab-link').addClass('is-active');
                      }else{
                        $('.content_container, #content-list, #content-mbl-tab').remove();
                        $('.document_container, #document-tab-link').addClass('is-active');
                      }
                    }
                    $('#productTabLink span').html('Products (' + _productCount + ')');
                    return this;
                }

            });



            var docpaginationView = Backbone.View.extend({
                el: "#document-pagination-output",
                template: _.template($("#document-pagination").html()),
                initialize: function (initData) {
                    this.render(initData);
                },
                events: {
                    'click [type="button"]': 'docpagination'
                },
                docpagination: function (e) {
                    var params = [];
                    params['pageNum'] = $(e.currentTarget).val();
                    params['first'] = $(e.currentTarget).attr('first');
                    params['last'] = $(e.currentTarget).attr('last');
                    if($(e.currentTarget).val() === '>') {
                      params['pageNum'] = $('.pager__link.is-active').val();
                      params['action'] = 'next';
                    }
                    if($(e.currentTarget).val() === '>>') {
                      var total = $(e.currentTarget).attr('total');
                      params['pageNum'] = parseInt(total);
                      params['first'] = parseInt(total)-4;
                      params['last'] = parseInt(total);
                    }
                    if($(e.currentTarget).val() === '<') {
                      params['pageNum'] = $('.pager__link.is-active').val();
                      params['action'] = 'prev';
                    }
                    if($(e.currentTarget).val() === '<<') {
                      params['pageNum'] = 1;
                      params['first'] = 1;
                      params['last'] = 5;
                    }
                    applydocFacets(params);
                },
                render: function (initData) {
                  if (initData === null) {
                    // $('.documentoutput').html('<h4>No results at this time.</h4>');
                    return false;
                  }
                  var cont_disp_cnt = drupalSettings.documentSetting.size;
                  if (cont_disp_cnt == null) {
                    cont_disp_cnt = 30;
                  }
                  var _self = this;
                  var _render = "";
                  var _nextPage = (initData === null) ? 0 : parseInt(initData.number) + 1;
                  var _currentpage = initData.currentpage;  // Current page
                  var _totalmatches = initData.totalElements;
                  var _totalpages = Math.ceil(_totalmatches / cont_disp_cnt);
                  var _nextPage;
                  var _first;
                  if (_currentpage < _totalpages) {
                    _nextPage = _currentpage + 1;
                  } else {
                    _nextPage = _currentpage;
                  }
                  var _previousPage;
                  if (_currentpage === 1) {
                    _previousPage = 1;
                    _first = 1;
                  } else {
                    _previousPage = _currentpage - 1;
                    _first = initData.first;
                  }
                  if(_currentpage>=initData.first && _currentpage<=initData.last) {
                    _first = parseInt(initData.first);
                  }
                  if(initData.action && initData.action == 'next') {
                    if(_currentpage>=initData.first && _currentpage<initData.last) {
                      _first = parseInt(initData.first);
                    } else {
                      _first = parseInt(initData.first) + 1;
                    }
                    _currentpage = parseInt(_currentpage) + 1;
                  }
                  if(initData.action && initData.action == 'prev') {
                    if(_currentpage>initData.first && _currentpage<=initData.last) {
                      _first = parseInt(initData.first);
                    } else {
                      _first = parseInt(initData.first) - 1;
                    }
                    _currentpage = parseInt(_currentpage) - 1;
                  }
                  var pagination = parseInt(_first)+4;
                  if(_totalpages < pagination) {
                    pagination = _totalpages;
                  }
                  if(_currentpage != 1) {
                    _render += '<input total="'+_totalpages+'" class="pager__link first" type="button" data="" value="<<">';
                    _render += '<input first="'+_first+'" last="'+pagination+'" class="pager__link prev" type="button" data="" value="<">';
                  }
                  for(var i=_first;i<=pagination;i++) {
                    var classValue = '';
                    if(i === parseInt(_currentpage)) {
                      classValue = 'is-active';
                    }

                    _render += '<input first="'+_first+'" last="'+pagination+'" class="pager__link '+ classValue +'" type="button" value="'+ i +'">';

                  }
                  if(_currentpage != _totalpages) {
                    _render += '<input first="'+_first+'" last="'+pagination+'" class="pager__link next" type="button" data="" value=">">';
                    _render += '<input total="'+_totalpages+'" class="pager__link last" type="button" data="" value=">>">';
                  }
                var start_value = (_currentpage-1)*cont_disp_cnt;
                if(_currentpage ==1) {
                    start_value = 1;
                }
                var endvalue = _currentpage*cont_disp_cnt;
                if(start_value == 1 && endvalue > initData.totalElements){
                    // no pager
                }
                else if(endvalue > initData.totalElements){
                endvalue = initData.totalElements;
                $('.srp-result-doc').html('<span class="srp-qty">Results ' + start_value + '-' + endvalue  + ' of ' + initData.totalElements +'</span>').attr('data-content-cnt', initData.totalElements);
                }
                else{
                $('.srp-result-doc').html('<span class="srp-qty">Results ' + start_value + '-' + endvalue  + ' of ' + initData.totalElements +'</span>').attr('data-content-cnt', initData.totalElements);
                }
                  _self.$el.html(_render);
                }
            });


            // Pagination fot content section.
            var contpaginationView = Backbone.View.extend({
                el: ".content-pagination-output",
                template: _.template($("#content-pagination").html()),
                initialize: function (initData) {
                    this.render(initData);
                },
                events: {
                    'click [type="button"]': 'contentpagination'
                },
                contentpagination: function (e) {
                  var params = [];
                    params['pageNum'] = parseInt($(e.currentTarget).val()); // Current
                    params['first'] = $(e.currentTarget).attr('first');
                    params['last'] = $(e.currentTarget).attr('last');
                    if($(e.currentTarget).val() === '>') {
                        params['pageNum'] = $('.pager__link.is-active').val();
                        params['action'] = 'next';
                    }
                    if($(e.currentTarget).val() === '>>') {
                        var total = $(e.currentTarget).attr('total');
                        params['pageNum'] = parseInt(total);
                        params['first'] = parseInt(total)-4;
                        params['last'] = parseInt(total);
                    }
                    if($(e.currentTarget).val() === '<') {
                        params['pageNum'] = $('.pager__link.is-active').val();
                        params['action'] = 'prev';
                    }
                    if($(e.currentTarget).val() === '<<') {
                        params['pageNum'] = 1;
                        params['first'] = 1;
                        params['last'] = 5;
                    }
                  applycontFacets(params);


                },
                render: function (initData) {

                    var cont_disp_cnt = 20;//drupalSettings.cont_disp_cnt;
                    if (cont_disp_cnt == null) {
                        cont_disp_cnt = 10;
                    }
                    var _self = this;
                    var _render = "";
                    var _currentpage = initData.currentpage;  // Current page
                    var _totalmatches = initData.totalmatches;
                    var _totalpages = Math.ceil(_totalmatches / cont_disp_cnt);
                    var _nextPage;
                    var _first;
                    if (_currentpage < _totalpages) {
                        _nextPage = _currentpage + 1;
                    } else {
                        _nextPage = _currentpage;
                    }
                    var _previousPage;
                  if (_currentpage === 1) {
                    _previousPage = 1;
                    _first = 1;
                  } else {
                    _previousPage = _currentpage - 1;
                    _first = initData.first;
                  }
                  if(_currentpage>=initData.first && _currentpage<=initData.last) {
                    _first = parseInt(initData.first);
                  }
                  if(initData.action && initData.action == 'next') {
                    if(_currentpage>=initData.first && _currentpage<initData.last) {
                      _first = parseInt(initData.first);
                    } else {
                      _first = parseInt(initData.first) + 1;
                    }
                    _currentpage = parseInt(_currentpage) + 1;
                  }
                  if(initData.action && initData.action == 'prev') {
                    if(_currentpage>initData.first && _currentpage<=initData.last) {
                      _first = parseInt(initData.first);
                    } else {
                      _first = parseInt(initData.first) - 1;
                    }
                    _currentpage = parseInt(_currentpage) - 1;
                  }
                  var pagination = parseInt(_first)+4;
                  if(_totalpages < pagination) {
                    pagination = _totalpages;
                  }
                  if(_currentpage != 1) {
                    _render += '<input total="'+_totalpages+'" class="pager__link first" type="button" data="" value="<<">';
                    _render += '<input first="'+_first+'" last="'+pagination+'" class="pager__link prev" type="button" data="" value="<">';
                  }
                  for(var i=_first;i<=pagination;i++) {
                    /*_render += _self.template({
                      pageNumber: i,
                      currentPage: _currentpage,
                      totalRecordset: initData.totalElements
                    });*/
                    var classValue = '';
                    if(i === parseInt(_currentpage)) {
                      classValue = 'is-active';
                    }

                    _render += '<input first="'+_first+'" last="'+pagination+'" class="pager__link '+ classValue +'" type="button" value="'+ i +'">';

                  }
                  if(_currentpage != _totalpages) {
                    _render += '<input first="'+_first+'" last="'+pagination+'" class="pager__link next" type="button" data="" value=">">';
                    _render += '<input total="'+_totalpages+'" class="pager__link last" type="button" data="" value=">>">';
                  }
                    if (_totalpages > 1 ) {
                      _self.$el.html(_render);
                    }
                }
            });
            if (context === document) {
                var pathArray = window.location.pathname.split('/');
                if (pathArray[1] === 'replacement-parts-search-results') {
                    var _param = getSearchParams('searchTerm');
                    var _type = getSearchParams('type');
                    var _filter = getSearchParams('documentLookFor');
                    $.ajax({
                        type: "GET",
                        url: "/search-doc-output",
                        dataType: 'json',
                        data: {
                            title: _param,
                            categories: _type,
                            // documentLookFor: _filter,
                        },
                        beforeSend: function () {
                          loader.show();
                        },
                        complete: function () {
                            recalculate();
                        },
                        success: function (data) {
                            var DocView = new docView(data);
                            var DocpaginationView = new docpaginationView(data);
                            loader.hide();
                        }
                    });

                } else {
                    var _param = getSearchParams('searchTerm');
                    var searchurl = window.location.href;
                    var check = searchurl.indexOf('?searchTerm');
                    if(_param == '' || check == -1){
                      $('.search-empty').show();
                      $('.search-result-text-wrapper').hide();
                      $('.product-filters').hide();
                      loadProducts = 0;
                      return false;
                    }
                    var _type = getSearchParams('type');
                    var _mediaroomFilter = getSearchParams('filter');
                    var _filter = getSearchParams('documentLookFor');
                    $.ajax({
                        type: "GET",
                        url: "/search-output",
                        dataType: 'json',
                        data: {
                            searchTerm: _param,
                            categories: _type,
                            // documentLookFor: _filter,
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            showErrorMessage(XMLHttpRequest.status, '#all-tab #output');
                            showErrorMessage(XMLHttpRequest.status, '#all-tab #output');

                        },
                        beforeSend: function () {
                          loader.show();
                        },
                        complete: function () {
                            // Manage tag records
                            recalculate();

                        },
                        success: function (data) {
                            var prodView = new ProdView(data);
                            facetView(data.facets, 'facetFinish');
                            if(data){
                              data.currentpage = 1;
                            }
                            if(data.last == false){
                              loadProducts = 1;
                            }else{
                              loadProducts = 0;
                            }
                          loader.hide();
                        }
                    });
                    $.ajax({
                        type: "GET",
                        url: "/search-doc-output",
                        dataType: 'json',
                        data: {
                            searchTerm: _param,
                            categories: _type,
                            documentLookFor: _filter,
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            showErrorMessage(XMLHttpRequest.status, '#all-tab #output');
                        },
                        beforeSend: function () {
                          loader.show()
                        },
                        complete: function () {
                            $('.product-call').remove();
                            $('#all-tab #output').removeClass('loader-active');
                            recalculate();
                        },
                        success: function (data) {
                            var DocView = new docView(data);

                            if(data){
                              data.currentpage = 1;
                              var DocpaginationView = new docpaginationView(data);
                              //var DocpaginationView = new docpaginationView(data);
                              loader.show();
                            }else{
                              $('.docTab').hide();
                              loader.hide();
                            }

                        }
                    });
                    // Matching records for content being fetched and processed based on
                    // matching record count.
                    var page = [];
                    page['pageNum'] = 1;
                    var content_matchings = getContentSearchResults(_param, _mediaroomFilter, page);
                    if (content_matchings.totalmatches !== undefined) {
                        _totalCount = content_matchings.totalmatches;
                    }
                    totalResult = totalResult + content_matchings.totalmatches;
                    //$('#contentTabLink span').html('Content (' + _totalCount + ')');
                    $('.content-count').html( _totalCount);
                    $('#content-count-tab,#content-mbl-cnt').attr('count', _totalCount);
                    $('#srp-result-content').html('<span class="srp-qty">' + _totalCount + '</span> Contents').attr('data-content-cnt', _totalCount);
                    if (_totalCount > 0) {
                        var ContView = new contView(content_matchings, 0);
                        //var ContpaginationView = new contpaginationView(content_matchings);
                    } else {
                        $('#content-tab .row').html("<b>Nothing found. Please refine your search.</b>");
                        var typedKeyword = getSearchParams('title');
                        var noResultsMessage = "<div class='noResults'>We're sorry, we didn't find any results for " + decodeURIComponent(typedKeyword) + ". We recommend using different words in your search.</div>";
                        // $('#content-tab .row').html(noResultsMessage);
                        $("#contentTabLink").hide();
                        $("#search-content-title").hide();
                        $(".content-see-all").hide();
                    }

                  var _mediaroomFilter = 'innovations';
                  var innovation_matchings = getContentSearchResults(_param, _mediaroomFilter, page);

                  if (innovation_matchings.totalmatches !== undefined) {
                    _totalCount = innovation_matchings.totalmatches;
                  }
                  totalResult = totalResult + innovation_matchings.totalmatches;
                  $('.innovation-count').html( _totalCount );
                  $('#innovation-count-tab').attr('count', _totalCount);
                  if (_totalCount > 0) {
                    var innView = new innovationView(innovation_matchings, 0);
                    //var ContpaginationView = new contpaginationView(content_matchings);
                  } else {

                  }
                }
                //lets select document tab as default when user go with advanced search.
                if (_filter !== "" && _filter !== undefined && (_filter === 'ts' || _filter === 'mi' || _filter === 'pd')) {
                    $("#documentTabLink").trigger('click');
                }

            }


            var showErrorMessage = function (status, wrapper) {
                switch (status) {
                    case 400:
                        $(wrapper).html("<div class='sorry-msg'>Server understood the request, but request content was invalid., Please try with valid information.</div>");
                        break;
                    case 401:
                        $(wrapper).html("<div class='sorry-msg'>Unauthorized access.</div>");
                        break;
                    case 403:
                        $(wrapper).html("<div class='sorry-msg'>We are unable to process your request at this time, Please try again later.</div>");
                        break;
                    case 500:
                        $(wrapper).html("<div class='sorry-msg'>We are into the system upgrade process, Please try again later.</div>");
                        break;
                    case 401:
                        $(wrapper).html("<div class='sorry-msg'>Service unavailable. Please try again later.</div>");
                        break;
                    default:
                        $(wrapper).html("<div class='sorry-msg'>Oops something went wrong, Please try again later.</div>");
                }
            }

            /**
             * Recalculate tab total counts of records
             */
            function recalculate() {
                var grandTotal = 0;
                var data_document_cnt = 0;
                var data_product_cnt = 0;
                var data_content_cnt = 0;
                var  data_innovation_cnt = 0;

                if($(".product-count-tab").attr("count")) {
                  data_product_cnt = $(".product-count-tab").attr("count");
                  data_product_cnt = parseInt(data_product_cnt);
                }
                if($("#content-count-tab").attr("count")) {
                  data_content_cnt = $("#content-count-tab").attr("count");
                  data_content_cnt = parseInt(data_content_cnt);
                }
                if($("#innovation-count-tab").attr("count")) {
                  data_innovation_cnt = $("#innovation-count-tab").attr("count");
                  data_innovation_cnt = parseInt(data_innovation_cnt);
                }
                if($("#document-count").attr("count")){
                  data_document_cnt = $("#document-count").attr("count");
                  data_document_cnt = parseInt(data_document_cnt);
                }



                grandTotal = data_document_cnt + data_product_cnt
                  + data_content_cnt + data_innovation_cnt;
                if(!$.isNumeric(grandTotal)) {
                  grandTotal = 0;
                }
                $('#grand_total').html(grandTotal);
                $('#search-all-count-tab,#all-mbl-cnt').html('('+ grandTotal + ')');
                if (grandTotal === 0) {
                  $('.tabs').hide();
                  $('.search-result-text-wrapper').html('<div class ="error-message" role="contentinfo" aria-label="Error message">Please refine your search for results</div>');
                }else{
                  $('.tabs').show();
                  //$('.search-result-text-wrapper').html('');
                }
                loader.hide();
            }

            /**
             *  Convert date to timestamp - 2015-07-23
             * @param dates
             * @returns {number}
             * @constructor
             */
            function DateTotimeStamp(dates) {
                if (dates !== undefined) {
                    var dates1 = dates.split("-");
                    var newDate = dates1[0] + "/" + dates1[1] + "/" + dates1[2];
                    return new Date(newDate).getTime();
                }
            }

            /**
             * Get current time.
             * @returns {string}
             */

            function getCurrentDate() {
                var d = new Date($.now());
                return d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate();
            }

            /* Define functin to find and replace specified term with replacement string */
            function replaceAll(str, term, replacement) {
                return str.replace(new RegExp(escapeRegExp(term), 'g'), replacement);
            }


            /*
       Get search value from querystring.
       */
            function getSearchParams(k) {
                var p = {};
                location.search.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (s, k, v) {
                    p[k] = v
                })
                var response = k ? p[k] : p;
                var decodedResponse = decodeURIComponent(response);
                var sanitizedResponse = removeHTMLTags(decodedResponse);
                return sanitizedResponse;
            }

            /*
                 Function to remove HTML tags.
            */

            function removeHTMLTags(decodedResponse) {
                paramText = decodedResponse;
                paramText = paramText.replace(/<script.*>[\w\W]{1,}(.*?)[\w\W]{1,}<\/script>/gi, "");
                paramText = paramText.replace(/%3Cscript.*%3E[\w\W]{1,}(.*?)[\w\W]{1,}%3C\/script%3E/gi, "");
                paramText = paramText.replace(/<style.*>[\w\W]{1,}(.*?)[\w\W]{1,}<\/style>/gi, "");
                paramText = paramText.replace(/&nbsp;/gi, " ");
                paramText = paramText.replace(/&amp;/gi, "&");
                paramText = paramText.replace(/&quot;/gi, '"');
                paramText = paramText.replace(/&lt;/gi, '<');
                paramText = paramText.replace(/&gt;/gi, '>');
                paramText = paramText.replace(/svg\/onload%3D/gi, '');
                paramText = paramText.replace(/--%21/gi, '');
                paramText = paramText.replace(/svg\/onload%3D/gi, '');
                paramText = paramText.replace(/--%21/gi, '');
                paramText = paramText.replace(/%28confirm%29/gi, '')
                paramText = paramText.replace(/%3C/gi, '');
                paramText = paramText.replace(/%3E/gi, '');
                paramText = paramText.replace(/<(?:.|\s)*?>/g, "");
                return paramText;

            }


            /*
            To return content search results based on current search value and requesting page based on previous or next button hits in pagination.
            @params: search value, requesting page
            @return: search result
             */
            function getContentSearchResults(params, mediaroomFilter, arguments) {
                var search_result = [];
                requestingpage = parseInt(arguments['pageNum']);
                //search_result.currentpage = requestingpage;
                if(arguments['pageNum']) {
                  search_result.currentpage = arguments['pageNum'];
                  search_result.first = arguments['first'];
                  search_result.last = arguments['last'];
                }
                if(arguments['action']) {
                  search_result.action = arguments['action'];
                }
                var matching_count = 0;
                if(_mediaroomFilter == 'innovations') {
                    contentUrl = "/rest/export/search_content_inno?_format=json";
                    contentCountUrl = "/rest/matching_records_count_inno?_format=json";
                } else if(_mediaroomFilter == 'blogs') {
                    contentUrl = "/rest/export/search_content_blog?_format=json";
                    contentCountUrl = "/rest/export/matching_records_count_blog?_format=json";
                } else {
                    contentUrl = "/rest/export/search_content?_format=json";
                    contentCountUrl = "/rest/export/matching_records_count?_format=json";
                }
                $.ajax({
                    type: "GET",
                    url: contentUrl,
                    dataType: 'json',
                    async: false,
                    data: {
                        searchTerm: _param,
                        categories: _type,
                        page: requestingpage - 1,  // Since views counts from 0
                        categories: _type,
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        //showErrorMessage(XMLHttpRequest.status, '#document-tab');
                    },
                    beforeSend: function () {
                        loader.show();
                    },
                    complete: function () {
                        $('.preloader').remove();
                        $('body').removeClass('loader-active');
                    },
                    success: function (data) {
                        if(data.length == 0 && _mediaroomFilter == 'innovations'){
                          $('.innTab').remove();
                          tabHide();
                          loader.hide();
                        }else if(data.length == 0 && _mediaroomFilter != 'innovations'){
                          $('.artTab').remove();
                          tabHide();
                          loader.hide();
                        }
                        var matches = [];
                        $.each(data, function (key, value) {
                            matches.push(value);
                        });
                        search_result.matches = matches;

                        $.ajax({
                            type: "GET",
                            url: contentCountUrl,
                            dataType: 'json',
                            async: false,
                            data: {
                                searchTerm: _param,
                                categories: _type,
                            },
                            success: function (data) {
                                matching_count = data.length;
                                loader.hide();
                            }
                        });

                        search_result.totalmatches = matching_count;
                        if(matching_count == 0){
                          $(".all-content").hide();
                          $("#content-tab-link").hide();
                          $('.content_container, #content-tab-link , #content-mbl-tab').remove();
                          if($('#product-tab-link').is(":hidden")){
                            $('.document_container, #document-tab-link').addClass('is-active');
                          }
                        }
                    }
                });
                var start_value = (requestingpage-1)*20;
                if(requestingpage ==1) {
                    start_value = 1;
                }
                var endvalue = requestingpage*20;
                if(start_value == 1 && endvalue > search_result.totalmatches){
                    // no pager
                }
                else if(endvalue > search_result.totalmatches){
                  endvalue = search_result.totalmatches;
                  $('.srp-result-content').html('<span class="srp-qty">Results ' + start_value + '-' + endvalue  + ' of ' + search_result.totalmatches +'</span>').attr('data-content-cnt', search_result.totalmatches);
                }
                else{
                  $('.srp-result-content').html('<span class="srp-qty">Results ' + start_value + '-' + endvalue  + ' of ' + search_result.totalmatches +'</span>').attr('data-content-cnt', search_result.totalmatches);
                }
                return search_result;
            }

            function applyPager(params, eventName) {
                $('body').addClass('loader-active');
                let limit = drupalSettings.productSetting.size;
                let currentPage = params['pageNum'] == 0 ? 1 : params['pageNum'];

                loader.show();
                let pageNum = parseInt(params['pageNum']) - 1;
                $.ajax({
                    type: "GET",
                    url: "/search-output",
                    dataType: 'json',
                    data: {
                        searchTerm: _param,
                        categories: _type,
                        params: params['facets'],
                        size: limit,
                        page: pageNum,
                        currentpage : currentPage
                    },
                    beforeSend: function () {
                      loader.show();
                    },

                    success: function (data) {
                      if(data.totalElements == 0){
                        $('.prodTab').hide();
                        loader.hide();
                      }
                      if(params['pageNum'] && data) {
                        data.currentpage = params['pageNum'];
                        data.first = params['first'];
                        data.last = params['last'];
                      }
                      if(params['action'] && data) {
                        data.action = params['action'];
                      }
                        var prodView = new ProdView(data, eventName);
                      loader.hide();
                      if(data.last == false){
                        loadProducts = 1;
                      }else{
                        loadProducts = 0;
                      }
                      var productPager = new ProductPager(data);
                      productPager.undelegateEvents();

                    }
                });
            }

            function applydocFacets(params) {
                $.ajax({
                    type: "GET",
                    url: "/search-doc-output",
                    dataType: 'json',
                    data: {
                        title: _param,
                        categories: _type,
                        page: params['pageNum'],
                    },
                    success: function (data) {
                        var DocView = new docView(data);
                        if(params['pageNum'] && data) {
                          data.currentpage = params['pageNum'];
                          data.first = params['first'];
                          data.last = params['last'];
                        }
                        if(params['action'] && data) {
                          data.action = params['action'];
                        }
                        var PaginationView = new docpaginationView(data);
                        PaginationView.undelegateEvents();

                        $('body').removeClass('loader-active');
                        $('body').removeClass('preloader');

                    }
                });
            }

            /*
            This function will be called when user hits on certain page(previous or next) on pagination.
            Fetches contents based on current search value and page number requested
            @params: pagenumber
             */
            function applycontFacets(params) {
                $('body').addClass('loader-active');
                if (!$('.preloader').length) {
                    $('.search-result-page').prepend('<div class="preloader"><i class="input-loader"></i></div>');
                }
                var content_matchings = getContentSearchResults(_param, _mediaroomFilter, params);
                var ContView = new contView(content_matchings, (params['pageNum'] - 1));
                var ContpaginationView = new contpaginationView(content_matchings);
                ContpaginationView.undelegateEvents();
                $('body').removeClass('loader-active');
                $('body').removeClass('preloader');
            }


            $('.form-item__select, #include-discontinued-products').once().change(function(){
              let facet = $('#facetFinish').val();
              let sort  = $('#sortProduct').val();
              let discontinued = $('#include-discontinued-products').is(':checked');
              pageNumber = 0;
              loader.show();
              $.ajax({
                type: "GET",
                url: "/search-output",
                dataType: 'json',
                data: {
                  searchTerm: _param,
                  categories: _type,
                  facet: facet,
                  sort : sort,
                  page: pageNumber,
                  discontinued: discontinued
                  //documentLookFor: _filter,
                },
                success: function (data) {
                  var prodView = new ProdView(data);
                  if(data.last == false){
                    loadProducts = 1;
                  }else{
                    loadProducts = 0;
                  }
                  //var PaginationView = new ProductPager(data);
                  loader.hide();
                }
              });

            })
            $(window).once().scroll(function () {
            if (($(window).scrollTop() + $(window).height()
              > $('.layout-container').height() - $('.footer').height())
              && loadProducts && (!lastPage) &&
              ($('.prodTab').hasClass('tab-title-active') && $('.prodTab.tab-accordion-title').hasClass('tab-accordion-active'))) {
              loadProducts = false;
              totalResult = 0;
              let facet = $('#facetFinish').val();
              let sort  = $('#sortProduct').val();
              let discontinued = $('#include-discontinued-products').is(':checked');
              // On Scroll to achieve lazyload functionality.
              var _param = getSearchParams('searchTerm');
              var _type = getSearchParams('type');

              // var _filter = getSearchParams('documentLookFor');
              loader.show();

              pageNumber = pageNumber + 1;
              $.ajax({
                type: "GET",
                url: "/search-output",
                dataType: 'json',
                data: {
                  searchTerm: _param,
                  categories: _type,
                  facet: facet,
                  sort : sort,
                  page:pageNumber,
                  discontinued: discontinued
                },
                success: function (data) {
                  if(data.totalElements > data.size) {
                    var prodView = new ProdView(data, null, 1);
                  }
                  lastPage = data.last;
                  if(data.last == false){
                    loadProducts = 1;
                  }else{
                    loadProducts = 0;
                  }
                  loader.hide();
                }
              });
            }
          });

          var tab = null;
          var tabTrigger = function(tab) {
              var offset = jQuery('#tabs').offset().top - 100;
              jQuery('html, body').animate({ scrollTop: offset });
              jQuery("a[href='"+tab+"']")[0].click();

          }

          $('.tabActivate').click(function(){
            tab = $(this).attr('href');
            tabTrigger(tab);
          });

          $('#facetFinishAll').change(function(){
            var facetFilter = $(this).val();
            tabTrigger('#tab-0');
            $('#facetFinish').val($(this).val());
            $('#facetFinish').trigger('change');
          });

          $('#sortProductAll').change(function(){
            var sortFilter = $(this).val();
            tabTrigger('#tab-0');
            $('#sortProduct').val($(this).val());
            $('#sortProduct').trigger('change');
          });

          $(".slp_tabs li").click(function(event) {

            if($('#product-tab-link').length == 0) {
              $(".slp_tabs li a").removeClass("is-active");
              $(this).find("a").addClass("is-active");
              $(".tabs__tab").removeClass("is-active");
              $(".tabs__tab").eq($(this).index()).addClass("is-active");
              return false;
            }
          });
          function tabHide(){
            $(".search-result-tabs ul.tab-block li:first").addClass("tab-title-active");
            $(".search-result-tabs .tab-desc .tab-accordion-title:first").addClass("tab-accordion-active");
            $(".search-result-tabs .tab-desc .tab-data:first").addClass("tab-data-active");
          }

          /* Convert to camel case .*/
          function upperCamelCase(text) {
            return text.replace(/\w+/g,
              function(w){
                return w[0].toUpperCase() + w.slice(1).toLowerCase();
              });
          }

        }

    };

})(jQuery, Drupal, drupalSettings, Backbone);

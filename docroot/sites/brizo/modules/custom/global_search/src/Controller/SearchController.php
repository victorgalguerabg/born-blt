<?php

namespace Drupal\global_search\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\sync_vocabulary\VocabularySyncService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\Core\Form\FormBuilderInterface;

/**
 * Implementation of search controller.
 */
class SearchController extends ControllerBase {

  /**
   * To get current query string.
   *
   * @var Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * @var  \Drupal\Core\Form\FormBuilderInterface $formbuilder
   */
  protected $formbuilder;

  /**
   * @var  \Drupal\sync_vocabulary\VocabularySyncService $vocabularySyncService
   */
  protected $vocabularySyncService;

  /**
   * Implementation of constructor.
   */
  public function __construct(RequestStack $requestStack,
                              FormBuilderInterface $form_builder,
                              VocabularySyncService $vocabularySyncService) {
    $this->requestStack = $requestStack;
    $this->formbuilder = $form_builder;
    $this->vocabularySyncService = $vocabularySyncService;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
      // Load the service required to construct this class.
      $container->get('request_stack'),
      $container->get('form_builder'),
      $container->get('sync_vocabulary.sync')
    );
  }

  /**
   * Implementation of search results.
   */
  public function getSearchResults() {
    $searchKey = $this->requestStack->getCurrentRequest()->get('searchTerm');
    $searchType = $this->requestStack->getCurrentRequest()->get('type');
    $searchFilter = ($this->requestStack->getCurrentRequest()->get('filter')) ? $this->requestStack->getCurrentRequest()->get('filter') : 0;
    $config = $this->config('global_sync.api_settings');
    $prodPerPage = $config->get('product_per_page');
    $docPerSize = $config->get('document_per_page');
    $finishData = $this->vocabularySyncService->getFacetFinishData();
    $form = $this->formBuilder()->getForm(
      'Drupal\global_search\Form\GlobalSearchOnResultsForm'
    );

    return [
      '#theme' => 'search_results_page',
      '#data' => ['term' => $searchKey, 'filter' => $searchFilter, 'categories' => $searchType],
      '#attached' => [
        'drupalSettings' => [
          'productSetting' => [
            'size' => $prodPerPage
          ],
          'documentSetting' => [
            'size' => $docPerSize
          ],
          'finishData' => $finishData
        ],
        'library' =>
        [
          'global_search/searchResult',
        ],
      ],
      'form' => $form
    ];
  }

  /**
   * Implementation of support search results.
   */
  public function getSupportSearchResults() {

    return [
      '#theme' => 'support_search_results_page',
      '#attached' => [
        'library' =>
          [
            'global_search/customerSupport',
          ],
      ],
    ];
  }

}

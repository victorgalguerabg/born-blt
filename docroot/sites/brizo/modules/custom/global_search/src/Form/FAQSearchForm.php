<?php

namespace Drupal\global_search\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Implementation of FAQ search form.
 */
class FAQSearchForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'faq_search_form';
  }

  /**
   * Implementation for support search orm build().
   */
  public function buildForm(array $form, FormStateInterface $form_state, $args = NULL) {
    $form = [];
    $form['name'] = [
      '#type' => 'textfield',
      '#title' => 'Search FAQs',
      '#autocomplete' => 'off',
      '#attributes' => [
        'class' => [
          'form-control',
        ],
        'autocomplete' => 'off',
      ],
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => 'search',
      '#attributes' => [
        'class' => [
          'btn btn-primary btn-md',
        ],
      ],
    ];
    if ($args[0] == '/faq-search-result') {
      $form['#theme'] = 'support_search_result_block';
    }
    $form['#attached']['library'][] = 'global_search/customerSupport';
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $data = $form_state->getValue('name');
    $url = Url::fromRoute('faq_search.result', ['title' => $data]);
    $form_state->setRedirectUrl($url);
  }

}

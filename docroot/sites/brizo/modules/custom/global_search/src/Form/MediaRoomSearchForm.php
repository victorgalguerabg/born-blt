<?php
namespace Drupal\global_search\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Implementation of global search form.
 */
class MediaRoomSearchForm extends FormBase {

  /**
   * To access pdp methods using services.
   *
   * @var pdpServices
   */
  protected $pdpServices;


  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'media_room_search_form';
  }

  /**
   * Implementation fof Media room search form build().
   */
  public function buildForm(array $form, FormStateInterface $form_state, $arg = null) {
    $form = [];
    $form['search_input'] = [
      '#type' => 'search',
      '#placeholder' => $this->t('Search'),
      '#attributes' => [
        'class' => ['form-item__textfield form-item__textfield--required serach-input'],
      ],
    ];
    $form['filter_option'] = [
      '#type' => 'radios',
      '#title' => '',
      '#options' => [
        'media-tab' => 'Search Only Media Room',
        'entire-site' => 'Search Entire Site',
      ],
      '#default_value' => 'media-tab',
      '#attributes' => [
        'class' => ['radio'],
      ],
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => 'search',
    ];

    $form['#theme'] = 'media_room_search_block';
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    global $base_url;
    $filter = $form_state->getUserInput();
    $filterKey = '';
    $filtersArr = [];
    $searchPattern = $filter['search_input'];
    $searchFilter = $filter['filter_option'];
    $filtersArr = ['title' => $searchPattern, 'filter' => $searchFilter];
    $url = Url::fromRoute('global_search.search', $filtersArr);
    $form_state->setRedirectUrl($url);
  }

  /**
   * If you put model number, the page  automatically take you to the PDP page.
   */
  public function productSearch($typedString) {
    $config = $this->config('sync.api_settings');
    $domain = $config->get('sync.wcs_rest_api');
    $output = [];
    $method = 'GET';
    $headers = add_headers(TRUE);

    // Search Products.
    $searchProductUrl = $domain . $config->get('sync.search_products');
    $searchProductUrl = str_replace("{typedString}", $typedString, $searchProductUrl);
    $response = web_services_api_call($searchProductUrl, $method, $headers, '', 'SearchProducts');
    $pdpUrl = '';
    if ($response['status'] == 'SUCCESS') {
      $data = json_decode($response['response']);
      if (count($data->content) > 0) {
        foreach ($data->content as $content) {
          $output['products'][$content->name]['name'] = $content->name;
          $output['products'][$content->name]['description'] = $content->descriptionWithCollection;
          // $output['products'][$content->name]['url'] = ($this->pdpServices->getProductUrl($content->name) == '#') ? '/pdp/' . $content->name : $this->pdpServices->getProductUrl($content->name);
          //When user entered actual sku then redirect to PDP.
          if (strtolower($output['products'][$content->name]['name']) === strtolower($typedString)) {
            $pdpUrl = $output['products'][$content->name]['url'];
          }
        }
      }
    }
    return $pdpUrl;
  }

}

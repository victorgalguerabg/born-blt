<?php

namespace Drupal\global_search\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Implementation of global search form.
 */
class GlobalSearchOnResultsForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'global_search_on_results_form';
  }

  /**
   * Implementation of global search on content region form build().
   */
  public function buildForm(array $form, FormStateInterface $form_state, $arg = NULL) {
    $request = $this->getRequest();
    $searchPattern = $request->query->get('searchTerm');
    $form['#var']['searchterm'] = $searchPattern;
    $form['name'] = [
      '#type' => 'textfield',
      '#value' => urldecode($searchPattern),
      '#attributes' => [
        'placeholder' => 'Search',
        'class' => [
          'form-item__textfield search-results',
        ],
      ],
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => 'search',
      '#attributes' => [
        'class' => [
          'btn btn-default search-results-submit',
        ],
      ],
      '#prefix' => '<div class="action-container">',
      '#suffix' => '</div>'
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $filter = $form_state->getUserInput();
    $filterKey = '';
    $filtersArr = [];
    $searchPattern = $filter['name'];
    $filtersArr = ['searchTerm' => $searchPattern];
    if (!empty($filter['filter_option'])) {
      $filterKey = $filter['filter_option'];
      $searchPattern = $filter['search_box'];
      $filtersArr = ['searchTerm' => $searchPattern, 'documentLookFor' => $filterKey];
    }
    $url = Url::fromRoute('global_search.search', $filtersArr);
    $form_state->setRedirectUrl($url);
  }

}

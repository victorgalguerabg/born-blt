<?php

namespace Drupal\brizo_content_alerts\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class BrizoContentAlertsForm extends FormBase {

    /**
   * {@inheritdoc}
   */
  public function getEditableConfigNames() {
    return [
      'brizo_content_alerts.config',
    ];
  }

  /**
   * Returns a unique string identifying the form.
   *
   * The returned ID should be a unique string that can be a valid PHP function
   * name, since it's used in hook implementation names such as
   * hook_form_FORM_ID_alter().
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'brizo_content_alerts_form';
  }

  /**
   * Form constructor.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The form structure.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('brizo_content_alerts.config');
    $form['content_alert_recipients'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Recipients'),
      '#description' => $this->t('Enter recipients, one per line.'),
      '#default_value' => $config->get('content_alert_recipients'),
    ];

    // Group submit handlers in an actions element with a key of "actions" so
    // that it gets styled correctly, and so that other modules may add actions
    // to the form. This is not required, but is convention.
    $form['actions'] = [
      '#type' => 'actions',
    ];
    // Add a submit button that handles the submission of the form.
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];
    return $form;
    //return parent::buildForm($form, $form_state);

  }

/**
   * Validate the title and the checkbox of the form
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $recipients = $form_state->getValue('content_alert_recipients');
    if (empty($recipients)){
      $form_state->setErrorByName('content_alert_recipients', $this->t('You must fill recipients'));
    }
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $messenger = \Drupal::messenger();
    $messenger->addMessage('Recipients saved: '.$form_state->getValue('content_alert_recipients'));
    \Drupal::configFactory()->getEditable('brizo_content_alerts.config')
      ->set('content_alert_recipients', $form_state->getValue('content_alert_recipients'))
      ->save();
  }
}


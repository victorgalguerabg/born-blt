<?php

namespace Drupal\brizo_showroom_locator\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'Hello' Block.
 *
 * @Block(
 *   id = "showroom_locator_find_block",
 *   admin_label = @Translation("Showroom Locator Custom block"),
 *   category = @Translation("Showroomm Locator"),
 * )
 */
class ShowroomLocatorFindBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $form = \Drupal::formBuilder()->getForm('\Drupal\brizo_showroom_locator\Form\ShowroomLocatorForm');
    return $form;
  }

}

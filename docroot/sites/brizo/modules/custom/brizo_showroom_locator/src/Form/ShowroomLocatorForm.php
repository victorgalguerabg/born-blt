<?php

namespace Drupal\brizo_showroom_locator\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\RedirectCommand;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\delta_services\DeltaService;
use Drupal\brizo_design_gallery\DesignGalleryService;
use Drupal\Core\Url;
/**
 * Class DeltaServicesConfigForm.
 */
class ShowroomLocatorForm extends FormBase {
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'showroom_locator_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['showroom_title'] = [
      '#type' => 'markup',
      '#markup' => '<div class="showroom-data"><div class="icon-menu-close"></div><div class="showroom-title">Find showrooms Near You</div>',
      '#weignt' => '1'
    ];
    $form['showroom_locator'] = [
      '#type' => 'textfield',
      '#title' => t('Enter Zip/Postal Code'),
      '#maxlength' => 255,
      '#size' => 64,
      '#weight' => '2',
      '#prefix' => '<div class="showroom-input">',
      '#suffix' => '</div>',
      '#attributes' => [
        'class' => [
          'form-item__textfield form-item__textfield--required form-text',
        ],
        'id' => [
          'edit-showroom-locator',
        ],
      ],
    ];

    $form['showroom_links'] = [
      '#type' => 'markup',
      '#markup' => '<div class="showroom-links"><ul><li><a class="button button--primary" href="/showroom-locator">Use current location</a></li></ul></div></div>',
      '#weight' => '3',
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => 'submit',
      '#attributes' => [
        'class' => [
          'btn btn-default search-results-submit',
        ],
        'style' => 'display:none',
      ],
      '#prefix' => '<div class="action-container">',
      '#suffix' => '</div>'
    ];
    return $form;

  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Can add validation if required.
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $filter = $form_state->getUserInput();
    $searchPattern = $filter['showroom_locator'];
    $filtersArr['query'] = ['addressline' => $searchPattern];

    global $base_url;
    $base_url_parts = parse_url($base_url);
    $host = $base_url_parts['host'];
    $form_state->setRedirectUrl(Url::fromUri('internal:' . '/showroom-locator', $filtersArr));
  }

}

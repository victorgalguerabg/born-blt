<?php

namespace Drupal\brizo_plp\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a block.
 *
 * @Block(
 *   id = "facet_block",
 *   admin_label = @Translation("facet_block"),
 *   deriver = "Drupal\brizo_plp\Plugin\Derivative\FacetsDerivativeBlock"
 * )
 */
class FacetsBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $id = $this->getDerivativeId();
    $items = [];
    $items['id'] = $id;
    $items['output'] = $id;
    $build = [
      '#theme' => 'facets_template',
      '#items' => $items,
    ];
    return $build;
  }

}

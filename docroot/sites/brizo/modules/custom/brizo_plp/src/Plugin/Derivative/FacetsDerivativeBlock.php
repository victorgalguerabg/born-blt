<?php

namespace Drupal\brizo_plp\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;

/**
 * Provides a block.
 *
 * @Block(
 *   id = "facet_derivative_block",
 *   admin_label = @Translation("facet_derivative_block"),
 * )
 */
class FacetsDerivativeBlock extends DeriverBase {

  /**
   * Implementation of getDerivativeDefinitions().
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $facetSettings = \Drupal::service('config.factory')
      ->getEditable('brizo_plp.settings');
    foreach ($facetSettings->get() as $key => $block_url) {
      $this->derivatives[$key] = $base_plugin_definition;
      $this->derivatives[$key]['admin_label'] = $key . " facet block";
    }
    return $this->derivatives;
  }

}

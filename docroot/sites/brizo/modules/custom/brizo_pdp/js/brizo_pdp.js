(function ($, Drupal) {

  Drupal.behaviors.parts_list = {
    attach: function (context, drupalSettings) {

      // toggle sku match sub item on load

        var curentUrl = window.location.href;
        urlParam = curentUrl.split("/");
        var sku = urlParam[urlParam.length-1];
        sku_list = sku.split("--");
        if ( sku_list.length > 0 ) {
          for( var i = 0; i < sku_list.length; i++ ) {
              if( $("." + sku_list[i] + "-PRD.sub-part-area").length > 0) {
                  $("." + sku_list[i] + "-PRD.sub-part-area").show();
              }
          }
        }

        $(".part-products", context).on("click", function(){
          var img_path = $(this).attr('data-url');

          // adding active class for the selected items.
          var type_selected = $(this).attr('data-type');
          var index_selected = $(this).attr('data-index');

          $('.part-products[data-type='+type_selected+']').removeClass('pdp-product-active');
          $(this).addClass('pdp-product-active');

          // set session
          //localStorage.getItem("Cold_Handle"+modelNo);
          if(sku_list.length > 0) {
             var modelNo = sku_list[0].split("-")[0];
             modelNo = modelNo.replace(/[\\/:*?"()!<>|]/g,'');
           }
           localStorage.setItem(type_selected+ '_' + modelNo, index_selected );
           localStorage.setItem('test', 'rf');

          // Change the image slider on click
          if( img_path != "") {
            thumb_img = img_path.replace("/md/", '/sm/');
            $(".prod-big-img").find("img").attr('src', img_path);
            $(".slick-track .thumb-img-block").removeClass("active-thumbnail");
            first_thumb = $(".slick-track .thumb-img-block").first();
            $(first_thumb).addClass('active-thumbnail');
            $(first_thumb).find("img").attr('srcset',img_path);
            $(first_thumb).find("img").attr('src',img_path);
            $(".download__img-wrapper a").attr('href', img_path);

            // Mobile sliderImage
            mobile_slider = $(".mobile-thumb-img .slick-current").first();
            $(mobile_slider).addClass('active-thumbnail');
            $(mobile_slider).find("img").first().attr('srcset',img_path);
            $(mobile_slider).find("img").attr('src',img_path);
            $('.slick-slider').slick('slickGoTo', 0);
          }
          // show and hide sub sub_items

          var targetClass = $(this).attr("data-ref-part");

          if( targetClass != "PRD-SUB"){
              $(".sub-part-area").hide();
          }

          if( targetClass != "PRD"){
              $("." + targetClass ).show();
          }
         else {
            $(".sub-part-area").hide();
          }

          // Change product as shown content
          var targetPasClass = $(this).attr("data-target");
          var price = $(this).attr("data-price");
          var modalNo = $(this).find(".part-modalno").text();
          var title = $(this).find(".part-titles").html();
          var htlText = '<div class="col-sm">' + title + '<span>'+ modalNo +'</span></div><div class="prd-as-shown-price"> $'+ price +'</div>';
          var htlText2 = '<div class="col-sm">' + title + '<span>'+ modalNo +'</span></div><div class="prd-as-shown-price"></div>';
          // sub items
          if( $(this).attr('data-ref-part') !== "PRD" && $(this).attr('data-ref-part') !== "PRD-SUB" ){
              var sub_item_class = $(this).attr('data-ref-part');
              sub_item = $("." + sub_item_class).find(".part-products").first();
              var sub_label = $(sub_item).attr("data-label");
              if( $(".row."+sub_label).length == 0){
                var sub_title = $(sub_item).find(".part-titles").html();
                var sub_modalNo = $(sub_item).find(".part-modalno").text();
                var sub_price = $(sub_item).attr("data-price");

                var numItems = $('.prd-as-shown .row').length;
                numItems = parseInt(numItems) - 2 ;
                var sub_html = '<div class="row prd-itms '+ sub_label +'"><div class="col-sm">'+ sub_title +'<span>'+sub_modalNo+'</span></div><div class="prd-as-shown-price">$'+sub_price+'</div></div>';
                $(".prd-as-shown>div:eq("+ numItems +")").after(sub_html);
              }

          }
          else if ($(this).attr('data-ref-part') == "PRD-SUB") {
            var sub_section = 1;
          }
          else {
              if( $('.prd-as-shown .prd-itms').length > 0 ) {
                  $('.prd-as-shown .prd-itms').remove();
              }
          }

          if ($( "body" ).is(".brizofaucet-ca")) { //This body class is added in the canada domain configuration
            $("." + targetPasClass).html(htlText); //without price
          } else {
            $("." + targetPasClass).html(htlText);
          }

          // calculate total price.

          var total_price = 0;
          $(".prd-as-shown .row").each(function(){
              if ( !$(this).hasClass('listPrice') ) {
                list_price = $(this).find(".prd-as-shown-price").text();
                price = parseFloat(list_price.replace(/[$,]/g,""));
                total_price = total_price + price;
              }
          });
          listPriceTotal = "$" + total_price.toFixed(2);
          listPriceTotal = thousands_separators(listPriceTotal);
          $(".listPrice .prd-as-shown-price").text(listPriceTotal)

          var total_price = 0;
          $(".prd-as-shown .row").each(function(){
              if ( !$(this).hasClass('calistPrice') ) {
                list_price = $(this).find(".prd-as-shown-price").text();
                price = parseFloat(list_price.replace(/[$,]/g,""));
                if(price) {
                  total_price = total_price + price;
                }
                
              }
          });
          if(total_price) {
            listPriceTotal = "$" + total_price.toFixed(2);
            listPriceTotal = thousands_separators(listPriceTotal);
          } else {
            listPriceTotal = "N/A";
          }

          $(".calistPrice .prd-as-shown-price").text(listPriceTotal)
          $(".calistPrice .prd-as-shown-price").addClass("tot-calculated");

          //Mark 0.0 as NA
          if ($("body").hasClass("brizofaucet-ca")) {
            $(".prd-as-shown .row").each(function(){
              if ( !$(this).hasClass('calistPrice')) {
                list_price = $(this).find(".prd-as-shown-price").text();
                price = parseFloat(list_price.replace(/[$,]/g,""));
                if(price === 0.0 || price == '') {
                  $(this).find(".prd-as-shown-price").text("N/A");
                }
              }
            });
          }
        });
        

        // Method to add thousands seperator of a number
        function thousands_separators(num)
        {
          var num_parts = num.toString().split(".");
          num_parts[0] = num_parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
          return num_parts.join(".");
        }
    }
   }

   // Added to select the handle_accent etc by defaule
   Drupal.behaviors.parts_list_details = {
     attach: function (context, drupalSettings) {

       var curentUrl = window.location.href;
       urlParam = curentUrl.split("/");
       var sku = urlParam[urlParam.length-1];
       sku_list = sku.split("--");
       if(sku_list.length > 0) {
          var modelNo = sku_list[0].split("-")[0];
          modelNo = modelNo.replace(/[\\/:*?"()!<>|]/g,'');
        }

       if ( sku_list.length > 1 ) {
         for( var i = 0; i < sku_list.length; i++ ) {
           // hilight the url matching parts sku as default
           if ($('.part-products[data-type=body_assembly]').length > 0 ) {

             if ( $('.part-products[data-modalno='+ sku_list[i] +'][data-type=body_assembly]').length > 0 ) {
               $('.part-products[data-modalno='+ sku_list[i] +'][data-type=body_assembly]').addClass('pdp-product-active');
               $('.part-products[data-modalno='+ sku_list[i] +'][data-type=body_assembly]').click();
             }

           } // body_assembly


           if ($('.part-products[data-type=handle_accent]').length > 0 ) {
               if ( $('.part-products[data-modalno='+ sku_list[i] +'][data-type=handle_accent]').length > 0 ) {
                 $('.part-products[data-modalno='+ sku_list[i] +'][data-type=handle_accent]').addClass('pdp-product-active');
                 $('.part-products[data-modalno='+ sku_list[i] +'][data-type=handle_accent]').click();
               }
           } // Handle_or_Accent

           if($('.part-products[data-type=riser_union]').length > 0 ) {
               if ( $('.part-products[data-modalno='+ sku_list[i] +'][data-type=riser_union]').length > 0 ) {
                 $('.part-products[data-modalno='+ sku_list[i] +'][data-type=riser_union]').addClass('pdp-product-active');
                 $('.part-products[data-modalno='+ sku_list[i] +'][data-type=riser_union]').click();
               }
           } // riser_union

           if($('.part-products[data-type=rough_valve]').length > 0 ) {

               if ( $('.part-products[data-modalno='+ sku_list[i] +'][data-type=rough_valve]').length > 0 ) {
                 $('.part-products[data-modalno='+ sku_list[i] +'][data-type=rough_valve]').addClass('pdp-product-active');
                 $('.part-products[data-modalno='+ sku_list[i] +'][data-type=rough_valve]').click();
               }

           }


           if($('.part-products[data-type=cold_handle]').length > 0 ) {

               if ( $('.part-products[data-modalno='+ sku_list[i] +'][data-type=cold_handle]').length > 0 ) {
                 $('.part-products[data-modalno='+ sku_list[i] +'][data-type=cold_handle]').addClass('pdp-product-active');
                  $('.part-products[data-modalno='+ sku_list[i] +'][data-type=cold_handle]').click();
               }

           } // Cold_Handle


         }
       } // if sku_list
       else {

         if ($('.part-products[data-type=body_assembly]').length > 0 ) {
           if (typeof(Storage) !== "undefined") {
             var body_assembly_index = localStorage.getItem("body_assembly_"+modelNo);
             if(body_assembly_index){
               $('.part-products[data-index='+ body_assembly_index +'][data-type=body_assembly]').addClass('pdp-product-active');
               $('.part-products[data-index='+ body_assembly_index +'][data-type=body_assembly]').click();
             }
             else {
               $('.part-products[data-type=body_assembly]').first().addClass('pdp-product-active');
               $('.part-products[data-type=body_assembly]')[0].click();
             }
           }
           else {
             $('.part-products[data-type=body_assembly]').first().addClass('pdp-product-active');
             $('.part-products[data-type=body_assembly]')[0].click();
           }
         } // body_assembly

         if ($('.part-products[data-type=handle_accent]').length > 0 ) {
           if (typeof(Storage) !== "undefined") {
             var handle_accent_index = localStorage.getItem("handle_accent_"+modelNo);
             if(handle_accent_index){
               $('.part-products[data-index='+ handle_accent_index +'][data-type=handle_accent]').addClass('pdp-product-active');
               $('.part-products[data-index='+ handle_accent_index +'][data-type=handle_accent]').click();
             }
             else {
               $('.part-products[data-type=handle_accent]').first().addClass('pdp-product-active');
               $('.part-products[data-type=handle_accent]')[0].click();
             }
           }
           else {
             $('.part-products[data-type=handle_accent]').first().addClass('pdp-product-active');
             $('.part-products[data-type=handle_accent]')[0].click();
           }
         } // handle_Accent


         if($('.part-products[data-type=riser_union]').length > 0 ) {
           if (typeof(Storage) !== "undefined") {
             var riser_union_index = localStorage.getItem("riser_union_"+modelNo);
             if(riser_union_index){
               $('.part-products[data-index='+ riser_union_index +'][data-type=riser_union]').addClass('pdp-product-active');
               $('.part-products[data-index='+ riser_union_index +'][data-type=riser_union]').click();
             }
             else {
               $('.part-products[data-type=riser_union]').first().addClass('pdp-product-active');
               $('.part-products[data-type=riser_union]')[0].click();
             }
           }
           else {
             $('.part-products[data-type=riser_union]').first().addClass('pdp-product-active');
             $('.part-products[data-type=riser_union]')[0].click();
           }
         }


         if($('.part-products[data-type=rough_valve]').length > 0 ) {
           if (typeof(Storage) !== "undefined") {
             var rough_valve_index = localStorage.getItem("rough_valve_"+modelNo);
             if(rough_valve_index){
               $('.part-products[data-index='+ rough_valve_index +'][data-type=rough_valve]').addClass('pdp-product-active');
               $('.part-products[data-index='+ rough_valve_index +'][data-type=rough_valve]').click();
             }
             else {
               $('.part-products[data-type=rough_valve]').first().addClass('pdp-product-active');
               $('.part-products[data-type=rough_valve]')[0].click();
             }
           }
           else {
             $('.part-products[data-type=rough_valve]').first().addClass('pdp-product-active');
             $('.part-products[data-type=rough_valve]')[0].click();
           }
         }

         if($('.part-products[data-type=cold_handle]').length > 0 ) {
           if (typeof(Storage) !== "undefined") {
             var cold_handle_index = localStorage.getItem("cold_handle_"+modelNo);
             if(cold_handle_index){
               $('.part-products[data-index='+ cold_handle_index +'][data-type=cold_handle]').addClass('pdp-product-active');
               $('.part-products[data-index='+ cold_handle_index +'][data-type=cold_handle]').click();
             }
             else {
               $('.part-products[data-type=cold_handle]').first().addClass('pdp-product-active');
               $('.part-products[data-type=Cold_cold_handleHandle]')[0].click();
             }
           }
           else {
             $('.part-products[data-type=cold_handle]').first().addClass('pdp-product-active');
             $('.part-products[data-type=cold_handle]')[0].click();
           }
         } // Cold_Handle

       } // $deltaservice sku_list

     }
   };

  Drupal.behaviors.downloadImage = {
    attach: function (context, drupalSettings) {

      $('.download__img-wrapper a', context).on('click', function(e){
          e.preventDefault();
          var img_path = $(".prod-big-img").first().find('img').attr('src');
          $("#edit-downlod-file-url").val(img_path);
          $("#edit-downlod-file-type").val('image');
          $("#download-handler-form").submit();
          return true;
      });
      //download pdf
      $('.download-tech-file', context).on('click', function(e){
          e.preventDefault();
          var file_path = $(this).attr('href');
          $("#edit-downlod-file-url").val(file_path);
          $("#edit-downlod-file-type").val('image');
          $("#download-handler-form").submit();
          return true;
      });
      // download collection pdf
      $('.download-collection-file', context).on('click', function(e){
          e.preventDefault();
          var file_path = $(this).attr('href');
          $("#edit-downlod-file-url").val(file_path);
          $("#edit-downlod-file-type").val('image');
          $("#download-handler-form").submit();
          return true;
      });
     }
  };
})(jQuery, Drupal);

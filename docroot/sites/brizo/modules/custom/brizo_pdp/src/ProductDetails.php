<?php

namespace Drupal\brizo_pdp;

use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Path\AliasManagerInterface;
use Drupal\Core\Url;
use Drupal\delta_services\DeltaService;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\product_sync\ProductImport;
use Drupal\taxonomy\Entity\Term;
use Symfony\Component\HttpFoundation\Request;
use Drupal\delta_cache\DeltaCacheService;
use Drupal\Core\Form\FormBuilderInterface;
use \Symfony\Component\HttpFoundation\Response;
use \Symfony\Component\HttpFoundation\Cookie;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Drupal\delta_custom\GlobalProduct;
use Drupal\Core\File\FileSystemInterface;
use Drupal\file\Entity\File;



/**
 * Provides service for Product Description Page.
 */
class ProductDetails {

  /**
   * Drupal\Core\Logger\LoggerChannelFactoryInterface definition.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */

  protected $loggerChannelFactory;


  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Drupal\Core\Path\AliasStorageInterface definition.
   *
   * @var \Drupal\Core\Path\AliasStorageInterface
   */
  protected $pathAliasStorage;

  /**
   *
   * @param \Drupal\product_sync\ProductImport $productImport
   *   The deltaservice to make API calls.
   */

   protected $productImport;


   /**
    * The current route match.
    *
    * @var \Drupal\delta_cache\DeltaCacheService
    */
   protected $deltaCache;

  /**
   * formBuilder.   *
   */

  protected $formBuilder;

  /**
    * Drupal\Core\TempStore\PrivateTempStoreFactory definition.
    *
    * @var \Drupal\Core\TempStore\PrivateTempStoreFactory
    */
   private $tempStoreFactory;


  /**
   * Drupal\global_product\GlobalProduct definition.
   *
   * @var \Drupal\global_product\GlobalProduct
  */
  protected $globalproduct;

  /**
   * {@inheritdoc}
   */

  public function __construct(DeltaService $deltaservice, ConfigFactoryInterface $config_factory, LoggerChannelFactoryInterface $loggerChannel, EntityTypeManagerInterface $entity_type_manager, ProductImport $product_import, DeltaCacheService $deltaCache, FormBuilderInterface $form_builder, PrivateTempStoreFactory $tempStoreFactory, GlobalProduct $globalproduct) {
    $this->deltaservice = $deltaservice;
    $this->configFactory = $config_factory;
    $this->loggerChannelFactory = $loggerChannel;
    $this->entityTypeManager = $entity_type_manager;
    $this->pathAliasStorage = $this->entityTypeManager->getStorage('path_alias');
    $this->productImport = $product_import;
    $this->deltaCache = $deltaCache;
    $this->formBuilder = $form_builder;
    $this->tempStoreFactory = $tempStoreFactory;
    $this->globalproduct = $globalproduct;
  }


  /**
   * {@inheritdoc}
   */


   public function productDetailsPage($sku) {

     $collection_url = '';
     $output['download_image_enabled'] = 0;
     $config = $this->configFactory->get('brizo_pdp.settings');
     $recently_viewed_enabled = $config->get('recently_viewd_products');
     $output['recently_viewed_avail'] = 0;
     $link_avail = 0;
     $domain = '';
     if (\Drupal::service('module_handler')->moduleExists('domain')) {
       $loader = \Drupal::service('domain.negotiator');
       $current_domain = $loader->getActiveDomain();
       if ($current_domain) {
         $domain = $current_domain->get('name') . '_';
       }
     }
     // set Cookies
     if ( $recently_viewed_enabled == 1) {

       $viewed_product_id = $this->getRecentlyViewedProducts();
       $set_cookies = $this->setRecentlyViewedProducts($sku, $viewed_product_id);
     }
     $sync_config = $this->configFactory->get('delta_services.deltaservicesconfig');
     $foundationalAttribute = $sync_config->get('foundational_attribute');
     $catch_data = $this->deltaCache->deltaGetCache($domain.'_PDP_'. $sku);
     if(!empty($catch_data) && is_array($catch_data)) {
       if (!isset($catch_data[$foundationalAttribute]) || empty($catch_data[$foundationalAttribute]) || $catch_data[$foundationalAttribute] !== 'T') {
        //throw new NotFoundHttpException();
        return $this->redirectNoProductFound($sku);
       }
       $output = $catch_data;
     }
     else {
       //      $this->deltaCache->deltaSetCache($cacheId, $output);
       $response = $this->productBaseDetails($sku);
       $output['product_as_shown'] = [];
       $output['categories'] = [$response['product_category']];
       $data = json_decode($response['product_response_data'][0]['value']);
       //Redirect to 404, when the AvailableToOrderDate /  AvailableToShipDate is 9999-12-31
       $validateOrderShipDate = $sync_config->get('order_ship_date');
       if((!(empty($data->values->AvailableToOrderDate)) && ($data->values->AvailableToOrderDate === $validateOrderShipDate)) || ((!(empty($data->values->AvailableToShipDate))) && ($data->values->AvailableToShipDate === $validateOrderShipDate)))
       {
         //throw new NotFoundHttpException();
         //throw new \Symfony\Component\HttpFoundation\RedirectResponse(\Drupal::url('brizo_pdp.no_modal_number_kitchen'));
         return $this->redirectNoProductFound($sku);

       }
       if (!isset($data->values->{$foundationalAttribute}) || empty($data->values->{$foundationalAttribute}) || $data->values->{$foundationalAttribute} !== 'T') {
        //throw new NotFoundHttpException();
        return $this->redirectNoProductFound($sku);
       }
       $output[$foundationalAttribute] = $data->values->{$foundationalAttribute};
       $description = $data->description;
       $output['name']['#markup'] = $description;
       $output['description'] = $description;
       $output['collections'] = (isset($data->values->Collection))?$data->values->Collection:$data->values->defaultCollection;
       $output['name'] = $data->name;
       $output['model'] = $data->name;
       $output['listPrice'] = number_format($data->values->ListPrice, 2);
       if($data->values->CAListPrice  ) {
        $output['caprice'] = number_format($data->values->CAListPrice, 2); //Total
       }
       $urlPatternCollection  = $config->get('pdp_collection_url_patterns');
       $output['download_file_label']  = $config->get('download_file');
       $output['view_file_label']  = $config->get('view_file');
       $output['view_collection_link_label']  = $config->get('view_full_collection');
       $output['download_collection_pdf_label']  = $config->get('download_collection_pdf');
       $output['technical_spec_avail'] = 0;
       $output['technical_spec_attr_avail'] = 0;
       $output['shower_image_avail'] = 0;
       $output['related_prd_avail'] = 0;
       $output['config_options_avail'] = 0;
       $output['collections'] = trim($output['collections']);
       $configOptions = [];
       $output['short_description'] = $data->values->PBShortDescription;
       if( $data->values->discontinued == 1) {
         $output['ribbontext'] = $config->get('discontinued_prd_label');
       }
       if($data->values->FltWebExclusiveCustomerItem == 'T' && $data->values->WebExclusiveCustomerItem == 'Ferguson')
       {
         $output['ribbontext'] = 'Ferguson';
         $output['ribboncolor'] = strtolower($data->values->WebExclusiveCustomerItem);
       }
       $availableToOrderDate = isset($data->values->AvailableToOrderDate) ? $data->values->AvailableToOrderDate : '';
       $availableToShipDate = isset($data->values->AvailableToShipDate) ? $data->values->AvailableToShipDate : '';
       $orderDate = strtotime($availableToOrderDate);
       $shipDate = strtotime($availableToShipDate);
       $currentDate = strtotime(date('Y-m-d', time()));
       if ($currentDate < $shipDate) {
         if ($currentDate > $orderDate) {
           $output['ribbontext'] = "Coming Soon";
           $output['ribboncolor'] = "brizo-teal";
         }
       }
      $term_id = $this->productImport->getCollectionTermByName($output['collections']);

       if ( $output['collections'] != '' && (count($data->collections) > 0) && $output['collections'] != 'Brizo' ) {
            $collection_name = htmlentities($output['collections']);
            $collection_name = str_replace('&lt;sup&gt;&amp;trade;&lt;/sup&gt;', '', $collection_name);
            $collection_name = str_replace('&lt;sup&gt;&amp;reg;&lt;/sup&gt;', '', $collection_name);
            $collection_name = str_replace(' ', '-', $collection_name);
            $collection_name = strtolower($collection_name);
            if( $term_id != '') {
              $collection_url = str_replace('{collection-name}', $collection_name, $urlPatternCollection);
              $collection_url = str_replace('{CATEGORY}', $response['product_category'], $collection_url);
              // Check the collection link avail
              $storage = $this->entityTypeManager->getStorage('menu_link_content');
              $menu_name = 'main';
              $menu_links = $storage->loadByProperties(['menu_name' => $menu_name]);
              foreach ($menu_links as $mlid => $menu_link) {
                $menu_title = strtolower($menu_link->title->value);
                $menu_title = str_replace(' ', '-', $menu_title);

                  if ( $menu_title == $collection_name) {
                    $link_avail = 1;
                  }
                  $link_segment = explode(":", $menu_link->link->uri);
                  if (count($link_segment) > 0 ) {
                    $link = $link_segment[1];
                    if($collection_url == $link) {
                        $link_avail = 1;
                    }
                  }
              }
              if($link_avail != 1) {
                $collection_url = '#';
              }
              // EOF Check the collection link avail
            }
            else {
              $collection_url = '#';
            }

       }
       else {
            $collection_url = '#';
            $output['collections'] = 'Brizo';
          }
        $output['viewAllCollectionLink'] =  $collection_url;
        //get collection pdf

       if ( $collection_url != "#") {
         if ($term_id != "" ) {
           $collection_pdf_url = $this->getCollectionPDF($term_id, $response['product_category']);
           if ($collection_pdf_url != "") {

             $output['collection_pdf_link_avail'] = 1;
             $output['collection_pdf_link'] = $collection_pdf_url;
             $collection_file_name = explode("/", $collection_pdf_url);
             $output['collection_file_name'] = $collection_file_name[count($collection_file_name)-1];
           }
         }
       }
       else {
         $ouput['collection_pdf_link_avail'] = 0;
       }

       $output['ConsumerCopy'] = isset($data->values->ConsumerCopy) ? $data->values->ConsumerCopy : '';
       //Product as shown data
       $output['product_as_shown_title'] = $config->get('product_as_shown');
       $output['product_as_shown_list_price_title'] = $config->get('product_as_shown_list_title');
       // Product as shown
       if(!empty($response['product_shown'])) {
          $prdshown = json_decode($response['product_shown']);
          $output['product_as_shown'] = $this->getPrdAsShown($prdshown, $sku);
       }
       else {
        if($data->name == $data->values->BaseModel || $data->name == $data->values->ModelNumber) {
          $output['product_shown'][] = [
            'title'=> $data->description, 
            'sku'=> $data->name,
            'price' =>  $data->values->ListPrice,
            'caprice' =>  number_format($data->values->CAListPrice, 2),
          ];
          if( count($data->configOptions) > 0 ) {
            $output['product_shown'] = $this->getProductShownConfigData($output['product_shown'], $data->configOptions, $data->name, 1 );
          }
          $prdshown = $output['product_shown'];
          $output['product_as_shown'] = $this->getPrdAsShown($prdshown, $sku);
        }
        else {
          $productId = key($this->productImport->productExists($data->values->ModelNumber));
          if(!empty($productId)) {
            $title = $this->getProductTitle($productId);
            $price = $this->getProductPrice($productId);
          }
          else {
            $modelNumberdata = $this->productImport->isValidProductSKU($data->values->ModelNumber);
            if($modelNumberdata['status'] == 1){
              $serice_config = $this->configFactory->get('delta_services.deltaservicesconfig');
              $siteName = $serice_config->get('global_site_name');
              $productId = $this->productImport->createProduct($modelNumberdata['productData'],$siteName);
              if (!empty($productId)) {
                $title = $this->getProductTitle($productId);
                $price = $this->getProductPrice($productId);
              }
            }
          }
          $output['product_shown'][] = [
            'title'=> $title,
            'sku' => $data->values->ModelNumber,
            'price' =>  $price,
            'caprice' =>  number_format($data->values->CAListPrice, 2),
            'optionLabel' => "PRD"
          ];
          
          if( count($data->configOptions) > 0 ) {
            $output['product_shown'] = $this->getProductShownConfigData($output['product_shown'], $data->configOptions, $data->name );
          }
          $output['product_as_shown'] = $output['product_shown'];
        } // else name = baseModal
        $update_product = $this->productImport->updateProductShown($output['product_shown'], $response['product_id']);
      }

       // Design Files
       $file_types = $this->getFileExtension();
       $design_files = $this->getDesignFiles($data->assets, $file_types);

       if( count($design_files) > 0 ) {
         $output['design_files'] = $design_files;
         $output['design_files_avail'] = 1;
         $output['design_files_title'] = $config->get('cad_design_title');
         }
         else {
           $output['design_files_avail'] = 0;
         }

       //image slider
       $output['heroImage'] = $data->heroImage;
       $output['heroImageSmall'] = str_replace("/md/", "/sm/", $output['heroImage']);
       $output['heroImageLarge'] = str_replace("/md/", "/lg/", $output['heroImage']);
       $slider_image = $this->getproductSliderImages($data);
       $output['assets'] = $slider_image['assets'];

       // Download image
       if($this->isModuleEnabled('download_image')) {
         $output['download_image_enabled'] = 1;
         $output['download_link']['text'] = t('Download Image');
       }
       //Breadcrumb
       $breadcrumb = $this->create_breadcrumb($response['product_category'], $data->values->Series);
       $output['breadcrumb'] = $breadcrumb;

       // Features bulltet in points
       $faturesBenefitsResult = $this->getProductFeature($data);
       if (!empty($faturesBenefitsResult)) {
         $featuresTranslated = $this->deltaservice->translationsCode($faturesBenefitsResult);
         $output['faturesBenefits'] = $featuresTranslated;
         $output['features_title'] = $config->get('features_title');
       }

       // technical Document
       $tecnical_document_type = $this->getTechicalFileType();
       $technical_documents = $this->getTechnicalDocuments($data->assets, $tecnical_document_type);
       if ( count($technical_documents) > 0) {
         $output['technical_documents'] = $technical_documents;
         $output['technical_documents_avail'] = 1;
         $output['technical_doc_title'] = $config->get('technical_design_title');
        }
       else {
         $output['technical_documents_avail'] = 0;
       }

       // finishOptions
       $output['finish_title'] = $config->get('finish_label');
       $output['finish'] = $data->values->Finish;
       if (count($data->finishOptions) > 0) {
         $output['finishOptions'] = $this->getFinishOptionsList($data->finishOptions, $response['product_category'], $sku);
        }

        // Technical Specification
        $technical_spec_type = $config->get('pdp_technical_specification');
        $output['technical_spec_title'] = $config->get('technical_specific_title');
        if ( !empty($technical_spec_type)) {
          $technical_spec = $this->getTechnicalSpecification($data->assets,$technical_spec_type);
          if (count($technical_spec) > 0) {
            $output['technical_spec_avail'] = 1;
            $output['technical_spec_file'] = $technical_spec;

          }
        }
        // get product configoptions body assembly, handle accent, rough value, riser union
        $configOptions = $this->getProductConfigOptions($data->configOptions, $data->assets);
        if (count($configOptions) > 0) {
          $output['config_options_avail'] = 1;
          $output['config_options'] = $configOptions;
          $output['handle_accsent_title'] = $config->get('handle_accent_title');
          $output['rough_valve_title'] = $config->get('rought_value_title');
          $output['body_assembly_title'] = $config->get('body_assembly_title');
          $output['riser_union_title'] = $config->get('riser_union_title');

        }

        // Technical specification Attributes

        $technicaAttribute = $config->get('pdp_technical_spec_attributes');
        if( $technicaAttribute != '') {
          $getTechnicalAttribute = $this->getTechnicalAttributes($data->values, $technicaAttribute);
          if (count($getTechnicalAttribute) > 0 ) {
            $output['technical_spec_attr_avail'] = 1;
            $output['tech_spec_attr'] = $getTechnicalAttribute;
          }
          $showerIconparam = $config->get('pdp_technical_spec_image_param');
          if ( $showerIconparam != "") {
            $showerIconkeys = explode("|", $showerIconparam);
            if(array_key_exists($showerIconkeys[0], $data->values ) ){
              $key_name = $showerIconkeys[0];
              $image_key = $showerIconkeys[1];
              if ( $data->values->$key_name == "T" ) {
                $image_name =  str_replace(" ", "_", $data->values->$image_key);
                if($image_name != ''){
                  $image_name =  str_replace(".bmp", ".png", $image_name);
                  $output['shower_img_alt'] = $data->values->$image_key;
                  $output['shower_image_avail'] = 1;
                  $output['shower_image'] = file_create_url($config->get('pdp_technical_spec_image_path').$image_name );
                }
                else {
                  $output['shower_image_avail'] = 0;
                }

              }
            }
          }
        }

        // Get Related Products
        if ( isset($data->values->defaultRefCollection ) ) {
          $collection_term_id = $this->productImport->getCollectionTermByName($data->values->defaultRefCollection);
        }
        else {
          $collection_term_id = $term_id;
        }

        $relatedProducts = $this->getRelatedProducts($output['collections'], $response['product_category'], $collection_term_id, $sku);
        if ( count($relatedProducts) > 0 ) {
          $orderBy = explode(",", $config->get('pdp_related_prd_order'));
          $relatedSortOrdredList = array_merge(array_flip($orderBy), $relatedProducts);
          $output['related_prd_avail'] = 1;
          $output['related_products'] = array_filter($relatedSortOrdredList);
          $output['related_products_title'] = $config->get('related_product_title');
        }
        $api_config = $this->configFactory->get('delta_services.deltaservicesconfig');
        $bimSmithAPI =  $api_config->get('bimsmith');
        $bimsmithResponse = ['status' => FALSE ];

        if(strlen(trim($bimSmithAPI)) > 0){
          $getBaseModelNo = 0;
          // in future, this logic might come
          //if(is_object($data) && property_exists($data, 'defaultModelNumber')){
            //$getBaseModelNo = $output['defaultModelNumber'] = $data->defaultModelNumber;
          //}
          if(is_object($data->values) && property_exists($data->values, 'BaseModel')){
            $getBaseModelNo = $output['defaultModelNumber'] = $data->values->BaseModel;
          }
          $bimSmithAPI = str_replace('{bmSKU}', $getBaseModelNo, $bimSmithAPI);
          $bimsmithResponse = $this->deltaservice->getBimsmithData($bimSmithAPI);
        }
        $output['bimsmith'] = $bimsmithResponse;
       $cacheId = $domain.'_PDP_'. $sku;
        $this->deltaCache->deltaSetCache($cacheId, $output);

     } // check cache

     // Download Handler Form.
     $form = $this->formBuilder->getForm('Drupal\brizo_pdp\Form\DownloadHandler');

     // recently Viewed products
     if ( $recently_viewed_enabled == 1) {
       $recently_viewed = $this->getRecentlyViewedProductDetails($sku);
       if (count($recently_viewed) > 0) {
         $output['recently_viewed_avail'] = 1;
         $output['pdp_recently_viewed_title'] = $config->get('pdp_recently_viewed_title');
       }
      }
      $build['output'] = [
         '#theme' => 'commerce_product',
         '#data' => $output,
         '#title' => "Product",
         '#download_form' => $form,
         '#recently_viewed' => $recently_viewed,
       ];
     return $build;

   }

   /**
    * {@inheritdoc}
    */
    public function productBaseDetails($sku){

      $productId = key($this->productImport->productExists($sku));
      if(!empty($productId)) {
        $product = $this->entityTypeManager->getStorage('commerce_product')->load($productId);
        $response['product_response_data'] = $product->get("field_product_response_data")->getValue();
        $response['product_shown'] = $product->get("field_product_as_shown")->value;
        $response['product_id'] = $product->id();
        $response['ConsumerCopy'] = $product->get("body")->value;
        $response['collection_id'] = $product->get("field_collection")->target_id;
        $response['product_category'] = $product->get("field_category")->value;
        return $response;
      }

    }
    // param 1 - product as shown array
    // param 2 - sku
    // In pdp param 1 will be object
    // While update product as shown param 1 variable is array

    public function getPrdAsShown($ps_data, $sku){
      $getSkuList = [];
      if(!empty($sku)){
        $getSkuList = explode('--', $sku);
      }
      if(is_array($ps_data) && count($ps_data) > 0 && count($getSkuList) > 1 ){
        foreach ($ps_data as $key => $prdObjArr) {
          if(is_object($prdObjArr) && property_exists($prdObjArr, 'sku')){
            $getSku = $prdObjArr->sku;
            if(!in_array($getSku, $getSkuList)){
              unset($ps_data[$key]);
            }
          }
          if(is_array($prdObjArr) && array_key_exists('sku', $prdObjArr)){
            $getSku = $prdObjArr['sku'];
            if(!in_array($getSku, $getSkuList)){
              unset($ps_data[$key]);
            }
          }
        }
        $ps_data = array_values($ps_data);
      }

      foreach($ps_data as $k => $prod_shown) {
        $ps_data[$k]->caprice = (!substr_count($prod_shown->caprice,",") )?number_format($prod_shown->caprice, 2, '.', ','):$prod_shown->caprice;
        $ps_data[$k]->price = $prod_shown->price;
      }
      return $ps_data;
    }

    /**
     * {@inheritdoc}
     */
     public function getProductPrice($productId) {
       $product = $this->entityTypeManager->getStorage('commerce_product')->load($productId);
       foreach( $product->variations as $variation) {
         $prices = $variation->entity->getPrice()->getNumber();
         $currency = $variation->entity->getPrice()->getCurrencyCode();
       }
      $price =  number_format($prices,2);
      return $price;
    }
/**

     * {@inheritdoc}

     */

    public function getProductCaprice($productId) {
      $product = $this->entityTypeManager->getStorage('commerce_product')->load($productId);
      $product_response_data = $product->get('field_product_response_data')->value;
      if(!empty($product_response_data)) {
        $data = json_decode($product_response_data);
        $caprice = isset($data->values->CAListPrice)?number_format($data->values->CAListPrice, 2):'0';
      }
      return $caprice;
    }


     public function getProductTitle($productId) {
       $product = $this->entityTypeManager->getStorage('commerce_product')->load($productId);
       $title = $product->get('title')->value;
       return $title;
     }

     public function getProductHeroImage($productId) {
       $product = $this->entityTypeManager->getStorage('commerce_product')->load($productId);
       $image_url = $product->get('field_image_url')->value;
       return $image_url;
     }

     public function getProductCollectionName($productId, $sku) {
       $product = $this->entityTypeManager->getStorage('commerce_product')->load($productId);
       $collection_id = $product->get('field_collection')->target_id;
       if ( $collection_id != "" && $collection_id != 0 ) {
         $entity = $this->entityTypeManager->getStorage("taxonomy_term")->load($collection_id);
         $collection_name = $entity->getName();
       }
       else {
         $response = $this->productBaseDetails($sku);
         $data = json_decode($response['product_response_data'][0]['value']);
         $collection_name = (isset($data->values->Collection))?$data->values->Collection:$data->values->defaultCollection;
       }
       if( $collection_name == '') {
         $collection_name = 'Brizo';
       }
       return $collection_name;
     }

     public function getProductUrl($productId) {
       $alias = \Drupal::service('path_alias.manager')->getAliasByPath('/product/'.$productId);
       return $alias;
     }


     public function getProductValues($sku) {
       $response = $this->productBaseDetails($sku);
       $data = $response['product_response_data'][0]['value'];
       return json_decode($data);
     }

     public function getProductAsShown($sku) {
      $response = $this->productBaseDetails($sku);
      $product_as_shown = $response['product_shown'];
      return $product_as_shown;
     }



     public function getproductSliderImages($data){
       if (isset($data->heroImageSmall)) {
         $output['assets']['images']['url'] = $data->heroImage;
         $heroImageString = explode("/", $data->heroImage);
         $heroImageFilename = $heroImageString[count($heroImageString) - 1];
         $output['assets']['images']['heroImageFileName'] = $heroImageFilename;
       }
       if (count($data->assets) > 0) {
         foreach ($data->assets as $assets) {
           if (!empty($assets->smallUrl) && ($assets->type == 'InContextShot')) {
             $assets->mediaType = 'image';
             $output['assets']['images'][] = $assets;
           }
           if (!empty($assets->smallUrl) && ($assets->type == 'SecondaryOnWhiteShots')) {
             $assets->mediaType = 'image';
             $output['assets']['images'][] = $assets;
           }
           if (!empty($assets->url) && ($assets->type == 'Video')) {
             $assets->mediaType = 'video';
             $output['assets']['images'][] = $assets;
           }
           if (!empty($assets->url) && ($assets->type == 'CAD')) {
             $cadFilename = $assets->filename;
             $cadArray = explode(".", $cadFilename);
             $cadFileExtension = end($cadArray);
             $cadAssetDetails[] = [
               'cadFileExtension' => '.' . strtoupper($cadFileExtension),
               'filename' => $cadFilename,
               'fileUrl' => $assets->url,
             ];
             $output['assets']['cadAssets'] = $cadAssetDetails;
           }
         }
       }
      return $output;
     }

     public function isModuleEnabled($module_name){
       $config = $this->configFactory->get('brizo_pdp.settings');
       $isEnabled = $config->get($module_name);
       return $isEnabled;
     }


     /**
      * Create breadcrumb based on the referer
      */
     public function create_breadcrumb($category = '', $series = '') {
       $output = $component = [];
       $link = '';
       $front_url = Url::fromRoute('<front>');
       $home_path = $front_url->toString();

       $current_url = Url::fromRoute('<current>')->toString();
       $url_segment = explode("/", $current_url);
       if( $series != 404 ) {
         unset($url_segment[count($url_segment)-1]);

       }

       $loop = 1;
       $loop_count = count($url_segment);
       foreach($url_segment as $segment) {
         if( $loop == 1 ) {
           $label = 'Home';
           $front_url = Url::fromRoute('<front>');
           $link = $front_url->toString();
         }
         elseif ( $loop == $loop_count) {
           $label = ucwords(str_replace("-", ' ', $segment));
           $link = '';
         }
         else {
          $label = ucwords(str_replace("-", ' ', $segment));
          $link =  '/' . $segment;
         }
         $component['text'] = $label;
         $component['url'] = $link;
         $output[] = $component;
         $loop++;
       }
       return $output;
     }


     /**
      * Get the facet feature values.
      */
     public function getProductFeature($data) {
       $faturesBenefits = [];
       $featuresBenefitsResult = [];
       foreach ($data->values as $key => $value) {
         if (preg_match('/^BrandBaseProdBullet/', $key, $matches)) {
           $bulletkeys = preg_split('/^BrandBaseProdBullet/', $key);
           $featuresBenefitsResult[$bulletkeys[1]] = $value;
         }
         ksort($featuresBenefitsResult);
       }
       return $featuresBenefitsResult;
     }
     /**
      * Get Design Files
      */

      public function getDesignFiles($assets, $file_types) {

        $design_files = [];
        if( count($assets) > 0) {

          foreach ( $assets as $asset) {
              if( $asset->type == 'CAD') {
                $file_title = $this->getFileExtensionByName($asset->filename, $file_types);
                $design_files[] = ['title' => $file_title , 'file_url' => $asset->url, 'file_name' => $asset->filename];

              }
          }
        }
        array_multisort(array_column($design_files, 'title'), SORT_ASC, $design_files);

        return $design_files;
      }

      /**
       * Get File Extension
       */
      public function getFileExtensionByName($file_name, $file_types = []) {
        $file_extension_name = '';
         if ($file_name != '') {
            $extension = explode('.', $file_name )[1];
            if ( count($file_types) > 0){
              $file_extension_name = $file_types[strtoupper($extension)];
              if ( $file_extension_name == '') {
                $file_extension_name = strtoupper($extension) ." FILE " . "(." . strtoupper($extension) . ")";
              }
            }
            else {
              if ( $file_extension_name == '') {
                $file_extension_name = strtoupper($extension) ." FILE " . "(." . strtoupper($extension) . ")";
              }
            }
         }
        return $file_extension_name;


      }


      /**
       * Get File Extension
       */
      public function getFileExtension() {
        $file_extension_name = '';
        $extension_array = [];
        $config = $this->configFactory->get('brizo_pdp.settings');
        $file_extensions_list  = $config->get('pdp_design_files');
                if( $file_extensions_list != "") {
          $file_extensions_list = array_filter(explode(PHP_EOL,$file_extensions_list));
          foreach( $file_extensions_list as $extension_list) {
            $extension = explode("|", $extension_list);
            $extension_array[trim($extension[0])] =  trim($extension[1]);
          }
        }
        return $extension_array;
      }


      /**
       * Get Technical document
       */
      public function getTechnicalDocuments($assets, $tecnical_document_type) {

        $technical_documents = [];
        $technical_documents_list = [];
        if( count($assets) > 0) {
          foreach ( $assets as $asset) {
              if( array_key_exists($asset->type,$tecnical_document_type)) {
                $technical_documents[$asset->type] = ['title' => $tecnical_document_type[$asset->type] , 'file_url' => $asset->url, 'file_name' => $asset->filename];
              }
          }
          $order_by = array_keys($tecnical_document_type);
          if( count($technical_documents) > 0) {
            $technical_documents_list = array_filter(array_merge(array_flip($order_by), $technical_documents ));

          }
        }

        return $technical_documents_list;
      }

      /**
       * Get Technical file type
       */

      public function getTechicalFileType() {
        $file_extension_name = '';
        $extension_array = [];
        $config = $this->configFactory->get('brizo_pdp.settings');
        $file_extensions_list  = $config->get('pdp_technical_files');
        if( $file_extensions_list != "") {
          $file_extensions_list = array_filter(explode(PHP_EOL,$file_extensions_list));
          foreach( $file_extensions_list as $extension_list) {
            $extension = explode("|", $extension_list );
            $extension_array[trim($extension[0])] =  trim($extension[1]);
          }
        }
        return $extension_array;
      }

      /**
       * Get Finish option list
       */

      public function getFinishOptionsList($finsihOptions, $category, $current_sku) {
        $config = $this->configFactory->get('brizo_pdp.settings');
        $urlPattern  = $config->get('pdp_url_patterns');
        $parts_url_pattern = $config->get('pdp_parts_url_patterns');
        $finish_stocking_type = $config->get('finish_stocking_type');
        $product_finsih['finishOptions'] = [];
        $finishstocking = explode(",", $finish_stocking_type);
        $now_n = time();
        $unix_time = $now_n * 1000;
        $i=0;
        foreach ($finsihOptions as $finish) {
          if( strlen($finish->availableToOrderDate) > 10 ){
              $now = $unix_time;
          }
          else {
            $now = $now_n;
          }
          /* Check the product modal */
          $productId = key($this->productImport->productExists($finish->modelNumber));
          if(!empty($productId)) {
            $product_values = $this->getProductValues($finish->modelNumber);
           }
          else {
            $data = $this->productImport->isValidProductSKU($finish->modelNumber);
            if($data['status'] == 1){
              $config = $this->configFactory->get('delta_services.deltaservicesconfig');
              $siteName = $config->get('global_site_name');
              $productId = $this->productImport->createProduct($data['productData'],$siteName);
              if (!empty($productId)) {
                $product_values = $this->getProductValues($finish->modelNumber);
              }
            }
          } // Product id
          $getFinishSkuData = $this->globalproduct->getFoundationalOrderShipAttr($product_values);
          if ($getFinishSkuData == TRUE){
            if (!in_array($finish->stockingType, $finishstocking) && ($now > $finish->availableToOrderDate)) {
               if( $category != 'parts') {
                 $path = str_replace('{CATEGORY}', $category, $urlPattern);
                 $finish->productUrl = str_replace('{sku}', $finish->modelNumber, $path);
               }
               else {
                 $finish->productUrl = str_replace('{sku}', $finish->modelNumber, $parts_url_pattern);
               }
               $finish->productIcon = get_taxonomy_using_name($finish->facetFinish, 'facetfinish', 'icon');

               if ($finish->modelNumber == $current_sku) {
                 $finish->active_class = "active";

               }
               else {
                 $finish->active_class = "in-active";
               }
                if((strpos($current_sku , '--') !== false)
                  &&(strpos($current_sku , $finish->modelNumber) !== false) ){
                  $finish->active_class = "active";
                }
               // if ($finish->modelNumber == $current_sku) {
               //   $product_finsih['finishOptions'][0] = $finish;
               // }
               // else {
               //   $product_finsih['finishOptions'][$i] = $finish;
               //   $i++;
               // }
               $product_finsih['finishOptions'][$i] = $finish;
               $i++;
             }
          }

      }
      return $product_finsih['finishOptions'];
    }

    /**
     * Get Collection pdf
     */

    public function getCollectionPDF($term_id, $category) {

      $term_obj = $this->entityTypeManager->getStorage('taxonomy_term')->load($term_id);
      $file_url = "";
      if ( $category == 'bath') {
        if(isset($term_obj->get('field_bath_collection_pdf')->entity)){
            $file_url = file_create_url($term_obj->get('field_bath_collection_pdf')->entity->getFileUri());
        }
      }
      else if ($category == 'kitchen') {
        if(isset($term_obj->get('field_kitchen_collection_pdf')->entity)){
            $file_url = file_create_url($term_obj->get('field_kitchen_collection_pdf')->entity->getFileUri());
        }
     }
     return $file_url;
    }

    /**
     * Get Technical specification
     */

    public function getTechnicalSpecification($assets, $type) {

      $tech_files = [];
      if( count($assets) > 0) {
        foreach ( $assets as $asset) {
            if( $asset->type == $type) {
              $file_title = $this->getFileExtensionByName($asset->filename, []);
              $tech_files[] = ['file_url' => $asset->url, 'file_name' => $asset->filename];

            }
        }
      }
      return $tech_files;
    }


    /**
     * Get Related products
     */

    public function getRelatedProducts($collection_name, $category, $term_id, $sku) {
      $relatedProducts = [];
      if (  $term_id != "") { // check the collection name is exist.
        // set catch id
        $terms = $this->entityTypeManager->getStorage('taxonomy_term')->load($term_id);
        if( is_object($terms) ) {
          $term_name = $terms->getName();
          $collection_name = $term_name; // assigning value to get matching tern name collection
        }
        $collection_machine_name = strip_tags($collection_name);
        $collection_machine_name = str_replace(" ", "_", $collection_machine_name);
        $domain = '';
        if (\Drupal::service('module_handler')->moduleExists('domain')) {
          $loader = \Drupal::service('domain.negotiator');
          $current_domain = $loader->getActiveDomain();
          if ($current_domain) {
            $domain = $current_domain->get('name') . '_';
          }
        }
        $catchID = $domain . 'RELATED_PRD_' . $collection_name . '_' . $category;
        $pdp_config = $this->configFactory->get('brizo_pdp.settings');
        $size = $pdp_config->get('pdp_related_page_size');
        $toltal = $pdp_config->get('pdp_related_page_max_size');
        $data = $this->deltaCache->deltaGetCache($catchID);
        $page = 0;
        $apiResponse = [];
        if(!empty($data)) {
            $relatedProducts = $data;
        }
        else {
          $category = ucfirst($category);
          $config = $this->configFactory->get('delta_services.deltaservicesconfig');
          $basePath  = $config->get('api_base_path');
          $endpoint = $config->get('pdp_related_product_api_url');
          $collection_name = ucwords(strtolower($collection_name));
          $collection_name = str_replace('For', 'for', $collection_name );
          $collection_name = strip_tags($collection_name);
          $collection_name = str_replace('&reg;', '®', $collection_name);
          $collection_name = str_replace('&trade;', '™', $collection_name);
          $collection_name = rawurlencode($collection_name);
          $enpointUrl = $basePath . $endpoint;
          $enpointUrl = str_replace("{COLLECTION_NAME}", $collection_name, $enpointUrl);
          $enpointUrl = str_replace("{CATEGORY}", "Brizo_$category", $enpointUrl);
          $enpointUrl = str_replace("{SIZE}", $size, $enpointUrl);
          $enpointAPIUrl = str_replace("{PAGE}", "page=0", $enpointUrl);
          $response = $this->deltaservice->apiCall( $enpointAPIUrl, 'GET', [], [], "RELATED_PRODUCTS");
          $apiResponses[$page] = json_decode($response['response']);
          if ($response['status'] == 'SUCCESS') {
            // check the total no of elements
            $decoded_data = json_decode($response['response']);
            if ($decoded_data->totalElements > $size ) {
              for ($page = 1; $page < 3; $page++ ) {
                $enpointAPIUrl = str_replace("{PAGE}", "page=".$page, $enpointUrl);
                $response = $this->deltaservice->apiCall( $enpointAPIUrl, 'GET', [], [], "RELATED_PRODUCTS");
                $apiResponses[$page] = json_decode($response['response']);
              }
            }
            foreach ($apiResponses as $decoded_data) {
              if ($decoded_data->totalElements > 0) {
              foreach($decoded_data->content as $product) {
                if ( $product->name != $sku) {
                  $prdCategory = "";
                  $categories = $product->categories;
                  $basecategory = [];
                  $maincategoryfinal = "";
                  $maincategoryurl = "";
                  foreach ($categories as $urlalias) {
                    $urlalias_seperate = explode("_", $urlalias);
                    $sitefromcategory[] = $urlalias_seperate[0];
                    if ($urlalias_seperate[0] == "Brizo") {
                      $prdCategory = str_replace(" ", "_", $urlalias_seperate[2]);
                      $category_title = $urlalias_seperate[2];
                      $base_category[$prdCategory] = ['title' => $urlalias_seperate[2]];
                      $basecategory[] = strtolower($urlalias_seperate[1]);
                    }
                  }
                  if(!empty($sitefromcategory) && in_array("Brizo", $sitefromcategory)){
                    $basecategoryfinal = array_unique($basecategory);
                    if (in_array('kitchen', $basecategoryfinal)) {
                      $maincategoryfinal = "kitchen";
                      $maincategoryurl = "kitchen/product/";
                    }
                    elseif (in_array('bath', $basecategoryfinal)) {
                      $maincategoryfinal =  "bath";
                      $maincategoryurl = "bath/product/";
                    }
                    elseif (in_array('parts', $basecategoryfinal)) {
                      $maincategoryfinal =  "parts";
                      $maincategoryurl = "bath/product/";
                    }
                  }
                  if( count($categories) == 0 ){
                    $maincategoryfinal =  "kitchen";
                    $maincategoryurl = "kitchen/product/";
                  }
                  $mainCategory = $maincategoryfinal;
                  $config = $this->configFactory->get('brizo_pdp.settings');
                  $urlPattern  = $config->get('pdp_url_patterns');
                  $parts_url_pattern = $config->get('pdp_parts_url_patterns');

                  if( $maincategoryfinal != 'parts') {
                    $path = str_replace('{CATEGORY}', $mainCategory, $urlPattern);
                    $path = str_replace('{sku}', $product->name, $path);
                  }
                  else {
                    $path = str_replace('{sku}', $product->name, $parts_url_pattern);
                  }
                  if ($prdCategory != "" ) {
                    $relatedProducts[$prdCategory]['title'] = $category_title;
                    $relatedProducts[$prdCategory]['products'][] = [
                      'title' => $product->description,
                      'heroImageSmall' => $this->catch_fetch_remote_image($product->heroImageSmall),
                      'path' => $path,
                     ];
                  }
                }
              }//foreach
              } // if total count
            }
          }  // if success
          $this->deltaCache->deltaSetCache($catchID, $relatedProducts);
        } // else - cache

      } // check term id
      return $relatedProducts;
    }


    public function getTechnicalAttributes($data_values, $technicaAttribute ) {
      $attributes_array = [];
      $tech_spec_attribute = [];
      if( $technicaAttribute != "") {
        $tech_attribute_list = array_filter(explode(PHP_EOL, $technicaAttribute));
        foreach( $tech_attribute_list as $attribute_list) {
          $attributes = explode("|", $attribute_list );
          $attributes_array[trim($attributes[0])] =  [ "value" => trim($attributes[1]), "unit" => isset($attributes[2])?$attributes[2]:'' ];
        }
      }
      if( count($attributes_array) > 0) {
        foreach ($attributes_array as $key => $tech_attributes) {

          if ( array_key_exists($key,$data_values ) ) {

              $tech_spec_attribute[] = ["key" => $key, "label" => $tech_attributes['value'], "values" => $data_values->$key . $tech_attributes['unit'] ];
          }

        }
      }
      return $tech_spec_attribute;
    }

    /**
     * Get handle accent items
     */

    public function getProductConfigOptions($configOptions, $assets) {
      $configOptionData = [];
      if (!empty($configOptions)){
        $config = $this->configFactory->get('brizo_pdp.settings');
        $handle_accent_key  = $config->get('pdp_handle_accent_param');
        $rough_valve_key = $config->get('pdp_rough_valve_param');
        $riser_union = $config->get('pdp_riser_union_param');
        $body_assembly = $config->get('pdp_body_assembly_param');
        $cold_handle = $config->get('pdp_cold_handle_param');
        $configOptionLabels = [ $handle_accent_key, $rough_valve_key, $riser_union, $body_assembly, $cold_handle ];
        $config_assets = $this->getProductConfigureImg($optionProducts->modelNumber, $assets);
        foreach( $configOptions as $configData) {

          if (in_array($configData->optionLabel, $configOptionLabels)) {
            foreach ($configData->optionProducts as $optionProducts) {  // get sub items
              $configOptionsProducts = [];
              if (count($optionProducts->configurationOptions) > 0) {
                  foreach ($optionProducts->configurationOptions as $configurationOptions ) {
                    if (in_array($configurationOptions->optionLabel, $configOptionLabels)) {
                      foreach($configurationOptions->optionProducts as $subOptionProducts) {
                        $title = $subOptionProducts->description;
                        $title = str_replace('®' , '<sup>®</sup>', $title);
                        $title = str_replace('™' , '<sup>™</sup>', $title);
                        $configOptionsProducts[$configurationOptions->optionLabel][] = [
                          'imageUrl' => $subOptionProducts->image,
                          'title' => $title,
                          'modalNo' => $subOptionProducts->modelNumber,
                          'price' => number_format($subOptionProducts->listPrice, 2),
                          'caprice' => isset($subOptionProducts->calistPrice)?number_format($subOptionProducts->calistPrice, 2):0,
                        ];
                      }
                    }
                  }
              }
              if( count($configOptionsProducts) > 0 ) {
                array_multisort(array_column($configOptionsProducts[$configurationOptions->optionLabel], 'price'), SORT_ASC, $configOptionsProducts[$configurationOptions->optionLabel]);
              }

              $sub_title = $optionProducts->description;
              $sub_title = str_replace('®' , '<sup>®</sup>', $sub_title);
              $sub_title = str_replace('™' , '<sup>™</sup>', $sub_title);

              $configOptionData[$configData->optionLabel][] = [
                'imageUrl' => $optionProducts->image,
                'title' => $sub_title,
                'modalNo' => $optionProducts->modelNumber,
                'configOptionsProducts' => $configOptionsProducts,
                'price' => number_format($optionProducts->listPrice, 2),
                'caprice' => isset($optionProducts->calistPrice)?number_format($optionProducts->calistPrice, 2):0,
                'productImageurl' => isset($config_assets[$optionProducts->modelNumber])?$config_assets[$optionProducts->modelNumber]['mdImageUrl']: '',
                'productThumbImageurl' => isset($config_assets[$optionProducts->modelNumber])?$config_assets[$optionProducts->modelNumber]['smImageUrl']: '',
              ];
            }
          }
          array_multisort(array_column($configOptionData[$configData->optionLabel], 'price'), SORT_ASC, $configOptionData[$configData->optionLabel]);
          array_multisort(array_column($configOptionData[$configData->optionLabel], 'modalNo'), SORT_ASC, $configOptionData[$configData->optionLabel]);

        } // foreach config values
      }
      return $configOptionData;
    }

    // catch in local storage image

    public function catch_fetch_remote_image($url) {

      if (!$url) {
        return FALSE;//fail
      }

      $file_name_split = explode("/" , $url);
      $filename = $file_name_split[count($file_name_split)-1];

      $directory_path = 'public://products/';
      $file_path = file_create_url($directory_path. "/sm_" . $filename);
      /*if (file_exists($file_path)) {
        unlink($file_internal_path);
      }*/
      $file_path_image = $directory_path. "/sm_" . $filename;
      $files = \Drupal::entityTypeManager()->getStorage('file')->loadByProperties(['uri' => $file_path_image]);
      if (count($files)) {
        foreach ($files as $item) {
          $fid = $item->id();
          $file = File::load($fid);
          if ($file) {
            $file_usage = \Drupal::service('file.usage');
            $usage = $file_usage->listUsage($file);
            if(count($usage) == 0) {
              $file->delete();
            }
          }
        }
      }
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_URL, $url);
      $data = curl_exec($ch);
      curl_close($ch);
      /**
      * If data returned save to mls_images folder and return filename
      */
      if ($data) {
        if ( \Drupal::service('file_system')
    ->prepareDirectory($directory_path, FileSystemInterface::CREATE_DIRECTORY)) {
          if (file_save_data($data, $directory_path ."/sm_". $filename  , TRUE))//replace
            return $file_path;
          }
        }

    }

    public function getProductConfigureImg($modelNo, $assets) {
      $src = [];
      foreach ($assets as $images) {
        if ($images->type == 'ConfiguredOnWhite') {
          $ids = explode("_", $images->filename);
          array_pop($ids);
          foreach ($ids as $id) {
            $src[$id] = ['mdImageUrl' => $images->url, 'smImageUrl' => $images->smallUrl ];
          }
        }
      }
      return $src;
    }

    /**
     * Get product as shown from config data
     */

    public function getProductShownConfigData($product_as_shown, $configOptions, $product_name, $default = 0) {
      $config = $this->configFactory->get('brizo_pdp.settings');
      $filters = $config->get('pdp_config_options_filter');
      if( !empty($filters)) {
        $filters_key = explode(",", $filters);
        foreach ($configOptions as $configOption) {
          if (!in_array($configOption->optionLabel, $filters_key)) {
            $parent_config_counter = 0;
            foreach($configOption->optionProducts as $optionProducts){
              if( strpos($product_name, $optionProducts->modelNumber) !== FALSE){
                $product_as_shown[] = [
                  'title'=> $optionProducts->description,
                  'sku'=> $optionProducts->modelNumber,
                  'price' =>  number_format($optionProducts->listPrice, 2),
                  'caprice' => isset($optionProducts->calistPrice)?number_format($optionProducts->calistPrice, 2):0.00,
                  'optionLabel' => $configOption->optionLabel
                ];
              }
              if ( $default == 1) { //get first config data as default
                if($parent_config_counter == 0) {
                  $caprice = 0.00;
                  if(isset($optionProducts->CAlistPrice)) {
                    $caprice =  (!substr_count($optionProducts->calistPrice,",") )?number_format($optionProducts->calistPrice, 2, '.', ','):$optionProducts->calistPrice;
                  }
                  $product_as_shown[] = [
                  'title'=> $optionProducts->description,
                  'sku'=> $optionProducts->modelNumber,
                  'price' => number_format($optionProducts->listPrice, 2),
                  'caprice' => $caprice,
                  'optionLabel' => $configOption->optionLabel];
                  $parent_config_counter++;
                }

              }
              if(count($optionProducts->configurationOptions)> 0 ) {
                foreach($optionProducts->configurationOptions as $sub_items){
                  if (!in_array($sub_items->optionLabel, $filters_key)) {
                    $child_config_counter = 0;
                    foreach($sub_items->optionProducts as $sub_options) {
                      if( strpos($product_name, $sub_options->modelNumber) !== FALSE){
                        $caprice = 0.00;
                        if(isset($sub_options->CAlistPrice)) {
                          $caprice =  (!substr_count($sub_options->calistPrice,",") )?number_format($sub_options->calistPrice, 2, '.', ','):$sub_options->calistPrice;
                        }
                        $product_as_shown[] = [
                          'title'=> $sub_options->description,
                          'sku'=> $sub_options->modelNumber,
                          'price' => number_format($sub_options->listPrice,2),
                          'caprice' => $caprice,
                          'optionLabel' => "prd-itms " . $sub_items->optionLabel
                        ];
                      }
                      // else { //get first config data as default
                      //   if($child_config_counter == 0) {
                      //     $product_as_shown[] = ['title'=> ucwords(strtolower($sub_options->description)), 'sku'=> $sub_options->modelNumber, 'price' => number_format($sub_options->listPrice, 2), 'optionLabel' => "prd-itms " . $sub_items->optionLabel];
                      //     $child_config_counter++;
                      //   }
                      // }
                    }
                  }
                }
              }
            }
          }
        }
      }
      $product_as_shown = $this->getPrdAsShown($product_as_shown, $product_name );
      return $product_as_shown;
    }


    /*
     ** set cookies
     */

    public function setRecentlyViewedProducts($sku, $existing_sku) {
      $config = $this->configFactory->get('brizo_pdp.settings');
      $cookie_name = $config->get('pdp_cookie_key');
      $max_count = $config->get('pdp_cookie_max_count') + 1;
      $product_sku = "";
      $cookie_name = str_replace(" ", "-", $cookie_name);
       // set productc ids.
      $product_array = explode("!" , $existing_sku);

      if( count($product_array) > 0 ) {
        if (!in_array($sku, $product_array)) {
            if ( $existing_sku != "" ) {
              $product_sku =  $sku . "!" . $existing_sku;
            }
            else {
              $product_sku = $sku;
            }
        }
        else {
          if (($key = array_search($sku, $product_array)) !== false) {
            unset($product_array[$key]);
          }
            $product_sku = $sku ."!" . implode("!", $product_array );
        }
      }
      else {
        $product_sku = $sku;
      }
      $tempstore = $this->tempStoreFactory->get($cookie_name);
      $tempstore->set('sku', $product_sku);

      // check cookie
    }


    public function getRecentlyViewedProducts() {
      $config = $this->configFactory->get('brizo_pdp.settings');
      $cookie_name = $config->get('pdp_cookie_key');
      $sku_values = $this->tempStoreFactory->get($cookie_name);
      $sku = $sku_values->get('sku');
      return $sku;
    }

    public function getRecentlyViewedProductDetails($sku) {

      $config = $this->configFactory->get('brizo_pdp.settings');
      $max_count = $config->get('pdp_cookie_max_count')+1;
      $sku_list = $this->getRecentlyViewedProducts();
      $viewed_products = [];
      if( !empty($sku_list)) {
        /* To check max limit */
        $product_sku_array = explode("!", $sku_list);
        if ( (count($product_sku_array) > $max_count) && $max_count != 0) {
          $diff = $max_count - count($product_sku_array);
          array_splice($product_sku_array, $diff);
          $sku_list = implode( "!", $product_sku_array);
        }
        $product_array = explode("!" , $sku_list);
        foreach( $product_array as $product_sku) {
          if( $product_sku != $sku ) {
            $productId = key($this->productImport->productExists($product_sku));
            if( !empty($productId)) {
              $title = $this->getProductTitle($productId);
              $title = str_replace('®' , '<sup>®</sup>', $title);
              $title = str_replace('™' , '<sup>™</sup>', $title);
              $alias = \Drupal::service('path_alias.manager')->getAliasByPath('/product/'.$productId);
              $product_as_shown = json_decode($this->getProductAsShown($product_sku));
              $collection_name  = $this->getProductCollectionName($productId, $product_sku);
              $collection_name = str_replace('®' , '<sup>®</sup>', $collection_name);
              $collection_name = str_replace('™' , '<sup>™</sup>', $collection_name);
              if($product_as_shown == '') {
                $product_as_shown = $title;
              }
                $product_as_shown = str_replace('®' , '<sup>®</sup>', $product_as_shown);
                $product_as_shown = str_replace('™' , '<sup>™</sup>', $product_as_shown);

              $viewed_products[] = [
                'title' => $title,
                'product_as_shown' => $product_as_shown,
                'heroImageSmall' => $this->getProductHeroImage($productId),
                'path' => $this->getProductUrl($productId),
                'collection_name' => $collection_name,
                'modelNo' => $product_sku,
              ];
            }
          }
        }

      }
      return $viewed_products;
    }

    public function redirectNoProductFound($sku) {

      $pdp_config = $this->configFactory->get('brizo_pdp.settings');
      $recently_viewed_enabled = $pdp_config->get('recently_viewd_products');
      $page_404_content = $pdp_config->get('pdp_404_body_text.value');
      $page_404_content = str_replace('{PRD_SKU}', $sku, $page_404_content);
      $page_content = $page_404_content;
      $category = '';
      $breadcrumb = $this->create_breadcrumb($category);
      $product_type = '';
      $current_url =  Url::fromRoute('<current>', [], ['query' => \Drupal::request()->query->all(), 'absolute' => 'true'])->toString();
      if ( $current_url != '' ) {
        $url_segment = explode("/", $current_url);
        if ( count($url_segment)  > 2 ) {
          $product_type = $url_segment[count($url_segment) - 2];
          $product_type = str_replace('-', ' ', $product_type);
          $product_type = ucfirst($product_type);
        }
      }
      if ( $recently_viewed_enabled == 1) {
        $recently_viewed_products = $this->getRecentlyViewedProductDetails($sku);
        if (count($recently_viewed_products) > 0) {
          $recently_viewed_avail = 1;
          $pdp_recently_viewed_title = $pdp_config->get('pdp_recently_viewed_title');
        }
        $recently_viewed = [ 'recently_viewed_avail' => $recently_viewed_avail,
            'pdp_recently_viewed_title' => $pdp_recently_viewed_title,
            'recently_viewed_products' => $recently_viewed_products
          ];
      }

      return $build['output'] = [
        '#no-data-content' => $page_content,
        '#title' => $product_type,
        '#recently_viewed' => $recently_viewed,
        '#breadcrumb' => $breadcrumb,
      ];

    }
}

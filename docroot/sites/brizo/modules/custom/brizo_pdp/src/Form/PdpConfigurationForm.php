<?php
namespace Drupal\brizo_pdp\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines a form that pdp configures forms module settings.
 */

 class PdpConfigurationForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */

  public function getFormId() {
  return 'brizo_pdp';
  }

  /**
   * {@inheritdoc}
   */

   protected function getEditableConfigNames() {
    return [
      'brizo_pdp.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */

   public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('brizo_pdp.settings');

    $form['pdp_module_details'] = array(
      '#type' => 'details',
      '#title' => $this->t('PDP Modules'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#description' => $this->t('Enable the module to be used in PDP'),
    );

    $form['pdp_module_details']['related_products'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Related Products'),
      '#title' => $this->t('Related Product API URL'),
      '#default_value' => $config->get('related_products'),
    );

    $form['pdp_module_details']['recently_viewd_products'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Recently Viewed Products'),
      '#default_value' => $config->get('recently_viewd_products'),
    );

    $form['pdp_module_details']['download_image'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Download Image'),
      '#default_value' => $config->get('download_image'),
    );

    $form['pdp_module_details']['image_slider'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Image Slider'),
      '#default_value' => $config->get('image_slider'),
    );

    $form['pdp_module_details']['share_option'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Share +'),
      '#default_value' => $config->get('share_option'),

    );

    $form['url_patterns'] = array(
      '#type' => 'details',
      '#title' => $this->t('URL Pattern'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#description' => $this->t('Enter the url patterns'),
    );

    $form['url_patterns']['pdp_url_patterns'] = [
     '#type' => 'textfield',
     '#title' => $this->t('PDP URL'),
     '#default_value' => $config->get('pdp_url_patterns'),
    ];

    $form['url_patterns']['pdp_collection_url_patterns'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Collection details page url pattern'),
      '#default_value' => $config->get('pdp_collection_url_patterns'),
    ];

    $form['url_patterns']['pdp_parts_url_patterns'] = [
       '#type' => 'textfield',
       '#title' => $this->t('Parts details page url pattern'),
       '#default_value' => $config->get('pdp_parts_url_patterns'),
     ];

    $form['pdp_category'] = array(
      '#type' => 'details',
      '#title' => $this->t('Category'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#description' => $this->t('Enter the url patterns'),
    );

    $form['pdp_category']['product_category'] = array(
    '#type'       =>  'select',
    '#title'      =>  'Product main category',
    '#options'      =>  array( "kitchen" => $this->t("Kitchen"), "bath" => $this->t("Bathroom")),
    '#default_value'  => $config->get('product_category'),
    '#multiple'     =>  true,
    '#required'     =>  true,
    '#size'       =>  4
    );

    $form['pdp_file_extension'] = array(
    '#type' => 'details',
    '#title' => $this->t('File extension and Filters'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#description' => $this->t('Enter the file extensions'),
    );

    $form['pdp_file_extension']['pdp_design_files'] = array(
    '#type' => 'textarea',
    '#title' => $this->t('File extension'),
    '#default_value' => $config->get('pdp_design_files'),
    '#description' => $this->t('Enter the file extensions label and key seperate with | symbol'),
    );

    $form['pdp_file_extension']['pdp_technical_files'] = array(
     '#type' => 'textarea',
     '#title' => $this->t('Technical File type'),
     '#default_value' => $config->get('pdp_technical_files'),
     '#description' => $this->t('Enter the Technical file type label and key seperate with | symbol'),
    );

    $form['pdp_file_extension']['pdp_technical_specification'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Technical specification type'),
      '#default_value' => $config->get('pdp_technical_specification'),
      '#description' => $this->t('Enter the asset type name to filter the technical specification. ex : SpecDrawing'),
    );

    $form['pdp_file_extension']['pdp_technical_spec_attributes'] = array(
       '#type' => 'textarea',
       '#title' => $this->t('Technical specification attributes'),
       '#default_value' => $config->get('pdp_technical_spec_attributes'),
       '#description' => $this->t('Enter the technical specification attributes name. Ex: Height Deck to Aerator|PBSpoutHeightDecktoAerator|"'),
     );

    $form['pdp_file_extension']['pdp_technical_spec_image_param'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Technical Spec Waterflow Icon Param'),
      '#default_value' => $config->get('pdp_technical_spec_image_param'),
      '#description' => $this->t('Enter the technical spec icon attributes name. Ex:WatersenseCert|WatersenseLogo'),
    );

    $form['pdp_file_extension']['pdp_technical_spec_image_path'] = array(
     '#type' => 'textfield',
     '#title' => $this->t('Watersense image path'),
     "#attributes"  => ["readonly" => 'readonly'],
     '#default_value' => drupal_get_path('module', 'brizo_pdp'). "/watersense_icons/",
     '#description' => $this->t('Enter watersense image path'),
    );

    $form['pdp_file_extension']['pdp_handle_accent_param'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Handle & Accent Param'),
      '#default_value' => $config->get('pdp_handle_accent_param'),
      '#description' => $this->t('Enter the handle and accent spec icon attributes name. Ex:Handle_or_Accent'),
    );

    $form['pdp_file_extension']['pdp_body_assembly_param'] = array(
       '#type' => 'textfield',
       '#title' => $this->t('Body Assembly Param'),
       '#default_value' => $config->get('pdp_body_assembly_param'),
       '#description' => $this->t('Enter the body assembly attributes name. Ex:Body_Assembly'),
    );

    $form['pdp_file_extension']['pdp_riser_union_param'] = array(
       '#type' => 'textfield',
       '#title' => $this->t('Riser Union Param'),
       '#default_value' => $config->get('pdp_riser_union_param'),
       '#description' => $this->t('Enter the riser union attributes name. Ex:Riser_Union'),
    );

    $form['pdp_file_extension']['pdp_rough_valve_param'] = array(
       '#type' => 'textfield',
       '#title' => $this->t('Rough valve Param'),
       '#default_value' => $config->get('pdp_rough_valve_param'),
       '#description' => $this->t('Enter the rought valve attributes name. Ex:Rough_Valve'),
    );
    $form['pdp_file_extension']['pdp_cold_handle_param'] = array(
       '#type' => 'textfield',
       '#title' => $this->t('Cold Handle Param'),
       '#default_value' => $config->get('pdp_cold_handle_param'),
       '#description' => $this->t('Enter the cold handle attribute name. Ex:Cold_Handle'),
    );

    $form['pdp_labels'] = array(
      '#type' => 'details',
      '#title' => $this->t('Title and Labels'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#description' => $this->t('Enter the Title/Lable for the PDP sections'),
    );

    $form['pdp_labels']['cad_design_title'] = [
       '#type' => 'textfield',
       '#title' => $this->t('CAD/DESIGN FILES'),
       '#default_value' => $config->get('cad_design_title'),
     ];
    $form['pdp_labels']['accessories_title'] = [
        '#type' => 'textfield',
        '#title' => $this->t('ACCESSORIES'),
        '#default_value' => $config->get('accessories_title'),
      ];
    $form['pdp_labels']['technical_design_title'] = [
         '#type' => 'textfield',
         '#title' => $this->t('TECHNICAL DOCUMENTS'),
         '#default_value' => $config->get('technical_design_title'),
       ];
    $form['pdp_labels']['technical_specific_title'] = [
          '#type' => 'textfield',
          '#title' => $this->t('TECHNICAL SPECIFICATIONS'),
          '#default_value' => $config->get('technical_specific_title'),
        ];
    $form['pdp_labels']['features_title'] = [
           '#type' => 'textfield',
           '#title' => $this->t('FEATURES'),
           '#default_value' => $config->get('features_title'),
         ];

    $form['pdp_labels']['related_product_title'] = [
          '#type' => 'textfield',
          '#title' => $this->t('RELATED PRODUCTS'),
          '#default_value' => $config->get('related_product_title'),
        ];
    $form['pdp_labels']['pdp_recently_viewed_title'] = [
          '#type' => 'textfield',
          '#title' => $this->t('RECENTLY VIEWED PRODUCTS'),
          '#default_value' => $config->get('pdp_recently_viewed_title'),
        ];
    $form['pdp_labels']['handle_accent_title'] = [
          '#type' => 'textfield',
          '#title' => $this->t('HANDLE OR ACCENT'),
          '#default_value' => $config->get('handle_accent_title'),
        ];
    $form['pdp_labels']['rought_value_title'] = [
          '#type' => 'textfield',
          '#title' => $this->t('ROUGH VALVE'),
          '#default_value' => $config->get('rought_value_title'),
        ];
    $form['pdp_labels']['body_assembly_title'] = [
          '#type' => 'textfield',
          '#title' => $this->t('BODY ASSEMBLY'),
          '#default_value' => $config->get('body_assembly_title'),
        ];
    $form['pdp_labels']['riser_union_title'] = [
          '#type' => 'textfield',
          '#title' => $this->t('RISER / UNION'),
          '#default_value' => $config->get('riser_union_title'),
        ];
    $form['pdp_labels']['finish_label'] = [
          '#type' => 'textfield',
          '#title' => $this->t('Finish Option'),
          '#default_value' => $config->get('finish_label'),
        ];
    $form['pdp_labels']['download_file'] = [
          '#type' => 'textfield',
          '#title' => $this->t('DOWNLOAD'),
          '#default_value' => $config->get('download_file'),
        ];
    $form['pdp_labels']['view_file'] = [
          '#type' => 'textfield',
          '#title' => $this->t('View'),
          '#default_value' => $config->get('view_file'),
        ];

    $form['pdp_labels']['product_as_shown'] = [
          '#type' => 'textfield',
          '#title' => $this->t('Product as Shown'),
          '#default_value' => $config->get('product_as_shown'),
        ];
    $form['pdp_labels']['product_as_shown_list_title'] = [
          '#type' => 'textfield',
          '#title' => $this->t('Product as Shown list label'),
          '#default_value' => $config->get('product_as_shown_list_title'),
        ];
    $form['pdp_labels']['view_full_collection'] = [
          '#type' => 'textfield',
          '#title' => $this->t('View Collection'),
          '#default_value' => $config->get('view_full_collection'),
        ];
    $form['pdp_labels']['download_collection_pdf'] = [
          '#type' => 'textfield',
          '#title' => $this->t('Download Collection PDF'),
          '#default_value' => $config->get('download_collection_pdf'),
        ];
    $form['pdp_labels']['discontinued_prd_label'] = [
          '#type' => 'textfield',
          '#title' => $this->t('Discontinued Ribbon Text'),
          '#default_value' => $config->get('discontinued_prd_label'),
        ];
    $form['pdp_labels']['pdp_404_body_text'] = array(
      '#type' => 'text_format',
      '#title' => '404 content',
      '#default_value' => $config->get('pdp_404_body_text.value'),
      '#format' => $config->get('pdp_404_body_text.format'),
      );

      $form['pdp_labels']['pdp_no_model_body_text'] = array(
        '#type' => 'text_format',
        '#title' => 'No model number specified message content',
        '#default_value' => $config->get('pdp_no_model_body_text.value'),
        '#format' => $config->get('pdp_no_model_body_text.format'),
        );

    $form['pdp_settings'] = array(
       '#type' => 'details',
       '#title' => $this->t('Settings'),
       '#collapsible' => TRUE,
       '#collapsed' => TRUE,
       '#description' => $this->t('Configuration Settings'),
     );
    $form['pdp_settings']['finish_stocking_type'] = [
          '#type' => 'textfield',
          '#title' => $this->t('Finish Stocking Type'),
          '#default_value' => $config->get('finish_stocking_type'),
    ];
    $form['pdp_settings']['pdp_related_page_size'] = [
          '#type' => 'textfield',
          '#title' => $this->t('Page size'),
          '#default_value' => $config->get('pdp_related_page_size'),
    ];
    $form['pdp_settings']['pdp_related_page_max_size'] = [
          '#type' => 'textfield',
          '#title' => $this->t('Max size count'),
          '#default_value' => $config->get('pdp_related_page_max_size'),
    ];
    $form['pdp_settings']['pdp_related_prd_order'] = [
          '#type' => 'textfield',
          '#title' => $this->t('Key order for the Related Products'),
          '#default_value' => $config->get('pdp_related_prd_order'),
          '#maxlength' => 500,
    ];
    $form['pdp_settings']['pdp_config_options_filter'] = [
          '#type' => 'textfield',
          '#title' => $this->t('Enter optionLabel to be excluded'),
          '#default_value' => $config->get('pdp_config_options_filter'),
          '#maxlength' => 500,
    ];

    $form['pdp_cookies'] = array(
       '#type' => 'details',
       '#title' => $this->t('Cookies'),
       '#collapsible' => TRUE,
       '#collapsed' => TRUE,
       '#description' => $this->t('PDP recently viewed cookies Settings'),
     );
     $form['pdp_cookies']['pdp_cookie_max_count'] = [
           '#type' => 'textfield',
           '#title' => $this->t('Max product count'),
           '#default_value' => $config->get('pdp_cookie_max_count'),
           '#description' => $this->t('Maximun count of product to store in temp storage. Enter 0 for unlimited'),
           '#maxlength' => 500,
     ];
    $form['pdp_cookies']['pdp_cookie_key'] = [
          '#type' => 'textfield',
          '#title' => $this->t('Cookie name'),
          '#default_value' => $config->get('pdp_cookie_key'),
          '#maxlength' => 500,
    ];


      return parent::buildForm($form, $form_state);

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $this->config('brizo_pdp.settings')
      ->set('related_products', $form_state->getValue('related_products'))
      ->set('recently_viewd_products', $form_state->getValue('recently_viewd_products'))
      ->set('image_slider', $form_state->getValue('image_slider'))
      ->set('share_option', $form_state->getValue('share_option'))
      ->set('download_image', $form_state->getValue('download_image'))
      ->set('pdp_url_patterns', $form_state->getValue('pdp_url_patterns'))
      ->set('product_category', $form_state->getValue('product_category'))
      ->set('pdp_collection_url_patterns', $form_state->getValue('pdp_collection_url_patterns'))
      ->set('pdp_parts_url_patterns', $form_state->getValue('pdp_parts_url_patterns'))
      ->set('pdp_design_files', $form_state->getValue('pdp_design_files'))
      ->set('pdp_technical_files', $form_state->getValue('pdp_technical_files'))
      ->set('cad_design_title', $form_state->getValue('cad_design_title'))
      ->set('accessories_title', $form_state->getValue('accessories_title'))
      ->set('technical_design_title', $form_state->getValue('technical_design_title'))
      ->set('technical_specific_title', $form_state->getValue('technical_specific_title'))
      ->set('features_title', $form_state->getValue('features_title'))
      ->set('handle_accent_title', $form_state->getValue('handle_accent_title'))
      ->set('related_product_title', $form_state->getValue('related_product_title'))
      ->set('rought_value_title', $form_state->getValue('rought_value_title'))
      ->set('finish_stocking_type', $form_state->getValue('finish_stocking_type'))
      ->set('finish_label', $form_state->getValue('finish_label'))
      ->set('download_file', $form_state->getValue('download_file'))
      ->set('product_as_shown', $form_state->getValue('product_as_shown'))
      ->set('product_as_shown_list_title', $form_state->getValue('product_as_shown_list_title'))
      ->set('view_file', $form_state->getValue('view_file'))
      ->set('view_full_collection', $form_state->getValue('view_full_collection'))
      ->set('download_collection_pdf', $form_state->getValue('download_collection_pdf'))
      ->set('pdp_technical_specification', $form_state->getValue('pdp_technical_specification'))
      ->set('pdp_technical_spec_attributes', $form_state->getValue('pdp_technical_spec_attributes'))
      ->set('pdp_technical_spec_image_param', $form_state->getValue('pdp_technical_spec_image_param'))
      ->set('pdp_technical_spec_image_path', $form_state->getValue('pdp_technical_spec_image_path'))
      ->set('pdp_body_assembly_param', $form_state->getValue('pdp_body_assembly_param'))
      ->set('pdp_riser_union_param', $form_state->getValue('pdp_riser_union_param'))
      ->set('pdp_rough_valve_param', $form_state->getValue('pdp_rough_valve_param'))
      ->set('pdp_handle_accent_param', $form_state->getValue('pdp_handle_accent_param'))
      ->set('pdp_related_page_size', $form_state->getValue('pdp_related_page_size'))
      ->set('pdp_related_page_max_size', $form_state->getValue('pdp_related_page_max_size'))
      ->set('pdp_related_prd_order', $form_state->getValue('pdp_related_prd_order'))
      ->set('body_assembly_title', $form_state->getValue('body_assembly_title'))
      ->set('riser_union_title', $form_state->getValue('riser_union_title'))
      ->set('pdp_config_options_filter', $form_state->getValue('pdp_config_options_filter'))
      ->set('pdp_cookie_expiration_time', $form_state->getValue('pdp_cookie_expiration_time'))
      ->set('pdp_cookie_path', $form_state->getValue('pdp_cookie_path'))
      ->set('pdp_cookie_key', $form_state->getValue('pdp_cookie_key'))
      ->set('pdp_cookies_httpOnly', $form_state->getValue('pdp_cookies_httpOnly'))
      ->set('pdp_recently_viewed_title', $form_state->getValue('pdp_recently_viewed_title'))
      ->set('pdp_cookie_max_count', $form_state->getValue('pdp_cookie_max_count'))
      ->set('pdp_cold_handle_param', $form_state->getValue('pdp_cold_handle_param'))
      ->set('discontinued_prd_label', $form_state->getValue('discontinued_prd_label'))
      ->set('pdp_404_body_text', $form_state->getValue('pdp_404_body_text'))
      ->set('pdp_no_model_body_text', $form_state->getValue('pdp_no_model_body_text'))
      ->save();
    parent::submitForm($form, $form_state);
  }


 }

<?php

namespace Drupal\brizo_pdp\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\delta_services\DeltaService;
use Drupal\product_sync\ProductImport;
use Drupal\brizo_pdp\ProductDetails;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Url;


/**
 *  Controller for the brizo_pdp module.
 */

 class PdpController extends ControllerBase {

   /**
    * Drupal\Core\Logger\LoggerChannelFactoryInterface definition.
    *
    * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
    */
   protected $loggerFactory;

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */

   protected $configFactory;

   /**
    * Constructs a new GetProductsController object.
    *
    * @param \Drupal\delta_services\DeltaService $deltaservice
    *   The deltaservice to make API calls.
    */

    protected $deltaservice;

    /**
     *
     * @param \Drupal\product_sync\ProductImport $productImport
     *   The deltaservice to make API calls.
     */

     protected $productImport;

     /**
      *
      * @param \Drupal\product_sync\ProductDetails $productImport
      *   The deltaservice to make API calls.
      */

      protected $productDetails;


     /**
      * {@inheritdoc}
      */

     public function __construct(DeltaService $delta_service, ConfigFactoryInterface $config_factory, ProductImport $product_import,
                                 LoggerChannelFactoryInterface $logger_factory, ProductDetails $product_detail) {
      $this->deltaservice = $delta_service;
      $this->configFactory = $config_factory;
      $this->productImport = $product_import;
      $this->loggerFactory = $logger_factory;
      $this->productDetails = $product_detail;

     }

     /**
      * {@inheritdoc}
      */

     public static function create(ContainerInterface $container) {
       return new static(
         $container->get('delta_services.service'),
         $container->get('config.factory'),
         $container->get('product_sync.import'),
         $container->get('logger.factory'),
         $container->get('brizo_pdp.details')
       );
     }


  public function getRoute($sku = null) {
    $this->loggerFactory->get('Brizo PDP')->info('PDP log- Entering pdp method  - ' . $sku);
    $isExist = $this->productImport->productExists($sku);
    $config = $this->configFactory->get('delta_services.deltaservicesconfig');
    $siteName = $config->get('global_site_name');
    if (!empty(key($isExist))) {
      $productId = key($isExist);
      $alias = \Drupal::service('path_alias.manager')->getAliasByPath('/product/'.$productId);
      $this->loggerFactory->get('Brizo PDP')->info('PDP log- Product already exist  - ' . $sku);
      $response = new RedirectResponse($alias);
      return $response->send();
    }
    else{
      $this->loggerFactory->get('Brizo PDP')->info('PDP log- Before valid product sku check  - ' . $sku);
      $data = $this->productImport->isValidProductSKU($sku);
      $this->loggerFactory->get('Brizo PDP')->info('PDP log- After valid product sku check  - ' . $sku);
      if($data['status'] == 1 && !empty($data['productData'])){
        $productId = $this->productImport->createProduct($data['productData'],$siteName);
        $this->loggerFactory->get('Brizo PDP')->info('PDP log- After product created  - ' . $sku);
        if($productId) {
          $alias = \Drupal::service('path_alias.manager')->getAliasByPath('/product/'.$productId);
          $this->loggerFactory->get('Brizo PDP')->info('PDP log- Redirect after product created  - ' . $sku);
          $response = new RedirectResponse($alias);
          return $response->send();
        }
        else {
          $this->loggerFactory->get('Brizo PDP')->warning('Invalid SKU/CATEGORY  - ' . $sku);
          return $this->get404Page($sku);
        }

      }
      else {
        $this->loggerFactory->get('Brizo PDP')->warning('Invalid SKU given - ' . $sku);
        return $this->get404Page($sku);

      }
    }
  }

  public function content() {

    return [
      '#type' => 'markup',
      '#markup' => $this->t('Hello, World!'),
    ];
  }


  public function get404Page($sku = '') {

    $pdp_config = $this->configFactory->get('brizo_pdp.settings');
    $recently_viewed_enabled = $pdp_config->get('recently_viewd_products');
    $config = $this->configFactory->get('delta_services.deltaservicesconfig');
    $page_content = '';
    $recently_viewed_avail = 0;

    if( $sku != '') {
      $page_404_content = $pdp_config->get('pdp_404_body_text.value');
      $page_404_content = str_replace('{PRD_SKU}', $sku, $page_404_content);
      $page_content = $page_404_content;
    }
    else {
      $no_model_no_content = $pdp_config->get('pdp_no_model_body_text.value');
      $page_content = $no_model_no_content;
    }

    if ( $recently_viewed_enabled == 1) {
      $recently_viewed_products = $this->productDetails->getRecentlyViewedProductDetails($sku);
      if (count($recently_viewed_products) > 0) {
        $recently_viewed_avail = 1;
        $pdp_recently_viewed_title = $pdp_config->get('pdp_recently_viewed_title');
      }
    }
    $recently_viewed = [ 'recently_viewed_avail' => $recently_viewed_avail,
        'pdp_recently_viewed_title' => $pdp_recently_viewed_title,
        'recently_viewed_products' => $recently_viewed_products
      ];

    $current_url =  Url::fromRoute('<current>', [], ['query' => \Drupal::request()->query->all(), 'absolute' => 'true'])->toString();
    if ( $current_url != '' ) {
      $url_segment = explode("/", $current_url);
      if ( count($url_segment)  > 3 ) {
        $category = $url_segment[count($url_segment) - 3];
      }
    }
    if ( $sku != '') {
      $breadcrumb = $this->productDetails->create_breadcrumb($category);
    }
    else {
      $breadcrumb = $this->productDetails->create_breadcrumb($category, 404);

    }
    return [
      '#theme' => 'product-404',
      '#recently_viewed' => $recently_viewed,
      '#content' => $page_content,
      '#breadcrumb' => $breadcrumb,
    ];
  //throw new NotFoundHttpException();

  }

  public function noModelNumber() {
    return $this->get404Page();
  }

}

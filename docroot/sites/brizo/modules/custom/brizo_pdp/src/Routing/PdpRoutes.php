<?php

namespace Drupal\brizo_pdp\Routing;

use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines dynamic routes.
 */
class PdpRoutes implements ContainerInjectionInterface {

  /**
  * The Config.
  *
  * @var \Drupal\Core\Config\ConfigFactoryInterface
  */
 protected $config;

 /**
   * Class constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory interface service.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->config = $config_factory->get('brizo_pdp.settings');
  }

  /**
 * {@inheritdoc}
 */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory')
    );
  }



  /**
   * Returns an array of route objects.
   *
   * @return \Symfony\Component\Routing\Route[]
   *   An array of route objects.
   */

  public function routes() {

    $route_collection = new RouteCollection();
    $category = $this->config->get('product_category');
    $detail_page_pattern = $this->config->get('pdp_url_patterns');
    foreach ($category as $path)
    {
      $detail_page_path = str_replace("{CATEGORY}" , $path, $detail_page_pattern );
      ${ 'route' . $path } = new Route(
        $detail_page_path,
        [
          '_controller' => '\Drupal\brizo_pdp\Controller\PdpController::getRoute',
          '_title' => 'Product'
        ],
        [
          '_permission'  => 'access content',
        ]
      );
      $route_collection->add('pdp.details_page_'.$path, ${ 'route' . $path });
    }

    // Parts details page path

    $parts_url_pattern = $this->config->get('pdp_parts_url_patterns');
    $part_route = new Route(

      $parts_url_pattern,
      [
        '_controller' => '\Drupal\brizo_pdp\Controller\PdpController::getRoute',
        '_title' => 'Reparit Parts'
      ],
      [
        '_permission'  => 'access content',
      ]

    );

    $route_collection->add('pdp.details_page_parts', $part_route);


    return $route_collection;
  }

}

<?php

/**
 * @file
 * Contains \Drupal\delta_login\Plugin\Block\UserLoginBlock.
 */

namespace Drupal\brizo_general\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\user\UserStorageInterface;
use Drupal\user\Entity\User;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;



/**
 * Provides a 'User Login Welcome' Block.
 *
 * @Block(
 *   id = "user_login_welcome_block",
 *   admin_label = @Translation("User Login Welcome"),
 *   category = @Translation("Custom Block"),
 * )
 */
class UserLoginWelcomeBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Drupal\user\UserStorageInterface
   */
  protected $user;

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Symfony\Component\HttpFoundation\Session.
   *
   * @var \Symfony\Component\HttpFoundation\SessionInterface
   */
  protected $session;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, AccountProxyInterface $current_user, UserStorageInterface $user, ConfigFactoryInterface $config_factory, SessionInterface $session) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->currentUser = $current_user;
    $this->user = $user;
    $this->configFactory = $config_factory;
    $this->session = $session;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_user'),
      $container->get('entity_type.manager')->getStorage('user'),
      $container->get('config.factory'),
      $container->get('session')
    );
  }


  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->configFactory->get('user_account_mgmt.user_account_mgmt_config');
    $build = [];
    $name = '';
    $postUrl = $config->get('elearning_url');
    $ouid = $config->get('ouid');
    $key = $this->session->get('training_token');
    $mail = $this->currentUser->getEmail();
    $uid = $this->currentUser->id();
    if($uid > 0) {
      $user = $this->user->load($uid);
      $firstname = $user->field_first_name->value;
      $lastname = $user->field_last_name->value;
      $name = $firstname ." ". $lastname;
    }

    $build['output'] = [
      '#theme' => 'custom_login_block',
      '#user' => ['uid' => $uid, 'name' => $name, 'ouid' => $ouid, 'key' => $key, 'postUrl' => $postUrl]
    ];
    $build['#attached']['library'][] = 'brizo_general/elearning';
    $build['#cache']['max-age'] = 0;
    return $build;
  }
}

(function ($, Drupal, drupalSettings) {

    Drupal.behaviors.globalSearch = {
        attach: function (context, settings) {
            $("#LMSLink").click(function(e){
                e.preventDefault();
                $("form#LMSForm").submit();
            });
        }
    };
})(jQuery, Drupal, drupalSettings);

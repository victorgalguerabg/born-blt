<?php

namespace Drupal\brizo_design_gallery\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'Hello' Block.
 *
 * @Block(
 *   id = "design_gallery_block",
 *   admin_label = @Translation("Design Gallery block"),
 *   category = @Translation("Design Gallery"),
 * )
 */
class DesignGalleryBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $form = \Drupal::formBuilder()->getForm('\Drupal\brizo_design_gallery\Form\DesignGalleryForm');
    return $form;
  }

}

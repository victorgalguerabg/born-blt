<?php

namespace Drupal\brizo_design_gallery;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\InsertCommand;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\delta_services\DeltaService;

class DesignGalleryService {
  /**
   * Drupal\delta_services\DeltaService definition.
   *
   * @var \Drupal\delta_services\DeltaService
   */
  protected $deltaservice;

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  public function __construct(DeltaService $deltaservice,ConfigFactoryInterface $configFactory) {
    $this->deltaservice = $deltaservice;
    $this->configFactory = $configFactory;
  }

  public function getDesignGallery($room="",$collection="",$finishes="",$pagevalue=""){
    $output = [];
    $build = [];
    $config = $this->configFactory->get('delta_services.deltaservicesconfig');
    $deltaConfig = $this->configFactory->get('sync.api_settings');
    $domain = $config->get('api_base_path');
    $designGalleryConfig = $this->configFactory->get('global_sync.api_settings');
    $designgalleryApi = $designGalleryConfig->get('design_gallery_api');

    if($room != ""){
      $roomValue = "&type=".$room;
    }
    else{
      $roomValue = "";
    }
    if($collection != ""){
      $collectionValue = "&title=".$collection;
    }
    else{
      $collectionValue = "";
    }
    if($finishes != ""){
      $finishValue = "&cf_finish=".$finishes;
    }
    else{
      $finishValue = "";
    }
    if($pagevalue == ""){
      $pagevalue = 0;
    }
    $designgalleryfilter = $roomValue.$collectionValue.$finishValue;
    $url = $domain.$designgalleryApi;
    $setpage = str_replace("{pagevalue}",$pagevalue, $url);
    $designGalleryPath = str_replace("{selectedstring}", $designgalleryfilter, $setpage);
    $rawResponse = $this->deltaservice->apiCall(
      $designGalleryPath, 'GET', [],'', "Design-gallery"
    );
    $response = json_decode($rawResponse['response']);
    $basecontent = $response->content;
    $totalItems = $response->totalElements;
    $totalPages = $response->totalPages;
    $numberOfElements = $response->numberOfElements;
    $output['totalitems'] = $totalItems;
    $total_pages =  ceil($totalItems/$numberOfElements);
   // $output['tot_pages']  =  (int) $total_pages;
    $output['tot_pages'] = $totalPages;
    $output['page_value'] = $pagevalue;
    $len = count($basecontent);
    foreach ($basecontent as $key => $value){
      if ($key != 0){
        $previd = $key - 1;
      }
      else{
        $previd = 0;
      }
      if ($key == $len-1){
        $nextid = "null";
      }else{
        $nextid = $key +1;
      }
      $fp_list = $value->metadata->cf_modelNumber;
      $large_url = $value->webURLLarge;
      $keys = parse_url($large_url); // parse the url
      $path = explode("/", $keys['path']); // splitting the path
      $last = end($path); //
      $replace_largeurl = str_replace("https","test", $large_url);
      $temp = [];
      $featureProductslist = "";
      $prefix = $responseFeature = "";
      foreach($fp_list as $fpproducts){
        $responseFeature .= $prefix . '' . $fpproducts . '';
        $prefix = ',';
      }
      $contributor = $value->metadata->contributor;
      if($contributor == ""){
        $contributor =  $value->metadata->creatorName;
      }
      $temp = [
        'id' => $key,
        'prevId' => $previd,
        'nextId' => $nextid,
        'title' => $value->metadata->title,
        'contributor' => $contributor,
        'feature_products' => $responseFeature,
        'webURLSmall' => $value->webURLSmall,
        'webURLLarge' => $last,
      ];
      $output['design_data'][] = $temp;
    }
    $build['output'] = [
      '#theme' => 'design_gallery',
      '#data' => $output,
    ];
    return $build;
  }
}

<?php

namespace Drupal\brizo_design_gallery\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\RedirectCommand;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\delta_services\DeltaService;
use Drupal\brizo_design_gallery\DesignGalleryService;
use Drupal\Core\Url;
use Drupal\taxonomy\Entity\Term;
use Drupal\Core\Entity\EntityTypeManagerInterface;



/**
 * Class DeltaServicesConfigForm.
 */
class DesignGalleryForm extends FormBase {

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;
  /**
   * Drupal\delta_services\DeltaService definition.
   *
   * @var \Drupal\delta_services\DeltaService
   */
  protected $deltaservice;
  /**
   * Drupal\DesignGalleryService\DeltaService definition.
   *
   * @var \Drupal\delta_services\DeltaService
   */
  protected $designgalleryservice;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Class constructor.
   */
  public function __construct(DeltaService $deltaService, DesignGalleryService $designgalleryservice, EntityTypeManagerInterface $entity_type_manager) {
    $this->deltaservice = $deltaService;
    $this->designgalleryservice = $designgalleryservice;
    $this->entityTypeManager = $entity_type_manager;

  }

  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
    // Load the service required to construct this class.
      $container->get('delta_services.service'),
      $container->get('brizo_design_gallery.service'),
      $container->get('entity_type.manager')
    );
  }
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'design_gallery_general_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['showroom_title'] = [
      '#type' => 'markup',
      '#markup' => '<div class="design-gallery-filter"><div class="design-gallery-title">Narrow Results</div>',
      '#weignt' => '1'
    ];
    $current_path = \Drupal::service('path.current')->getPath();
    $result = \Drupal::service('path_alias.manager')->getAliasByPath($current_path);
    $room_options['Bath'] = t('BATH');
    $room_options['Kitchen'] = t('KITCHEN');
    //if(strpos($result, 'shower') !== false) {
    $room_options['Shower'] = t('SHOWER');
    //  }
    $form['roomselect'] = [
      '#type' => 'select',
      '#options' => $room_options,
      '#empty_option' => t('ROOM'),
      '#attributes' => [
        'class' => ['form-item__select'],
      ],
      '#ajax' => [
        'callback' => '::myAjaxCallback',
        'wrapper' => 'gallery'
      ],
      '#default_value' => (strpos($result, 'shower') !== false)? ['Shower']:'',
    ];

    $collections = $this->getCollectionList('', '');
    foreach($collections as $options){
      $collection_options[] = $options['name'];
    }
    sort($collection_options);
    $form['collection'] = [
      '#type' => 'select',
      '#options' => $collection_options,
      '#empty_option' => $this->t('COLLECTION'),
      '#attributes' => [
        'class' => ['form-item__select'],
      ],
      '#ajax' => [
        'callback' => '::myAjaxCallback',
        'wrapper' => 'gallery'
      ],
    ];
    $system_options = [
      'pressure-balance' => "Pressure Balance",
      "sensori-thermostatic" => "SENSORI THERMOSTATIC",
      "tempassure-thermostatic" => "TEMPASSURE THERMOSTATIC",
    ];
    /*$form['system'] = [
      '#type' => 'select',
      '#options' => $system_options,
      '#empty_option' => $this->t('SYSTEMS'),
      '#attributes' => [
        'class' => ['form-item__select'],
      ],
      '#ajax' => [
        'callback' => '::myAjaxCallback',
        'wrapper' => 'gallery'
      ],
      '#states' => array(
        'visible' => array(
          array(':input[name="roomselect"]' => array('value' => 'Shower')),
        ),
      )
    ];*/
    $form['finishes'] = [
      '#type' => 'select',
      '#options' => array('Brilliance+Brass' => t('BRILLIANCE BRASS'),
        'Brilliance+Brushed+Bronze'=>t('BRILLIANCE BRUSHED BRONZE'),
        'Brilliance+Luxe+Stainless+/+Black+Crystal'=>t('BRILLIANCE LUXE STAINLESS / BLACK CRYSTAL'),
        'Brushed+Nickel'=>t('BRUSHED NICKEL'), 'Chrome'=>t('CHROME'),
        'Cocoa+Bronze+/+Polished+Nickel'=>t('COCOA BRONZE / POLISHED NICKEL'),
        'Cocoa+Bronze+/+Stainless+Steel'=>t('COCOA BRONZE / STAINLESS STEEL'),
        'Concrete'=>t('CONCRETE'),'Luxe+Gold'=>t('LUXE GOLD'),
        'Luxe+Gold+/+Black+Crystal'=>t('LUXE GOLD / BLACK CRYSTAL'),
        'Luxe+Nickel'=>('LUXE NICKEL'),'Luxe+Nickel++/Matte+Black' => t('LUXE NICKEL  / MATTE BLACK'),
        'Luxe+Nickel+/+Black+Crystal'=>t('LUXE NICKEL / BLACK CRYSTAL'),
        'Luxe+Nickel+/+Polished+Chrome'=>t('LUXE NICKEL / POLISHED CHROME'),
        'Luxe+Nickel+/+Teak+Wood'=> t('LUXE NICKEL / TEAK WOOD'),
        'Luxe+Steel'=>t('LUXE STEEL'),'Matte+Black'=>t('MATTE BLACK'),
        'Matte+Black+/+Luxe+Gold'=>t('MATTE BLACK/ LUXE GOLD'),
        'Matte+Black+With+Softtouch'=>t('MATTE BLACK WITH SOFTTOUCH'),
        'Matte+White'=>t('MATTE WHITE'),'Not+Applicable'=>t('NOT APPLICABLE'),
        'Polished+Chrome+/+Black+Crystal'=>t('POLISHED CHROME/ BLACK CRYSTAL'),
        'Polished+Chrome+/+Matte+White'=>t('POLISHED CHROME / MATTE WHITE'),
        'Polished+Chrome+/+Teak+Wood'=>t('POLISHED CHROME / TEAK WOOD'),
        'Polished+Gold'=>t('POLISHED GOLD'),'Polished+Gold+/+Black+Crystal'=>t('POLISHED GOLD / BLACK CRYSTAL'),
        'Polished+Nickel'=>t('POLISHED NICKEL'),'Polished+Nickel+/+Black+Crystal'=>t('POLISHED NICKEL / BLACK CRYSTAL'),
        'Stainless'=>t('STAINLESS'),
        'Stainless+With+Soft+Touch'=>t('STAINLESS WITH SOFT TOUCH'),
        'Venetian+Bronze'=>t('VENETIAN BRONZE'),'White'=>t('WHITE')),
      '#empty_option' => $this->t('FINISH'),
      '#attributes' => [
        'class' => ['form-item__select'],
      ],
      '#ajax' => [
        'callback' => '::myAjaxCallback',
        'wrapper' => 'gallery'
      ],
    ];
    $form['showroom_filter'] = [
      '#type' => 'markup',
      '#markup' => '</div>',
      '#weight' => '3',
    ];
    $form['help'] = [
      '#type' => 'markup',
      '#markup' => '<div id="gallery"></div>',
      '#weight' => '3',
    ];
    /*$form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
      '#attributes' => [
        'class' => ['button button--primary button--lg'],
      ],
    ];*/
    return $form;

  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Can add validation if required.
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    /*$field_values = $form_state->getValues();
    $room_value = $field_values['roomselect'];
      $url = Url::fromRoute('brizo_design_gallery.autocomplete');
      $form_state->setRedirectUrl($url);*/
  }

  public function myAjaxCallback(array &$form, FormStateInterface $form_state) {

    $roomValue = $form_state->getValue('roomselect');
    $collectionValue = $form_state->getValue('collection');
    $finshesValue = $form_state->getValue('finishes');
    $designgalleryresult = $this->designgalleryservice->getDesignGallery($roomValue, $collectionValue, $finshesValue);
    $collection_options = '';
    $finish_options = '';
    $default_finish = '';
    $response = new AjaxResponse();
    $availFinish_array = $this->getFinishNames();

    /* dynamic collection & finish options */

    $triggered_element = $form_state->getTriggeringElement()['#name'];

    if( $triggered_element == 'roomselect') {

      // Collection dropdown
      $collection_selected = '';
      $collection_avil = 0;
      $collection_list = $this->getCollectionList($roomValue);
      if( !empty($collection_list)) {
        if( $collectionValue != '' ){
          $collection_options = "<option value='' >COLLECTION</option>";
        }
        else {
          $collection_options = "<option value='' selected=selected>COLLECTION</option>";
        }
        foreach($collection_list as $collection) {
          if( $collectionValue != '' ){

            if ($collectionValue == $collection['name']) {
              $collection_avil = 1;
              $collection_selected = 'selected = selected';
            }
            else {
              $collection_selected = '';
            }
           }

          $collection_options .= "<option value = '" . $collection['name'] . "'" . $collection_selected . ">" . strtoupper($collection['name'] ). "</option>";

        }
        if ($finshesValue == '' || $collection_avil == 0) {
          $default_finish = "<option value='' >FINISH</option>";
          foreach( $availFinish_array as $availFinish) {

            $default_finish .= "<option value = '" . $availFinish['value'] . "'>" . $availFinish['label'] . "</option>";
          }
          $response->addCommand(new HtmlCommand("#edit-finishes", $default_finish));
        }


        $response->addCommand(new HtmlCommand("#edit-collection", $collection_options));

      }
    } // collection and finish dropdown
    if($triggered_element == 'collection') {
      // Finish dropdown
      $selected = '';
      $collection_avial_finishes = $this->getCollectionList($roomValue, $collectionValue);
      if ( count($collection_avial_finishes[0]['finisOptions']) > 0 ) {
        $finish_list[] = $this->getFinishByTargetId($collection_avial_finishes[0]['finisOptions']);
      }
      $finises = array_unique($finish_list);
      if ($finshesValue != '' ) {
        $finish_options = "<option value='' >FINISH</option>";
      }
      else {
        $finish_options = "<option value='' selected=selected>FINISH</option>";

      }

      foreach($finises[0] as $finish) {
        if ($finshesValue != '' ) {
            if ($finshesValue == $availFinish_array[$finish['name']]['value']) {
              $selected = 'selected = selected';
            }
            else {
              $selected = '';
            }
          }
        $finish_options .= "<option value = '" . $availFinish_array[$finish['name']]['value'] . "' " . $selected . ">" . $availFinish_array[$finish['name']]['label'] . "</option>";
      }
      $response->addCommand(new HtmlCommand("#edit-finishes", $finish_options));
    }

    /* EOF dymaic collection */

    $response->addCommand(new HtmlCommand('#gallery', $designgalleryresult));
    return $response;
  }

  public function getCollectionList($category = '', $collection_name = '') {
    $query = $this->entityTypeManager->getStorage('taxonomy_term')->getQuery();
    $query->condition('vid', 'collections');
    $collection_category = $category;
    if( $collection_name != '') {
      $term_name = trim($collection_name);
      $term_name = htmlentities($term_name);
      $term_name = str_replace('&lt;sup&gt;&amp;trade;&lt;/sup&gt;', '', $term_name);
      $term_name = str_replace('&lt;sup&gt;&amp;reg;&lt;/sup&gt;', '', $term_name);
      $term_name = str_replace('&reg;', '', $term_name);
      $term_name = str_replace('&trade;', '', $term_name);
      $query->condition('name', '%' . $term_name . '%', "LIKE");
    }
    if ( $category != '' ) {
      $category_type = strtolower($category);
      if ( $category_type == 'bath') {
        $category_type = 'bathroom';
        $query->condition('field_bath_facet_hide', 0);
      }else{
        $query->condition('field_facet_hide', 0);
      }
      $query->condition('field_collection_category', $category_type);
      $query->sort('name')->addTag('distinct');
    }
    else {
      $group = $query->orConditionGroup()
        ->condition('field_collection_category', 'bathroom')
        ->condition('field_collection_category', 'kitchen')
        ->condition('field_collection_category', 'shower');
        $query->condition($group);
        $query->sort('name')->addTag('distinct');
    }

    $collection = [];
    $entity_ids = $query->execute();
    foreach ($entity_ids as $term_id) {
      $term = $this->entityTypeManager->getStorage("taxonomy_term")->load($term_id);
      $finishOptions = $term->get('field_available_finishes')->getValue();

      $collection[] = [
        'name' => $term->getName(),
        'finisOptions' => $finishOptions,
      ];
    }
    return $collection;
  }

  public function getTermByTargetID($target_id) {

    $term = $this->entityTypeManager->getStorage("taxonomy_term")->load($target_id);
    if( is_object($term)) {
      return $term->getName();
    }
  }

  public function getFinishByTargetId($target_ids) {

    $finish_name = [];
    foreach($target_ids as $entity) {
      $term = $this->entityTypeManager->getStorage("taxonomy_term")->load($entity['target_id']);
      if( is_object($term)) {
        $name = $term->getName();
        $finish_name[] = [ 'name' => $name ];
      }
    }
    return $finish_name;
  }


  public function getFinishNames() {

    $finish_list = [
      'brilliancebrass' => [ 'label' => 'BRILLIANCE BRASS', 'value'=> 'Brilliance+Brass'],
      'brilliancebrushedbronze' => [ 'label' => 'BRILLIANCE BRUSHED BRONZE', 'value'=> 'Brilliance+Brushed+Bronze'],
      'brillianceluxestainlessblackcrystal' => [ 'label' => 'BRILLIANCE LUXE STAINLESS / BLACK CRYSTAL', 'value'=> 'Brilliance+Luxe+Stainless+/+Black+Crystal'],
      'brushednickel' => [ 'label' => 'BRUSHED NICKEL', 'value'=> 'Brushed+Nickel'],
      'chrome' => ['label' => 'CHROME', 'value' => 'Chrome'],
      'cocoabronzepolishednickel' => [ 'label' => 'COCOA BRONZE / POLISHED NICKEL', 'value'=> 'Cocoa+Bronze+/+Polished+Nickel'],
      'cocoabronzestainlesssteel' => [ 'label' => 'COCOA BRONZE / STAINLESS STEEL', 'value' => 'Cocoa+Bronze+/+Stainless+Steel'],
      'concrete' => [ 'label' => 'CONCRETE', 'value'=> 'Concrete'],
      'luxegold' => [ 'label' => 'LUXE GOLD', 'value'=> 'Luxe+Gold'],
      'luxegoldblackcrystal' => [ 'label' => 'LUXE GOLD / BLACK CRYSTAL', 'value'=> 'Luxe+Gold+/+Black+Crystal'],
      'luxenickel' => [ 'label' => 'LUXE NICKEL', 'value'=> 'Luxe+Nickel'],
      'luxenickelblackcrystal' => [ 'label' => 'LUXE NICKEL / BLACK CRYSTAL', 'value' => 'Luxe+Nickel+/+Black+Crystal'],
      'luxenickelpolishedchrome' => [ 'label'=> 'LUXE NICKEL / POLISHED CHROM', 'value' => 'Luxe+Nickel+/+Polished+Chrome'],
      'luxenickelteakwood' => [ 'label'=> t('LUXE NICKEL / TEAK WOOD'), 'value' => 'Luxe+Nickel+/+Teak+Wood' ],
      'luxesteel' => ['label'=>t('LUXE STEEL'), 'value' => 'Luxe+Steel'],
      'luxenickelmatteblack' => [ 'label' => 'LUXE NICKEL  / MATTE BLACK', 'value' => 'Luxe+Nickel++/Matte+Black'],
      'matteblack' => [ 'label'=>t('MATTE BLACK'), 'value' => 'Matte+Black'],
      'matteblackluxegold' => [ 'label'=>t('MATTE BLACK/ LUXE GOLD'), 'value' => 'Matte+Black+/+Luxe+Gold'],
      'matteblackwithsofttouch' => [ 'label'=> 'MATTE BLACK WITH SOFTTOUCH', 'value' => 'Matte+Black+With+Softtouch'],
      'mattewhite' => [ 'label' => 'MATTE WHITE', 'value' => 'MATTE+WHITE'],
      'polishedchromeblackcrystal' => [ 'label' => 'POLISHED CHROME/ BLACK CRYSTAL', 'value'=> 'Polished+Chrome+/+Black+Crystal'],
      'polishedchromemattewhite' => [ 'label' => 'POLISHED CHROME / MATTE WHITE', 'value' => 'Polished+Chrome+/+Matte+White'],
      'polishedchrometeakwood' => [ 'label' => 'POLISHED CHROME / TEAK WOOD', 'value' => 'Polished+Chrome+/+Teak+Wood'],
      'polishedgold' => [ 'label'=>t('POLISHED GOLD'), 'value' => 'Polished+Gold'],
      'polishedgoldblackcrystal' => [ 'label'=>t('POLISHED GOLD / BLACK CRYSTAL'), 'value' => 'Polished+Gold+/+Black+Crystal'],
      'stainless' => [ 'label'=>t('STAINLESS'), 'value' => 'Stainless'],
      'stainlesswithsofttouch' => [ 'label'=>t('STAINLESS WITH SOFT TOUCH'), 'value' => 'Stainless+With+Soft+Touch'],
      'venetianbronze' => [ 'label'=>t('VENETIAN BRONZE'), 'value' => 'Venetian+Bronze'],
      'white' => [ 'label'=>t('WHITE'), 'value' => 'White'],
      'polishednickel'=> ['label' => "POLISHED NICKEL", 'value' => 'Polished+Nickel'],
      'notapplicable' => ['label' => 'NOT APPLICABLE', 'value' => 'Not+Applicable']
      ];
    return $finish_list;
  }

}

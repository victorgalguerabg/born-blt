<?php

namespace Drupal\brizo_design_gallery\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class DeltaServicesConfigForm.
 */
class DesignGalleryConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'design_gallery.configform',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'design_gallery_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('design_gallery.configuration');
    $form['design_gallery_path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API base path'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('api_base_path'),
    ];
    $form['api_credentials'] = [
      '#type' => 'details',
      '#title' => $this->t("API Credentials"),
    ];
    $form['api_credentials']['delta_api_username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Product API - Username'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('delta_api_username'),
      '#description' => 'Username for product API service authentication',
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Can add validation if required.
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('delta_services.deltaservicesconfig')
      ->set('api_base_path', $form_state->getValue('api_base_path'))
      ->set('delta_api_username', $form_state->getValue('delta_api_username'))
      ->set('api_contact_us', $form_state->getValue('api_contact_us'))
      ->save();
  }

}

<?php

namespace Drupal\brizo_design_gallery\Controller;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\delta_services\DeltaService;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Drupal\brizo_design_gallery\DesignGalleryService;
use Zend\Diactoros\Response\JsonResponse;
use Drupal\brizo_pdp\ProductDetails;
use Drupal\product_sync\ProductImport;

/**
 * Class DefaultController.
 */
class DesignGalleryController extends ControllerBase {
  /**
   * Drupal\sync_product\ProductImportController definition.
   *
   * @var \Drupal\brizo_design_gallery\DesignGalleryService
   */
  protected $designgalleryservice;
  /**
   * Drupal\delta_services\DeltaService definition.
   *
   * @var \Drupal\delta_services\DeltaService
   */
  protected $deltaservice;

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Drupal \brizo_pdp\ProductDetails definition.
   *
   * @var \Drupal\brizo_pdp\ProductDetails
   */
  protected $productDetails;

  /**
   * Drupal \brizo_pdp\ProductDetails definition.
   *
   * @var \Drupal\product_sync\ProductImport
   */
  protected $productImport;

  /**
   * To get current query string.
   *
   * @var Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;
  public function __construct(RequestStack $requestStack,DesignGalleryService $designgalleryservice,ProductDetails $productDetails,ProductImport $productImport,ConfigFactoryInterface $configFactory) {
    $this->requestStack = $requestStack;
    $this->designgalleryservice = $designgalleryservice;
    $this->productDetails = $productDetails;
    $this->productImport = $productImport;
    $this->configFactory = $configFactory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('request_stack'),
      $container->get("brizo_design_gallery.service"),
      $container->get('brizo_pdp.details'),
      $container->get('product_sync.import'),
      $container->get("config.factory")
    );
  }
  /**
   * Return the selected value.
   */
  public function testMethod() {
    $build = [
      '#markup' => 'The selected value: ',
    ];
    return $build;
  }
  /**
   * Implementation of compare products.
   */
  public function designGalleryPopup($title,$image,$nextid,$previd,$fp) {
    $build = [];
    $output = [];
    $large_url = urldecode($image);
    $feature_products = explode(",",$fp);
    $fp_details = [];
    $config = $this->configFactory->get('delta_services.deltaservicesconfig');
    $siteName = $config->get('global_site_name');
    foreach ($feature_products as $value) {
      $productId = key($this->productImport->productExists($value));
      if (!empty($productId)) {
        $fp_details = ['fpa_image' => $this->productDetails->getProductHeroImage($productId),
          'fpa_title' => $this->productDetails->getProductTitle($productId),
          'fpa_link' => $this->productDetails->getProductUrl($productId)];
      }
      else {
        $data = $this->productImport->isValidProductSKU($value);
        if ($data['status'] == 1) {
          $productId = $this->productImport->createProduct($data['productData'], $siteName);
          if (!empty($productId)) {
           $fp_details = ['fpa_image' => $this->productDetails->getProductHeroImage($productId),
                          'fpa_title' => $this->productDetails->getProductTitle($productId),
                          'fpa_link' => $this->productDetails->getProductUrl($productId)];
          }
        }
      }
      $fp_final[] = $fp_details;
    }
    $output['designgalleryimage'] = $image;
    $output['description'] = $title;
    $output['nextid'] = $nextid;
    $output['previd']=$previd;
    $output['fp_products'] = $fp_final;
    $build['output'] = [
      '#theme' => 'design_gallery_popup',
      '#data' => $output,
    ];
    return $build;
  }
  public function getDesignpaginationGallery(){
    $room = \Drupal::request()->query->get('roomselectvalue');
    $collection = \Drupal::request()->query->get('collectionselectvalue');
    $finishes = \Drupal::request()->query->get('finishselectvalue');
    $pagevalue =  \Drupal::request()->query->get('pagevalue');
    $response = $this->designgalleryservice->getDesignGallery($room,$collection,$finishes,$pagevalue);
    $output = render($response);
    return new Response($output);
  }

}

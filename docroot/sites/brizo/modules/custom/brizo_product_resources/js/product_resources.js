(function ($, Drupal, drupalSettings, Backbone) {
  Drupal.behaviors.plpBehaviour = {
    attach: function (context, settings) {
      let gtm_search = [];
      let gtm_search_click = [];
      let loadProducts = true;
      let pageNumber = 0;
      let lastPage = false;
      let totalResult = 0;
      let pageLimit = drupalSettings.pageLimit;
      let loader = $('.ajax-progress-throbber');
      $('#products-tab', context).once('getProducts').each(function () {

        // Retain search text in global search form.
        var vars = [], hash;
        var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        var pathArray = window.location.pathname.split( '/' );
        for(var i = 0; i < hashes.length; i++)
        {
          hash = hashes[i].split('=');
          vars.push(hash[0]);
          vars[hash[0]] = hash[1];
        }
        var _param = getSearchParams('searchTerm');

        var _type = getSearchParams('collection');
        var _cat = '';
        if(drupalSettings.searchParams.categories){
          _cat = drupalSettings.searchParams.categories;
        }

        $.ajax({
          type: "GET",
          url: "/search-spec",
          dataType: 'json',
          data: {
            searchTerm: _param,
            collection: _type,
            categories: _cat,
          },
          error: function (XMLHttpRequest, textStatus, errorThrown) {
            showErrorMessage(XMLHttpRequest.status, '#all-tab #output');
            showErrorMessage(XMLHttpRequest.status, '#all-tab #output');

          },
          beforeSend: function () {
            loader.show();
            $('#all-tab #output').addClass('loader-active');

          },
          complete: function () {
            // Manage tag records
            recalculate();

          },
          success: function (data) {
            if(data.totalElements == 0){
              $('.specTab').hide();
            }
            var prodView = new ProdView(data);
            loadProducts = 1;
            if(data){
              data.currentpage = 1;
            }
            var PaginationView = new ProductPager(data);
            $('html,body').animate({
              scrollTop: $("#product-resources-form").offset().top
            }, 'slow');
            loader.hide();

          }
        });


        $.ajax({
          type: "GET",
          url: "/search-cad",
          dataType: 'json',
          data: {
            searchTerm: _param,
            collection: _type,
            categories: _cat,
          },
          error: function (XMLHttpRequest, textStatus, errorThrown) {
            showErrorMessage(XMLHttpRequest.status, '#all-tab #output');
            showErrorMessage(XMLHttpRequest.status, '#all-tab #output');

          },
          beforeSend: function () {
            loader.show();

          },
          complete: function () {
            // Manage tag records
            recalculate();

          },
          success: function (data) {
            if(data.totalElements == 0){
              $('.cadTab').hide();
            }
            var cadView = new CadView(data);
            loadProducts = 1;
            if(data){
              data.currentpage = 1;
            }
            var PaginationView = new CadPager(data);
            loader.hide();
          }
        });


        $.ajax({
          type: "GET",
          url: "/search-image",
          dataType: 'json',
          data: {
            searchTerm: _param,
            collection: _type,
            categories: _cat,
          },
          error: function (XMLHttpRequest, textStatus, errorThrown) {
            showErrorMessage(XMLHttpRequest.status, '#all-tab #output');
            showErrorMessage(XMLHttpRequest.status, '#all-tab #output');

          },
          beforeSend: function () {
            loader.show();
          },
          complete: function () {
            // Manage tag records
            recalculate();
          },
          success: function (data) {
            if(data.totalElements == 0){
              $('.imgTab').hide();
            }
            var prodView = new ImageView(data);
            loadProducts = 1;
            if(data){
              data.currentpage = 1;
            }
            var PaginationView = new ImagePager(data);
            loader.hide();
          }
        });


      });

      var ImagePager = Backbone.View.extend({

        el: ".images-pager",
        template: _.template($("#images-pagination").html()),

        initialize: function (initData) {
          this.render(initData);
        },
        events: {
          'click [type="button"]': 'imgPager'
        },

        imgPager: function (e) {

          var params = [];
          params['pageNum'] = $(e.currentTarget).val();
          params['first'] = $(e.currentTarget).attr('first');
          params['last'] = $(e.currentTarget).attr('last');
          if($(e.currentTarget).val() === '>') {
            params['pageNum'] = $('.images-pager .pager__link.is-active').val();
            params['action'] = 'next';
          }
          if($(e.currentTarget).val() === '>>') {
            var total = $(e.currentTarget).attr('total');
            let deduct = 0;
            if(total != 0 ){
              deduct = 4;
              if(total <= 4){
                deduct = total - 1;
              }
            }

            params['pageNum'] = parseInt(total);
            params['first'] = parseInt(total)-deduct;
            params['last'] = parseInt(total);
          }
          if($(e.currentTarget).val() === '<') {
            params['pageNum'] = $('.images-pager .pager__link.is-active').val();
            params['action'] = 'prev';
          }
          if($(e.currentTarget).val() === '<<') {
            params['pageNum'] = 1;
            params['first'] = 1;
            params['last'] = 5;
          }
          let type = 'search-image';
          applyPager(type, params, 'pager');
        },
        render: function (initData) {
          var cont_disp_cnt = pageLimit;
          if (cont_disp_cnt == null) {
            cont_disp_cnt = 24;
          }
          var _self = this;
          var _render = "";
          var _nextPage = (initData === null) ? 0 : parseInt(initData.number) + 1;
          var _currentpage = initData.currentpage;  // Current page
          var _totalmatches = initData.totalElements;
          var _totalpages = Math.ceil(_totalmatches / cont_disp_cnt);
          var _nextPage;
          var _first;
          if (_currentpage < _totalpages) {
            _nextPage = _currentpage + 1;
          } else {
            _nextPage = _currentpage;
          }
          var _previousPage;
          if (_currentpage === 1) {
            _previousPage = 1;
            _first = 1;
          } else {
            _previousPage = _currentpage - 1;
            _first = initData.first;
          }
          if(_currentpage>=initData.first && _currentpage<=initData.last) {
            _first = parseInt(initData.first);
          }
          if(initData.action && initData.action == 'next') {
            if(_currentpage>=initData.first && _currentpage<initData.last) {
              _first = parseInt(initData.first);
            } else {
              _first = parseInt(initData.first) + 1;
            }
            _currentpage = parseInt(_currentpage) + 1;
          }
          if(initData.action && initData.action == 'prev') {
            if(_currentpage>initData.first && _currentpage<=initData.last) {
              _first = parseInt(initData.first);
            } else {
              _first = parseInt(initData.first) - 1;
            }
            _currentpage = parseInt(_currentpage) - 1;
          }
          var pagination = parseInt(_first)+4;
          if(_totalpages < pagination) {
            pagination = _totalpages;
          }
          if(_currentpage != 1) {
            _render += '<input total="'+_totalpages+'" class="pager__link first" type="button" data="" value="<<">';
            _render += '<input first="'+_first+'" last="'+pagination+'" class="pager__link prev" type="button" data="" value="<">';
          }
          for(var i=_first;i<=pagination;i++) {
            var classValue = '';
            if(i === parseInt(_currentpage)) {
              classValue = 'is-active';
            }

            _render += '<input total="'+_totalpages+'"first="'+_first+'" last="'+pagination+'" class="pager__link '+ classValue +'" type="button" value="'+ i +'">';

          }

          for(var i=_first;i<=pagination;i++) {
            var classValue = '';

            if(i === parseInt(_currentpage)) {
              classValue = 'is-active';
              _render += '<div class="mobile-pagination pager__link">Page <span>'+i+'</span> of <span>'+_totalpages+'</span></div>';
            }
          }

          if((_currentpage != _totalpages) && _totalpages != 0) {
            _render += '<input first="'+_first+'" last="'+pagination+'" class="pager__link next" type="button" data="" value=">">';
            _render += '<input total="'+_totalpages+'" class="pager__link last" type="button" data="" value=">>">';
          }
          var start_value = (_currentpage-1) * cont_disp_cnt;
          if(_currentpage ==1) {
            start_value = 1;
          }
          var endvalue = _currentpage * cont_disp_cnt;
          if(start_value == 1 && endvalue > initData.totalElements){
            // no pager
          }
          else if(endvalue > initData.totalElements){
            endvalue = initData.totalElements;
            $('.srp-result-doc').html('<span class="srp-qty">Results ' + start_value + '-' + endvalue  + ' of ' + initData.totalElements +'</span>').attr('data-content-cnt', initData.totalElements);
          }
          else{
            $('.srp-result-doc').html('<span class="srp-qty">Results ' + start_value + '-' + endvalue  + ' of ' + initData.totalElements +'</span>').attr('data-content-cnt', initData.totalElements);
          }
          _self.$el.html(_render);
        }

      });

      var CadPager = Backbone.View.extend({

        el: ".cad-pager",
        template: _.template($("#cad-pagination").html()),

        initialize: function (initData) {
          this.render(initData);
        },
        events: {
          'click [type="button"]': 'cadPager'
        },

        cadPager: function (e) {

          var params = [];
          params['pageNum'] = $(e.currentTarget).val();
          params['first'] = $(e.currentTarget).attr('first');
          params['last'] = $(e.currentTarget).attr('last');
          if($(e.currentTarget).val() === '>') {
            params['pageNum'] = $('.cad-pager .pager__link.is-active').val();
            params['action'] = 'next';
          }
          if($(e.currentTarget).val() === '>>') {
            var total = $(e.currentTarget).attr('total');
            let deduct = 0;
            if(total != 0 ){
              deduct = 4;
              if(total <= 4){
                deduct = total - 1;
              }
            }

            params['pageNum'] = parseInt(total);
            params['first'] = parseInt(total)-deduct;
            params['last'] = parseInt(total);
          }
          if($(e.currentTarget).val() === '<') {
            params['pageNum'] = $('.cad-pager .pager__link.is-active').val();
            params['action'] = 'prev';
          }
          if($(e.currentTarget).val() === '<<') {
            params['pageNum'] = 1;
            params['first'] = 1;
            params['last'] = 5;
          }
          let type = 'search-cad';
          applyPager(type,params, 'pager');
        },
        render: function (initData) {
          var cont_disp_cnt = pageLimit;
          if (cont_disp_cnt == null) {
            cont_disp_cnt = 24;
          }
          var _self = this;
          var _render = "";
          var _nextPage = (initData === null) ? 0 : parseInt(initData.number) + 1;
          var _currentpage = initData.currentpage;  // Current page
          var _totalmatches = initData.totalElements;
          var _totalpages = Math.ceil(_totalmatches / cont_disp_cnt);
          var _nextPage;
          var _first;
          if (_currentpage < _totalpages) {
            _nextPage = _currentpage + 1;
          } else {
            _nextPage = _currentpage;
          }
          var _previousPage;
          if (_currentpage === 1) {
            _previousPage = 1;
            _first = 1;
          } else {
            _previousPage = _currentpage - 1;
            _first = initData.first;
          }
          if(_currentpage>=initData.first && _currentpage<=initData.last) {
            _first = parseInt(initData.first);
          }
          if(initData.action && initData.action == 'next') {
            if(_currentpage>=initData.first && _currentpage<initData.last) {
              _first = parseInt(initData.first);
            } else {
              _first = parseInt(initData.first) + 1;
            }
            _currentpage = parseInt(_currentpage) + 1;
          }
          if(initData.action && initData.action == 'prev') {
            if(_currentpage>initData.first && _currentpage<=initData.last) {
              _first = parseInt(initData.first);
            } else {
              _first = parseInt(initData.first) - 1;
            }
            _currentpage = parseInt(_currentpage) - 1;
          }
          var pagination = parseInt(_first)+4;
          if(_totalpages < pagination) {
            pagination = _totalpages;
          }
          if(_currentpage != 1) {
            _render += '<input total="'+_totalpages+'" class="pager__link first" type="button" data="" value="<<">';
            _render += '<input first="'+_first+'" last="'+pagination+'" class="pager__link prev" type="button" data="" value="<">';
          }
          for(var i=_first;i<=pagination;i++) {
            var classValue = '';
            if(i === parseInt(_currentpage)) {
              classValue = 'is-active';
            }

            _render += '<input total="'+_totalpages+'"first="'+_first+'" last="'+pagination+'" class="pager__link '+ classValue +'" type="button" value="'+ i +'">';

          }

          for(var i=_first;i<=pagination;i++) {
            var classValue = '';

            if(i === parseInt(_currentpage)) {
              classValue = 'is-active';
              _render += '<div class="mobile-pagination pager__link">Page <span>'+i+'</span> of <span>'+_totalpages+'</span></div>';
            }
          }

          if((_currentpage != _totalpages) && _totalpages != 0) {
            _render += '<input first="'+_first+'" last="'+pagination+'" class="pager__link next" type="button" data="" value=">">';
            _render += '<input total="'+_totalpages+'" class="pager__link last" type="button" data="" value=">>">';
          }
          var start_value = (_currentpage-1) * cont_disp_cnt;
          if(_currentpage ==1) {
            start_value = 1;
          }
          var endvalue = _currentpage * cont_disp_cnt;
          if(start_value == 1 && endvalue > initData.totalElements){
            // no pager
          }
          else if(endvalue > initData.totalElements){
            endvalue = initData.totalElements;
            $('.srp-result-doc').html('<span class="srp-qty">Results ' + start_value + '-' + endvalue  + ' of ' + initData.totalElements +'</span>').attr('data-content-cnt', initData.totalElements);
          }
          else{
            $('.srp-result-doc').html('<span class="srp-qty">Results ' + start_value + '-' + endvalue  + ' of ' + initData.totalElements +'</span>').attr('data-content-cnt', initData.totalElements);
          }
          _self.$el.html(_render);
        }

      });

      var _totalCount = 0;




      var CadView = Backbone.View.extend({

        el: "#cad-tab",
        template: _.template($("#cad-tiles").html()),

        initialize: function (initData, eventName, append) {
          this.render(initData, eventName,append);
        },

        render: function (initData, eventName, append) {
          if (!$('.preloader').length) {
            $('#plp-wrapper').prepend('<div class="preloader"><i class="input-loader"></i></div>');
          }

          var _self = this;
          var _render = "";
          var _i = 0;
          if (initData.content != '') {
            _.each(initData.content, function (entry) {

              var search_position = 0;
              var _price = 0;
              let _ribbonColor = '';

              var _filter = getSearchParams('documentLookFor');
              if (_filter !== '' || _filter !== undefined) {
                var _price = entry.values.ListPrice;
              }
              var _heroImage = entry.heroImageSmall;
              let _ribbonText = '';
              if (entry.values.MaterialStatus !== undefined && entry.values.MaterialStatus >= 50) {
                _ribbonText = 'discontinued';
                _ribbonColor = "gray-30";
              }

              var _title = "Title";
              if (entry.description !== '' && entry.description !== undefined) {
                _title = entry.description;
              }

              var _collection = "";
              if (entry.values.Collection !== "" && entry.values.Collection !== undefined) {
                _collection = entry.values.Collection;
              }


              let _currentDate = entry.values.currentDate;
              let _availableToShipDate = entry.values.AvailableToShipDateTimestamp;
              let _availableToOrderDate = entry.values.AvailableToOrderDateTimestamp;
              if (_currentDate < _availableToShipDate) {
                if (_currentDate > _availableToOrderDate) {
                  _ribbonText = entry.values.comingSoonText;
                  _ribbonColor = "delta-red";
                }
              }
              let _viewAllCollectionLink = entry.values.viewAllCollectionLink;

              var _sku = entry.name;
              var _url = '';

              _url = '/product-detail/' + _sku ;

              let _specSheet = '';
              let _MandI = '';
              let _partsDiagram = '';
              let _skp = new Array();
              let _dxf = new Array();
              let _rfa = new Array();
              let _dwg = new Array();
              let _vwx = new Array();
              let _gsm = new Array();

              if(entry.assets.length > 0){
                _.each(entry.assets, function (f) {
                  switch(f.type) {
                    case 'CAD':
                      let raw = f.filename.split('.');
                      let ext = raw[1];
                      if(ext == 'skp'){
                        _skp.push(f.url);
                      }else if(ext == 'dxf'){
                        _dxf.push(f.url);
                      }else if(ext == 'rfa'){
                        _rfa.push(f.url);
                      }else if(ext == 'dwg'){
                        _dwg.push(f.url);
                      }else if(ext == 'vwx'){
                        _vwx.push(f.url);
                      }else if(ext == 'gsm'){
                        _gsm.push(f.url);
                      }

                  }
                })
              }

              _render += _self.template({
                title: _title,
                collectionName: _collection,
                heroImage: _heroImage,
                sku: _sku,
                url: _url,
                ribbonText: _ribbonText,
                ribbonColor: _ribbonColor,
                viewAllCollectionLink:  _viewAllCollectionLink,
                SKP : _skp,
                DXF : _dxf,
                RFA : _rfa,
                DWG : _dwg,
                VWX : _vwx,
                GSM : _gsm
              });

              $('.tab-content-title-prod').show();
              $('.preloader').remove();
              $('body').removeClass('loader-active');
              _i++;
            });

            if(append ==1){
              _self.$el.append(_render);
            }else{
              _self.$el.html(_render);
              $('.product-all-search-result').html(_render);
            }
          } else {
            var noResultsMessage = '<div class ="error-message" role="contentinfo" aria-label="Error message"><h2 class="visually-hidden">Status message</h2>Oops!!! Nothing Found. Please refine your search</div>';
            $('#output').html(noResultsMessage);
            $('.preloader').remove();
            $('body').removeClass('loader-active');
          }
          var _cadCount = initData.totalElements;
          totalResult = totalResult + _cadCount;
          $('.cad-count').html('('+ _cadCount + ')');
          $('.cad-count').attr('count', _cadCount);
          recalculate();

          return this;
        }

      });

      var ProdView = Backbone.View.extend({

        el: "#products-tab",
        template: _.template($("#product").html()),

        initialize: function (initData, eventName, append) {
          this.render(initData, eventName,append);
        },

        render: function (initData, eventName, append) {
          if (!$('.preloader').length) {
            $('#plp-wrapper').prepend('<div class="preloader"><i class="input-loader"></i></div>');
          }

          var _self = this;
          var _render = "";
          var _i = 0;
          if (initData.content != '') {
            _.each(initData.content, function (entry) {

              var search_position = 0;
              var _price = 0;
              let _ribbonColor = '';

              var _filter = getSearchParams('documentLookFor');
              if (_filter !== '' || _filter !== undefined) {
                var _price = entry.values.ListPrice;
              }
              var _heroImage = entry.heroImageSmall;
              let _ribbonText = '';
              if (entry.values.MaterialStatus !== undefined && entry.values.MaterialStatus >= 50) {
                _ribbonText = 'discontinued';
                _ribbonColor = "gray-30";
              }

              var _title = "Title";
              if (entry.description !== '' && entry.description !== undefined) {
                _title = entry.description;
              }

              var _collection = "";
              if (entry.values.Collection !== "" && entry.values.Collection !== undefined) {
                _collection = entry.values.Collection;
              }


              let _currentDate = entry.values.currentDate;
              let _availableToShipDate = entry.values.AvailableToShipDateTimestamp;
              let _availableToOrderDate = entry.values.AvailableToOrderDateTimestamp;
              if (_currentDate < _availableToShipDate) {
                if (_currentDate > _availableToOrderDate) {
                  _ribbonText = entry.values.comingSoonText;
                  _ribbonColor = "delta-red";
                }
              }
              let _viewAllCollectionLink = entry.values.viewAllCollectionLink;

              var _sku = entry.name;
              var _url = '';

              _url = '/product-detail/' + _sku ;

              let _specSheet = '';
              let _MandI = '';
              let _partsDiagram = '';

              if(entry.assets.length > 0){
                _.each(entry.assets, function (f) {
                  switch(f.type) {
                    case 'SpecSheet':
                      _specSheet = f.url;
                      break;
                    case 'MandI':
                      _MandI = f.url;
                      break;
                    case 'PartsDiagram':
                      _partsDiagram = f.url;
                      break;
                  }
                })
              }

              _render += _self.template({
                title: _title,
                collectionName: _collection,
                heroImage: _heroImage,
                sku: _sku,
                url: _url,
                ribbonText: _ribbonText,
                ribbonColor: _ribbonColor,
                viewAllCollectionLink:  _viewAllCollectionLink,
                specSheet : _specSheet,
                MandI : _MandI,
                PartsDiagram : _partsDiagram
              });

              $('.tab-content-title-prod').show();
              $('.preloader').remove();
              $('body').removeClass('loader-active');
              _i++;
            });

            if(append ==1){
              _self.$el.append(_render);
            }else{
              _self.$el.html(_render);
              $('.product-all-search-result').html(_render);
            }
          } else {
            var noResultsMessage = '<div class ="error-message" role="contentinfo" aria-label="Error message"><h2 class="visually-hidden">Status message</h2>Oops!!! Nothing Found. Please refine your search</div>';
            $('#output').html(noResultsMessage);
            $('.preloader').remove();
            $('body').removeClass('loader-active');
          }
          var _productCount = initData.totalElements;
          totalResult = totalResult + _productCount;
          var allTabCount = totalResult;
          $('.product-count-tab').html('('+ _productCount + ')');
          $('.product-count-tab').attr('count', _productCount);
          recalculate();

          return this;
        }

      });

      var ProductPager = Backbone.View.extend({

        el: ".products-pager",
        template: _.template($("#product-pagination").html()),

        initialize: function (initData) {
          this.render(initData);
        },
        events: {
          'click [type="button"]': 'prodPager'
        },

        prodPager: function (e) {

          var params = [];
          params['pageNum'] = $(e.currentTarget).val();
          params['first'] = $(e.currentTarget).attr('first');
          params['last'] = $(e.currentTarget).attr('last');
          if($(e.currentTarget).val() === '>') {
            params['pageNum'] = $('.products-pager .pager__link.is-active').val();
            params['action'] = 'next';
          }
          if($(e.currentTarget).val() === '>>') {
            var total = $(e.currentTarget).attr('total');
            let deduct = 0;
            if(total != 0 ){
              deduct = 4;
              if(total <= 4){
                deduct = total - 1;
              }
            }

            params['pageNum'] = parseInt(total);
            params['first'] = parseInt(total)-deduct;
            params['last'] = parseInt(total);
          }
          if($(e.currentTarget).val() === '<') {
            params['pageNum'] = $('.products-pager .pager__link.is-active').val();
            params['action'] = 'prev';
          }
          if($(e.currentTarget).val() === '<<') {
            params['pageNum'] = 1;
            params['first'] = 1;
            params['last'] = 5;
          }
          let type = 'search-spec';
          applyPager(type, params, 'pager');
        },
        render: function (initData) {
          var cont_disp_cnt = pageLimit;
          if (cont_disp_cnt == null) {
            cont_disp_cnt = 24;
          }
          var _self = this;
          var _render = "";
          var _nextPage = (initData === null) ? 0 : parseInt(initData.number) + 1;
          var _currentpage = initData.currentpage;  // Current page
          var _totalmatches = initData.totalElements;
          var _totalpages = Math.ceil(_totalmatches / cont_disp_cnt);
          var _nextPage;
          var _first;
          if (_currentpage < _totalpages) {
            _nextPage = _currentpage + 1;
          } else {
            _nextPage = _currentpage;
          }
          var _previousPage;
          if (_currentpage === 1) {
            _previousPage = 1;
            _first = 1;
          } else {
            _previousPage = _currentpage - 1;
            _first = initData.first;
          }
          if(_currentpage>=initData.first && _currentpage<=initData.last) {
            _first = parseInt(initData.first);
          }
          if(initData.action && initData.action == 'next') {
            if(_currentpage>=initData.first && _currentpage<initData.last) {
              _first = parseInt(initData.first);
            } else {
              _first = parseInt(initData.first) + 1;
            }
            _currentpage = parseInt(_currentpage) + 1;
          }
          if(initData.action && initData.action == 'prev') {
            if(_currentpage>initData.first && _currentpage<=initData.last) {
              _first = parseInt(initData.first);
            } else {
              _first = parseInt(initData.first) - 1;
            }
            _currentpage = parseInt(_currentpage) - 1;
          }
          var pagination = parseInt(_first)+4;
          if(_totalpages < pagination) {
            pagination = _totalpages;
          }
          if(_currentpage != 1) {
            _render += '<input total="'+_totalpages+'" class="pager__link first" type="button" data="" value="<<">';
            _render += '<input first="'+_first+'" last="'+pagination+'" class="pager__link prev" type="button" data="" value="<">';
          }
          for(var i=_first;i<=pagination;i++) {
            var classValue = '';
            if(i === parseInt(_currentpage)) {
              classValue = 'is-active';
            }

            _render += '<input total="'+_totalpages+'"first="'+_first+'" last="'+pagination+'" class="pager__link '+ classValue +'" type="button" value="'+ i +'">';

          }

          for(var i=_first;i<=pagination;i++) {
            var classValue = '';

            if(i === parseInt(_currentpage)) {
              classValue = 'is-active';
              _render += '<div class="mobile-pagination pager__link">Page <span>'+i+'</span> of <span>'+_totalpages+'</span></div>';
            }
          }

          if((_currentpage != _totalpages) && _totalpages != 0) {
            _render += '<input first="'+_first+'" last="'+pagination+'" class="pager__link next" type="button" data="" value=">">';
            _render += '<input total="'+_totalpages+'" class="pager__link last" type="button" data="" value=">>">';
          }

          var start_value = (_currentpage-1) * cont_disp_cnt;
          if(_currentpage ==1) {
            start_value = 1;
          }
          var endvalue = _currentpage * cont_disp_cnt;
          if(start_value == 1 && endvalue > initData.totalElements){
            // no pager
          }
          else if(endvalue > initData.totalElements){
            endvalue = initData.totalElements;
            $('.srp-result-doc').html('<span class="srp-qty">Results ' + start_value + '-' + endvalue  + ' of ' + initData.totalElements +'</span>').attr('data-content-cnt', initData.totalElements);
          }
          else{
            $('.srp-result-doc').html('<span class="srp-qty">Results ' + start_value + '-' + endvalue  + ' of ' + initData.totalElements +'</span>').attr('data-content-cnt', initData.totalElements);
          }
          _self.$el.html(_render);
        }

      });

      var ImageView = Backbone.View.extend({

        el: "#images-tab",
        template: _.template($("#image-tiles").html()),

        initialize: function (initData, eventName, append) {
          this.render(initData, eventName,append);
        },

        render: function (initData, eventName, append) {
          if (!$('.preloader').length) {
            $('#plp-wrapper').prepend('<div class="preloader"><i class="input-loader"></i></div>');
          }

          var _self = this;
          var _render = "";
          var _i = 0;
          if (initData.content != '') {
            _.each(initData.content, function (entry) {

              var search_position = 0;
              var _price = 0;
              let _ribbonColor = '';

              var _filter = getSearchParams('documentLookFor');
              if (_filter !== '' || _filter !== undefined) {
                var _price = entry.values.ListPrice;
              }
              var _heroImage = entry.heroImageSmall;
              let _ribbonText = '';
              if (entry.values.MaterialStatus !== undefined && entry.values.MaterialStatus >= 50) {
                _ribbonText = 'discontinued';
                _ribbonColor = "gray-30";
              }

              var _title = "Title";
              if (entry.description !== '' && entry.description !== undefined) {
                _title = entry.description;
              }

              var _collection = "";
              if (entry.values.Collection !== "" && entry.values.Collection !== undefined) {
                _collection = entry.values.Collection;
              }


              let _currentDate = entry.values.currentDate;
              let _availableToShipDate = entry.values.AvailableToShipDateTimestamp;
              let _availableToOrderDate = entry.values.AvailableToOrderDateTimestamp;
              if (_currentDate < _availableToShipDate) {
                if (_currentDate > _availableToOrderDate) {
                  _ribbonText = entry.values.comingSoonText;
                  _ribbonColor = "delta-red";
                }
              }
              let _viewAllCollectionLink = entry.values.viewAllCollectionLink;

              var _sku = entry.name;
              var _url = '';

              _url = '/product-detail/' + _sku ;


              _render += _self.template({
                title: _title,
                collectionName: _collection,
                heroImage: _heroImage,
                sku: _sku,
                url: _url,
                ribbonText: _ribbonText,
                ribbonColor: _ribbonColor,
                viewAllCollectionLink:  _viewAllCollectionLink
              });

              $('.tab-content-title-prod').show();
              $('.preloader').remove();
              $('body').removeClass('loader-active');
              _i++;
            });

            if(append ==1){
              _self.$el.append(_render);
            }else{
              _self.$el.html(_render);
              $('.product-all-search-result').html(_render);
            }
          } else {
            var noResultsMessage = '<div class ="error-message" role="contentinfo" aria-label="Error message"><h2 class="visually-hidden">Status message</h2>Oops!!! Nothing Found. Please refine your search</div>';
            $('#output').html(noResultsMessage);
            $('.preloader').remove();
            $('body').removeClass('loader-active');
          }
          var _imgCount = initData.totalElements;
          totalResult = totalResult + _imgCount;
          $('.images-count').html('('+ _imgCount + ')');
          $('.images-count').attr('count', _imgCount);
          recalculate();

          return this;
        }

      });

      var showErrorMessage = function (status, wrapper) {
        switch (status) {
          case 400:
            $(wrapper).html("<div class='sorry-msg'>Server understood the request, but request content was invalid., Please try with valid information.</div>");
            break;
          case 401:
            $(wrapper).html("<div class='sorry-msg'>Unauthorized access.</div>");
            break;
          case 403:
            $(wrapper).html("<div class='sorry-msg'>We are unable to process your request at this time, Please try again later.</div>");
            break;
          case 500:
            $(wrapper).html("<div class='sorry-msg'>We are into the system upgrade process, Please try again later.</div>");
            break;
          case 401:
            $(wrapper).html("<div class='sorry-msg'>Service unavailable. Please try again later.</div>");
            break;
          default:
            $(wrapper).html("<div class='sorry-msg'>Oops something went wrong, Please try again later.</div>");
        }
      }

      /**
       * Recalculate tab total counts of records
       */
      function recalculate() {
        var grandTotal = 0;
        var data_cad_cnt = 0;
        var data_product_cnt = 0;
        var data_image_cnt = 0;

        if($(".product-count-tab").attr("count")) {
          data_product_cnt = $(".product-count-tab").attr("count");
          data_product_cnt = parseInt(data_product_cnt);
        }
        if($(".images-count").attr("count")) {
          data_image_cnt = $(".images-count").attr("count");
          data_image_cnt = parseInt(data_image_cnt);
        }
        if($(".cad-count").attr("count")){
          data_cad_cnt = $(".cad-count").attr("count");
          data_cad_cnt = parseInt(data_cad_cnt);
        }



        grandTotal = data_product_cnt;
        if(!$.isNumeric(grandTotal)) {
          grandTotal = 0;
        }
        $('#grand_total').html(grandTotal);
        if (grandTotal === 0) {
          $("#emptyResults").show();
          $('.search-container').hide();

          $('.tab-content-title-doc').hide();
          $('.tab-content-title-cont').hide();
          $('.tab-content-title-prod').hide();
          var typedKeyword = getSearchParams('title');
        }else{
          $("#emptyResults").hide();
          $('.search-container').show();
        }
      }

      /**
       *  Convert date to timestamp - 2015-07-23
       * @param dates
       * @returns {number}
       * @constructor
       */
      function DateTotimeStamp(dates) {
        if (dates !== undefined) {
          var dates1 = dates.split("-");
          var newDate = dates1[0] + "/" + dates1[1] + "/" + dates1[2];
          return new Date(newDate).getTime();
        }
      }

      /**
       * Get current time.
       * @returns {string}
       */

      function getCurrentDate() {
        var d = new Date($.now());
        return d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate();
      }

      /* Define functin to find and replace specified term with replacement string */
      function replaceAll(str, term, replacement) {
        return str.replace(new RegExp(escapeRegExp(term), 'g'), replacement);
      }


      /*
 Get search value from querystring.
 */
      function getSearchParams(k) {
        var p = {};
        location.search.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (s, k, v) {
          p[k] = v
        })
        var response = k ? p[k] : p;
        var decodedResponse = decodeURIComponent(response);
        var sanitizedResponse = removeHTMLTags(decodedResponse);
        return sanitizedResponse;
      }

      /*
           Function to remove HTML tags.
      */

      function removeHTMLTags(decodedResponse) {
        paramText = decodedResponse;
        paramText = paramText.replace(/<script.*>[\w\W]{1,}(.*?)[\w\W]{1,}<\/script>/gi, "");
        paramText = paramText.replace(/%3Cscript.*%3E[\w\W]{1,}(.*?)[\w\W]{1,}%3C\/script%3E/gi, "");
        paramText = paramText.replace(/<style.*>[\w\W]{1,}(.*?)[\w\W]{1,}<\/style>/gi, "");
        paramText = paramText.replace(/&nbsp;/gi, " ");
        paramText = paramText.replace(/&amp;/gi, "&");
        paramText = paramText.replace(/&quot;/gi, '"');
        paramText = paramText.replace(/&lt;/gi, '<');
        paramText = paramText.replace(/&gt;/gi, '>');
        paramText = paramText.replace(/svg\/onload%3D/gi, '');
        paramText = paramText.replace(/--%21/gi, '');
        paramText = paramText.replace(/svg\/onload%3D/gi, '');
        paramText = paramText.replace(/--%21/gi, '');
        paramText = paramText.replace(/%28confirm%29/gi, '')
        paramText = paramText.replace(/%3C/gi, '');
        paramText = paramText.replace(/%3E/gi, '');
        paramText = paramText.replace(/<(?:.|\s)*?>/g, "");
        return paramText;

      }




      function applyPager(type,params, eventName) {
        $('body').addClass('loader-active');
        let limit = pageLimit;
        let currentPage = params['pageNum'] == 0 ? 1 : params['pageNum'];

        loader.show();
        let pageNum = parseInt(params['pageNum']) - 1;
        let _param = getSearchParams('searchTerm');

        var _type = getSearchParams('collection');
        var _cat = '';
        if(drupalSettings.searchParams.categories){
          _cat = drupalSettings.searchParams.categories;
        }
        let _path = '/' + type;
        $.ajax({
          type: "GET",
          url: _path,
          dataType: 'json',
          data: {
            searchTerm: _param,
            collection: _type,
            categories: _cat,
            size: limit,
            page: pageNum,
            currentpage : currentPage
          },

          success: function (data) {
            if(params['pageNum'] && data) {
              data.currentpage = params['pageNum'];
              data.first = params['first'];
              data.last = params['last'];
            }
            if(params['action'] && data) {
              data.action = params['action'];
            }
            if(type == 'search-spec'){

              var prodView = new ProdView(data, eventName);
              var productPager = new ProductPager(data);
              loadProducts = 1;
              productPager.undelegateEvents();
            }else if(type == 'search-cad'){
              var cadView = new CadView(data, eventName);
              var cadPager = new CadPager(data);
              loadProducts = 1;
              cadPager.undelegateEvents();
            }else{
              var imgView = new ImageView(data, eventName);
              var imgPager = new ImagePager(data);
              loadProducts = 1;
              imgPager.undelegateEvents();
            }
            $('html,body').animate({
              scrollTop: $("#product-resources-form").offset().top
            }, 'slow');

            loader.hide();

          }
        });
      }

      $(window).once().scroll(function () {
        if (($(window).scrollTop() + $(window).height() > $('#plp-wrapper').height())
          && loadProducts && (!lastPage)) {
          loadProducts = false;
          totalResult = 0;
          let facet = $('#facetFinish').val();
          let sort  = $('#sortProduct').val();
          // On Scroll to achieve lazyload functionality.
          var _param = getSearchParams('title');
          var _type = getSearchParams('type');

          // var _filter = getSearchParams('documentLookFor');
          loader.show();

          pageNumber = pageNumber + 1;
          $.ajax({
            type: "GET",
            url: "/search-output",
            dataType: 'json',
            data: {
              title: _param,
              categories: _type,
              facet: facet,
              sort : sort,
              page:pageNumber
            },
            success: function (data) {
              if(data.totalElements > data.size) {
                var prodView = new ProdView(data, null, 1);
              }
              loadProducts = true;
              lastPage = data.last;
              loader.hide();
            }
          });
        }
      });

      var isFirst = true;
      $('.tabs').once().on('click', '.use-ajax', function(e){
        e.preventDefault();
        var url = $(this).attr('href');
        Drupal.ajax({url: url}).execute();
        var id = $(this).attr('id');
        if(isFirst){
          setTimeout(function () {
            $('#' + id).trigger('click');
          }, 200);
        }
      });


      $(".product-resources-page .product-resource-tab #images-tab .product-modelno a").on('click', function(e){
          $("body").addClass("pop-open");
      });
      $('.product-resources-page div').on('dialogclose', function(event) {
          $("body").removeClass("pop-open");
      });

    }

  };
})(jQuery, Drupal, drupalSettings, Backbone);

(function ($, Drupal, drupalSettings, Backbone) {
  Drupal.behaviors.modalBehaviour = {
    attach: function (context, settings) {

      if($(".image-resources-modal").length) {
        $(".image-resources-modal .product_details").insertAfter(".image-resources-modal .prod-big-img");
      }

      jQuery('.multiple-items').on('init', function(event, slick){

      });
      jQuery('.multiple-items').slick();


      var default_img = jQuery(".thumb-img-block.slick-current").find("img").attr("src");
      jQuery(".prod-big-img").find("img").attr('src', default_img);
      if(default_img){

        let small_src = default_img.replace('/md/','/sm/');
        let large_src = default_img.replace('/md/','/lg/');
        jQuery("#dl-large").attr('href', large_src);
        jQuery("#dl-medium").attr('href', default_img);
        jQuery("#dl-small").attr('href', small_src);
      }

      jQuery(".download__img-wrapper").find(".cta-link").attr('href', default_img);

      var first_img_pos = jQuery(".thumb-img-block.slick-current").index();
      jQuery(".desktop-thumb-img .thumb-img-block").click(function(){
        jQuery(".desktop-thumb-img .thumb-img-block").removeClass("active-thumbnail");
        jQuery(this).toggleClass("active-thumbnail");
        var img_src = jQuery(this).find("img").attr("src");

        console.log("URL"+ img_src);
        var img_pos = jQuery(this).index();

        jQuery(".prod-big-img").find("img").attr('src', img_src);
        let small_src = img_src.replace('/md/','/sm/');
        let large_src = img_src.replace('/md/','/lg/');
        jQuery("#dl-large").attr('href', large_src);
        jQuery("#dl-medium").attr('href', img_src);
        jQuery("#dl-small").attr('href', small_src);
        jQuery(".download__img-wrapper").find(".cta-link").attr('href', img_src);

        if(img_pos == first_img_pos) {
          jQuery(".thumb-img-wrapper-list .slider_flag").show();
        } else {
          jQuery(".thumb-img-wrapper-list .slider_flag").hide()
        }
      });

      var m_first_img_pos = jQuery(".mobile-thumb-img .thumb-img-block.slick-current").index();

      jQuery('.mobile-thumb-img .multiple-items').on('afterChange', function(){
        var m_active_img_pos = jQuery(".thumb-img-wrapper-list.mobile-thumb-img").find(".thumb-img-block.slick-current").index();

        if(m_active_img_pos == m_first_img_pos) {
          jQuery(".thumb-img-wrapper-list .slider_flag").show();
        } else {
          jQuery(".thumb-img-wrapper-list .slider_flag").hide();
        }
      });
    }

  };
})(jQuery, Drupal, drupalSettings, Backbone);

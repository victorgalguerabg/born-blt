<?php

namespace Drupal\brizo_product_resources\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\delta_services\DeltaService;
use Drupal\brizo_product_resources\ProductResourcesService;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\Component\Utility\Xss;

/**
 * Class DeltaServicesConfigForm.
 */
class ProductResourcesForm extends FormBase {

  /**
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;
  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;
  /**
   * Drupal\delta_services\DeltaService definition.
   *
   * @var \Drupal\delta_services\DeltaService
   */
  protected $deltaservice;

  /**
   * Drupal\brizo_product_resources\ProductResourcesService definition.
   *
   * @var \Drupal\brizo_product_resources\ProductResourcesService
   */
  protected $productResourceService;

  /**
   * Class constructor.
   */
  public function __construct(DeltaService $deltaService,
                              ProductResourcesService $productResourceService,
                              RequestStack $requestStack,
                              EntityTypeManagerInterface $entity_type_manager
  ) {
    $this->deltaservice = $deltaService;
    $this->productResourceService = $productResourceService;
    $this->requestStack = $requestStack;
    $this->entityTypeManager = $entity_type_manager;
  }
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
    // Load the service required to construct this class.
      $container->get('delta_services.service'),
      $container->get('brizo_product_resources.service'),
      $container->get('request_stack'),
      $container->get('entity_type.manager')
    );
  }
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'product_resources_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $categoryOptions = [];
    $subCategoryOptions = [];
    $queryString = $this->requestStack->getCurrentRequest();
    $defaultSearchTerm = $queryString->get('searchTerm');
    $defaultModel = $queryString->get('model');
    $defaultCollection = $queryString->get('collection');
    $defaultRoom = $queryString->get('room');
    if(!empty($defaultRoom)){
      $categoryOptions = $this->productResourceService->getCategory($defaultRoom);
    }
    $defaultSubCategory = $queryString->get('subCategory');
    if(!empty($defaultModel)){
      $subCategoryOptions = $this->productResourceService->getSubCategory($defaultModel);
    }

    $bathCollections = $this->getCollections('bathroom');
    $kitchenCollections = $this->getCollections('kitchen');

    $form['model_or_keyword'] = [
      '#type' => 'textfield',
      '#attributes' => [
        'class' => ['form-item__textfield'],
        'placeholder' => 'Enter Model Number or Keyword'
      ],
      '#default_value' => $defaultSearchTerm
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Search'),
      '#attributes' => [
        'class' => ['button button--primary button--lg'],
        'id' => 'by_keyword'
      ],
      '#name' => 'by_keyword'
    ];
    $form['collection'] = [
      '#type' => 'select',
      '#empty_option' => $this->t('Search By Collection Name'),
      '#default_value' => $defaultCollection,
      '#options' => [
        'Kitchen' => $kitchenCollections,
        'Bath'  =>  $bathCollections
      ],
      '#attributes' => [
        'class' => ['form-item__select'],
      ]
    ];
    $form['submit_1'] = [
      '#name' => 'by_collection',
      '#type' => 'submit',
      '#value' => $this->t('Search'),
      '#attributes' => [
        'class' => ['button button--primary button--lg'],
        'id' => 'by_collection'
      ],
    ];

    $form['room'] = [
      '#type' => 'select',
      '#empty_option' => $this->t('Select Room'),
      '#default_value' => $defaultRoom,
      '#options' => [
        'Brizo_Bath' => 'Bath',
        'Brizo_Kitchen' => 'Kitchen',
        'Brizo_Parts' => 'Parts'
      ],
      '#ajax' => [
        'event' => 'change',
        'callback' => [$this, 'ajax_select_callback'],
        'wrapper' => 'replace_model_div',
        'method' => 'replace',
      ]
    ];

    $form['submit_2'] = [
      '#name' => 'by_category',
      '#type' => 'submit',
      '#value' => $this->t('Search'),
      '#attributes' => [
        'class' => ['button button--primary button--lg'],
        'id' => 'by_category'
      ],
    ];

    $form['model_wrapper']['model'] = array(
      '#type' => 'select',
      '#empty_option' => $this->t('Search Category'),
      '#default_value' => $defaultModel,
      '#options' => $categoryOptions,
      '#ajax' => [
        'event' => 'change',
        'callback' => [$this, 'ajax_select_callback2'],
        'wrapper' => 'replace_sub_cat',
        'method' => 'replace',
      ],
      '#prefix' => '<div id="replace_model_div">',
      //'#suffix' => '</div>',
      '#validated' => TRUE
    );

    $form['model_wrapper']['subCategory'] = array(
      '#type' => 'select',
      '#empty_option' => $this->t('Search Sub-Category'),
      '#options' => $subCategoryOptions,
      '#default_value' => $defaultSubCategory,
      '#prefix' => '<div id="replace_sub_cat">',
      '#suffix' => '</div></div>',
      '#validated' => TRUE
    );

    if ($form_state->isRebuilding() &&
      $form_state->getValue('room')) {

      $form['model_wrapper'] = array(
        '#prefix' => '<div id="replace_model_div">',
      );

      $model_options = $this->_get_options_by_room($form_state->getValue('room'));
      $form['model_wrapper']['model'] = array(
        '#type' => 'select',
        '#empty_option' => $this->t('Search Category'),
        '#options' => $model_options,
        '#validated' => TRUE,
        '#ajax' => [
          'event' => 'change',
          'callback' => [$this, 'ajax_select_callback2'],
          'wrapper' => 'replace_sub_cat',
          'method' => 'replace',
        ]
      );
      $form['model_wrapper']['subCategory'] = array(
        '#type' => 'select',
        '#empty_option' => $this->t('Search Sub-Category'),
        '#options' => [],
        '#default_value' => [],
        '#prefix' => '<div id="replace_sub_cat">',
        '#suffix' => '</div></div>',
        '#validated' => TRUE
      );
    }elseif($form_state->isRebuilding() &&
      empty($form_state->getValue('room'))){
      $form['model_wrapper'] = array(
        '#prefix' => '<div id="replace_model_div">',
      );

      $form['model_wrapper']['model'] = array(
        '#type' => 'select',
        '#empty_option' => $this->t('Search Category'),
        '#options' => [],
        '#validated' => TRUE,
        '#ajax' => [
          'event' => 'change',
          'callback' => [$this, 'ajax_select_callback2'],
          'wrapper' => 'replace_sub_cat',
          'method' => 'replace',
        ]
      );
      $form['model_wrapper']['subCategory'] = array(
        '#type' => 'select',
        '#empty_option' => $this->t('Search Sub Category'),
        '#options' => [],
        '#default_value' => [],
        '#prefix' => '<div id="replace_sub_cat">',
        '#suffix' => '</div></div>',
        '#validated' => TRUE
      );
    }

    if($form_state->getValue('model')){
      $form['sub_cat_wrapper'] = array(
        '#prefix' => '<div id="replace_sub_cat">',
        '#suffix' => '</div>',
      );

      $sub_options = $this->_get_options_by_model($form_state->getValue('model'));
      $form['sub_cat_wrapper']['subCategory'] = array(
        '#type' => 'select',
        '#empty_option' => $this->t('Search Sub-Category'),
        '#options' => $sub_options,
        '#validated' => TRUE
      );
    }


    $form['#cache'] = ['max-age' => 0];
    $form['#theme'] = 'product_resource_form';

    return $form;

  }

  public function getCollections($category = '') {
    $query = $this->entityTypeManager->getStorage('taxonomy_term')->getQuery();
    $query->condition('vid', 'collections');
    if ( $category != '' ) {
      if ( $category == 'bathroom') {
        $query->condition('field_bath_facet_hide', 0);
      }else{
        $query->condition('field_facet_hide', 0);
      }
      $query->condition('field_collection_category', $category);
      $query->sort('name')->addTag('distinct');
    }
    $collection = [];
    $entity_ids = $query->execute();
    foreach ($entity_ids as $term_id) {
      $term = $this->entityTypeManager->getStorage("taxonomy_term")->load($term_id);
      $finishOptions = $term->get('field_available_finishes')->getValue();
      $collection[$term->getName()] = $term->getName();
    }
    return $collection;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Can add validation if required.
  }

  function ajax_select_callback($form, $form_state) {
    return $form['model_wrapper'];
  }

  function ajax_select_callback2($form, $form_state) {
    return $form['sub_cat_wrapper'];
  }

  function _get_options_by_room($room){
    if(empty($room)){
      return [];
    }
    return $this->productResourceService->getCategory($room);
  }

  function _get_options_by_model($room){
    if(empty($room)){
      return [];
    }
    return $this->productResourceService->getSubCategory($room);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $post = $form_state->getUserInput();
    $filterKey = '';
    $filtersArr = [];
    $searchPattern = Xss::filter($post['model_or_keyword']);
    $collection = Xss::filter($post['collection']);
    $path = \Drupal::service('path_alias.manager')
      ->getPathByAlias('/design-resources/technical-documents');
    $element = $form_state->getTriggeringElement()['#attributes'];
    $action = $element['id'];


    if(preg_match('/node\/(\d+)/', $path, $matches)) {

      $filtersArr['node'] = $matches[1];
      switch ($action){
        case 'by_keyword':
          if($searchPattern){
            $filtersArr['searchTerm'] = Xss::filter($searchPattern);
          }
          break;
        case 'by_collection':
          if($collection){
            $filtersArr['collection'] = Xss::filter($collection);
          }
          break;
        case 'by_category':
          if($post['room']) {
            $filtersArr['room'] = $post['room'];
          }
          if($post['model']) {
            $filtersArr['model'] = $post['model'];
          }
          if($post['subCategory']) {
            $filtersArr['subCategory'] = $post['subCategory'];
          }
          break;
      }


      $url = Url::fromRoute(
        'entity.node.canonical',
        $filtersArr
      );
      $form_state->setRedirectUrl($url);
    }

  }


}

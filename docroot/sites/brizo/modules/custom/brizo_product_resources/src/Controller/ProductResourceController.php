<?php

namespace Drupal\brizo_product_resources\Controller;

use Drupal\brizo_pdp\ProductDetails;
use Drupal\brizo_product_resources\ProductResourcesService;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\product_sync\ProductImport;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Implementation of search controller.
 */
class ProductResourceController extends ControllerBase {

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * To get current query string.
   *
   * @var Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Drupal\brizo_product_resources\ProductResourcesService definition.
   *
   * @var \Drupal\brizo_product_resources\ProductResourcesService
   */
  protected $productResourceService;

  /**
   * Drupal \brizo_pdp\ProductDetails definition.
   *
   * @var \Drupal\brizo_pdp\ProductDetails
   */
  protected $productDetails;

  /**
   * Drupal \brizo_pdp\ProductDetails definition.
   *
   * @var \Drupal\product_sync\ProductImport
   */
  protected $productImport;

  /**
   * Implementation of constructor.
   */
  public function __construct(RequestStack $requestStack,
                              ConfigFactoryInterface $configFactory,
                              ProductResourcesService $productResourceService,
                              ProductDetails $productDetails,
                              ProductImport $productImport) {
    $this->requestStack = $requestStack;
    $this->productResourceService = $productResourceService;
    $this->productDetails = $productDetails;
    $this->productImport = $productImport;
    $this->configFactory = $configFactory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
    // Load the service required to construct this class.
      $container->get('request_stack'),
      $container->get("config.factory"),
      $container->get('brizo_product_resources.service'),
      $container->get('brizo_pdp.details'),
      $container->get('product_sync.import')
    );
  }

  /**
   * Implementation of product spec search results.
   */
  public function searchSpec() {
    $request = $this->requestStack->getCurrentRequest();
    $query = $request->get('searchTerm');
    $collection = $request->get('collection');
    $categories = $request->get('categories');

    if($query != 'undefined'){
      $data = $this->productResourceService->getResource(
        $query,
        'spec',
        'searchTerm'
      );
    }elseif($collection != 'undefined' ){
      $data = $this->productResourceService->getResource(
        $collection,
        'spec',
        'collection'
      );
    }else{
      $data = $this->productResourceService->getResource(
        $categories,
        'spec',
        'categories'
      );
    }
    return $data;
  }

  /**
   * Implementation of product CAD search results.
   */
  public function searchCad() {
    $request = $this->requestStack->getCurrentRequest();
    $query = $request->get('searchTerm');
    $collection = $request->get('collection');
    $categories = $request->get('categories');
    if($query != 'undefined'){
      $data = $this->productResourceService->getResource(
        $query,
        'cad',
        'searchTerm'
      );
    }elseif($collection != 'undefined' ){
      $data = $this->productResourceService->getResource(
        $collection,
        'cad',
        'collection'
      );
    }else{
      $data = $this->productResourceService->getResource(
        $categories,
        'cad',
        'categories'
      );
    }
    return $data;
  }

  /**
   * Implementation of product searchImage results.
   */
  public function searchImage() {
    $request = $this->requestStack->getCurrentRequest();
    $query = $request->get('searchTerm');
    $collection = $request->get('collection');
    $categories = $request->get('categories');

    if($query != 'undefined'){
      $data = $this->productResourceService->getResource(
        $query,
        'image',
        'searchTerm'
      );
    }elseif($collection != 'undefined' ){
      $data = $this->productResourceService->getResource(
        $collection,
        'image',
        'collection'
      );
    }else{
      $data = $this->productResourceService->getResource(
        $categories,
        'image',
        'categories'
      );
    }
    return $data;
  }

  public function getImages($sku){
    $config = $this->configFactory->get('delta_services.deltaservicesconfig');
    $siteName = $config->get('global_site_name');
    $productId = key($this->productImport->productExists($sku));
    if (!empty($productId)) {
      $product = $this->productDetails->getProductValues($sku);
      $output['collection'] = (isset($product->values->Collection))?
        $product->values->Collection:$product->values->defaultCollection;
      $slider_image = $this->productDetails->getproductSliderImages($product);
      $output['assets'] = $slider_image['assets'];
      $fp_details = [
        'hero_img' => $this->productDetails->getProductHeroImage($productId),
        'title' => $this->productDetails->getProductTitle($productId),
      ];

    }else{
      $data = $this->productImport->isValidProductSKU($sku);
      if ($data['status'] == 1) {
        $productId = $this->productImport->createProduct(
          $data['productData'],
          $siteName
        );
        if (!empty($productId)) {
          $product = $this->productDetails->getProductValues($sku);
          $slider_image = $this->productDetails->getproductSliderImages($product);
          $output['assets'] = $slider_image['assets'];
          $fp_details = [
            'hero_img' => $this->productDetails->getProductHeroImage($productId),
            'title' => $this->productDetails->getProductTitle($productId)
          ];
        }
      }
    }
    $output['product'] = $fp_details;
    $build['output'] = [
      '#theme' => 'product_resources_image_modal',
      '#data' => $output,
    ];
    return $build;

  }

}

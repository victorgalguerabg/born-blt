<?php

namespace Drupal\brizo_product_resources\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Component\Utility\Xss;

/**
 * Provides a 'ProductResourceResult' Block.
 *
 * @Block(
 *   id = "product_resource_result_block",
 *   admin_label = @Translation("Product Resource Result block"),
 *   category = @Translation("Product Resource Search"),
 * )
 */

class ProductResourcesResultBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Implementation of constructor.
   */
  public function __construct(array $configuration,
                              $plugin_id,
                              $plugin_definition,
                              ConfigFactoryInterface $configFactory){
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->configFactory = $configFactory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container,
                                array $configuration,
                                $plugin_id,
                                $plugin_definition) {
    // Instantiates this form class.
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get("config.factory")
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    //$searchKey = \Drupal::request()->query->all();
    $searchKey = Xss::filter(\Drupal::request()->query->all());
    $category = [];
    $config = $this->configFactory->get('global_sync.api_settings');
    $limit = $config->get('product_resources_per_page');

    if(array_key_exists('room', $searchKey)){
      $category[] = $searchKey['room'];
      $find = ['Brizo_', '_'];
      $replace = ['', ' '];

      $searchKey['queryCategory'] = str_replace(
        $find,
        $replace,
        $searchKey['room']
      );
    }
    if(array_key_exists('model', $searchKey)){
      $category[] = $searchKey['model'];
      $find = ['Brizo_', '_'];
      $replace = ['', ' '];

      $searchKey['queryCategory'] = str_replace(
        $find,
        $replace,
        $searchKey['model']
      );
    }
    if(array_key_exists('subCategory', $searchKey)){
      $category[] = $searchKey['subCategory'];
      $find = ['Brizo_', '_'];
      $replace = ['', ' '];

      $searchKey['queryCategory'] = str_replace(
        $find,
        $replace,
        $searchKey['subCategory']
      );
    }
    if(count($category) > 0){
      $categories = '';
      foreach ($category as $str){
        $categories .= '&and_categories=' . rawurlencode($str);
      }
      $searchKey['categories'] = $categories;
    }

    $render = [
      '#data' => $searchKey,
      '#theme' => 'product_resources_results',
      '#cache' => [
        'max-age' => 0,
      ]

    ];

    if($searchKey){
      $render['#attached'] = [
        'library' =>
          [
            'brizo_product_resources/searchResult',
          ],
        'drupalSettings' => [
          'searchParams' => $searchKey,
          'pageLimit' => $limit
        ]
      ];
    }

    return $render;
  }

}

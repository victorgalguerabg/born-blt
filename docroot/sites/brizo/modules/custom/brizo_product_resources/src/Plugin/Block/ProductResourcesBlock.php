<?php

namespace Drupal\brizo_product_resources\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'ProductResource' Block.
 *
 * @Block(
 *   id = "product_resource_block",
 *   admin_label = @Translation("Product Resource Search block"),
 *   category = @Translation("Product Resource Search"),
 * )
 */
class ProductResourcesBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $form = \Drupal::formBuilder()->getForm('\Drupal\brizo_product_resources\Form\ProductResourcesForm');
    return $form;
  }

}

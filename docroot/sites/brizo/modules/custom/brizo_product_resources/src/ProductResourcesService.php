<?php

namespace Drupal\brizo_product_resources;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Drupal\delta_services\DeltaService;
use Symfony\Component\HttpFoundation\RequestStack;

class ProductResourcesService {

  /**
   * Implementation of EntityTypeManagerInterface.
   *
   * @var Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entitytypemanager;

  /**
   * To handle response.
   *
   * @var response
   */
  public $response;

  /**
   * Drupal\delta_services\DeltaService definition.
   *
   * @var \Drupal\delta_services\DeltaService
   */
  protected $deltaservice;

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;


  /**
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  public function __construct(DeltaService $deltaservice,
                              ConfigFactoryInterface $configFactory,
                              RequestStack $requestStack,
                              EntityTypeManagerInterface $entitytypemanager) {
    $this->deltaservice = $deltaservice;
    $this->configFactory = $configFactory;
    $this->requestStack = $requestStack;
    $this->entitytypemanager = $entitytypemanager;
    $this->response = new Response();
  }

  /**
   * Implementation of search output.
   */
  public function getResource($query = NULL, $type = NULL, $searchBy = NULL) {
    $pageNum = 0;
    if (!empty($this->requestStack->getCurrentRequest()->get('page'))) {
      $pageNum = $this->requestStack->getCurrentRequest()->get('page');
    }
    $config = $this->configFactory->get('global_sync.api_settings');
    $apiBaseDetails = $this->configFactory
      ->get('delta_services.deltaservicesconfig');
    $domain = $apiBaseDetails->get('api_base_path');
    $pagerSize = $config->get('product_resources_per_page');
    $websiteText = $config->get('website');
    $productSearchApi = str_replace(
      '{website}',
      $websiteText,
      $config->get('product_resources_api')
    );

    if($searchBy == 'searchTerm' || $searchBy == 'categories'){
      $searchApiUrl = $domain . $productSearchApi . $query;
    }elseif($searchBy == 'collection'){
      $searchApiUrl = $domain . $productSearchApi . '&collections=' . $query;
    }

    $includeObsolete = '&includeObsolete=true';
    if($type == 'spec'){
      $assetType =  '&or_assets.type=MandI&or_assets.type=PartsDiagram&or_assets.type=SpecSheet';
    }elseif($type == 'cad'){
      $assetType =  '&or_assets.type=CAD';
    }elseif($type == 'image'){
      $assetType = '&or_assets.type=InContextShot&or_assets.type=OnWhiteShot';
    }
    $url = $searchApiUrl . $this->searchKey . $includeObsolete .$assetType;
    $url .= '&page=' . $pageNum . '&size=' . $pagerSize;

    $productRequest = $this->deltaservice->apiCall(
      $url, "GET", [], "", "Search"
    );

    $this->response->setContent($productRequest['response']);
    $this->response->headers->set('Content-Type', 'application/json');
    return $this->response;

  }

  public function getCategory($room = NULL){
    $options = [];
    $category = '';
    switch ($room){
      case 'Brizo_Bath':
        $category = 'bathroom';
        break;
      case 'Brizo_Kitchen':
        $category = 'kitchen';
        break;
      case 'Brizo_Parts':
        $category = 'parts';
        break;
    }
    $terms =$this->entitytypemanager
      ->getStorage('taxonomy_term')
      ->loadByProperties(
        [
          'field_collection_category' => $category,
          'vid' => 'product_resource_categories'
        ]
      );
    foreach ($terms as $term){
      $term = $term->toArray();
      $optionName = $term['name'][0]['value'];
      $optionValue = $term['field_parameter_value'][0]['value'];
      $options[$optionValue] = $optionName;
    }
    return $options;
  }

  public function getSubCategory($category = NULL){

    $name = $category . '_%';
    $options = [];
    $result = \Drupal::entityQuery('taxonomy_term')
      ->condition('name', $name, 'LIKE')
      ->condition('vid', 'product_category')
      ->execute();
    if(count($result) > 0){
      $terms = $this->entitytypemanager
        ->getStorage('taxonomy_term')
        ->loadMultiple($result);

      foreach ($terms as $term){
        $raw = explode('_',$term->getName());
        $optionname = $term->getName();
        $options[$optionname] = $raw[count($raw)-1];
      }
    }

    return $options;
  }

  public function getCollections($category= NULL){
    $terms = $term_data = [];
    $terms =$this->entitytypemanager
      ->getStorage('taxonomy_term')
      ->loadByProperties(
        [
          'field_collection_category' => $category,
          'vid' => 'product_resource_collections',
          'field_facet_hide' => 0
        ]
      );
    foreach ($terms as $term) {
      $optionValue = '';
      $term = $term->toArray();
      $termName = $optionName = $term['name'][0]['value'];
      if(strpos($termName, '+') !== false){
        $optionName = str_replace('+', ' ',$termName);
      }
      $term_data[$termName] = $optionName;
    }
    return $term_data;

  }


}

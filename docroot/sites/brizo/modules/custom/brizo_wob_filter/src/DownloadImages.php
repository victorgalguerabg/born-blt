<?php

namespace Drupal\brizo_wob_filter;

use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Path\AliasManagerInterface;
use Drupal\Core\Url;
use Drupal\delta_services\DeltaService;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\product_sync\ProductImport;
use Drupal\taxonomy\Entity\Term;
use Symfony\Component\HttpFoundation\Request;
use Drupal\delta_cache\DeltaCacheService;
use Drupal\Core\Form\FormBuilderInterface;
use \Symfony\Component\HttpFoundation\Response;
use \Symfony\Component\HttpFoundation\Cookie;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Drupal\delta_custom\GlobalProduct;
use Drupal\Core\File\FileSystemInterface;
use Drupal\file\Entity\File;



/**
 * Provides service for Product Description Page.
 */
class DownloadImages {

  /**
   * Drupal\Core\Logger\LoggerChannelFactoryInterface definition.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */

  protected $loggerChannelFactory;


  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Drupal\Core\Path\AliasStorageInterface definition.
   *
   * @var \Drupal\Core\Path\AliasStorageInterface
   */
  protected $pathAliasStorage;

  /**
   *
   * @param \Drupal\product_sync\ProductImport $productImport
   *   The deltaservice to make API calls.
   */

   protected $productImport;


   /**
    * The current route match.
    *
    * @var \Drupal\delta_cache\DeltaCacheService
    */
   protected $deltaCache;

  /**
   * formBuilder.   *
   */

  protected $formBuilder;

  /**
    * Drupal\Core\TempStore\PrivateTempStoreFactory definition.
    *
    * @var \Drupal\Core\TempStore\PrivateTempStoreFactory
    */
   private $tempStoreFactory;


  /**
   * Drupal\global_product\GlobalProduct definition.
   *
   * @var \Drupal\global_product\GlobalProduct
  */
  protected $globalproduct;

  /**
   * {@inheritdoc}
   */

  public function __construct(DeltaService $deltaservice, ConfigFactoryInterface $config_factory, LoggerChannelFactoryInterface $loggerChannel, EntityTypeManagerInterface $entity_type_manager, ProductImport $product_import, DeltaCacheService $deltaCache, FormBuilderInterface $form_builder, PrivateTempStoreFactory $tempStoreFactory, GlobalProduct $globalproduct) {
    $this->deltaservice = $deltaservice;
    $this->configFactory = $config_factory;
    $this->loggerChannelFactory = $loggerChannel;
    $this->entityTypeManager = $entity_type_manager;
    $this->pathAliasStorage = $this->entityTypeManager->getStorage('path_alias');
    $this->productImport = $product_import;
    $this->deltaCache = $deltaCache;
    $this->formBuilder = $form_builder;
    $this->tempStoreFactory = $tempStoreFactory;
    $this->globalproduct = $globalproduct;
  }


  /**
   * {@inheritdoc}
   */


  public function DownloadImagesPage() {

     // Download Handler Form.
     $form = $this->formBuilder->getForm('Drupal\brizo_wob_filter\Form\DownloadImagesHandler');

     // recently Viewed products

      $build['output'] = [
         '#theme' => 'commerce_product',
         '#title' => "Product",
         '#download_form' => $form,
       ];
     return $build;

   }

  }

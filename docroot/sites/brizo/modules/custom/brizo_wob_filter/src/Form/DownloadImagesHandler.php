<?php

namespace Drupal\brizo_wob_filter\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpFoundation\Response;


/**
 * Implements an example form.
 */
class DownloadImagesHandler extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'download_handler_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['downlod_file_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Source URL'),
      '#maxlength' => 450,
    ];
    $form['downlod_file_type'] = [
      '#type' => 'textfield',
      '#title' => $this->t('File Type'),
    ];
    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#button_type' => 'primary',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $url = $form_state->getValue('downlod_file_url');
    set_time_limit(0);
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    $r = curl_exec($ch);
    curl_close($ch);
    $response = new Response($r);
    $response->headers->set("Expires", "0");
    $response->headers->set("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
    $response->headers->set("Last-Modified", gmdate('D, d M Y H:i:s', time()) . ' GMT');
    $response->headers->set("Cache-Control", "private, false");
    //Use the switch-generated Content-Type
    $response->headers->set("Content-Type", "application/force-download");
    $response->headers->set("Content-Disposition"," attachment; filename=" . basename($url));
    //Force the download
    $response->headers->set("Content-Transfer-Encoding", "binary");
    $response->headers->set("Content-Length", strlen($r));
    $response->headers->set("Connection", "close");
    $form_state->setResponse($response);
    $form_state->setRebuild(true);
  }

}

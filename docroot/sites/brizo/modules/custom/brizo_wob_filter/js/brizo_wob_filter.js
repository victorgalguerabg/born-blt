(function ($) {

  $(function() {
    $(window).scroll(function () {
        // Get the navbar
        var wobTabs = document.querySelector(".wob-filtered-view form");
        var wobTabsBar = document.querySelector(".wob-tabs");
        var mobileBarSpace = document.querySelector(".space-mobile");
        var header = document.getElementsByClassName("header");
        // Get the offset position of the navbar
        var sticky = wobTabs.offsetTop;
        var heightHeader = header[0].clientHeight;
        var windowWidth = $(window).width();
        wobTabs.style.top = heightHeader+'px';
        var adminBar  = $("#toolbar-administration")[0];
        if(adminBar && windowWidth >= 671){
          wobTabs.style.top = (adminBar.offsetTop + adminBar.clientHeight) + 'px';
        }
        var stickyClass = "mobile-sticky";
        if(adminBar){
          stickyClass = "admin-sticky-bar";
        }
        var barDefaultPosition = $('.wob-filtered-view').offset().top - $(wobTabs)[0].clientHeight;
        var pageScroll = $(this).scrollTop();
        if(windowWidth <= 670 && pageScroll > barDefaultPosition ){
          wobTabs.classList.add(stickyClass);
          $(mobileBarSpace).show();
        }else{
          wobTabs.classList.remove(stickyClass);
          $(mobileBarSpace).hide();
        }
        if(windowWidth >= 670 && pageScroll > barDefaultPosition ){
          wobTabsBar.classList.add('remove-padding');
        }else{
          wobTabsBar.classList.remove('remove-padding');
        }
        // Add the sticky class to the navbar when you reach its scroll position. Remove "sticky" when you leave the scroll position
        if (pageScroll < 2) {
          wobTabs.classList.remove("sticky");
        } else {
          wobTabs.classList.add("sticky");
        }
    });
  });

  var clickType = 'normal';

  /**
  * Set active class on Views AJAX filter 
  * on selected category
  */
  Drupal.behaviors.exposedfilter_buttons = {
    attach: function(context, settings) {

      $('#views-exposed-form-wob-filtered-view-block-1 select[name="field_wob_category_target_id"]').on('change', function() {
        var $form = $(this).closest('form');
        var id = $form.find(":selected").val();
        if(typeof id == 'undefined'){
          id = 'All';
        }
        $(this).val(id);
        $form.find('input[type=submit]').click();

        // Unset and then set the active class
        $('.filter-tab a').removeClass('active');
        // Set the new value in the SELECT element
        $('#'+id).addClass('active');
        setTimeout(function(){
          var filter_titles = $('#views-exposed-form-world-of-brizo-category-titles-block-1 select[name="tid"]');
          filter_titles.prop("required", false);
          filter_titles.val(id);
          clickType = 'normal';
          $('#views-exposed-form-world-of-brizo-category-titles-block-1 select[name="tid"]').trigger('change');
          $('#views-exposed-form-world-of-brizo-category-titles-block-1 input.form-submit').trigger('click');
          imageSize();
        }, 250);
        /*
        if($('.banner').length){
          if(id == 'All'){
            $('.banner').show();
          }else{
            $('.banner').hide();
          }
        }*/
      });

      $('.filter-tab').on('click', function(e) {
        e.preventDefault();
        // Get ID of clicked item
        var id = $(e.target).attr('id');
        if(id == 'All' && window.location.pathname != '/world-of-brizo') {
          window.open("/world-of-brizo", "_self");
        }
        var filter = $('#views-exposed-form-wob-filtered-view-block-1 select[name="field_wob_category_target_id"]');
        filter.val(id);
        clickType = 'normal';
        $('#views-exposed-form-wob-filtered-view-block-1 select[name="field_wob_category_target_id"]').trigger('change');
        $('#views-exposed-form-wob-filtered-view-block-1 input.form-submit').trigger('click');
      });

      if (typeof context['location'] !== 'undefined') { // Only fire on document load.

        const queryString = window.location.search;
        const urlParams = new URLSearchParams(queryString);
        const queryPathString = window.location.pathname;

        if(queryPathString.includes('/world-of-brizo/')){
          var category = queryPathString.replace("/world-of-brizo/", '');
        }else{
          var category = urlParams.get('category');
        }

        var id = $('.filter-tab').find(".active").attr("id");
        if(typeof id == 'undefined'){
          id = 'All';
        }

        var filter = $('#views-exposed-form-wob-filtered-view-block-1 select[name="field_wob_category_target_id"]');
        filter.find("option").each(function(){
          // Add $(this).val() to your list
          // Validate category in dropdown options
          var optionCategory = $(this).text().toLowerCase().split(' ').join('-');
          if(category == optionCategory){
            let stateUri = '/world-of-brizo/' + category;
            let newUrl = location.protocol + '//' + location.host + stateUri;
            id = $(this).attr("value");
            window.history.replaceState('', '', newUrl);
            //window.location.replace(newUrl);
          }
        });
        filter.val(id);
        clickType = 'normal';
        $('#views-exposed-form-wob-filtered-view-block-1 select[name="field_wob_category_target_id"]').trigger('change');
        $('#views-exposed-form-wob-filtered-view-block-1 input.form-submit').trigger('click');
      }

      // Navigation by url's logged user and default load in /world-of-brizo
      if(typeof context['location'] === 'undefined'){
        var adminBar  = $("#toolbar-administration")[0];
        if(adminBar ){
          var id = $('.filter-tab').find(".active").attr("id");
          if(typeof id == 'undefined'){
            const queryString = window.location.search;
            const urlParams = new URLSearchParams(queryString);
            const queryPathString = window.location.pathname;

            var category = '';
            if(queryPathString.includes('/world-of-brizo/')){
              var category = queryPathString.replace("/world-of-brizo/", '');
            }

            setTimeout(function(){
              var id = $('.filter-tab').find(".active").attr("id");
              $('.filter-tab').each(function() {
                var getCategory = $( this ).find('a').text().toLowerCase().replace(' ', "-");
                if(getCategory == category){
                  id = $( this ).find('a').attr("id");
                }
              });
              clickType = 'normal';
              $( "#"+id ).click();
            }, 100);
          }
        }
      }

      // Assign height to img
      function imageSize() {
        var images = $('.wob-row').find('img');
        var imageHeight = $('.wob-row').find('img')[0].clientHeight;
        for(var i = 0; i < images.length; i++){
          images[i].style.height = imageHeight + "px";
        }
      }
      $(window).on('resize', function(){
        resizeBanner();
      });

      function resizeBanner(){
        if($('.world-of-brizo-page')){
          var windowLoadedWidth = $(window).width();
          var wobBannerClass = $('.wob-banner');
          if(wobBannerClass){
            if(windowLoadedWidth >= 1600){
              windowLoadedWidth = 1600;
            }
            $('.wob-banner').width(windowLoadedWidth);
          }

          var wobBannerClass = $('.wob-extended-line');
          if(wobBannerClass){
            if(windowLoadedWidth >= 1600){
              windowLoadedWidth = 1600;
            }
            $('.wob-extended-line').width(windowLoadedWidth);
          }
        }
      }
      resizeBanner();
    }
  };

  Drupal.behaviors.viewsAjax = {
    attach: function (context, settings) {
        // Attach action to Drupal Ajax Views event
        $(document).once('#views-exposed-form-wob-filtered-view-block-1', context).ajaxSuccess(function (event, data) {
            event.preventDefault();
            if ($('#views-exposed-form-wob-filtered-view-block-1').length) {
              let category = $('#views-exposed-form-wob-filtered-view-block-1 select[name="field_wob_category_target_id"] > option:selected').text().toLowerCase();
              let newCategory = category.replace(" ","-");
              let wobUrl = '/world-of-brizo';
              let stateUrl = wobUrl+ '/' + newCategory;
              if(newCategory == 'all'){
                stateUrl = wobUrl;
              }
              var newurl = window.location.protocol + "//" + window.location.host + stateUrl ;
              if(data.responseJSON){
                if(data.responseJSON[0]){
                var newSettings = data.responseJSON[0].settings;
                if(newSettings.views){
                  var newAjaxViews = newSettings.views.ajaxViews;
                  let keys = Object.keys(newAjaxViews)[0];
                  if (newAjaxViews[keys].view_name == "wob_filtered_view" && clickType == 'normal'){
                    history.pushState({path:newurl}, newCategory.toUpperCase(), newurl);
                  }
                }
              }
            }
          }
        });
    }
  }

  $(document).ajaxComplete(function(event, xhr, settings) {
    if(typeof settings.extraData !== 'undefined'){
      if(typeof settings.extraData.view_name !== 'undefined'){
        switch(settings.extraData.view_name){
          case "wob_filtered_view":
            var filter_id = $('#views-exposed-form-wob-filtered-view-block-1 select[name="field_wob_category_target_id"]').find(":selected").val();
            $('.filter-tab a').removeClass('active');
            $('.filter-tab').find('#' + filter_id).addClass('active');
          break;
          case "world_of_brizo_category_titles":
            event.preventDefault();
            if($('.wob-titles')){
              var id = $('.filter-tab').find(".active").attr("id");
              $('.wob-titles').find('.wob-all-articles').remove();
              /* if(id == 'All'){
                $('.wob-titles').append("<div class='wob-all-articles views-row'><h2>All Articles</h2></div>");
              } */
            }
          break;
        default:
        break;
        };
      }
    }
  });

  /*Resize image for Featured Stories - same height in different grid columns*/
  if($('.wob-featured-stories').length){
    var image;
    $(".wob-featured-stories .views-field-field-wob-teaser-image-narrow").each(function () {
      image = $(this).find("img");
    });

    $(document).ready(
      resizeimage(image)
    );

    $(window).on('resize', function(){
      resizeimage(image);
    });

    function resizeimage(image){
      if(image.height() > 0) {
        $(".wob-featured-stories .views-field-field-wob-teaser-image-wide").each(function () {
          $(this).find("img").css("max-height", image.height() + "px");
        });
      }
    }
  }
  /*Resize image for Featured Stories - same height in different grid columns*/

  function ajaxNavigation(queryPathString){
    if(queryPathString.includes('/world-of-brizo')){
      var category = queryPathString.replace(location.protocol + '//' + location.host+"/world-of-brizo/", '');
      setTimeout(function(){
      var id = $('.filter-tab').find(".active").attr("id");
      if(queryPathString == (location.protocol + '//' + location.host+"/world-of-brizo")){
        id = 'All';
      }
      var filter = $('#views-exposed-form-wob-filtered-view-block-1 select[name="field_wob_category_target_id"]');
      filter.find("option").each(function(){
        var optionCategory = $(this).text().toLowerCase().split(' ').join('-');
        if(category == optionCategory){
          let stateUri = '/world-of-brizo/' + category;
          let newUrl = location.protocol + '//' + location.host + stateUri;
          id = $(this).attr("value");
        }
      });
      filter.val(id);
      $('#views-exposed-form-wob-filtered-view-block-1 select[name="field_wob_category_target_id"]').trigger('change');
      $('#views-exposed-form-wob-filtered-view-block-1 input.form-submit').trigger('click');
      }, 250);
    }
  }

  window.addEventListener('popstate', function(event, state){
    event.preventDefault();
    clickType = 'history';
    if(state = event.state){
      ajaxNavigation(event.state.path);
    }else{
      history.back();
    }
  }, false);

})(jQuery);
<?php

namespace Drupal\general\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\file\Entity\File;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Class DeltaServicesConfigForm.
 */
class ContactusConfigForm extends ConfigFormBase  implements ContainerInjectionInterface{


  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

   /**
   * {@inheritdoc}
   */
  public function __construct(EntityTypeManagerInterface $entityManager) {
    $this->entityTypeManager = $entityManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'general.deltaservicesconfig',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'contact_us_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('general.deltaservicesconfig');
    $form['api_method'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Contact Us API Method'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('api_method'),
    ];
    $form['source_site'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Source Site'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('source_site'),
    ];
    $form['origin_email'] = [
      '#type' => 'email',
      '#title' => $this->t('Origin Email'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('origin_email'),
    ];
    $form['api_contact_us_autocomplete'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API base path AutoComplete'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('api_contact_us_autocomplete'),
    ];
    $form['api_authentication'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Authorization Key'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('api_authentication'),
    ];
    $form['image_finder_info'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('Product Register - Product finder image info popup'),
      '#default_value' => $config->get('image_finder_info'),
      '#required' => TRUE,
      '#size' => 100,
      '#upload_validators' => [
        'file_validate_extensions' => ['gif png jpg jpeg'],
      ],
      '#upload_location' => 'public://',
      '#attributes' => ['class' => ['photo-upload']],
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Can add validation if required.
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $getUserInput = $form_state->getUserInput();
    if (!empty($getUserInput['image_finder_info'])) {
      $image = $getUserInput['image_finder_info'];
      if (!empty($image['fids'])) {
        $file = $this->entityTypeManager->getStorage('file')->load($image['fids']);
        $file->setPermanent();
        $file->save();
      }
    }
    parent::submitForm($form, $form_state);

    $this->config('general.deltaservicesconfig')
      ->set('api_method', $form_state->getValue('api_method'))
      ->set('source_site', $form_state->getValue('source_site'))
      ->set('origin_email', $form_state->getValue('origin_email'))
      ->set('api_contact_us_autocomplete', $form_state->getValue('api_contact_us_autocomplete'))
      ->set('image_finder_info', $form_state->getValue('image_finder_info'))
      ->set('api_authentication', $form_state->getValue('api_authentication'))
      ->save();
  }

}

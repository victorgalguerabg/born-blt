<?php

namespace Drupal\carousel\Plugin\views\style;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\style\StylePluginBase;

/**
 * Style plugin to render each item into carousel.
 *
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *   id = "carousel",
 *   title = @Translation("Carousel"),
 *   help = @Translation("Displays rows as Carousel."),
 *   theme = "carousel_views",
 *   display_types = {"normal"}
 * )
 */
class Carousel extends StylePluginBase {
  /**
   * Does the style plugin allows to use style plugins.
   *
   * @var bool
   */
  protected $usesRowPlugin = TRUE;
  /**
   * Does the style plugin support custom css class for the rows.
   *
   * @var bool
   */
  protected $usesRowClass = TRUE;

  /**
   * Set default options.
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    $settings = _carousel_default_settings();
    foreach ($settings as $k => $v) {
      $options[$k] = ['default' => $v];
    }
    return $options;
  }

  /**
   * Render the given style.
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
    // Items.
    $form['items'] = [
      '#type' => 'number',
      '#title' => $this->t('Items'),
      '#description' => $this->t('Maximum amount of items displayed at a time with the widest browser width.'),
      '#default_value' => $this->options['items'],
    ];
    // Margin.
    $form['margin'] = [
      '#type' => 'number',
      '#title' => $this->t('Margin'),
      '#default_value' => $this->options['margin'],
      '#description' => $this->t('margin-right(px) on item.'),
    ];
    // Autoplay.
    $form['autoplay'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('AutoPlay'),
      '#default_value' => $this->options['autoplay'],
    ];
    // autoplayHoverPause.
    $form['autoplayHoverPause'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Pause On Hover'),
      '#default_value' => $this->options['autoplayHoverPause'],
      '#description' => $this->t('Pause autoplay on mouse hover.'),
    ];
    // autoplaySpeed.
    $form['autoplaySpeed'] = [
      '#type' => 'number',
      '#title' => $this->t('AutoPlay Speed'),
      '#default_value' => $this->options['autoplaySpeed'],
      '#description' => $this->t('AutoPlay speed in milliseconds.'),
    ];
    // autoplayTimeout.
    $form['autoplayTimeout'] = [
      '#type' => 'number',
      '#title' => $this->t('AutoPlay Timeout'),
      '#default_value' => $this->options['autoplayTimeout'],
      '#description' => $this->t('AutoPlay Timeout in milliseconds.'),
    ];
    // Navigation.
    $form['nav'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Navigation'),
      '#default_value' => $this->options['nav'],
      '#description' => $this->t('Display "next" and "prev" buttons.'),
    ];
    // Navigation Speed.
    $form['navSpeed'] = [
      '#type' => 'number',
      '#title' => $this->t('Navigation Speed'),
      '#default_value' => $this->options['navSpeed'],
      '#description' => $this->t('Navigation speed in milliseconds.'),
    ];
    // Loop.
    $form['loop'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Slide Loop'),
      '#default_value' => $this->options['loop'],
      '#description' => $this->t('Slide to first item.'),
    ];
    // navRewind.
    $form['navRewind'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Rewind'),
      '#default_value' => $this->options['navRewind'],
      '#description' => $this->t('Rewind to first item.'),
    ];
    // rewindSpeed.
    $form['rewindSpeed'] = [
      '#type' => 'number',
      '#title' => $this->t('Rewind Speed'),
      '#default_value' => $this->options['rewindSpeed'],
      '#description' => $this->t('Rewind speed in milliseconds.'),
    ];
    // Pagination.
    $form['dots'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Dots Pagination'),
      '#default_value' => $this->options['dots'],
      '#description' => $this->t('Show dots pagination.'),
    ];
    // paginationSpeed.
    $form['dotsSpeed'] = [
      '#type' => 'number',
      '#title' => $this->t('Dots Pagination Speed'),
      '#default_value' => $this->options['dotsSpeed'],
      '#description' => $this->t('Pagination speed in milliseconds.'),
    ];
    // slideBy.
    $form['slideBy'] = [
      '#type' => 'number',
      '#title' => $this->t('Slide By'),
      '#default_value' => $this->options['slideBy'],
      '#description' => $this->t('Slide by how many items.'),
    ];
    // mouseDrag.
    $form['mouseDrag'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Mouse Drag'),
      '#default_value' => $this->options['mouseDrag'],
      '#description' => $this->t('Turn off/on mouse events.'),
    ];
    // touchDrag.
    $form['touchDrag'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Touch Drag'),
      '#default_value' => $this->options['touchDrag'],
      '#description' => $this->t('Turn off/on touch events.'),
    ];
    // pullDrag.
    $form['pullDrag'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Pull Drag'),
      '#default_value' => $this->options['pullDrag'],
      '#description' => $this->t('Turn off/on pull events.'),
    ];
    // freeDrag.
    $form['freeDrag'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Free Drag'),
      '#default_value' => $this->options['freeDrag'],
      '#description' => $this->t('Turn off/on free events.'),
    ];
    // stagePadding.
    $form['stagePadding'] = [
      '#type' => 'number',
      '#title' => $this->t('Stage Padding'),
      '#default_value' => $this->options['stagePadding'],
      '#description' => $this->t('Stage padding in px.'),
    ];
    // Merge.
    $form['merge'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Merge'),
      '#default_value' => $this->options['merge'],
      '#description' => $this->t('Merge items.'),
    ];
    // mergeFit.
    $form['mergeFit'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Merge Fit'),
      '#default_value' => $this->options['mergeFit'],
      '#description' => $this->t('Merge fit.'),
    ];
    // Center.
    $form['center'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Center'),
      '#default_value' => $this->options['center'],
      '#description' => $this->t('center.'),
    ];
    // autoWidth.
    $form['autoWidth'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Auto Width'),
      '#default_value' => $this->options['autoWidth'],
      '#description' => $this->t('Auto width items.'),
    ];
    // Video.
    $form['video'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Video'),
      '#default_value' => $this->options['video'],
      '#description' => $this->t('Video items.'),
    ];
    // videoHeight.
    $form['videoHeight'] = [
      '#type' => 'number',
      '#title' => $this->t('Video Height'),
      '#default_value' => $this->options['videoHeight'],
      '#description' => $this->t('Video height in px.'),
    ];
    // videoWidth.
    $form['videoWidth'] = [
      '#type' => 'number',
      '#title' => $this->t('Video Width'),
      '#default_value' => $this->options['videoWidth'],
      '#description' => $this->t('Video width in px.'),
    ];
  }

}

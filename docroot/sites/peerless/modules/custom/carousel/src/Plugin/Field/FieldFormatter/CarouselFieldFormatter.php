<?php

namespace Drupal\carousel\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceFormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'carousel_field_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "carousel_field_formatter",
 *   label = @Translation("Carousel"),
 *   field_types = {
 *     "image"
 *   }
 * )
 */
class CarouselFieldFormatter extends EntityReferenceFormatterBase implements ContainerFactoryPluginInterface {

  /**
   * To current user dedection.
   *
   * @var currentUser
   */
  protected $currentUser;
  /**
   * For image style storage.
   *
   * @var imageStyleStorage
   */
  protected $imageStyleStorage;

  /**
   * Implementation of constructor.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, AccountInterface $current_user, EntityStorageInterface $image_style_storage) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->currentUser = $current_user;
    $this->imageStyleStorage = $image_style_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('current_user'),
      $container->get('entity_type.manager')->getStorage('image_style')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return _carousel_default_settings() + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {

    $image_styles = image_style_options(FALSE);
    $description_link = Link::fromTextAndUrl(
      $this->t('Configure Image Styles'),
      Url::fromRoute('entity.image_style.collection')
    );
    $element['image_style'] = [
      '#title' => $this->t('Image style'),
      '#type' => 'select',
      '#default_value' => $this->getSetting('image_style'),
      '#empty_option' => $this->t('None (original image)'),
      '#options' => $image_styles,
      '#description' => $description_link->toRenderable() + [
        '#access' => $this->currentUser->hasPermission('administer image styles'),
      ],
    ];
    $link_types = [
      'content' => $this->t('Content'),
      'file' => $this->t('File'),
    ];
    $element['image_link'] = [
      '#title' => $this->t('Link image to'),
      '#type' => 'select',
      '#default_value' => $this->getSetting('image_link'),
      '#empty_option' => $this->t('Nothing'),
      '#options' => $link_types,
    ];

    $element['items'] = [
      '#type' => 'number',
      '#title' => $this->t('Items'),
      '#description' => $this->t('Maximum amount of items displayed at a time with the widest browser width.'),
      '#default_value' => $this->getSetting('items'),
    ];

    // Margin.
    $element['margin'] = [
      '#type' => 'number',
      '#title' => $this->t('Margin'),
      '#default_value' => $this->getSetting('margin'),
      '#description' => $this->t('margin-right(px) on item.'),
    ];
    // Autoplay.
    $element['autoplay'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('AutoPlay'),
      '#default_value' => $this->getSetting('autoplay'),
    ];
    // autoplayHoverPause.
    $element['autoplayHoverPause'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Pause On Hover'),
      '#default_value' => $this->getSetting('autoplayHoverPause'),
      '#description' => $this->t('Pause autoplay on mouse hover.'),
    ];
    // autoplaySpeed.
    $element['autoplaySpeed'] = [
      '#type' => 'number',
      '#title' => $this->t('AutoPlay Speed'),
      '#default_value' => $this->getSetting('autoplaySpeed'),
      '#description' => $this->t('AutoPlay speed in milliseconds.'),
    ];
    // autoplayTimeout.
    $element['autoplayTimeout'] = [
      '#type' => 'number',
      '#title' => $this->t('AutoPlay Timeout'),
      '#default_value' => $this->getSetting('autoplayTimeout'),
      '#description' => $this->t('AutoPlay Timeout in milliseconds.'),
    ];
    // Navigation.
    $element['nav'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Navigation'),
      '#default_value' => $this->getSetting('nav'),
      '#description' => $this->t('Display "next" and "prev" buttons.'),
    ];
    // Navigation Speed.
    $element['navSpeed'] = [
      '#type' => 'number',
      '#title' => $this->t('Navigation Speed'),
      '#default_value' => $this->getSetting('navSpeed'),
      '#description' => $this->t('Navigation speed in milliseconds.'),
    ];
    // Loop.
    $element['loop'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Rewind Loop'),
      '#default_value' => $this->getSetting('loop'),
      '#description' => $this->t('Slide to first item.'),
    ];
    // navRewind.
    $element['navRewind'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Rewind'),
      '#default_value' => $this->getSetting('navRewind'),
      '#description' => $this->t('Rewind to first item.'),
    ];
    // rewindSpeed.
    $element['rewindSpeed'] = [
      '#type' => 'number',
      '#title' => $this->t('Rewind Speed'),
      '#default_value' => $this->getSetting('rewindSpeed'),
      '#description' => $this->t('Rewind speed in milliseconds.'),
    ];
    // Pagination.
    $element['dots'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('pagination'),
      '#default_value' => $this->getSetting('dots'),
      '#description' => $this->t('Show pagination.'),
    ];
    // paginationSpeed.
    $element['dotsSpeed'] = [
      '#type' => 'number',
      '#title' => $this->t('Pagination Speed'),
      '#default_value' => $this->getSetting('dotsSpeed'),
      '#description' => $this->t('Pagination speed in milliseconds.'),
    ];
    // slideBy.
    $element['slideBy'] = [
      '#type' => 'number',
      '#title' => $this->t('Slide By'),
      '#default_value' => $this->getSetting('slideBy'),
      '#description' => $this->t('Slide by how many items.'),
    ];
    // mouseDrag.
    $element['mouseDrag'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Mouse Drag'),
      '#default_value' => $this->getSetting('mouseDrag'),
      '#description' => $this->t('Turn off/on mouse events.'),
    ];
    // touchDrag.
    $element['touchDrag'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Touch Drag'),
      '#default_value' => $this->getSetting('touchDrag'),
      '#description' => $this->t('Turn off/on touch events.'),
    ];
    // pullDrag.
    $element['pullDrag'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Pull Drag'),
      '#default_value' => $this->getSetting('pullDrag'),
      '#description' => $this->t('Turn off/on pull events.'),
    ];
    // freeDrag.
    $element['freeDrag'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Free Drag'),
      '#default_value' => $this->getSetting('freeDrag'),
      '#description' => $this->t('Turn off/on free events.'),
    ];
    // stagePadding.
    $element['stagePadding'] = [
      '#type' => 'number',
      '#title' => $this->t('Stage Padding'),
      '#default_value' => $this->getSetting('stagePadding'),
      '#description' => $this->t('Stage padding in px.'),
    ];
    // Merge.
    $element['merge'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Merge'),
      '#default_value' => $this->getSetting('merge'),
      '#description' => $this->t('Merge items.'),
    ];
    // mergeFit.
    $element['mergeFit'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Merge Fit'),
      '#default_value' => $this->getSetting('mergeFit'),
      '#description' => $this->t('Merge fit.'),
    ];
    // Center.
    $element['center'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Center'),
      '#default_value' => $this->getSetting('center'),
      '#description' => $this->t('center.'),
    ];
    // autoWidth.
    $element['autoWidth'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Auto Width'),
      '#default_value' => $this->getSetting('autoWidth'),
      '#description' => $this->t('Auto width items.'),
    ];
    // Video.
    $element['video'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Video'),
      '#default_value' => $this->getSetting('video'),
      '#description' => $this->t('Video items.'),
    ];
    // videoHeight.
    $element['videoHeight'] = [
      '#type' => 'number',
      '#title' => $this->t('Video Height'),
      '#default_value' => $this->getSetting('videoHeight'),
      '#description' => $this->t('Video height in px.'),
    ];
    // videoWidth.
    $element['videoWidth'] = [
      '#type' => 'number',
      '#title' => $this->t('Video Width'),
      '#default_value' => $this->getSetting('videoWidth'),
      '#description' => $this->t('Video width in px.'),
    ];

    return $element + parent::settingsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    // Implement settings summary.
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {

    $elements = [];

    $files = $this->getEntitiesToView($items, $langcode);

    // Early opt-out if the field is empty.
    if (empty($files)) {
      return $elements;
    }

    $url = NULL;
    $image_link_setting = $this->getSetting('image_link');
    // Check if the formatter involves a link.
    if ($image_link_setting == 'content') {
      $entity = $items->getEntity();
      if (!$entity->isNew()) {
        $url = $entity->toUrl();
      }
    }
    else {
      if ($image_link_setting == 'file') {
        $link_file = TRUE;
      }
    }

    $image_style_setting = $this->getSetting('image_style');

    // Collect cache tags to be added for each item in the field.
    $cache_tags = [];
    if (!empty($image_style_setting)) {
      $image_style = $this->imageStyleStorage->load($image_style_setting);
      $cache_tags = $image_style->getCacheTags();
    }

    foreach ($files as $delta => $file) {
      if (isset($link_file)) {
        $image_uri = $file->getFileUri();
        $url = Url::fromUri(file_create_url($image_uri));
      }
      $cache_tags = Cache::mergeTags($cache_tags, $file->getCacheTags());

      // Extract field item attributes for the theme function, and unset them
      // from the $item so that the field template does not re-render them.
      $item = $file->_referringItem;
      $item_attributes = $item->_attributes;
      unset($item->_attributes);

      $elements[$delta] = [
        '#theme' => 'image_formatter',
        '#item' => $item,
        '#item_attributes' => $item_attributes,
        '#image_style' => $image_style_setting,
        '#url' => $url,
        '#cache' => [
          'tags' => $cache_tags,
        ],
      ];
    }

    $settings = _carousel_default_settings();
    foreach ($settings as $k => $v) {
      $s = $this->getSetting($k);
      $settings[$k] = isset($s) ? $s : $settings[$k];
    }
    return [
      '#theme' => 'carousel',
      '#items' => $elements,
      '#settings' => $settings,
      '#attached' => ['library' => ['carousel/owl-carousel']],
    ];

  }

  /**
   * Generate the output appropriate for one field item.
   *
   * @param \Drupal\Core\Field\FieldItemInterface $item
   *   One field item.
   *
   * @return string
   *   The textual output generated.
   */
  protected function viewValue(FieldItemInterface $item) {
    // The text value has no text format assigned to it, so the user input
    // should equal the output, including newlines.
    return nl2br(Html::escape($item->value));
  }

}

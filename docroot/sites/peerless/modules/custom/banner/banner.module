<?php

/**
 * @file
 * Implementation of banner.
 */

use Drupal\Core\Form\FormStateInterface;

/**
 * Implements hook_field_widget_form_alter().
 */
function banner_field_widget_form_alter(&$element, FormStateInterface $form_state, $context) {
  $field_definition = $context['items']->getFieldDefinition();
  $paragraph_entity_reference_field_name = $field_definition->getName();
  $paragraph_types = banner_get_paragraph_types();
  foreach ($paragraph_types as $type) {
    if ($type == 'banner') {
      // Add new fields to this list if any.
      $dependee_field_names = [
        'field_banner_title_type',
        'field_banner_title_text_type',
        'field_banner_description_type',
      ];
      foreach ($dependee_field_names as $dependee_field) {
        $selector = sprintf('select[name="%s[%d][subform][%s]"]', $paragraph_entity_reference_field_name, @$dependee_field['#delta'], $dependee_field);
        switch ($dependee_field) {
          // Banner title type field.
          case 'field_banner_title_type':
            $element['subform']['field_banner_title_text_type']['#states'] = [
              'visible' => [
                $selector => ['value' => '0'],
              ],
            ];
            $element['subform']['field_banner_title_image']['#states'] = [
              'visible' => [
                $selector => ['value' => '1'],
              ],
            ];
            $element['subform']['field_banner_mobile_title_image']['#states'] = [
              'visible' => [
                $selector => ['value' => '1'],
              ],
            ];
            break;

          // Banner title text type field.
          case 'field_banner_title_text_type':

            $element['subform']['field_banner_plain_title']['#states'] = [
              'visible' => [
                $selector => ['value' => '0'],
              ],
            ];
            $element['subform']['field_banner_formatted_title']['#states'] = [
              'visible' => [
                $selector => ['value' => '1'],
              ],
            ];
            break;

          // Banner description type field.
          case 'field_banner_description_type':

            $element['subform']['field_banner_plain_description']['#states'] = [
              'visible' => [
                $selector => ['value' => '0'],
              ],
            ];
            $element['subform']['field_banner_formatted_desc']['#states'] = [
              'visible' => [
                $selector => ['value' => '1'],
              ],
            ];
            break;
        }
      }
    }
  }
}

/**
 * Implements hook_theme().
 */
function banner_theme() {
  $theme_templates = [];
  $type = 'banner';
  // Register custom Paragraph bundle templates.
  $theme_templates['paragraph__' . $type] = [
    'base hook' => 'paragraph',
  ];
  return $theme_templates;
}

/**
 * Implements hook_theme_registry_alter().
 *
 * Alters the theme suggestions to pick template from custom module.
 */
function banner_theme_registry_alter(&$theme_registry) {
  $module_path = drupal_get_path('module', 'banner');
  $template_objects = drupal_find_theme_templates($theme_registry, '.html.twig', $module_path);
  $type = 'banner';
  $template = 'paragraph__' . $type;
  if (!isset($template_objects[$template])) {
    unset($theme_registry['paragraph__' . $type]);
  }
  else {
    $theme_registry['paragraph__' . $type]['path'] = $template_objects[$template]['path'];
  }
}

/**
 * Helper function to get a list of paragraph types by machine name.
 */
function banner_get_paragraph_types() {
  $paragraph_bundles = \Drupal::service('entity_type.bundle.info')->getBundleInfo('paragraph');
  return array_keys($paragraph_bundles);
}

<?php

namespace Drupal\support\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

/**
 * Implementation of search controller.
 */
class SearchOutputController extends ControllerBase {
  public $searchKey;

  public $response;

  public $pageNum;

  /**
   * Implementation of constructor.
   */
  public function __construct() {
    $request = new Request();
    $this->searchKey = $request->get('title');
    $this->response = new Response();
    $this->pageNum = 0;
    if (!empty($request->get('page'))) {
      $this->pageNum = $request->get('page');
    }
  }

  /**
   * Implementation of search output.
   */
  public function getSearcssshOutput() {
    $config = $this->config('sync.api_settings');
    $configPrimary = $this->config('delta_services.deltaservicesconfig');
    $domain = $configPrimary->get('api_base_path');
    $searchProductsUrl = $domain . $config->get('sync.global_search_products');
    $searchProductsUrl = str_replace("{searchKey}", $this->searchKey, $searchProductsUrl);
    $url = str_replace("{pageNum}", $this->pageNum, $searchProductsUrl);
    $productRequest = web_services_api_call($url, "GET", [], "", "Search");

    if ($productRequest['status'] == 'SUCCESS') {
      $this->response->setContent($productRequest['response']);
      $this->response->headers->set('Content-Type', 'application/json');
      return $this->response;
    }

  }

  /**
   * Implementation of search document output.
   */
  public function getDocumentSearchOutput() {
    $config = $this->config('sync.api_settings');
    $documentSearchApi = $config->get('sync.product_document_search');
    $url = $documentSearchApi . $this->searchKey . '&size=20&brand=peerless&from=' . $this->pageNum;
    $documentRequest = web_services_api_call($url, "GET", [], "", "Search");

    if ($documentRequest['status'] == 'SUCCESS') {
      $this->response->setContent($documentRequest['response']);
      $this->response->headers->set('Content-Type', 'application/json');
      return $this->response;
    }

  }

}

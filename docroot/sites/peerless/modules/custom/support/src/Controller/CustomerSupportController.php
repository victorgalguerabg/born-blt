<?php

namespace Drupal\support\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Request;

/**
 * Implementation of support controller.
 */
class CustomerSupportController extends ControllerBase {

  protected $pageNum;

  protected $searchKey;

  protected $request;

  /**
   * Implementation of support results.
   */
  public function getSupportResults(Request $request) {
    $searchKey = $request->get('q');
    $pageNum = 0;
    if (!empty($request->get('page'))) {
      $pageNum = $request->get('page');
    }
    $config = $this->config('sync.api_settings');
    $documentSearchApi = $config->get('sync.product_document_search');
    $url = $documentSearchApi . $searchKey . '&size=20&brand=peerless&from=' . $pageNum;
    $documentRequest = web_services_api_call($url, "GET", [], "", "Search");

    if ($documentRequest['status'] == 'SUCCESS') {
      $extractResponse = json_decode($documentRequest['response'], TRUE);
      $pdfPathTitle = [];
      if (!empty($extractResponse['content'])) {
        $readPDF = $extractResponse['content'];
        // Lets count records.
        if (count($readPDF)) {
          $i = 0;
          foreach ($readPDF as $respCollections) {
            $pdfPath = $respCollections['filePath'];
            $name = $respCollections['fileName'];
            if (!empty($respCollections['modelNumber']) && count($respCollections['modelNumber'])) {
              $mobileNumber = array_slice($respCollections['modelNumber'], 0, 5);
              $pdfTitle = implode(" ", $mobileNumber);
              $pdfPathTitle[] = [
                'title' => $pdfTitle,
                'path' => $pdfPath,
                'name' => $name,
              ];
            }
            $i++;
          }
        }
      }
      $output = [
        '#theme' => 'support_results_page',
        '#items' => $pdfPathTitle,
      ];
      return $output;
    }

  }

  /**
   * Implementation of support results.
   */
  public function customerSupportBlock() {
    return [];
  }

}

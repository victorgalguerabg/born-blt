<?php

namespace Drupal\support\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Implementation of customer support form.
 */
class CustomerSupportForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'customer_support_form';
  }

  /**
   * Implementation fof customer support form build().
   */
  public function buildForm(array $form, FormStateInterface $form_state, $args = NULL) {

    $form = [];
    $form['name'] = [
      '#type' => 'textfield',
      '#autocomplete' => 'off',
      '#attributes' => [
        'class' => [
          'form-control',
        ],
        'autocomplete' => 'off',
      ],
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => 'search',
      '#attributes' => [
        'class' => [
          'btn btn-primary btn-md',
        ],
      ],
    ];

    $form['product_type'] = [
      '#type' => 'select',
      '#attributes' => [
        'class' => [
          'dropdown-list',
        ],
      ],
      '#options' => [
        '/kitchen/two-handle-faucets' => $this->t('Kitchen - Two-Handle'),
        'kitchen/pullouts' => $this->t('Kitchen - Pull-Out'),
        '/kitchen/singlehandles' => $this->t('Kitchen - Single-Handle'),
      ],
    ];

    $form['browse'] = [
      '#type' => 'submit',
      '#value' => 'browse',
      '#attributes' => [
        'class' => [
          'btn btn-primary btn-md',
        ],
      ],
    ];
    if ($args == '/customer-support-result') {
      $form['#theme'] = 'customer_support_result_block';
    }
    else {
      $form['#theme'] = 'customer_support_block';
    }

    $form['#attached']['library'][] = 'support/customer_support';
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $input = $form_state->getUserInput();
    $data = $form_state->getValue('name');
    if ($input['op'] == 'search') {
      // $url = Url::fromRoute('customer_support:result', ['q' => $data]);
      // $form_state->setRedirectUrl($url);
      $data = $form_state->getValue('name');
      $url = Url::fromRoute('customer_support.result', ['title' => $data]);
      $form_state->setRedirectUrl($url);
    }
    else {
      $url = $form_state->getValue('product_type');
      $response = new RedirectResponse($url);
      $response->send();
    }
  }

}

(function ($, Drupal, drupalSettings, Backbone) {

  Drupal.behaviors.plpBehaviour = {
    attach: function (context, settings) {

      $('document').ready(function () {

        $('.product-see-all').click(function () {
          $('ul.nav-tabs li.active').removeClass('active');
          $('#productTabLink').parent('li').addClass('active');
        });
        $('.document-see-all').click(function () {
          $('ul.nav-tabs li.active').removeClass('active');
          $('#documentTabLink').parent('li').addClass('active');

          $('html, body').stop().animate({
            scrollTop: $('.nav-tabs').offset().top
          }, 'slow');

        });
        $('.content-see-all').click(function () {
          $('ul.nav-tabs li.active').removeClass('active');
          $('#contentTabLink').parent('li').addClass('active');

          $('html, body').stop().animate({
            scrollTop: $('.nav-tabs').offset().top
          }, 'slow');

        });
      })

      var _totalCount = 0;
      var docView = Backbone.View.extend({
        el: ".documentoutput",
        template: _.template($("#document-output").html()),
        initialize: function (initData) {
          this.render(initData);
        },
        render: function (initData) {
          var _self = this;
          var _render = "";
          var content = initData.content;
          if (content.length > 0) {
            _.each(initData.content, function (entry) {
              var _filePath = entry.filePath;

              var _fileName = entry.fileName;
              _render += _self.template({
                fileName: _fileName,
                fileLinks: entry.modelNumber.slice(0,5),
                filePath: _filePath,
              });
              _self.$el.html(_render);


            });
          }
          var _documentCount = initData.totalElements;
          _totalCount = parseInt(_documentCount);
          $('#documentTabLink span').html('Documents (' + _documentCount + ')');
          $('#search-document-title').html('Documents (' + _documentCount + ')');
          return this;
        }
      });

      // Process content data and assemble the page.
      var contView = Backbone.View.extend({
        el: ".contentoutput",
        el1: ".contentoutputfull",
        template: _.template($("#content-output").html()),
        template1: _.template($("#full-content-output").html()),
        initialize: function (content) {
          if(content.currentpage === 1){
            this.renderAllsectionContent(content);
          }
          this.renderContentSection(content);
        },
        renderAllsectionContent: function (content) {
          var _self = this;
          var _render = "";
          if (content.totalmatches > 0) {
            var matches = content.matches.slice(0,8);
            _.each(matches, function (entry) {
              var _title = entry.title;
              var _body = entry.body;
              var _url = entry.url;
              _render += _self.template({
                searchContentBody: _body,
                searchContentUrl: _url,
                searchContentTitle: _title,
              });
              _self.$el.html(_render);
            });
          }
          return this;
        },
        renderContentSection: function (content) {
          var _self = this;
          var _render = "";
          if (content.totalmatches > 0) {
            var matches = content.matches;
            _.each(matches, function (entry) {
              var _title = entry.title;
              var _body = entry.body;
              var _url = entry.url;
              _render += _self.template1({
                searchContentBody: _body,
                searchContentUrl: _url,
                searchContentTitle: _title,
              });
              $('#fullcontentoutput').html(_render);
            });
          }
          return this;
        }
      });

      var ProdView = Backbone.View.extend({
        el: "#output",
        el2: "#output-1",
        template: _.template($("#output-node").html()),
        template2: _.template($("#output-node-1").html()),

        initialize: function (initData) {
          this.render(initData);
          this.renderFullProducts(initData);
        },

        render: function (initData) {
          var _self = this;
          var _render = "";
          var _i = 0;
          if (initData.content != '') {
            _.each(initData.content, function (entry) {
              if (_i == 8) {
                return false;
              }
              var _price = entry.values.ListPrice;
              var _heroImage = entry.heroImage;
              var _title = "Title";
              if (entry.description != '') {
                _title = entry.description;
              }
              var _collection = "PEERLESS";
              if (entry.values.defaultCollection != "") {
                _collection = entry.values.defaultCollection;
              }
              var _sku = entry.name;
              var urlAliasObject = drupalSettings.urlAliasObject;
              var _url = '';
              if (typeof urlAliasObject[_sku] != 'undefined') {
                _url = urlAliasObject[_sku];
              }
              if (_url == '') {
                _url = '#';
              }
              _render += _self.template({
                title: _title,
                collectionName: _collection,
                heroImage: _heroImage,
                sku: _sku,
                price: _price,
                url: _url,
              });
              _self.$el.html(_render);
              $('.tab-content-title').show();
              $('.preloader').remove();
              $('body').removeClass('loader-active');
              _i++;
            });
          } else {
            var noResultsMessage = '<div role="contentinfo" aria-label="Error message"><h2 class="visually-hidden">Status message</h2>Oops!!! Nothing Found. Please refine your search</div>';
            $('#output').html(noResultsMessage);
            $('.preloader').remove();
            $('body').removeClass('loader-active');
          }
          var _productCount = initData.totalElements;
          _totalCount = parseInt(_totalCount) + parseInt(_productCount);

          $('#productTabLink span').html('Products (' + _productCount + ')');
          $('#search-product-title').html('Products (' + _productCount + ')');
          $('#allTabLink span').html('All (' + _totalCount + ')');
          return this;
        },
        renderFullProducts: function (initData) {
          var _self = this;
          var _render = "";
          if (initData.content != '') {
            _.each(initData.content, function (entry) {
              var _price = entry.values.ListPrice;
              var _heroImage = entry.heroImage;
              var _title = "Title";
              if (entry.description != '') {
                _title = entry.description;
              }
              var _collection = "PEERLESS";
              if (entry.values.defaultCollection != "") {
                _collection = entry.values.defaultCollection;
              }
              var _sku = entry.name;
              var urlAliasObject = drupalSettings.urlAliasObject;
              var _url = '';
              if (typeof urlAliasObject[_sku] != 'undefined') {
                _url = urlAliasObject[_sku];
              }
              if (_url == '') {
                _url = '#';
              }
              _render += _self.template2({
                title: _title,
                collectionName: _collection,
                heroImage: _heroImage,
                sku: _sku,
                price: _price,
                url: _url,
              });
              $('#output-1').html(_render);
              $('.preloader').remove();
              $('body').removeClass('loader-active');
            });
          } else {
            var noResultsMessage = '<div role="contentinfo" aria-label="Error message"><h2 class="visually-hidden">Status message</h2>Oops!!! Nothing Found. Please refine your search</div>';
            $('#output').html(noResultsMessage);
            $('.preloader').remove();
            $('body').removeClass('loader-active');
          }
          var _productCount = initData.totalElements;
          $('#productTabLink span').html('Products (' + _productCount + ')');
          return this;
        }

      });

      var paginationView = Backbone.View.extend({
        el: ".pagination-output",
        template: _.template($("#pagination").html()),
        initialize: function (initData) {
          this.render(initData);
        },
        events: {
          'click [type="button"]': 'pagination'
        },
        pagination: function (e) {
          var params = [];
          params['pageNum'] = $(e.currentTarget).val();
          applyPLPFacets(params);
        },
        render: function (initData) {
          var _self = this;
          var _render = "";
          //stopLoop value
          var _nextPage = parseInt(initData.number) + 1;
          var _previousPage = parseInt(initData.number) - 1;
          _render += _self.template({
            totalPager: initData.totalPages,
            currentPage: _nextPage,
            nextPage: _nextPage,
            previousPage: _previousPage,
            totalRecordset: initData.totalElements
          });
          _self.$el.html(_render);
        }
      });

      var docpaginationView = Backbone.View.extend({
        el: ".doc-pagination-output",
        template: _.template($("#doc-pagination").html()),
        initialize: function (initData) {
          this.render(initData);
        },
        events: {
          'click [type="button"]': 'docpagination'
        },
        docpagination: function (e) {
          var params = [];
          params['pageNum'] = $(e.currentTarget).val();
          applydocFacets(params);
        },
        render: function (initData) {
          var _self = this;
          var _render = "";
          //stopLoop value
          var _nextPage = parseInt(initData.number) + 1;
          var _previousPage = parseInt(initData.number) - 1;
          _render += _self.template({
            totalPager: initData.totalPages,
            currentPage: _nextPage,
            nextPage: _nextPage,
            previousPage: _previousPage,
            totalRecordset: initData.totalElements
          });
          _self.$el.html(_render);
        }
      });

      // Pagination fot content section.
      var contpaginationView = Backbone.View.extend({
        el: ".content-pagination-output",
        template: _.template($("#content-pagination").html()),
        initialize: function (initData) {
          this.render(initData);
        },
        events: {
          'click [type="button"]': 'contentpagination'
        },
        contentpagination: function (e) {
          var params = [];
          params['pageNum'] = parseInt($(e.currentTarget).val()); // Current
                                                                  // Page
                                                                  // number
          applycontFacets(params);
        },
        render: function (initData) {
          var _self = this;
          var _render = "";
          var _currentpage = initData.currentpage;  // Current page
          var _totalmatches = initData.totalmatches;
          var _totalpages = Math.ceil(_totalmatches / 8);
          var _nextPage;
          if (_currentpage < _totalpages) {
            _nextPage = _currentpage + 1;
          }
          else {
            _nextPage = _currentpage;
          }
          var _previousPage;
          if (_currentpage === 1) {
            _previousPage = 1;
          }
          else {
            _previousPage = _currentpage - 1;
          }

          _render += _self.template({
            totalPager: _totalpages,
            currentPage: _currentpage,
            nextPage: _nextPage,
            previousPage: _previousPage,
            totalRecordset: initData.totalmatches
          });
          _self.$el.html(_render);
        }
      });

      if (context == document) {
        var _param = getSearchParams('title');
        $.ajax({
          type: "GET",
          url: "/search-output",
          dataType: 'json',
          data: {
            title: _param,
          },
          success: function (data) {
            var prodView = new ProdView(data);
            var PaginationView = new paginationView(data);
          }
        });

        $.ajax({
          type: "GET",
          url: "/search-doc-output",
          dataType: 'json',
          data: {
            title: _param,
          },
          success: function (data) {
            var DocView = new docView(data);
            var DocpaginationView = new docpaginationView(data);
          }
        });

        // Matching records for content being fetched and processed based on
        // matching record count.
        var content_matchings = getContentSearchResults(_param, page = 1);
        _totalCount = content_matchings.totalmatches;
        $('#contentTabLink span').html('Content (' + _totalCount + ')');
        $('#search-content-title').html('Content (' + _totalCount + ')');
        if (_totalCount > 0) {
          var ContView = new contView(content_matchings);
          var ContpaginationView = new contpaginationView(content_matchings);
        }
        else {
          $('#content-tab .row').html("<b>Nothing found. Please refine your search.</b>");
        }

      }


      /*
      To return content search results based on current search value and requesting page based on previous or next button hits in pagination.
      @params: search value, requesting page
      @return: search result
       */
      function getContentSearchResults(params, requestingpage) {
        var search_result = [];
        search_result.currentpage = requestingpage;
        var matching_count = 0;
        $.ajax({
          type: "GET",
          url: "/rest/export/search_content",
          dataType: 'json',
          async: false,
          data: {
            title: _param,
            page: requestingpage - 1,  // Since views counts from 0
          },
          success: function (data) {
            var matches = [];
            $.each(data, function (key, value) {
              matches.push(value);
            });
            search_result.matches = matches;

            $.ajax({
              type: "GET",
              url: "/rest/export/matching_records_count",
              dataType: 'json',
              async: false,
              data: {
                title: _param,
              },
              success: function (data) {
                matching_count = data.length;
              }
            });

            search_result.totalmatches = matching_count;
          }
        });

        return search_result;
      }

      function applyPLPFacets(params) {
        $('body').addClass('loader-active');
        if (!$('.preloader').length) {
          $('#plp-wrapper').prepend('<div class="preloader"><i class="input-loader"></i></div>');
        }
        $('html, body').stop().animate({
          scrollTop: $('.nav-tabs').offset().top
        }, 'slow');
        $.ajax({
          type: "GET",
          url: "/search-output",
          dataType: 'json',
          data: {
            title: _param,
            params: params['facets'],
            size: "9",
            page: params['pageNum'],
          },

          success: function (data) {
            var prodView = new ProdView(data);
            var PaginationView = new paginationView(data);
            PaginationView.undelegateEvents();


            $('body').removeClass('loader-active');

          }
        });
      }

      function applydocFacets(params) {
        $('body').addClass('loader-active');
        if (!$('.preloader').length) {
          $('#plp-wrapper').prepend('<div class="preloader"><i class="input-loader"></i></div>');
        }
        $('html, body').stop().animate({
          scrollTop: $('#plp-wrapper').offset().top
        }, 'slow');
        $.ajax({
          type: "GET",
          url: "/search-doc-output",
          dataType: 'json',
          data: {
            title: _param,
            page: params['pageNum'],
          },

          success: function (data) {
            var DocView = new docView(data);
            var PaginationView = new docpaginationView(data);
            PaginationView.undelegateEvents();

            $('body').removeClass('loader-active');
            $('body').removeClass('preloader');

          }
        });
      }

      /*
      This function will be called when user hits on certain page(previous or next) on pagination.
      Fetches contents based on current search value and page number requested
      @params: pagenumber
       */
      function applycontFacets(params) {
        $('body').addClass('loader-active');
        if (!$('.preloader').length) {
          $('.search-result-page').prepend('<div class="preloader"><i class="input-loader"></i></div>');
        }
        $('html, body').stop().animate({
          scrollTop: $('.search-result-page').offset().top
        }, 'slow');

        var content_matchings = getContentSearchResults(_param, page = params['pageNum']);

        var ContView = new contView(content_matchings);
        var ContpaginationView = new contpaginationView(content_matchings);
        ContpaginationView.undelegateEvents();

        $('body').removeClass('loader-active');
        $('body').removeClass('preloader');
      }

      function getSearchParams(k) {
        var p = {};
        location.search.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (s, k, v) {
          p[k] = v
        })
        var response = k ? p[k] : p;
        var decodedResponse = decodeURIComponent(response);
        var sanitizedResponse = removeHTMLTags(decodedResponse);
        return sanitizedResponse;
      }

      /*
              Function to remove HTML tags.
         */

      function removeHTMLTags(decodedResponse) {
        paramText = decodedResponse;
        paramText = paramText.replace(/<script.*>[\w\W]{1,}(.*?)[\w\W]{1,}<\/script>/gi, "");
        paramText = paramText.replace(/%3Cscript.*%3E[\w\W]{1,}(.*?)[\w\W]{1,}%3C\/script%3E/gi, "");
        paramText = paramText.replace(/<style.*>[\w\W]{1,}(.*?)[\w\W]{1,}<\/style>/gi, "");
        paramText = paramText.replace(/&nbsp;/gi, " ");
        paramText = paramText.replace(/&amp;/gi, "&");
        paramText = paramText.replace(/&quot;/gi, '"');
        paramText = paramText.replace(/&lt;/gi, '<');
        paramText = paramText.replace(/&gt;/gi, '>');
        paramText = paramText.replace(/svg\/onload%3D/gi, '');
        paramText = paramText.replace(/--%21/gi, '');
        paramText = paramText.replace(/svg\/onload%3D/gi, '');
        paramText = paramText.replace(/--%21/gi, '');
        paramText = paramText.replace(/%28confirm%29/gi, '')
        paramText = paramText.replace(/%3C/gi, '');
        paramText = paramText.replace(/%3E/gi, '');
        paramText = paramText.replace(/<(?:.|\s)*?>/g, "");
        return paramText;

      }

    }

  };
})(jQuery, Drupal, drupalSettings, Backbone);

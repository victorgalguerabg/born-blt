(function ($, Drupal, drupalSettings) {

  Drupal.behaviors.globalSearch = {
    attach: function (context, settings) {
      var xhr;
      $('form#global-search-form input.autocomplete').keyup(function (event) {
        var searchText = $(this).val();
        if (searchText.length > 2) {
          fn(searchText);
          $('.autocomplete-container--inner .input-close').addClass('hide');
          $('.autocomplete-container--inner .input-loader').removeClass('hide');
        } else {
          if (xhr) {
            xhr.abort();
          }
          $("#search-block-suggestion-result").empty();
          $('.autocomplete-container--inner .input-loader, .autocomplete-container--inner .input-close').addClass('hide');
        }
      });

      $(document).on('click', '#search-block-suggestion-result li.record', function () {
        var itemValue = $(this).attr('data-item-value');
        $('form#global-search-form input.autocomplete').val(itemValue);
        $("#search-block-suggestion-result").empty();
      });

      $('.autocomplete-container--inner .input-close').click(function () {
        $('form#global-search-form input.autocomplete').val('');
        $("#search-block-suggestion-result").empty();
        $('.autocomplete-container--inner .input-close').addClass('hide');
        $('.autocomplete-container--inner input.autocomplete').focus();
      });
      var fn = function (searchText) {
        if (xhr && xhr.readyState != 4) {
          xhr.abort();
        }
        var output = '';
        $('.autocomplete-container--inner .input-loader').removeClass('hide');
        var searchUrl = drupalSettings.path.baseUrl + "global-search-autocomplete";
        xhr = $.get(searchUrl, {text: searchText});
        xhr.done(function (data) {
          if (data == '') {
            output = "<div class=\"pad-aa text-align-center\">No Results Found!</div>";
          } else {
            output = data;
          }
          $("#search-block-suggestion-result").html(output);
          $('.autocomplete-container--inner .input-loader').addClass('hide');
          $('.autocomplete-container--inner .input-close').removeClass('hide');
        });
      };
    }
  };
})(jQuery, Drupal, drupalSettings);

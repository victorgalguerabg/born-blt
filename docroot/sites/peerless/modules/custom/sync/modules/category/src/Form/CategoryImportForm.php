<?php

namespace Drupal\sync_category\Form;

use Drupal\sync_category\Controller\CategoryImportController;
use Drupal\taxonomy\Entity\Vocabulary;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\Core\Messenger\MessengerInterface;

/**
 * Class CategoryImportForm.
 */
class CategoryImportForm extends FormBase {
  /**
   * To get current query string.
   *
   * @var Symfony\Component\HttpFoundation\RequestStack
   */
  protected $request;

  /**
   * Implementation of EntityTypeManagerInterface.
   *
   * @var Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entitytypemanager;

  /**
   * To record logs.
   *
   * @var logger
   */
  protected $logger;

  /**
   * Class constructor.
   */
  public function __construct(RequestStack $request, EntityTypeManagerInterface $entitytypemanager, LoggerChannelFactoryInterface $logger, MessengerInterface $messenger) {
    $this->requestStack = $request;
    $this->entitytypemanager = $entitytypemanager;
    $this->logger = $logger;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
    // Load the service required to construct this class.
      $container->get('request_stack'),
      $container->get('entity_type.manager'),
      $container->get('logger.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'category_import_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $nid = '') {
    $form['#id'] = 'category-import-form';
    $form['choose'] = [
      '#type' => 'select',
      '#title' => $this->t('Import Category'),
      '#required' => TRUE,
      '#options' => [
        'insert' => 'Import Category',
        'delete' => 'Remove Category',
        'facetInsert' => 'Import Facets',
      ],
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#id' => 'import_category',
      '#value' => $this->t('Import'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $input_selection = $form_state->getUserInput();
    $input = $input_selection['choose'];
    $configPrimary = $this->config('delta_services.deltaservicesconfig');
    $domain = $configPrimary->get('api_base_path');
    $config = $this->config('sync.api_settings');
    if ($input == 'insert') {
      $term_depth = $config->get('sync.taxonomy_depth');
      $term_filter = $config->get('sync.term_filter');
      if (empty($term_filter)) {
        $message = $this->t('Category import failed, Please add prefix name on the CMS backend');
        $this->messenger->addMessage($message, 'warning');
        $this->logger->get('category_import')->error('@message', [
          '@message' => $message,
        ]);
        return FALSE;
      }
      $url = $domain . $config->get('sync.category_api');
      $method = 'GET';
      $headers = add_headers(FALSE);
      $data = '';
      $response = web_services_api_call($url, $method, $headers, $data, 'CATEGORY-SYNC');

      if ($response['status'] == 'SUCCESS') {
        $results = json_decode($response['response']);

        if (!empty($results)) {
          $category_list = [];
          foreach ($results as $key => $val) {
            $category_path = $val->categoryPath;
            $split_category_path = explode("_", $category_path);

            if ($split_category_path[0] == $term_filter) {

              if (!empty($split_category_path[1])) {
                $parent_category = $split_category_path[1];
                $cat_path = $term_filter . '_' . $parent_category;
                $unique_key = str_replace(['/', ' '], "_", $cat_path);
                $category_list[$unique_key] = [
                  'id' => $parent_category,
                  'name' => $parent_category,
                  'category_path' => $cat_path,
                ];
              }
              if (!empty($split_category_path[2]) && $term_depth > 1) {
                $child_category = $split_category_path[2];
                $parent_category = $split_category_path[1];
                $cat_path = $term_filter . '_' . $parent_category . '_' . $child_category;
                $unique_key = str_replace(['/', ' '], "_", $cat_path);
                $category_list[$unique_key] = [
                  'id' => $child_category,
                  'name' => $child_category,
                  'parent' => $parent_category,
                  'category_path' => $cat_path,
                ];
              }
              if (!empty($split_category_path[3]) && $term_depth > 2) {
                $sibling_category = $split_category_path[3];
                $child_category = $split_category_path[2];
                $parent_category = $split_category_path[1];
                $cat_path = $term_filter . '_' . $parent_category . '_' . $child_category . '_' . $sibling_category;
                $unique_key = str_replace(['/', ' '], "_", $cat_path);
                $category_list[$unique_key] = [
                  'id' => $sibling_category,
                  'name' => $sibling_category,
                  'parent' => $child_category,
                  'category_path' => $cat_path,
                ];
              }
            }
          }
          if (empty($category_list)) {
            $message = $this->t('There is no category exist with the prefix : @term_filter', ['@term_filter' => $term_filter]);
            $this->messenger->addMessage($message, 'warning');
            $this->logger->get('category_import')->error('@message', [
              '@message' => $message,
            ]);
            return FALSE;
          }

          // To run without batch support.
          $operations[] = [
            '\Drupal\sync_category\Controller\CategoryImportController::categoryImport',
            [$category_list],
          ];
          $batch = [
            'title' => $this->t('Import Category...'),
            'operations' => $operations,
            'finished' => '\Drupal\sync_category\Controller\CategoryImportController::categoryImportFinishedCallback',
          ];
          // To run with batch support. uncomment.
          batch_set($batch);
        }
      }
      else {
        $message = $this->t('Category import failed');
        $this->messenger->addMessage($message, 'warning');
        $this->logger->get('category_import')->error('@message', [
          '@message' => $message,
        ]);
      }
    }
    elseif ($input == 'delete') {
      $result = $this->entitytypemanager->getStorage('taxonomy_term')->loadByProperties([
        'vid' => 'product_category'
      ]);
      foreach ($result as $item) {
        $item->delete();
      }
      $message = $this->t('Category deleted successfully');
      $this->messenger->addMessage($message, 'status');
      $this->logger->get('category_delete')->error('@message', [
        '@message' => $message,
      ]);
    }
    elseif ($input == 'facetInsert') {
      $facet_url = $domain . $config->get('sync.facet_api');
      $facet_size = 24;
      $siteUrl = str_replace(['{facetSize}'], [$facet_size], $facet_url);
      $method = 'GET';
      $headers = add_headers(FALSE);
      $data = '';
      $response = web_services_api_call($siteUrl, $method, $headers, $data, 'FACET-SYNC');
      if ($response['status'] == 'SUCCESS') {
        $results = json_decode($response['response']);
        if (!empty($results->facets)) {
          foreach ($results->facets as $key => $facet_obj) {
            if ($facet_obj->type == 'term' || $facet_obj->type == 'range') {
              $vid = $facet_obj->name;

              $vid = trim($vid);
              $name = $facet_obj->name;
              $vocabularies = Vocabulary::loadMultiple();
              $listVid = [];
              foreach ($vocabularies as $key => $vocObj) {
                $listVid[] = strtolower($key);
              }
              if (!in_array(strtolower($vid), $listVid) && count($listVid) > 0) {
                $vocabulary = Vocabulary::create([
                  'vid' => strtolower($vid),
                  'machine_name' => strtolower($vid),
                  'description' => $name,
                  'name' => strtolower($name),
                ]);
                $vocabulary->save();
              }
              if ($facet_obj->type == 'range') {
                foreach ($facet_obj->ranges as $term) {
                  $name = $term->from . '-' . $term->to;
                  $facet_list = ['id' => $name, 'name' => $name, 'vid' => $vid];
                  $facet_list = new \stdClass();
                  $facet_list->id = $name;
                  $facet_list->name = $name;
                  $facet_list->vid = strtolower($vid);
                  CategoryImportController::facetImport($facet_list);
                  $operations[] = [
                    '\Drupal\sync_category\Controller\CategoryImportController::facetImport',
                    [$facet_list],
                  ];
                }
              }
              else {
                foreach ($facet_obj->terms as $term) {
                  $name = $term->term;
                  $facet_list = ['id' => $name, 'name' => $name, 'vid' => $vid];
                  $facet_list = new \stdClass();
                  $facet_list->id = $name;
                  $facet_list->name = $name;
                  $facet_list->vid = strtolower($vid);
                  // $facet_terms[] = $facet_list;.
                  CategoryImportController::facetImport($facet_list);
                  $operations[] = [
                    '\Drupal\sync_category\Controller\CategoryImportController::facetImport',
                    [$facet_list],
                  ];
                }
              }
            }
          }
        }
        $batch = [
          'title' => $this->t('Facet import...'),
          'operations' => $operations,
          'finished' => '\Drupal\sync_category\Controller\CategoryImportController::facetImportFinishedCallback',
        ];
        // To run with batch support. uncomment.
        batch_set($batch);
      }
    }
  }

}

<?php

namespace Drupal\sync_category\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\taxonomy\Entity\Term;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;
use Drupal\taxonomy\Entity\Vocabulary;
use Drupal\Core\Messenger\MessengerInterface;

/**
 * Category import controller.
 */
class CategoryImportController extends ControllerBase {

  const PRODUCTVID = 'product_category';

  /**
   * To stores taxonomy object.
   *
   * @var taxonomyEntity
   */
  static private $taxonomyEntity;
  /**
   * To handle success message success Message.
   *
   * @var successMessage
   */
  static private $successMessage;
  /**
   * TO handle success message Failure Message.
   *
   * @var FailureMessage
   */
  static private $FailureMessage;
  /**
   * For logging purpose.
   *
   * @var Logger
   */
  protected $logger;

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Class constructor.
   */
  public function __construct(LoggerChannelFactoryInterface $logger, MessengerInterface $messenger) {
    $this->logger = $logger;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
    // Load the service required to construct this class.
      $container->get('logger.factory'),
      $container->get('messenger')
    );
  }

  /**
   * To print category import success message.
   */
  final public static function getImportSuccessMessage() {
    if (!isset(self::$successMessage)) {
      self::$successMessage = t('Category sync successfully completed.');
    }
    return self::$successMessage;
  }

  /**
   * To print category import fail message.
   */
  final public static function getImportFailureMessage() {
    if (!isset(self::$FailureMessage)) {
      self::$FailureMessage = t('Finished with an error.');
    }
    return self::$FailureMessage;
  }

  /**
   * Get term entity object.
   */
  final public static function gettaxonomyEntity() {
    if (!isset(self::$taxonomyEntity)) {
      self::$taxonomyEntity = \Drupal::entityTypeManager()->getStorage('taxonomy_term');
    }
    return self::$taxonomyEntity;
  }

  /**
   * Description: Created menu call back will hit category function.
   */
  public function category(LoggerChannelFactoryInterface $logger) {
    $message = self::drushCategoryImport();
    $this->logger->get('sync_category')->error('@message', [
      '@message' => $message,
    ]);
    exit;
  }

  /**
   * Implementation drushCategoryImport.
   */
  public static function drushCategoryImport() {
    $configPrimary = \Drupal::config('delta_services.deltaservicesconfig');
    $domain = $configPrimary->get('api_base_path');
    $config = \Drupal::config('sync.api_settings');
    $url = $domain . $config->get('sync.category_api');

    $headers = add_headers(FALSE);
    $data = [];
    $response = web_services_api_call($url, 'GET', $headers, $data, 'CAT-SYNC');
    if ($response['status'] == 'SUCCESS') {
      $results = json_decode($response['response']);
      if (!empty($results) && !empty($results->categories) && count($results->categories) > 0) {
        self::categoryImport($results);
      }
      return t('Category import successfully completed');
    }
    return t('Category import failed');
  }

  /**
   * Implementation of categoryImportFinishedCallback.
   */
  public static function categoryImportFinishedCallback($success, $results, $operations) {
    if ($success) {
      $message = t('Category sync successfully completed.');
    }
    else {
      $message = t('Finished with an error.');
    }
    $this->messenger->addMessage($message);
  }

  /**
   * Implementation of facetImportFinishedCallback.
   */
  public static function facetImportFinishedCallback($success, $results, $operations) {
    if ($success) {
      $message = t('Facet sync successfully completed.');
    }
    else {
      $message = t('Finished with an error.');
    }
    $this->messenger->addMessage($message);
  }

  /**
   * Implementation of categoryImport.
   */
  public static function categoryImport($results) {
    // Get api info.
    \Drupal::config('sync.api_settings');
    self::getDataExtract($results);
  }

  /**
   * Implementation of isTermExists.
   */
  public static function isTermExists($catName, $vid = 'product_category') {
    $query = \Drupal::entityTypeManager()
            ->getStorage('taxonomy_term')->getQuery();
    $query->condition('vid', $vid);
    $query->condition('name', $catName);
    $tids = $query->execute();
    if (!empty($tids)) {
      return current($tids);
    }
    return $tids;
  }

  /**
   * Implementation of isTermExistsWithPid.
   */
  public static function isTermExistsWithPid($catName, $pid = 0, $vid = 'product_category') {
    $query = \Drupal::entityTypeManager()
            ->getStorage('taxonomy_term')->getQuery();
    $query->condition('vid', $vid);
    $query->condition('name', $catName);
    $query->condition('parent', $pid);
    $tids = $query->execute();
    if (!empty($tids)) {
      return current($tids);
    }
    return $tids;
  }

  /**
   * Implementation of taxonomyTermCreate.
   */
  public static function taxonomyTermCreate($term, $vocabulary, $weight) {
    $cat_path = (!empty($term->category_path)) ? $term->category_path : '';
    $parent = (!empty($term->parent)) ? $term->parent : [];
    $terms = [];
    if (!empty($cat_path)) {
      $terms['field_category_path'] = ['value' => $cat_path];
    }
    $terms['field_show_filters'] = ['value' => TRUE];
    $terms += [
      'name' => $term->id,
      'description' => $term->name,
      'parent' => $parent,
      'vid' => $vocabulary,
      'weight' => [$weight],
    ];
    $new_term = Term::create($terms);
    // Save the taxonomy term.
    $new_term->save();
    return $new_term->id();
  }

  /**
   * Implementation of taxonomyTermUpdate.
   */
  public static function taxonomyTermUpdate($updateterm, $vocabulary, $weight) {
    $cat_path = (!empty($updateterm->category_path)) ? $updateterm->category_path : '';
    $cat_description = (!empty($updateterm->name)) ? $updateterm->name : '';
    $parent = (!empty($updateterm->parent)) ? $updateterm->parent : [];
    $term = Term::load($vocabulary);
    $term->weight->setValue($weight);
    $term->name->setValue($updateterm->id);
    if (empty($updateterm->parent)) {
      $term->parent->setValue($parent);
    }
    if($term->hasField('field_show_filters')){
      $term->field_show_filters->setValue(TRUE);
    }

    if (!empty($cat_path)) {
      $term->field_category_path->setValue($cat_path);
    }
    $term->description->setValue($cat_description);
    // TO do add code to validate image URL path.
    $term->save();
  }

  /**
   * Implementation of getDataExtract.
   */
  public static function getDataExtract($results) {
    $term_names = [];
    if (!empty($results)) {
      $weight = 0;
      //Create parent categories now
      foreach ($results as $get_category) {
        $getCatPath = $get_category['category_path'];
        if(!empty($getCatPath)){
          $getCatPathParts = explode("_", $getCatPath);
          $getCatPathPartsCnt = count($getCatPathParts);
          if($getCatPathPartsCnt == 2){
            $category = new \stdClass();
            $category->id = $get_category['id'];
            $category->name = $get_category['name'];
            $category->category_path = $get_category['category_path'];
            $tid = self::isTermExistsWithPid($category->id);
            // Lets create term if not exist.
            if (empty($tid)) {
              $tid = self:: taxonomyTermCreate($category, self::PRODUCTVID, $weight);
            }
          }
        }
      }
      //Removing parent categories from the results array
      //like Peerless_Kitchen, Peerless_Bath, Peerless_Parts
      foreach ($results as $get_category) {
        $getCatPath = $get_category['category_path'];
        if(!empty($getCatPath)){
          $getCatPathParts = explode("_", $getCatPath);
          $getCatPathPartsCnt = count($getCatPathParts);
          if($getCatPathPartsCnt == 2){
            unset($results[$getCatPath]);
          }
        }
      }

      //Now results array holding sub categories only
      foreach ($results as $get_category) {
        $category = new \stdClass();
        $category->id = $get_category['id'];
        $category->name = $get_category['name'];
        $category->category_path = $get_category['category_path'];
        if (!empty($get_category['parent'])) {
          $parent_id = self::isTermExists($get_category['parent']);
          $category->parent = $parent_id;
        }
        $tid = [];
        if(!empty($parent_id)){
          $tid = self::isTermExistsWithPid($category->id, $parent_id);
        }
        // Lets create term if not exist.
        if (empty($tid)) {
          $tid = self:: taxonomyTermCreate($category, self::PRODUCTVID, $weight);
        }
        else {
          // If exist, check term id with parent id.
          $get_terms = get_term_parent_details($tid);
          $update_flag = TRUE;
          if (!empty($get_terms) && isset($get_terms[1][0]) && $get_terms[1][0] == $parent_id) {
            self::taxonomyTermUpdate($category, $tid, $weight);
            $update_flag = FALSE;
          }
          if (empty($parent_id) || (!empty($get_terms[0][0]))) {
            self::taxonomyTermUpdate($category, $tid, $weight);
            $update_flag = FALSE;
          }
          if ($update_flag === TRUE) {
            $tid = self:: taxonomyTermCreate($category, self::PRODUCTVID, $weight);
          }
        }
        $term_names[$tid] = $category->name;
        $weight++;
      }
      return $term_names;
    }
    return $term_names;
  }

  /**
   * Implementation of facetImport.
   */
  public static function facetImport($terms) {
    $tid = self::isTermExists($terms->id, $terms->vid);
    if (empty($tid)) {
      $tid = self:: taxonomyTermCreate($terms, $terms->vid, 0);
    }
    else {
      self::taxonomyTermUpdate($terms, $tid, 0);
    }
  }
  /**
   * Implementation of all category sync menu callback.
   */
  public function importAllCategory($key = NULL) {
    $return_response = new Response();
    $config = $this->config('sync.api_settings');
    $cron_key = $config->get('sync.sync_cron_key');
    if ($cron_key == $key) {
      $configPrimary = $this->config('delta_services.deltaservicesconfig');
      $domain = $configPrimary->get('api_base_path');
      $term_depth = $config->get('sync.taxonomy_depth');
      $term_filter = $config->get('sync.term_filter');
      if (empty($term_filter)) {
        $message = $this->t('Category import failed, Please add prefix name on the CMS backend');
        $this->logger->get('category_import')->error('@message', [
          '@message' => $message,
        ]);
        return FALSE;
      }
      $url = $domain . $config->get('sync.category_api');
      $method = 'GET';
      $headers = add_headers(FALSE);
      $data = '';
      $response = web_services_api_call($url, $method, $headers, $data, 'CATEGORY-SYNC');
      if ($response['status'] == 'SUCCESS') {
        $results = json_decode($response['response']);

        if (!empty($results)) {
          $category_list = [];
          foreach ($results as $key => $val) {
            $category_path = $val->categoryPath;
            $split_category_path = explode("_", $category_path);

            if ($split_category_path[0] == $term_filter) {

              if (!empty($split_category_path[1])) {
                $parent_category = $split_category_path[1];
                $cat_path = $term_filter . '_' . $parent_category;
                $unique_key = str_replace(['/', ' '], "_", $cat_path);
                $category_list[$unique_key] = [
                  'id' => $parent_category,
                  'name' => $parent_category,
                  'category_path' => $cat_path,
                ];
              }
              if (!empty($split_category_path[2]) && $term_depth > 1) {
                $child_category = $split_category_path[2];
                $parent_category = $split_category_path[1];
                $cat_path = $term_filter . '_' . $parent_category . '_' . $child_category;
                $unique_key = str_replace(['/', ' '], "_", $cat_path);
                $category_list[$unique_key] = [
                  'id' => $child_category,
                  'name' => $child_category,
                  'parent' => $parent_category,
                  'category_path' => $cat_path,
                ];
              }
              if (!empty($split_category_path[3]) && $term_depth > 2) {
                $sibling_category = $split_category_path[3];
                $child_category = $split_category_path[2];
                $parent_category = $split_category_path[1];
                $cat_path = $term_filter . '_' . $parent_category . '_' . $child_category . '_' . $sibling_category;
                $unique_key = str_replace(['/', ' '], "_", $cat_path);
                $category_list[$unique_key] = [
                  'id' => $sibling_category,
                  'name' => $sibling_category,
                  'parent' => $child_category,
                  'category_path' => $cat_path,
                ];
              }
            }
          }

          if (empty($category_list)) {
            $message = $this->t('There is no category exist with the prefix : @term_filter', ['@term_filter' => $term_filter]);
            $this->logger->get('category_import')->error('@message', [
              '@message' => $message,
            ]);
            return FALSE;
          }
          else {
            $message = $this->t('Category import success!');
            $this->logger->get('category_import')->info('@message', [
              '@message' => $message,
            ]);
          }
        }
      }
      else {
        $message = $this->t('Category import failed');
        $this->logger->get('category_import')->error('@message', [
          '@message' => $message,
        ]);
      }
      $return_response->setContent($message);
    }
    else {
      $message = $this->t('Category Sync cron key not matching');
      $this->logger->get('category_import')->error('@message', [
        '@message' => $message,
      ]);
      $return_response->setContent($message);
    }
    return $return_response;
  }
  /**
   * Implementation of all facet sync menu callback.
   */
  public function importAllFacets($key = NULL) {
    $return_response = new Response();
    $config = $this->config('sync.api_settings');
    $cron_key = $config->get('sync.sync_cron_key');
    if ($cron_key == $key) {
      $facet_url = $config->get('sync.facet_api');
      $facet_size = 24;
      $siteUrl = str_replace(['{facetSize}'], [$facet_size], $facet_url);
      $method = 'GET';
      $headers = add_headers(FALSE);
      $data = '';
      $response = web_services_api_call($siteUrl, $method, $headers, $data, 'FACET-SYNC');
      if ($response['status'] == 'SUCCESS') {
        $results = json_decode($response['response']);
        if (!empty($results->facets)) {
          foreach ($results->facets as $key => $facet_obj) {
            if ($facet_obj->type == 'term' || $facet_obj->type == 'range') {
              $vid = $facet_obj->name;

              $vid = trim($vid);
              $name = $facet_obj->name;
              $vocabularies = Vocabulary::loadMultiple();
              $listVid = [];
              foreach ($vocabularies as $key => $vocObj) {
                $listVid[] = strtolower($key);
              }
              if (!in_array(strtolower($vid), $listVid) && count($listVid) > 0) {
                $vocabulary = Vocabulary::create([
                  'vid' => strtolower($vid),
                  'machine_name' => strtolower($vid),
                  'description' => $name,
                  'name' => strtolower($name),
                ]);
                $vocabulary->save();
              }
              if ($facet_obj->type == 'range') {
                foreach ($facet_obj->ranges as $term) {
                  $name = $term->from . '-' . $term->to;
                  $facet_list = ['id' => $name, 'name' => $name, 'vid' => $vid];
                  $facet_list = new \stdClass();
                  $facet_list->id = $name;
                  $facet_list->name = $name;
                  $facet_list->vid = strtolower($vid);
                  CategoryImportController::facetImport($facet_list);
                }
              }
              else {
                foreach ($facet_obj->terms as $term) {
                  $name = $term->term;
                  $facet_list = ['id' => $name, 'name' => $name, 'vid' => $vid];
                  $facet_list = new \stdClass();
                  $facet_list->id = $name;
                  $facet_list->name = $name;
                  $facet_list->vid = strtolower($vid);
                  // $facet_terms[] = $facet_list;.
                  CategoryImportController::facetImport($facet_list);
                }
              }
            }
          }
          $message = $this->t('Facets import success!');
          $this->logger->get('category_import')->info('@message', [
            '@message' => $message,
          ]);
        }
      }
      else {
        $message = $this->t('Facets import api failed');
        $this->logger->get('category_import')->error('@message', [
          '@message' => $message,
        ]);
      }
      $return_response->setContent($message);
    }
    else {
      $message = $this->t('Facets Sync cron key not matching');
      $this->logger->get('category_import')->error('@message', [
        '@message' => $message,
      ]);
      $return_response->setContent($message);
    }
    return $return_response;
  }
}

<?php

namespace Drupal\sync_product\Controller;

use Drupal\file\Entity\File;
use Drupal\Core\Controller\ControllerBase;
use Drupal\node\Entity\Node;
use Drupal\paragraphs\Entity\Paragraph;
use Symfony\Component\HttpFoundation\RedirectResponse;
use \Drupal\Core\Url;
use FFMpeg\Coordinate\TimeCode;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Symfony\Component\HttpFoundation\Response;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\path_alias\Entity\PathAlias;

/**
 * Class ProductImportController.
 */
class ProductImportController extends ControllerBase {
    /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;
  /**
   * To handle SuccessMessage.
   *
   * @var successMessage
   */
  static private $successMessage;
  /**
   * To handle FailureMessage.
   *
   * @var FailureMessage
   */
  static private $FailureMessage;
    /**
    * Implementation of EntityTypeManagerInterface.
    *
    * @var Drupal\Core\Entity\EntityTypeManagerInterface
    */


  /**
   * Implementation of EntityTypeManagerInterface.
   *
   * @var Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entitytypemanager;

  /**
   * For logging purpose.
   *
   * @var Logger
   */
  protected $logger;

  /**
   * Class constructor.
   */
  public function __construct(EntityTypeManagerInterface $entitytypemanager, LoggerChannelFactoryInterface $logger, ConfigFactoryInterface $config_factory) {
    $this->entitytypemanager = $entitytypemanager;
    $this->logger = $logger;
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
      $container->get('entity_type.manager'),
      $container->get('logger.factory'),
      $container->get('config.factory')
    );
  }

  /**
   * Implementation of getImportSuccessMessage.
   */
  final public static function getImportSuccessMessage() {
    if (!isset(self::$successMessage)) {
      self::$successMessage = t('Product sync successfully completed.');
    }
    return self::$successMessage;
  }

  /**
   * Implementation of getImportFailureMessage.
   */
  final public static function getImportFailureMessage() {
    if (!isset(self::$FailureMessage)) {
      self::$FailureMessage = t('Finished with an error.');
    }
    return self::$FailureMessage;
  }

  /**
   * Implementation of productImportAll.
   */
  public static function productImportAll($products, $product_search_url) {
    self::productsSync($products, $product_search_url, []);
  }

  /**
   * Implementation of getProductData.
   */
  public static function getProductData($accessor, $dataObj, $key) {

    if ($accessor->isReadable($dataObj, $key)) {
      return $accessor->getValue($dataObj, $key);
    }
    return FALSE;
  }

  /**
   * Implementation of productsSync.
   */
  public static function productsSync($term_arr, $product_search_url, $manual_creation_resp) {
    $results_content = [];

    $config = \Drupal::config('sync.api_settings');
    $ffmpegPath = $config->get('sync.ffmpeg_installation_path');
    $term_ids = [];
    $term_name = 'Peerless_Kitchen';
    $main_category = 'Kitchen';
    if(is_array($term_arr) && array_key_exists('term', $term_arr)){
      $term_ids = $term_arr['term'];
      $term_name = $term_arr['pattern'];
      $arr = explode('_', $term_arr['pattern']);
      $main_category = $arr[1];
    }

    // Via - Sync
    if(is_array($term_arr) && array_key_exists('syncType', $term_arr) && $term_arr['syncType'] === 'Specific'){
      $productUrl = $term_arr['productUrl'];
      $product_response = self::createProductSpecific($productUrl);
      if(count($product_response) == 0){
        return [];
      }
      $term_name = $product_response[0]['pattern'];
      $term_ids = $product_response[0]['term'];
      $arr = explode('_', $term_name);
      $main_category = $arr[1];
    }
    if ($main_category == 'Kitchen' || $main_category == 'Bath' || $main_category == 'Parts') {
      if (array_key_exists("source", $term_arr)) {
        $title = '';
        if ($manual_creation_resp['status'] == 'SUCCESS') {
          $results_content[0][0] = (array) json_decode($manual_creation_resp['response']);
        }
      }
      else if (array_key_exists("syncType", $term_arr) && $term_arr['syncType'] == 'Specific') {
        $results_content[0][0] = (array) json_decode($product_response[1]['response']);
      }
      else {
        $siteUrlReplace = str_replace(['{productCat}'], [$term_name], $product_search_url);
        $j = 6;
        for ($i = 0; $i <= $j; $i++) {
          $siteUrl = $siteUrlReplace . "&page=$i";
          $method = 'GET';
          $headers = add_headers(FALSE);
          $data = $results = [];
          $response = web_services_api_call($siteUrl, $method, $headers, $data, 'PRODUCT-IMPORT');
          if($response['status'] === 'ERROR'){
            \Drupal::logger('product_import_failed')->notice('Synchronization down, please try again later. Error time:' . time());
            exit('Synchronization down, please try again later.');
          }
          $title = '';
          if ($response['status'] == 'SUCCESS') {
            $results = (array) json_decode($response['response']);
            $j = $results['totalPages'];
            if ($results['numberOfElements'] > 0) {
              $results_content[] = $results['content'];
            }
          }
        }
      }
      if (!empty($results_content)) {
        foreach ($results_content as $items) {
          foreach ($items as $product_obj) {
            $name = is_object($product_obj) ? $product_obj->description : $product_obj['description'];
            $model_number = is_object($product_obj) ? $product_obj->values->ModelNumber : $product_obj['values']->ModelNumber;
            $title = $model_number . ' - ' . $name;
            if (!empty($title)) {
              $sku = is_object($product_obj) ? $product_obj->values->ModelNumber : $product_obj['values']->ModelNumber;
              $product = [
                'assets' => is_object($product_obj) ? $product_obj->assets : $product_obj['assets'],
                'sku' => $sku,
                'title' => $title,
                'term' => $term_ids,
                'main_category' => $main_category,
              ];
              $productSku = $product['sku'];
              $product_assets_arr = [];
              $node = get_product_details_by_productcode($productSku);
              if (!$node) {
                // Create new node when node not exist.
                $node = Node::create(['type' => 'commerce_product']);
              }
              $node->field_product_image_url = [];
              if (!empty($product['assets'])) {
                $product_assets_cnt = count($product['assets']);
                $product_assets_arr = $product['assets'];
                if (is_array($product_assets_arr) && $product_assets_cnt > 0) {
                  foreach ($product_assets_arr as $image_assets) {
                    if ($image_assets->type == 'ConfiguredOnWhite' || $image_assets->type == 'InContextShot' || $image_assets->type == 'Video' || $image_assets->type == 'OnWhiteShot') {
                      if (array_search($image_assets->url, array_column($node->field_product_image_url->getValue(), 'value')) !== TRUE) {
                        $node->field_product_image_url[] = ['value' => $image_assets->url];
                      }
                      else {
                        \Drupal::logger('sync asset type skipped')->notice(print_r($node->field_product_image_url->getValue(), 1));
                      }
                    }
                  }
                }
              }

              $node_arr = $node->toArray();
              if (!empty($node_arr['field_product_image'])) {
                $image_arr = $node_arr['field_product_image'];
                foreach ($image_arr as $image_list) {
                  $image = file_create_url(File::load($image_list['target_id'])->getFileUri());
                  if (array_search($image, array_column($node->field_product_image_url->getValue(), 'value')) !== FALSE) {

                  }
                  else {
                    $node->field_product_image_url[] = ['value' => $image];
                  }
                }
              }
              if (!empty($node_arr['field_product_video'])) {
                $video_arr = $node_arr['field_product_video'];
                foreach ($video_arr as $video_list) {
                  $video = file_create_url(File::load($video_list['target_id'])->getFileUri());
                  // $product_videos[] = $video;.
                  if (array_search($video, array_column($node->field_product_image_url->getValue(), 'value')) !== FALSE) {

                  }
                  else {
                    $node->field_product_image_url[] = ['value' => $video];
                  }
                }
              }
              $productAssetUrlValues = $node->field_product_image_url->getValue();
              $node->field_assets = [];
              $current = [];
              foreach ($productAssetUrlValues as $value) {
                $pathInfo = pathinfo($value['value']);
                if ($pathInfo['extension'] == 'mp4') {
                  $input = str_replace("https", "http", $value['value']);
                  $output = DRUPAL_ROOT . "/sites/peerless/files/video_thumbnails/" . $pathInfo['filename'] . ".png";

                  if ($ffmpegPath === 'nonterminal') {
                    $ffmpeg = \Drupal::service('php_ffmpeg');
                    $video = $ffmpeg->open($input);
                    $video->frame(TimeCode::fromSeconds(5))->save($output);
                    \Drupal::logger('sync_php_cmd')->notice(print_r($video, 1));
                  }
                  else {
                    exec("$ffmpegPath -y  -i {$input} -f mjpeg -vframes 1 -ss 5 {$output} 2>&1", $out);
                    \Drupal::logger('sync')->notice(print_r($out, 1));
                  }
                }
                $paragraph = Paragraph::create(['type' => 'peerless_asset_component']);
                $paragraph->set('field_image_url', $value['value']);
                $paragraph->isNew();
                $paragraph->save();
                $current[] = [
                  'target_id' => $paragraph->id(),
                  'target_revision_id' => $paragraph->getRevisionId(),
                ];
                $node->set('field_assets', $current);
              }
              $node->set('field_assets', $current);
              $node->set('title', $product['title']);
              $node->set('field_product_sku', $productSku);
              $node->set('field_main_category', $product['main_category']);
              // Attach categories  on products.
              $searchableFinish = '';
              if (is_object($product_obj)) {
                if (property_exists($product_obj->values, 'Finish')) {
                  \Drupal::logger('sync_finish_debug')->notice(print_r($product_obj, 1));
                  $searchableFinish = $product_obj->values->Finish;
                }
              }
              else {
                $searchableFinish = $product_obj['values']->Finish;
              }

              $searchableText = is_object($product_obj) ? $product_obj->searchableText : $product_obj['searchableText'];

              $node->set('field_meta_tags', serialize([
                                                        'description' => $name,
                                                        'keywords' => $searchableText,
                                                        'title' => $product['title'],
                                                      ]));
              $node->set('field_product_finish', $searchableFinish);
              $node->set('uid', 1);
              $node->status = 1;

              $node->save();
              $nid = $node->id();
              $categoryPath = 'kitchen';
              if ($main_category == 'Bath') {
                $categoryPath = 'bath';
              }
              $aliasPath = '/' . $categoryPath . '/product/' . $model_number;
	      PathAlias::create(['path' => "/node/".$nid, 'alias' => urldecode($aliasPath),'langcode' => 'en'])->save();
              \Drupal::cache()->delete('PDP_' . $model_number);
              if (array_key_exists("source", $term_arr)) {
                global $base_url;
                // Lets redirect to pdp page
                $url = \Drupal\Core\Url::fromRoute('entity.node.canonical', ['node' => $nid])->toString();
                $redirect = new RedirectResponse($base_url . $url, 301);
                return $redirect->send();
              }
              if (array_key_exists("syncType", $term_arr) && $term_arr['syncType'] == 'Specific'){
                return [];
              }
            }
            else {
              \Drupal::logger('sync node error')->notice(print_r($product_obj, 1));
            }
          }
        }
      }
    }
  }

  /**
   * Implementation of productImportFinished.
   */
  public static function productImportFinished($success, $results, $operations) {
    // The 'success' parameter means no fatal PHP errors were detected. All
    // other error management should be handled using 'results'.
    if ($success) {
      $message = self::getImportSuccessMessage();
    }
    else {
      $message = self::getImportFailureMessage();
    }
    \Drupal::messenger()->addMessage($message, 'error');

  }

   /*
   *  Implementation of product create instantly.
   */
  public static function createProductSpecific($siteUrl) {
      $skuCatArr['specific'] = 'specific';
      $method = 'GET';
      $headers = add_headers(FALSE);
      $data = $results = [];
      $response = web_services_api_call($siteUrl, $method, $headers, $data, 'PRODUCT-CREATE-SPECIFIC');
        $cat_arr = [];
        if ($response['status'] == 'SUCCESS' && $response['response'] != 'null') {
            $results = (array) json_decode($response['response']);
            $categoriesLst = $results['categories'];
            foreach ($categoriesLst as $urlalias) {
              $urlalias_seperate = explode("_", $urlalias);
              $sitefromcategory[] = $urlalias_seperate[0];
              if ($urlalias_seperate[0] == "Peerless") {
                $cat_arr[] = "Peerless_" . ucwords($urlalias_seperate[1]);
              }
            }
        if (empty($cat_arr)) {
          $skuCatArr['pattern'] = 'Peerless_Kitchen';
          $result = 'Peerless_Kitchen';
          $getApiParts = explode("/",$siteUrl);
          $sku = end($getApiParts);
          \Drupal::logger('Error - Create One Product')->notice("This product does not coming under peerless defined category list, So considering as default category. SKU - $sku");
        }
        else {
          $skuCatArr['pattern'] = current($cat_arr);
          $result = current($cat_arr);
        }
        $getTermParts = explode("_", $result);
        $skuCatArr['term'] = $getTermParts[1];
        return [$skuCatArr, $response];
      }
      else {
        $getApiParts = explode("/",$siteUrl);
        $sku = end($getApiParts);
        \Drupal::logger('Error - Create One Product')->notice("Product not exist in the api SKU - $sku");
        return [];
      }

  }

  /*
   *  Implementation of product create instantly.
   */
  public function createProduct($sku) {
    $product_status = get_product_details_by_productcode($sku);
    if (!empty($product_status)) {
      $nodeId = $product_status->id();
      $routeName = 'entity.node.canonical';
      $routeParameters = ['node' => $nodeId];
      $path = Url::fromRoute($routeName,$routeParameters)->toString();
      return new RedirectResponse($path);
    }else {
      $skuCatArr['source'] = 'manual';
      $skuCatArr['sku'] = $sku;
      $configPrimary = $this->configFactory->get('delta_services.deltaservicesconfig');
      $domain = $configPrimary->get('api_base_path');
      $webservice_config = $this->configFactory->get('sync.api_settings');
      $product_url = $domain . $webservice_config->get('sync.product_details_by_sku');
      $checkCatInStr = $webservice_config->get('sync.pl_categories');
      $checkCatInArr = explode(",", $checkCatInStr);
      if (empty($checkCatInArr)) {
        $checkCatInArr = ['Peerless_Kitchen'];
      }
      $siteUrl = str_replace(['{productSku}'], [$sku], $product_url);
      $method = 'GET';
      $headers = add_headers(FALSE);
      $data = $results = [];
      $response = web_services_api_call($siteUrl, $method, $headers, $data, 'PRODUCT-CREATE');
        $cat_arr = [];
        if ($response['status'] == 'SUCCESS' && $response['response'] != 'null') {
            $results = (array) json_decode($response['response']);
            $categoriesLst = $results['categories'];
            foreach ($categoriesLst as $urlalias) {
              $urlalias_seperate = explode("_", $urlalias);
              $sitefromcategory[] = $urlalias_seperate[0];
              if ($urlalias_seperate[0] == "Peerless") {
                $cat_arr[] = "Peerless_" . ucwords($urlalias_seperate[1]);
              }
            }
        if (empty($cat_arr)) {
          $skuCatArr['pattern'] = 'Peerless_Kitchen';
          $result = 'Peerless_Kitchen';
          \Drupal::logger('Error - Create One Product')->notice("This product does not coming under peerless defined category list, So considering as default category. SKU - $sku");
        }
        else {
          $skuCatArr['pattern'] = current($cat_arr);
          $result = current($cat_arr);
        }
        $getTermParts = explode("_", $result);
        $skuCatArr['term'] = $getTermParts[1];
        self::productsSync($skuCatArr, $siteUrl, $response);
      }
      else {
        \Drupal::logger('Error - Create One Product')->notice("Product not exist in the api SKU - $sku");
        $language = \Drupal::languageManager()->getLanguage('vi');
        $url = Url::fromRoute('<front>', [], ['language' => $language]);
        return new RedirectResponse($url->toString());
      }
    }
  }
  /**
   * Implementaion of all product import on menu callback (for cron)
   */
  public function importAllProduct($key = NULL) {
    $configPrimary = $this->config('delta_services.deltaservicesconfig');
    $webservice_config = $this->config('sync.api_settings');
    $cron_key = $webservice_config->get('sync.sync_cron_key');
    $return_response = new Response();
    if ($cron_key == $key) {
      $vid = 'product_category';
      $terms = (array) $this->entitytypemanager->getStorage('taxonomy_term')->loadTree($vid);
      foreach ($terms as $term) {
        $term_cat_obj = $this->entitytypemanager->getStorage('taxonomy_term')->load($term->tid);
        $category_path = (!empty($term_cat_obj->get('field_category_path')->value)) ? $term_cat_obj->get('field_category_path')->value : '';
        if (empty($category_path)) {
          continue;
        }
        $category_path_parts = explode("_", $category_path);
        $search_string = [];
        if (count($category_path_parts)) {
          foreach ($category_path_parts as $parts) {
            $search_string[] = $parts;
            if ($parts == $term->name) {
              break;
            }
          }
        }
        $pattern = implode('_', $search_string);
        $ancestors = (array) $this->entitytypemanager->getStorage('taxonomy_term')->loadAllParents($term->tid);
        $list = [];
        foreach ($ancestors as $term) {
          $list[$term->id()] = $term->id();
        }
        $term_data[] = [
          "term" => $list,
          "pattern" => $pattern,
        ];
      }
      $domain = $configPrimary->get('api_base_path');
      $product_search_url = $domain . $webservice_config->get('sync.product_fetch_category');
      if (count($term_data)) {
        foreach ($term_data as $term_arr) {
          ProductImportController::productImportAll($term_arr, $product_search_url);
        }
      }
      $message = $this->t('Product Sync success!');
      $this->logger->get('category_import')->info('@message', [
        '@message' => $message,
      ]);
      $return_response->setContent($message);
    }
    else {
      $message = $this->t('Synchronization down, please try again later.');
      $this->logger->get('category_import')->error('@message', [
        '@message' => $message,
      ]);
      $return_response->setContent($message);
    }
    return $return_response;
  }
}

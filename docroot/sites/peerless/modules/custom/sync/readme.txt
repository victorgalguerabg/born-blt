
To add category and product sync
--------------------------------

Step 1 : add name space in the sync.module file top
    use Drupal\sync_product\Form\ProductSync;
    use Drupal\sync_category\Controller\CategoryImportController;

Step 2 : add below code into sync.module
/*
 * Drupal hook_cron
 */

function sync_cron() {
  $response = CategoryImportController::drushCategoryImport();
  \Drupal::logger('Sync - Category')->notice(json_encode($response));
  
  $response = ProductSync::drushProductImport();
  \Drupal::logger('Sync - Product')->notice(json_encode($response));
}

<?php

namespace Drupal\sync\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\ConfigFormBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\file\Entity\File;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Configure settings for this site.
 */
class SyncSettingsForm extends ConfigFormBase implements ContainerInjectionInterface {

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The Config storage.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configfactory;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entityManager) {
    $this->configfactory = $config_factory;
    $this->entityTypeManager = $entityManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'sync_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['sync.api_settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('sync.api_settings');
    $form['category_call_api'] = [
      '#type' => 'details',
      '#title' => $this->t('Category API'),
      '#open' => FALSE,
      '#weight' => 2,
    ];

    $form['category_call_api']['facet_api'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Facet Sync API'),
      '#default_value' => $config->get('sync.facet_api'),
      '#required' => TRUE,
      '#size' => 100,
    ];

    $form['category_call_api']['category_api'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Category Sync API'),
      '#default_value' => $config->get('sync.category_api'),
      '#required' => TRUE,
      '#size' => 100,
    ];

    $form['category_call_api']['term_filter'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Name of the term to filter ex:Peerless'),
      '#default_value' => empty($config->get('sync.term_filter')) ? 'Peerless' : $config->get('sync.term_filter'),
      '#size' => 100,
    ];

    $form['category_call_api']['taxonomy_depth'] = [
      '#type' => 'select',
      '#title' => $this->t('Product category sync depth'),
      '#default_value' => $config->get('sync.taxonomy_depth'),
      '#options' => ['1' => 1, '2' => 2, '3' => 3],
      '#default_value' => empty($config->get('sync.taxonomy_depth')) ? 3 : $config->get('sync.taxonomy_depth'),
      '#description' => $this->t('1:Parent Terms, 2: Child Terms, 3:Sibling Terms'),
    ];

    $form['category_call_api']['pl_categories'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Peerless categories'),
      '#default_value' => $config->get('sync.pl_categories'),
      '#required' => TRUE,
      '#size' => 200,
    ];

    $form['category_call_api']['collection_detail_finish_api'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Collection Detail page Finish facet API'),
      '#description' => $this->t('Collection Detail API details for Finish excluding basepath.'),
      '#default_value' => $config->get('sync.collection_detail_finish_api'),
    ];
    $form['category_call_api']['collection_detail_api'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Collection Detail API'),
      '#description' => $this->t('Collection Detail API details excluding basepath.'),
      '#default_value' => $config->get('sync.collection_detail_api'),
    ];
    $form['category_call_api']['collection_detail_items_per_page'] = [
      '#type' => 'number',
      '#title' => $this->t('Items per Page'),
      '#description' => $this->t('Number of items per page in collection detail page.'),
      '#default_value' => $config->get('sync.collection_detail_items_per_page'),
    ];
    $form['category_call_api']['discontinue_ribbontext'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Discontinue Product Ribbon Text'),
      '#default_value' => $config->get('sync.discontinue_ribbontext'),
      '#size' => 100,
    ];
    $form['category_call_api']['comming_soon_ribbontext'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Coming Soon Ribbon Text'),
      '#default_value' => $config->get('sync.comming_soon_ribbontext'),
      '#size' => 100,
    ];
    $form['category_call_api']['cdp_kitchen_meta_title'] = [
      '#type' => 'textarea',
      '#title' => $this->t('CDP - Default Kitchen Meta Title'),
      '#default_value' => $config->get('sync.cdp_kitchen_meta_title'),
      '#description' => $this->t('Tokens : {name} is collection name'),
      '#size' => 100,
    ];
    $form['category_call_api']['cdp_kitchen_meta_desc'] = [
      '#type' => 'textarea',
      '#title' => $this->t('CDP - Default Kitchen Meta Desc'),
      '#default_value' => $config->get('sync.cdp_kitchen_meta_desc'),
      '#description' => $this->t('Tokens : {name} is collection name'),
      '#size' => 100,
    ];
    $form['category_call_api']['cdp_bath_meta_title'] = [
      '#type' => 'textarea',
      '#title' => $this->t('CDP - Default Bathroom Meta Title'),
      '#default_value' => $config->get('sync.cdp_bath_meta_title'),
      '#description' => $this->t('Tokens : {name} is collection name'),
      '#size' => 100,
    ];
    $form['category_call_api']['cdp_bath_meta_desc'] = [
      '#type' => 'textarea',
      '#title' => $this->t('CDP - Default Bathroom Meta Desc'),
      '#default_value' => $config->get('sync.cdp_bath_meta_desc'),
      '#description' => $this->t('Tokens : {name} is collection name'),
      '#size' => 100,
    ];


    $form['product_call_api'] = [
      '#type' => 'details',
      '#title' => $this->t('Product API'),
      '#open' => FALSE,
      '#weight' => 3,
    ];

     $form['product_call_api']['modelno_autocomplete'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Model Number Search'),
      '#default_value' => $config->get('sync.modelno_autocomplete'),
      '#required' => TRUE,
      '#size' => 200,
    ];

    $form['product_call_api']['ffmpeg_installation_path'] = [
      '#type' => 'textarea',
      '#title' => $this->t('FFMPEG Installation path for product sync'),
      '#description' => $this->t('For SYNC: Set as nonterminal, To generate video thumb using php_ffmpeg, Set ffmpeg path, to generate video with ffmpeg via terminal like /usr/bin/ffmpeg'),
      '#default_value' => $config->get('sync.ffmpeg_installation_path'),
      '#required' => TRUE,
    ];

    $form['product_call_api']['ffmpeg_installation_path_node'] = [
      '#type' => 'textarea',
      '#title' => $this->t('FFMPEG Installation path for product node update'),
      '#description' => $this->t('For Product Update: Set as nonterminal, To generate video thumb using php_ffmpeg, Set ffmpeg path, to generate video with ffmpeg via terminal like /usr/bin/ffmpeg -y  -i {sourceMp4} -f mjpeg -vframes 1 -ss 5 {destPng} 2>&1'),
      '#default_value' => $config->get('sync.ffmpeg_installation_path_node'),
      '#required' => TRUE,
    ];

    $form['product_call_api']['pdp_thumb_order'] = [
      '#type' => 'textarea',
      '#title' => $this->t('PDP Thumb order preference'),
      '#description' => $this->t('Use commas to separate words ex: OnWhite,ConfiguredOnWhite,video_thumbnails,InContext,peerless'),
      '#default_value' => $config->get('sync.pdp_thumb_order'),
    ];

    $form['product_call_api']['product_def_thumb_image'] = [
       '#type' => 'managed_file',
       '#title' => $this->t('Product - Video default thumb image'),
       '#default_value' => $config->get('sync.product_def_thumb_image'),
       '#size' => 100,
       '#upload_validators' => [
         'file_validate_is_image' => [],
         'file_validate_extensions' => ['gif png jpg jpeg'],
         'file_validate_size' => [25600000]
       ],
       '#upload_location' => 'public://product',
       '#attributes' => ['class' => ['photo-upload']],
     ];

    $form['product_call_api']['product_no_image'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('Product - No image'),
      '#default_value' => $config->get('sync.product_no_image'),
      '#size' => 100,
      '#upload_validators' => [
        'file_validate_is_image' => [],
        'file_validate_extensions' => ['gif png jpg jpeg'],
        'file_validate_size' => [25600000]
      ],
      '#upload_location' => 'public://product',
      '#attributes' => ['class' => ['photo-upload']],
    ];

    $form['product_call_api']['product_fetch_category'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Product Fetch Category'),
      '#default_value' => $config->get('sync.product_fetch_category'),
      '#required' => TRUE,
      '#size' => 200,
    ];

    $form['product_call_api']['plp_items_per_page'] = [
      '#type' => 'textfield',
      '#title' => $this->t('PLP Items per page'),
      '#default_value' => $config->get('sync.plp_items_per_page'),
      '#required' => TRUE,
      '#size' => 20,
    ];
    $form['product_call_api']['plp'] = [
      '#type' => 'textarea',
      '#title' => $this->t('PLP'),
      '#default_value' => $config->get('sync.plp'),
      '#required' => TRUE,
      '#size' => 200,
    ];

    $form['product_call_api']['product_details_by_sku'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Product Details By SKU'),
      '#default_value' => $config->get('sync.product_details_by_sku'),
      '#required' => TRUE,
      '#size' => 200,
    ];

    $form['product_call_api']['product_collections_api'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Product references by collections'),
      '#default_value' => $config->get('sync.product_collections_api'),
      '#required' => TRUE,
      '#size' => 200,
    ];

    $form['product_call_api']['load_multiple_products'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Load Multiple Products by SKU'),
      '#description' => $this->t('For multiple products add more products at end of the query string'),
      '#default_value' => $config->get('sync.load_multiple_products'),
      '#required' => TRUE,
      '#size' => 200,
    ];

    $form['product_call_api']['search_products'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Search Products'),
      '#default_value' => $config->get('sync.search_products'),
      '#required' => TRUE,
      '#size' => 200,
    ];

    $form['product_call_api']['global_search_products'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Global Search Products'),
      '#default_value' => $config->get('sync.global_search_products'),
      '#required' => TRUE,
      '#size' => 200,
    ];

    $form['product_call_api']['search_parts'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Search Parts'),
      '#default_value' => $config->get('sync.search_parts'),
      '#required' => TRUE,
      '#size' => 200,
    ];

    $form['product_call_api']['compare'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Product Compare'),
      '#default_value' => $config->get('sync.compare'),
      '#required' => TRUE,
      '#size' => 200,
    ];

    $form['product_call_api']['installation_type_text'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Installation type text'),
      '#default_value' => $config->get('sync.installation_type_text'),
      '#required' => TRUE,
    ];

    $form['product_call_api']['sync_cron_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Sync Cron key'),
      '#default_value' => $config->get('sync.sync_cron_key'),
      '#required' => TRUE,
      '#size' => 200,
    ];

    $form['document_content_api'] = [
      '#type' => 'details',
      '#title' => $this->t('Documents & content API'),
      '#open' => FALSE,
      '#weight' => 2,
    ];

    $form['document_content_api']['doc_disp_cnt'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Document Search - Pager count'),
      '#description' => $this->t('Search Results - Number of document to be display'),
      '#default_value' => $config->get('sync.doc_disp_cnt'),
      '#required' => TRUE,
      '#size' => 200,
    ];

    $form['document_content_api']['product_document_search'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Product Document Search'),
      '#default_value' => $config->get('sync.product_document_search'),
      '#required' => TRUE,
      '#size' => 200,
    ];

     $form['document_content_api']['cont_disp_cnt'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Content Search - Pager count'),
      '#description' => $this->t('Search Results - Number of content to be display'),
      '#default_value' => $config->get('sync.cont_disp_cnt'),
      '#required' => TRUE,
      '#size' => 200,
    ];



    $form['global_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Global Settings'),
      '#open' => FALSE,
      '#weight' => 2,
    ];

    $form['global_settings']['cache_exp_time'] = [
      '#type' => 'textfield',
      '#title' => $this->t('CACHE Expires Time'),
      '#default_value' => $config->get('sync.cache_exp_time'),
      '#size' => 100,
      '#description' => $this->t('Set cache Expires time in seconds, Ex: 1 min = 60 sec, 1 hr = 3600 sec 24 hr = 86400 sec'),
    ];

    $form['global_settings']['turn_log_off_on'] = [
      '#type' => 'select',
      '#title' => $this->t('Turn off / on log at api hit'),
      '#description' => $this->t('on or off'),
      '#options' => [ 'on' => $this->t('Turn On'), 'off' => $this->t('Turn Off')],
      '#default_value' => $config->get('sync.turn_log_off_on'),
      '#required' => TRUE,
    ];


    $form['global_settings']['whereToBuy'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Where to buy page Iframe'),
      '#default_value' => $config->get('sync.whereToBuy'),
      '#description' => $this->t('PDP to Where to buy'),
      '#required' => TRUE,
      '#size' => 200,
    ];

    $form['global_settings']['whereToBuyLocator'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Where to buy at PDP Iframe'),
      '#default_value' => $config->get('sync.whereToBuyLocator'),
      '#required' => TRUE,
      '#size' => 200,
    ];

    $form['global_settings']['technical_difficulties'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Technical difficulties mssage'),
      '#default_value' => $config->get('sync.technical_difficulties'),
      '#required' => TRUE,
      '#size' => 200,
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $getUserInput = $form_state->getUserInput();
    if (!empty($getUserInput['product_def_thumb_image'])) {
      $image = $getUserInput['product_def_thumb_image'];
      if (!empty($image['fids'])) {
        $file = $this->entityTypeManager->getStorage('file')->load($image['fids']);
        $file->setPermanent();
        $file->save();
      }
    }
    if (!empty($getUserInput['product_no_image'])) {
      $image = $getUserInput['product_no_image'];
      if (!empty($image['fids'])) {
        $file = $this->entityTypeManager->getStorage('file')->load($image['fids']);
        $file->setPermanent();
        $file->save();
      }
    }
    $this->config('sync.api_settings')
      ->set('sync.product_fetch_category', $form_state->getValue('product_fetch_category'))
      ->set('sync.cache_exp_time', $form_state->getValue('cache_exp_time'))
      ->set('sync.product_details_by_sku', $form_state->getValue('product_details_by_sku'))
      ->set('sync.plp_items_per_page', $form_state->getValue('plp_items_per_page'))
      ->set('sync.plp', $form_state->getValue('plp'))
      ->set('sync.category_api', $form_state->getValue('category_api'))
      ->set('sync.facet_api', $form_state->getValue('facet_api'))
      ->set('sync.taxonomy_depth', $form_state->getValue('taxonomy_depth'))
      ->set('sync.term_filter', $form_state->getValue('term_filter'))
      ->set('sync.product_collections_api', $form_state->getValue('product_collections_api'))
      ->set('sync.load_multiple_products', $form_state->getValue('load_multiple_products'))
      ->set('sync.search_products', $form_state->getValue('search_products'))
      ->set('sync.search_parts', $form_state->getValue('search_parts'))
      ->set('sync.product_def_thumb_image', $form_state->getValue('product_def_thumb_image'))
      ->set('sync.product_no_image', $form_state->getValue('product_no_image'))
      ->set('sync.ffmpeg_installation_path', $form_state->getValue('ffmpeg_installation_path'))
      ->set('sync.ffmpeg_installation_path_node', $form_state->getValue('ffmpeg_installation_path_node'))
      ->set('sync.pdp_thumb_order', $form_state->getValue('pdp_thumb_order'))
      ->set('sync.turn_log_off_on', $form_state->getValue('turn_log_off_on'))
      ->set('sync.installation_type_text', $form_state->getValue('installation_type_text'))
      ->set('sync.compare', $form_state->getValue('compare'))
      ->set('sync.modelno_autocomplete', $form_state->getValue('modelno_autocomplete'))
      ->set('sync.product_document_search', $form_state->getValue('product_document_search'))
      ->set('sync.doc_disp_cnt', $form_state->getValue('doc_disp_cnt'))
      ->set('sync.cont_disp_cnt', $form_state->getValue('cont_disp_cnt'))
      ->set('sync.global_search_products', $form_state->getValue('global_search_products'))
      ->set('sync.whereToBuy', $form_state->getValue('whereToBuy'))
      ->set('sync.whereToBuyLocator', $form_state->getValue('whereToBuyLocator'))
      ->set('sync.pl_categories', $form_state->getValue('pl_categories'))
      ->set('sync.collection_detail_finish_api', $form_state->getValue('collection_detail_finish_api'))
      ->set('sync.collection_detail_api', $form_state->getValue('collection_detail_api'))
      ->set('sync.collection_detail_items_per_page', $form_state->getValue('collection_detail_items_per_page'))
      ->set('sync.discontinue_ribbontext', $form_state->getValue('discontinue_ribbontext'))
      ->set('sync.comming_soon_ribbontext', $form_state->getValue('comming_soon_ribbontext'))
      ->set('sync.cdp_kitchen_meta_title', $form_state->getValue('cdp_kitchen_meta_title'))
      ->set('sync.cdp_kitchen_meta_desc', $form_state->getValue('cdp_kitchen_meta_desc'))
      ->set('sync.cdp_bath_meta_desc', $form_state->getValue('cdp_bath_meta_desc'))
      ->set('sync.cdp_bath_meta_title', $form_state->getValue('cdp_bath_meta_title'))
      ->set('sync.sync_cron_key', $form_state->getValue('sync_cron_key'))
      ->set('sync.technical_difficulties', $form_state->getValue('technical_difficulties'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}

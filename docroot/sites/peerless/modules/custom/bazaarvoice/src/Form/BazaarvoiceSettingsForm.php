<?php

namespace Drupal\bazaarvoice\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\ConfigFormBase;

/**
 * Configure settings for this site.
 */
class BazaarvoiceSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'bazaarvoice_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['bazaarvoice.api_settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('bazaarvoice.api_settings');

    $form['bazaarvoice_api_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Bazzarvoice API Settings'),
      '#open' => TRUE,
      '#weight' => 1,
    ];

    $form['bazaarvoice_api_settings']['site_environment'] = [
      '#type' => 'select',
      '#title' => $this->t('Environment'),
      '#default_value' => $config->get('bazaarvoice_api_settings.site_environment'),
      '#required' => TRUE,
      '#options' => ['staging' => 'Staging', 'production' => 'Production'],
    ];

    $form['bazaarvoice_api_settings']['cloud_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Cloud Key'),
      '#default_value' => $config->get('bazaarvoice_api_settings.cloud_key'),
      '#required' => TRUE,
    ];

    $form['bazaarvoice_api_settings']['site_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Site ID'),
      '#default_value' => $config->get('bazaarvoice_api_settings.site_id'),
      '#required' => TRUE,
    ];

    $form['bazaarvoice_api_settings']['site_locale'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Locale'),
      '#default_value' => $config->get('bazaarvoice_api_settings.site_locale'),
      '#required' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('bazaarvoice.api_settings')
      ->set('bazaarvoice_api_settings.site_environment', $form_state->getValue('site_environment'))
      ->set('bazaarvoice_api_settings.cloud_key', $form_state->getValue('cloud_key'))
      ->set('bazaarvoice_api_settings.site_id', $form_state->getValue('site_id'))
      ->set('bazaarvoice_api_settings.site_locale', $form_state->getValue('site_locale'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}

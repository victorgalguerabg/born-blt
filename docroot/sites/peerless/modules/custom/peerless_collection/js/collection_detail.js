(function ($, Drupal, drupalSettings, Backbone) {
 
  Drupal.behaviors.collectionDetailBehaviour = {
    attach: function (context, settings) {
      let size = drupalSettings.collectionProductsPerPage;
      let category = drupalSettings.collectionCategory;
      let collection = drupalSettings.collectionName;
      let finishData = drupalSettings.finishData;
      let pageNumber = 0;
      let filterFacets = '';
      let loadProducts = true;
      let lastPage = false;
      let appliedSort = '';
      let loader = $('.ajax-progress-throbber');
      $('.collection-details__page-header').once().on('change', '#priceFilter', function (){
        let sortValue = $(this).find('option:selected').val();
        if (typeof sortValue !== 'undefined') {
          appliedSort = sortValue;
        }else{
          appliedSort = appliedSort.replace(sortValue, "");
        }
        pageNumber = 0;
        applyFacets();
      });
      // On selection of finish facet corresponding value will be sent to API.
      $('.collection-details__page-header').on('change', '#finishFilter', function () {
        let finishValue = $(this).find('option:selected').val();
        if (typeof finishValue !== 'undefined') {
          if(finishValue === 'all'){
            var count = 1,
            facet_str = [];
            $('#finishFilter option').each(function(index){
              if (index > 1){
                let id = $(this).val();
                facet_str += '&facetFinish=' + id;
              }
            });
            filterFacets = facet_str;
          }
          else {
            filterFacets = finishValue;
          }
        }else{
          filterFacets = appliedFacet.replace(finishValue, "");
        }
        pageNumber = 0;
        applyFacets();
      });

      // Renders product data in template using backbone JS.
      let ProdView = Backbone.View.extend({
        el: "#output",
        template: _.template($("#output-node").html()),
        initialize: function (initData, append) {
          this.render(initData, append);
        },
        render: function (initData, append) {
          let _self = this;
          let _render = "";

          $("#output").addClass('grid');
            var collection_position = 0;
            _.each(initData.content, function (entry) {
              let collection_product = new Object();
              collection_position = collection_position + 1;
              collection_product.list = "Collection Results";
              if(entry.collections[0] != undefined) {
                  collection_product.brand = entry.collections[0];
              }
              else {
                  collection_product.brand = "";
              }
              collection_product.name = entry.description;
              collection_product.position = collection_position;
              collection_product.id = entry.name;
              collection_product.variant = entry.values.Finish;

              let _finishValue = entry.facetFinish;
              let _price = "Price";
              if (entry.values !== null) {
                _price = entry.values.ListPrice;
              }
              let _heroImage = entry.heroImageSmall;
              let _title = "Title";
              if (entry.description != '') {
                _title = entry.description;
              }
              let _collection = "COLLECTION NAME";
              if (entry.values !== null) {
                _collection = entry.values.Collection;
              }
              let _sku = entry.name;
              let _ribbonText = entry.ribbontext;
              let _ribbonColor = entry.ribboncolor;
              let _pdp = '/pdp/' + entry.name;
              _render += _self.template({
                title: _title,
                collectionName: _collection,
                heroImage: _heroImage,
                sku: _sku,
                price: _price,
                pdp: _pdp,
                ribbonText: _ribbonText,
                ribbonColor: _ribbonColor,
              });
            });
            if (append == true) {
              _self.$el.html(_render);
            }
            if (append == false) {
              _self.$el.append(_render);
            }           

          return this;
        },
      });
      //Facet View
      let facetView = Backbone.View.extend({
        el: "#facets",
        template: _.template($("#finish-facets").html()),
        initialize: function (initData, append) {
          let facetBlock = getFinishFacets(initData);
          this.render(facetBlock, append);
        },
        events: {
          'change #finishFilter': 'applyFacets',
        },
        applyFacets: function (e) {
          loadProducts = true;

        },
        render: function (facetBlock, append) {
          let _self = this;
          let _render = "";

          $("#finish-facets").addClass('cdp-finish');
          if (facetBlock != '') {
            $('#finish-filters').html(facetBlock);
          }
          else {
            // TBD.
            var noResultsMessage = '<div class ="error-message" role="contentinfo" aria-label="Error message">Oops!!! Nothing Found. Please refine your search</div>';
            $('#facets').html(noResultsMessage);
          }
          return this;
        }
        });


      function applyFacets(e) {
        loadProducts = true;
        loader.show();
        $.ajax({
          type: "GET",
          url: "/collection-listing",
          dataType: 'json',
          data: {
            size: size,
            category: category,
            collection: collection,
            facets: filterFacets,
            sort: appliedSort,
            pagenumber: pageNumber,
            cache: 'T',
          },
         error: function (XMLHttpRequest, textStatus, errorThrown) {
              console.log(XMLHttpRequest.status);
              console.log(textStatus);
              var noResultsMessage = '<div class ="cdp-api-error error-message" role="contentinfo" aria-label="Error message">Something has gone wrong on our end, please try refreshing this&nbsp;<a href="'+window.location.href+'">page</a>. If the issue persists, please try again later</div>';
              $('#output').html(noResultsMessage);
              $('.loader').hide();
              loader.hide();
              return false;
        },
          success: function (data) {
            lastPage = data.last;
            let response = new ProdView(data, true);
            loadProducts = true;
            $('.loader').hide();
            loader.hide();
          }
        });
      }
    jQuery( document ).ready(function() {
      if (drupalSettings.termfound != 'false'){
        pageNumber = 0;
        jQuery("#priceFilter").prop("selectedIndex", 1);
        apiCall();
      }
    });
    function apiCall(){
      loadProducts = false;
      loader.show();
      /* API call to get the products */
      $.ajax({
        type: "GET",
        url: "/collection-listing",
        dataType: 'json',
        data: {
          size: size,
          category: category,
          collection: collection,
          pagenumber: pageNumber,
          cache: 'T',
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
              console.log(XMLHttpRequest.status);
              console.log(textStatus);
              var noResultsMessage = '<div class ="cdp-api-error error-message" role="contentinfo" aria-label="Error message">Something has gone wrong on our end, please try refreshing this&nbsp;<a href="'+window.location.href+'">page</a>. If the issue persists, please try again later</div>';
              $('#output').html(noResultsMessage);
              $('.loader').hide();
              loader.hide();
              return false;
        },
        success: function (data) {
          if (data !== null && data.content != ''){
           let response = new ProdView(data, true);
           $('.loader').hide();
           loader.hide();
          }
          else{
            var noResultsMessage = '<div class ="error-message" role="contentinfo" aria-label="Error message">Oops!!! Nothing Found. Please refine your search</div>';
            $('#output').html(noResultsMessage);
            $('.loader').hide();
            loader.hide();
          }
          // if no products then set last as true
          lastPage = (data !== null) ? data.last : true;
          pageNumber++;
          loadProducts = true;
          $('.loader').hide();
          loader.hide();
        }
    });
      /* API call to get the finish facets */
      $.ajax({
        type: "GET",
        url: "/collection-finish-facets",
        dataType: 'json',
        data: {
          size: size,
          category: category,
          collection: collection,
          pagenumber: pageNumber,
          cache: 'T',
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
              console.log(XMLHttpRequest.status);
              console.log(textStatus);
              var noResultsMessage = '<div class ="cdp-api-error error-message" role="contentinfo" aria-label="Error message">Something has gone wrong on our end, please try refreshing this&nbsp;<a href="'+window.location.href+'">page</a>. If the issue persists, please try again later</div>';
              $('#output').html(noResultsMessage);
              $('.loader').hide();
              loader.hide();
              return false;
        },
        success: function (data) {
          if(data != null){
            let FacetView = new facetView(data);
          }
        }
      });
    }
    function getFinishFacets(initData){
      let output = '<option value="">Choose your finish</option><option value="all">All Finishes</option>';
      if(initData != null){
        let facetBlock = [];
        let count = 0;
          let facetName = initData.facets[0].name;
          let facetType = initData.facets[0].type;
          let facetCount = 0;
          if (facetType == 'term') {
            _.each(initData.facets[0].terms, function (terms) {
              var termLabel = findKey(finishData, terms.term);
              output += '<option value="' + terms.term + '">'+ termLabel + '</option>';
              facetCount++;
            });
          }
        }
        return '<select id="finishFilter" class="form-item__select form-item__select--sm">' + output + '</select>';
      }

      function findKey(input, target){
        var found;
        for (var prop in input) {
          if(prop == target){
            found = input[prop];
          }
        };
        return found;
      };
    }
  };
})
(jQuery, Drupal, drupalSettings, Backbone);

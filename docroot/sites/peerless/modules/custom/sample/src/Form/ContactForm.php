<?php

namespace Drupal\sample\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use CommerceGuys\Addressing\AddressFormat\AddressField;

/**
 * Class contactForm.
 */
class ContactForm extends Formbase {

  /**
   * Implementation of getFormId().
   */
  public function getFormId() {
    return 'contact_form';
  }

  /**
   * Implementation of build().
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['topic'] = [
      '#type' => 'select',
      '#title' => 'Topic',
      '#options' => [
        'select_a_topic' => 'Select a topic*',
        'cleaning_and_Care' => 'Cleaning and Care',
        'general' => 'General',
        'finish' => 'Finish',
        'replacement_parts' => 'Replacement Parts',

      ],
      '#default_value' => 'Select a topic*',
      '#required' => TRUE,
    ];
    $form['first_name'] = [
      '#type' => 'textfield',
      '#title' => t('First Name'),
      '#required' => TRUE,
      '#attributes' => ['placeholder' => t('First Name*')],
    ];
    $form['last_name'] = [
      '#type' => 'textfield',
      '#title' => t('Last Name'),
      '#required' => TRUE,
      '#attributes' => ['placeholder' => t('Last Name*')],
    ];
    $form['address1'] = [
      '#type' => 'textfield',
      '#title' => t('Address1'),
      '#required' => TRUE,
      '#attributes' => ['placeholder' => t('Address 1*')],
    ];
    $form['address2'] = [
      '#type' => 'textfield',
      '#title' => t('Address2'),
      '#attributes' => ['placeholder' => t('Address 2')],
    ];
    $form['city'] = [
      '#type' => 'textfield',
      '#title' => t('City'),
      '#required' => TRUE,
      '#attributes' => ['placeholder' => t('City*')],
    ];
    $form['state'] = [
      '#type' => 'address',
      '#default_value' => ['country_code' => 'US'],
      '#used_fields' => [
        AddressField::ADMINISTRATIVE_AREA,
      ],
      '#available_countries' => ['US', 'CA'],
    ];
    $form['postal_code'] = [
      '#type' => 'textfield',
      '#title' => t('Zip/Postal Code'),
      '#required' => TRUE,
      '#attributes' => ['placeholder' => t('Zip/Postal Code*')],
    ];
    $form['phone'] = [
      '#type' => 'textfield',
      '#title' => t('Phone Number'),
      '#attributes' => ['placeholder' => t('Phone Number')],
    ];
    $form['email'] = [
      '#type' => 'textfield',
      '#title' => t('Email Address'),
      '#required' => TRUE,
      '#attributes' => ['placeholder' => t('Email address*')],
    ];
    $form['model_number'] = [
      '#type' => 'textfield',
      '#title' => t('Model Number'),
      '#attributes' => ['placeholder' => t('Model Number')],
    ];
    $form['purchased_date'] = [
      '#type' => 'textfield',
      '#title' => t('Purchase Date'),
      '#attributes' => ['placeholder' => t('Purchase Date'), 'id' => 'datepicker'],
    ];
    $form['purchased_from'] = [
      '#type' => 'textfield',
      '#title' => t('Purchased From'),
      '#attributes' => ['placeholder' => t('Purchased From')],
    ];
    $form['help_you'] = [
      '#type' => 'textarea',
      '#title' => t('How can we help you?'),
      '#attributes' => ['placeholder' => t('How can we help you?*')],
      '#required' => TRUE,
    ];
    $form['photo_upload1'] = [
      '#type' => 'managed_file',
      '#title' => t('Photo Upload'),
      '#upload_validators' => [
        'file_validate_extensions' => ['gif png jpg jpeg'],
      ],
      '#upload_location' => 'public://',
    ];
    $form['photo_upload2'] = [
      '#type' => 'managed_file',
      '#title' => t('Photo Upload'),
      '#upload_validators' => [
        'file_validate_extensions' => ['gif png jpg jpeg'],
      ],
      '#upload_location' => 'public://',
    ];
    $form['photo_upload3'] = [
      '#type' => 'managed_file',
      '#title' => t('Photo Upload'),
      '#upload_validators' => [
        'file_validate_extensions' => ['gif png jpg jpeg'],
      ],
      '#upload_location' => 'public://',
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => t('Submit'),
    ];
    $form['actions']['reset'] = [
      '#type' => 'button',
      '#button_type' => 'reset',
      '#value' => t('Reset'),
    ];
    return $form;
  }

  /**
   * Implementation of contact submit.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

  }

}

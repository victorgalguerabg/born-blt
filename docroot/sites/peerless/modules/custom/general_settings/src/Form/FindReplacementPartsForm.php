<?php

namespace Drupal\general_settings\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Url;

/**
 * Implementation Find and replacement form.
 */
class FindReplacementPartsForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'find_replacement_parts_form';
  }

  /**
   * Implementation Find and replacement form build().
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = [];
    $form['search'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#attributes' => [
        'placeholder' => $this->t('Search ...'),
        'class' => ['search-icon'],
        'data-url' => $this->t('url'),
      ],
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => 'Search',
      '#attributes' => ['class' => ['search-icon']],
    ];

    $form['#theme'] = 'find_replacement_parts';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Route to the search page with searched keywords.
    $data = Xss::filter($form_state->getValue('search'));
    $url = Url::fromRoute('support_search.result', ['title' => $data, 'source' => 'support']);
    $form_state->setRedirectUrl($url);
  }

}

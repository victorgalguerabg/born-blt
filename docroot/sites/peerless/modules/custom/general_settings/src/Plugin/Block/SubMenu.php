<?php

namespace Drupal\general_settings\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides my custom block.
 *
 * @Block(
 *   id = "submenu_kitchen_block",
 *   admin_label = @Translation("Submenu Kitchen Block"),
 *   category = @Translation("Blocks")
 * )
 */
class SubMenu extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [
      '#markup' => 'mega_menu',
    ];

    return $build;
  }

}

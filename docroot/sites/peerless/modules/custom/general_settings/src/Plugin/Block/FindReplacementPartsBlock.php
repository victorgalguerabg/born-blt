<?php

namespace Drupal\general_settings\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Render\Renderer;

/**
 * Provides my custom block.
 *
 * @Block(
 *   id = "find_replacement_parts_block",
 *   admin_label = @Translation("Find Replacement Parts Block"),
 *   category = @Translation("Blocks")
 * )
 */
class FindReplacementPartsBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Class constructor.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, FormBuilderInterface $form_builder, Renderer $renderer) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->formbuilder = $form_builder;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    // Instantiates this form class.
    return new static(
    // Load the service required to construct this class.
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('form_builder'),
      $container->get('renderer')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $form = $this->formbuilder->getForm('Drupal\general_settings\Form\FindReplacementPartsForm');
    return [
      '#type' => 'markup',
      '#markup' => render($form),
      '#cache' => [
        'max-age' => 0,
      ],
    ];
  }

}

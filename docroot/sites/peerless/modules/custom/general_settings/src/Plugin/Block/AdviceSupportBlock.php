<?php

namespace Drupal\general_settings\Plugin\Block;

use Drupal\Core\Url;
use Drupal\Core\Block\BlockBase;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Config\Config;

/**
 * Provides my custom block.
 *
 * @Block(
 *   id = "advice_support_block",
 *   admin_label = @Translation("Advice & Support"),
 *   category = @Translation("Blocks")
 * )
 */
class AdviceSupportBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new SystemMenuBlock.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entityTypeManager service.
   */
  public function __construct(array $configuration, $plugin_id, array $plugin_definition, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = \Drupal::config('system.site');
    $front_uri = $config->get('page.front');
    $alias = \Drupal::service('path_alias.manager')->getAliasByPath($front_uri);
    $path = \Drupal::service('path_alias.manager')->getPathByAlias($alias);
    $getNodeParts = explode("/", $path);
    $node = null;
    if (is_array($getNodeParts) && count($getNodeParts) === 3) {
      $node = $this->entityTypeManager->getStorage('node')->load($getNodeParts[2]);
      $advice_support_header = 'Advice & Support';
    }

    $advice_support_arr = [];
    if ($node != null) {
      $nodeArr = $node->toArray();
      if (!empty($nodeArr['field_advice_support_heading'])) {
        $advice_support_header = $nodeArr['field_advice_support_heading'][0]['value'];
      }

      $advice_support_arr = [];
      if ($node->hasField('field_advice_support')) {
        $iterateFields = $nodeArr['field_advice_support'];
        foreach ($iterateFields as $iterateFieldsVal) {
          $paragraphId = $iterateFieldsVal['target_id'];
          $paragaphObj = Paragraph::load($paragraphId);
          if ($paragaphObj == null) {
            return;
          }
          $paragaphObjArr = $paragaphObj->toArray();
          $image = $action_link = $action_link_txt = $heading_txt = '';
          if (!empty($paragaphObjArr['field_action_link'])) {
            $link = $paragaphObjArr['field_action_link'][0]['uri'];
            $action_link = Url::fromUri($link);
            $action_link_txt = $paragaphObjArr['field_action_link'][0]['title'];
          }
          if (!empty($paragaphObjArr['field_callout_image'])) {
            $image_id = $paragaphObjArr['field_callout_image'][0]['target_id'];
            $image_data = $this->entityTypeManager->getStorage('file')->load($image_id);
            $image = file_create_url($image_data->get('uri')->getValue()[0]['value']);
          }
          if (!empty($paragaphObjArr['field_heading_text'])) {
            $heading_txt = $paragaphObjArr['field_heading_text'][0]['value'];
          }
          $advice_support_arr[] = [
            'action_link' => $action_link,
            'action_link_txt' => $action_link_txt,
            'image' => ['src' => $image, 'alt' => basename($image)],
            'heading_txt' => $heading_txt,
          ];
        }
      }
    }
    return [
      '#theme' => 'advice_support',
      '#header' => $advice_support_header,
      '#items' => $advice_support_arr,
      '#cache' => [
        'max-age' => 0,
      ],
    ];
  }

}

<?php

namespace Drupal\product_details\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceFormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\product_details\Controller\ProductController;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Field\FieldDefinitionInterface;

/**
 * Plugin implementation of the 'entity reference rendered entity' formatter.
 *
 * @FieldFormatter(
 *   id = "entity_reference_home_featured_products_widget",
 *   label = @Translation("Home Page - Featured Products Widget"),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class HomeFeaturedProductsEntityReferenceFormatter extends EntityReferenceFormatterBase implements ContainerFactoryPluginInterface {

  /**
   * To access pdp methods using services.
   *
   * @var pdpServices
   */
  protected $pdpServices;

  /**
   * Constructor with Dependency Injection.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, ProductController $pdp) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);

    $this->pdpServices = $pdp;
  }

  /**
   * Function create.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   Container interface.
   * @param array $configuration
   *   Block Configuration.
   * @param string $plugin_id
   *   Block Plugin id.
   * @param mixed $plugin_definition
   *   Block Definition.
   *
   * @return static
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      // Add any services you want to inject here.
      $container->get('pdp')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $featuredProductsSkus = [];
    $featuredProducts = [];

    $refProducts = $items->referencedEntities();
    foreach ($refProducts as $refProduct) {
      $featuredProductsSkus[] = $refProduct->get('field_product_sku')->value;
    }
    if (count($featuredProductsSkus) > 0) {
      $featuredProducts['productReferences'] = $this->pdpServices->getMultipleProducts($featuredProductsSkus);
    }
    return [
      '#theme' => 'home_feature_products',
      '#data' => $featuredProducts,
    ];
  }

}

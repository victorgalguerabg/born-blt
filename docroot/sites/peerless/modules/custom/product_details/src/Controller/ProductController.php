<?php

namespace Drupal\product_details\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\paragraphs\Entity\Paragraph;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\file\Entity\File;
use Drupal\Core\Cache\CacheBackendInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Drupal\delta_custom\GlobalProduct;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Controller routines for user routes.
 */
class ProductController extends ControllerBase {

      /**
  * Drupal\global_product\GlobalProduct definition.
  *
  * @var \Drupal\global_product\GlobalProduct
  */
  protected $globalproduct;


  /**
   * Implementation of EntityTypeManagerInterface.
   *
   * @var Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entitytypemanager;

  /**
   * DB connection resource.
   *
   * @var connection
   */
  protected $connection;

  /**
   * To get path alias.
   *
   * @var service
   */
  public $service;

  /**
   * To get current query string.
   *
   * @var Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cacheBackend;

  /**
   * The default cache bin.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * Class constructor.
   */
  public function __construct(RequestStack $requestStack, Connection $connection, ContainerInterface $service, EntityTypeManagerInterface $entitytypemanager, CacheBackendInterface $cache, GlobalProduct $globalproduct) {
    $this->requestStack = $requestStack;
    $this->connection = $connection;
    $this->service = $service;
    $this->entitytypemanager = $entitytypemanager;
    $this->cache = $cache;
    $this->globalproduct = $globalproduct;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
    // Load the service required to construct this class.
      $container->get('request_stack'),
      $container->get('database'),
      $container->get('service_container'),
      $container->get('entity_type.manager'),
      $container->get('cache.default'),
      $container->get('global_product.service')
    );
  }

  /**
   * Fetch the product detail page.
   */
  public function productDetailsPage($sku, $mainCategory, $node) {
    $config = $this->config('sync.api_settings');
    $cidExpTime = $config->get('sync.cache_exp_time');
    $deltaConfig = $this->config('delta_services.deltaservicesconfig');
    $validateOrderShipDate = $deltaConfig->get('order_ship_date');
    $technical_difficulties = $config->get('sync.technical_difficulties');
    $pageNotFoundNid = $this->config('system.site')->get('page.404');
    $pageNotFound = $this->service->get('path_alias.manager')->getAliasByPath($pageNotFoundNid);
    $cidExpTime = \Drupal::time()->getRequestTime() + $cidExpTime;
    $cid = 'peer_pdp_' . $sku;
    $output = '';
    $outputObj = $this->cache->get($cid);
    if ((!empty($outputObj)) && property_exists($outputObj, 'cid')) {
      $this->getLogger('PDP API Hit Error')->notice('PDP cache id' . $cid);
      $output = $outputObj->data;
      $bimsmithAPI = $deltaConfig->get('bimsmith');
      $bimsmithResp = ['status' => FALSE ];
      if(strlen(trim($bimsmithAPI)) > 0){
        $getBaseModelNo = 0;
        if(is_array($output) && array_key_exists('values', $output)){
          if(is_object($output['values']) && property_exists($output['values'], 'BaseModel')){
            $getBaseModelNo = $output['values']->BaseModel;
          }
        }
        //if(is_array($output) && array_key_exists('defaultModelNumber', $output)){
          //$getBaseModelNo = $output['defaultModelNumber'];
        //}
        $bimsmithAPI = str_replace('{bmSKU}', $getBaseModelNo, $bimsmithAPI);
        $bimsmithResp = bimsmith_apicall($bimsmithAPI);
      }
      $output['bimsmith'] = $bimsmithResp;
      $build['output'] = [
        '#theme' => 'product_details_page',
        '#data' => $output,
      ];
      return $build;
    }
    // Cache pdp information.
    $configPrimary = $this->config('delta_services.deltaservicesconfig');
    $domain = $configPrimary->get('api_base_path');
    $pdpUrl = $domain . $config->get('sync.product_details_by_sku');
    $pdpUrl = str_replace("{productSku}", $sku, $pdpUrl);
    $pdpThumbOrder = $config->get('sync.pdp_thumb_order');
    if (!empty($pdpThumbOrder)) {
      $pdpThumbOrder = explode(',', $pdpThumbOrder);
      $pdpThumbOrder = array_filter($pdpThumbOrder);
    }
    else {
      $pdpThumbOrder = ['OnWhite', 'ConfiguredOnWhite', 'video_thumbnails', 'InContext', 'peerless', 'iamnew'];
    }
    $videoThumbImg = '';
    $defaultThumbIcon = $config->get('sync.product_def_thumb_image');
    if (is_array($defaultThumbIcon) && (!empty($defaultThumbIcon))) {
      $fid = $defaultThumbIcon[0];
      $imagePathArr = $this->entitytypemanager()->getStorage('file')->load($fid)->toArray();
      $videoThumbImg = file_create_url($imagePathArr['uri'][0]['value']);
    }
    $output = [];
    $method = 'GET';
    $headers = add_headers(FALSE);
    $response = web_services_api_call($pdpUrl, $method, $headers, '', 'PDP');
    $build = [];
    if ($response['status'] == 'SUCCESS' && $response['response'] != 'null') {
      $data = json_decode($response['response']);

      //Redirect to 404, when the AvailableToOrderDate /  AvailableToShipDate is 9999-12-31
      if((!(empty($data->values->AvailableToOrderDate)) && ($data->values->AvailableToOrderDate === $validateOrderShipDate)) || ((!(empty($data->values->AvailableToShipDate))) && ($data->values->AvailableToShipDate === $validateOrderShipDate)))
      {
        throw new NotFoundHttpException();
        exit;
      }

      if(property_exists($data->values, 'WebsitePeerlessUS')){
        if(((empty($data->values->WebsitePeerlessUS)) || ($data->values->WebsitePeerlessUS !== 'T'))){
             throw new NotFoundHttpException();
             exit;
         }
       } else {
            throw new NotFoundHttpException();
            exit;
      }
      $output['heroImage'] = (property_exists($data, 'heroImage')) ? $data->heroImage : '';
      $output['name'] = (property_exists($data, 'description')) ? $data->description : '';
      $output['model'] = $data->name;
      $output['collections'] = 'PEERLESS';

      if (count($data->collections) > 0) {
        $output['collections'] = strtoupper(implode(", ", $data->collections));
        $output['collectionsUrl'] = (!empty(self::getCollectionUrl($data->collections))) ? self::getCollectionUrl($data->collections) : '#';
      }
      // Print finish images on pdp.
      $getFinishSkus = $this->getPDPFinishParts($data->finishOptions);
      if(!empty($getFinishSkus)){
        $i = 1;
        if (count($data->finishOptions) > 0) {
          foreach ($data->finishOptions as $finish) {
            if(in_array($finish->modelNumber, $getFinishSkus)){
              $finish->productUrl = (self::getProductUrl($finish->modelNumber) == '#') ? '/pdp/' . $finish->modelNumber : self::getProductUrl($finish->modelNumber);
              $finish->productIcon = get_taxonomy_using_name($finish->facetFinish, 'facetfinish', 'icon');
              if ($finish->modelNumber == $data->name) {
                $output['finishOptions'][0] = $finish;
              }else{
                $output['finishOptions'][$i] = $finish;
                $i++;
              }
            }
          }
        }
      }

      if (!empty($data->values->ListPrice)) {
        $output['listPrice'] = $data->values->ListPrice;
      }
      else {
        $output['listPrice'] = '';
      }



      $output['buyUrl'] = Url::fromRoute('product_details.where2buy', [], ['query' => ['modelNumber' => $data->name ]]);
      $showDiscontinue = 0;
      $comingSoonFlat = 0;

      // Discontinued logic.
      $discontinued = ((!empty($data->values->MaterialStatus)) && $data->values->MaterialStatus >= 50) ? TRUE : FALSE;
      if ($discontinued) {
        $showDiscontinue = 1;
      }
      // Product coming Soon
      $orderDate = (!empty($data->values->AvailableToOrderDate)) ? strtotime($data->values->AvailableToOrderDate) : '';
      $shipDate = (!empty($data->values->AvailableToShipDate)) ? strtotime($data->values->AvailableToShipDate) : '';
      $currentDate = strtotime(date('Y-m-d', time()));
      if (empty($orderDate) || empty($shipDate)) {
        $shipDate = $currentDate - (60 * 60 * 24);
      }
      if ($currentDate < $shipDate) {
        if ($currentDate > $orderDate) {
          $comingSoonFlat = 1;
        }
      }


      // Ribbon Text.
      $ribbonText = '';
      $ribbonVal = $data->values->WebExclusiveCustomerItem;
      if (!empty($ribbonVal)) {
        if ($ribbonVal != 'NA' &&
          $ribbonVal != 'OEM' &&
          $ribbonVal != 'Retail' &&
          $ribbonVal != 'Warranty' &&
          $ribbonVal != 'Internal Use Only' &&
          $discontinued == FALSE
        ) {
          $ribbonText = "Only at " . str_replace("_", " and ", $ribbonVal);
        }
      }
      $output['ribbonText'] = $ribbonText;
      $patterns = [' ', "'"];
      $ribbonVal = str_replace($patterns, '-', strtolower($ribbonVal));
      $output['ribbonTextColor'] = $ribbonVal;
      $output['comingSoonFlag'] = 0;
      if ($comingSoonFlat === 1) {
        $output['ribbonText'] = "coming soon";
        $output['ribbonTextColor'] = 'comingsoon';
        $output['comingSoonFlag'] = 1;
      }
      if ($showDiscontinue === 1) {
        $output['ribbonText'] = "DISCONTINUED";
        $output['ribbonTextColor'] = 'discontinued';
      }
      $faturesBenefitsResult = self::getProductFeature($data);
      if (!empty($faturesBenefitsResult)) {
        $output['faturesBenefits'] = $faturesBenefitsResult;
      }

      if (!empty($data->facetFeature)) {
        $facetFeature = explode(" ", $data->facetFeature);
        $facetFeatureResult = self::getFacetFeature($facetFeature);
        $output['facetFeature'] = $facetFeatureResult;
      }

      $productDetails = self::getProductDetails($data);
      if (!empty($productDetails)) {
        $output['productDetails'] = $productDetails;
      }
      $productCertification = self::getProductCertification($data, $config);
      if (!empty($productCertification)) {
        $output['productCertification'] = $productCertification;
      }
      if (count($data->assets) > 0) {
        foreach ($data->assets as $assets) {

          if (!empty($assets->smallUrl) && ($assets->type == 'ConfiguredOnWhite' || $assets->type == 'InContextShot' || $assets->type == 'OnWhiteShot')) {
            $assets->mediaType = 'image';
            $output['assets']['images'][] = $assets;
          }
          if ($assets->type == 'MandI') {
            $output['assets']['installation'][] = $assets;
          }
          if ($assets->type == 'SpecSheet') {
            $output['assets']['specsheet'][] = $assets;
          }
          if ($assets->type == 'PartsDiagram') {
            $output['assets']['diagram'][] = $assets;
          }
          if ($assets->type == 'Video') {
            $output['assets']['Video'][] = $assets;
          }
        }
      }
      $output['values'] = $data->values;
      //Commenting the below block of lines to get the image assets from api and not from database, since we always hit api,
      // its better to use api values instead.

      /*$mainImages = $node->get('field_assets')->getValue();
      if (count($mainImages) > 0) {
        $output['assets']['images'] = [];
        foreach ($mainImages as $i => $images) {
          $paragraph = Paragraph::load($images['target_id']);
          $assetUrl = $paragraph->get('field_image_url')->getValue()[0]['value'];
          $pathInfo = pathinfo($paragraph->get('field_image_url')->getValue()[0]['value']);
          if ($pathInfo['extension'] == 'mp4') {
            global $base_root;
            $output['assets']['images'][$i]['mediaType'] = 'video';
            $output['assets']['images'][$i]['url'] = $assetUrl;
            $thumbIcon = "/sites/peerless/files/video_thumbnails/" . $pathInfo['filename'] . ".png";
            $thumbFullPath = getcwd() . $thumbIcon;
            if (file_exists($thumbFullPath)) {
              $output['assets']['images'][$i]['smallUrl'] = $thumbIcon;
            }
            else {
              //Lets Check in configuration default video thumb image uploaded.
              $defaultThumbIcon = $config->get('sync.product_def_thumb_image');
              if (is_array($defaultThumbIcon) && (!empty($defaultThumbIcon))) {
                $fid = $defaultThumbIcon[0];
                $imagePathArr = $this->entitytypemanager()->getStorage('file')->load($fid)->toArray();
                $defaultThumbIcon = file_create_url($imagePathArr['uri'][0]['value']);
                $output['assets']['images'][$i]['smallUrl'] = $defaultThumbIcon;
                $output['assets']['images'][$i]['videoThumbDefault'] = 1;
              }
            }
          }
          else {
            $output['assets']['images'][$i]['mediaType'] = 'image';
            $output['assets']['images'][$i]['url'] = $assetUrl;
            $output['assets']['images'][$i]['smallUrl'] = $assetUrl;
            if ($paragraph->hasField('field_image_alt') && (!empty($paragraph->get('field_image_alt')->getValue()))) {
              $output['assets']['images'][$i]['altTag'] = $paragraph->get('field_image_alt')->getValue()[0]['value'];
            }
            else {
              $output['assets']['images'][$i]['altTag'] = basename($assetUrl);
            }
          }
        }
      }*/
      // User manually uploaded images files.
      $manualImages = $node->get('field_product_image')->getValue();
      if (is_array($manualImages) && count($manualImages)) {
        // $i = ($i == 0) ? $i = 1 : $i++;
        foreach ($manualImages as $userImages) {
          $imgId = $userImages['target_id'];
          $imgFile = File::load($imgId);
          if (!is_null($imgFile)) {
            $imgUri = $imgFile->toArray()['uri'][0]['value'];
            $imgUrl = file_create_url($imgUri);
            if (!empty($imgUrl)) {
              $output['assets']['images'][] = ['mediaType' => 'image', 'url' => $imgUrl, 'smallUrl' => $imgUrl];
            }
          }
        }
      }

      if(array_key_exists('Video', $output['assets'])){
        $videoArray = $output['assets']['Video'];
        if(is_array($videoArray) && count($videoArray)){
          foreach ($videoArray as $keyVideo => $valueVideo) {
            $output['assets']['images'][] = [
              'id' => 'video_thumbnails',
              'mediaType' => 'video',
              'url' => $valueVideo->url,
              'smallUrl' => $videoThumbImg,
              'videoThumbDefault' =>  1
            ];
          }
        }
      }

      // User manually uploaded video files.
      $product_video = $node->get('field_product_video')->getValue();
      if (count($product_video) > 0) {
        $i = ($i == 0) ? $i = 1 : $i++;
        foreach ($product_video as $images) {
          $videoId = $images['target_id'];
          $videoFile = File::load($videoId);
          if (!is_null($videoFile)) {
            $videoUri = $videoFile->toArray()['uri'][0]['value'];
            $videoUrl = file_create_url($videoUri);
            $videoThumnailArray = explode("/", $videoUrl);
            $videoName = explode(".", array_pop($videoThumnailArray));
            $pathInfo = pathinfo($videoUrl);
            if ($pathInfo['extension'] == 'mp4') {
              $videoThumb = "/sites/peerless/files/video_thumbnails/" . $pathInfo['filename'] . ".png";
              $thumbFullPath = getcwd() . $videoThumb;
              if (file_exists($thumbFullPath)) {
                $output['assets']['images'][] = [
                  'mediaType' => 'video',
                  'url' => $videoUrl,
                  'smallUrl' => $videoThumb,
                  'videoThumbDefault' => 0
                ];

              }
              else {
                //Lets Check in configuration default video thumb image uploaded.
                $defaultThumbIcon = $config->get('sync.product_def_thumb_image');
                if (is_array($defaultThumbIcon) && (!empty($defaultThumbIcon))) {
                  $fid = $defaultThumbIcon[0];
                  $imagePathArr = $this->entitytypemanager()->getStorage('file')->load($fid)->toArray();
                  $defaultThumbIcon = file_create_url($imagePathArr['uri'][0]['value']);
                  $output['assets']['images'][] = [
                    'mediaType' => 'video',
                    'url' => $videoUrl,
                    'smallUrl' => $defaultThumbIcon,
                    'videoThumbDefault' => 1
                  ];
                }
              }

            }
          }
        }
      }

      // Let sort the image url.
      $imageSort = [];
      if (array_key_exists("assets", $output)) {
        foreach ($output['assets']['images'] as $getAssetInfo) {
          $getImageUrl = ((is_array($getAssetInfo)) && array_key_exists('smallUrl', $getAssetInfo)) ? $getAssetInfo['smallUrl'] :  $getAssetInfo->smallUrl;
          $getImageUrlParts = explode("/", $getImageUrl);
          $imageSort[] = [
            'id' => $getImageUrlParts[4],
            'mediaType' => ((is_array($getAssetInfo)) && array_key_exists('mediaType', $getAssetInfo)) ? $getAssetInfo['mediaType'] :  $getAssetInfo->mediaType,
            'url' => ((is_array($getAssetInfo)) && array_key_exists('url', $getAssetInfo)) ? $getAssetInfo['url'] :  $getAssetInfo->url,
            'smallUrl' => ((is_array($getAssetInfo)) && array_key_exists('smallUrl', $getAssetInfo)) ? $getAssetInfo['smallUrl'] :  $getAssetInfo->smallUrl,
            'videoThumbDefault' => ((is_array($getAssetInfo)) && array_key_exists('videoThumbDefault', $getAssetInfo)) ? $getAssetInfo['videoThumbDefault'] : 0,
            'altTag' => ((is_array($getAssetInfo)) && array_key_exists('altTag', $getAssetInfo)) ? $getAssetInfo['altTag'] : $sku,
          ];
        }
      }
      $imageSortCleand = [];
      foreach ($pdpThumbOrder as $orderName) {
        foreach ($imageSort as $specialOrderImg) {
          if ($orderName == $specialOrderImg['id']) {
            $imageSortCleand['images'][] = [
              'mediaType' => $specialOrderImg['mediaType'],
              'url' => $specialOrderImg['url'],
              'smallUrl' => $specialOrderImg['smallUrl'],
              'altTag' => $specialOrderImg['altTag'],
              'videoThumbDefault' => ((is_array($specialOrderImg)) && array_key_exists('videoThumbDefault', $specialOrderImg)) ? $specialOrderImg['videoThumbDefault'] : 0,
            ];
          }
        }
      }

      $installationArr = ((is_array($output['assets'])) && array_key_exists('installation', $output['assets'])) ? $output['assets']['installation'] : [];
      $diagramArr = ((is_array($output['assets'])) && array_key_exists('diagram', $output['assets'])) ? $output['assets']['diagram'] : [];
      $specsheetArr = ((is_array($output['assets'])) && array_key_exists('specsheet', $output['assets'])) ? $output['assets']['specsheet'] : [];

      if (!empty($imageSortCleand) &&  is_array($imageSortCleand['images'])) {
        $output['assets'] = $imageSortCleand;
      }
      $output['assets']['installation'] = $installationArr;
      $output['assets']['diagram'] = $diagramArr;
      $output['assets']['specsheet'] = $specsheetArr;

      $output['assets']['noimage'] = 0;
      if (empty($output['assets']['images'])) {
        $output['assets']['noimage'] = 1;
        $noImagePath = $config->get('sync.product_no_image');
        if (is_array($noImagePath) && (!empty($noImagePath))) {
          $fid = $noImagePath[0];
          $imagePathArr = $this->entitytypemanager()->getStorage('file')->load($fid)->toArray();
          $noImageUri = file_create_url($imagePathArr['uri'][0]['value']);
          $output['assets']['url'] = $noImageUri;
        }
        else {
          $output['assets']['url'] = $output['heroImage'];
        }
        $output['assets']['altTag'] = 'No Image';
      }
      if ($data->facetHoles == 'NA' || $data->facetHoles == 'na') {
        $holesforInstallation = 'N/A';
      }
      else {
        $holesforInstallation = get_taxonomy_using_name($data->facetHoles, 'facetholes', 'both');
      }
      $output['holes4installation'] = $holesforInstallation;
      // Product References.
      $productType = (!empty($mainCategory)) ? $mainCategory : 'bath';
      $collection = (!empty($data->values->defaultCollection)) ? $data->values->defaultCollection : 'Peerless';
      $productReferences = self::getProductReference($collection, $productType);
      $output['productReferences'] = $productReferences;
      $bazaarvoice_settings = 'bazaarvoice.api_settings';
      // Product Reviews and Ratings.
      $config = $this->config($bazaarvoice_settings);
      $cloudKey = $config->get('bazaarvoice_api_settings.cloud_key');
      $siteEnvironment = $config->get('bazaarvoice_api_settings.site_environment');
      $siteId = $config->get('bazaarvoice_api_settings.site_id');
      $siteLocale = $config->get('bazaarvoice_api_settings.site_locale');
      $bazaarvoice = [
        'cloudKey' => $cloudKey,
        'siteEnvironment' => $siteEnvironment,
        'siteId' => $siteId,
        'siteLocale' => $siteLocale,
      ];
      $bimsmithAPI = $deltaConfig->get('bimsmith');
      $bimsmithResp = ['status' => FALSE ];
      if(strlen(trim($bimsmithAPI)) > 0){
        $getBaseModelNo = 0;
        if(is_object($data->values) && property_exists($data->values, 'BaseModel')){
           $getBaseModelNo = $data->values->BaseModel;
        }
        //if(is_object($data) && property_exists($data, 'defaultModelNumber')){
          //$getBaseModelNo = $output['defaultModelNumber'] = $data->defaultModelNumber;
        //}
        $bimsmithAPI = str_replace('{bmSKU}', $getBaseModelNo, $bimsmithAPI);
        $bimsmithResp = bimsmith_apicall($bimsmithAPI);
      }
      $output['bimsmith'] = $bimsmithResp;
      $output['bazaarvoice'] = $bazaarvoice;
      $this->cache->set($cid, $output, $cidExpTime);
      $build['output'] = [
        '#theme' => 'product_details_page',
        '#data' => $output,
      ];
      return $build;
    }
    else {
      $this->messenger()->addMessage($technical_difficulties, 'error');
      $this->getLogger('PDP API Hit Error')->notice('Unable to hit API' . $sku);
      $this->getLogger('API Error')->notice('<pre>' . print_r($response, 1) . '</pre>');
      $build['output'] = [
        '#theme' => 'product_details_error_page',
        '#data' => $technical_difficulties,
      ];
      return $build;
    }
    $redirectResp = new RedirectResponse($pageNotFound);
    $redirectResp->send();
  }

  /**
   * Fetch the where to buy page.
   */
  public function whereToBuy() {
    $GetModelNumber = $this->requestStack->getCurrentRequest()->get('modelNumber');
    $modelNumber =    strip_tags($GetModelNumber);
    $config = $this->config('sync.api_settings');
    $whereToBuyPage = $config->get('sync.whereToBuy');
    $whereToBuyPDP = $config->get('sync.whereToBuyLocator');
    $url = (!empty($modelNumber)) ? $whereToBuyPDP . $modelNumber : $whereToBuyPage;
    $letUrlPartArr = explode("?", $this->requestStack->getCurrentRequest()->getRequestUri());
    $dataOutput = [];
    if (count($letUrlPartArr) && $letUrlPartArr[0] == '/where-to-buy') {
      $getModelNumArr = (!empty($letUrlPartArr[1])) ? explode("=", $letUrlPartArr[1]) : [];
      if (count($getModelNumArr) && $getModelNumArr[0] === 'modelNumber') {
        $sku = $getModelNumArr[1];
        $cid = 'peer_pdp_' . $sku;
        // Cache pdp information.
        $data = $this->cache->get($cid);
        if (!empty($data->data)) {
          $dataOutput = $data->data;
        }
        else {
          $sku = $getModelNumArr[1];
          $node = self::getProductNid($sku);

          if (!empty($node)) {
            $mainCategory = $node->get('field_main_category')->value;
            $productDetails = self::productDetailsPage($sku, $mainCategory, $node);
            if (!empty($productDetails) && (!empty($productDetails['output']['#data']))) {
              $dataOutput = $productDetails['output']['#data'];
            }
          }
          else {
            $this->messenger()->addMessage($this->t("Invalid model number " . $modelNumber), 'error');
          }
        }
      }
    }
    $build['output'] = [
      '#theme' => 'where_to_buy',
      '#sku' => $modelNumber,
      '#url' => $url,
      '#data' => $dataOutput,
    ];
    return $build;
  }

  /**
   * Check the product exist using product sku.
   */
  public function getProductExist($sku) {
    $database = $this->connection->select('peerless_node__field_product_sku', 'ns');
    $result = $database
      ->fields('ns', ['entity_id'])
      ->condition('field_product_sku_value', $sku)
      ->condition('bundle', 'commerce_product')
      ->execute()
      ->fetchField();
    if (!empty($result)) {
      return 1;
    }
    else {
      return 0;
    }

  }

  /**
   * Fetch the product url by using product sku.
   */
  public function getProductUrl($sku) {
    $database = $this->connection->select('node__field_product_sku', 'ns');
    $result = $database
      ->fields('ns', ['entity_id'])
      ->condition('field_product_sku_value', $sku)
      ->condition('bundle', 'commerce_product')
      ->execute()
      ->fetchField();
    if (!empty($result)) {
      return $this->service->get('path_alias.manager')->getAliasByPath('/node/' . $result);
    }
    else {
      return "#";
    }

  }

  /**
   * Fetch the product url by using product sku.
   */
  public function getCollectionUrl($name) {
    $termLoad = taxonomy_term_load_multiple_by_name($name[0], 'collections');
    $term = reset($termLoad);
    $result = '';
    if (!empty($term)) {
      $tid = $term->get('tid')->value;
      $result = $this->entitytypemanager()->getStorage('node')->getQuery()->condition('type', 'collections_landing')
        ->condition('field_select_collection', $tid, '=')->execute();
    }

    if (!empty($result)) {
      return $this->service->get('path_alias.manager')->getAliasByPath('/node/' . current($result));
    }
    else {
      return "#";
    }

  }

  /**
   * Fetch the product url by using product sku.
   */
  public function getProductNid($sku) {
    $database = $this->connection->select('node__field_product_sku', 'ns');
    $nid = $database
      ->fields('ns', ['entity_id'])
      ->condition('field_product_sku_value', $sku)
      ->condition('bundle', 'commerce_product')
      ->execute()
      ->fetchField();
    if (!empty($nid)) {
      return $this->service->get('entity_type.manager')->getStorage("node")->load($nid);
    }
    else {
      return '';
    }

  }

  /**
   * Get the facet feature values.
   */
  public function getProductFeature($data) {
    $faturesBenefits = [];
    $faturesBenefitsResult = '';
    for ($i = 1; $i <= 20; $i++) {
      $attr = "BrandBaseProdBullet" . $i;
      if (isset($data->values->$attr) && !empty($data->values->$attr)) {
        $faturesBenefits[$attr] = $data->values->$attr;
      }
    }
    for ($i = 1; $i <= 20; $i++) {
      $attr1 = "BrandBaseProdBullet0" . $i;
      if (isset($data->values->$attr1) && !empty($data->values->$attr1)) {
        $faturesBenefits[$attr1] = $data->values->$attr1;
      }
    }
    if (count($faturesBenefits) > 0) {
      $faturesBenefitsResult = [
        '#theme' => 'item_list',
        '#items' => $faturesBenefits,
        '#attributes' => [
          'class' => [
            'fatures--benefits list-unstyled',
          ],
        ],
      ];
    }
    return $faturesBenefitsResult;
  }

  /**
   * Get the facet feature values.
   */
  public function getFacetFeature($facetFeatures) {
    $faturesBenefitsValues = [];
    $facetFeatureResult = '';
    $vid = strtolower('facetFeature');
    $terms = $this->entitytypemanager()->getStorage('taxonomy_term')->loadTree($vid, 0, 1, TRUE);
    foreach ($terms as $term) {
      $termName = $term->getName();
      $iconUri = '';
      if ($term->hasField('field_facet_label')) {
        $labelVal = $term->get('field_facet_label')->getValue();
      }
      if ($term->hasField('field_facet_icon')) {
        $iconVal = $term->get('field_facet_icon')->getValue();
      }
      $label = (!empty($labelVal)) ? $labelVal[0]['value'] : $termName;
      if (!empty($iconVal)) {
        $entityArray = $this->entityTypeManager()->getStorage('media')->load($iconVal[0]['target_id'])->toArray();
        $iconArray = $this->entityTypeManager()->getStorage('file')->load($entityArray['image'][0]['target_id'])->toArray();
        $iconUri = file_create_url($iconArray['uri'][0]['value']);
      }
      $faturesBenefits[$termName] = [
        '#theme' => 'icon_text',
        '#data' => ['text' => $label, 'icon' => $iconUri, 'iconalt' => $label],
      ];
    }
    foreach ($facetFeatures as $facetFeature) {
      if (!empty($faturesBenefits[$facetFeature])) {
        $faturesBenefitsValues[] = $faturesBenefits[$facetFeature];
      }
    }
    if (count($faturesBenefitsValues) > 0) {
      $facetFeatureResult = [
        '#theme' => 'item_list',
        '#items' => $faturesBenefitsValues,
        '#attributes' => [
          'class' => [
            'facet--feature list-unstyled',
          ],
        ],
      ];
    }
    return $facetFeatureResult;
  }

  /**
   * Get the product details.
   */
  public function getProductDetails($data) {
    $values = $data->values;
    $productDetailsValues = [];
    $productDetailsResult = '';
    if (!empty($values->Handle)) {
      $productDetailsValues['handle'] = 'Handle(s): ' . $values->Handle;
    }
    if (!empty($values->FltSprayKitchen) && $values->FltSprayKitchen != 'F') {
      $productDetailsValues['spary'] = ($values->FltSprayKitchen == 'T') ? 'With Side Sprayer: ' . 'Yes' : $values->FltSprayKitchen;
    }
    if (!empty($values->FltSoapDispenser) && $values->FltSoapDispenser != 'F') {
      $productDetailsValues['soap_dispenser'] = ($values->FltSoapDispenser == 'T') ? 'With Soap/Lotion Dispenser: ' . 'Yes' : $values->FltSoapDispenser;
    }
    if (!empty($values->FlowRate)) {
      $productDetailsValues['flowrate'] = 'Flow Rate: ' . $values->FlowRate;
    }

    // Tall High and Pull Out.
    foreach ($data->categories as $categories) {
      if (strpos($categories, 'Tall/High-Arc')) {
        $productDetailsValues['tall_high'] = 'Tall/High Arc: Yes';
      }
      if (strpos($categories, 'Pull-Outs')) {
        $productDetailsValues['pull_out_down'] = 'Pull-out/Pull-down: Yes';
      }
    }

    if (!empty($values->ValveType)) {
      $productDetailsValues['valve'] = 'Valve Type: ' . $values->ValveType;
    }
    if (!empty($values->HolesforInstallation)) {
      $option = $values->HolesforInstallation;
      $productDetailsValues['holes'] = 'Installation: ' . $option;
    }
    if (!empty($values->PBSpoutLength)) {
      $productDetailsValues['spout_length'] = 'Spout Length: ' . $values->PBSpoutLength;
    }
    $heightText = "Over All Height";
    if (!empty($values->$heightText)) {
      $productDetailsValues['spout_height'] = 'Spout Total Height: ' . $values->$heightText;
    }
    if (!empty($values->PBSpoutHeightDecktoAerator)) {
      $productDetailsValues['height_deck'] = 'Spout Height Deck to Aerator: ' . $values->PBSpoutHeightDecktoAerator;
    }
    if (count($productDetailsValues) > 0) {
      $productDetailsResult = [
        '#theme' => 'item_list',
        '#items' => $productDetailsValues,
        '#attributes' => [
          'class' => [
            'product--details list-unstyled',
          ],
        ],
      ];
    }
    return $productDetailsResult;
  }

  /**
   * Get the product certification.
   */
  public function getProductCertification($data, $config) {
    $values = $data->values;
    $productCertValues = [];
    if (isset($data->values->WatersenseCert) && $data->values->WatersenseCert == 'T') {
      if (isset($data->values->WatersenseLogo)) {
        $facetFeatureResult = $this->getfacetFeaturesImagePath($data->values->WatersenseLogo);
        $imageUri = (!empty($facetFeatureResult['watersenselogo'])) ? $facetFeatureResult['watersenselogo'] : $values->WatersenseLogo;
        $imageLabel = (!empty($facetFeatureResult['watersenselabel'])) ? $facetFeatureResult['watersenselabel'] : '';
        $productCertValues[] = [
          '#theme' => 'icon_text',
          '#data' => ['text' => '', 'icon' => $imageUri, 'iconalt' => basename($imageUri)],
        ];
      }
    }
    return $productCertValues;
  }

  /**
   * Get the taxanomy Media Image Path values.
   */
  public function getfacetFeaturesImagePath($facetFeatures) {

    $term_obj = $this->entitytypemanager->getStorage('taxonomy_term')
      ->loadByProperties(['name' => $facetFeatures]);
    $facetFeatureResult = [];
    if (!empty($term_obj)) {
      $term_id = array_keys($term_obj);
      $final_term = array_pop($term_id);
      if ($term_obj[$final_term]->hasField('field_facet_icon')) {
        $term_icon = $term_obj[$final_term]->get('field_facet_icon')->getValue();
      }
      if ($term_obj[$final_term]->hasField('field_facet_label')) {
        $term_label = $term_obj[$final_term]->get('field_facet_label')->getValue();
      }

      $mediaentity = [];
      if (!empty($term_icon)) {
        $mediaentity = $this->entityTypeManager()->getStorage('media')->load($term_icon[0]['target_id']);
        $mediaentity_tid = $mediaentity->get('image')->target_id;
        $image_file = $this->entityTypeManager()->getStorage('file')->load($mediaentity_tid);
        $uri = $image_file->uri->value;
        $watersensecert_label = '';
        $watersensecert_label = (!empty($term_label)) ? $term_label[0]['value'] : '';
        $watersense_image = '';
        $watersense_image = file_create_url($uri);
        $facetFeatureResult = ['watersenselogo' => $watersense_image, 'watersenselabel' => $watersensecert_label];
      }
    }
    return $facetFeatureResult;

  }

  /**
   * Get the product reference.
   */
  public function getProductReference($collection, $productType = 'kitchen') {
    $config = $this->config('sync.api_settings');
    $configPrimary = $this->config('delta_services.deltaservicesconfig');
    $domain = $configPrimary->get('api_base_path');
    $collectionUrl = $domain . $config->get('sync.product_collections_api');
    $collectionUrl .= "?" . http_build_query(['name' => $collection]);
    $method = 'GET';
    $headers = add_headers(FALSE);
    $response = web_services_api_call($collectionUrl, $method, $headers, '', 'Collection');
    $data = json_decode($response['response']);
    if ($response['status'] == 'SUCCESS' && (!empty($data)) && (!is_null($data))) {
      $output = reset($data);
      $recommendedSkusText = ($productType == 'bath') ? 'recommendedBathSuite' : 'recommendedKitchenSuite';
      $recommendedSkus = $output->$recommendedSkusText;
      if ((!empty($recommendedSkus)) && count($recommendedSkus) > 0) {
        return self::getMultipleProducts($recommendedSkus, 'F');
      }
    }
  }

  /**
   * Get the multiple products.
   */
  public function getMultipleProducts($recommendedSkus, $cacheApi = 'T') {

    $config = $this->config('sync.api_settings');
    $cidExpTime = $config->get('sync.cache_exp_time');
    $cidExpTime = \Drupal::time()->getRequestTime() + $cidExpTime;

    if ($cacheApi === 'T') {
      if (count($recommendedSkus)) {
        $cid = end($recommendedSkus);
        $data = $this->cache->get($cid);
        if (isset($data->data) && $data->data != NULL) {
          return $data->data;
        }
      }
    }

    $config = $this->config('sync.api_settings');
    $configPrimary = $this->config('delta_services.deltaservicesconfig');
    $domain = $configPrimary->get('api_base_path');
    $method = 'GET';
    $headers = add_headers(FALSE);
    $productSkus = '';
    foreach ($recommendedSkus as $recommendedSku) {
      $productSkus .= "&" . http_build_query(['name' => $recommendedSku]);
    }
    $referenceUrl = $domain . $config->get('sync.load_multiple_products');
    $referenceUrl .= "?" . $productSkus;
    $response = web_services_api_call($referenceUrl, $method, $headers, '', 'Reference');
    if ($response['status'] == 'SUCCESS' && $response['response'] != 'null') {
      $data = json_decode($response['response']);
      $referenceContents = $data->content;
      $result = [];
      foreach ($referenceContents as $referenceContent) {
        $collection = (!empty($referenceContent->values->defaultCollection)) ? $referenceContent->values->defaultCollection : "Peerless";
        $result[] = [
          'productUrl' => self::getProductUrl($referenceContent->name),
          'description' => $referenceContent->description,
          'image' => $referenceContent->heroImageSmall,
          'type' => $collection,
          'modelNumber' => $referenceContent->name,
          'listPrice' => (!empty($referenceContent->values->ListPrice)) ? $referenceContent->values->ListPrice : '',
        ];
      }
      if ($cacheApi === 'T') {
        $this->cache->set($cid, $result, $cidExpTime);
      }
      return $result;
    }
  }

  /**
  * Get products finish with specific validation rule.
  */

  public function getPDPFinishParts($dataObj){
    $productSkus = "";
    $getFinishSkuList = [];
    foreach ($dataObj as $key => $value) {
      $productSkus .= "&" . http_build_query(['name' => $value->modelNumber]);
    }
    $config = $this->config('sync.api_settings');
    $configPrimary = $this->config('delta_services.deltaservicesconfig');
    $domain = $configPrimary->get('api_base_path');
    $method = 'GET';
    $headers = add_headers(FALSE);
    $referenceUrl = $domain . $config->get('sync.load_multiple_products');
    $referenceUrl .= "?" . ltrim($productSkus,"&");
    $response = web_services_api_call($referenceUrl, $method, $headers, '', 'ProductFinish');
    if ($response['status'] == 'SUCCESS') {
      $response = json_decode($response['response']);
      $obj = $response->content;
      foreach ($obj as $key1 => $value1) {
        $getFinishVisibilityFlag = $this->globalproduct->getFoundationalOrderShipAttr($value1);
        if($getFinishVisibilityFlag === TRUE){
          $getFinishSkuList[] = $value1->name;
        }
      }
    }else{
       $this->getLogger('PDPFinishAPIError')->notice('Facing Technical Difficulties');
       $this->getLogger('PDPFinishAPIError')->notice($referenceUrl);
    }
    return $getFinishSkuList;
  }
}

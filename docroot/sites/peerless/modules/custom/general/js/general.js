(function ($, Drupal) {
    'use strict';
    Drupal.behaviors.general = {
        attach: function (context, settings) {
            $("#info-bubble a").attr('href',$('div#info-bubble-uri input[type=hidden]').val());
        }
    };
})(jQuery, Drupal, drupalSettings);

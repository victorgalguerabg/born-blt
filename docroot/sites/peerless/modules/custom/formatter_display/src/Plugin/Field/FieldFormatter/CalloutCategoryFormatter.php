<?php

namespace Drupal\formatter_display\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceFormatterBase;
use Drupal\Core\Url;

/**
 * Plugin implementation of the 'paragraph_summary' formatter.
 *
 * @FieldFormatter(
 *   id = "paragraph_summary_callout_category",
 *   label = @Translation("Callout Category Formatter"),
 *   field_types = {
 *     "entity_reference_revisions"
 *   }
 * )
 */
class CalloutCategoryFormatter extends EntityReferenceFormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $render_items = [];
    foreach ($this->getEntitiesToView($items, $langcode) as $entity) {
      if ($entity->id()) {
        if (!empty($entity->field_heading_text->getValue())) {
          $heading_text = $entity->field_heading_text->getValue()[0]['value'];
        }
        if (!empty($entity->field_action_link->getValue())) {
          $action_link_txt = $entity->field_action_link->getValue()[0]['title'];
          $action_link = Url::fromUri($entity->field_action_link->getValue()[0]['uri']);
        }
        if (NULL != $entity->field_callout_image->referencedEntities()) {
          $imageDetails = $entity->field_callout_image->referencedEntities();
          $image = file_create_url($imageDetails[0]->uri->value);
        }
        $render_items[] = [
          'action_link_txt' => $action_link_txt,
          'action_link' => $action_link,
          'heading_text' => $heading_text,
          'image' => $image,
        ];

      }
    }
    return [
      '#theme' => 'callout_category',
      '#items' => $render_items,
    ];
  }

}

<?php
namespace Drupal\formatter_display\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceFormatterBase;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Plugin implementation of the 'entity reference rendered entity' formatter.
 *
 * @FieldFormatter(
 *   id = "collections_landing_page_formatter",
 *   label = @Translation("Collections Landing Page Formatter"),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class CollectionsLandingPageFormatter extends EntityReferenceFormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $refCollections = $items->referencedEntities();
    foreach ($refCollections as $collections) {
      if ($collections->hasField('nid')) {
        $nid = $collections->get('nid')->getValue()[0]['value'];
      }
      $alias = \Drupal::service('path_alias.manager')->getAliasByPath('/node/'.$nid);
      $collectionItems['alias'] = $alias;
      $imageUrl = '';
      if ($collections->hasField('field_image')) {
        $image = $collections->get('field_image')->referencedEntities();
        if(is_array($image) && count($image) > 0){
          $imageUrl = file_create_url($image[0]->getFileUri());
        }
      }
      $collectionItems['imageUrl'] = $imageUrl;
      $isNew = 0;
      if ($collections->hasField('field_is_new')) {
        $isNew = 'new';
      }
      $collectionItems['isNew'] = $isNew;
      $collectionAssets[] = $collectionItems;
    }

    return [
      '#theme' => 'collection_landing_formatter',
      '#items' => $collectionAssets,
    ];

  }

}

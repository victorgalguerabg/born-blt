<?php

namespace Drupal\formatter_display\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceFormatterBase;
use Drupal\Core\Routing\RequestContext;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Field\FieldDefinitionInterface;

/**
 * Plugin implementation of the 'paragraph_summary' formatter.
 *
 * @FieldFormatter(
 *   id = "paragraph_priceasset_component",
 *   label = @Translation("Price Asset Formatter"),
 *   field_types = {
 *     "entity_reference_revisions"
 *   },
 * )
 */
class PriceAssetFormatter extends EntityReferenceFormatterBase implements ContainerFactoryPluginInterface {

  /**
   * The route match service.
   *
   * @var Drupal\Core\Routing\RequestContext
   */
  protected $routeMatch;

  /**
   * {@inheritdoc}
   */
  protected $service;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructor with Dependency Injection.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, EntityTypeManagerInterface $entityManager, RequestContext $request, ContainerInterface $service) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);

    $this->entityTypeManager = $entityManager;
    $this->routeMatch = $request;
    $this->service = $service;
  }

  /**
   * Function create.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   Container interface.
   * @param array $configuration
   *   Block Configuration.
   * @param string $plugin_id
   *   Block Plugin id.
   * @param mixed $plugin_definition
   *   Block Definition.
   *
   * @return static
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      // Add any services you want to inject here.
      $container->get('entity_type.manager'),
      $container->get('router.request_context'),
      $container->get('service_container')

    );
  }

  /**
   * Implementation of formatter.
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    // $slider_items = $items->referencedEntities();
    $render_items = [];
    $asset_link = '';
    $asset_title = '';
    $asset_type = '';
    foreach ($this->getEntitiesToView($items, $langcode) as $entity) {
      if ($entity->id()) {
        if (!empty($entity->field_asset_title->getValue())) {
          $asset_title = $entity->field_asset_title->getValue()[0]['value'];
        }
        if (!empty($entity->field_file_asset->getValue())) {
          $getFid = $entity->field_file_asset->getValue()[0]['target_id'];
          $fileLoc = $this->entityTypeManager->getStorage('file')->load($getFid)->getFileUri();
          if (!empty($fileLoc)) {
            $asset_link = file_create_url($fileLoc);
          }
        }
        if (!empty($entity->field_assets_type->getValue())) {
          $asset_type = $entity->field_assets_type->getValue()[0]['value'];
        }
        $render_items[] = [
          'asset_link' => $asset_link,
          'asset_title' => $asset_title,
          'asset_type' => $asset_type,
        ];

      }
    }
    return [
      '#theme' => 'price_asset',
      '#items' => $render_items,
    ];
  }

}

<?php

namespace Drupal\formatter_display\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceFormatterBase;
use Drupal\Core\Url;

/**
 * Plugin implementation of the 'paragraph_summary' formatter.
 *
 * @FieldFormatter(
 *   id = "paragraph_banner_component",
 *   label = @Translation("Banner Formatter"),
 *   field_types = {
 *     "entity_reference_revisions"
 *   },
 * )
 */
class BannerFormatter extends EntityReferenceFormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    // $slider_items = $items->referencedEntities();
    $text_alignment = $content_alignment = '';
    $render_items = [];
    foreach ($this->getEntitiesToView($items, $langcode) as $entity) {
      if ($entity->id()) {
        if (!empty($entity->field_banner_formatted_title->getValue())) {
          $heading_text = $entity->field_banner_formatted_title->getValue()[0]['value'];
        }
        if (!empty($entity->field_embedded_video_label) && !empty($entity->field_embedded_video_label->getValue())) {
          $action_link_txt = $entity->field_embedded_video_label->getValue()[0]['value'];
        }
        $link_video_flag = 'l';
        if (!empty($entity->field_banner_embedded_video) && !empty($entity->field_banner_embedded_video->getValue())) {
          $path = $entity->field_banner_embedded_video->getValue()[0]['value'];
          $video_id = explode('?v=', $path);
          $get_video_id = explode('&', $video_id[1]);
          if (!empty($get_video_id)) {
            $youtubeId = $get_video_id[0];
          }
          $action_link = 'https://www.youtube.com/embed/' . $youtubeId;
          $link_video_flag = 'v';
        }
        if (!empty($entity->field_banner_cta->getValue())) {
          $action_link_txt = $entity->field_banner_cta->getValue()[0]['title'];
          $action_link = Url::fromUri($entity->field_banner_cta->getValue()[0]['uri']);
        }

        if (NULL != $entity->field_banner_image->referencedEntities()) {
          $imageDetails = $entity->field_banner_image->referencedEntities();
          $image = file_create_url($imageDetails[0]->uri->value);
        }

        if (!empty($entity->field_banner_formatted_desc->getValue())) {
          $pre_heading_text = $entity->field_banner_formatted_desc->getValue()[0]['value'];
        }

        if ((!empty($heading_text)) || (!empty($pre_heading_text)) || (!empty($image))) {
          $render_items[] = [
            'link_video_flag' => $link_video_flag,
            'action_link_txt' => $action_link_txt,
            'action_link' => $action_link,
            'text_alignment' => $text_alignment,
            'content_alignment' => $content_alignment,
            'heading_text' => $heading_text,
            'pre_heading_text' => $pre_heading_text,
            'image' => $image,
          ];
        }
      }
    }
    return [
      '#theme' => 'banner',
      '#items' => $render_items,
    ];
  }

}

<?php

namespace Drupal\formatter_display\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceFormatterBase;
use Drupal\core\Url;
use Drupal\media\Entity\Media;
use Drupal\file\Entity\File;

/**
 * Plugin implementation of the 'paragraph_summary' formatter.
 *
 * @FieldFormatter(
 *   id = "paragraph_slider_component",
 *   label = @Translation("Slider Formatter"),
 *   field_types = {
 *     "entity_reference_revisions"
 *   },
 * )
 */
class SliderFormatter extends EntityReferenceFormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    // $slider_items = $items->referencedEntities();
    $render_items = [];
    foreach ($this->getEntitiesToView($items, $langcode) as $entity) {
      if ($entity->id()) {
        $text_alignment = '';
        $heading_html = '';
        $content_alignment = '';
        $action_link_attribute = $imagealt = $imagetitle = '';
        if (($entity->hasField('field_text_align')) && !empty($entity->field_text_align->getValue()) && ($entity->hasField('field_text_align'))) {
          $text_alignment = $entity->field_text_align->getValue()[0]['value'];
        }
        if (($entity->hasField('field_content_placement')) && !empty($entity->field_content_placement->getValue())) {
          $content_alignment = $entity->field_content_placement->getValue()[0]['value'];
        }
        if (($entity->hasField('field_heading_text')) && !empty($entity->field_heading_text->getValue())) {
          $heading_text = $entity->field_heading_text->getValue()[0]['value'];
        }
        if (($entity->hasField('field_heading_formatted_text')) && !empty($entity->field_heading_formatted_text->getValue())) {
          $heading_html = $entity->field_heading_formatted_text->getValue()[0]['value'];
        }

        if (($entity->hasField('field_video_label')) && !empty($entity->field_video_label) && !empty($entity->field_video_label->getValue())) {
          $action_link_txt = $entity->field_video_label->getValue()[0]['value'];
        }
        $link_video_flag = 'l';
        if (($entity->hasField('field_video')) && !empty($entity->field_video) && !empty($entity->field_video->getValue())) {
          $path = $entity->field_video->getValue()[0]['value'];
          $get_video_id = explode('?v=', $path);
          if (!empty($get_video_id)) {
            $youtubeId = $get_video_id[1];
          }
          $action_link = 'https://www.youtube.com/embed/' . $youtubeId;
          $link_video_flag = 'v';
        }
        if (($entity->hasField('field_action_link')) && !empty($entity->field_action_link->getValue())) {
          $action_link_txt = $entity->field_action_link->getValue()[0]['title'];
          $action_link = Url::fromUri($entity->field_action_link->getValue()[0]['uri']);
          if (!empty($entity->field_action_link->getValue()[0]['options'])) {
            $action_link_attribute = $entity->field_action_link->getValue()[0]['options']['attributes']['target'];
          }
        }

        if (null != $entity->field_banner_image->referencedEntities()) {
          $imageDetails = $entity->field_banner_image->referencedEntities();
          $image = file_create_url($imageDetails[0]->uri->value);
          $imgproperties = $entity->field_banner_image->getValue();
          $imagealt = $imgproperties[0]['alt'];
          $imagetitle = $imgproperties[0]['title'];
          if($imagealt==''){
          $imagealt = basename($image);
	      }
          if($imagetitle==''){
          $imagetitle = '';
	      }
        }

        if (!empty($entity->field_pre_heading_text->getValue())) {
          $pre_heading_text = $entity->field_pre_heading_text->getValue()[0]['value'];
        }

        if ((!empty($heading_text)) || (!empty($heading_html)) || (!empty($pre_heading_text)) || (!empty($image))) {
          $render_items[] = [
            'link_video_flag' => $link_video_flag,
            'action_link_txt' => $action_link_txt,
            'action_link' => $action_link,
            'action_link_attribute' => $action_link_attribute,
            'text_alignment' => $text_alignment,
            'content_alignment' => $content_alignment,
            'heading_text' => $heading_text,
            'heading_html' => $heading_html,
            'pre_heading_text' => $pre_heading_text,
            'image' => $image,
            'imagealt' => $imagealt,
            'imagetitle' => $imagetitle,
          ];
        }
      }
    }
    return [
      '#theme' => 'slider',
      '#items' => $render_items,
    ];
  }

}

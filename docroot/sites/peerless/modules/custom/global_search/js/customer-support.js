(function ($, Drupal, drupalSettings, Backbone) {

    Drupal.behaviors.plpBehaviour = {
        attach: function (context, settings) {

            // Add advanced class when the url having the filter as query string.
            var _filter = getSearchParams('documentLookFor');
            var _source = getSearchParams('source');
            if (_filter !== '' && _filter !== undefined) {
                $('#plp-wrapper').addClass('advance-search');
            } else {
                $('#product-link').remove();
                $('#product-tab').remove();
                if ($('#plp-wrapper advance-search').length) {
                    $('#plp-wrapper').removeClass('advance-search');
                }
            }

            // Professional page : switch off document tab.
            if ((_filter !== undefined && _filter === 'productdetails') && _source === 'prof') {
                $('#document-link').remove();
                $('#document-tab').remove();
            }
            if ((_filter !== undefined && _filter !== 'productdetails') && _source === 'prof') {
                $('#product-link').remove();
                $('#product-tab').remove();
            }


            if (_source !== '' && _source !== undefined) {
                var addExtraClass = '';
                if (_filter === 'productdetails') {
                    addExtraClass = 'product ' + _source;
                    if ($('#document-link.active').length) {
                        $('#document-link').removeClass('active');
                        $('#document-tab').removeClass('active in');
                    }
                    $('#product-link').addClass('active');
                    $('#product-tab').addClass('active in');

                } else {
                    addExtraClass = 'document ' + _source;
                    if ($('#product-link.active').length) {
                        $('#product-link').removeClass('active');
                        $('#product-tab').removeClass('active in');
                    }
                    $('#document-link').addClass('active');
                    $('#document-tab').addClass('active in');
                }
                $('#plp-wrapper').addClass('advance-search ' + addExtraClass);
                $(".support-search-result #product-link").hide();
            }


            //System will display warning message when user change query string manually.
            if ((_source !== '' && _source !== undefined) && (_source !== 'support' && _source !== 'prof')) {
                if ($('.show-warning').length) {
                    $('#plp-wrapper').removeClass('show-warning').remove('stop-results');
                }
                $('#plp-wrapper').addClass('show-warning').remove('.search-result-page .nav-tabs').remove('.search-result-page .tab-content').html('<div class="stop-results">Something went wrong, Please try again later.</div>');
            }

            if ($("#document-output").length) {
                var _totalCount = 0;
                var docView = Backbone.View.extend({
                    el: ".documentoutput",
                    template: _.template($("#document-output").html()),
                    initialize: function (initData) {
                        this.render(initData);
                    },
                    render: function (initData) {
                        if (initData === null) {
                            $('.documentoutput').html('<h4>No results at this time.</h4>');
                        }
                        var _self = this;
                        var _render = "";
                        if (initData !== null) {
                            var content = initData.content;
                            if (content.length > 0) {
                                _.each(initData.content, function (entry) {
                                    var _filePath = entry.filePath;
                                    var _fileName = entry.fileName;
                                    var _docType = entry.documentType;
                                    _render += _self.template({
                                        fileName: _fileName,
                                        fileLinks: entry.modelNumber.slice(0, 5),
                                        filePath: _filePath,
                                        docType: _docType,
                                    });
                                    _self.$el.html(_render);
                                });
                            }
                        }
                        $('.preloader').remove();
                        $('body').removeClass('loader-active');
                        var _documentCount = (initData !== null) ? initData.totalElements : 0;
                        _totalCount = parseInt(_totalCount) + parseInt(_documentCount);
                        $('#documentTabLink span').html('Documents (' + _documentCount + ')');
                        $('#search-document-title').html('Documents (' + _documentCount + ')');
                        return this;
                    }
                });


            }

            if ($("#pagination").length) {
                var paginationView = Backbone.View.extend({
                    el: ".pagination-output",
                    template: _.template($("#pagination").html()),
                    initialize: function (initData) {
                        this.render(initData);
                    },
                    events: {
                        'click [type="button"]': 'pagination'
                    },
                    pagination: function (e) {
                        var params = [];
                        params['pageNum'] = $(e.currentTarget).val();
                        applyPLPFacets(params);
                    },
                    render: function (initData) {
                        if (initData !== null) {
                            var _self = this;
                            var _render = "";
                            //stopLoop value
                            var _nextPage = parseInt(initData.number) + 1;
                            var _previousPage = parseInt(initData.number) - 1;
                            _render += _self.template({
                                totalPager: initData.totalPages,
                                currentPage: _nextPage,
                                nextPage: _nextPage,
                                previousPage: _previousPage,
                                totalRecordset: initData.totalElements
                            });
                            _self.$el.html(_render);
                        }
                    }
                });
            }

            if ($("#doc-pagination").length) {
                var docpaginationView = Backbone.View.extend({
                    el: ".doc-pagination-output",
                    template: _.template($("#doc-pagination").html()),
                    initialize: function (initData) {
                        this.render(initData);
                    },
                    events: {
                        'click [type="button"]': 'docpagination'
                    },
                    docpagination: function (e) {
                        var params = [];
                        params['pageNum'] = $(e.currentTarget).val();
                        applydocFacets(params);
                    },
                    render: function (initData) {
                        if (initData !== null) {
                            var _self = this;
                            var _render = "";
                            //stopLoop value
                            var _nextPage = parseInt(initData.number) + 1;
                            var _previousPage = parseInt(initData.number) - 1;
                            _render += _self.template({
                                totalPager: initData.totalPages,
                                currentPage: _nextPage,
                                nextPage: _nextPage,
                                previousPage: _previousPage,
                                totalRecordset: initData.totalElements
                            });
                            _self.$el.html(_render);
                        }
                    }
                });
            }

            if ($("#output-node").length) {
                //product
                var ProdView = Backbone.View.extend({

                    el: "#output",
                    el2: "#output-1",
                    template: _.template($("#output-node").html()),
                    template2: _.template($("#output-node-1").html()),

                    initialize: function (initData) {
                        this.render(initData);
                        this.renderFullProducts(initData);
                    },

                    render: function (initData) {
                        var _self = this;
                        var _render = "";
                        var _i = 0;
                        $(".support-search-result #product-link").css('display', 'block');
                        if (initData.content != '') {
                            _.each(initData.content, function (entry) {
                                if (_i == 8) {
                                    return false;
                                }
                                var _price = 0;
                                var _filter = getSearchParams('documentLookFor');
                                // if(_filter === ''){
                                var _price = entry.values.ListPrice;
                                // }

                                var _heroImage = entry.heroImageSmall;
                                var _SyndigoActive = '';
                                if (entry.values.MaterialStatus !== undefined && entry.values.MaterialStatus >= 50) {
                                    var _SyndigoActive = 'discontinued';
                                }
                                var orderDate = (entry.values.AvailableToOrderDate !== '' || entry.values.AvailableToOrderDate !== undefined) ? DateTotimeStamp(entry.values.AvailableToOrderDate) : '';
                                var shipDate = (entry.values.AvailableToShipDate !== '' || entry.values.AvailableToShipDate !== undefined) ? DateTotimeStamp(entry.values.AvailableToShipDate) : '';
                                var currentDate = DateTotimeStamp(getCurrentDate());
                                if (orderDate === '' || shipDate === '') {
                                    shipDate = currentDate - (60 * 60 * 24);
                                }
                                if (currentDate < shipDate) {
                                    if (currentDate > orderDate) {
                                        var _SyndigoActive = 'coming soon';
                                    }
                                }
                                var _title = "Title";
                                if (entry.description !== '' && entry.description !== undefined) {
                                    _title = entry.description;
                                }

                                var _collection = "PEERLESS";
                                //if(_filter === ''){
                                if (entry.values.Collection !== "" && entry.values.Collection !== undefined) {
                                    _collection = entry.values.Collection;
                                }
                                //}

                                var _sku = entry.name;
                                var urlAliasObject = drupalSettings.urlAliasObject;
                                var _url = '';
                                if (typeof urlAliasObject[_sku] != undefined) {
                                    _url = urlAliasObject[_sku];
                                }
                                if (_url === '' || _url === undefined) {
                                    _url = '/pdp/' + _sku;
                                }

                                _render += _self.template({
                                    title: _title,
                                    collectionName: _collection,
                                    heroImage: _heroImage,
                                    sku: _sku,
                                    price: _price,
                                    url: _url,
                                    prodOutDated: _SyndigoActive,
                                });
                                _self.$el.html(_render);
                                $('.tab-content-title').show();
                                $('.preloader').remove();
                                $('body').removeClass('loader-active');
                                _i++;
                            });
                        } else {
                            var noResultsMessage = '<div role="contentinfo" aria-label="Error message"><h2 class="visually-hidden">Status message</h2>Oops!!! Nothing Found. Please refine your search</div>';
                            $('#output').html(noResultsMessage);
                            $('.preloader').remove();
                            $('body').removeClass('loader-active');
                        }
                        var _productCount = initData.totalElements;
                        _totalCount = parseInt(_totalCount) + parseInt(_productCount);

                        $('#productTabLink span').html('Products (' + _productCount + ')');
                        $('#search-product-title').html('Products (' + _productCount + ')');
                        //$('#allTabLink span').html('All (' + _totalCount + ')');
                        return this;
                    },
                    renderFullProducts: function (initData) {
                        var _self = this;
                        var _render = "";
                        if (initData.content != '') {
                            _.each(initData.content, function (entry) {
                                var _price = entry.values.ListPrice;
                                var _heroImage = entry.heroImageSmall;
                                var _SyndigoActive = '';
                                if (entry.values.MaterialStatus !== undefined && entry.values.MaterialStatus >= 50) {
                                    var _SyndigoActive = 'discontinued';
                                }
                                var orderDate = (entry.values.AvailableToOrderDate !== '' || entry.values.AvailableToOrderDate !== undefined) ? DateTotimeStamp(entry.values.AvailableToOrderDate) : '';
                                var shipDate = (entry.values.AvailableToShipDate !== '' || entry.values.AvailableToShipDate !== undefined) ? DateTotimeStamp(entry.values.AvailableToShipDate) : '';
                                var currentDate = DateTotimeStamp(getCurrentDate());
                                if (orderDate === '' || shipDate === '') {
                                    shipDate = currentDate - (60 * 60 * 24);
                                }
                                if (currentDate < shipDate) {
                                    if (currentDate > orderDate) {
                                        var _SyndigoActive = 'coming soon';
                                    }
                                }
                                var _title = "Title";
                                if (entry.description != '') {
                                    _title = entry.description;
                                }
                                var _collection = "PEERLESS";
                                if (entry.values.Collection !== "" && entry.values.Collection !== undefined) {
                                    _collection = entry.values.Collection;
                                }
                                var _sku = entry.name;
                                var urlAliasObject = drupalSettings.urlAliasObject;
                                var _url = '';
                                if (typeof urlAliasObject[_sku] != undefined) {
                                    _url = urlAliasObject[_sku];
                                }
                                if (_url === '' || _url === undefined) {
                                    _url = '/pdp/' + _sku;
                                }
                                _render += _self.template2({
                                    title: _title,
                                    collectionName: _collection,
                                    heroImage: _heroImage,
                                    sku: _sku,
                                    price: _price,
                                    url: _url,
                                    prodOutDated: _SyndigoActive,
                                });
                                $('#output-1').html(_render);
                                $('.preloader').remove();
                                $('body').removeClass('loader-active');
                            });
                        } else {
                            var noResultsMessage = '<div role="contentinfo" aria-label="Error message"><h2 class="visually-hidden">Status message</h2>Oops!!! Nothing Found. Please refine your search</div>';
                            $('#output').html(noResultsMessage);
                            $('.preloader').remove();
                            $('body').removeClass('loader-active');
                        }
                        var _productCount = initData.totalElements;
                        $('#productTabLink span').html('Products (' + _productCount + ')');
                        return this;
                    }

                });


            }

            var _param = getSearchParams('title');
            var _filter = getSearchParams('documentLookFor');
            var _source = getSearchParams('source');
            if (context == document && (_param !== undefined || _filter !== undefined || _source !== undefined)) {
                $('#searchPattern').html(decodeURIComponent(_param));
                $.ajax({
                    type: "GET",
                    url: "/search-doc-output",
                    dataType: 'json',
                    data: {
                        title: _param,
                        documentLookFor: _filter,
                        source: _source,
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        showErrorMessage(XMLHttpRequest.status, '#all-tab');
                    },
                    beforeSend: function () {
                        $('body').addClass('loader-active');
                        if (!$('.preloader').length) {
                            $('.search-result-page').prepend('<div class="preloader"><i class="input-loader"></i></div>');
                        }
                    },
                    complete: function () {
                        $('.preloader').remove();
                        $('body').removeClass('loader-active');
                    },
                    success: function (data) {
                        if (data !== null && data.totalElements > 0) {
                            var DocView = new docView(data);
                            var DocpaginationView = new docpaginationView(data);
                        } else {
                            var noResultsMessage = '<div role="contentinfo" aria-label="Error message"><h2 class="visually-hidden">Status message</h2>Oops!!! Nothing Found. Please refine your search</div>';
                            $('.documentoutput').html(noResultsMessage);
                            $('.preloader').remove();
                            $('body').removeClass('loader-active');
                        }
                    }
                });
                // Let switch off the product tab when product tab really not required.
                if (_filter !== undefined) {
                    // Lets remove
                    // var _param = getSearchParams('title');
                    // var _filter = getSearchParams('documentLookFor');
                    // var _source = getSearchParams('source');
                    $.ajax({
                        type: "GET",
                        url: "/search-output",
                        dataType: 'json',
                        data: {
                            title: _param,
                            documentLookFor: _filter,
                            source: _source,
                        },
                        success: function (data) {
                            if (data !== null && data.totalElements > 0) {
                                var prodView = new ProdView(data);
                                var PaginationView = new paginationView(data);
                            } else {
                                var noResultsMessage = '<div role="contentinfo" aria-label="Error message"><h2 class="visually-hidden">Status message</h2>Oops!!! Nothing Found. Please refine your search</div>';
                                $('#output').html(noResultsMessage);
                                $('.preloader').remove();
                                $('body').removeClass('loader-active');
                                $(".support-search-result #product-link").css('display', 'block');
                            }
                        }
                    });
                }
            }

            var showErrorMessage = function (status, wrapper) {
                switch (status) {
                    case 400:
                        $(wrapper).html("<div class='sorry-msg'>Server understood the request, but request content was invalid., Please try with valid information.</div>");
                        break;
                    case 401:
                        $(wrapper).html("<div class='sorry-msg'>Unauthorized access.</div>");
                        break;
                    case 403:
                        $(wrapper).html("<div class='sorry-msg'>We are unable to process your request at this time, Please try again later.</div>");
                        break;
                    case 500:
                        $(wrapper).html("<div class='sorry-msg'>We are into the system upgrade process, Please try again later.</div>");
                        break;
                    case 401:
                        $(wrapper).html("<div class='sorry-msg'>Service unavailable. Please try again later.</div>");
                        break;
                    default:
                        $(wrapper).html("<div class='sorry-msg'>Oops something went wrong, Please try again later.</div>");
                }
            }

            /**
             *  Convert date to timestamp - 2015-07-23
             * @param dates
             * @returns {number}
             * @constructor
             */
            function DateTotimeStamp(dates) {
                if (dates !== undefined) {
                    var dates1 = dates.split("-");
                    var newDate = dates1[0] + "/" + dates1[1] + "/" + dates1[2];
                    return new Date(newDate).getTime();
                }
            }

            /**
             * Get current time.
             * @returns {string}
             */

            function getCurrentDate() {
                var d = new Date($.now());
                return d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate();
            }

            // When u click on the pager, this function will be called.
            function applyPLPFacets(params) {
                $('body').addClass('loader-active');
                if (!$('.preloader').length) {
                    $('#plp-wrapper').prepend('<div class="preloader"><i class="input-loader"></i></div>');
                }
                $('html, body').stop().animate({
                    scrollTop: $('.nav-tabs').offset().top
                }, 'slow');
                $.ajax({
                    type: "GET",
                    url: "/search-output",
                    dataType: 'json',
                    data: {
                        title: _param,
                        params: params['facets'],
                        size: "9",
                        page: params['pageNum'],
                    },
                    success: function (data) {
                        var prodView = new ProdView(data);
                        var PaginationView = new paginationView(data);
                        PaginationView.undelegateEvents();


                        $('body').removeClass('loader-active');

                    }
                });
            }

            // When u click on the pager, this function will be called.
            function applydocFacets(params) {
                $('body').addClass('loader-active');
                if (!$('.preloader').length) {
                    $('#plp-wrapper').prepend('<div class="preloader"><i class="input-loader"></i></div>');
                }
                $('html, body').stop().animate({
                    scrollTop: $('#plp-wrapper').offset().top
                }, 'slow');
                $.ajax({
                    type: "GET",
                    url: "/search-doc-output",
                    dataType: 'json',
                    data: {
                        title: _param,
                        page: params['pageNum'],
                    },
                    success: function (data) {
                        if (data !== null && data.totalElements > 0) {
                            var DocView = new docView(data);
                            var PaginationView = new docpaginationView(data);
                            PaginationView.undelegateEvents();
                            $('body').removeClass('loader-active');
                            $('body').removeClass('preloader');
                        }
                    }
                });
            }

            /*
Get search value from querystring.
*/
            function getSearchParams(k) {
                var p = {};
                location.search.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (s, k, v) {
                    p[k] = v
                })
                var response = k ? p[k] : p;
                var decodedResponse = decodeURIComponent(response);
                var sanitizedResponse = removeHTMLTags(decodedResponse);
                return sanitizedResponse;
            }

            /*
                 Function to remove HTML tags.
            */

            function removeHTMLTags(decodedResponse) {
                paramText = decodedResponse;
                paramText = paramText.replace(/<script.*>[\w\W]{1,}(.*?)[\w\W]{1,}<\/script>/gi, "");
                paramText = paramText.replace(/%3Cscript.*%3E[\w\W]{1,}(.*?)[\w\W]{1,}%3C\/script%3E/gi, "");
                paramText = paramText.replace(/<style.*>[\w\W]{1,}(.*?)[\w\W]{1,}<\/style>/gi, "");
                paramText = paramText.replace(/&nbsp;/gi, " ");
                paramText = paramText.replace(/&amp;/gi, "&");
                paramText = paramText.replace(/&quot;/gi, '"');
                paramText = paramText.replace(/&lt;/gi, '<');
                paramText = paramText.replace(/&gt;/gi, '>');
                paramText = paramText.replace(/svg\/onload%3D/gi, '');
                paramText = paramText.replace(/--%21/gi, '');
                paramText = paramText.replace(/svg\/onload%3D/gi, '');
                paramText = paramText.replace(/--%21/gi, '');
                paramText = paramText.replace(/%28confirm%29/gi, '')
                paramText = paramText.replace(/%3C/gi, '');
                paramText = paramText.replace(/%3E/gi, '');
                paramText = paramText.replace(/<(?:.|\s)*?>/g, "");
                return paramText;

            }

        }

    };
})(jQuery, Drupal, drupalSettings, Backbone);

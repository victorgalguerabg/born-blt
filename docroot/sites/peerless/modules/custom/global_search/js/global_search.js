jQuery( document ).ready(function() {
    //This take user to specific part on the page - Anchor link, Jump Link
    var url = window.location.hash;
    if(url != ""){
        var getHashPart = window.location.hash.split('#');
        var getTargetSelector = getHashPart[1];
        if(jQuery('.' + getTargetSelector).length){
            var offset = jQuery('.' + getTargetSelector).offset().top - 100;
            jQuery('html, body').animate({scrollTop: offset},2000); // for all browsers
        }

    }
});
(function ($, Drupal, drupalSettings) {

    Drupal.behaviors.globalSearch = {
        attach: function (context, settings) {
          // Product Registration Form
          $(".product-registration-form .peerlessmodel_number").addClass('valid');

          //Product registration remove button
          $(window).on('load',function() {
            var removebutton = $('.webform-multiple-table .parent_product_sku_purchase_date_table_remove');
            if(removebutton.length === 1) {
              $(this).addClass('removecrossbutton');
            }
          });

          //Remove Button on Product Registration (Only one Button then Remove)
            if($(".remove-products").length) {
                $(".remove-products").val('Remove');
            }

            $(document).ajaxComplete(function(event, xhr, settings) {
              var getExtraObj = settings.extraData;
              if (getExtraObj !== undefined && getExtraObj.hasOwnProperty("_triggering_element_name") && xhr.status == 200
              && settings.extraData != undefined && settings.extraData._triggering_element_value !== 'undefined' &&
              settings.extraData._triggering_element_name === "product_image_1_upload_button") {
                $('input[name="product_image_1_remove_button"]').addClass('remove1');
                $('input[name="product_image_1_remove_button"]').parent().siblings(".field-prefix").remove();
              }
              if (getExtraObj !== undefined && getExtraObj.hasOwnProperty("_triggering_element_name") && xhr.status == 200
              && settings.extraData != undefined && settings.extraData._triggering_element_value !== 'undefined' &&
              settings.extraData._triggering_element_name === "product_image_2_upload_button") {
                $('input[name="product_image_2_remove_button"]').addClass('remove1');
                $('input[name="product_image_2_remove_button"]').parent().siblings(".field-prefix").remove();
              }
              if (getExtraObj !== undefined && getExtraObj.hasOwnProperty("_triggering_element_name") && xhr.status == 200
              && settings.extraData != undefined && settings.extraData._triggering_element_value !== 'undefined' &&
              settings.extraData._triggering_element_name === "product_image_3_upload_button") {
                $('input[name="product_image_3_remove_button"]').addClass('remove1');
                $('input[name="product_image_3_remove_button"]').parent().siblings(".field-prefix").remove();
              }
            });
            if($(".inner-link").length){
                $(".inner-link").click(function(){
                    var get_target_selector = $(this).attr("rel");
                    $("#"+get_target_selector).parent().show();
                });
            }
            if ($('.btn-support-browse').length) {
                $('.btn-support-browse').click(function () {
                    if ($("#edit-product-type").val() === '') {
                        $("#edit-product-type").addClass('error');
                        return false;
                    }
                });
            }

            var xhr;
            $('form#global-search-form input.autocomplete').keyup(function (event) {
                var searchText = $(this).val();
                if (searchText.length > 2) {
                    fn(searchText);
                    $('.autocomplete-container--inner .input-close').addClass('hide');
                    $('.autocomplete-container--inner .input-loader').removeClass('hide');
                } else {
                    if (xhr) {
                        xhr.abort();
                    }
                    $("#search-block-suggestion-result").empty();
                    $('.autocomplete-container--inner .input-loader, .autocomplete-container--inner .input-close').addClass('hide');
                }
            });

            $(document).on('click', '#search-block-suggestion-result li.record', function () {
                var itemValue = $(this).attr('data-item-value');
                $('form#global-search-form input.autocomplete').val(itemValue);
                $("#search-block-suggestion-result").empty();
            });

            $('.autocomplete-container--inner .input-close').click(function () {
                $('form#global-search-form input.autocomplete').val('');
                $("#search-block-suggestion-result").empty();
                $('.autocomplete-container--inner .input-close').addClass('hide');
                $('.autocomplete-container--inner input.autocomplete').focus();
            });
            var fn = function (searchText) {
                if (xhr && xhr.readyState != 4) {
                    xhr.abort();
                }
                var output = '';
                $('.autocomplete-container--inner .input-loader').removeClass('hide');
                var searchUrl = drupalSettings.path.baseUrl + "global-search-autocomplete";
                xhr = $.get(searchUrl, {text: searchText});
                xhr.done(function (data) {
                    if (data == '') {
                        output = "<div class=\"pad-aa text-align-center\">No Results Found!</div>";
                    } else {
                        output = data;
                    }
                    $("#search-block-suggestion-result").html(output);
                    $('.autocomplete-container--inner .input-loader').addClass('hide');
                    $('.autocomplete-container--inner .input-close').removeClass('hide');
                });
            };
            if ($(".get-support").length) {
                $('.get-support select').change(function () {
                    window.location.href = this.value;
                });
            }
            //set as go-back as class, which will redirect to previous page.
            if ($('a.back').length) {
                $('a.back').click(function () {
                    parent.history.back();
                    return false;
                });
            }

            /*
Get search value from querystring.
*/
            function getSearchParams(k) {
                var p = {};
                location.search.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (s, k, v) {
                    p[k] = v
                })
                var response = k ? p[k] : p;
                var decodedResponse = decodeURIComponent(response);
                var sanitizedResponse = removeHTMLTags(decodedResponse);
                return sanitizedResponse;
            }

            /*
                 Function to remove HTML tags.
            */

            function removeHTMLTags(decodedResponse) {
                paramText = decodedResponse;
                paramText = paramText.replace(/<script.*>[\w\W]{1,}(.*?)[\w\W]{1,}<\/script>/gi, "");
                paramText = paramText.replace(/%3Cscript.*%3E[\w\W]{1,}(.*?)[\w\W]{1,}%3C\/script%3E/gi, "");
                paramText = paramText.replace(/<style.*>[\w\W]{1,}(.*?)[\w\W]{1,}<\/style>/gi, "");
                paramText = paramText.replace(/&nbsp;/gi, " ");
                paramText = paramText.replace(/&amp;/gi, "&");
                paramText = paramText.replace(/&quot;/gi, '"');
                paramText = paramText.replace(/&lt;/gi, '<');
                paramText = paramText.replace(/&gt;/gi, '>');
                paramText = paramText.replace(/svg\/onload%3D/gi, '');
                paramText = paramText.replace(/--%21/gi, '');
                paramText = paramText.replace(/svg\/onload%3D/gi, '');
                paramText = paramText.replace(/--%21/gi, '');
                paramText = paramText.replace(/%28confirm%29/gi, '')
                paramText = paramText.replace(/%3C/gi, '');
                paramText = paramText.replace(/%3E/gi, '');
                paramText = paramText.replace(/<(?:.|\s)*?>/g, "");
                return paramText;

            }



            var professionalsfrm = $('#professionals_search_block_form');
            professionalsfrm.submit(function (e) {
                e.preventDefault();
                $.ajax({
                    type: professionalsfrm.attr('method'),
                    url: '/ajax/block/professional_search_block',
                    data: professionalsfrm.serialize(),
                    success: function (data) {
                        if(data[0].url){
                           window.location = data[0].url;
                        }
                    },
                    error: function (data) {
                        console.log('An error occurred.');
                        console.log(data);
                    },
                });
            });

        }
    };
})(jQuery, Drupal, drupalSettings);

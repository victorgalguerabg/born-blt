(function ($, Drupal, drupalSettings, Backbone) {
    Drupal.behaviors.plpBehaviour = {
        attach: function (context, settings) {

            $('document').ready(function () {
                // Add advanced class when the url having the filter as query string.
                var _filter = getSearchParams('documentLookFor');
                if (_filter !== '' && _filter !== undefined) {
                    $('#plp-wrapper').addClass('advance-search');
                } else {
                    if ($('#plp-wrapper advance-search').length) {
                        $('#plp-wrapper').removeClass('advanced');
                    }
                }

                $('.product-see-all').click(function () {
                    $('ul.nav-tabs li.active').removeClass('active');
                    $('#productTabLink').parent('li').addClass('active');
                });
                $('.document-see-all').click(function () {
                    $('ul.nav-tabs li.active').removeClass('active');
                    $('#documentTabLink').parent('li').addClass('active');

                    $('html, body').stop().animate({
                        scrollTop: $('.nav-tabs').offset().top
                    }, 'slow');

                });
                $('.content-see-all').click(function () {
                    $('ul.nav-tabs li.active').removeClass('active');
                    $('#contentTabLink').parent('li').addClass('active');

                    $('html, body').stop().animate({
                        scrollTop: $('.nav-tabs').offset().top
                    }, 'slow');

                });
            })

            var _totalCount = 0;
            var docView = Backbone.View.extend({
                el: ".documentoutput",
                template: _.template($("#document-output").html()),
                initialize: function (initData) {
                    this.render(initData);
                    this.afterRender(initData);
                },
                render: function (initData) {

                    var typedKeyword = getSearchParams('title');
                    var noResultsMessage = "<div class='noResults no-rows-doc'>We're sorry, we didn't find any results for " + decodeURIComponent(typedKeyword) + ". We recommend using different words in your search.</div>";
                    if (initData === null) {
                        $("#document-tab div.row h3, .paging-compare-bar").hide();
                        $("#document-tab .documentoutput").html(noResultsMessage);
                        $("#documentTabLink").hide();
                        $("#search-document-title").hide();
                        $(".document-see-all").hide();
                        return false;
                    }
                    var _self = this;
                    var _render = "";
                    var content = initData.content;
                    if (content.length > 0) {
                        _.each(initData.content, function (entry) {
                            var _filePath = entry.filePath;
                            var _fileName = entry.fileName;
                            var _docType = entry.documentType;
                            _render += _self.template({
                                fileName: _fileName,
                                fileLinks: entry.modelNumber.slice(0, 5),
                                filePath: _filePath,
                                docType: _docType,
                            });
                            _self.$el.html(_render);
                            $('.preloader').remove();
                            $('body').removeClass('loader-active');
                            $('.tab-content-title-doc').show();
                        });
                    }

                    var _documentCount = initData.totalElements;
                    _totalCount = parseInt(_totalCount) + parseInt(_documentCount);
                    $('#documentTabLink span').html('Documents (' + _documentCount + ')');
                    $('#search-document-title').html('Documents (' + _documentCount + ')').attr('data-document-cnt', _documentCount);
                    recalculate();
                    return this;
                },
                afterRender: function (initData) {
                    if (initData != null) {
                        var _documentCount = initData.totalElements;
                        if (_documentCount > 0) {
                            var _filter = getSearchParams('documentLookFor');
                            //lets select document tab as default when user go with advanced search.
                            if (_filter !== "" && _filter !== undefined && (_filter === 'ts' || _filter === 'mi' || _filter === 'pd')) {
                                $("#documentTabLink").trigger('click');
                            }
                        }

                    }

                }
            });

            // Process content data and assemble the page.
            var contView = Backbone.View.extend({
                el: ".contentoutput",
                el1: ".contentoutputfull",
                template: _.template($("#content-output").html()),
                template1: _.template($("#full-content-output").html()),
                initialize: function (content, _sno) {
                    if (content.currentpage === 1) {
                        this.renderAllsectionContent(content, _sno);
                    }
                    this.renderContentSection(content, _sno);
                },
                renderAllsectionContent: function (content, _sno) {
                    var _self = this;
                    var _render = "";
                    if (content.totalmatches > 0) {
                        var matches = content.matches.slice(0, 8);
                        var counter = _sno * 20;
                        var _incrCnter = counter + 1;
                        _.each(matches, function (entry) {
                            var _title = entry.title;
                            var _body = entry.search_api_excerpt;
                            var _url = entry.url;
                            _render += _self.template({
                                searchContentBody: _body,
                                searchContentUrl: _url,
                                searchContentTitle: _title,
                                searchSno: _incrCnter,
                            });
                            _self.$el.html(_render);
                            _incrCnter++;
                        });
                    }
                    $('.tab-content-title-cont').show();
                    recalculate();
                    return this;
                },
                renderContentSection: function (content, _sno) {
                    var _sno = parseInt(_sno);
                    var _self = this;
                    var _render = "";
                    if (content.totalmatches > 0) {
                        var matches = content.matches;
                        var counter = _sno * 20;
                        var _incrCnter = counter + 1;
                        _.each(matches, function (entry) {
                            var _title = entry.title;
                            var _body = entry.search_api_excerpt;
                            var _url = entry.url;
                            _render += _self.template1({
                                searchContentBody: _body,
                                searchContentUrl: _url,
                                searchContentTitle: _title,
                                searchSno: _incrCnter,
                            });
                            $('#fullcontentoutput').html(_render);
                            $('.preloader').remove();
                            $('body').removeClass('loader-active');
                            _incrCnter++;
                        });
                    }
                    return this;
                }
            });

            var ProdView = Backbone.View.extend({

                el: "#output",
                el2: "#output-1",
                template: _.template($("#output-node").html()),
                template2: _.template($("#output-node-1").html()),

                initialize: function (initData, eventName) {
                    this.render(initData, eventName);
                    this.renderFullProducts(initData);
                },

                render: function (initData, eventName) {
                    if (!$('.preloader').length) {
                        $('#plp-wrapper').prepend('<div class="preloader"><i class="input-loader"></i></div>');
                    }

                    var _self = this;
                    var _render = "";
                    var _i = 0;
                    if (initData.content != '') {
                        _.each(initData.content, function (entry) {
                            if (_i == 8) {
                                return false;
                            }
                            var _price = 0;
                            var _filter = getSearchParams('documentLookFor');
                            if (_filter !== '' || _filter !== undefined) {
                                var _price = entry.values.ListPrice;
                            }
                            var _heroImage = entry.heroImageSmall;
                            var _SyndigoActive = '';
                            if (entry.values.MaterialStatus !== undefined && entry.values.MaterialStatus >= 50) {
                                var _SyndigoActive = 'discontinued';
                            }
                            var orderDate = (entry.values.AvailableToOrderDate !== '' || entry.values.AvailableToOrderDate !== undefined) ? DateTotimeStamp(entry.values.AvailableToOrderDate) : '';
                            var shipDate = (entry.values.AvailableToShipDate !== '' || entry.values.AvailableToShipDate !== undefined) ? DateTotimeStamp(entry.values.AvailableToShipDate) : '';
                            var currentDate = DateTotimeStamp(getCurrentDate());
                            if (orderDate === '' || shipDate === '') {
                                shipDate = currentDate - (60 * 60 * 24);
                            }
                            if (currentDate < shipDate) {
                                if (currentDate > orderDate) {
                                    var _SyndigoActive = 'coming soon';
                                }
                            }
                            var _title = "Title";
                            if (entry.description !== '' && entry.description !== undefined) {
                                _title = entry.description;
                            }

                            var _collection = "PEERLESS";
                            //if(_filter === ''){
                            if (entry.values.Collection !== "" && entry.values.Collection !== undefined) {
                                _collection = entry.values.Collection;
                            }
                            //}

                            var _sku = entry.name;
                            var urlAliasObject = drupalSettings.urlAliasObject;
                            var _url = '';
                            if (typeof urlAliasObject[_sku] != 'undefined') {
                                _url = urlAliasObject[_sku];
                            }
                            if (_url === '' || _url === undefined) {
                                _url = '/pdp/' + _sku;
                            }

                            _render += _self.template({
                                title: _title,
                                collectionName: _collection,
                                heroImage: _heroImage,
                                sku: _sku,
                                price: _price,
                                url: _url,
                                prodOutDated: _SyndigoActive,
                            });
                            _self.$el.html(_render);
                            $('.tab-content-title-prod').show();
                            $('.preloader').remove();
                            $('body').removeClass('loader-active');
                            _i++;
                        });
                    } else {
                        var noResultsMessage = '<div role="contentinfo" aria-label="Error message"><h2 class="visually-hidden">Status message</h2>Oops!!! Nothing Found. Please refine your search</div>';
                        $('#output').html(noResultsMessage);
                        $('.preloader').remove();
                        $('body').removeClass('loader-active');
                    }
                    var _productCount = initData.totalElements;
                    _totalCount = parseInt(_totalCount) + parseInt(_productCount);

                    $('#productTabLink span').html('Products (' + _productCount + ')');
                    $('#search-product-title').html('Products (' + _productCount + ')').attr('data-product-cnt', _productCount);
                    recalculate();
                    //This will prevent the " All " tab total count messup.
                    // if (eventName === undefined) {
                    //   $('#allTabLink span').html('All (' + _totalCount + ')');
                    // }

                    return this;
                },
                renderFullProducts: function (initData) {
                    var typedKeyword = getSearchParams('title');
                    var noResultsMessage = "<div class='noResults no-rows-pro'>We're sorry, we didn't find any results for " + decodeURIComponent(typedKeyword) + ". We recommend using different words in your search.</div>";
                    if (initData.content.length === 0) {
                        $("#output-1").html(noResultsMessage);
                        $("#productTabLink").hide();
                    }
                    var _self = this;
                    var _render = "";
                    if (initData.content != '') {
                        _.each(initData.content, function (entry) {
                            var _price = entry.values.ListPrice;
                            var _heroImage = entry.heroImageSmall;
                            var _SyndigoActive = '';
                            if (entry.values.MaterialStatus !== undefined && entry.values.MaterialStatus >= 50) {
                                var _SyndigoActive = 'discontinued';
                            }
                            var orderDate = (entry.values.AvailableToOrderDate !== '' || entry.values.AvailableToOrderDate !== undefined) ? DateTotimeStamp(entry.values.AvailableToOrderDate) : '';
                            var shipDate = (entry.values.AvailableToShipDate !== '' || entry.values.AvailableToShipDate !== undefined) ? DateTotimeStamp(entry.values.AvailableToShipDate) : '';
                            var currentDate = DateTotimeStamp(getCurrentDate());
                            if (orderDate === '' || shipDate === '') {
                                shipDate = currentDate - (60 * 60 * 24);
                            }
                            if (currentDate < shipDate) {
                                if (currentDate > orderDate) {
                                    var _SyndigoActive = 'coming soon';
                                }
                            }
                            var _title = "Title";
                            if (entry.description != '') {
                                _title = entry.description;
                            }
                            var _collection = "PEERLESS";
                            if (entry.values.Collection !== "" && entry.values.Collection !== undefined) {
                                _collection = entry.values.Collection;
                            }
                            var _sku = entry.name;
                            var urlAliasObject = drupalSettings.urlAliasObject;
                            var _url = '';
                            if (typeof urlAliasObject[_sku] != 'undefined') {
                                _url = urlAliasObject[_sku];
                            }
                            if (_url === '' || _url === undefined) {
                                _url = '/pdp/' + _sku;
                            }
                            _render += _self.template2({
                                title: _title,
                                collectionName: _collection,
                                heroImage: _heroImage,
                                sku: _sku,
                                price: _price,
                                url: _url,
                                prodOutDated: _SyndigoActive,
                            });
                            $('#output-1').html(_render);
                            //  $('.preloader').remove();
                            // $('body').removeClass('loader-active');
                        });
                    } else {
                        var typedKeyword = getSearchParams('title');
                        var noResultsMessage = "<div class='noResults no-rows-pro'>We're sorry, we didn't find any results for " + decodeURIComponent(typedKeyword) + ". We recommend using different words in your search.</div>";
                        $('#output').html(noResultsMessage);
                        $("#product-tab div.row h3, .paging-compare-bar").hide();
                        $('.preloader').remove();
                        $("#productTabLink").hide();
                        $('body').removeClass('loader-active');
                    }
                    var _productCount = initData.totalElements;
                    $('#productTabLink span').html('Products (' + _productCount + ')');
                    return this;
                }

            });

            var paginationView = Backbone.View.extend({
                el: ".pagination-output",
                template: _.template($("#pagination").html()),
                initialize: function (initData) {
                    this.render(initData);
                },
                events: {
                    'click [type="button"]': 'pagination'
                },
                pagination: function (e) {
                    var params = [];
                    params['pageNum'] = $(e.currentTarget).val();
                    applyPLPFacets(params, 'pager');
                },
                render: function (initData) {
                    var _self = this;
                    var _render = "";
                    //stopLoop value
                    var _nextPage = parseInt(initData.number) + 1;
                    var _previousPage = parseInt(initData.number) - 1;
                    _render += _self.template({
                        totalPager: initData.totalPages,
                        currentPage: _nextPage,
                        nextPage: _nextPage,
                        previousPage: _previousPage,
                        totalRecordset: initData.totalElements
                    });
                    _self.$el.html(_render);
                }
            });

            var docpaginationView = Backbone.View.extend({
                el: ".doc-pagination-output",
                template: _.template($("#doc-pagination").html()),
                initialize: function (initData) {
                    this.render(initData);
                },
                events: {
                    'click [type="button"]': 'docpagination'
                },
                docpagination: function (e) {
                    var params = [];
                    params['pageNum'] = $(e.currentTarget).val();
                    applydocFacets(params);
                },
                render: function (initData) {
                    var _self = this;
                    var _render = "";
                    //stopLoop value
                    var _nextPage = (initData === null) ? 0 : parseInt(initData.number) + 1;
                    var _previousPage = (initData === null) ? 0 : parseInt(initData.number) - 1;
                    _render += _self.template({
                        totalPager: (initData === null) ? 0 : initData.totalPages,
                        currentPage: _nextPage,
                        nextPage: _nextPage,
                        previousPage: _previousPage,
                        totalRecordset: (initData === null) ? 0 : initData.totalElements
                    });
                    _self.$el.html(_render);
                }
            });


            // Pagination fot content section.
            var contpaginationView = Backbone.View.extend({
                el: ".content-pagination-output",
                template: _.template($("#content-pagination").html()),
                initialize: function (initData) {
                    this.render(initData);
                },
                events: {
                    'click [type="button"]': 'contentpagination'
                },
                contentpagination: function (e) {
                    var params = [];
                    params['pageNum'] = parseInt($(e.currentTarget).val()); // Current
                                                                            // Page
                                                                            // number
                    applycontFacets(params);
                },
                render: function (initData) {
                    if (initData === null) {
                        $('.documentoutput').html('<h4>No results at this time.</h4>');
                        return false;
                    }
                    var cont_disp_cnt = 20;//drupalSettings.cont_disp_cnt;
                    if (cont_disp_cnt == null) {
                        cont_disp_cnt = 10;
                    }
                    var _self = this;
                    var _render = "";
                    var _currentpage = initData.currentpage;  // Current page
                    var _totalmatches = initData.totalmatches;
                    var _totalpages = Math.ceil(_totalmatches / cont_disp_cnt);
                    var _nextPage;
                    if (_currentpage < _totalpages) {
                        _nextPage = _currentpage + 1;
                    } else {
                        _nextPage = _currentpage;
                    }
                    var _previousPage;
                    if (_currentpage === 1) {
                        _previousPage = 1;
                    } else {
                        _previousPage = _currentpage - 1;
                    }

                    _render += _self.template({
                        totalPager: _totalpages,
                        currentPage: _currentpage,
                        nextPage: _nextPage,
                        previousPage: _previousPage,
                        totalRecordset: initData.totalmatches
                    });
                    _self.$el.html(_render);
                }
            });
            if (context === document) {
                var pathArray = window.location.pathname.split('/');
                if (pathArray[1] === 'replacement-parts-search-results') {
                    var _param = getSearchParams('title');
                    var _filter = getSearchParams('documentLookFor');
                    $.ajax({
                        type: "GET",
                        url: "/search-doc-output",
                        dataType: 'json',
                        data: {
                            title: _param,
                            documentLookFor: _filter,
                        },
                        complete: function () {
                            recalculate();
                        },
                        success: function (data) {
                            var DocView = new docView(data);
                            var DocpaginationView = new docpaginationView(data);
                        }
                    });

                } else {
                    var _param = getSearchParams('title');
                    var _filter = getSearchParams('documentLookFor');
                    $.ajax({
                        type: "GET",
                        url: "/search-output",
                        dataType: 'json',
                        data: {
                            title: _param,
                            documentLookFor: _filter,
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            showErrorMessage(XMLHttpRequest.status, '#all-tab #output');
                            showErrorMessage(XMLHttpRequest.status, '#all-tab #output');

                        },
                        beforeSend: function () {
                            $('#all-tab #output').addClass('loader-active');
                            if ($('#all-tab .product-cal').length === 0) {
                                $('#plp-wrapper').prepend('<div class="product-call"><i class="input-loader"></i></div>');
                            }
                        },
                        complete: function () {
                            $('.product-call').remove();
                            $('#all-tab #output').removeClass('loader-active');
                            // Manage tag records
                            recalculate();

                        },
                        success: function (data) {
                            var prodView = new ProdView(data);
                            var PaginationView = new paginationView(data);

                        }
                    });
                    $.ajax({
                        type: "GET",
                        url: "/search-doc-output",
                        dataType: 'json',
                        data: {
                            title: _param,
                            documentLookFor: _filter,
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            showErrorMessage(XMLHttpRequest.status, '#all-tab #output');
                        },
                        beforeSend: function () {
                            $('#all-tab #output').addClass('loader-active');
                            if ($('#all-tab .product-cal').length === 0) {
                                $('#plp-wrapper').prepend('<div class="product-call"><i class="input-loader"></i></div>');
                            }
                        },
                        complete: function () {
                            $('.product-call').remove();
                            $('#all-tab #output').removeClass('loader-active');
                            recalculate();
                        },
                        success: function (data) {
                            var DocView = new docView(data);
                            var DocpaginationView = new docpaginationView(data);
                            var DocpaginationView = new docpaginationView(data);
                        }
                    });
                    // Matching records for content being fetched and processed based on
                    // matching record count.
                    var content_matchings = getContentSearchResults(_param, page = 1);
                    if (content_matchings.totalmatches !== undefined) {
                        _totalCount = content_matchings.totalmatches;
                    }
                    $('#contentTabLink span').html('Content (' + _totalCount + ')');
                    $('#search-content-title').html('Content (' + _totalCount + ')').attr('data-content-cnt', _totalCount);
                    if (_totalCount > 0) {
                        var ContView = new contView(content_matchings, 0);
                        var ContpaginationView = new contpaginationView(content_matchings);
                    } else {
                        $('#content-tab .row').html("<b>Nothing found. Please refine your search.</b>");
                        var typedKeyword = getSearchParams('title');
                        var noResultsMessage = "<div class='noResults'>We're sorry, we didn't find any results for " + decodeURIComponent(typedKeyword) + ". We recommend using different words in your search.</div>";
                        $('#content-tab .row').html(noResultsMessage);
                        $("#contentTabLink").hide();
                        $("#search-content-title").hide();
                        $(".content-see-all").hide();
                    }
                }
                //lets select document tab as default when user go with advanced search.
                if (_filter !== "" && _filter !== undefined && (_filter === 'ts' || _filter === 'mi' || _filter === 'pd')) {
                    $("#documentTabLink").trigger('click');
                }

            }


            var showErrorMessage = function (status, wrapper) {
                switch (status) {
                    case 400:
                        $(wrapper).html("<div class='sorry-msg'>Server understood the request, but request content was invalid., Please try with valid information.</div>");
                        break;
                    case 401:
                        $(wrapper).html("<div class='sorry-msg'>Unauthorized access.</div>");
                        break;
                    case 403:
                        $(wrapper).html("<div class='sorry-msg'>We are unable to process your request at this time, Please try again later.</div>");
                        break;
                    case 500:
                        $(wrapper).html("<div class='sorry-msg'>We are into the system upgrade process, Please try again later.</div>");
                        break;
                    case 401:
                        $(wrapper).html("<div class='sorry-msg'>Service unavailable. Please try again later.</div>");
                        break;
                    default:
                        $(wrapper).html("<div class='sorry-msg'>Oops something went wrong, Please try again later.</div>");
                }
            }

            /**
             * Recalculate tab total counts of records
             */
            function recalculate() {
                var grandTotal = 0;
                var data_document_cnt = $("#search-document-title").attr("data-document-cnt");
                data_document_cnt = parseInt(data_document_cnt);
                if (data_document_cnt === 0) {
                    var _filter = getSearchParams('documentLookFor');
                    //lets select document tab as default when user go with advanced search.
                    if (_filter !== "" && _filter !== undefined && (_filter === 'ts' || _filter === 'mi' || _filter === 'pd')) {
                        $("#allTabLink").trigger('click');
                        $(".tab-content-title-doc, .no-rows-doc").hide();
                    }

                }
                var data_product_cnt = $("#search-product-title").attr("data-product-cnt");
                data_product_cnt = parseInt(data_product_cnt);
                if (data_product_cnt === 0) {
                    $(".tab-content-title-prod, .no-rows-pro").hide();
                }
                var data_content_cnt = $("#search-content-title").attr("data-content-cnt");
                data_content_cnt = parseInt(data_content_cnt);
                if (data_content_cnt === 0) {
                    $(".tab-content-title-cont, .no-rows-con").hide();
                }
                grandTotal = data_document_cnt + data_product_cnt + data_content_cnt;
                $('#allTabLink span').html('All (' + grandTotal + ')');
                if (grandTotal === 0) {
                    $('.tab-content-title-doc').hide();
                    $('.tab-content-title-cont').hide();
                    $('.tab-content-title-prod').hide();
                    var typedKeyword = getSearchParams('title');
                    var noResultsMessage = "<div class='noResultsFound'>We're sorry, we didn't find any results for " + decodeURIComponent(typedKeyword) + ". We recommend using different words in your search.</div>";
                    $(".no-rows-pro").html(noResultsMessage).show();
                }
            }

            /**
             *  Convert date to timestamp - 2015-07-23
             * @param dates
             * @returns {number}
             * @constructor
             */
            function DateTotimeStamp(dates) {
                if (dates !== undefined) {
                    var dates1 = dates.split("-");
                    var newDate = dates1[0] + "/" + dates1[1] + "/" + dates1[2];
                    return new Date(newDate).getTime();
                }
            }

            /**
             * Get current time.
             * @returns {string}
             */

            function getCurrentDate() {
                var d = new Date($.now());
                return d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate();
            }

            /* Define functin to find and replace specified term with replacement string */
            function replaceAll(str, term, replacement) {
                return str.replace(new RegExp(escapeRegExp(term), 'g'), replacement);
            }


            /*
       Get search value from querystring.
       */
            function getSearchParams(k) {
                var p = {};
                location.search.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (s, k, v) {
                    p[k] = v
                })
                var response = k ? p[k] : p;
                var decodedResponse = decodeURIComponent(response);
                var sanitizedResponse = removeHTMLTags(decodedResponse);
                return sanitizedResponse;
            }

            /*
                 Function to remove HTML tags.
            */

            function removeHTMLTags(decodedResponse) {
                paramText = decodedResponse;
                paramText = paramText.replace(/<script.*>[\w\W]{1,}(.*?)[\w\W]{1,}<\/script>/gi, "");
                paramText = paramText.replace(/%3Cscript.*%3E[\w\W]{1,}(.*?)[\w\W]{1,}%3C\/script%3E/gi, "");
                paramText = paramText.replace(/<style.*>[\w\W]{1,}(.*?)[\w\W]{1,}<\/style>/gi, "");
                paramText = paramText.replace(/&nbsp;/gi, " ");
                paramText = paramText.replace(/&amp;/gi, "&");
                paramText = paramText.replace(/&quot;/gi, '"');
                paramText = paramText.replace(/&lt;/gi, '<');
                paramText = paramText.replace(/&gt;/gi, '>');
                paramText = paramText.replace(/svg\/onload%3D/gi, '');
                paramText = paramText.replace(/--%21/gi, '');
                paramText = paramText.replace(/svg\/onload%3D/gi, '');
                paramText = paramText.replace(/--%21/gi, '');
                paramText = paramText.replace(/%28confirm%29/gi, '')
                paramText = paramText.replace(/%3C/gi, '');
                paramText = paramText.replace(/%3E/gi, '');
                paramText = paramText.replace(/<(?:.|\s)*?>/g, "");
                return paramText;

            }


            /*
            To return content search results based on current search value and requesting page based on previous or next button hits in pagination.
            @params: search value, requesting page
            @return: search result
             */
            function getContentSearchResults(params, requestingpage) {
                var search_result = [];
                search_result.currentpage = requestingpage;
                var matching_count = 0;
                $.ajax({
                    type: "GET",
                    url: "/rest/export/search_content?_format=json",
                    dataType: 'json',
                    async: false,
                    data: {
                        title: _param,
                        page: requestingpage - 1,  // Since views counts from 0
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        showErrorMessage(XMLHttpRequest.status, '#document-tab');
                    },
                    beforeSend: function () {
                        $('body').addClass('loader-active');
                        if ($('.preloader').length === 0) {
                            $('#plp-wrapper').prepend('<div class="preloader"><i class="input-loader"></i></div>');
                        }
                    },
                    complete: function () {
                        $('.preloader').remove();
                        $('body').removeClass('loader-active');
                    },
                    success: function (data) {
                        var matches = [];
                        $.each(data, function (key, value) {
                            matches.push(value);
                        });
                        search_result.matches = matches;

                        $.ajax({
                            type: "GET",
                            url: "/rest/export/matching_records_count?_format=json",
                            dataType: 'json',
                            async: false,
                            data: {
                                title: _param,
                            },
                            success: function (data) {
                                matching_count = data.length;
                            }
                        });

                        search_result.totalmatches = matching_count;
                    }
                });

                return search_result;
            }

            function applyPLPFacets(params, eventName) {
                $('body').addClass('loader-active');
                if (!$('.preloader').length) {
                    $('#plp-wrapper').prepend('<div class="preloader"><i class="input-loader"></i></div>');
                }
                $('html, body').stop().animate({
                    scrollTop: $('.nav-tabs').offset().top
                }, 'slow');
                $.ajax({
                    type: "GET",
                    url: "/search-output",
                    dataType: 'json',
                    data: {
                        title: _param,
                        params: params['facets'],
                        size: "9",
                        page: params['pageNum'],
                    },

                    success: function (data) {
                        var prodView = new ProdView(data, eventName);
                        var PaginationView = new paginationView(data);
                        PaginationView.undelegateEvents();


                        $('body').removeClass('loader-active');

                    }
                });
            }

            function applydocFacets(params) {
                $('body').addClass('loader-active');
                if (!$('.preloader').length) {
                    $('#plp-wrapper').prepend('<div class="preloader"><i class="input-loader"></i></div>');
                }
                $('html, body').stop().animate({
                    scrollTop: $('#plp-wrapper').offset().top
                }, 'slow');
                $.ajax({
                    type: "GET",
                    url: "/search-doc-output",
                    dataType: 'json',
                    data: {
                        title: _param,
                        page: params['pageNum'],
                    },
                    success: function (data) {
                        var DocView = new docView(data);
                        var PaginationView = new docpaginationView(data);
                        PaginationView.undelegateEvents();

                        $('body').removeClass('loader-active');
                        $('body').removeClass('preloader');

                    }
                });
            }

            /*
            This function will be called when user hits on certain page(previous or next) on pagination.
            Fetches contents based on current search value and page number requested
            @params: pagenumber
             */
            function applycontFacets(params) {
                $('body').addClass('loader-active');
                if (!$('.preloader').length) {
                    $('.search-result-page').prepend('<div class="preloader"><i class="input-loader"></i></div>');
                }
                $('html, body').stop().animate({
                    scrollTop: $('.search-result-page').offset().top
                }, 'slow');
                var content_matchings = getContentSearchResults(_param, page = params['pageNum']);
                var ContView = new contView(content_matchings, (params['pageNum'] - 1));
                var ContpaginationView = new contpaginationView(content_matchings);
                ContpaginationView.undelegateEvents();
                $('body').removeClass('loader-active');
                $('body').removeClass('preloader');
            }


        }

    };
})(jQuery, Drupal, drupalSettings, Backbone);

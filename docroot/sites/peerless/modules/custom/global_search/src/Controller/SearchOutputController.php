<?php

namespace Drupal\global_search\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Implementation of search controller.
 */
class SearchOutputController extends ControllerBase {

  /**
   * To get current query string.
   *
   * @var Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;
  /**
   * To handle search term.
   *
   * @var searchKey
   */
  public $searchKey;
  /**
   * Flag to dedect advance or normal search.
   *
   * @var advSearchKey
   */
  public $advSearchKey;
  /**
   * To capture source key.
   *
   * @var sourceKey
   */
  public $sourceKey;
  /**
   * To handle response.
   *
   * @var response
   */
  public $response;
  /**
   * To handle page number.
   *
   * @var pageNum
   */
  public $pageNum;
  /**
   * To get current current url.
   *
   * @var getCurUrl
   */
  public $getCurUrl;

  /**
   * Implementation of constructor.
   */
  public function __construct(RequestStack $requestStack) {
    $this->requestStack = $requestStack;
    $this->getCurUrl = $this->requestStack->getCurrentRequest()->getRequestUri();
    $this->searchKey = $this->requestStack->getCurrentRequest()->get('title');
    $this->sourceKey = $this->requestStack->getCurrentRequest()->get('source');
    $this->advSearchKey = $this->requestStack->getCurrentRequest()->get('documentLookFor');
    $this->response = new Response();
    $this->pageNum = 0;
    if (!empty($this->requestStack->getCurrentRequest()->get('page'))) {
      $this->pageNum = $this->requestStack->getCurrentRequest()->get('page');
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
    // Load the service required to construct this class.
      $container->get('request_stack')
    );
  }

  /**
   * Implementation of search output.
   */
  public function getSearchOutput() {
    $includeObsolete = '&includeObsolete=true';
    if (isset($this->sourceKey) && $this->sourceKey === 'prof') {
      $includeObsolete = '&includeObsolete=false';
    }
    //find -140 from sku and remove it.
    $getLastFourChar = substr($this->searchKey, -4);
    if($getLastFourChar == '-140'){
      $this->searchKey = str_replace($getLastFourChar, '', $this->searchKey);
    }
    $config = $this->config('sync.api_settings');
    $configPrimary = $this->config('delta_services.deltaservicesconfig');
    $domain = $configPrimary->get('api_base_path');
    $searchApiUrl = $domain . $config->get('sync.global_search_products');
    $url = $searchApiUrl . $this->searchKey . $includeObsolete . '&WebsitePeerlessUS=T&page=' . $this->pageNum;
    $productRequest = web_services_api_call($url, "GET", [], "", "Search");
    if ($productRequest['status'] == 'SUCCESS') {
      $this->response->setContent($productRequest['response']);
      $this->response->headers->set('Content-Type', 'application/json');
      return $this->response;
    }

  }

  /**
   * Implementation of search document output.
   */
  public function getDocumentSearchOutput() {
    //find -140 from sku and remove it.
    $getLastFourChar = substr($this->searchKey, -4);
    if($getLastFourChar == '-140'){
      $this->searchKey = str_replace($getLastFourChar, '', $this->searchKey);
    }
    $config = $this->config('sync.api_settings');
    $configPrimary = $this->config('delta_services.deltaservicesconfig');
    $domain = $configPrimary->get('api_base_path');
    $documentSearchApi = $domain . $config->get('sync.product_document_search');
    $pagerSize = $config->get('sync.doc_disp_cnt');
    $url = $documentSearchApi . $this->searchKey . '&size=' . $pagerSize . '&brand=peerless&from=' . $this->pageNum;
    if (!empty($this->advSearchKey)) {
      $filterKey = '';
      if ($this->advSearchKey == 'ts') {
        $filterKey = '&documentType=SpecSheet';
      }
      else {
        if ($this->advSearchKey == 'mi') {
          $filterKey = '&documentType=mandi';
        }
        else {
          if ($this->advSearchKey == 'pd') {
            $filterKey = '&documentType=partsdiagram';
          }
        }
      }
      $url = $documentSearchApi . $this->searchKey . '&size=' . $pagerSize . $filterKey . '&from=' . $this->pageNum . '&brand=peerless';
    }

    $documentRequest = web_services_api_call($url, "GET", [], "", "Search");
    if ($documentRequest['status'] == 'SUCCESS') {
      $this->response->setContent($documentRequest['response']);
      $this->response->headers->set('Content-Type', 'application/json');
      return $this->response;
    }

  }

}

<?php

namespace Drupal\global_search\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Component\Utility\Tags;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\product_details\Controller\ProductController;
use Drupal\Core\Cache\CacheBackendInterface;

/**
 * Defines a route controller for entity autocomplete form elements.
 */
class AutocompleteController extends ControllerBase {

  /**
   * To access pdp methods using services.
   *
   * @var pdpServices
   */
  protected $pdpServices;

  /**
   * Class constructor.
   */
  public function __construct(ProductController $pdp) {
    $this->pdpServices = $pdp;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
    // Load the service required to construct this class.
      $container->get('pdp')
    );
  }

  /**
   * Handler for autocomplete request.
   */
  public function handleAutocomplete(Request $request) {
    $results = [];
    $output = '';
    $response = new Response();
    $inputQs = $request->get('text');
    //find -140 from sku and remove it.
    $getLastFourChar = substr($inputQs, -4);
    if($getLastFourChar == '-140'){
      $inputQs = str_replace($getLastFourChar, '', $inputQs);
    }
    // Get the typed string from the URL, if it exists.
    $input = trim($inputQs);
    if ($input) {
      $typedString = Tags::explode($input);
      $typedString = trim(array_pop($typedString));
      if (strlen($typedString) > 2) {
        $result = self::productSearchAutocomplete($typedString);
        $totalRecords = 0;
        if (is_array($result) && (!empty($result['products']))) {
          $totalRecords = count($result['products']);
        }

        if ($totalRecords > 0) {
          $results[] = [
            'label' => 'PRODUCTS',
          ];
          foreach ($result['products'] as $product) {
            $results[] = [
              'value' => $product['description'],
              'label' => Link::fromTextAndUrl($product['productText'], Url::fromUserInput($product['url'])),
            ];
          }
          if(count($results)){
            $results = array_slice($results, 0,5);
          }
          $url = Url::fromRoute('global_search.search', ['title' => $typedString]);
          $viewAllLink['viewAll'] = $url;
          $link = [
            '#theme' => 'view_all_link',
            '#data' => (Array) $viewAllLink,
          ];
          $addViewAll = [
            'label' => render($link),
          ];
          $results[] = $addViewAll;
          // Build Output.
          $output = [
            '#theme' => 'search_suggestions_output',
            '#data' => $results,
          ];
        }
        $totalRecords = 0;
        if (!empty($result['parts'])) {
          $totalRecords = count($result['parts']);
        }
        if ($totalRecords > 0) {
          $resultsr[] = [
            'label' => 'REPAIR PARTS',
          ];
          foreach ($result['parts'] as $product) {
            $resultsr[] = [
              'value' => $product['description'],
              'label' => Link::fromTextAndUrl($product['productText'], Url::fromUserInput($product['url'])),
            ];
          }

          if(count($resultsr)){
            $resultsr = array_slice($resultsr, 0,5);
          }
          // Check if the file is uploaded, let combine file data in to array.
          if (is_array($resultsr) && count($resultsr)) {
            $results = array_merge($results, $resultsr);
          }


          // Add view all link.
          $repairProdUrl = Url::fromRoute('global_search.search', ['title' => $typedString]);
          $repairProdViewAllLink['viewAll'] = $repairProdUrl;
          $repairLink = [
            '#theme' => 'view_all_link',
            '#data' => (Array) $repairProdViewAllLink,
          ];
          $repairProdAddViewAll = [
            'label' => render($repairLink),
          ];
          $results[] = $repairProdAddViewAll;
          // Build Output.
          $output = [
            '#theme' => 'search_suggestions_output',
            '#data' => $results,
          ];
        }
      }
    }
    $response->setContent(render($output));
    $response->headers->set('Content-Type', 'text/html');
    return $response;
  }

  /**
   * Fetch the search result for auto complete.
   */
  public function productSearchAutocomplete($typedString) {
    $cidExpTime = 60;
    $cidExpTime = \Drupal::time()->getRequestTime() + ($cidExpTime);
    $config = $this->config('sync.api_settings');
    $configPrimary = $this->config('delta_services.deltaservicesconfig');
    $domain = $configPrimary->get('api_base_path');
    $output = [];
    $method = 'GET';
    $headers = add_headers(TRUE);

    // Search Products.
    $searchProductUrl = $domain . $config->get('sync.search_products');
    $searchProductUrl = str_replace("{typedString}", $typedString, $searchProductUrl);
    $response = web_services_api_call($searchProductUrl, $method, $headers, '', 'SearchProducts');
    if ($response['status'] == 'SUCCESS') {
      $data = json_decode($response['response']);
      if (count($data->content) > 0) {
        foreach ($data->content as $content) {
          $output['products'][$content->name]['name'] = $content->name;
          $output['products'][$content->name]['description'] = $content->descriptionWithCollection;
          $output['products'][$content->name]['url'] = ($this->pdpServices->getProductUrl($content->name) == '#') ? '/pdp/' . $content->name : $this->pdpServices->getProductUrl($content->name);
          //When user entered actual sku then redirect to PDP.
          if (strtolower($output['products'][$content->name]['name']) === strtolower($typedString)) {
            $pdpUrl = $output['products'][$content->name]['url'];
            $this->cache()->set('search_' . strtolower($typedString), $pdpUrl, $cidExpTime);
          }
          $content->description = preg_replace("#(" . $typedString . ")#i", "<b>$1</b>", $content->descriptionWithCollection);
          $link = [
            '#theme' => 'search_item_link',
            '#data' => (Array) $content,
          ];
          $output['products'][$content->name]['productText'] = render($link);
        }
      }
    }

    // Search Parts.
    $searchPartsUrl = $domain . $config->get('sync.search_parts');
    $searchPartsUrl = str_replace("{typedString}", $typedString, $searchPartsUrl);
    $response = web_services_api_call($searchPartsUrl, $method, $headers, '', 'SearchParts');
    if ($response['status'] == 'SUCCESS') {
      $data = json_decode($response['response']);
      if (count($data->content) > 0) {
        foreach ($data->content as $content) {
          $output['parts'][$content->name]['name'] = $content->name;
          $output['parts'][$content->name]['description'] = $content->descriptionWithCollection;
          $output['parts'][$content->name]['url'] = ($this->pdpServices->getProductUrl($content->name) == '#') ? '/pdp/' . $content->name : $this->pdpServices->getProductUrl($content->name);
          $content->description = preg_replace("#(" . $typedString . ")#i", "<b>$1</b>", $content->descriptionWithCollection);
          $link = [
            '#theme' => 'search_item_link',
            '#data' => (Array) $content,
          ];
          $output['parts'][$content->name]['productText'] = render($link);
        }
      }
    }
    return $output;
  }

}

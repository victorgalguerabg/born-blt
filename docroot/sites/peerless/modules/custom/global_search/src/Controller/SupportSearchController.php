<?php

namespace Drupal\global_search\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Implementation of support controller.
 */
class SupportSearchController extends ControllerBase {

  /**
   * Implementation of support results.
   */
  public function customerSupportBlock() {
    return [];
  }

}

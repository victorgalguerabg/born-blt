<?php

namespace Drupal\global_search\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Implementation of search controller.
 */
class SearchController extends ControllerBase {

  /**
   * Implementation of search results.
   */
  public function getSearchResults() {
    return [
      '#theme' => 'search_results_page',
      '#attached' => [
        'library' =>
        [
          'global_search/searchResult',
        ],
      ],
    ];
  }

  /**
   * Implementation of support search results.
   */
  public function getSupportSearchResults() {
    return [
      '#theme' => 'support_search_results_page',
      '#attached' => [
        'library' =>
          [
            'global_search/customerSupport',
          ],
      ],
    ];
  }

}

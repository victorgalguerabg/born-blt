<?php

namespace Drupal\global_search\Controller;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;

/**
 * Class ProfessionalSearchAjaxController.
 */
class ProfessionalSearchAjaxController extends ControllerBase {

   /**
   * Professional Search Ajax Controller.
   *
   * Generated Professional Search Ajax Controller redirection.
   */
  public function ajaxGlobalSearch() {
    $response = new AjaxResponse();
    $filterKey = $_POST['filter_type'];
    $searchPattern = $_POST['model_number'];
    $filtersArr = [
        'title' => $searchPattern,
        'documentLookFor' => $filterKey,
        'source' => 'prof',
    ];
    $url = Url::fromRoute('support_search.result', $filtersArr);
    $command = new RedirectCommand($url->toString());
    $response->addCommand($command);
    return $response;
  }
}

<?php

namespace Drupal\global_search\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Implementation of support search form.
 */
class SupportSearchForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'support_search_form';
  }

  /**
   * Implementation for support search orm build().
   */
  public function buildForm(array $form, FormStateInterface $form_state, $args = null) {
    $form = [];
    $form['name'] = [
      '#type' => 'textfield',
      '#autocomplete' => 'off',
      '#title' => $this->t('Support search field'),
      '#title_display' => 'invisible',
      '#attributes' => [
        'class' => [
          'form-control',
        ],
        'autocomplete' => 'off',
      ],
    ];

    $submitLabel = 'search';
    if ($args[0] == '/replacement-parts-search-results') {
      $submitLabel = 'submit';
    }

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $submitLabel,
      '#attributes' => [
        'class' => [
          'btn btn-primary btn-md btn-support-search',
        ],
      ],
    ];
    $form['product_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Select Your Product Type'),
      '#title_display' => 'invisible',
      '#title_attributes' => [
        'for' => [
          'edit-product-type',
        ],
      ],
      '#attributes' => [
        'class' => [
          'dropdown-list',
        ],
      ],
      '#options' => [
        '' => $this->t('Select Your Product Type'),
        '/kitchen/two-handle-faucets' => $this->t('Kitchen - Two-Handle'),
        '/kitchen/pull-out-faucets' => $this->t('Kitchen - Pull-Out'),
        '/kitchen/single-handle-faucets' => $this->t('Kitchen - Single-Handle'),
        '/kitchen/bar--laundry-faucets' => $this->t('Kitchen - Bar/Laundry'),
        '/bath/tub--shower-faucets' => $this->t('Bath - Tub/Shower'),
        '/bath/bathroom-faucets' => $this->t('Bath - Lavatory'),
        '/bath/roman-tub-faucets' => $this->t('Bath - Roman Tub'),
      ],
    ];

    $form['browse'] = [
      '#type' => 'submit',
      '#value' => 'browse',
      '#attributes' => [
        'class' => [
          'btn btn-primary btn-md btn-support-browse',
        ],
      ],
    ];
    if ($args[0] == '/replacement-parts-search-results') {
      $form['#theme'] = 'support_search_result_block';
    }
    else {
      if ($args[0] == '/customer-support/get-support--repair-parts-for-yo') {
        $form['#theme'] = 'get_support_search_block';
      }
      else {
        $form['#theme'] = 'support_search_block';
      }
    }
    $form['#attached']['library'][] = 'global_search/customerSupport';
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $input = $form_state->getUserInput();
    if ($input['op'] === 'browse' && (empty($input['product_type']) || $input['product_type'] == '')) {
      $form_state->setErrorByName('product_type', $this->t('Please select your browse type'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $input = $form_state->getUserInput();
    if ($input['op'] == 'search' || $input['op'] == 'submit') {
      $data = $form_state->getValue('name');
      $url = Url::fromRoute('support_search.result', ['title' => $data, 'source' => 'support']);
      $form_state->setRedirectUrl($url);
    }
    else {
      $url = $form_state->getValue('product_type');
      $responseObj = new RedirectResponse($url);
      $responseObj->send();
    }
  }

}

<?php

namespace Drupal\global_search\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Implementation of professionals search form.
 */
class ProfessionalsSearchForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'professionals_search_form';
  }

  /**
   * Implementation fof professionals search form build().
   */
  public function buildForm(array $form, FormStateInterface $form_state, $arg = NULL) {
    $form = [];
    $form['model_number'] = [
      '#type' => 'textfield',
      '#placeholder' => $this->t('model#'),
    ];
    $form['filter_type'] = [
      '#type' => 'radios',
      '#options' => [
        'productdetails' => 'Detailed Product Information',
        'ts' => 'Specification Sheets',
        'mi' => 'Maintenance & Installation Information',
        'pd' => 'Parts Information',
      ],
      '#default_value' => 'productdetails',
      '#value' => $this->t("I'm looking for:"),
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t("SEARCH"),
      '#attributes' => ['class' => ['btn btn-primary btn-md']],
    ];

    $form['#theme'] = 'professionals_search_block';
    $form['#attached']['library'][] = 'global_search/customerSupport';
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $input = $form_state->getUserInput();
    if ($input['op'] === 'SEARCH') {
      $searchPattern = $form_state->getValue('model_number');
      $filterKey = $input['filter_type'];
      $filtersArr = [
        'title' => $searchPattern,
        'documentLookFor' => $filterKey,
        'source' => 'prof',
      ];
      $url = Url::fromRoute('support_search.result', $filtersArr);
      $form_state->setRedirectUrl($url);
    }
  }

}

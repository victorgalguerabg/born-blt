<?php

namespace Drupal\global_search\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Core\Cache\CacheBackendInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\product_details\Controller\ProductController;


/**
 * Implementation of global search form.
 */
class GlobalSearchForm extends FormBase {

  /**
   * To access pdp methods using services.
   *
   * @var pdpServices
   */
  protected $pdpServices;

  /**
   * Class constructor.
   */
  public function __construct(ProductController $pdp) {
    $this->pdpServices = $pdp;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
    // Load the service required to construct this class.
      $container->get('pdp')
    );
  }


  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'global_search_form';
  }

  /**
   * Implementation fof global search form build().
   */
  public function buildForm(array $form, FormStateInterface $form_state, $arg = null) {
    $form = [];
    $form['name'] = [
      '#type' => 'search',
      '#placeholder' => $this->t('Search ...'),
      '#autocomplete' => 'off',
      '#attributes' => [
        'class' => [
          'form-control',
          'autocomplete',
        ],
        'autocomplete' => 'off',
      ],
    ];
    $form['advanced_expand'] = [
      '#type' => 'details',
      '#title' => $this->t('ADVANCED SEARCH'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#attributes' => [
        'class' => ['advanced-search-drop-down'],
      ],
    ];
    $form['advanced_expand']['filter_option'] = [
      '#type' => 'radios',
      '#title' => $this->t("I'm looking for:"),
      '#options' => [
        'productdetails' => 'Detailed Product Information',
        'ts' => 'Specification Sheets',
        'mi' => 'Maintenance & Installation Information',
        'pd' => 'Parts Information',
      ],
      '#default_value' => 'productdetails',
      '#value' => $this->t("I'm looking for:"),
      '#attributes' => [
        'class' => ['radio'],
      ],
    ];
    $form['advanced_expand']['search_box'] = [
      '#type' => 'textfield',
      '#placeholder' => $this->t('Search ...'),
    ];
    $form['advanced_expand']['adv_submit'] = [
      '#type' => 'submit',
      '#value' => $this->t("SEARCH"),
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => 'search',
    ];

    $form['#theme'] = 'search_block';
    $form['#attached']['library'][] = 'global_search/search';
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    global $base_url;
    $filter = $form_state->getUserInput();
    $filterKey = '';
    $filtersArr = [];
    $searchPattern = $filter['name'];
    //If you put model number, the page  automatically take you to the PDP page.
    $result = self::productSearch($searchPattern);
    // redirect to  pdp.
    if (!empty($result)) {
      $pdpUrl = $base_url . $result;
      $response = new RedirectResponse($pdpUrl);
      return $response->send();
    }
    $filtersArr = ['title' => $searchPattern];
    if ((!empty($filter['filter_option']) && (!empty($filter['search_box'])))) {
      $filterKey = $filter['filter_option'];
      $searchPattern = $filter['search_box'];
      $filtersArr = ['title' => $searchPattern, 'documentLookFor' => $filterKey];
    }
    if (empty($filtersArr) && empty($filter['search_box'])) {
      $form_state->setRedirect('<front>');
      return;
    }
    $url = Url::fromRoute('global_search.search', $filtersArr);
    $form_state->setRedirectUrl($url);
  }

  /**
   * If you put model number, the page  automatically take you to the PDP page.
   */
  public function productSearch($typedString) {
    $config = $this->config('sync.api_settings');
    $configPrimary = $this->config('delta_services.deltaservicesconfig');
    $domain = $configPrimary->get('api_base_path');
    $output = [];
    $method = 'GET';
    $headers = add_headers(TRUE);

    // Search Products.
    $searchProductUrl = $domain . $config->get('sync.search_products');
    $searchProductUrl = str_replace("{typedString}", $typedString, $searchProductUrl);
    $response = web_services_api_call($searchProductUrl, $method, $headers, '', 'SearchProducts');
    $pdpUrl = '';
    if ($response['status'] == 'SUCCESS') {
      $data = json_decode($response['response']);
      if (count($data->content) > 0) {
        foreach ($data->content as $content) {
          $output['products'][$content->name]['name'] = $content->name;
          $output['products'][$content->name]['description'] = $content->descriptionWithCollection;
          $output['products'][$content->name]['url'] = ($this->pdpServices->getProductUrl($content->name) == '#') ? '/pdp/' . $content->name : $this->pdpServices->getProductUrl($content->name);
          //When user entered actual sku then redirect to PDP.
          if (strtolower($output['products'][$content->name]['name']) === strtolower($typedString)) {
            $pdpUrl = $output['products'][$content->name]['url'];
          }
        }
      }
    }
    return $pdpUrl;
  }

}

var cacheAPICalls = 'T';
(function ($, Drupal, drupalSettings, Backbone) {

    Drupal.behaviors.plpBehaviour = {
        attach: function (context, settings) {
            $('.block-product-listing').each(function (index, element) {
                if ($(this).find('.custom-scroll ul').children('li').length == 0) {
                    $(this).find('.block-title-content').hide();
                } else {
                    $(this).find('.block-title-content').show();
                }
            });

            //Product Details display view
            var ProdView = Backbone.View.extend({
                el: "#output",
                template: _.template($("#output-node").html()),
                initialize: function (initData) {
                    this.render(initData);
                    this.postRender(initData);
                },
                postRender: function (initData) {
                      _.each(initData.facets, function (facet) {
                        if (facet.type == 'term' && facet.terms.length >= 1) {
                            _.each(facet.terms, function (facetEntry) {
                                var count = facetEntry.count;
                                var label = facetEntry.term;
                                var elemIdTrim = '.' + label.replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, '-');
                                $(elemIdTrim).removeClass('greyed-out-on').addClass('greyed-out-off').removeAttr("disabled");
                            });
                        }
                        if (facet.type == 'range') {
                            _.each(facet.ranges, function (facetRange) {
                                var count = facetRange.count
                                var facetValue = facetRange.from + ".00-" + facetRange.to + ".00";
                                if (count !== 0) {
                                  var elemIdTrim = '.' + facetValue.replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, '-');
                                  $(elemIdTrim).removeClass('greyed-out-on').addClass('greyed-out-off').removeAttr("disabled");
                                }
                            });
                        }
                    });
                },
                render: function (initData) {
                    var _self = this;
                    var _render = "";
                    if (initData.content != '') {
                        _.each(initData.content, function (entry) {
                            var _price = entry.values.ListPrice;
                            var _heroImage = entry.heroImageSmall;
                            var _SyndigoActive = '';
                            if (entry.values.MaterialStatus !== undefined && entry.values.MaterialStatus >= 50) {
                                var _SyndigoActive = 'discontinued';
                            }
                            var orderDate = (entry.values.AvailableToOrderDate !== '' || entry.values.AvailableToOrderDate !== undefined) ? DateTotimeStamp(entry.values.AvailableToOrderDate) : '';
                            var shipDate = (entry.values.AvailableToShipDate !== '' || entry.values.AvailableToShipDate !== undefined) ? DateTotimeStamp(entry.values.AvailableToShipDate) : '';
                            var currentDate = DateTotimeStamp(getCurrentDate());
                            if (orderDate === '' || shipDate === '') {
                                shipDate = currentDate - (60 * 60 * 24);
                            }
                            if (currentDate < shipDate) {
                                if (currentDate > orderDate) {
                                    var _SyndigoActive = 'coming soon';
                                }
                            }
                            var _title = "Title";
                            if (entry.description != '') {
                                _title = entry.description;
                            }
                            var _collection = "PEERLESS";
                            if (entry.values.defaultCollection != "") {
                                _collection = entry.values.defaultCollection;
                            }
                            var _sku = entry.name;
                            var urlAliasObject = drupalSettings.urlAliasObject;
                            var _url = urlAliasObject[_sku];
                            if (_url === '' || _url === undefined) {
                                _url = '/pdp/' + _sku;
                            }
                            _render += _self.template({
                                title: _title,
                                collectionName: _collection,
                                heroImage: _heroImage,
                                sku: _sku,
                                price: _price,
                                url: _url,
                                prodOutDated: _SyndigoActive,
                            });
                            _self.$el.html(_render);
                            $('.preloader').remove();
                            $('body').removeClass('loader-active');
                        });
                        productCompare();

                    } else {
                        var noResultsMessage = '<div role="contentinfo" aria-label="Error message"><h2 class="visually-hidden">Status message</h2>Oops!!! Nothing Found. Please refine your search</div>';
                        $('#output').html(noResultsMessage);
                        $('.preloader').remove();
                        $('body').removeClass('loader-active');
                    }
                    return this;
                }
            });

            var facetView = Backbone.View.extend({
                el: "#facets",
                template: _.template($("#facets-view").html()),
                initialize: function (initData) {
                    this.render = _.wrap(this.render, function (render) {
                        render(initData);
                        this.afterRender();
                    });
                    this.render(initData);
                },
                events: {
                    'click [type="checkbox"]': 'applyFacets',
                    'click .clear-all--link': 'applyFacets',

                },
                applyFacets: function (e) {

                },
                render: function (initData) {
                    _.each(initData.facets, function (facet) {
                        if (facet.type == 'term' && facet.terms.length >= 1) {
                            var blockId = facet.name.toLowerCase() + 'facetblock';
                            var name = "facet_" + facet.name;
                            var output = "";
                            var facetName = facet.name;
                            var facetLowercase = facetName.toLowerCase();
                            _.each(facet.terms, function (facetEntry) {
                                var facetLabels = drupalSettings.vocabularyFacetLabels;
                                var facetValue = facetEntry.term;
                                var count = facetEntry.count;
                                var label = facetEntry.term;
                                var actualFacetlabel = facetEntry.term;
                                var facetImg = "";
                                var position = 0;

                                if (facet.name == 'facetFinish' && typeof (facetLabels[facetLowercase][label]) != 'undefined') {
                                    if (facetLabels[facetLowercase][label]['imgUri'] != 'null') {
                                        facetImg = "<img src=" + facetLabels[facetLowercase][label]['imgUri'] + " alt= " + label + " title = " + label + " />";
                                    }
                                }

                                if (facet.name != 'categories' && typeof (facetLabels[facetLowercase][label]) != 'undefined' && label != undefined) {
                                    if (facetLabels[facetLowercase][label]['label'] != 'null') {
                                        actualFacetlabel = facetLabels[facetLowercase][label]['label'];
                                    }
                                    if (facetLabels[facetLowercase][label]['position']) {
                                        position = facetLabels[facetLowercase][label]['position'];
                                    }
                                    var elemIdTrim = facetValue.replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, '-');
                                    output += '<li class="facet-item" data-position ="'+position+'" ><input data-facet-attr="' + facet.name + '" id="' + facetValue + '" type="checkbox" class="facets-checkbox '+ elemIdTrim +'" value="' + facetValue + '"><label for="' + facetValue + '">' + facetImg + '<span class="facet-item__value"> ' + actualFacetlabel + '</span></label></li>';
                                }

                                if (facet.name == 'categories' && typeof (facetLabels['categories'][label]) != 'undefined') {
                                    facetLowercase = 'facetcategories';
                                    actualFacetlabel = facetLabels['categories'][label]['label'];
                                    if (facetLabels['categories'][label]['position']) {
                                        position = facetLabels['categories'][label]['position'];
                                    }
                                    var elemIdTrim = facetValue.replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, '-');
                                    output += '<li class="facet-item" data-position ="'+position+'" ><input data-facet-attr="' + facet.name + '" id="' + facetValue + '" type="checkbox" class="facets-checkbox '+ elemIdTrim +'" value="' + facetValue + '"><label for="' + facetValue + '">' + facetImg + '<span class="facet-item__value">' + actualFacetlabel + '</span></label></li>';
                                }


                            });

                            var blockId = facetName.toLowerCase() + 'facetblock';
                            $('#block-peerless-' + blockId + ' .block-title-content').show();
                            $('#' + facetLowercase).html(output);
                        }
                        var _facetName = facet.name;
                        if (_facetName === 'facetHoles') {
                            var _blockID = _facetName.toLowerCase() + 'facetblock';
                            var getBlockId = '#block-peerless-' + _blockID;
                            var position = 0;
                            if ($(getBlockId).length) {
                                var popupContentCMS = drupalSettings.holesInstallPopupHtml;
                                var popupContent = popupContentCMS.split("|");
                                var popupHtmlContent = '<div class="facet--tooltip"><a href="#" class="facetModalLink" data-toggle="modal" data-target="#myModal">What is installation type?</a></div>' + ' <!-- Modal -->\n' +
                                    '  <div class="modal fade" id="myModal" role="dialog">\n' +
                                    '    <div class="modal-dialog">\n' +
                                    '    \n' +
                                    '      <!-- Modal content-->\n' +
                                    '      <div class="modal-content">\n' +
                                    '        <div class="modal-header">\n' +
                                    '          <button type="button" class="close" data-dismiss="modal">&times;</button>\n' +
                                    '          <h4 class="modal-title facet--modal-title">' + popupContent[0] + '</h4>\n' +
                                    '        </div>\n' +
                                    '        <div class="modal-body">\n' +
                                    '          <div class="facet--modal-content"><p>' + popupContent[1] + '</p>\n' +
                                    '</div>' +
                                    '        </div>\n' +
                                    '      </div>\n' +
                                    '      \n' +
                                    '    </div>\n' +
                                    '  </div>\n' +
                                    '  ';
                                $(getBlockId + ' .block-title-content h2').once().after(popupHtmlContent);
                            }

                        }

                        if (facet.type == 'range') {
                            var output = "";
                            _.each(facet.ranges, function (facetRange) {
                                var facetLabels = drupalSettings.vocabularyFacetLabels;
                                var facetLabel = "$" + facetRange.from + ".00 - $" + facetRange.to + ".00";
                                var count = facetRange.count
                                var facetValue = facetRange.from + ".00-" + facetRange.to + ".00";
                                var termfacetValue = facetRange.from + "-" + facetRange.to;
                                var facetName = facet.name;
                                var facetLowercase = facetName.toLowerCase();
                                var position = 0;
                                if (count !== 0) {
                                    var elemIdTrim = facetValue.replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, '-');
                                    if (facetLabels[facetLowercase][termfacetValue]['position']) {
                                        position = facetLabels[facetLowercase][termfacetValue]['position'];
                                    }
                                    output += '<li class="facet-item" data-position ="'+position+'" ><input data-facet-attr="range_ListPrice" id="' + facetValue + '" type="checkbox" class="facets-checkbox '+ elemIdTrim +'" value="' + facetValue + '"><label for="' + facetValue + '"><span class="facet-item__value">' + facetLabel + '</span></label></li>';
                                }
                                var blockId = facetName.toLowerCase() + 'facetblock';
                                $('#block-peerless-' + blockId + ' .block-title-content').show();
                                $('#' + facetLowercase).html(output);
                            });
                        }

                        //Clean up unwanted facets.
                        if (facet.type == 'term') {
                            if (facet.terms.length > 0) {
                                var facetName = facet.name;
                                var blockId = facetName.toLowerCase() + 'facetblock';
                                $('#block-peerless-' + blockId + ' .block-title-content').show();
                                var facetLowercase = facetName.toLowerCase();
                                $('#' + facetLowercase).show();
                            } else {
                                var facetName = facet.name;
                                var blockId = facetName.toLowerCase() + 'facetblock';
                                $('#block-peerless-' + blockId + ' .block-title-content').hide();
                                var facetLowercase = facetName.toLowerCase();
                                $('#' + facetLowercase).hide();
                            }
                        }
                    });
                    jQuery(".plp-facets-list").each(function(){
                        jQuery(this).html(jQuery(this).children('.facet-item').sort(function(a, b){
                            return (jQuery(b).data('position')) < (jQuery(a).data('position')) ? 1 : -1;
                        }))
                    });
                    //Todo After rendered the output.
                    $('.goBack').click(function () {
                        window.history.back();
                    });
                },
                afterRender: function () {
                    //This will cleanup the facet blocks.
                    $("ul.plp-facets-list").each(function (index) {
                        var getRunningId = $(this).attr('id');
                        var getLiExist = $("#" + getRunningId).has('li').length;
                        if (getLiExist === 0) {
                            var dynamicFacetId = 'block-peerless-' + getRunningId + 'facetblock';
                            $("#" + dynamicFacetId).hide();
                        }
                    });

                    var urlParams = window.location.search.replace("?", "");
                    if (urlParams !== '' && urlParams !== null) {
                        urlParams = decodeURI(urlParams);
                        getParamsParts = urlParams.split("&");
                        if (getParamsParts.length > 0) {
                            $.each(getParamsParts, function (index, value) {
                                keyValArr = value.split("=");
                                if (keyValArr.length > 1) {
                                    var facetParamsKey = keyValArr[0];
                                    var facetParamsVal = keyValArr[1];
                                    if (facetParamsKey === 'includeObsolete') {
                                        $("#includeObsolete").prop('checked', true);
                                    }
                                    if (facetParamsKey === 'and_availableToTrade') {
                                        $("#availableToTrade").prop('checked', true);
                                    }
                                    $('.facets-checkbox').each(function (index, value) {
                                        var elementUniqId = $(this).attr('id');
                                        if (facetParamsVal === elementUniqId) {
                                            $(this).prop('checked', true);
                                        }
                                    });
                                }
                            });
                        }
                    }
                },
            });


            var paginationView = Backbone.View.extend({
                el: ".pagination-output",
                template: _.template($("#pagination").html()),
                initialize: function (initData) {
                    this.render(initData);
                },
                events: {
                    'click [type="button"]': 'pagination'
                },
                pagination: function (e) {
                    var params = [];
                    params['pageNum'] = $(e.currentTarget).val();
                    var facets = "";
                    $('.region-sidebar-first input[type=checkbox]:checked').each(function () {
                        cacheAPICalls = 'F';
                        if ($(this).attr('data-facet-attr') != 'range_ListPrice') {
                            facets += 'and_' + $(this).attr('data-facet-attr') + '=' + $(this).val() + "&";
                        }
                        if ($(this).attr('data-facet-attr') == 'range_ListPrice') {
                            facets += $(this).attr('data-facet-attr') + '=' + $(this).val() + "&";
                        } else if ($(this).attr('data-facet-attr') == 'includeObsolete') {
                            facets += $(this).attr('data-facet-attr') + '=' + $(this).val() + "&";
                        }
                    });
                    if($('.region-sidebar-first input[type=checkbox]:checked').length == 0){
                        cacheAPICalls = 'T';
                    }
                    facets = facets.replace("and_includeObsolete=true&", "");
                    params['facets'] = facets;
                    applyPLPFacets(params, 'pager', cacheAPICalls);
                },
                render: function (initData) {
                    var _self = this;
                    var _render = "";
                    //stopLoop value
                    var _nextPage = parseInt(initData.number) + 1;
                    var _previousPage = parseInt(initData.number) - 1;
                    _render += _self.template({
                        totalPager: initData.totalPages,
                        currentPage: _nextPage,
                        nextPage: _nextPage,
                        previousPage: _previousPage,
                        totalRecordset: initData.totalElements
                    });
                    _self.$el.html(_render);
                }
            });

            $(document).on('change', '.facets-checkbox', function () {
                var facets = "";
                var params = [];
                $('.region-sidebar-first input[type=checkbox]:checked').each(function () {
                    cacheAPICalls = 'F';
                    if ($(this).attr('data-facet-attr') != 'range_ListPrice' && $(this).attr('data-facet-attr') != 'includeObsolete') {
                        facets += 'and_' + $(this).attr('data-facet-attr') + '=' + $(this).val() + "&";
                    }
                    if ($(this).attr('data-facet-attr') == 'range_ListPrice') {
                        facets += $(this).attr('data-facet-attr') + '=' + $(this).val() + "&";
                    } else if ($(this).attr('data-facet-attr') === 'includeObsolete') {
                        facets += $(this).attr('data-facet-attr') + '=' + $(this).val() + "&";
                    }

                });
                facets = facets.replace("and_includeObsolete=true&", "");
                params['facets'] = facets;
                applyPLPFacets(params, 'facets', cacheAPICalls);
            });
            // alert( decode window.location.href.split('?')[0]);
            if (context == document) {
                var selectedfacetCat = drupalSettings.selectedFacets;
                var getURLPath = decodeURIComponent(window.location.href);
                var chkQSExist = getURLPath.split('?');
                if (chkQSExist.length > 1) {
                    cacheAPICalls = 'F';
                }
                var facetCat = generateFacetsAPI(selectedfacetCat);
                var size = "16";
                $.ajax({
                    type: "GET",
                    url: "/product-listing",
                    dataType: 'json',
                    data: {
                        categories: drupalSettings.selectedCategories,
                        facets: facetCat,
                        size: size,
                        cache: cacheAPICalls,
                        getUrlPathOnly: getURLPath
                    },
                    beforeSend: function (){
                      $('body').addClass('loader-active');
                      if ($('.preloader').length === 0) {
                          $('.plp--list').prepend('<div class="preloader"><i class="input-loader"></i></div>');
                      }
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        showErrorMessage(XMLHttpRequest.status, '.plp--list');
                    },
                    complete: function () {
                        $('body').removeClass('loader-active');
                        $('.preloader').remove();
                    },
                    success: function (data) {
                        //On page load
                        var prodView = new ProdView(data);
                        var FacetView = new facetView(data);
                        FacetView.undelegateEvents();
                        var PaginationView = new paginationView(data);

                    }
                });
            }

            //renderVia - facets, pager click
            function applyPLPFacets(params, renderVia, cacheAPICalls) {
                //Lets grey out facet filters
                if($("input.facets-checkbox").hasClass('greyed-out')){
                  $("input.facets-checkbox").removeClass('greyed-out').addClass('greyed-out-on').attr("disabled", true);
                }else{
                   $("input.facets-checkbox").addClass('greyed-out-on').attr("disabled", true);
                }
                //Dont greyout narrow result
                $("input#includeObsolete, input#availableToTrade").removeClass('greyed-out').removeClass('greyed-out-on').removeAttr("disabled");
                $('body').addClass('loader-active');
                if (!$('.preloader').length) {
                    $('#plp-wrapper').prepend('<div class="preloader"><i class="input-loader"></i></div>');
                }
                $('html, body').stop().animate({
                    scrollTop: $('#plp-wrapper').offset().top
                }, 'slow');
                //Let append query string on URL.
                var queryParams = params['facets'];
                var urlParts = queryParams;
                var mapObj = {
                   and_facetFinish:"or_facetFinish",
                   and_collections:"or_collections",
                   and_facetHoles:"or_facetHoles",
                   and_facetFeature:"or_facetFeature",
                  };
                var re = new RegExp(Object.keys(mapObj).join("|"),"gi");
                urlParts = urlParts.replace(re, function(matched){
                  return mapObj[matched];
                });
                var newurl = window.location.protocol + "//" + window.location.host + window.location.pathname + '?' + urlParts.slice(0, -1);
                window.history.pushState({path: newurl}, '', newurl);
                var getURLPath = decodeURIComponent(window.location.href);

                $.ajax({
                    type: "GET",
                    url: "/product-listing",
                    dataType: 'json',
                    data: {
                        categories: drupalSettings.selectedCategories,
                        facets: drupalSettings.selectedFacets,
                        params: params['facets'],
                        size: "16",
                        page: params['pageNum'],
                        cache: cacheAPICalls,
                        getUrlPathOnly: getURLPath
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        showErrorMessage(XMLHttpRequest.status, '.plp--list');
                    },
                    complete: function () {
                        $('body').removeClass('loader-active');
                        $('.preloader').remove();

                        if ($('.sidebar.active').length) {
                            $('.sidebar').removeClass('active');
                        }
                        if ($('.sidebar--container.show').length) {
                            $('.sidebar--container').removeClass('show');
                        }
                        if ($('.sidebar--mobile-header .glyphicon').length) {
                            $('.sidebar--mobile-header .glyphicon').removeClass('open');
                        }
                    },
                    success: function (data) {
                        //Facet filter - Trigger.
                        var prodView = new ProdView(data);
                        var PaginationView = new paginationView(data);
                        PaginationView.undelegateEvents();
                        $('body').removeClass('loader-active');

                    }
                });
            }


            /**
             * Description: Get query params and run plp page.
             * @param facetCat
             * @returns {*}
             */

            function generateFacetsAPI(facetCat) {
                var urlParams = window.location.search.replace("?", "");
                var facetCatNew = '';
                if (urlParams !== '' && urlParams !== null) {
                    urlParams = decodeURI(urlParams);
                    getParamsParts = urlParams.split("&");
                    if (getParamsParts.length > 0) {
                        $.each(getParamsParts, function (index, value) {
                            keyValArr = value.split("=");
                            if (keyValArr.length > 1) {
                                var facetParamsKey = keyValArr[0];
                                var facetParamsVal = keyValArr[1];
                                if (facetCat === "" || facetCat === undefined) {
                                    facetCatNew = drupalSettings.selectedFacets + 'and_' + facetParamsKey + '=' + facetParamsVal + "&";
                                } else {
                                    facetCatNew += '&' + facetParamsKey + '=' + facetParamsVal + '&';
                                }

                            }
                        });
                        facetCat = facetCat + facetCatNew;
                    }
                }

                return facetCat;
            }


                            $(".clear-all--link").click(function () {
                                cacheAPICalls = 'F';
                                // This is will remove the query params from URL
                                var uri = window.location.toString();
                                if (uri.indexOf("?") > 0) {
                                    var clean_uri = uri.substring(0, uri.indexOf("?"));
                                    window.history.replaceState({}, document.title, clean_uri);
                                }
                                // This is will remove the query params from URL
                                $("#includeObsolete").prop("checked", false);
                                $("#availableToTrade").prop("checked", false);
                                var getURLPath = decodeURIComponent(window.location.href);
                                var size = "16";
                                $.ajax({
                                    type: "GET",
                                    url: "/product-listing",
                                    dataType: 'json',
                                    data: {
                                        categories:  drupalSettings.selectedCategories,
                                        facets: drupalSettings.selectedFacets,
                                        size: size,
                                        cache: cacheAPICalls,
                                        getUrlPathOnly: getURLPath,
                                    },
                                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                                        showErrorMessage(XMLHttpRequest.status, '.plp--list');
                                    },
                                    complete: function () {
                                        $('body').removeClass('loader-active');
                                        $('.preloader').remove();
                                    },
                                    beforeSend: function () {
                                        $('body').addClass('loader-active');
                                        if ($('.preloader').length === 0) {
                                            $('.plp--list').prepend('<div class="preloader"><i class="input-loader"></i></div>');
                                        }
                                    },
                                    success: function (data) {
                                        //Will be trigger at clear all
                                        var prodView = new ProdView(data);
                                        prodView.undelegateEvents();
                                        var FacetView = new facetView(data);
                                        FacetView.undelegateEvents();
                                        var PaginationView = new paginationView(data);
                                        PaginationView.undelegateEvents();

                                    }
                                });
                            });


            function getParams(k) {
                var p = {};
                location.search.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (s, k, v) {
                    p[k] = v
                })
                var response = k ? p[k] : p;
                var decodedResponse = decodeURIComponent(response);
                var sanitizedResponse = removeHTMLTags(decodedResponse);
                return sanitizedResponse;
            }

            /*
               Function to remove HTML tags.
          */

            function removeHTMLTags(decodedResponse) {
                paramText = decodedResponse;
                paramText = paramText.replace(/<script.*>[\w\W]{1,}(.*?)[\w\W]{1,}<\/script>/gi, "");
                paramText = paramText.replace(/%3Cscript.*%3E[\w\W]{1,}(.*?)[\w\W]{1,}%3C\/script%3E/gi, "");
                paramText = paramText.replace(/<style.*>[\w\W]{1,}(.*?)[\w\W]{1,}<\/style>/gi, "");
                paramText = paramText.replace(/&nbsp;/gi, " ");
                paramText = paramText.replace(/&amp;/gi, "&");
                paramText = paramText.replace(/&quot;/gi, '"');
                paramText = paramText.replace(/&lt;/gi, '<');
                paramText = paramText.replace(/&gt;/gi, '>');
                paramText = paramText.replace(/svg\/onload%3D/gi, '');
                paramText = paramText.replace(/--%21/gi, '');
                paramText = paramText.replace(/svg\/onload%3D/gi, '');
                paramText = paramText.replace(/--%21/gi, '');
                paramText = paramText.replace(/%28confirm%29/gi, '')
                paramText = paramText.replace(/%3C/gi, '');
                paramText = paramText.replace(/%3E/gi, '');
                paramText = paramText.replace(/<(?:.|\s)*?>/g, "");
                return paramText;

            }

            function DateTotimeStamp(dates) {
                if (dates !== undefined) {
                    var dates1 = dates.split("-");
                    var newDate = dates1[0] + "/" + dates1[1] + "/" + dates1[2];
                    return new Date(newDate).getTime();
                }
            }

            function getCurrentDate() {
                var currentTs = drupalSettings.currentTimeStamp;
                return currentTs;
            }

            // Product Compare
            var productCompare = function () {
                var compContainer = $('.compare-button--container');
                var nextPage = 0;
                var PreviousPageFilters = '';
                var checkedList = [];
                var selectedValues = [];
                selectedValues = $('[name=selectedCheckboxes]').val();
                if (selectedValues !== undefined && selectedValues.length > 0) {
                    checkedList = JSON.parse($('[name=selectedCheckboxes]').val());
                    $.each(checkedList, function (key, value) {
                        $('#' + value).prop('checked', true);
                    });
                }
                $('.add-to-compare .checkbox').click(function () {

                    var buttonLink = $("a.btn-compare").attr('href');

                    $.each($("input[name='modelNumber']:checked"), function () {
                        var idx = $.inArray($(this).val(), checkedList);
                        if (idx == -1) {
                            checkedList.push($(this).val());
                        } else {
                            checkedList.splice(idx, 1);
                        }
                        $('[name=selectedCheckboxes]').val(JSON.stringify(checkedList));
                    });

                    if (($('.add-to-compare .checkbox input').is(':checked') && ($('.add-to-compare .checkbox input[type="checkbox"]:checked').length >= 2)) || (buttonLink != '/compare')) {

                        compContainer.show();
                        var generateQueryString = '';
                        $.each(checkedList, function (key, value) {
                            generateQueryString += '&modelNumber=' + value;
                        });
                        var getUrlParts = location.pathname.slice(drupalSettings.path.baseUrl.length);
                        var getUrlsQs = getUrlParts.replace('/', '_');
                        $("a.btn-compare").attr('href', '/compare?category=Peerless_' + getUrlsQs + generateQueryString);
                        if (checkedList.length < 2) {
                            compContainer.hide();
                        }

                    } else {
                        compContainer.hide();
                    }
                });
            };

            var showErrorMessage = function (status, wrapper) {
                switch (status) {
                    case 400:
                        $(wrapper).html("<div class='sorry-msg'>Server understood the request, but request content was invalid., Please try with valid information.</div>");
                        break;
                    case 401:
                        $(wrapper).html("<div class='sorry-msg'>Unauthorized access.</div>");
                        break;
                    case 403:
                        $(wrapper).html("<div class='sorry-msg'>We are unable to process your request at this time, Please try again later.</div>");
                        break;
                    case 500:
                        $(wrapper).html("<div class='sorry-msg'>We are into the system upgrade process, Please try again later.</div>");
                        break;
                    case 401:
                        $(wrapper).html("<div class='sorry-msg'>Service unavailable. Please try again later.</div>");
                        break;
                    default:
                        $(wrapper).html("<div class='sorry-msg'>Oops something went wrong, Please try again later.</div>");
                }
            }
        }

    };
})(jQuery, Drupal, drupalSettings, Backbone);

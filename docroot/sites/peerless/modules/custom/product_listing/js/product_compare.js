(function ($, Drupal, drupalSettings, Backbone) {

    Drupal.behaviors.plpBehaviour = {
        attach: function (context, settings) {

            //Display product category string as header on the compare page.
            var _param = getSearchParams('category');
            if (_param !== '') {
                var getParts = _param.split('_');
                $('h1.product__compare__heading').text("Compare " + getParts[1] + " Products");
            }


            //add duplicate class on the compare table.
            if ($(".product__compare__table").length > 0) {
                $(".product__compare__table .entireRow").each(function () {
                    if ($(this).children().children().children().hasClass('dp')) {
                        $(this).addClass('duplicate');
                    }
                    if ($(this).children().children().children().hasClass('ndp')) {
                        $(this).addClass('no-duplicate');
                    }
                });
            }

            function getSearchParams(k) {
                var p = {};
                location.search.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (s, k, v) {
                    p[k] = v
                })
                var response = k ? p[k] : p;
                var decodedResponse = decodeURIComponent(response);
                var sanitizedResponse = removeHTMLTags(decodedResponse);
                return sanitizedResponse;
            }

            /*
              Function to remove HTML tags.
         */

            function removeHTMLTags(decodedResponse) {
                paramText = decodedResponse;
                paramText = paramText.replace(/<script.*>[\w\W]{1,}(.*?)[\w\W]{1,}<\/script>/gi, "");
                paramText = paramText.replace(/%3Cscript.*%3E[\w\W]{1,}(.*?)[\w\W]{1,}%3C\/script%3E/gi, "");
                paramText = paramText.replace(/<style.*>[\w\W]{1,}(.*?)[\w\W]{1,}<\/style>/gi, "");
                paramText = paramText.replace(/&nbsp;/gi, " ");
                paramText = paramText.replace(/&amp;/gi, "&");
                paramText = paramText.replace(/&quot;/gi, '"');
                paramText = paramText.replace(/&lt;/gi, '<');
                paramText = paramText.replace(/&gt;/gi, '>');
                paramText = paramText.replace(/svg\/onload%3D/gi, '');
                paramText = paramText.replace(/--%21/gi, '');
                paramText = paramText.replace(/svg\/onload%3D/gi, '');
                paramText = paramText.replace(/--%21/gi, '');
                paramText = paramText.replace(/%28confirm%29/gi, '')
                paramText = paramText.replace(/%3C/gi, '');
                paramText = paramText.replace(/%3E/gi, '');
                paramText = paramText.replace(/<(?:.|\s)*?>/g, "");
                return paramText;

            }
        }

    };
})(jQuery, Drupal, drupalSettings, Backbone);

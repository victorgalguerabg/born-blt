<?php

namespace Drupal\product_listing\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * Implementation of alterRoutes().
   */
  protected function alterRoutes(RouteCollection $collection) {
    // This will find the taxonomy term url and control the route.
    $route = $collection->get('entity.taxonomy_term.canonical');
    if ($route) {
      $route->setDefault('_controller', '\Drupal\product_listing\Controller\RouteController::controlRoute');
    }
  }

}

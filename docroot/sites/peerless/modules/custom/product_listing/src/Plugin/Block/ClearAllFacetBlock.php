<?php

namespace Drupal\product_listing\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides my custom block.
 *
 * @Block(
 *   id = "clear_all_block",
 *   admin_label = @Translation("Clear All Filters PLP"),
 *   category = @Translation("Blocks")
 * )
 */
class ClearAllFacetBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {

    return [
      '#theme' => 'clear_all_facet',
    ];
  }

}

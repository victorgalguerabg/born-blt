<?php

namespace Drupal\product_listing\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides my custom block.
 *
 * @Block(
 *   id = "custom_narrow_results_block",
 *   admin_label = @Translation("Narrow Results Facet Block"),
 *   category = @Translation("Blocks")
 * )
 */
class NarrowResultsFacetBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {

    return [
      '#theme' => 'narrow_results_facet',
    ];
  }

}

<?php

namespace Drupal\product_listing\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides my custom block.
 *
 * @Block(
 *   id = "product_listing_block",
 *   admin_label = @Translation("Product Listing Block"),
 *   category = @Translation("Blocks")
 * )
 */
class PLPBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {

    return [
      '#theme' => 'product_listing_template',
    ];
  }

}

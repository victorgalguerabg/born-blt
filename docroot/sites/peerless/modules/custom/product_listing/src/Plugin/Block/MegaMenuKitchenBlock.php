<?php

namespace Drupal\product_listing\Plugin\Block;

use Drupal\Core\Menu\MenuTreeParameters;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Menu\MenuLinkTreeInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Provides a block.
 *
 * @Block(
 *   id = "mega_menu_kitchen_block",
 *   admin_label = @Translation("Mega Menu Kitchen Block"),
 * )
 */
class MegaMenuKitchenBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The menu link tree service.
   *
   * @var \Drupal\Core\Menu\MenuLinkTreeInterface
   */
  protected $menuTree;

  /**
   * The LanguageManagerInterface.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * Constructs a new SystemMenuBlock.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Menu\MenuLinkTreeInterface $menu_tree
   *   The menu tree service.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entityTypeManager service.
   */
  public function __construct(array $configuration, $plugin_id, array $plugin_definition, MenuLinkTreeInterface $menu_tree, LanguageManagerInterface $language_manager, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->menuTree = $menu_tree;
    $this->languageManager = $language_manager;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('menu.link_tree'),
      $container->get('language_manager'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * Implementation of build().
   */
  public function build() {
    $menu_parameters = new MenuTreeParameters();
    // If you need to set depth of the tree.
    $menu_parameters->setMaxDepth(2);
    // If you need only enabled links.
    $menu_parameters->onlyEnabledLinks();
    $menus = $this->menuTree->load('main', $menu_parameters);
    $menuItem = [];

    foreach ($menus as $key => $value) {
      $menuUUIDArray = explode(':', $key);
      $uuid = $menuUUIDArray[1];
      // If its a multi lingual site.
      $languageCode = $this->languageManager->getCurrentLanguage()->getId();
      $menu_content = current($this->entityTypeManager->getStorage('menu_link_content')->loadByProperties(['uuid' => $uuid]));
      if ($menu_content->hasTranslation($languageCode)) {
        $menu_content = $menu_content->getTranslation($languageCode);
      }
      if ($value->hasChildren && $menu_content->get('title')->value == 'Kitchen') {
        $subTreeArray = $value->subtree;
        foreach ($subTreeArray as $key => $child) {
          $iconUri = $topMenu = "";

          $nodeId = $child->link->getUrlObject()->getRouteParameters()['node'];
          if (!empty($nodeId)) {
            $nodeObject = $this->entityTypeManager->getStorage('node')->load($nodeId);
            if ($nodeObject->hasField('field_display_image_on_menu')) {
              $displayImage = $nodeObject->get('field_display_image_on_menu')
                ->getValue();
              if (isset($displayImage[0]['value']) && $displayImage[0]['value']) {
                if ($nodeObject->hasField('field_mega_menu_image')) {
                  $mediaID = $nodeObject->get('field_mega_menu_image')
                    ->getValue()[0]['target_id'];

                  $mediaLoad = $this->entityTypeManager->getStorage('media')->load($mediaID)->toArray();
                  $iconLoad = $this->entityTypeManager->getStorage('file')->load($mediaLoad['image'][0]['target_id']);
                  $iconArray = $iconLoad->toArray();
                  $iconUri = $iconArray['url'][0]['value'];
                  $topMenu = "TRUE";
                }
              }
            }
          }

          $menuItem[] = [
            'title' => $child->link->getTitle(),
            'url' => $child->link->getUrlObject()->toString(),
            'imgUri' => $iconUri,
            'topMenu' => $topMenu,
          ];

        }
      }
    }
    $build = [
      '#theme' => 'mega_menu',
      '#items' => $menuItem,
    ];

    return $build;

  }

}

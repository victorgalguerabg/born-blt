<?php

namespace Drupal\product_listing\Http;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Class CompareCall.
 *
 * @package Drupal\product_listing\Http
 */
class CompareCall {

  use StringTranslationTrait;

  /**
   * Implementation of performRequest().
   */
  public static function performRequest($siteUrl = NULL, $params = NULL) {
    $websphere_config = \Drupal::config('sync.api_settings');
    $url = $websphere_config->get('sync.compare_api');
    $siteUrl = $url . $params;
    $client = new Client();
    try {
      $request = $client->get($siteUrl);
      $response = json_decode($request->getBody());
      return($response);
    }
    catch (RequestException $e) {
      return($e);
    }
  }

}

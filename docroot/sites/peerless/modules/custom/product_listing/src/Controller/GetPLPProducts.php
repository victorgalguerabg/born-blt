<?php

namespace Drupal\product_listing\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Cache\CacheBackendInterface;

/**
 * Class GetPLPProducts.
 *
 * @package Drupal\product_listing\Controller
 */
class GetPLPProducts extends ControllerBase {
  /**
   * DB connection resource.
   *
   * @var connection
   */
  protected $connection;

  /**
   * To get current query string.
   *
   * @var Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * To get current URL.
   *
   * @var getCurUrl
   */
  public $getCurUrl;

  /**
   * To get path alias.
   *
   * @var service
   */
  public $service;


  /**
   * The default cache bin.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * Implementation of constructor.
   */
  public function __construct(RequestStack $requestStack, Connection $connection, ContainerInterface $service, CacheBackendInterface $cache) {
    $this->requestStack = $requestStack;
    $this->connection = $connection;
    $this->service = $service;
    $this->cache = $cache;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
    // Load the service required to construct this class.
      $container->get('request_stack'),
      $container->get('database'),
      $container->get('service_container'),
      $container->get('cache.default')
    );
  }

  /**
   * Fetch the product url by using product sku.
   */
  public function getProductUrl($sku) {
    $database = $this->connection->select('node__field_product_sku', 'ns');
    $result = $database
      ->fields('ns', ['entity_id'])
      ->condition('field_product_sku_value', $sku)
      ->condition('bundle', 'commerce_product')
      ->execute()
      ->fetchField();
    if (!empty($result)) {
      return $this->service->get('path_alias.manager')->getAliasByPath('/node/' . $result);
    }
    else {
      return "#";
    }
  }

  public function plpProducts() {
    $response = new Response();
    $cacheAPI = $this->requestStack->getCurrentRequest()->get('cache');
    $page = 0;
    if (!empty($this->requestStack->getCurrentRequest()->get('page'))) {
      $page = $this->requestStack->getCurrentRequest()->get('page');
    }
    $categoryParams = $this->requestStack->getCurrentRequest()->get('categories');
    $config = $this->config('sync.api_settings');
    $configPrimary = $this->config('delta_services.deltaservicesconfig');
    $domain = $configPrimary->get('api_base_path');
    $plpUrl = $domain . $config->get('sync.plp');
    $cidExpTime = $config->get('sync.cache_exp_time');
    $cidExpTime = \Drupal::time()->getRequestTime() + ($cidExpTime);
    if (empty($cidExpTime)) {
      $cidExpTime = \Drupal::time()->getRequestTime() + (86400);
    }
    $setCatNameCleaned = '';
    $getUrlPathOnly = parse_url($this->requestStack->getCurrentRequest()->get('getUrlPathOnly'));
    $urlQueryParts = $getUrlPathOnly;
    $categoryParamsReplced = str_replace(['\'', '"', ',', ';', '<', '>', '-', '&', '$', '%','*','/'], '_', $getUrlPathOnly['path']);
    $cid = 'plp_' .$categoryParamsReplced . '_' . $page;
    if ($cacheAPI == 'T') {
      $cache = $this->cache->get($cid);
      if ($cache) {
        $response->setContent($cache->data);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
      }
    }
    $facetParams = $this->requestStack->getCurrentRequest()->get('facets');
    $params = $this->requestStack->getCurrentRequest()->get('params');
    $size = $this->requestStack->getCurrentRequest()->get('size');
    if (empty($size)) {
      $size = $config->get('sync.plp_items_per_page');
    }
    $page = 0;
    if (!empty($this->requestStack->getCurrentRequest()->get('page'))) {
      $page = $this->requestStack->getCurrentRequest()->get('page');
    }
    $url = $plpUrl . "&page=" . $page . "&size=" . $size . "&" . $categoryParams . $facetParams . $params;
    $facetsAndtoOrList = [
              "and_facetFinish" => "or_facetFinish",
              "and_collections" => "or_collections",
              "and_facetHoles" => "or_facetHoles",
              "and_facetFeature" => "or_facetFeature",
            ];
    $url = strtr($url,$facetsAndtoOrList);
    $request = web_services_api_call($url, "GET", [], "", "PLP");
    if ($request['status'] == 'SUCCESS') {
      $response->setContent($request['response']);
      $jsonDecoded = json_decode($request['response']);
      $resultCount = count($jsonDecoded->content);
      $response->headers->set('Content-Type', 'application/json');
      if ($cacheAPI == 'T' && $resultCount > 0) {
        $this->cache->set($cid, $request['response'], $cidExpTime);
      }
      return $response;
    }
    return [];
  }

  /**
   * Implementation of compare products.
   */
  public function compareProducts() {
    $config = $this->config('sync.api_settings');
    $configPrimary = $this->config('delta_services.deltaservicesconfig');
    $domain = $configPrimary->get('api_base_path');
    $compareUrl = $domain . $config->get('sync.compare');
    $technical_difficulties = $config->get('sync.technical_difficulties');
    $splitUrlPartsArr = $this->requestStack->getCurrentRequest()->getRequestUri();
    $urlParts = explode("?", $splitUrlPartsArr);
    if (count($urlParts) < 2) {
      $this->messenger()->addMessage($technical_difficulties , 'error');
      return [];
    }
    $readModelNos = explode("&", $urlParts[1]);
    if(count($readModelNos) < 2){
      $this->messenger()->addMessage($technical_difficulties , 'error');
      return [];
    }
    if (count($readModelNos) > 0) {
      $getCatName = explode("_", $readModelNos[0]);
      $catName = $getCatName[1];
      unset($readModelNos[0]);
    }
    $numCompResp = count($readModelNos);
    $modelNumberList = implode("&", $readModelNos);
    $url = str_replace("{modelNumber}", $modelNumberList, $compareUrl);
    $request = web_services_api_call($url, "GET", [], "", "ProductCompare");
    $productDetails = [];
    $compareLabValArr = [];

    if ($request['status'] == 'ERROR') {
      $this->messenger()->addMessage($technical_difficulties , 'error');
      return [];
    }
    if ($request['status'] == 'SUCCESS' && $request['response'] == 'null') {
      $this->messenger()->addMessage($technical_difficulties, 'error');
      return [];
    }
    if ($request['status'] == 'SUCCESS' && $request['response'] != 'null') {
      $readJsonArr = json_decode($request['response'], TRUE);
      if (count($readJsonArr)) {
        foreach ($readJsonArr['products'] as $productAttributes) {
          $productDetails[$productAttributes['name']] = [
            'sku' => [
              'sku' => $productAttributes['name'],
            ],
            'image' => [
              'path' => $productAttributes['heroImageSmall'],
            ],
            'name' => [
              'name' => $productAttributes['name'],
            ],
            'description' => [
              'desc' => $productAttributes['description'],
            ],
            'category' => [
              'path' => $catName,
            ],
            'price' => [
              'label' => 'List Price as Shown',
              'price' => $productAttributes['values']['ListPrice'],

            ],
          ];
        }
      }

      $compareLabValArr['finishOptions'][] = 'Available Finishes';
      $compareLabValArr['ListPrice'][] = 'List Price as Shown';
      $compareLabValArr['Finish'][] = 'Finish Shown';
      $compareLabValArr['Handle'][] = 'Handle Type';
      $compareLabValArr['CalgreenCert'][] = 'Water Efficient';
      $compareLabValArr['FltTouchClean'][] = 'Touch Clean';
      $compareLabValArr['FltSprayKitchen'][] = 'With Side Sprayer';
      $compareLabValArr['FltSoapDispenser'][] = 'With Soap/Lotion Dispenser';
      $compareLabValArr['ADA'][] = 'ADA Compliant';
      $compareLabValArr['FltLeadFree'][] = 'CA/VT Compliant';
      $compareLabValArr['WatersenseCert'][] = 'WaterSense Certified';
      $compareLabValArr['ValveType'][] = 'Valve Type';
      $compareLabValArr['FlowRate'][] = 'Flow Rate';
      $compareLabValArr['HolesforInstallation'][] = 'Holes/Width';
      $compareLabValArr['PBSpoutLength'][] = 'Spout Length';
      $compareLabValArr['PBSpoutTotalHeight'][] = 'Spout Total Height';
      $compareLabValArr['PBSpoutHeightDecktoAerator'][] = 'Spout Height Deck to Aerator';

      foreach ($readJsonArr['products'] as $productAttributes) {

        if (array_key_exists("HolesforInstallation", $productAttributes['values'])) {
          $compareLabValArr['HolesforInstallation'][] = $productAttributes['values']['HolesforInstallation'];
        }
        if (array_key_exists("FltLeadFree", $productAttributes['values'])) {
          $compareLabValArr['FltLeadFree'][] = ($productAttributes['values']['FltLeadFree'] == 'T') ? 'Yes' : 'No';
        }
        if (array_key_exists("FltSoapDispenser", $productAttributes['values'])) {
          $compareLabValArr['FltSoapDispenser'][] = ($productAttributes['values']['FltSoapDispenser'] == 'F') ? "No" : "Yes";
        }
        if (array_key_exists("FltSprayKitchen", $productAttributes['values'])) {
          $compareLabValArr['FltSprayKitchen'][] = ($productAttributes['values']['FltSprayKitchen'] == 'F') ? '' : 'Yes';
        }
        if (array_key_exists("ListPrice", $productAttributes['values'])) {
          $compareLabValArr['ListPrice'][] = '$' . $productAttributes['values']['ListPrice'];
        }
        if (array_key_exists("Finish", $productAttributes['values'])) {
          $compareLabValArr['Finish'][] = $productAttributes['values']['Finish'];
        }

        if (array_key_exists("Handle", $productAttributes['values'])) {
          $compareLabValArr['Handle'][] = $productAttributes['values']['Handle'];
        }
        if (array_key_exists("finishOptions", $productAttributes)) {
          if (count($productAttributes['finishOptions'])) {
            $addMore = [];
            foreach ($productAttributes['finishOptions'] as $finishList) {
              $finishProductUrl = self::getProductUrl($finishList['modelNumber']);
              if ($finishProductUrl === '#') {
                $finishProductUrl = "/pdp/" . $finishList['modelNumber'];

              }
              $addMore[] = get_finish_image($finishList['facetFinish'], 'facetfinish', 'both', $finishProductUrl);
            }
            $compareLabValArr['finishOptions'][] = $addMore;
          }
        }
        if (array_key_exists("WatersenseCert", $productAttributes['values'])) {
          $compareLabValArr['WatersenseCert'][] = $productAttributes['values']['WatersenseCert'];
        }
        if (array_key_exists("CalgreenCert", $productAttributes['values'])) {
          $compareLabValArr['CalgreenCert'][] = ($productAttributes['values']['CalgreenCert'] == 'T') ? 'Yes' : 'No';
        }
        if (array_key_exists("ValveType", $productAttributes['values'])) {
          $compareLabValArr['ValveType'][] = $productAttributes['values']['ValveType'];
        }
        if (array_key_exists("ADA", $productAttributes['values'])) {
          $compareLabValArr['ADA'][] = ($productAttributes['values']['ADA'] == 'T') ? 'Yes' : 'No';
        }
        if (array_key_exists("FlowRate", $productAttributes['values'])) {
          $compareLabValArr['FlowRate'][] = $productAttributes['values']['FlowRate'];
        }
        if (array_key_exists("PBSpoutLength", $productAttributes['values'])) {
          $compareLabValArr['PBSpoutLength'][] = $productAttributes['values']['PBSpoutLength'];
        }
        if (array_key_exists("PBSpoutTotalHeight", $productAttributes['values'])) {
          $compareLabValArr['PBSpoutTotalHeight'][] = $productAttributes['values']['PBSpoutTotalHeight'];
        }
        if (array_key_exists("PBSpoutHeightDecktoAerator", $productAttributes['values'])) {
          $compareLabValArr['PBSpoutHeightDecktoAerator'][] = $productAttributes['values']['PBSpoutHeightDecktoAerator'];
        }
        if (array_key_exists("FltTouchClean", $productAttributes['values'])) {
          $compareLabValArr['FltTouchClean'][] = ($productAttributes['values']['FltTouchClean'] == 'F') ? 'No' : 'Yes';
        }
      }
    }


    foreach ($compareLabValArr as $key_0 => $getArrVal) {
      if ($this->is_multi($getArrVal) === FALSE) {
        $dup_cnt_arr_val = array_count_values($getArrVal);
        foreach ($dup_cnt_arr_val as $grepArrDupCnt) {
          // number of product comparison vs highest dup count.
          if ($grepArrDupCnt === $numCompResp) {
            $compareLabValArr[$key_0][] = 'dp';
          }
        }
      }
    }
    foreach ($compareLabValArr as $key => $nxtArr) {
      $newArr = [];
      foreach ($nxtArr as $key2 => $val) {
        $newArr[] = $val;
        if ((in_array('dp', $newArr))) {
          $newArr['dp'] = 'dp';
          if ($nxtArr[$key2] === 'dp') {
            unset($newArr[$key2]);
          }
        }
      }
      $compareLabValArr[$key] = $newArr;
    }


    foreach ($compareLabValArr as $key3 => $compareLabValArrVal) {
      foreach ($compareLabValArrVal as $key4 => $compareLabValArrVal1) {
        if ($key4 === 'dp') {
          $compareLabValArr[$key3][0] = [$compareLabValArr[$key3][0], 'dp'];
          unset($compareLabValArr[$key3]['dp']);
        }
      }
    }
    foreach ($compareLabValArr as $key3_ndp => $compareLabValArrVal_ndp) {
      if ($this->is_multi($compareLabValArrVal_ndp) === FALSE) {
        if (!in_array("ndp", $compareLabValArrVal_ndp)) {
          $compareLabValArr[$key3_ndp][0] = [$compareLabValArr[$key3_ndp][0], 'ndp'];
        }
      }
    }


    $compareLabValArr = array_map('array_values', $compareLabValArr);
    $counts = array_map('count', $compareLabValArr);
    if (is_array($counts) && !empty($counts)) {
      $topArrCnt = max($counts);
      foreach ($compareLabValArr as $keyK => $valK) {
        if ($topArrCnt != count($valK)) {
          $startingIndex = count($valK);
          for ($j = $startingIndex;
               $j < $topArrCnt;
               $j++) {
            $compareLabValArr[$keyK][$j] = '';
          }
        }
      }
    }

    $output = [
      '#theme' => 'product_compare',
      '#items' => $productDetails,
      '#compareItems' => $compareLabValArr,
      '#attached' => [
        'library' => 'product_listing/product_compare',
      ],
    ];
    return $output;
  }

  //Check this is single or multi dimension.
  public  function is_multi($geeks) {
    $rv = array_filter($geeks, 'is_array');
    if (count($rv) > 0) {
      return TRUE;
    }
    return FALSE;
  }

}

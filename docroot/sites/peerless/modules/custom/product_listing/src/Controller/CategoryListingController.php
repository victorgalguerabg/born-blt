<?php

namespace Drupal\product_listing\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\file\Entity\File;
use Drupal\taxonomy\Entity\Term;

/**
 * To change this license header, choose License Headers in Project Properties.
 *
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class CategoryListingController extends ControllerBase {

  /**
   * Implementation of categoryListing().
   */
  public static function categoryListing($tid, $taxonomyName, $shortDesc, $termTree) {
    $levellabel = 'level';
    $result = [];
    $result['title'] = $taxonomyName;
    $result['desc'] = '';
    if (!empty($shortDesc[0]['value'])) {
      $result['desc'] = $shortDesc[0]['value'];
    }
    // Remove view all - clp page.
    $cnt = (int) count($termTree);
    $last = $cnt - 1;
    unset($termTree[$last]);

    foreach ($termTree as $terms) {
      $catImageUrl = '';
      if (in_array($tid, $terms->parents)) {
        $taxonomy = Term::load($terms->tid);
        $categoryDesc = '';
        if (!empty($taxonomy->get('field_shortdescription')->getValue())) {
          $categoryDesc = $taxonomy->get('field_shortdescription')->getValue()[0]['value'];
        }
        $categoryImg = $taxonomy->get('field_category_listing_image')->getValue();
        if ($categoryImg) {
          $categoryImgId = $categoryImg[0]['target_id'];
          $categoryImage = File::load($categoryImgId);
          $catImageUrl = file_create_url($categoryImage->uri->value);
        }
        $catLink = \Drupal::service('pat_alias.manager')->getAliasByPath('/taxonomy/term/' . $terms->tid);
        $result[$levellabel][$terms->tid]['title'] = $terms->name;
        $result[$levellabel][$terms->tid]['image'] = $catImageUrl;
        $result[$levellabel][$terms->tid]['desc'] = $categoryDesc;
        $result[$levellabel][$terms->tid]['url'] = $catLink;
      }
    }
    return [
      '#theme' => 'category_listing_template',
      '#items' => $result,
    ];
  }

}

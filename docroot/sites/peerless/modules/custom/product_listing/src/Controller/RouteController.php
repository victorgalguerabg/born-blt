<?php

namespace Drupal\product_listing\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Class RouteController.
 *
 * @package Drupal\product_listing\Controller
 */
class RouteController extends ControllerBase {

  /**
   * Implementation of  controlRoute().
   */
  public static function controlRoute() {
    return PLPBackBoneController::plpPage();
  }

}

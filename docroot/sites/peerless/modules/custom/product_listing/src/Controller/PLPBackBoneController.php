<?php

namespace Drupal\product_listing\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Class PLPBackBoneController.
 *
 * @package Drupal\product_listing\Controller
 */
class PLPBackBoneController extends ControllerBase {

  /**
   * Implementation of plp page.
   */
  public static function plpPage() {
    return [
      '#theme' => 'product_listing_template',
      '#attached' => [
        'library' => ['product_listing/product_listing'],
      ],
    ];
  }

}

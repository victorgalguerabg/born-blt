
var fileError = true;

(function ($, Drupal) {
  Drupal.behaviors.custom = {
    attach: function(context, settings) {
      $(".product-registration-form .webform-multiple-table .required").on("change input keyup", function(e) {
       var element = $(this);
       var inputValue = element.val(),
           label = element.parent().find('label').text();
           model_number = Drupal.behaviors.commonvalidationfunction.modelnumber();

       // setTimeout (function() {
       if(element.hasClass("validate-modelnumber")) {
         $('.product-registration-form').find(".validate-modelnumber").removeClass("form-item--error");
         var inputValue = element.val();
         if( inputValue == "" ) {
           element.next('p.form-error').remove();
           element.parent().parent().next('p.form-error').remove();
           element.parent().parent('.ac-model-number').after("<p class='form-error'> "+ label + " field is required.</p>");
           //element.after("<p class='form-error'> "+ label + " field is required.</p>");
           element.addClass("error");

         } else if(!inputValue.match(model_number)) {
           element.parent().parent().next('p.form-error').remove();
           element.parent().parent('.ac-model-number').after("<p class='form-error'>Please enter a valid " + label + ".</p>");
           element.addClass("error");

         }else {
           element.parent().parent().next('p.form-error').remove();
         }
       };

       if((inputValue == "") || (inputValue == 0)) {
         if(element.next().find('p.form-error')) {
           element.next('p.form-error').remove();
           element.parent().parent().next('p.form-error').remove();
           element.addClass("error");
           element.after("<p class='form-error'>" + label + " field is required.</p>");
         }
       } else {
         element.next('p.form-error').remove();
         element.removeClass("error");
       }

       $('.auto-suggest .model-num-list').on('click','.row-sku',function() {
         if(element.parent().parent().next().find('p.form-error')) {
           element.parent().parent().next('p.form-error').remove();
         }
       });
     // },1000)
     });
    }
  }
    'use strict';

    $(document).ready(function () {
        var mobileDropDownSubHandled = false;


        $('.carousel-home .carousel-inner:visible .item').first().addClass('active');
        $('.carousel-home .carousel-indicators li').first().addClass('active');
        $(window).on('resize', function () {
            $('.carousel-product .item').removeClass('active');
            $('.carousel-product .carousel-inner:visible .item').first().addClass('active');
        });


        var fileSize = 3000000;
        var allowedExt = ['png','gif','jpg','jpeg','gif'];
        // true : Disable, falst : enabled
     //    var getBtnStatus = $("input#formSubmit").is(":disabled");
        $('#edit-product-image-1, #edit-product-image-2, #edit-product-image-3').on('click', 'blur', 'beforeunload', function(e) {
         var getId = $(this).attr('id');

         if($("." + getId + "-file-validator").length){
             $("." + getId + "-file-validator").remove();
          }
          $("body").data( "filePressed", '1' );
         var saveOldStatus = $("input#formSubmit").is(":disabled");
         var gettnOldStatusObj = $( "body" ).data();
         if(gettnOldStatusObj.btnStatus === undefined){
             $("body").data( "btnStatus", saveOldStatus );
         }
        });
        $('#edit-product-image-1, #edit-product-image-2, #edit-product-image-3').on('change', function() {
         var getBtnStatus = $("input#formSubmit").is(":disabled");
          var ext = $(this).val().split('.').pop().toLowerCase();
           var getId = $(this).attr('id');
         if(($("." + getId + "-file-validator").length)){
             $("." + getId + "-file-validator").remove();
          }
          var gettnOldStatusObj = $( "body" ).data();
          $("input#formSubmit").attr('disabled', gettnOldStatusObj.btnStatus );
          if((!($.inArray(ext, allowedExt) !== -1) || (this.files[0].size > fileSize ) )){
             $("<span class='"+ getId + "-file-validator file-error'>File cannot be more than 3MB & File should be only jpeg,png and gif</span>").insertAfter(this);
          }
        });

        $('.home-button_playback img').on('click',function() {
          $('.embed-responsive iframe').addClass("videotriggered");
          $('.backgroundImage').toggle();
          $('.home-button_playback img').toggle();
        })

        $('.cta-gallery').on('click',function() {
          $('body').addClass('open-model');
          $('#myModal').toggle();
          // galleryImg();
          if($('.modal-content').hasClass('slick-initialized')) {
            $('.modal-content').slick('setPosition');
          }else {

            $('.modal-content').slick({
              dots: false,
              refresh: true,
              infinite: true,
              speed: 300,
              slidesToShow: 1,
              slidesToScroll: 1,
              adaptiveHeight: true,
              prevArrow: '<div class="previousArrow"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 407.436 407.436" style="enable-background:new 0 0 407.436 407.436;" xml:space="preserve"> <polygon points="315.869,21.178 294.621,0 91.566,203.718 294.621,407.436 315.869,386.258 133.924,203.718 "></polygon> </svg></div>',
              nextArrow: '<div class="nextArrow"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 407.436 407.436" style="enable-background:new 0 0 407.436 407.436;" xml:space="preserve"> <polygon points="112.814,0 91.566,21.178 273.512,203.718 91.566,386.258 112.814,407.436 315.869,203.718 "></polygon> </svg></div>'
            });
          }
      });

    function getYTid(url){
      var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
      var match = url.match(regExp);
      return (match&&match[7].length==11)? match[7] : false;

        }

        // You Tube Video onclick
        var youtube = document.querySelectorAll( ".youtube-video" );
        var youtubeVideContainer = document.querySelectorAll( ".collection--landing--videoblock-in" );
        var videoWraper = document.querySelectorAll( ".video-wrapper" );

        for (var i = 0; i < youtube.length; i++) {

            youtube[i].addEventListener( "click", function() {

        var iframe = document.createElement( "iframe" );

        iframe.setAttribute( "frameborder", "0" );
        iframe.setAttribute( "allowfullscreen", "" );
        iframe.setAttribute( "src", "https://www.youtube.com/embed/"+ getYTid(this.dataset.url) +"?rel=0&showinfo=0&autoplay=1" );

        var container = this.getElementsByClassName('collection--landing--videoblock-in-else')[0];
        var video = this.getElementsByClassName('video-responsive')[0];

        video.classList.add('active');
        container.style.display = 'none';
        video.appendChild( iframe );
        $('.video-wrapper').addClass('video-open');
            });

        };

        $('.youtube-closeBtn').on('click',function(){
            $('.video-wrapper').removeClass('video-open');
            $('.collection--landing--videoblock-in-else').show();
            $('.video-responsive').html('').removeClass('active');
            // stopVideo('#nofocusvideo-homeVideo');
        })

        $(".button-modal-homeVideo").on('click', function(){
          var videoUrl = $(this).data('video');
          var iframeVideo = document.createElement( "iframe" );

          iframeVideo.setAttribute( "frameborder", "0" );
          iframeVideo.setAttribute( "allowfullscreen", "" );
          iframeVideo.setAttribute( "src", videoUrl + "?rel=0&showinfo=0&autoplay=1" );

          $('.video-responsive-other').html(iframeVideo);
        });

        $(document).on('click', '.modal--video .close, .modal--video', function(){
          $('.video-responsive-other').html(" ");
      })

        /// PDP
        $('.product-img').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            fade: true,
            asNavFor: '.product-img__nav',
            adaptiveHeight: false,
            responsive: [
                {
                  breakpoint: 768,
                  settings: {
                    dots: true
                    }
                }
            ]
        });

        $('.product-img__nav').slick({

            variableWidth: true,
            slidesToShow: 4,
            slidesToScroll: 1,
            asNavFor: '.product-img',
            dots: false,
            arrows: true,
            focusOnSelect: true,
            infinite: true,
            prevArrow: '<div class="pre"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 407.436 407.436" style="enable-background:new 0 0 407.436 407.436;" xml:space="preserve"> <polygon points="315.869,21.178 294.621,0 91.566,203.718 294.621,407.436 315.869,386.258 133.924,203.718 "></polygon> </svg></div>',
            nextArrow: '<div class="next"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 407.436 407.436" style="enable-background:new 0 0 407.436 407.436;" xml:space="preserve"> <polygon points="112.814,0 91.566,21.178 273.512,203.718 91.566,386.258 112.814,407.436 315.869,203.718 "></polygon> </svg></div>'
        });


        //var video = $('.product-img .slick-active').find('video').get(0).play();



        $('.product-slider').slick({
            arrows: true,
            slidesToShow: 3,
            slidesToScroll: 3,
            autoplay: true,
            prevArrow: '<div class="pre"><svg viewBox="0 0 28.667 56.565"> <polygon points="2.414,27.698 26.598,3.514 27.305,4.221 3.121,28.405 27.477,52.76 26.77,53.467 1.707,28.405"></polygon> </svg></div>',
            nextArrow: '<div class="next"><svg viewBox="0 0 28.667 56.565"> <polygon points="26.77,27.698 2.586,3.514 1.879,4.221 26.062,28.405 1.707,52.76 2.414,53.467 27.477,28.405"></polygon> </svg></div>',
            responsive: [
                {
                    breakpoint: 992,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2,
                    }
                },
                {
                  breakpoint: 768,
                  settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: true,
                    dots: true,
                    arrows: false,
                  }
                }
            ]
        });
     $('.product-slide .row').slick({
      slidesToShow: 4,
      slidesToScroll: 1,
      dots: false,
      infinite: false,
      cssEase: 'linear',
      swipe:false,
      nextArrow: '<button id="compare-nextbtn" class="compare-next btn btn-default pull-right"><span></span></button>',
      prevArrow: '<button id="compare-prevbtn" class="compare-previous btn btn-default"><span></span></button>',
      responsive: [{

          breakpoint: 1024,
          settings: {
          slidesToShow: 2
          }

        }, {

          breakpoint: 600,
          settings: {
          slidesToShow: 2
          }

        }, {

          breakpoint: 300,
          settings: "unslick" // destroys slick

        }]
    });


    var sliderCount = $(".product__compare__table .product-slides-images .slick-slide").length;
    if(sliderCount == 2){
      $(".product__compare__table").addClass("slide-two");
    }
    else if(sliderCount == 3){
      $(".product__compare__table").addClass("slide-three");
    }

    $("#compare-nextbtn").bind("click", (function () {
      $(".product-sub-info .compare-next").trigger("click");

    }));

    $("#compare-prevbtn").bind("click", (function () {
      $(".product-sub-info .compare-previous").trigger("click");

    }));

        $(document).on('click', '.bv_numReviews_text, .bv_button_buttonFull', function(event) {
            event.preventDefault();
            $('.nav-tabs a[href="#reviews"]').tab('show');
                $('html, body').stop().animate({
                    scrollTop: $('.tab-content').offset().top
            }, 'slow');
        });
        $(document).on('click', '.bv-content-product-stats-item-reviews', function(event) {
            event.preventDefault();
            $('.nav-tabs a[href="#reviews"]').tab('show');

        });
        $(document).on('click', '.bv-content-product-stats-item-questions, .bv-content-product-stats-item-answers', function(event) {
            event.preventDefault();
            $('.nav-tabs a[href="#questions"]').tab('show');

        });
        $(document).on('click', 'button#AAQ', function(event) {
            event.preventDefault();
            $('.nav-tabs a[href="#questions"]').tab('show');
        });

        $(document).on('click', '.header-search-icon', function(event) {
            event.preventDefault();
            $('.header-nav-icon, .header-mega-menu').removeClass('open');
            $('body').toggleClass('search-active');
        });

        $(document).click(function() {
        var $trigger = $(".ui-dialog");
            if($trigger !== event.target && !$trigger.has(event.target).length){
                $(".ui-dialog").remove();
            }
        });


        $('.header-nav-icon').click(function(){
            $('body').removeClass('search-active');
            $(this).toggleClass('open');
            $('.header-mega-menu').toggleClass('open');
            $('.menu--mobile-main-menu').toggleClass('open');
            $('body').toggleClass('body-no-scroll');
            $('.header-mega-menu__list--dropddown').removeClass('current');
        });
    $('.faqfield-question').click(function() {
      if (!$(this).parent().parent().parent('.paragraph--type--faq').hasClass('accordion-selected')) {
        $('.paragraph--type--faq').removeClass('accordion-selected');
        $(this).parent().parent().parent('.paragraph--type--faq').addClass('accordion-selected');
      } else {
        $(this).parent().parent().parent('.paragraph--type--faq').removeClass('accordion-selected');
      }
    });
    $('.autocomplete-container--inner .autocomplete ').focus(function() {
      $(".autocomplete-container--inner").addClass("animate");
    });

    $('.autocomplete-container--inner .autocomplete ').focusout(function() {
      $(".autocomplete-container--inner").removeClass("animate");
    });

      $('.product-img').on('beforeChange', function(event, slick, currentSlide, nextSlide){
        $('video').each(function() {
          $(this).get(0).pause();
          $(this).get(0).currentTime = 0;
        });
      });

        // $(document).on('click', '.mobile-view .header-mega-menu__list--dropddown a', function(event) {
        //    // event.preventDefault();
        //     var elem = $(this).closest('li');
        //     if(elem.hasClass('header-mega-menu__list--active')){
        //         elem.removeClass('header-mega-menu__list--active');
        //     } else{
        //         $('.header-mega-menu__list__link').removeClass('header-mega-menu__list--active');
        //         elem.toggleClass('header-mega-menu__list--active');
        //     }
        // });
    $('#edit-product-image-1-upload').on("change", function(){
      $('.file-upload-cont-first .form-submit').val('Choose File');
        });
    $('#edit-product-image-2-upload').change(function(e){
            var fileName = e.target.files[0].name;
            $(".file-up-sec .file-select-name").html(fileName);
        });
    $('#edit-product-image-3-upload').change(function(e){
            var fileName = e.target.files[0].name;
            $(".file-up-third .file-select-name").html(fileName);
        });


    // FAQ navigation active links
    if ($('.view-faq').length && ($('.view-faq ul.list-unstyled li a').html() !== '' || $('.view-faq ul.list-unstyled li a').html() !== null || $('.view-faq ul.list-unstyled li a').html() !== undefined)) {
               var getPath = window.location.pathname;
               $('.view-faq ul li').each(function () {
                   var termPath = $(this).find('a').attr('href');
                   if (getPath === termPath) {
                       $('[href*="' + getPath + '"]').addClass('is-active');
                   }
               });
           }


           if ($('.menu--customer-support').length && ($('.menu--customer-support ul.list-unstyled li a').html() !== '' || $('.menu--customer-support ul.list-unstyled li a').html() !== null || $('.menu--customer-support ul.list-unstyled li a').html() !== undefined)) {
             var getPath = window.location.pathname;
             $('.menu--customer-support ul li').each(function () {
               var termPath = $(this).find('a').attr('href');
               if (getPath === termPath) {
                 $(this).addClass('is-active');
                 return false;
               }
             });
           }

        $('.sidebar--mobile-header').click(function(){
            $('.sidebar').toggleClass('active');
            $('.sidebar--container').toggleClass('show');
            $('.sidebar--mobile-header .glyphicon').toggleClass('open');
        });

        var resMobile = true, resDesktop = true;

        responsiveView();
        desktopDropDownHover();


        $(window).resize(function() {
            responsiveView();
        });

        function responsiveView(){
            var screenWidth = $(window).width();
            var breakpoints = '992';

            if (screenWidth <= breakpoints - 1 && resMobile === true) {
                resMobile = false;
                resDesktop = true;
                $('body').removeClass('desktop-view').addClass('mobile-view');
                mobileDropDownSub();
            }
            else if(screenWidth >= breakpoints && resDesktop === true ){
                resMobile = true;
                resDesktop = false;
                $("body").addClass('desktop-view').removeClass('mobile-view');
            }
        }

     var response;

         // hover event and enders for desktop nav dropdown

        function desktopDropDownHover() {

        var itemImage = $('.menu-item--image');
        var itemLinks = $('.menu-item--links');
          if ($(window).width() > 990) {

            $('.header-mega-menu__ul >.header-mega-menu__list--dropddown').hoverIntent(
                function (event) {
                    event.stopPropagation();
                    event.stopImmediatePropagation();
                    event.preventDefault();

                    var menuElement = $(this);
                    menuElement.addClass('current');
                },
                function (event) {
                    event.stopPropagation();
                    event.stopImmediatePropagation();
                    event.preventDefault();
                  var menuElement = $(this);
                    menuElement.removeClass('current');
                }
            );
          }
        }

        $('.header-mega-menu__list--dropddown').on('click', function () {
          if ($(window).width() > 990) {
            $('.header-mega-menu__list--dropddown').removeClass('current');
            $(this).addClass('current');
          }
        });
        // on click behavior to trigger dropdowns on mobile
        function mobileDropDownSub() {
        $('.header-mega-menu__list').on('click', function(){
          if ($(this).hasClass('current')){
           $(this).removeClass('current');
          }
          else{
           $('.header-mega-menu__list').removeClass('current');
           $(this).addClass('current');
          }
        });
      }

      //Mobile Menu
      if($('.header-nav-icon').hasClass('open')) {
        $('body').addClass('body-no-scroll');
      } else {
        $('body').removeClass('body-no-scroll');
      }
  });

  $('.contact-form .webform-button--submit').attr('disabled',false);

//  $('.product-registration-form .btn--register-primary').attr("disabled", true);

  $('.g-recaptcha').attr('data-expired-callback','captchaExpired');

  $('.webform-submission-form .product-model-number, .webform-submission-form .product-purchase-date').find('input').addClass('form-control');

  $('.webform-submission-form .product-model-number-label,.webform-submission-form .product-purchase-date-label').addClass('webform-labels');

  function captchaExpired() {
    $('.product-registration-form input[name = "hdn_captcha"]').val("0");
    grecaptcha.reset();
  }

  $('.collection-detail-page').on('click', '.close',function() {
    $('body').removeClass('open-model')
  });

  $(document).ajaxComplete(function(event, xhr, settings) {
    if(($('.contact-form #edit-product-model-number').val()) == "") {
      $('.contact-form .auto-suggest').addClass('contactToggleclass');
    }
  });

  //Contact Us form Realtime Validation
    $(".contact-form .required").on("change input", function(e) {
      var nameregex  = Drupal.behaviors.commonvalidationfunction.usernameregex(),
          emailregex = Drupal.behaviors.commonvalidationfunction.emailidregex(),
          phoneregex = Drupal.behaviors.commonvalidationfunction.phonenumberregex(),
          address = Drupal.behaviors.commonvalidationfunction.addressreg(),
          city = Drupal.behaviors.commonvalidationfunction.cityvalidation(),
          zip_code = Drupal.behaviors.commonvalidationfunction.zipcodevalidation();
          value = 0;

      var inputValue = $(this).val(),
          label = $($("label[for='" + this.id + "']").contents().get(0)).text(),
          element = $(this);

      //Name field
      var validatefunction = setTimeout(function() {
        if((element.val()) !== "") {
          element.next('p.form-error').remove();
          element.removeClass('error');
          value--;
        } else {
            element.next('p.form-error').remove();
            element.after("<p class='form-error'> "+ label + " field is required.</p>");
            element.addClass("error");
            value++;
          }

        if(element.hasClass("validate-name")) {
          $('.contact-form').find(".validate-name").removeClass("form-error");
          var inputValue = element.val();
          if(!inputValue.match(nameregex) && inputValue !== '') {
            element.next('p.form-error').remove();
            element.after("<p class='form-error'>Please enter a valid " + label + ".</p>");
            element.addClass("error");
            value++;
          }
        }

        //Email Address
        if(element.hasClass("validate-email")) {
          $('.contact-form').find(".validate-email").removeClass("form-error");
          var inputValue = element.val();
          if(!inputValue.match(emailregex) && inputValue !== '') {
            element.next('p.form-error').remove();
            element.after("<p class='form-error'>Please enter a valid " + label + ".</p>");
            element.addClass("error");
            value++;
          }
        };

        //PhoneNumber validation
        if(element.hasClass("validate-phone")) {
          $('.contact-form').find(".validate-phone").removeClass("form-error");
          var inputValue = element.val();
          if(!inputValue.match(phoneregex) && inputValue !== '') {
            element.next('p.form-error').remove();
            element.after("<p class='form-error'>Please enter a valid " + label + ".</p>");
            element.addClass("error");
            value++;
          }
        };

        //Address field
        if(element.hasClass("validate-address")) {
          $('.contact-form').find(".validate-address").removeClass("form-error");
          var inputValue = element.val();
          if(!inputValue.match(address) && inputValue !== '') {
            element.next('p.form-error').remove();
            element.after("<p class='form-error'>Please enter a valid " + label + ".</p>");
            element.addClass("error");
            value++;
          }
        };

        //City
        if(element.hasClass("validate-city")) {
          $('.contact-form').find(".validate-city").removeClass("form-error");
          var inputValue = element.val();
          if(!inputValue.match(city) && inputValue !== '') {
            element.next('p.form-error').remove();
            element.after("<p class='form-error'>Please enter a valid " + label + ".</p>");
            element.addClass("error");
            value++;
          }
        };

        //Zipcode Validation
        if(element.hasClass("validate-zipcode")) {
          $('.contact-form').find(".validate-zipcode").removeClass("form-error");
          var inputValue = element.val();
          if(!inputValue.match(zip_code) && inputValue !== '') {
            element.next('p.form-error').remove();
            element.after("<p class='form-error'>Please enter a valid " + label + ".</p>");
            element.addClass("error");
            value++;
          }
        };

        clearTimeout(validatefunction);
      }, 3000);
    });

    $('.contact-form .required').on('change',function(){

      var element = $(this);
      var validatefunction = setTimeout(function() {
        if(element.hasClass("melliansa-validation")) {
          $('.contact-form').find(".melliansa-validation").removeClass("form-error");
          var emailID = element.val();
          mellinsa = Drupal.behaviors.commonvalidationfunction.searchEmail(emailID, element);
        };
/* 
        if($('.contact-form').find(".form-item--error")){
          //alert("working");
          $(".melliansa-validation").addClass("error");
          $(".form-item--error-message").addClass("form-error");
          $(".form-error").removeClass("form-item--error-message");
        }
        else{
          $(".melliansa-validation").removeClass("error");
        } */

        clearTimeout(validatefunction);
      },3000)
      
    });

    //ContactUs On click of submit button
    $('.contact-form').on('click','.form-submit',function() {
      var value = 0;
      $('.contact-form').find(".required:visible").not(".form-radio, .shipping_option .required").each(function() {
        var inputValue = $(this).val(),
        label = $($("label[for='" + this.id + "']").contents().get(0)).text();
        if((inputValue == "") || (inputValue == 0)) {
          if($(this).next().find('p.form-error')) {
            $(this).next('p.form-error').remove();
            value++;
            $(this).addClass("error");
            $(this).after("<p class='form-error'>" + label + " field is required.</p>");
          }
        }
        var element = $(this)
            value = peerless.commonFunctions.FormFieldTypeValidation(element, label, value);
      });
      if(grecaptcha.getResponse() == "") {
        event.preventDefault();
        value++;
      }else {
        $('.captcha').next('p.form-error').remove();
      }

      if($('.contact-form').find("p.form-error").text()) {
        value++;
      }

      if($('.contact-form').find('.error')) {
        $("html, body").animate({
          scrollTop:  ($('.error').offset() || { "top": NaN }).top - 200
        }, 500);
      }

      if(value > 0) {
        return false;
      }
    });

    $('.contact-form').on('change','.form-select',function() {
      var inputValue = $(this).val(),
          label = $($("label[for='" + this.id + "']").contents().get(0)).text();
      if(inputValue !== "") {
        $(this).next('p.form-error').remove();
        $(this).parent().removeClass('error');
      }else {
        $(this).after("<p class='form-error'>" + label + " field is required.</p>");
        $(this).parent().addClass('error');
      }
    });

    //Product Registration form Realtime Validation
      $(".product-registration-form .required").on("change input", function(e) {
        var nameregex = Drupal.behaviors.commonvalidationfunction.usernameregex(),
            emailregex = Drupal.behaviors.commonvalidationfunction.emailidregex(),
            phoneregex = Drupal.behaviors.commonvalidationfunction.phonenumberregex(),
            address = Drupal.behaviors.commonvalidationfunction.addressreg(),
            city = Drupal.behaviors.commonvalidationfunction.cityvalidation(),
            zip_code = Drupal.behaviors.commonvalidationfunction.zipcodevalidation(),
            model_number = Drupal.behaviors.commonvalidationfunction.modelnumber(),
            value = 0;

        var inputValue = $(this).val(),
            label = $(this).siblings('label').text(),
            element = $(this);

        //Name field
        var validatefunction = setTimeout(function() {
          if((element.val()) !== "") {
            element.next('p.form-error').remove();
            element.removeClass('error');
          }
          else {
            element.next('p.form-error').remove();
            element.after("<p class='form-error'> "+ label + " field is required.</p>");
            element.addClass("error");
          }

          if(element.hasClass("validate-name")) {
            $('.product-registration-form').find(".validate-name").removeClass("form-item--error");
            var inputValue = element.val();
            if( inputValue == "" ) {
              element.next('p.form-error').remove();
              element.after("<p class='form-error'> "+ label + " field is required.</p>");
              element.addClass("error");
              value++;
            } else if(!inputValue.match(nameregex)) {
              element.next('p.form-error').remove();
              element.after("<p class='form-error'>Please enter a valid " + label + ".</p>");
              element.addClass("error");
              value++;
            }
          }

          //Email Address
          if(element.hasClass("validate-email")) {
            $('.product-registration-form').find(".validate-email").removeClass("form-item--error");
            var inputValue = element.val();
            if( inputValue == "" ) {
              element.next('p.form-error').remove();
              element.after("<p class='form-error'> "+ label + " field is required.</p>");
              element.addClass("error");
              value++;
            } else if(!inputValue.match(emailregex)) {
              element.next('p.form-error').remove();
              element.after("<p class='form-error'>Please enter a valid " + label + ".</p>");
              element.addClass("error");
              value++;
            }
          };

          //PhoneNumber validation
          if(element.hasClass("validate-phone")) {
            $('.product-registration-form').find(".validate-phone").removeClass("form-item--error");
            var inputValue = element.val();
            if( inputValue == "" ) {
              element.next('p.form-error').remove();
              element.after("<p class='form-error'> "+ label + " field is required.</p>");
              element.addClass("error");
              value++;
            } else if(!inputValue.match(phoneregex)) {
              element.next('p.form-error').remove();
              element.after("<p class='form-error'>Please enter a valid " + label + ".</p>");
              element.addClass("error");
              value++;
            }
          };

          //Address field
          if(element.hasClass("validate-address")) {
            $('.product-registration-form').find(".validate-address").removeClass("form-item--error");
            var inputValue = element.val();
            if( inputValue == "" ) {
              element.next('p.form-error').remove();
              element.after("<p class='form-error'> "+ label + " field is required.</p>");
              element.addClass("error");
              value++;
            } else if(!inputValue.match(address)) {
              element.next('p.form-error').remove();
              element.after("<p class='form-error'>Please enter a valid " + label + ".</p>");
              element.addClass("error");
              value++;
            }
          };

          //City
          if(element.hasClass("validate-city")) {
            $('.product-registration-form').find(".validate-city").removeClass("form-item--error");
            var inputValue = element.val();
            if( inputValue == "" ) {
              element.next('p.form-error').remove();
              element.after("<p class='form-error'> "+ label + " field is required.</p>");
              element.addClass("error");
              value++;
            } else if(!inputValue.match(city)) {
              element.next('p.form-error').remove();
              element.after("<p class='form-error'>Please enter a valid " + label + ".</p>");
              element.addClass("error");
              value++;
            }
          };

          //Zipcode validation
          if(element.hasClass("validate-zipcode")) {
            $('.product-registration-form').find(".validate-zipcode").removeClass("form-item--error");
            var inputValue = element.val();
            if( inputValue == "" ) {
              element.next('p.form-error').remove();
              element.after("<p class='form-error'> "+ label + " field is required.</p>");
              element.addClass("error");
              value++;
            } else if(!inputValue.match(zip_code)) {
              element.next('p.form-error').remove();
              element.after("<p class='form-error'>Please enter a valid " + label + ".</p>");
              element.addClass("error");
              value++;
            }
          };

          if(element.hasClass("validate-textarea")) {
            $('.product-registration-form').find(".validate-textarea").removeClass("form-item--error");
            var inputValue = element.val();
            if( inputValue == "" ) {
              element.next('p.form-error').remove();
              element.after("<p class='form-error'> "+ label + " field is required.</p>");
              element.addClass("error");
              value++;
            }
          };

          clearTimeout(validatefunction);
        }, 3000);
      });

      $('.product-registration-form .required').on('change',function(){
        var element = $(this);
        var validatefunction = setTimeout(function() {
          if(element.hasClass("melliansa-validation")) {
            $('.product-registration-form').find(".melliansa-validation").removeClass("form-item--error");
            var emailID = element.val();
            mellinsa = Drupal.behaviors.commonvalidationfunction.searchEmail(emailID, element);
          };
          clearTimeout(validatefunction);
        },3000)
        
      });

    //Product Registration On click of submit button
    $('.product-registration-form').on('click','.form-submit',function() {
      var value = 0;
      $('.product-registration-form').find(".required:visible").not(".form-radio, .shipping_option .required").each(function() {
        var inputValue = $(this).val(),
        label = $(this).siblings('label').text();
        if((inputValue == "") || (inputValue == 0)) {
          if($(this).next().find('p.form-error')) {
            $(this).next('p.form-error').remove();
            value++;
            $(this).addClass("error");
            $(this).after("<p class='form-error'>" + label + " field is required.</p>");
          }
        }
        element = $(this);
        value = peerless.commonFunctions.FormFieldTypeValidation(element, label, value);
      });
      if(grecaptcha.getResponse() == "") {
        event.preventDefault();
      }else {
        $('.captcha').next('p.form-error').remove();
      }

      if($('.product-registration-form').find("p.form-error").text()) {
        value++;
      }

      if($('.product-registration-form').find('.error')) {
        $("html, body").animate({
          scrollTop:  ($('.form-error').offset() || { "top": NaN }).top - 200
        }, 500);
      }

      if(value > 0) {
        return false;
      }
    });

    $('.product-registration-form').on('change','.form-select',function() {
      var inputValue = $(this).val(),
          label = $($("label[for='" + this.id + "']").contents().get(0)).text();
      if(inputValue !== "") {
        $(this).next('p.form-error').remove();
        $(this).removeClass('error');
      }else {
        $(this).after("<p class='form-error'>" + label + " field is required.</p>");
        $(this).addClass('error');
      }
    });

    // //Product Registration Form On click of submit button
    // $('.product-registration-form').on('click','.webform-button--submit',function() {
    //   var value = 0;
    //   $('.product-registration-form').find(".required:visible").not(".form-radio, .shipping_option .required").each(function() {
    //     var inputValue = $(this).val(),
    //         element = $(this),
    //         label = element.siblings('label').text();
    //     if((inputValue == "") || (inputValue == 0)) {
    //       if($(this).next().find('p.form-error')) {
    //         $(this).next('p.form-error').remove();
    //         value++;
    //         $(this).addClass("error");
    //         $(this).after("<p class='form-error'>" + label + " field is required.</p>");
    //       }
    //     }
    //     var element = $(this)
    //         value = peerless.commonFunctions.FormFieldTypeValidation(element, label, value);
    //   });
    //   if(grecaptcha.getResponse() == "") {
    //     event.preventDefault();
    //     value++;
    //   }else {
    //     $('.captcha').next('p.form-error').remove();
    //   }
    //
    //
    //   if($('.product-registration-form').find('.error')) {
    //     $("html, body").animate({
    //       scrollTop:  ($('.error').offset() || { "top": NaN }).top - 200
    //     }, 500);
    //   }
    //
    //   if(value > 0) {
    //     return false;
    //   }
    // });

    $(".webform-submission-newsletter-sign-up-form .required").on("change input", function(e) {
      var nameregex  = Drupal.behaviors.commonvalidationfunction.usernameregex(),
          emailregex = Drupal.behaviors.commonvalidationfunction.emailidregex(),
          value = 0;
    
          //var inputValue = $(this).val(),
          label = $(this).parent().find('label').text(),
          element = $(this);
    
      //Name field
      var validatefunction = setTimeout(function() {
        if((element.val()) !== "") {
          element.next('p.form-error').remove();
          element.removeClass('error');
        } else {
          element.next('p.form-error').remove();
          element.after("<p class='form-error'> "+ label + " field is required.</p>");
          element.addClass("error");
        }
    
        if(element.hasClass("validate-name")) {
          $('.webform-submission-newsletter-sign-up-form').find(".validate-name").removeClass("form-item--error");
          var inputValue = element.val();
          if( inputValue == "" ) {
            element.next('p.form-error').remove();
            element.after("<p class='form-error'> "+ label + " field is required.</p>");
            element.addClass("error");
            value++;
          } else if(!inputValue.match(nameregex)) {
            element.next('p.form-error').remove();
            element.after("<p class='form-error'>Please enter a valid " + label + ".</p>");
            element.addClass("error");
            value++;
          }
        }
    
        //Email Address
        if(element.hasClass("validate-email")) {
          $('.webform-submission-newsletter-sign-up-form').find(".validate-email").removeClass("form-item--error");
          var inputValue = element.val();
          if( inputValue == "" ) {
            element.next('p.form-error').remove();
            element.after("<p class='form-error'> "+ label + " field is required.</p>");
            element.addClass("error");
            value++;
          } else if(!inputValue.match(emailregex)) {
            element.next('p.form-error').remove();
            element.after("<p class='form-error'>Please enter a valid " + label + ".</p>");
            element.addClass("error");
            value++;
          }
        };
        if(element.hasClass("validate-email")) {
          $(".webform-submission-newsletter-sign-up-form").find(".melliansa-validation").each(function() {
            var element = $(this);
            var emailID = element.val();
            mellinsa = Drupal.behaviors.commonvalidationfunction.searchEmail(emailID, element);
          })
        };
       
        clearTimeout(validatefunction);
      }, 3000);
      
    });

    $(".webform-submission-newsletter-sign-up-form.contact-form-newsletter .webform-button--submit").click(function(e) {
      var value = 0;
    
      $(".webform-submission-newsletter-sign-up-form").find(".required:visible").each(function() {
        element = $(this);
        label = $(this).parent().find('label').text();
        value = peerless.commonFunctions.FormFieldTypeValidation(element, label, value);
      });
     
      if($(".webform-submission-newsletter-sign-up-form").find("p.form-error").text()) {
        value++;
      }
    
      if($('.webform-submission-newsletter-sign-up-form').find('.form-error')) {
        $("html, body").animate({
          scrollTop:  ($('.form-error').offset() || { "top": NaN }).top - 200
        }, 500);
      }

      if(value > 0){
        return false;
      }
    });

    $(document).ajaxComplete(function(event, xhr, settings) {
      if($('.contact-form').find('.messages--error')) {
        event.preventDefault();
      }
    });


})(jQuery, Drupal);

jQuery.noConflict();

var peerless = {};

(function ($) {
    peerless.commonFunctions = {
      FormFieldTypeValidation: function(element, label, value){
        var nameregex  = Drupal.behaviors.commonvalidationfunction.usernameregex(),
            emailregex = Drupal.behaviors.commonvalidationfunction.emailidregex(),
            phoneregex = Drupal.behaviors.commonvalidationfunction.phonenumberregex(),
            address = Drupal.behaviors.commonvalidationfunction.addressreg(),
            city = Drupal.behaviors.commonvalidationfunction.cityvalidation(),
            zip_code = Drupal.behaviors.commonvalidationfunction.zipcodevalidation();
            //model_number = Drupal.behaviors.commonvalidationfunction.modelnumber();

          if(element.hasClass("validate-name")) {
            $('.contact-form').find(".validate-name").removeClass("form-error");
            var inputValue = element.val();
            if( inputValue == "" ) {
              element.next('p.form-error').remove();
              element.after("<p class='form-error'> "+ label + " field is required.</p>");
              element.addClass("error");
              value++;
            } else if(!inputValue.match(nameregex)) {
              element.next('p.form-error').remove();
              element.after("<p class='form-error'>Please enter a valid " + label + ".</p>");
              element.addClass("error");
              value++;
            }
          }

          if(element.hasClass("validate-email")) {
            $('.contact-form').find(".validate-email").removeClass("form-error");
            var inputValue = element.val();
            if( inputValue == "" ) {
              element.next('p.form-error').remove();
              element.after("<p class='form-error'> "+ label + " field is required.</p>");
              element.addClass("error");
              value++;
            } else if(!inputValue.match(emailregex)) {
              element.next('p.form-error').remove();
              element.after("<p class='form-error'>Please enter a valid " + label + ".</p>");
              element.addClass("error");
              value++;
            }
          }
          //PhoneNumber validation
          if(element.hasClass("validate-phone")) {
            $('.contact-form').find(".validate-phone").removeClass("form-error");
            var inputValue = element.val();
            if( inputValue == "" ) {
              element.next('p.form-error').remove();
              element.after("<p class='form-error'> "+ label + " field is required.</p>");
              element.addClass("error");
              value++;
            } else if(!inputValue.match(phoneregex)) {
              element.next('p.form-error').remove();
              element.after("<p class='form-error'>Please enter a valid " + label + ".</p>");
              element.addClass("error");
              value++;
            }
          }
          //Address field
          if(element.hasClass("validate-address")) {
            $('.contact-form').find(".validate-address").removeClass("form-error");
            var inputValue = element.val();
            if( inputValue == "" ) {
              element.next('p.form-error').remove();
              element.after("<p class='form-error'> "+ label + " field is required.</p>");
              element.addClass("error");
              value++;
            } else if(!inputValue.match(address)) {
              element.next('p.form-error').remove();
              element.after("<p class='form-error'>Please enter a valid " + label + ".</p>");
              element.addClass("error");
              value++;
            }
          }
          //City
          if(element.hasClass("validate-city")) {
            $('.contact-form').find(".validate-city").removeClass("form-error");
            var inputValue = element.val();
            if( inputValue == "" ) {
              element.next('p.form-error').remove();
              element.after("<p class='form-error'> "+ label + " field is required.</p>");
              element.addClass("error");
              value++;
            } else if(!inputValue.match(city)) {
              element.next('p.form-error').remove();
              element.after("<p class='form-error'>Please enter a valid " + label + ".</p>");
              element.addClass("error");
              value++;
            }
          }

          if(element.hasClass("validate-zipcode")) {
            $('.contact-form').find(".validate-zipcode").removeClass("form-error");
            var inputValue = element.val();
            if( inputValue == "" ) {
              element.next('p.form-error').remove();
              element.after("<p class='form-error'> "+ label + " field is required.</p>");
              element.addClass("error");
              value++;
            } else if(!inputValue.match(zip_code)) {
              element.next('p.form-error').remove();
              element.after("<p class='form-error'>Please enter a valid " + label + ".</p>");
              element.addClass("error");
              value++;
            }
          };
          return value;
        },

    //Form Validation
    FormValidation: function (Form) {
            var value = 0;
            var regex = Drupal.behaviors.commonvalidationfunction.emailidregex();
            var number_regex = /^([+|\d])+([\s|\d])+([\-|\d])+([\d])$/;
            var phone_number = Drupal.behaviors.commonvalidationfunction.phonenumberregex();
            var zip_code = Drupal.behaviors.commonvalidationfunction.zipcodevalidation();
            var address= Drupal.behaviors.commonvalidationfunction.addressreg();
            var name = Drupal.behaviors.commonvalidationfunction.usernameregex();
            var alphanumeric = /^[a-zA-Z'.\s]{1,40}$/;
            var pass_regex = /^(?=.*[A-Z])(?=.*[a-z])(?=.*[!@#$%^&*()_+\-=\[\]{};\':"\\|,.<>\/?])(?=.*[0-9]).{8,25}$/;
            var cvv = /^[0-9]{3,4}$/;
            var test = [];
            $(Form).find(".form-error").remove();
            $(Form).find(".required, .validate-email").removeClass('error');
      $(Form).find('.required:visible').not('.form-radio').each(function () {
                var inputValue = $(this).val();
                var label = $($("label[for='" + this.id + "']").contents().get(0)).text();
                if (inputValue == '' || inputValue == '_none' || inputValue == 0) {
                    value++;
                        $(this).addClass('error');
                        $(this).after("<p class='form-error'>" + label + " field is required.</p>");
                }

                if($('.checkboxes--wrapper').hasClass('required')) {
                  $('.checkboxes--wrapper').next('p.form-error').remove();
                  if(!$('.form-checkbox').is(":checked")) {
                    $('.checkboxes--wrapper').next('p.form-error').remove();
                    var error_message = $('.checkboxes--wrapper').attr('data-webform-required-error');
                    $('.checkboxes--wrapper').after('<p class="form-error">'+ error_message + '</p>');
                    value++;
                  } else {
                    $('.checkboxes--wrapper').next('p.form-error').remove();
                    $(this).parent().removeClass('form-error');
                    value--;
                  }
                }
            });


            $(Form).find('.validate-phone').each(function () {
                $(Form).find(".validate-phone").removeClass('error');
                var inputValue = $(this).val();
                var label = $($("label[for='" + this.id + "']").contents().get(0)).text();
                if ((inputValue != '' && !inputValue.match(phone_number))) {
                    value++;
                    $(this).after("<p class='form-error'>Please enter a valid 10 digit phone number(xxxxxxxxxx).</p>");
                    $(this).addClass('error');
                }
            });

            $(Form).find(".validate-address").each(function() {
                $(Form).find(".validate-address").removeClass("form-item--error");
                var inputValue = $(this).val();
                $($("label[for='" + this.id + "']").contents().get(0)).text();
                "" == inputValue || inputValue.match(address) || (value++, $(this).after("<p class='form-error'>Please enter a valid Address.</p>"),
                    $(this).parent().addClass("error"));
            }),

            $(Form).find('.validate-zipcode').each(function () {
                $(Form).find(".validate-zipcode").removeClass('error');
                var inputValue = $(this).val();
                var label = $($("label[for='" + this.id + "']").contents().get(0)).text();
                if ((inputValue != '' && !inputValue.match(zip_code))) {
                    value++;
                    $(this).after("<p class='form-error'>Zip code field is not valid.</p>");
                    $(this).addClass('error');
                }
            });

            $(Form).find('.validate-email').each(function () {
                var inputValue = $(this).val();
                if (!regex.test(inputValue) && inputValue != '') {
                    value++;
                    $(this).after("<p class='form-error'>Email field is not valid.</p>");
                    $(this).addClass('error');
                }
            });

            $(Form).find(".validate-name").each(function() {
                $(Form).find(".validate-name").removeClass("form-item--error");
                var inputValue = $(this).val();
                $($("label[for='" + this.id + "']").contents().get(0)).text();
                "" == inputValue || inputValue.match(name) || (value++, $(this).after("<p class='form-error'>Please enter a valid Name.</p>"),
                    $(this).parent().addClass("error"));
            });

            $(Form).find(".validate-city").each(function() {
                $(Form).find(".validate-city").removeClass("form-item--error");
                var inputValue = $(this).val();
                $($("label[for='" + this.id + "']").contents().get(0)).text();
                "" == inputValue || inputValue.match(name) || (value++, $(this).after("<p class='form-error'>Please enter a valid City.</p>"),
                    $(this).parent().addClass("error"));
            });

            $(Form).find(".g-recaptcha").each(function() {
              //var captchaValue = $(Form).find(".captcha-validate").val();
              var label = $($("label[for='" + this.id + "']").contents().get(0)).text();
              if(grecaptcha.getResponse() == "") {
                event.preventDefault();
                $(this).next('.form-error').remove();
                $(this).parent().addClass("error");
                $(this).after("<p class='form-error'>Recaptca field is required.</p>");
              } else {
                $(this).next('.form-error').remove();
                $(this).parent().removeClass("error");
              }
            });

            if (value > 0) {
                return false;
            } else {
                return true;
            }
        },

        ScrollTopPosition: function(ScrollPosition) {
          $("html, body").animate({
              scrollTop:  ($(ScrollPosition).offset() || { "top": NaN }).top - 200
          }, 500);
      },

      /* Newsletter: function () {
          $('.webform-submission-newsletter-sign-up-form .webform-button--submit').click(function (e) {
              var Form = $('.webform-submission-newsletter-sign-up-form');
              var FormField = peerless.commonFunctions.FormValidation(Form);
      if(!FormField){
        peerless.commonFunctions.ScrollTopPosition($(Form).find(".error:first"));
      }
              return FormField;
          });
      }, */

        Newsletter_Trade: function () {
            $('.webform-submission-trade-newsletter-sign-up-form .webform-button--submit').click(function (e) {
                var Form = $('.webform-submission-trade-newsletter-sign-up-form');
                var FormField = peerless.commonFunctions.FormValidation(Form);
        if(!FormField){
         peerless.commonFunctions.ScrollTopPosition($(Form).find(".error:first"));
        }
                return FormField;
            });
        },

        init: function () {
            //peerless.commonFunctions.ContactUs();
            peerless.commonFunctions.Newsletter_Trade();
            peerless.commonFunctions.ScrollTopPosition();
            //peerless.commonFunctions.Newsletter();
        }

    }
    if($('.faq-term-title').length){
      $( ".faq-term-title" ).insertBefore( "#faq-search-form" );
    }
    $(document).ready(function () {
      peerless.commonFunctions.init();

      $('.file-upload-cont-first input[type="file"]').change(function(e){
        //alert(fileName);
        if(e.target.files[0] == '' || e.target.files[0] == undefined ){
          $(".file-upload-cont-first .file-select-name").text('');
          $(".file-upload-cont-first .file-select-name").removeClass('file-uploaded');
          $('.edit-product-image-1-file-validator').remove();

          if(fileError === false) {
            $('.product-registration-form .btn--register-primary').attr("disabled", false);
          }
        }
        else{
          var fileName = e.target.files[0].name;
          $(".file-upload-cont-first .file-select-name").text(fileName);
          $(".file-upload-cont-first .file-select-name").addClass('file-uploaded');
        }
      });

      $('.file-upload-cont-sec input[type="file"]').change(function(e){
        //alert(fileName);
        if(e.target.files[0] == '' || e.target.files[0] == undefined ){
          $(".file-upload-cont-sec .file-select-name").text('');
          $(".file-upload-cont-sec .file-select-name").removeClass('file-uploaded');
          $('.edit-product-image-2-file-validator').remove();
          if(fileError === false) {
            $('.product-registration-form .btn--register-primary').attr("disabled", false);
          }
        }
        else{
          var fileName = e.target.files[0].name;
          $(".file-upload-cont-sec .file-select-name").text(fileName);
          $(".file-upload-cont-sec .file-select-name").addClass('file-uploaded');
        }
      });

      $('.file-upload-cont-third input[type="file"]').change(function(e){
        if(e.target.files[0] == '' || e.target.files[0] == undefined ){
          $(".file-upload-cont-third .file-select-name").text('');
          $(".file-upload-cont-third .file-select-name").removeClass('file-uploaded');
          $('.edit-product-image-3-file-validator').remove();
          if(fileError === false) {
            $('.product-registration-form .btn--register-primary').attr("disabled", false);
          }
        }
        else{
          var fileName = e.target.files[0].name;
          $(".file-upload-cont-third .file-select-name").text(fileName);
          $(".file-upload-cont-third .file-select-name").addClass('file-uploaded');
        }
      });

      if ($(window).width() < 990) {
        $(".header-mega-menu__list__link").each(function(){
          if($(this).next().hasClass("header-mega-menu__list__sub-menu")) {
            $(this).removeAttr("href");
          }
        });

        $(".contact-info #edit-email").attr("placeholder", "Email");
        $(".form-item-email input").attr("placeholder", "Email");
        $(".contact-info #edit-phone").attr("placeholder", "Phone Number");
      }

    });
  $( window ).resize(function() {
      if ($(window).width() < 990) {
        $(".contact-info #edit-email").attr("placeholder", "Email");
        $(".form-item-email input").attr("placeholder", "Email");
        $(".contact-info #edit-phone").attr("placeholder", "Phone Number");

        $(".header-mega-menu__list__link").each(function(){
          if($(this).next().hasClass("header-mega-menu__list__sub-menu")) {
            $(this).removeAttr("href");
          }
        });
      }
    });

    if($('[data-custom-row="0"]').length){
      $('[data-custom-row="0"]').remove();
    }

    $('input#edit-name.form-search').keypress(function(e) {
      if(e.which == 10 || e.which == 13) {
        $(".details-wrapper #edit-adv-submit").trigger("click");
      }
  });

    //Mega menu hot fix for kitchen and bath - specially for toggle icon

   if ($("li.header-mega-menu__list.header-mega-menu__list--dropddown > a").length) {
        $("li.header-mega-menu__list.header-mega-menu__list--dropddown > a").each(function() {
            var getHref = $(this).attr("href");
            if (getHref != undefined && (getHref == '/bath' || getHref == '/kitchen')) {
              $(this).html("<span>" + $(this).text() +"</span>");

            }
        });
    }

//Homepage carousel
  $(document).ready(function() {
    $('.path-home-page .carousel').carousel({
      interval: 5000
    })
  });

  $('.collection-detail-page').on('click','.close',function() {
    $('.collection-detail-page #myModal').css('display','none');
  })



})(jQuery);

var iframe_id="wheretobuy"; // replace with id of the iframe to dynamically resize
var eventMethod = window.addEventListener ? "addEventListener" : "attachEvent";
var eventer = window[eventMethod];
var messageEvent = eventMethod == "attachEvent" ? "onmessage" : "message";
eventer(messageEvent,function(e) {
  if (e === messageEvent) {
    if(e.data.indexOf("w2gi:iframeHeightUpdated") !== -1) {
      var dimensions = e.data.split("//");
      autoResize(dimensions[1], dimensions[2]);
    }
    if(e.data.indexOf("w2gi:scrollPage") !== -1) {
      var offsets = document.getElementById(iframe_id).getBoundingClientRect();
      var values = e.data.split("//");
      var destination = values[1];
      var offset = values[2];
      window.scrollTo(0, destination + offsets.top);
    }
  }
},false);
function autoResize(newheight, newwidth){
  document.getElementById(iframe_id).style.height= parseInt(newheight) + 40 + "px";
}

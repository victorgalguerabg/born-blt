(function($, Drupal, drupalSettings) {
	Drupal.behaviors.brizodelta_login = {
		attach: function(context, settings) {
      $(document).ready(function() {
				// To Redirect previous URl from after login.
				// This URL stored in hidden field in Form.
        var referrer =  document.referrer;
        if($("#edit-previous-url").length) {
	if(referrer.search("user") != -1) {
		$("#edit-previous-url").val("/booking");
	}
	else {
		$("#edit-previous-url").val(referrer);
	}
        }
      });
	   }
	};
})(jQuery, Drupal);

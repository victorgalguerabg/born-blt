<?php

namespace Drupal\brizodelta_login\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\user_account_mgmt\UserAccountManagement;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\user\Entity\User;
use Drupal\Core\Ajax\HtmlCommand;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\user\UserStorageInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;

/**
 * Implementation of Checkout flow multi step Form.
 */
class BrizoDeltaUserLoginForm extends FormBase {

  /**
   * @var \Drupal\user_account_mgmt\UserAccountManagement
   */
  protected $userAccountMgmt;

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $userAccountFactory;

  /**
   * Symfony\Component\HttpFoundation\RequestStack definition.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Drupal\Core\Session\AccountProxyInterface
   */
  protected $user;

  /**
   * @var \Drupal\user\PrivateTempStoreFactory
   */
  protected $tempStoreFactory;

  /**
   * @var \Drupal\user\PrivateTempStoreFactory
   */
  protected $userSession;

  /**
   * Constructs a CheckoutForm object.
   */
  public function __construct(ConfigFactoryInterface $configFactory, UserAccountManagement $usermanagement, MessengerInterface $messenger, RequestStack $request_stack, AccountProxyInterface $currentUser, UserStorageInterface $user, PrivateTempStoreFactory $temp_store_factory) {
    $this->requestStack = $request_stack;
    $this->userAccountFactory = $configFactory->get('user_account_mgmt.user_account_mgmt_config');
    $this->userAccountMgmt = $usermanagement;
    $this->messenger = $messenger;
    $this->currentUser = $currentUser;
    $this->user = $user;
    $this->tempStoreFactory = $temp_store_factory;
    $this->userSession = $this->tempStoreFactory->get('loginData');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('user_account_mgmt.user_account_management'),
      $container->get('messenger'),
      $container->get('request_stack'),
      $container->get('current_user'),
      $container->get('entity_type.manager')->getStorage('user'),
      $container->get('tempstore.private')
    );
  }
  /**
   * return FormId
   */
  public function getFormId() {
    return 'brizodelta_login_form';
  }

  /**
   * Implementation checkout flow form build().
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['#attached']['library'][] = 'brizodelta_login/brizodelta_login';
    $form['username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Username/Email Address'),
      '#required' => TRUE,
      '#attributes' => [
        'class' => ['form-item__textfield form-item__textfield--required'],
      ],
    ];

    $form['password'] = [
      '#type' => 'password',
      '#required' => TRUE,
      '#title' => $this->t('Password'),
      '#attributes' => [
        'class' => array('form-item__textfield form-item__textfield--password form-item__textfield--required'),
        'autocomplete' => 'off',
      ],
    ];

    $form['persistent_login'] = [
      '#type' => 'checkbox',
      '#title' => \Drupal::config('persistent_login.settings')->get('login_form.field_label'),
      '#cache' => [
        'tags' => ['config:persistent_login.settings'],
      ],
      '#attributes' => [
        'class' => array('custom-input__native'),
      ],
      '#label_attributes' => ['class' =>  ['custom-input__label']],
    ];

    $form['actions']['login'] = [
      '#type' => 'submit',
      '#value' => $this->t('Login'),
      '#button_type' => 'primary',
      '#attributes' => [
        'class' => array('user-login-btn button--lg'),
      ],
      '#ajax' => [
        'callback' => '::validateUser',
        'event' => 'click',
      ],
    ];

    $form['previous_url'] = [
      '#type' => 'hidden',
      '#attributes' => [
        'id' => 'edit-previous-url',
      ],
    ];

    $form['#theme'] = 'form_brizodelta_user_login';
    return $form;
  }

  /**
   * Checkout Flow form Validation based on steps.
   *
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * Checkout flow form submit.
   * @param array $form
   * @param FormStateInterface $form_state
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   *
   * User Validation through API.
   * @param array $form
   * @param FormStateInterface $form_state
   */
  public function validateUser(array &$form, FormStateInterface $form_state) {
    $form_data =  $form_state->getUserInput();
    if(!empty($form_data['previous_url'])) {
      $urlList = explode("/", $form_data['previous_url']);
      if(!empty($urlList[3])) {
        $user_current_path = $form_data['previous_url'];
      }
      else {
        $user_current_path =  $this->requestStack->getCurrentRequest()->server->get('HTTP_REFERER');
      }
    }
    else {
      $user_current_path =  $this->requestStack->getCurrentRequest()->server->get('HTTP_REFERER');
    }

    $errCnt = 0;
    $errorMsg = "";
    if (empty($form_data['username'])) {
      $errCnt++;
      $errorMsg .= "<span class='login-error'>".$this->t("Please enter Username/Email Address")."</span>";
    }

    if (empty($form_data['password'])) {
      $errCnt++;
      $errorMsg .= "<span class='login-error'>".$this->t("Please enter Password")."</span>";
    }

    if ($errCnt == 0) {
      $userData = ["username" => $form_data['username'], "password" => $form_data['password']];
      $authResponse = $this->userAccountMgmt->userValidationApiCall(trim($this->userAccountFactory->get('userAuthApi')), "POST", $userData);
      $errorString = $this->userAccountFactory->get('validationErrorMessage');
      if(strlen(trim($errorString)) == 0){
        $errorString = 'Invalid username/password';
      }
      // If API Returns success, Load the user in Drupal.
      if (isset($authResponse['success']) && !empty($authResponse['success'])) {
        // User load based on Username Only
        $account = user_load_by_name($form_data['username']);

        //If user Exists, Ennable Drupal Session By Username.
        if($account) {
          user_login_finalize($account);

          //Ennabling persistant login.
          if ($form_state->getValue('persistent_login', FALSE)) {
            \Drupal::service('persistent_login.token_handler')->setNewSessionToken($this->currentUser->id());
          }

          // Update user description for existing user
          $userDescription = $account->field_group_description->value;
          $userId = $account->field_user_id->value;

          // If User Description and User ID is Updating.
          if($userDescription == '' || $userId == '') {
            $userDetailsurl = trim($this->userAccountFactory->get('userValidateApi')) . $form_data['username'];
            $userDetailsRequest = $this->userAccountMgmt->userValidationApiCall($userDetailsurl, 'GET', []);
            if (isset($userDetailsRequest['userID'])) {
              $account->set('field_group_description', $userDetailsRequest['groupDescription']);
              $account->set('field_user_id', $userDetailsRequest['userID']);
              $account->save();
            }
          }

          // User Based session storage
          $this->userSession->set('currentUserGRPDescription', $userDescription);
          $this->userSession->set('currentUserId', $userId);

          // Check Existing Roles.
          $userExistsDetailsurl = trim($this->userAccountFactory->get('userValidateApi')) . $form_data['username'];
          $userExistsDetailsRequest = $this->userAccountMgmt->userValidationApiCall($userExistsDetailsurl, 'GET', []);
          if (isset($userExistsDetailsRequest['userID'])) {

            // Generate roles.
            $existsRoles = $this->userAccountMgmt->generateRoles($userExistsDetailsRequest['roles']);

            // Get the roles to be assigned to the user.
            $existsGetUserRoles = [];
            if ($existsRoles) {
              $existsGetUserRoles = $this->userAccountMgmt->getRoles($userExistsDetailsRequest['roles']);
              $userLoad = $this->user->load($this->currentUser->id());
              foreach ($existsGetUserRoles as $roleName) {
                if($roleName != 'authenticated') {
                  $userLoad->addRole($roleName);
                  $userLoad->save();
                }
              }
            }
          }

          //Page Redirect to previous URL.
          $response = new AjaxResponse();
          $random = substr(md5(mt_rand()), 0, 10);
          $response->addCommand(
            new RedirectCommand($user_current_path . '?' . $random)
          );
          return $response;
        }
        else {
          // Get user details by user Name.
          $userDetailsurl = trim($this->userAccountFactory->get('userValidateApi')) . $form_data['username'];
          $userDetailsRequest = $this->userAccountMgmt->userValidationApiCall($userDetailsurl, 'GET', []);

          if (isset($userDetailsRequest['userID'])) {
            // Generate roles.
            $roles = $this->userAccountMgmt->generateRoles($userDetailsRequest['roles']);

            // Get the roles to be assigned to the user.
            $getUserRoles = [];
            if ($roles) {
              $getUserRoles = $this->userAccountMgmt->getRoles($userDetailsRequest['roles']);
            }
            // New User Create.
            $userCreate = User::create([
              'name' => $form_data['username'],
              'password' => user_password(),
              'mail' => $userDetailsRequest['email'],
              'roles' => $getUserRoles,
              'status' => 1,
            ]);
            $userCreate->set('field_first_name', $userDetailsRequest['firstName']);
            $userCreate->set('field_last_name', $userDetailsRequest['lastName']);
            $userCreate->set('field_user_id', $userDetailsRequest['userID']);
            $userCreate->set('field_group_description', $userDetailsRequest['groupDescription']);
            $savedUser = $userCreate->save();

            // If user created successfully, The Ennable Drupal Session.
            if ($savedUser) {
              // User Load by username Only.
              $userAccount = user_load_by_name($form_data['username']);
              // Enable drupal session.
              user_login_finalize($userAccount);

              // User Based session storage
              $this->userSession->set('currentUserGRPDescription', $userDetailsRequest['groupDescription']);
              $this->userSession->set('currentUserId', $userDetailsRequest['userID']);

              // Ennable persist login.
              if ($form_state->getValue('persistent_login', FALSE)) {
                \Drupal::service('persistent_login.token_handler')->setNewSessionToken($this->currentUser->id());
              }

              //Page Redirect to Previous URL.
              $response = new AjaxResponse();
              $random = substr(md5(mt_rand()), 0, 10);
              $response->addCommand(
                new RedirectCommand($user_current_path . '?' . $random)
              );
              return $response;
            }
          }
          else {
            $msg = "<label class='login-error'>" . $errorString . "</label>";
            return (new AjaxResponse())->addCommand(
              new HtmlCommand('.user-login-error', $msg)
            );
          }
        }
      }
      else {
        $msg = "<label class='login-error'>" . $errorString . "</label>";
        return (new AjaxResponse())->addCommand(
          new HtmlCommand('.user-login-error', $msg)
        );
      }
    }
    else {
      return (new AjaxResponse())->addCommand(
        new HtmlCommand('.user-login-error', $errorMsg)
      );
    }
  }

}

<?php

/**
 * @file
 * Drupal site-specific configuration file override
 * Used by Delta AWS CI deployment scripts
 *
 * Copy to /sites/[site]/settings/local.settings.php
 */

use Drupal\Component\Assertion\Handle;
use Composer\Autoload\ClassLoader;

$databases['default']['default'] = array(
    'database' => 'BORNSQLDATA',
    'username' => 'BORNSQLUSER',
    'password' => 'BORNSQLPASS',
    'host' => 'BORNSQLHOST',
    'driver' => 'mysql',
    'prefix' => 'BORNSQLPREF_',
);



// Check for PHP Memcached libraries.
$memcache_exists = class_exists('Memcache', FALSE);
$memcached_exists = class_exists('Memcached', FALSE);
$memcache_services_yml = DRUPAL_ROOT . '/modules/contrib/memcache/memcache.services.yml';
$memcache_module_is_present = file_exists($memcache_services_yml);
if ($memcache_module_is_present && ($memcache_exists || $memcached_exists)) {


  if (class_exists(ClassLoader::class)) {

    // Configure memcache servers
    $settings['memcache']['servers'] = ['BORNMEMHOST:11211' => 'default'];
    $settings['memcache']['bins'] = ['default' => 'default'];
    $settings['memcache']['key_prefix'] = 'BORNSQLPREF';
    //$settings['cache']['default'] = 'cache.backend.memcache';
    //$settings['cache']['bins']['render'] = 'cache.backend.memcache';

    // Use Memcached extension if available.
    if ($memcached_exists) {
    $settings['memcache']['extension'] = 'Memcached';
    }

    $class_loader = new ClassLoader();
    $class_loader->addPsr4('Drupal\\memcache\\', DRUPAL_ROOT . '/modules/contrib/memcache/src');
    $class_loader->register();
    $settings['container_yamls'][] = $memcache_services_yml;

    // Acquia Default Settings for the memcache module
    // Default settings for the Memcache module.
    // Enable compression for PHP 7.
    $settings['memcache']['options'][Memcached::OPT_COMPRESSION] = TRUE;

    // Set key_prefix to avoid drush cr flushing all bins on multisite.
    $settings['memcache']['key_prefix'] = 'BORNSQLPREF';

    // Settings for SASL Authenticated Memcached.
    //$settings['memcache']['options'][Memcached::OPT_BINARY_PROTOCOL] = TRUE;

    // Bootstrap cache.container with memcache rather than database.
    $settings['bootstrap_container_definition'] = [
      'parameters' => [],
      'services' => [
        'database' => [
          'class' => 'Drupal\Core\Database\Connection',
          'factory' => 'Drupal\Core\Database\Database::getConnection',
          'arguments' => ['default'],
        ],
        'settings' => [
          'class' => 'Drupal\Core\Site\Settings',
          'factory' => 'Drupal\Core\Site\Settings::getInstance',
        ],
        'memcache.settings' => [
          'class' => 'Drupal\memcache\MemcacheSettings',
          'arguments' => ['@settings'],
        ],
        'memcache.factory' => [
          'class' => 'Drupal\memcache\Driver\MemcacheDriverFactory',
          'arguments' => ['@memcache.settings'],
        ],
        'memcache.timestamp.invalidator.bin' => [
          'class' => 'Drupal\memcache\Invalidator\MemcacheTimestampInvalidator',
          'arguments' => ['@memcache.factory', 'memcache_bin_timestamps', 0.001],
        ],
        'memcache.backend.cache.container' => [
          'class' => 'Drupal\memcache\DrupalMemcacheInterface',
          'factory' => ['@memcache.factory', 'get'],
          'arguments' => ['container'],
        ],
        'cache_tags_provider.container' => [
          'class' => 'Drupal\Core\Cache\DatabaseCacheTagsChecksum',
          'arguments' => ['@database'],
        ],
        'cache.container' => [
          'class' => 'Drupal\memcache\MemcacheBackend',
          'arguments' => [
            'container',
            '@memcache.backend.cache.container',
            '@cache_tags_provider.container',
            '@memcache.timestamp.invalidator.bin',
            '@memcache.settings',
          ],
        ],
      ],
    ];

    // Use memcache for bootstrap, discovery, config instead of fast chained
    // backend to properly invalidate caches on multiple webs.
    // See https://www.drupal.org/node/2754947
    $settings['cache']['bins']['bootstrap'] = 'cache.backend.memcache';
    $settings['cache']['bins']['discovery'] = 'cache.backend.memcache';
    $settings['cache']['bins']['config'] = 'cache.backend.memcache';

    // Use memcache as the default bin.
    $settings['cache']['default'] = 'cache.backend.memcache';

  }

  //$settings['memcache']['stampede_protection'] = TRUE;
 // $settings['container_yamls'][] = 'sites/default/memcache.yml';

}

$config['search_api.server.BORNSQLPREF_search'] = [
  'backend_config' => [
    'host' => 'BORNSLRHOST',
    'col' => 'BORNSLRCOLS',
  ],
];

$settings['reverse_proxy'] = TRUE;
$settings['reverse_proxy_addresses'] = array('10.50.16.0/20');

// Allow access to update.php for anonymous user.
$settings['update_free_access'] = FALSE;

$settings["file_temp_path"] = "sites/BORNSQLPREF/files/tmp";

// Allow access from our internal Drupal sites
$settings['trusted_host_patterns'] = array(
 'BORNHCKHOST',
 'BORNHCKIPv4',
 '^deltafaucet\.com$',
 '^.+\.deltafaucet\.com$',
  '^peerlessfaucet\.com$',
 '^.+\.peerlessfaucet\.com$',
  '^brizo\.com$',
 '^.+\.brizo\.com$',
   '^brizofaucet\.ca$',
 '^.+\.brizofaucet\.ca$',
  '^firstwavelab\.com$',
  '^.+\.firstwavelab\.com$',
  '^deltafaucetcompany\.com$',
  '^.+\.deltafaucetcompany\.com$',
  '^brizoanddelta\.com$',
  '^.+\.brizoanddelta\.com$',
  '^discoverdfc\.com$',
  '^.+\.discoverdfc\.com$',
  '^amazonaws\.com$',
 '^.+\.amazonaws\.com$',
);

<?php

namespace Drupal\sync\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\ConfigFormBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Configure settings for this site.
 */
class SyncSettingsForm extends ConfigFormBase implements ContainerInjectionInterface {

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The Config storage.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configfactory;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configfactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
          $container->get('config.factory')
      );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'sync_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['webservices_commerce.api_settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('webservices_commerce.api_settings');

    $form['category_call_api'] = [
      '#type' => 'details',
      '#title' => $this->t('Category Related Calls'),
      '#open' => TRUE,
      '#weight' => 2,
    ];
    $form['category_call_api']['category_api'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Category Sync API'),
      '#default_value' => $config->get('rest_api_settings.category_api'),
      '#required' => TRUE,
      '#size' => 100,
    ];
    $form['category_call_api']['term_filter'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Name of the term to filter ex:Delta'),
      '#default_value' => empty($config->get('rest_api_settings.term_filter')) ? 'Delta' : $config->get('rest_api_settings.term_filter'),
      '#size' => 100,
    ];

    $form['category_call_api']['taxonomy_depth'] = [
      '#type' => 'select',
      '#title' => $this->t('Product category sync depth'),
      '#default_value' => $config->get('rest_api_settings.taxonomy_depth'),
      '#options' => ['1' => 1, '2' => 2, '3' => 3],
      '#default_value' => empty($config->get('rest_api_settings.taxonomy_depth')) ? 3 : $config->get('rest_api_settings.taxonomy_depth'),
      '#description' => $this->t('1:Parent Terms, 2: Child Terms, 3:Sibling Terms'),
    ];
    $form['facet_call_api'] = [
      '#type' => 'details',
      '#title' => $this->t('Facet Related Calls'),
      '#open' => TRUE,
      '#weight' => 2,
    ];
    $form['facet_call_api']['facet_api'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Facet Sync API'),
      '#default_value' => $config->get('rest_api_settings.facet_api'),
      '#required' => TRUE,
      '#size' => 100,
    ];
    $form['facet_call_api']['facet_size'] = [
      '#type' => 'number',
      '#title' => $this->t('Facet Size'),
      '#default_value' => $config->get('rest_api_settings.facet_size'),
      '#required' => TRUE,
      '#size' => 100,
    ];
    $form['product_call_api'] = [
      '#type' => 'details',
      '#title' => $this->t('Product Related Calls'),
      '#open' => TRUE,
      '#weight' => 3,
    ];
    $form['product_call_api']['product_website'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Name of the website'),
      '#default_value' => empty($config->get('rest_api_settings.product_website')) ? 'WebsiteUS' : $config->get('rest_api_settings.product_website'),
      '#size' => 100,
    ];
    $form['product_call_api']['product_fetch_category'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Product Fetch Category'),
      '#default_value' => $config->get('rest_api_settings.product_fetch_category'),
      '#required' => TRUE,
      '#size' => 200,
    ];

    $form['product_call_api']['product_by_sku'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Fetch Product By SKU'),
      '#default_value' => $config->get('rest_api_settings.product_by_sku'),
      '#required' => TRUE,
      '#size' => 200,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('webservices_commerce.api_settings')
      ->set('rest_api_settings.product_fetch_category', $form_state->getValue('product_fetch_category'))
      ->set('rest_api_settings.category_api', $form_state->getValue('category_api'))
      ->set('rest_api_settings.product_by_sku', $form_state->getValue('product_by_sku'))
      ->set('rest_api_settings.facet_api', $form_state->getValue('facet_api'))
      ->set('rest_api_settings.facet_size', $form_state->getValue('facet_size'))
      ->set('rest_api_settings.taxonomy_depth', $form_state->getValue('taxonomy_depth'))
      ->set('rest_api_settings.product_website', $form_state->getValue('product_website'))
      ->set('rest_api_settings.term_filter', $form_state->getValue('term_filter'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}

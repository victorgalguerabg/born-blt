<?php

namespace Drupal\sync_category\Controller;

use Drupal;
use Drupal\Core\Controller\ControllerBase;
use Drupal\taxonomy\Entity\Term;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use stdClass;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;

/**
 * Category import controller.
 */
class CategoryImportController extends ControllerBase {

  const PRODUCTVID = 'product_category';

  /**
   * Taxonomy property.
   *
   * @var taxonomyEntity
   */
  static private $taxonomyEntity;
  /**
   * Success message property.
   *
   * @var successMessage
   */
  static private $successMessage;
  /**
   * Failure message property.
   *
   * @var FailureMessage
   */
  static private $FailureMessage;
  /**
   * Drupal\Core\Logger\LoggerChannelFactoryInterface definition.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactorXyInterface
   */
  protected $logger;

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config;

  /**
   * Drupal\Core\Messenger\MessengerInterface definition.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Class constructor.
   */
  public function __construct(LoggerChannelFactoryInterface $logger,
                              ConfigFactoryInterface $config_factory,
                              MessengerInterface $messenger) {
    $this->logger = $logger;
    $this->config = $config_factory;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
      // Load the service required to construct this class.
      $container->get('logger.factory'),
      $container->get('config.factory'),
      $container->get('messenger')
    );
  }

  /**
   * To print category import success message.
   */
  final public static function getImportSuccessMessage() {
    if (!isset(self::$successMessage)) {
      self::$successMessage = t('Category sync successfully completed.');
    }
    return self::$successMessage;
  }

  /**
   * To print category import fail message.
   */
  final public static function getImportFailureMessage() {
    if (!isset(self::$FailureMessage)) {
      self::$FailureMessage = t('Finished with an error.');
    }
    return self::$FailureMessage;
  }

  /**
   * Get term entity object.
   */
  final public static function gettaxonomyEntity() {
    if (!isset(self::$taxonomyEntity)) {
      self::$taxonomyEntity = Drupal::entityTypeManager()->getStorage('taxonomy_term');
    }
    return self::$taxonomyEntity;
  }

  /**
   * Description: Created menu call back will hit category function.
   */
  public function category(LoggerChannelFactoryInterface $logger) {
    $message = self::drushCategoryImport();
    $this->logger->get('sync_category')->error(
            '@message', [
              '@message' => $message,
            ]
    );
    exit;
  }

  /**
   * Implementation of categoryImport.
   */
  public static function categoryImport($results) {
    // Get api info.
    \Drupal::config('webservices_commerce.api_settings');
    self::getDataExtract($results);
  }

  /**
   * Implementation of isTermExists.
   */
  public static function isTermExists($catName, $vid = 'product_category') {
    $query = Drupal::entityQuery('taxonomy_term');
    $query->condition('vid', $vid);
    $query->condition('name', $catName);
    $tids = $query->execute();
    if (!empty($tids)) {
      return current($tids);
    }
    return $tids;
  }

  /**
   * Implementation of taxonomyTermCreate.
   */
  public static function taxonomyTermCreate($term, $vocabulary, $weight) {
    $cat_path = (!empty($term->category_path)) ? $term->category_path : '';
    $parent = (!empty($term->parent)) ? $term->parent : [];
    $terms = [];
    if (!empty($cat_path)) {
      $terms['field_category_path'] = ['value' => $cat_path];
    }
    $terms += [
      'name' => $term->id,
      'description' => $term->name,
      'parent' => $parent,
      'vid' => $vocabulary,
      'weight' => [$weight],
    ];
    $new_term = Term::create($terms);
    // Save the taxonomy term.
    $new_term->save();
    return $new_term->id();
  }

  /**
   * Implementation of taxonomyTermUpdate.
   */
  public static function taxonomyTermUpdate($updateterm, $vocabulary, $weight) {
    $cat_path = (!empty($updateterm->category_path)) ? $updateterm->category_path : '';
    $cat_description = (!empty($updateterm->name)) ? $updateterm->name : '';
    $parent = (!empty($updateterm->parent)) ? $updateterm->parent : [];
    $term = Term::load($vocabulary);
    $term->weight->setValue($weight);
    $term->name->setValue($updateterm->id);
    if (empty($updateterm->parent)) {
      $term->parent->setValue($parent);
    }
    if (!empty($cat_path)) {
      $term->field_category_path->setValue($cat_path);
    }
    $term->description->setValue($cat_description);
    // TO do add code to validate image URL path.
    $term->save();
  }

  /**
   * Implementation of getDataExtract.
   */
  public static function getDataExtract($results) {
    $term_names = [];
    if (!empty($results)) {
      $weight = 0;
      foreach ($results as $get_category) {
        $category = new stdClass();
        $category->id = $get_category['id'];
        $category->name = $get_category['name'];
        $category->category_path = $get_category['category_path'];
        if (!empty($get_category['parent'])) {
          $parent_id = self::isTermExists($get_category['parent']);
          $category->parent = $parent_id;
        }
        $tid = self::isTermExists($category->id);

        // Lets create term if not exist.
        if (empty($tid)) {
          $tid = self:: taxonomyTermCreate($category, self::PRODUCTVID, $weight);
        }
        else {
          // If exist, check term id with parent id.
          $get_terms = get_term_parent_details($tid);

          $update_flag = TRUE;
          if (!empty($get_terms) && isset($get_terms[1][0]) && $get_terms[1][0] == $parent_id) {
            self::taxonomyTermUpdate($category, $tid, $weight);
            $update_flag = FALSE;
          }
          if ($update_flag == TRUE) {
            $tid = self:: taxonomyTermCreate($category, self::PRODUCTVID, $weight);
          }
        }
        $term_names[$tid] = $category->name;
        $weight++;
      }
      return $term_names;
    }
    return $term_names;
  }

  /**
   * Implementation of facetImport.
   */
  public static function facetImport($terms) {
    $tid = self::isTermExists($terms->id, $terms->vid);
    if (empty($tid)) {
      $tid = self:: taxonomyTermCreate($terms, $terms->vid, 0);
    }
    else {
      self::taxonomyTermUpdate($terms, $tid, 0);
    }
  }

  /**
   * Implementation of categoryImportFinishedCallback.
   */
  public static function categoryImportFinishedCallback($success, $results, $operations) {
    if ($success) {
      $message = t('Category sync successfully completed.');
    }
    else {
      $message = t('Finished with an error.');
    }
    \Drupal::messenger()->addStatus($message);
  }

  /**
   * Implementation of facetImportFinishedCallback.
   */
  public static function facetImportFinishedCallback($success, $results, $operations) {
    if ($success) {
      $message = t('Facet sync successfully completed.');
    }
    else {
      $message = t('Finished with an error.');
    }
    \Drupal::messenger()->addStatus($message);
  }

}

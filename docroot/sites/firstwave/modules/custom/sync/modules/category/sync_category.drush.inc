<?php

/**
 * @file
 * Contains the code to generate the custom drush commands.
 */

use Drupal\sync_category\Controller\CategoryImportController;

/**
 * Implements hook_drush_command().
 */
function sync_category_drush_command() {
  $items = [];
  $items['import-category'] = [
    'description' => 'drush import-category',
    'drupal dependencies' => ['sync_category'],
    'aliases' => ['import:category'],
  ];

  return $items;
}

/**
 * Call back function drush_sync_category_import_category()
 */
function drush_sync_category_import_category() {
  $response = CategoryImportController::drushCategoryImport();
  drush_print($response);
}

<?php

namespace Drupal\sync_category;

use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\taxonomy\Entity\Vocabulary;
use stdClass;

use Drupal\sync_category\Controller\CategoryImportController;

/**
 * Class CategoryImportService.
 */
class CategoryImportService {

  /**
   * To get current query string.
   *
   * @var Symfony\Component\HttpFoundation\RequestStack
   */
  protected $request;

  /**
   * Implementation of EntityTypeManagerInterface.
   *
   * @var Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entitytypemanager;

  /**
   * Drupal\Core\Logger\LoggerChannelFactoryInterface definition.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactorXyInterface
   */
  protected $logger;

  /**
   * Drupal\Core\Messenger\MessengerInterface definition.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Class constructor.
   */
  public function __construct(RequestStack $request,
                                EntityTypeManagerInterface $entitytypemanager,
                                LoggerChannelFactoryInterface $logger,
                                MessengerInterface $messenger,
                                ConfigFactoryInterface $config_factory) {
    $this->request = $request;
    $this->entitytypemanager = $entitytypemanager;
    $this->logger = $logger;
    $this->messenger = $messenger;
    $this->configFactory = $config_factory;
  }

  /**
   * Create commerce category.
   */
  public function createCommerceCategory() {
    $delta_config = $this->configFactory->get('delta_services.deltaservicesconfig');
    $sync_config = $this->configFactory->get('webservices_commerce.api_settings');
    $domain = $delta_config->get('api_base_path');
    $term_depth = $sync_config->get('rest_api_settings.taxonomy_depth');
    $term_filter = $sync_config->get('rest_api_settings.term_filter');
    if (empty($term_filter)) {
      $message = $this->t('Category import failed, Please add prefix name on the CMS backend');
      $this->messenger->addWarning($message);
      $this->logger->get('category_import')->error(
            '@message', [
              '@message' => $message,
            ]
        );
      return FALSE;
    }
    $url = $domain . $sync_config->get('rest_api_settings.category_api');
    $method = 'GET';
    $headers = add_headers(FALSE);
    $data = '';

    $response = web_services_api_call($url, $method, $headers, $data, 'CATEGORY-SYNC');

    if ($response['status'] == 'SUCCESS') {
      $results = json_decode($response['response']);

      if (!empty($results)) {
        $category_list = [];
        foreach ($results as $val) {
          $category_path = $val->categoryPath;
          $split_category_path = explode("_", $category_path);

          if ($split_category_path[0] == $term_filter) {
            if (!empty($split_category_path[1])) {
              $parent_category = $split_category_path[1];
              $cat_path = $term_filter . '_' . $parent_category;
              $unique_key = str_replace(['/', ' '], "_", $cat_path);
              $category_list[$unique_key] = [
                'id' => $parent_category,
                'name' => $parent_category,
                'category_path' => $cat_path,
              ];
            }
            if (!empty($split_category_path[2]) && $term_depth > 1) {
              $child_category = $split_category_path[2];
              $parent_category = $split_category_path[1];
              $cat_path = $term_filter . '_' . $parent_category . '_' . $child_category;
              $unique_key = str_replace(['/', ' '], "_", $cat_path);
              $category_list[$unique_key] = [
                'id' => $child_category,
                'name' => $child_category,
                'parent' => $parent_category,
                'category_path' => $cat_path,
              ];
            }
            if (!empty($split_category_path[3]) && $term_depth > 2) {
              $sibling_category = $split_category_path[3];
              $child_category = $split_category_path[2];
              $parent_category = $split_category_path[1];
              $cat_path = $term_filter . '_' . $parent_category . '_' . $child_category . '_' . $sibling_category;
              $unique_key = str_replace(['/', ' '], "_", $cat_path);
              $category_list[$unique_key] = [
                'id' => $sibling_category,
                'name' => $sibling_category,
                'parent' => $child_category,
                'category_path' => $cat_path,
              ];
            }
          }
        }
        if (empty($category_list)) {
          $message = $this->t(
                'There is no category exist with the prefix : @term_filter',
                ['@term_filter' => $term_filter]
            );
          $this->messenger->addWarning($message);
          $this->logger->get('category_import')->error(
                '@message', [
                  '@message' => $message,
                ]
            );
          return FALSE;
        }

        // To run without batch support.
        $operations[] = [
          '\Drupal\sync_category\Controller\CategoryImportController::categoryImport',
              [$category_list],
        ];
        $batch = [
          'title' => 'Import Category...',
          'operations' => $operations,
          'finished' => '\Drupal\sync_category\Controller\CategoryImportController::categoryImportFinishedCallback',
        ];
        // To run with batch support. uncomment.
        batch_set($batch);
      }
    }
    else {
      $message = $this->t('Category import failed');
      $this->messenger->addWarning($message);
      $this->logger->get('category_import')->error(
            '@message', [
              '@message' => $message,
            ]
        );
    }
  }

  /**
   * Deletes commerce category.
   */
  public function deleteCommerceCategory() {
    $result = $this->entitytypemanager->getStorage('taxonomy_term')->getQuery()->condition('vid', 'product_category')
      ->execute();
    entity_delete_multiple('taxonomy_term', $result);
    $message = 'Category deleted successfully';
    $this->messenger->addMessage($message);
    $this->logger->get('category_delete')->error(
          '@message', [
            '@message' => $message,
          ]
      );

  }

  /**
   * Imports facet details.
   */
  public function facetInsert() {
    $delta_config = $this->configFactory->get('delta_services.deltaservicesconfig');
    $sync_config = $this->configFactory->get('webservices_commerce.api_settings');
    $domain = $delta_config->get('api_base_path');
    $facetapi = $sync_config->get('rest_api_settings.facet_api');
    $facet_size = $sync_config->get('rest_api_settings.facet_size');
    $siteUrl = str_replace(['{facetSize}'], [$facet_size], $domain . $facetapi);
    $method = 'GET';
    $headers = add_headers(FALSE);
    $data = '';
    $response = web_services_api_call($siteUrl, $method, $headers, $data, 'FACET-SYNC');
    if ($response['status'] == 'SUCCESS') {
      $results = json_decode($response['response']);
      if (!empty($results->facets)) {
        foreach ($results->facets as $key => $facet_obj) {
          if ($facet_obj->type == 'term' || $facet_obj->type == 'range') {
            $vid = $facet_obj->name;
            $vid = trim($vid);
            $name = $facet_obj->name;
            $vocabularies = Vocabulary::loadMultiple();
            $listVid = [];
            foreach ($vocabularies as $key => $vocObj) {
              $listVid[] = strtolower($key);
            }
            if (!in_array(strtolower($vid), $listVid) && count($listVid) > 0) {
              $vocabulary = Vocabulary::create(
                    [
                      'vid' => strtolower($vid),
                      'machine_name' => strtolower($vid),
                      'description' => $name,
                      'name' => strtolower($name),
                    ]
                );
              $vocabulary->save();
            }
            if ($facet_obj->type == 'range') {
              foreach ($facet_obj->ranges as $term) {
                $name = $term->from . '-' . $term->to;
                $facet_list = new stdClass();
                $facet_list->id = $name;
                $facet_list->name = $name;
                $facet_list->vid = strtolower($vid);
                CategoryImportController::facetImport($facet_list);
                $operations[] = [
                  '\Drupal\sync_category\Controller\CategoryImportController::facetImport',
                      [$facet_list],
                ];
              }
            }
            else {
              foreach ($facet_obj->terms as $term) {
                $name = $term->term;
                $facet_list = new stdClass();
                $facet_list->id = $name;
                $facet_list->name = $name;
                $facet_list->vid = strtolower($vid);
                CategoryImportController::facetImport($facet_list);
                $operations[] = [
                  '\Drupal\sync_category\Controller\CategoryImportController::facetImport',
                      [$facet_list],
                ];
              }
            }
          }
        }
      }
      $batch = [
        'title' => 'Facet import...',
        'operations' => $operations,
        'finished' => '\Drupal\sync_category\Controller\CategoryImportController::facetImportFinishedCallback',
      ];
      // To run with batch support. uncomment.
      batch_set($batch);
    }
  }

}

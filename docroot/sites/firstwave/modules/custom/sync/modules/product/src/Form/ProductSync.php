<?php

namespace Drupal\sync_product\Form;

use Drupal;
use Drupal\sync_product\Controller\ProductImportController;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\sync_product\ImportProductService;
use Symfony\Component\PropertyAccess\PropertyAccess;

/**
 * Defines the Product sync form.
 */
class ProductSync extends FormBase {

  /**
   * Implementation of EntityTypeManagerInterface.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entitytypemanager;
  protected $logger;

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Configuration Factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $customConfigFactory;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'product_sync_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['sync_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Import Products'),
      '#required' => TRUE,
      '#options' => [
        'all' => 'All Products',
      ],
      '#default_value' => ['all'],
    ];
    // 'specific' => 'Specific Products',.
    $form['product_sku'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Enter product sku. If multiple, enter comma seperated.'),
      '#states' => [
        'visible' => [
          ':input[name="sync_type"]' => ['value' => 'specific'],
        ],
        'required' => [
          ':input[name="sync_type"]' => ['value' => 'specific'],
        ],
      ],
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Import'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    ImportProductService::createCommerceProduct();
    /*$syncType = $form_state->getValue('sync_type');
    $operations = [];
    $webservice_config = $this->config('webservices_commerce.api_settings');
    $domain = $webservice_config->get('rest_api_settings.wcs_rest_api');
    $operations = $productSkus = [];
    if ($syncType == 'all') {
    $batch = $this->syncAll($webservice_config);
    // To run with batch support. uncomment.
    batch_set($batch);
    }
    elseif ($syncType == 'specific') {
    $post_sku = $form_state->getValue('product_sku');
    $productSkus = array_map("trim", explode(",", $post_sku));
    $totalProducts = count($productSkus);
    if ($totalProducts <= 0) {
    return;
    }
    }
    $totalProducts = count($productSkus);
    if ($totalProducts <= 0) {
    return;
    }
    foreach ($productSkus as $productSku) {
    $webservice_config = $this->config('webservices_commerce.api_settings');
    $domain = $webservice_config
    ->get('rest_api_settings.wcs_rest_api');
    $product_url = $domain . $webservice_config
    ->get('rest_api_settings.product_details_by_sku');
    $siteUrl = str_replace(['{productSku}'], [$productSku], $product_url);
    $batchArray = ['syncType' => 'Manual-All', 'productUrl' => $siteUrl];
    $operations[] = [
    '\Drupal\sync_product\Controller\ProductImportController::productImportAll',
    [$batchArray],
    ];
    }
    $batch = [
    'title' => $this->t('Import Products...'),
    'operations' => $operations,
    'finished' => '\Drupal\sync_product\Controller\ProductImportController::productImportFinished',
    ];
    // To run with batch support. uncomment.
    batch_set($batch);*/
  }

  /**
   * Implementation of sync().
   */
  public function syncAll($webservice_config) {

    $vid = 'product_category';
    $terms = (array) $this->entitytypemanager->getStorage('taxonomy_term')->loadTree($vid);

    foreach ($terms as $term) {
      $term_cat_obj = $this->entitytypemanager->getStorage('taxonomy_term')->load($term->tid);
      $category_path = (!empty($term_cat_obj->get('field_category_path')->value)) ? $term_cat_obj->get('field_category_path')->value : '';
      if (empty($category_path)) {
        continue;
      }
      $category_path_parts = explode("_", $category_path);
      $search_string = [];
      if (count($category_path_parts)) {
        foreach ($category_path_parts as $parts) {
          $search_string[] = $parts;
          if ($parts == $term->name) {
            break;
          }
        }
      }
      $pattern = implode('_', $search_string);
      $ancestors = (array) $this->entitytypemanager->getStorage('taxonomy_term')->loadAllParents($term->tid);
      $list = [];
      foreach ($ancestors as $term) {
        $list[$term->id()] = $term->id();
      }
      $term_data[] = [
        "term" => $list,
        "pattern" => $pattern,
      ];
    }
    $operations = [];
    $domain = $webservice_config->get('rest_api_settings.wcs_rest_api');
    $product_search_url = $domain . $webservice_config->get('rest_api_settings.product_fetch_category');
    if (count($term_data)) {
      foreach ($term_data as $term_arr) {
        $operations[] = [
          '\Drupal\sync_product\Controller\ProductImportController::productImportAll',
          [$term_arr, $product_search_url],
        ];

      }
      return [
        'title' => $this->t('Import Products...'),
        'operations' => $operations,
        'finished' => '\Drupal\sync_product\Controller\ProductImportController::productImportFinished',
      ];
    }
    else {
      $message = $this->t("Product import failed.");
      drupal_set_message($message, 'warning');
      $this->logger->get('product_sync')->error(
            '@message', [
              '@message' => $message,
            ]
        );
    }

  }

  /**
   * Implementation of getProductData.
   */
  public static function getProductData($accessor, $dataObj, $key) {

    if ($accessor->isReadable($dataObj, $key)) {
      return $accessor->getValue($dataObj, $key);
    }
    return FALSE;
  }

  /**
   * Import product via cli.
   */
  public static function drushProductImport($param = NULL) {
    // $accessor = PropertyAccess::createPropertyAccessor();
    $webservice_config = Drupal::config('webservices_commerce.api_settings');
    $productSkus = [];
    if (empty($param)) {
      return self::syncAllDrush($webservice_config);
    }
    $productSkus = array_map("trim", explode(",", $param));
    $totalProducts = count($productSkus);
    if ($totalProducts <= 0) {
      return t('Product import failed');
    }

    foreach ($productSkus as $productSku) {
      $webservice_config = Drupal::config('webservices_commerce.api_settings');
      $domain = $webservice_config->get('rest_api_settings.wcs_rest_api');
      $product_url = $domain . $webservice_config->get('rest_api_settings.product_details_by_sku');
      $siteUrl = str_replace(['{productSku}'], [$productSku], $product_url);
      $batchArray = ['syncType' => 'Manual-All', 'productUrl' => $siteUrl];
      ProductImportController::productImportAll($batchArray);
    }
    return t('Product import completed successfully');
  }

  /**
   * Imlementation of syncAllDrush.
   */
  public static function syncAllDrush($webservice_config) {
    $accessor = PropertyAccess::createPropertyAccessor();
    $domain = $webservice_config->get('rest_api_settings.wcs_rest_api');
    $product_url = $domain . $webservice_config->get('rest_api_settings.product_details_by_sku');
    $product_search_url = $domain . $webservice_config->get('rest_api_settings.product_listing_page');
    $method = 'GET';
    $headers = add_headers(FALSE);
    $data = $results = [];
    $response = web_services_api_call($product_search_url, $method, $headers, $data, 'PRODUCT-IMPORT');
    $message = t('Product import failed');
    if ($response['status'] == 'SUCCESS') {
      $results = json_decode($response['response']);
      // For testing purpose
      // $results->products = array_slice($results->products, 0, 2);.
      if (!empty($results) && !empty($results->products)) {
        foreach ($results->products as $product) {
          $siteUrl = str_replace(
                ['{productSku}'],
                [self::getProductData($accessor, $product, 'code')],
                $product_url
            );
          $batchArray['productUrl'] = $siteUrl;
          // To run manually.
          ProductImportController::productImportAll($batchArray);
        }
        $message = t('Product import completed successfully');
      }
    }
    return $message;
  }

}

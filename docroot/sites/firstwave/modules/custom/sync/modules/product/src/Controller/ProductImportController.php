<?php

namespace Drupal\sync_product\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\sync_product\ProductImportService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ProductImportController.
 */
class ProductImportController extends ControllerBase {

  /**
   * Drupal\sync_product\ProductImportController definition.
   *
   * @var \Drupal\sync_product\ProductImportInterface
   */
  protected $service;

  /**
   * Class constructor.
   */
  public function __construct(ProductImportService $service) {
    $this->service = $service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('sync_product.import')
    );
  }

}

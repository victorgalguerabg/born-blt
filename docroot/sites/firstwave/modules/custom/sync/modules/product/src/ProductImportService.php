<?php

namespace Drupal\sync_product;

use Drupal\commerce_price\Price;
use Drupal\commerce_product\Entity\Product;
use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Path\AliasStorageInterface;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Messenger\MessengerInterface;

/**
 * Class ProductImportService.
 */
class ProductImportService {

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;
  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;
  /**
   * Drupal\Core\Logger\LoggerChannelFactoryInterface definition.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactorXyInterface
   */
  protected $loggerFactory;

  /**
   * Drupal\Core\Path\AliasStorageInterface definition.
   *
   * @var \Drupal\Core\Path\AliasStorageInterface
   */
  protected $pathAliasStorage;

  /**
   * Drupal\Core\Messenger\MessengerInterface definition.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Constructs a new ProductImportService object.
   */
  public function __construct(ConfigFactoryInterface $config_factory,
                              EntityTypeManagerInterface $entity_type_manager,
                              LoggerChannelFactoryInterface $logger_factory,
                              AliasStorageInterface $pathAliasStorage,
                              MessengerInterface $messenger) {
    $this->configFactory = $config_factory;
    $this->entityTypeManager = $entity_type_manager;
    $this->loggerFactory = $logger_factory;
    $this->pathAliasStorage = $pathAliasStorage;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function productImportFinishedCallback($success, $results, $operations) {
    if ($success) {
      $message = t('Product sync successfully completed.');
    }
    else {
      $message = t('Finished with an error.');
    }
    /* @todo Use DI */
    // $this->messenger->addStatus($message);
    \Drupal::messenger()->addStatus($message);
  }

  /**
   * {@inheritdoc}
   */
  public function generateProductUrl() {
    $config = $this->configFactory->get('delta_services.deltaservicesconfig');
    $siteName = $config->get('site_name');

    $domainUrl = $config->get('domain_url');
    $syncProductURL = $domainUrl . $config->get('sync_api_url');

    if (!empty($siteName)) {
      $syncProductURL = $syncProductURL . '?categories=' . $siteName;
    }
    $filterWebExclusive = $config->get('filter_web_exclusive');
    if (!empty($filterWebExclusive)) {
      $syncProductURL = $syncProductURL . '&FltWebExclusiveCustomerItem=' . $filterWebExclusive;
    }

    $pagination = 10;
    if (!empty($config->get('pagination'))) {
      $pagination = $config->get('pagination');
      $syncProductURL = $syncProductURL . '&size=' . $pagination;
    }

    return $syncProductURL;

  }

  /**
   * Function to create commerce products.
   */
  public function createCommerceProduct($from = NULL, $to = NULL) {

    $sync_config = $this->configFactory->get('delta_services.deltaservicesconfig');
    $delta_config = $this->configFactory->get('webservices_commerce.api_settings');
    $siteName = $delta_config->get('rest_api_settings.term_filter');

    $from = 1;
    $domain = $sync_config->get('api_base_path');
    $website_name = $delta_config->get('rest_api_settings.product_website');
    $url = $domain . $delta_config->get('rest_api_settings.product_fetch_category');
    $url .= '&' . $website_name . '=T';
    $composedUrl = $url . '&page=' . $from;

    $result = web_services_api_call($composedUrl, 'GET', [], [], 'PRODUCT_SYNC');
    $result_array = Json::decode($result['response']);
    $data = $result_array['content'];

    $product_count = 0;
    if (is_array($data)) {
      $product_count = count($data);
    }

    $operations = [];
    for ($i = 0; $i < $product_count; $i++) {
      $operations[] = [
        $this->importProducts(
          $product_count,
          $data[$i],
          $siteName
        ),
      ];
    }
    $batch = [
      'title' => 'Import products...',
      'operations' => $operations,
      'finished' => '\Drupal\sync_product\ProductImportService::productImportFinishedCallback',
    ];
    batch_set($batch);
  }

  /**
   * {@inheritdoc}
   */
  public function importProducts(&$context, $product_item, $siteName = NULL) {
    $this->createCommerceEntities($product_item, $siteName);
    usleep(100);
  }

  /**
   * {@inheritdoc}
   */
  public function createCommerceEntities($product_item = NULL, $siteName = NULL, $update = FALSE) {
    if ($product_item) {
      $this->createProduct($product_item, $siteName);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function createProduct($content, $siteName) {
    $key_exists = $model = NULL;
    $value = NestedArray::getValue($content, ['values', 'ModelNumber'], $key_exists);
    if ($key_exists) {
      $model = $value;
    }
    $existProduct = $this->productExists($model);
    if (empty($existProduct)) {
      $productEntity = Product::create(
        [
          'title' => $content['description'],
          'uid' => '1',
          'type' => 'default',
          'variations' => [self::createCommerceProductVariation($content)],
          'stores' => ['1'],
          'field_image_url' => self::getImageUrls($content),
          'field_brand' => $siteName,
        ]
      );
      $productEntity->save();
      $this->pathAliasStorage
        ->save(
          "/product/" . $productEntity->id(),
          "/products/product-detail/modelNumber/" . $content['defaultModelNumber'],
          "en"
        );

      return $productEntity->id();
    }
    else {
      $message = 'Product exists - ' . $content['description'];
      $this->loggerFactory->get('sync_product')->info(
        '@message', [
          '@message' => $message,
        ]
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function productExists($modelNumber) {

    $products = $this->entityTypeManager
      ->getStorage('commerce_product_variation')
      ->loadByProperties(['sku' => $modelNumber]);
    return $products;

  }

  /**
   * {@inheritdoc}
   */
  public static function getImageUrls($content = NULL) {
    $imageUrls = [];
    if (!empty($content['assets'])) {
      $product_assets_arr = $content['assets'];
      if (count($content['assets'])) {
        foreach ($product_assets_arr as $image_assets) {
          // If ($image_assets['type'] == 'InContextShot'
          // || $image_assets->type == 'Video') {.
          $imageUrls[]['value'] = $image_assets['url'];
          // }
        }
      }
    }
    return $imageUrls;
  }

  /**
   * {@inheritdoc}
   */
  public function createCommerceProductVariation($content = NULL) {
    if (!is_null($content)) {
      $key_exists = $model = NULL;
      $model = NestedArray::getValue($content,
        [
          'values',
          'ModelNumber',
        ],
        $key_exists
      );

      $listPrice = NestedArray::getValue($content,
        [
          'values',
          'ListPrice',
        ],
        $key_exists
      );

      $weight = NestedArray::getValue($content,
        [
          'values',
          'ProductWeight_kg',
        ],
        $key_exists
      );

      $variation = ProductVariation::create(
        [
          'type' => 'default',
          'sku' => $model,
          'status' => 1,
          'price' => self::getPrice($listPrice),
        ]
      );
      $variation->set(
        'field_product_weight',
        $weight
      );
      $variation->save();
      return $variation;

    }
  }

  /**
   * {@inheritdoc}
   */
  public function getPrice($listPrice = NULL, $currencyCode = 'USD') {
    if (!is_null($listPrice)) {
      $price = new Price($listPrice, $currencyCode);
      return $price;
    }

  }

}

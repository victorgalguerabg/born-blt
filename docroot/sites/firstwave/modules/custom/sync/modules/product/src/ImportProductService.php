<?php

namespace Drupal\sync_product;

use Drupal;
use Drupal\commerce_price\Price;
use Drupal\commerce_product\Entity\Product;
use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Config\ConfigFactory;

/**
 * Class ImportProductService.
 */
class ImportProductService {

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Configuration Factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $customConfigFactory;

  /**
   * Logger Factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactory
   */
  protected $loggerFactory;

  /**
   * Constructs a new ImportProductService object.
   */
  public function __construct(ConfigFactoryInterface $config_factory,
                              EntityTypeManagerInterface $entity_type_manager,
                              ConfigFactory $customConfigFactory,
  LoggerChannelFactory $loggerFactory) {
    $this->configFactory = $config_factory->get('df_sync.dfapisettings');
    $this->entityTypeManager = $entity_type_manager;
    $this->customConfigFactory = $customConfigFactory;
    $this->loggerFactory = $loggerFactory;
  }

  /**
   * {@inheritdoc}
   */
  public static function generateProductUrl() {
    $config = Drupal::config('df_sync.dfapisettings');
    $siteName = $config->get('site_name');

    $domainUrl = $config->get('domain_url');
    $syncProductURL = $domainUrl . $config->get('sync_api_url');

    if (!empty($siteName)) {
      $syncProductURL = $syncProductURL . '?categories=' . $siteName;
    }
    $filterWebExclusive = $config->get('filter_web_exclusive');
    if (!empty($filterWebExclusive)) {
      $syncProductURL = $syncProductURL . '&FltWebExclusiveCustomerItem=' . $filterWebExclusive;
    }

    $pagination = 10;
    if (!empty($config->get('pagination'))) {
      $pagination = $config->get('pagination');
      $syncProductURL = $syncProductURL . '&size=' . $pagination;
    }

    return $syncProductURL;

  }

  /**
   * Function to create commerce products.
   */
  public static function createCommerceProduct($from = NULL, $to = NULL) {
    $url = self::generateProductUrl();

    $config = Drupal::config('webservices_commerce.api_settings');
    $siteName = $config->get('rest_api_settings.term_filter');

    $operations = [
          [
            '\Drupal\sync_product\ImportProductService::importProducts',
              [$url . '&sort_ListPrice=DESC', $siteName, $from, $to],
          ],
    ];
    $batch = [
      'title' => 'Import products...',
      'operations' => $operations,
    ];
    batch_set($batch);
    // drush_backend_batch_process();
  }

  /**
   * {@inheritdoc}
   */
  public static function importProducts(&$context, $url = NULL, $siteName = NULL, $from = NULL, $to = NULL) {

    do {

      $from = 1;
      $to = 2;
      $config = Drupal::config('webservices_commerce.api_settings');
      $domain = $config->get('rest_api_settings.wcs_rest_api');
      $website_name = $config->get('rest_api_settings.product_website');
      $url = $domain . $config->get('rest_api_settings.product_listing_page');
      //$url .= '&' . $website_name . '=T';
      //$composedUrl = $url . '&page=' . $from;
      $composedUrl = $url;
      // drush_print($composedUrl);
      $request = web_services_api_call($composedUrl, 'GET', [], [], 'PRODUCT_SYNC');
      $decodedResponse = json_decode($request['response']);

      self::createCommerceEntities($decodedResponse, $siteName);
      usleep(100);
      $_SESSION['http_request_count']++;

      // $context['sandbox']['progress']++;
      $from++;
      $composedUrl = '';
    } while ($to >= $from);
  }

  /**
   * {@inheritdoc}
   */
  public static function createCommerceEntities($decodedResponse = NULL, $siteName = NULL, $update = FALSE) {
    if (!is_null($decodedResponse)) {
      if (!$update) {
        foreach ($decodedResponse->content as $content) {
          self::createProduct($content, $siteName);
        }
      }
      else {
        self::createProduct($decodedResponse, $siteName);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function createProduct($content, $siteName) {
    $existProduct = self::productExists($content->values->ModelNumber);
    if (empty($existProduct)) {
      $productEntity = Product::create(
            [
              'title' => $content->description,
              'uid' => '1',
              'type' => 'default',
              'variations' => [self::createCommerceProductVariation($content)],
              'stores' => ['1'],
              'field_image_url' => self::getImageUrls($content),
              'field_brand' => $siteName,
            ]
        );
      $productEntity->save();
      Drupal::service('path.alias_storage')
        ->save("/product/" . $productEntity->id(), "/products/product-detail/modelNumber/" . $content->defaultModelNumber, "en");
    }
    else {
      $message = 'Product exists - ' . $content->description;
      Drupal::logger('sync_product')->info($message);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function deleteCommerceProduct() {
    $products = Drupal::entityTypeManager()
      ->getStorage('commerce_product')
      ->loadMultiple();
    $i = 0;

    if ($i == 0) {
      foreach ($products as $value) {
        $batch['operations'][] = [
          '\Drupal\sync_product\ImportProductService::delteProducts',

              [$value],
        ];
      }
      $i++;
    }

    batch_set($batch);
    drush_backend_batch_process();

  }

  /**
   * {@inheritdoc}
   */
  public static function delteProducts($product) {
    $product->delete();
  }

  /**
   * {@inheritdoc}
   */
  public static function productExists($modelNumber) {

    $products = Drupal::entityTypeManager()
      ->getStorage('commerce_product_variation')
      ->loadByProperties(['sku' => $modelNumber]);
    return $products;

  }

  /**
   * {@inheritdoc}
   */
  public static function getImageUrls($content = NULL) {
    $imageUrls = [];
    if (!empty($content->assets)) {
      $product_assets_arr = $content->assets;
      if (count($content->assets)) {
        foreach ($product_assets_arr as $image_assets) {
          if ($image_assets->type == 'InContextShot' || $image_assets->type == 'Video') {
            $imageUrls[]['value'] = $image_assets->url;
          }
        }
      }
    }
    return $imageUrls;
  }

  /**
   * {@inheritdoc}
   */
  public function createCommerceProductVariation($content = NULL) {
    if (!is_null($content)) {
      $variation = ProductVariation::create(
            [
              'type' => 'default',
              'sku' => $content->values->ModelNumber,
              'status' => 1,
              'price' => self::getPrice($content->values->ListPrice),
            ]
        );
      $variation->set('field_product_weight', $content->values->ProductWeight_kg);
      $variation->save();
      return $variation;

    }
  }

  /**
   * {@inheritdoc}
   */
  public function getPrice($listPrice = NULL, $currencyCode = 'USD') {
    if (!is_null($listPrice)) {
      $price = new Price($listPrice, $currencyCode);
      return $price;
    }

  }

}

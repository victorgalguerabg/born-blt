<?php

/**
 * @file
 * Contains the code to generate the custom drush commands.
 */

use Drupal\sync_product\Form\ProductSync;

/**
 * Implements hook_drush_command().
 */
function sync_product_drush_command() {
  $items = [];
  $items['import-product-all'] = [
    'description' => 'drush import-product-all',
    'drupal dependencies' => ['sync_product'],
    'aliases' => ['import:product'],
  ];
  $items['import-product-manual'] = [
    'description' => 'drush import-product-manual [sku1],[sku2]',
    'arguments' => [
      'sku' => 'pass sku as argument, for multiple use comma separator',
    ],
    'drupal dependencies' => ['sync_product'],
    'aliases' => ['import:product'],
  ];

  return $items;
}

/**
 * Call back function drush_sync_product_import_product_all()
 */
function drush_sync_product_import_product_all() {
  $response = ProductSync::drushProductImport();
  drush_print($response);
}

/**
 * Call back function drush_sync_product_import_product_manual()
 */
function drush_sync_product_import_product_manual($sku) {
  if (empty($sku)) {
    $response = t('Please enter sku separated by comma.');
    drush_print($response);
  }
  $response = ProductSync::drushProductImport($sku);
  drush_print($response);
}

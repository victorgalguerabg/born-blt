(function ($, Drupal, drupalSettings) {

  Drupal.behaviors.faq_component = {
    attach: function (context, settings) {
      jQuery( "#innovation-faq-wrapper" ).accordion({
        header: "h3",
      });
    }
  };
})(jQuery, Drupal, drupalSettings);
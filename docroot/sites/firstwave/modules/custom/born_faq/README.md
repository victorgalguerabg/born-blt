# BORN FAQ MODULE

1. Place the born_faq module under custom modules folder of your site.
2. Install the Module. Once the module is installed the following content type, paragraph and views will be created by Drupal.
  - FAQ Content Type.
  - FAQ Section Paragraph / component.
  - FAQ Category Vocabulary.
  - Views for listing FAQ's by category and FAQ Category listing.
3. You can make use of FAQ Section component by adding it to your content types.
4. The templates for the FAQ Section can be overridden by copying the template to your themes templates directory.
5. The views can be customized or used as it is per your requirement. Please note that the views output can also be themed.

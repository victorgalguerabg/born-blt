<?php

namespace Drupal\df_services;

use Drupal\commerce_cart\CartManagerInterface;
use Drupal\commerce_cart\CartProviderInterface;
use Drupal\commerce_product\Entity\Product;
use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\user\PrivateTempStoreFactory;
use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityManagerInterface;
use GuzzleHttp\ClientInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\checkout_services\CheckoutServices;

/**
 * {@inheritdoc}
 */

class DfCheckoutServices {

  protected $cartManager;

  protected $cartProvider;

  protected $entityTypeManager;

  protected $accountId;

  protected $currentUser;

  protected $priorityAcces;

  /**
   * Logger Factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactory
   */
  protected $loggerFactory;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $customEntityTypeManager;

  /**
   * The tempstore service.
   *
   * @var \Drupal\user\PrivateTempStoreFactory
   */
  protected $tempStoreFactory;

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  protected $entityManager;

  /**
   * Guzzle\Client instance.
   *
   * @var \Guzzle\Client
   */
  protected $httpClient;

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * @var \Drupal\checkout_services\CheckoutServices
   */
  protected $globalCheckoutServices;

  /**
   * {@inheritdoc}
   */
  public function __construct(CartManagerInterface $cartManager, CartProviderInterface $cartProvider, EntityTypeManager $entityTypeManager, AccountProxyInterface $currentUser, EntityTypeManagerInterface $customEntityTypeManager,  PrivateTempStoreFactory $privateTempStoreFactory, LoggerChannelFactory $loggerFactory, ConfigFactoryInterface $configFactory, EntityManagerInterface $entityManager, ClientInterface $httpClient, MessengerInterface $messenger, CheckoutServices $globalCheckoutServices) {
    $this->cart_manager = $cartManager;
    $this->cart_provider = $cartProvider;
    $this->entityTypeManager = $entityTypeManager->getStorage('commerce_order');
    $this->current_user = $currentUser;
    $this->customEntityTypeManager = $customEntityTypeManager;
    $this->tempStoreFactory = $privateTempStoreFactory->get('multistep_data');
    $this->loggerFactory = $loggerFactory->get('fw_checkout');
    $this->configFactory = $configFactory->get('api_config.apisettings');
    $this->entityManager = $entityManager;
    $this->httpClient = $httpClient;
    $this->messenger = $messenger;
    $this->globalCheckoutServices = $globalCheckoutServices;
  }

  /**
   * {@inheritdoc}
   */
  public function objConstruct( $cardDetails = NULL) {

    $store = $this->entityManager->getStorage('commerce_store')->loadDefault();
    $cart = $this->cart_provider->getCart($this->configFactory->get('store_type'), $store);
    if(is_null($cart)) {
      $this->messenger->addError("Cart is empty, Please add Products to continue.");
      return;
    }
    else {
      $total_items = $cart-> getItems();
      if (empty($total_items)) {
        return;
      }
      $order_list = $total_items[0]->toArray();

      $productVariation = ProductVariation::load($order_list['purchased_entity'][0]['target_id'])->toArray();
      $product = Product::load($productVariation['product_id'][0]['target_id'])->toArray();
      $total_unitPrice = (int) $order_list['total_price'][0]['number'];
      $unit_price = (int) $order_list['unit_price'][0]['number'];
      $product_weight = $productVariation['field_product_weight'][0]['value'] * $order_list['quantity'][0]['value'];

      $prdImg = "";
      if (count($product['field_image_url']) > 0) {
        $prdImg = $product['field_image_url'][0]['value'];
      }

      $order_item_construction[] =
        [
          "lineNumber"    => "1",
          "skuCode"       => $productVariation['sku'][0]['value'],
          "description"   => $product['title'][0]['value'],
          "listPrice"     => $unit_price,
          "unitPrice"     => $unit_price,
          "extendedPrice" => $unit_price,
          "externalPrice" => null,
          "offBottomDiscPct"=> null,
          "quantity"=> (int) $order_list['quantity'][0]['value'],
          "tax"=> 0,
          "taxPercent"=> 0,
          "taxCode"=> 0,
          "orderStatus"=> null,
          "trackingNumber"=> null,
          "lineTotal"=> 0,
          "carrierWebsite"=> null,
          "carrier"=> $this->configFactory->get('carrier'),
          "branchPlant"=> null,
          "onHandQuantity"=> "0",
          "availableQuantity"=> "0",
          "availableQuantityFGW"=> null,
          "committedQuantity"=> "0",
          "onReceiptQuantity"=> "0",
          "promoCode"=> "",
          "activityNumber"=> "",
          "estShipDate"=> null,
          "requestShipDate"=> null,
          "customerItemNumber"=> null,
          "shipDate"=> null,
          "qtyOrdered"=> null,
          "qtyShipped"=> "0",
          "qtyBackOrdered"=> "0",
          "productWeight"=> $product_weight,
          "netPrice"=> null,
          "imageFilename"=> $prdImg,
          "orderWeight"=> 0.19
        ];

      $shippingAddress = [];
      $billingAddress = [];
      $name = $this->tempStoreFactory->get('fullname');
      if (isset($name)) {
        $shippingAddress = [
          "addressNumber"=> null,
          "fullName"=> $this->tempStoreFactory->get('fullname') ? $this->tempStoreFactory->get('fullname') : NULL,
          "address1"=> $this->tempStoreFactory->get('address_line') ? $this->tempStoreFactory->get('address_line') : NULL,
          "address2"=> $this->tempStoreFactory->get('address_line_optional') ? $this->tempStoreFactory->get('address_line_optional') : NULL,
          "city"=> $this->tempStoreFactory->get('city') ? $this->tempStoreFactory->get('city') : NULL,
          "state"=> $this->tempStoreFactory->get('state') ? $this->tempStoreFactory->get('state') : NULL,
          "zip5"=> $this->tempStoreFactory->get('zipcode') ? $this->tempStoreFactory->get('zipcode') : NULL,
          "zip4"=> null,
          "country"=> null,
          "valid"=> false,
          "messages"=> null
        ];
      }

      if (isset($name)) {
        $billingAddress = [
          "addressNumber" => null,
          "fullName" => $this->tempStoreFactory->get('fullname') ? $this->tempStoreFactory->get('fullname') : NULL,
          "address1" => $this->tempStoreFactory->get('address_line') ? $this->tempStoreFactory->get('address_line') : NULL,
          "address2" => $this->tempStoreFactory->get('address_line_optional') ? $this->tempStoreFactory->get('address_line_optional') : NULL,
          "city" => $this->tempStoreFactory->get('city') ? $this->tempStoreFactory->get('city') : NULL,
          "state" => $this->tempStoreFactory->get('state') ? $this->tempStoreFactory->get('state') : NULL,
          "zip5" => $this->tempStoreFactory->get('zipcode') ? $this->tempStoreFactory->get('zipcode') : NULL,
          "zip4" => null,
          "country" => null,
          "valid" => false,
          "messages" => null
        ];
      }

      $orderNo = [9];
      //CustomerOrderNumber
      $phone = $this->tempStoreFactory->get('phone_number');
      $customerOrderNumber = substr($phone, 0, 3).' '.substr($phone, 4, 3).' '.substr($phone, 8, 4).' '.$this->globalCheckoutServices->randomNumber($orderNo);


      $lenthArray = [8,4,4,4,12];

      $final_objconstruct_req = [
        "cartID"=> $this->globalCheckoutServices->randomNumber($lenthArray),
        "customerID"=> "guest",
        "orderNumber"=> "",
        "companyNumber"=> $this->configFactory->get('companyNumber'),
        "documentType"=> "",
        "customerOrderNumber"=> $customerOrderNumber,
        "quoteID"=> null,
        "orderItems"=> $order_item_construction,
        "phone"=> $this->tempStoreFactory->get('phone_number') ? $this->tempStoreFactory->get('phone_number') : NULL,
        "email"=> $this->tempStoreFactory->get('email') ? $this->tempStoreFactory->get('email') : NULL,
        "futureRequestDate"=> null,
        "minimumOrderCharge"=> 0,
        "orderTradeAllowance"=> 0,
        "shippingAddress"=> $shippingAddress ? $shippingAddress : null,
        "billingAddress"=> $billingAddress ? $billingAddress : null,
        "shippingOriginAddress"=> null,
        "shippingServiceLevel"=> $this->tempStoreFactory->get('shipping_option') ? $this->tempStoreFactory->get('shipping_option') : NULL,
        "shippingCharge"=> null,
        "shippingTax"=> null,
        "shippingTaxPercent"=> 0,
        "tax"=> 0,
        "ccNumber"=> $cardDetails[3] ? $cardDetails[3] : NULL,
        "ccConfirmationCode"=> $cardDetails[5] ? $cardDetails[5] : NULL,
        "ccType"=> $cardDetails[0] ? $cardDetails[0] : NULL,
        "ccName"=> $cardDetails[4] ? $cardDetails[4] : NULL,
        "ccLastFour"=> null,
        "ccExpireMonth"=> $cardDetails[1] ? $cardDetails[1] : NULL,
        "ccExpireYear"=> $cardDetails[2] ? $cardDetails[2] : NULL,
        "coupon"=> null,
        "couponDiscount"=> null,
        "orderSubTotal"=> $total_unitPrice,
        "orderTotal"=> $total_unitPrice,
        "orderPlaced"=> null,
        "snappayAuthorizationID"=> null,
        "maxLeadTime"=> -1,
        "priorityAccessCode"=> null,
        "shiptoNumber"=> $this->configFactory->get('shiptoNumber'),
        "billtoNumber"=> null,
        "source"=> $this->configFactory->get('source'),
        "writeOrder"=> null,
        "quickShip"=> false,
        "activityCode"=> null,
        "dropShip"=> false,
        "dropShipAddress"=> null,
        "paymetricAuthorizationCode"=> null,
        "paymetricAuthorizationDate"=> null,
        "paymetricTransactionId"=> null,
        "paymetricAuthorizationTime"=> null,
        "paymetricMessage"=> null,
        "paymetricAVSAddress"=> null,
        "paymetricAVSCode"=> null,
        "paymetricAVSZipCode"=> null,
        "paymetricResponseCode"=> null,
        "paymetricAuthorizeAmount"=> null,
        "valid"=> false,
        "errors"=> [],
        "orderWeight"=> 0.19,
      ];

      return $final_objconstruct_req;
    }
  }

  /**
   * Http Client function to call Ecommerce API.
   */
  public function ecommerceCurlCall($url, $constructObj) {

    // Check to Ecommerce API.
    $this->loggerFactory->notice('Ecommerce-REQ-@message', [
      '@message' => $url,
    ]);
    $this->loggerFactory->notice('Ecommerce-REQ-OBJ-@message', [
      '@message' => json_encode($constructObj),
    ]);

    $request = $this->httpClient->post($url, [
      'json' => $constructObj,
      'auth' => [$this->configFactory->get('delta_api_username'),$this->configFactory->get('delta_api_password')],
      'headers' => [
        'Content-Type' => 'application/json',
        'Cookie' => 'BNI_persistence=0000000000000000000000007b6f7e0a00005000'
      ],
    ]);
    $response = json_decode($request->getBody(),true);

    $this->loggerFactory->notice('Ecommerce-RES- @message', [
      '@message' => json_encode($response),
    ]);

    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function getOrderDetails($apiDecodedResponse = NULL) {
    $items = [];
    if (!empty($apiDecodedResponse)) {
      $items['orderSubTotal'] = $this->getPriceFormat($apiDecodedResponse['orderSubTotal']);
      $items['orderTotal'] = $this->getPriceFormat($apiDecodedResponse['orderTotal']);
      $items['shippingCharge'] = $apiDecodedResponse['shippingCharge'];

      //Order Details
      if(!empty($apiDecodedResponse['orderItems'])) {
        foreach ($apiDecodedResponse['orderItems'] as $orderItems) {
          $items['orderItems'][] = [
            'imageUrl' => $orderItems['imageFilename'],
            'title' => $orderItems['description'],
            'estimatedShipDate' => date('m/d/Y', $orderItems['estShipDate']),
            'qty' => $orderItems['quantity'],
            'unitPrice' => $this->getPriceFormat($orderItems['unitPrice']),
            'extendedPrice' => $this->getPriceFormat($orderItems['extendedPrice']),
            'sku' => $orderItems['skuCode'],
          ];
        }
      }
      //Shipping Address
      if(isset($apiDecodedResponse['shippingAddress'])) {
          $items['shippingAddress'][] = [
            'fullName' => $apiDecodedResponse['shippingAddress']['fullName'],
            'address1' => $apiDecodedResponse['shippingAddress']['address1'],
            'address2' => $apiDecodedResponse['shippingAddress']['address2'],
            'city' => $apiDecodedResponse['shippingAddress']['city'],
            'state' => $apiDecodedResponse['shippingAddress']['state'],
            'zip5' => $apiDecodedResponse['shippingAddress']['zip5'],
          ];
      }
      //Phone number
      if(isset($apiDecodedResponse['phone'])) {
        $items['phone'] = $apiDecodedResponse['phone'];
      }
      //Email id
      if(isset($apiDecodedResponse['email'])) {
        $items['email'] = $apiDecodedResponse['email'];
      }

      //ShiptoNumber
      if(isset($apiDecodedResponse['shiptoNumber'])) {
        $items['shiptoNumber'] = $apiDecodedResponse['shiptoNumber'];
      }

      //billtoNumber
      if(isset($apiDecodedResponse['billtoNumber'])) {
        $items['billtoNumber'] = $apiDecodedResponse['billtoNumber'];
      }

      //cartID
      if(isset($apiDecodedResponse['cartID'])) {
        $items['cartID'] = $apiDecodedResponse['cartID'];
      }

      //customerOrderNumber
      if(isset($apiDecodedResponse['customerOrderNumber'])) {
        $items['customerOrderNumber'] = $apiDecodedResponse['customerOrderNumber'];
      }

      //shippingServiceLevel
      if(isset($apiDecodedResponse['shippingServiceLevel'])) {
        $items['shippingServiceLevel'] = $apiDecodedResponse['shippingServiceLevel'];
      }

      //shippingCharge
      if(isset($apiDecodedResponse['shippingCharge'])) {
        $items['shippingCharge'] = $apiDecodedResponse['shippingCharge'];
      }

      //tax
      if(isset($apiDecodedResponse['tax'])) {
        $items['tax'] = $apiDecodedResponse['tax'];
      }

      //Error response
      $items['errors'][] = count($apiDecodedResponse['errors']);
      $items['errors'][] = $apiDecodedResponse['errors'];
    }
    return [
      '#items' => $items,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function OrderConfConstruct( $cardDetails = NULL) {

    $store = $this->entityManager->getStorage('commerce_store')->loadDefault();
    $cart = $this->cart_provider->getCart($this->configFactory->get('store_type'), $store);
    $total_items = $cart-> getItems();
    $order_list = $total_items[0]->toArray();

    $productVariation = ProductVariation::load($order_list['purchased_entity'][0]['target_id'])->toArray();
    $product = Product::load($productVariation['product_id'][0]['target_id'])->toArray();
    $total_unitPrice = (int) $order_list['total_price'][0]['number'];
    $unit_price = (int) $order_list['unit_price'][0]['number'];
    $product_weight = $productVariation['field_product_weight'][0]['value'] * $order_list['quantity'][0]['value'];

    $prdImg = "";
    if (count($product['field_image_url']) > 0) {
      $prdImg = $product['field_image_url'][0]['value'];
    }

    $order_item_construction[] =
      [
        "lineNumber"    => "1",
        "skuCode"       => $productVariation['sku'][0]['value'],
        "description"   => $product['title'][0]['value'],
        "listPrice"     => $unit_price,
        "unitPrice"     => $unit_price,
        "extendedPrice" => $unit_price,
        "externalPrice" => null,
        "offBottomDiscPct"=> null,
        "quantity"=> (int) $order_list['quantity'][0]['value'],
        "tax"=> 0,
        "taxPercent"=> 0,
        "taxCode"=> 0,
        "orderStatus"=> null,
        "trackingNumber"=> null,
        "lineTotal"=> 0,
        "carrierWebsite"=> null,
        "carrier"=> $this->configFactory->get('carrier'),
        "branchPlant"=> null,
        "onHandQuantity"=> "0",
        "availableQuantity"=> "0",
        "availableQuantityFGW"=> null,
        "committedQuantity"=> "0",
        "onReceiptQuantity"=> "0",
        "promoCode"=> "",
        "activityNumber"=> "",
        "estShipDate"=> null,
        "requestShipDate"=> null,
        "customerItemNumber"=> null,
        "shipDate"=> null,
        "qtyOrdered"=> null,
        "qtyShipped"=> "0",
        "qtyBackOrdered"=> "0",
        "productWeight"=> $product_weight,
        "netPrice"=> null,
        "imageFilename"=> $prdImg,
        "orderWeight"=> $product_weight
      ];

    $shippingAddress = [];
    $billingAddress = [];
    $name = $this->tempStoreFactory->get('fullname');
    if (isset($name)) {
      $shippingAddress = [
        "addressNumber"=> null,
        "fullName"=> $this->tempStoreFactory->get('fullname') ? $this->tempStoreFactory->get('fullname') : NULL,
        "address1"=> $this->tempStoreFactory->get('address_line') ? $this->tempStoreFactory->get('address_line') : NULL,
        "address2"=> $this->tempStoreFactory->get('address_line_optional') ? $this->tempStoreFactory->get('address_line_optional') : NULL,
        "city"=> $this->tempStoreFactory->get('city') ? $this->tempStoreFactory->get('city') : NULL,
        "state"=> $this->tempStoreFactory->get('state') ? $this->tempStoreFactory->get('state') : NULL,
        "zip5"=> $this->tempStoreFactory->get('zipcode') ? $this->tempStoreFactory->get('zipcode') : NULL,
        "zip4"=> null,
        "country"=> null,
        "valid"=> false,
        "messages"=> null
      ];
    }

    if ($this->tempStoreFactory->get('bill_ship_address_same') == 0) {
      $billingAddress = [
        "addressNumber" => null,
        "fullName" => $this->tempStoreFactory->get('payment_fullname') ? $this->tempStoreFactory->get('payment_fullname') : NULL,
        "address1" => $this->tempStoreFactory->get('payment_address_line') ? $this->tempStoreFactory->get('payment_address_line') : NULL,
        "address2" => $this->tempStoreFactory->get('payment_address_line_optional') ? $this->tempStoreFactory->get('payment_address_line_optional') : NULL,
        "city" => $this->tempStoreFactory->get('payment_city') ? $this->tempStoreFactory->get('payment_city') : NULL,
        "state" => $this->tempStoreFactory->get('payment_state') ? $this->tempStoreFactory->get('payment_state') : NULL,
        "zip5" => $this->tempStoreFactory->get('payment_zipcode') ? $this->tempStoreFactory->get('payment_zipcode') : NULL,
        "zip4" => null,
        "country" => null,
        "valid" => false,
        "messages" => null
      ];
    }
    else {
      $billingAddress = [
        "addressNumber" => null,
        "fullName" => $this->tempStoreFactory->get('fullname') ? $this->tempStoreFactory->get('fullname') : NULL,
        "address1" => $this->tempStoreFactory->get('address_line') ? $this->tempStoreFactory->get('address_line') : NULL,
        "address2" => $this->tempStoreFactory->get('address_line_optional') ? $this->tempStoreFactory->get('address_line_optional') : NULL,
        "city" => $this->tempStoreFactory->get('city') ? $this->tempStoreFactory->get('city') : NULL,
        "state" => $this->tempStoreFactory->get('state') ? $this->tempStoreFactory->get('state') : NULL,
        "zip5" => $this->tempStoreFactory->get('zipcode') ? $this->tempStoreFactory->get('zipcode') : NULL,
        "zip4" => null,
        "country" => null,
        "valid" => false,
        "messages" => null
      ];
    }

    $ccType = "";
    if($cardDetails['#items']['ccType'] == "vi") {
      $ccType = "VISA";
    }

    if($cardDetails['#items']['ccType'] == "mc") {
      $ccType = "MC";
    }

    $final_objconstruct_req = [
      "cartID"=> $cardDetails['#items']['cartID'],
      "customerID"=> "guest",
      "orderNumber"=> "",
      "companyNumber"=> $this->configFactory->get('companyNumber'),
      "documentType"=> "",
      "customerOrderNumber"=> $cardDetails['#items']['customerOrderNumber'],
      "quoteID"=> null,
      "orderItems"=> $order_item_construction,
      "phone"=> $this->tempStoreFactory->get('phone_number') ? $this->tempStoreFactory->get('phone_number') : NULL,
      "email"=> $this->tempStoreFactory->get('email') ? $this->tempStoreFactory->get('email') : NULL,
      "futureRequestDate"=> null,
      "minimumOrderCharge"=> 0,
      "orderTradeAllowance"=> 0,
      "shippingAddress"=> $shippingAddress ? $shippingAddress : null,
      "billingAddress"=> $billingAddress ? $billingAddress : null,
      "shippingOriginAddress"=> null,
      "shippingServiceLevel"=> $this->tempStoreFactory->get('shipping_option') ? $this->tempStoreFactory->get('shipping_option') : NULL,
      "shippingCharge"=> $cardDetails['#items']['shippingCharge'],
      "shippingTax"=> null,
      "shippingTaxPercent"=> 0,
      "tax"=> $cardDetails['#items']['tax'],
      "ccNumber"=> $cardDetails['#items']['ccNumber'],
      "ccConfirmationCode"=> $cardDetails['#items']['ccConfirmationCode'],
      "ccType"=> $ccType,
      "ccName"=> $cardDetails['#items']['ccName'],
      "ccLastFour"=> null,
      "ccExpireMonth"=> $cardDetails['#items']['ccExpireMonth'],
      "ccExpireYear"=> $cardDetails['#items']['ccExpireYear'],
      "coupon"=> null,
      "couponDiscount"=> null,
      "orderSubTotal"=> $cardDetails['#items']['orderSubTotal'],
      "orderTotal"=> $cardDetails['#items']['orderTotal'],
      "orderPlaced"=> null,
      "snappayAuthorizationID"=> null,
      "maxLeadTime"=> -1,
      "priorityAccessCode"=> null,
      "shiptoNumber"=> $cardDetails['#items']['shiptoNumber'],
      "billtoNumber"=> $cardDetails['#items']['billtoNumber'],
      "source"=> $this->configFactory->get('source'),
      "writeOrder"=> true,
      "quickShip"=> false,
      "activityCode"=> null,
      "dropShip"=> false,
      "dropShipAddress"=> null,
      "paymetricAuthorizationCode"=> null,
      "paymetricAuthorizationDate"=> NULL,
      "paymetricTransactionId"=> NULL,
      "paymetricAuthorizationTime"=> NULL,
      "paymetricMessage"=> NULL,
      "paymetricAVSAddress"=> NULL,
      "paymetricAVSCode"=> NULL,
      "paymetricAVSZipCode"=> NULL,
      "paymetricResponseCode"=> NULL,
      "paymetricAuthorizeAmount"=> NULL,
      "valid"=> false,
      "errors"=> [],
      "orderWeight"=> 0.19,
    ];

    return $final_objconstruct_req;
  }

  /**
   * {@inheritdoc}
   */
  public function OrderConfirmationConstruct( $cardDetails = NULL) {

    $store = $this->entityManager->getStorage('commerce_store')->loadDefault();
    $cart = $this->cart_provider->getCart($this->configFactory->get('store_type'), $store);
    $total_items = $cart-> getItems();
    $order_list = $total_items[0]->toArray();

    $productVariation = ProductVariation::load($order_list['purchased_entity'][0]['target_id'])->toArray();
    $product = Product::load($productVariation['product_id'][0]['target_id'])->toArray();
    $total_unitPrice = (int) $order_list['total_price'][0]['number'];
    $unit_price = (int) $order_list['unit_price'][0]['number'];
    $product_weight = $productVariation['field_product_weight'][0]['value'] * $order_list['quantity'][0]['value'];

    $prdImg = "";
    if (count($product['field_image_url']) > 0) {
      $prdImg = $product['field_image_url'][0]['value'];
    }

    $order_item_construction[] =
      [
        "lineNumber"    => "1",
        "skuCode"       => $productVariation['sku'][0]['value'],
        "description"   => $product['title'][0]['value'],
        "listPrice"     => $unit_price,
        "unitPrice"     => $unit_price,
        "extendedPrice" => $unit_price,
        "externalPrice" => null,
        "offBottomDiscPct"=> null,
        "quantity"=> (int) $order_list['quantity'][0]['value'],
        "tax"=> 0,
        "taxPercent"=> 0,
        "taxCode"=> 0,
        "orderStatus"=> null,
        "trackingNumber"=> null,
        "lineTotal"=> 0,
        "carrierWebsite"=> null,
        "carrier"=> $this->configFactory->get('carrier'),
        "branchPlant"=> null,
        "onHandQuantity"=> "0",
        "availableQuantity"=> "0",
        "availableQuantityFGW"=> null,
        "committedQuantity"=> "0",
        "onReceiptQuantity"=> "0",
        "promoCode"=> "",
        "activityNumber"=> "",
        "estShipDate"=> null,
        "requestShipDate"=> null,
        "customerItemNumber"=> null,
        "shipDate"=> null,
        "qtyOrdered"=> null,
        "qtyShipped"=> "0",
        "qtyBackOrdered"=> "0",
        "productWeight"=> $product_weight,
        "netPrice"=> null,
        "imageFilename"=> $prdImg,
        "orderWeight"=> $product_weight
      ];

    $shippingAddress = [];
    $billingAddress = [];
    $name = $this->tempStoreFactory->get('fullname');
    if (isset($name)) {
      $shippingAddress = [
        "addressNumber"=> null,
        "fullName"=> $this->tempStoreFactory->get('fullname') ? $this->tempStoreFactory->get('fullname') : NULL,
        "address1"=> $this->tempStoreFactory->get('address_line') ? $this->tempStoreFactory->get('address_line') : NULL,
        "address2"=> $this->tempStoreFactory->get('address_line_optional') ? $this->tempStoreFactory->get('address_line_optional') : NULL,
        "city"=> $this->tempStoreFactory->get('city') ? $this->tempStoreFactory->get('city') : NULL,
        "state"=> $this->tempStoreFactory->get('state') ? $this->tempStoreFactory->get('state') : NULL,
        "zip5"=> $this->tempStoreFactory->get('zipcode') ? $this->tempStoreFactory->get('zipcode') : NULL,
        "zip4"=> null,
        "country"=> null,
        "valid"=> false,
        "messages"=> null
      ];
    }

    if ($this->tempStoreFactory->get('bill_ship_address_same') == 0) {
      $billingAddress = [
        "addressNumber" => null,
        "fullName" => $this->tempStoreFactory->get('payment_fullname') ? $this->tempStoreFactory->get('payment_fullname') : NULL,
        "address1" => $this->tempStoreFactory->get('payment_address_line') ? $this->tempStoreFactory->get('payment_address_line') : NULL,
        "address2" => $this->tempStoreFactory->get('payment_address_line_optional') ? $this->tempStoreFactory->get('payment_address_line_optional') : NULL,
        "city" => $this->tempStoreFactory->get('payment_city') ? $this->tempStoreFactory->get('payment_city') : NULL,
        "state" => $this->tempStoreFactory->get('payment_state') ? $this->tempStoreFactory->get('payment_state') : NULL,
        "zip5" => $this->tempStoreFactory->get('payment_zipcode') ? $this->tempStoreFactory->get('payment_zipcode') : NULL,
        "zip4" => null,
        "country" => null,
        "valid" => false,
        "messages" => null
      ];
    }
    else {
      $billingAddress = [
        "addressNumber" => null,
        "fullName" => $this->tempStoreFactory->get('fullname') ? $this->tempStoreFactory->get('fullname') : NULL,
        "address1" => $this->tempStoreFactory->get('address_line') ? $this->tempStoreFactory->get('address_line') : NULL,
        "address2" => $this->tempStoreFactory->get('address_line_optional') ? $this->tempStoreFactory->get('address_line_optional') : NULL,
        "city" => $this->tempStoreFactory->get('city') ? $this->tempStoreFactory->get('city') : NULL,
        "state" => $this->tempStoreFactory->get('state') ? $this->tempStoreFactory->get('state') : NULL,
        "zip5" => $this->tempStoreFactory->get('zipcode') ? $this->tempStoreFactory->get('zipcode') : NULL,
        "zip4" => null,
        "country" => null,
        "valid" => false,
        "messages" => null
      ];
    }

   /* $ccType = "";
    if($cardDetails['#items']['ccType'] == "vi") {
      $ccType = "VISA";
    }

    if($cardDetails['#items']['ccType'] == "mc") {
      $ccType = "MC";
    }*/

    $final_objconstruct_req = [
      "cartID"=> $cardDetails['#items']['cartID'],
      "customerID"=> "guest",
      "orderNumber"=> "",
      "companyNumber"=> $this->configFactory->get('companyNumber'),
      "documentType"=> "",
      "customerOrderNumber"=> $cardDetails['#items']['customerOrderNumber'],
      "quoteID"=> null,
      "orderItems"=> $order_item_construction,
      "phone"=> $this->tempStoreFactory->get('phone_number') ? $this->tempStoreFactory->get('phone_number') : NULL,
      "email"=> $this->tempStoreFactory->get('email') ? $this->tempStoreFactory->get('email') : NULL,
      "futureRequestDate"=> null,
      "minimumOrderCharge"=> 0,
      "orderTradeAllowance"=> 0,
      "shippingAddress"=> $shippingAddress ? $shippingAddress : null,
      "billingAddress"=> $billingAddress ? $billingAddress : null,
      "shippingOriginAddress"=> null,
      "shippingServiceLevel"=> $this->tempStoreFactory->get('shipping_option') ? $this->tempStoreFactory->get('shipping_option') : NULL,
      "shippingCharge"=> $cardDetails['#items']['shippingCharge'],
      "shippingTax"=> null,
      "shippingTaxPercent"=> 0,
      "tax"=> $cardDetails['#items']['tax'],
      "ccNumber"=> $cardDetails['#items']['ccNumber'],
      "ccConfirmationCode"=> $cardDetails['#items']['ccConfirmationCode'],
      "ccType"=> $cardDetails['#items']['ccType'],
      "ccName"=> $cardDetails['#items']['ccName'],
      "ccLastFour"=> null,
      "ccExpireMonth"=> $cardDetails['#items']['ccExpireMonth'],
      "ccExpireYear"=> $cardDetails['#items']['ccExpireYear'],
      "coupon"=> null,
      "couponDiscount"=> null,
      "orderSubTotal"=> $cardDetails['#items']['orderSubTotal'],
      "orderTotal"=> $cardDetails['#items']['orderTotal'],
      "orderPlaced"=> null,
      "snappayAuthorizationID"=> null,
      "maxLeadTime"=> -1,
      "priorityAccessCode"=> null,
      "shiptoNumber"=> $cardDetails['#items']['shiptoNumber'],
      "billtoNumber"=> $cardDetails['#items']['billtoNumber'],
      "source"=> $this->configFactory->get('source'),
      "writeOrder"=> true,
      "quickShip"=> false,
      "activityCode"=> null,
      "dropShip"=> false,
      "dropShipAddress"=> null,
      "paymetricAuthorizationCode"=> $cardDetails['#items']['paymetricAuthorizationCode'] ? $cardDetails['#items']['paymetricAuthorizationCode'] : NULL,
      "paymetricAuthorizationDate"=> $cardDetails['#items']['paymetricAuthorizationDate'] ? $cardDetails['#items']['paymetricAuthorizationDate'] : NULL,
      "paymetricTransactionId"=> $cardDetails['#items']['paymetricTransactionId'] ? $cardDetails['#items']['paymetricTransactionId'] : NULL,
      "paymetricAuthorizationTime"=> $cardDetails['#items']['paymetricAuthorizationTime'] ? $cardDetails['#items']['paymetricAuthorizationTime'] : NULL,
      "paymetricMessage"=> $cardDetails['#items']['paymetricMessage'] ? $cardDetails['#items']['paymetricMessage'] : NULL,
      "paymetricAVSAddress"=> $cardDetails['#items']['paymetricAVSAddress'] ? $cardDetails['#items']['paymetricAVSAddress'] : NULL,
      "paymetricAVSCode"=> $cardDetails['#items']['paymetricAVSCode'] ? $cardDetails['#items']['paymetricAVSCode'] : NULL,
      "paymetricAVSZipCode"=> $cardDetails['#items']['paymetricAVSZipCode'] ? $cardDetails['#items']['paymetricAVSZipCode'] : NULL,
      "paymetricResponseCode"=> $cardDetails['#items']['paymetricResponseCode'] ? $cardDetails['#items']['paymetricResponseCode'] : NULL,
      "paymetricAuthorizeAmount"=> $cardDetails['#items']['paymetricAuthorizeAmount'] ? $cardDetails['#items']['paymetricAuthorizeAmount'] : NULL,
      "valid"=> true,
      "errors"=> [],
      "orderWeight"=> 0.19,
    ];

    return $final_objconstruct_req;
  }

  /**
   * {@inheritdoc}
   */
  public function orderConfOrderDetails($apiDecodedResponse = NULL, $ccType = NULL) {
    $items = [];
    if (!empty($apiDecodedResponse)) {
      $items['customerOrderNumber'] = $apiDecodedResponse['customerOrderNumber'];
      $items['orderNumber'] = $apiDecodedResponse['orderNumber'];
      $items['orderSubTotal'] = $apiDecodedResponse['orderSubTotal'];
      $items['orderTotal'] = $apiDecodedResponse['orderTotal'];
      $items['shippingCharge'] = $apiDecodedResponse['shippingCharge'];

      //Order Details
      if(!empty($apiDecodedResponse['orderItems'])) {
        foreach ($apiDecodedResponse['orderItems'] as $orderItems) {
          $items['orderItems'][] = [
            'imageUrl' => $orderItems['imageFilename'],
            'title' => $orderItems['description'],
            'estimatedShipDate' => date('m/d/Y', $orderItems['estShipDate']),
            'qty' => $orderItems['quantity'],
            'unitPrice' => $this->getPriceFormat($orderItems['unitPrice']),
            'extendedPrice' => $this->getPriceFormat($orderItems['extendedPrice']),
            'sku' => $orderItems['skuCode'],
          ];
        }
      }
      //Shipping Address
      if(isset($apiDecodedResponse['shippingAddress'])) {
        $items['shippingAddress'][] = [
          'fullName' => $apiDecodedResponse['shippingAddress']['fullName'],
          'address1' => $apiDecodedResponse['shippingAddress']['address1'],
          'address2' => $apiDecodedResponse['shippingAddress']['address2'],
          'city' => $apiDecodedResponse['shippingAddress']['city'],
          'state' => $apiDecodedResponse['shippingAddress']['state'],
          'zip5' => $apiDecodedResponse['shippingAddress']['zip5'],
        ];
      }
      //Billing Address
      if(isset($apiDecodedResponse['billingAddress'])) {
        $items['billingAddress'][] = [
          'fullName' => $apiDecodedResponse['billingAddress']['fullName'],
          'address1' => $apiDecodedResponse['billingAddress']['address1'],
          'address2' => $apiDecodedResponse['billingAddress']['address2'],
          'city' => $apiDecodedResponse['billingAddress']['city'],
          'state' => $apiDecodedResponse['billingAddress']['state'],
          'zip5' => $apiDecodedResponse['billingAddress']['zip5'],
        ];
      }

      //Phone number
      if(isset($apiDecodedResponse['phone'])) {
        $items['phone'] = $apiDecodedResponse['phone'];
      }
      //Email id
      if(isset($apiDecodedResponse['email'])) {
        $items['email'] = $apiDecodedResponse['email'];
      }

      //ShiptoNumber
      if(isset($apiDecodedResponse['shiptoNumber'])) {
        $items['shiptoNumber'] = $apiDecodedResponse['shiptoNumber'];
      }

      //billtoNumber
      if(isset($apiDecodedResponse['billtoNumber'])) {
        $items['billtoNumber'] = $apiDecodedResponse['billtoNumber'];
      }

      //cartID
      if(isset($apiDecodedResponse['cartID'])) {
        $items['cartID'] = $apiDecodedResponse['cartID'];
      }

      //customerOrderNumber
      if(isset($apiDecodedResponse['customerOrderNumber'])) {
        $items['customerOrderNumber'] = $apiDecodedResponse['customerOrderNumber'];
      }

      //shippingServiceLevel
      if(isset($apiDecodedResponse['shippingServiceLevel'])) {
        $items['shippingServiceLevel'] = $apiDecodedResponse['shippingServiceLevel'];
      }

      //shippingCharge
      if(isset($apiDecodedResponse['shippingCharge'])) {
        $items['shippingCharge'] = $apiDecodedResponse['shippingCharge'];
      }

      //tax
      if(isset($apiDecodedResponse['tax'])) {
        $items['tax'] = $apiDecodedResponse['tax'];
      }

      $ccTypeConvert = "";
      if($ccType == "vi") {
        $ccTypeConvert = "VISA";
      }

      if($ccType == "mc") {
        $ccTypeConvert = "MC";
      }

      //valid flag
      if(isset($apiDecodedResponse['valid'])) {
        $items['valid'] = $apiDecodedResponse['valid'];
      }


      //Card and Payment conformation details
      if(!is_null($apiDecodedResponse['ccNumber'])) {
        $items['ccNumber'] = $apiDecodedResponse['ccNumber'];
        $items['ccConfirmationCode'] = $apiDecodedResponse['ccConfirmationCode'];
        $items['ccType'] = !empty($apiDecodedResponse['ccType']) ? $apiDecodedResponse['ccType'] : $ccTypeConvert;
        $items['ccName'] = $apiDecodedResponse['ccName'];
        $items['ccLastFour'] = $apiDecodedResponse['ccLastFour'];
        $items['ccExpireMonth'] = $apiDecodedResponse['ccExpireMonth'];
        $items['ccExpireYear'] = $apiDecodedResponse['ccExpireYear'];

        //Payment Details
        $items['paymetricAuthorizationCode'] = $apiDecodedResponse['paymetricAuthorizationCode'];
        $items['paymetricAuthorizationDate'] = $apiDecodedResponse['paymetricAuthorizationDate'];
        $items['paymetricTransactionId'] = $apiDecodedResponse['paymetricTransactionId'];
        $items['paymetricAuthorizationTime'] = $apiDecodedResponse['paymetricAuthorizationTime'];
        $items['paymetricMessage'] = $apiDecodedResponse['paymetricMessage'];
        $items['paymetricAVSAddress'] = $apiDecodedResponse['paymetricAVSAddress'];
        $items['paymetricAVSCode'] = $apiDecodedResponse['paymetricAVSCode'];
        $items['paymetricAVSZipCode'] = $apiDecodedResponse['paymetricAVSZipCode'];
        $items['paymetricResponseCode'] = $apiDecodedResponse['paymetricResponseCode'];
        $items['paymetricAuthorizeAmount'] = $apiDecodedResponse['paymetricAuthorizeAmount'];

      }

      //Error response
      $items['errors'][] = count($apiDecodedResponse['errors']);
      $items['errors'][] = $apiDecodedResponse['errors'];
    }
    return [
      '#items' => $items,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getorderId($cart = 0) {
    $orders = $this->entityTypeManager->loadByProperties([
        'uid' => $this->current_user->id(),
        'cart' => $cart,
      ]
    );
    return $orders;
  }

  /**
   * {@inheritdoc}
   */
  public function saveCartName($cartName = NULL) {
    $orderId = $this->getCurrentCartId();
    $order = $this->entityTypeManager->load($orderId);
    $order->field_cart_name->value = $cartName;
    $order->save();
  }

  /**
   * {@inheritdoc}
   */
  public function getCartName() {
    $orderId = $this->getCurrentCartId();
    if ($orderId) {
      $order = $this->entityTypeManager->load($orderId);
      return $order->field_cart_name->value;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getCurrentCartId() {
    $store_id = 1;
    $order_type = 'default';
    $store = $this->customEntityTypeManager->getStorage('commerce_store')
      ->load($store_id);
    $cart = $this->cart_provider->getCart($order_type, $store);
    if (!is_null($cart)) {
      $orderId = $cart->get('order_id')->getValue()[0]['value'];
      return $orderId;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function updateOrder($cartName = NULL) {
    $orderId = $this->getCurrentCartId();
    $order = $this->entityTypeManager->load($orderId);
    $order->state->value = 'completed';
    $order->save();
  }

  /**
   * Function to return the decimal numbers.
   */
  public function getPriceFormat($price) {
    return number_format((float) $price, 2, '.', '');
  }

  /**
   * Function to return the random number.
   */
  /*public function randomNumber($lenthArray) {
    $randomNumber = "";
    foreach ($lenthArray as $value) {
      $randomNumber .= $this->globalCheckoutServices->generateRandomNumber($value)."-";
    }
    return rtrim($randomNumber,"-");
  }*/

  /**
   * Function to return the unique number with 40 character.
   */
  /*public function generateRandomNumber($length) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $randomString = '';

    for ($i = 0; $i < $length; $i++) {
      $index = rand(0, strlen($characters) - 1);
      $randomString .= $characters[$index];
    }

    return $randomString;
  }*/

  /**
   * Function to return Order Status.
   */
  /*public function orderStstusWebCall($orderDetails) {

    // Check to Ecommerce Order Status API.
    $options = [];
    $options['http_errors'] = FALSE;
    $options['auth'] = [$this->configFactory->get('delta_api_username'), $this->configFactory->get('delta_api_password')];
    $apiUrl = trim($this->configFactory->get('order_status_api'))."orderNumber=".$orderDetails['orderno']."&phoneNumber=&email=".$orderDetails['email']."&zipCode=".$orderDetails['zipcode']."&source";
    $request = $this->httpClient->request('GET', $apiUrl, $options);
    $response = json_decode($request->getBody(),true);
    return $response;
  }*/

  /**
   * Function to return Ordered Product info.
   */
  public function orderprdInfoApiCall($orderDetails) {

    // Check to Ecommerce Ordered Product info API.
    $options = [];
    $options['http_errors'] = FALSE;
    $options['auth'] = [$this->configFactory->get('delta_api_username'), $this->configFactory->get('delta_api_password')];
    $apiUrl = trim($this->configFactory->get('ordered_product_info_api'))."orderNumber=".$orderDetails['orderNo']."&companyNumber=".$this->configFactory->get('companyNumber')."&documentType=".$this->configFactory->get('documentType')."&source=".$this->configFactory->get('site_name')."&status=All";
    $request = $this->httpClient->request('GET', $apiUrl, $options);
    $response = json_decode($request->getBody(),true);
    return $response;
  }

  /**
   * Function to change current checkout page Step.
   */
  public function changeStepToShip() {
    $this->tempStoreFactory->set('step', 1);
    return "T";
  }

}

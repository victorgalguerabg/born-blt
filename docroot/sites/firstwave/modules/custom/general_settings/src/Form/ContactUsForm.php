<?php

namespace Drupal\general_settings\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Form\FormBuilderInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ContactUS form to have custom contact us form.
 */
class ContactUsForm extends FormBase {
  /**
   * The form builder.
   *
   * Drupal\Core\Form\FormBuilderInterface.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * Class construtor.
   */
  public function __construct(FormBuilderInterface $formBuilder) {
    $this->formBuilder = $formBuilder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
        $container->get('form_builder')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'contact_us_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = [];
    $form['#id'] = 'contact-us-form';

    $form['name'] = [
      '#type' => 'textfield',
      '#attributes' => [
        'placeholder' => 'NAME',
      ],
    ];

    $form['email'] = [
      '#type' => 'textfield',
      '#attributes' => [
        'placeholder' => 'EMAIL',
      ],
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => 'Submit',
      '#ajax' => [
        'callback' => '::ajaxCallback',
        'wrapper' => 'contact-us-form',
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    $input = $form_state->getValues();
    if (empty($input['name'])) {
      $form_state->setErrorByName('name', 'Name is required');
    }

    if (empty($input['email'])) {
      $form_state->setErrorByName('email', 'Email is required');
    }

    if (!empty($input['email']) && (!valid_email_address($input['email']))) {
      $form_state->setErrorByName('email', 'Please enter valid email address');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * Ajax callback for the default submit button.
   */
  public function ajaxCallback(array &$form, FormStateInterface $form_state) {

    if ($form_state->hasAnyErrors()) {
      return $form;
    }
    $input = $form_state->getValues();
    $form_state->setRebuild(TRUE);
    $client = new Client();
    $request = new Request(
        "POST", "https://api.deltafaucet.com/salesforce/case", [
          "Authorization" => "Basic U0swMDEzOkRlbHRhMTIz",
          "Content-Type" => "multipart/form-data; charset=utf-8; boundary=__X_PAW_BOUNDARY__",
        ], "--__X_PAW_BOUNDARY__\r\nContent-Disposition: form-data; name=\"lastName\"\r\n\r\nLastname\r\n--__X_PAW_BOUNDARY__\r\nContent-Disposition: form-data; name=\"firstName\"\r\n\r\n" . $input['name'] . "\r\n--__X_PAW_BOUNDARY__\r\nContent-Disposition: form-data; name=\"country\"\r\n\r\nUSA\r\n--__X_PAW_BOUNDARY__\r\nContent-Disposition: form-data; name=\"city\"\r\n\r\nNew York\r\n--__X_PAW_BOUNDARY__\r\nContent-Disposition: form-data; name=\"address1\"\r\n\r\newrwerewr\r\n--__X_PAW_BOUNDARY__\r\nContent-Disposition: form-data; name=\"postalCode\"\r\n\r\n08536\r\n--__X_PAW_BOUNDARY__\r\nContent-Disposition: form-data; name=\"firstName\"\r\n\r\newrewrewr\r\n--__X_PAW_BOUNDARY__\r\nContent-Disposition: form-data; name=\"topic\"\r\n\r\ncleaning-and-care\r\n--__X_PAW_BOUNDARY__\r\nContent-Disposition: form-data; name=\"helpDescription\"\r\n\r\nfsdfsdfdfdsfsdfsfsfs\r\n--__X_PAW_BOUNDARY__\r\nContent-Disposition: form-data; name=\"state\"\r\n\r\nNY\r\n--__X_PAW_BOUNDARY__\r\nContent-Disposition: form-data; name=\"originEmail\"\r\n\r\nPeerlessService@peerlessfaucet.com\r\n--__X_PAW_BOUNDARY__\r\nContent-Disposition: form-data; name=\"email\"\r\n\r\n" . $input['email'] . "\r\n--__X_PAW_BOUNDARY__--\r\n");

    $response = $client->send($request);
    $output = $response->getBody();
    $decodedOutput = json_decode($output);
    if ($decodedOutput->success) {
      $response = new AjaxResponse();
      $contactUsForm = $this->formBuilder->formBuilder()->rebuildForm('contact_us_form', $form_state);
      $contactUsForm['name']['#value'] = '';
      $contactUsForm['email']['#value'] = '';
      return $contactUsForm;
    }
  }

}

<?php

namespace Drupal\general_settings\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\ConfigFormBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Configure Websphere settings for this site.
 */
class GeneralSettingsConfigForm extends ConfigFormBase {

  /**
   * The entity type Manager.
   *
   * Drupal\Core\Entity\EntityTypeManagerInterface.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Class constructor.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
        $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'general_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['general_settings.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('general_settings.settings');
    $validators = [
      'file_validate_extensions' => ['png', 'jpg'],
    ];
    $form['logo'] = [
      '#type' => 'managed_file',
      '#name' => 'logo',
      '#title' => $this->t('Logo'),
      '#size' => 20,
      '#description' => $this->t('Upload Firstwavelab logo'),
      '#upload_validators' => $validators,
      '#upload_location' => 'public://',
    ];

    $form['site_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Site Name'),
      '#default_value' => $config->get('site_name'),
      '#description' => $this->t('Enter the Site Name here.'),
    ];

    $form['site_slogan'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Site Slogan'),
      '#default_value' => $config->get('site_slogan'),
      '#description' => $this->t('Enter the Site Slogan here.'),
    ];

    $form['video_text'] = [
      '#type' => 'details',
      '#title' => $this->t('Video Overlay Texts'),
      '#open' => TRUE,
      '#weight' => 100,
    ];
    $form['video_text']['slogan_1'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Video Slogan 1'),
      '#default_value' => $config->get('video.slogan_1'),
      '#description' => $this->t('Enter the Animation video First Slogan.'),
    ];

    $form['video_text']['slogan_2'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Video Slogan 2'),
      '#default_value' => $config->get('video.slogan_2'),
      '#description' => $this->t('Enter the Animation video Second slogan.'),
    ];

    $form['video_text']['slogan_3'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Video Slogan 3'),
      '#default_value' => $config->get('video.slogan_3'),
      '#description' => $this->t('Enter the Animation video Third slogan.'),
    ];

    $form['video_text']['slogan_4'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Video Slogan 4'),
      '#default_value' => $config->get('video.slogan_4'),
      '#description' => $this->t('Enter the Animation video Fourth slogan.'),
    ];

    $form['footer_social_links'] = [
      '#type' => 'details',
      '#title' => $this->t('Contact Us configuration'),
      '#open' => TRUE,
      '#weight' => 100,
    ];

    $form['footer_social_links']['tel'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Phone Number'),
      '#description' => $this->t('Enter FirstWaveLabs Contact Number.'),
      '#default_value' => $config->get('contact.tel'),
    ];

    $form['footer_social_links']['contact_email'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Contact Email'),
      '#description' => $this->t('Enter FirstWaveLabs Contact Email.'),
      '#default_value' => $config->get('contact.email'),
    ];

    $form['footer_social_links']['facebook'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Facebook Link'),
      '#description' => $this->t('Enter FirstWaveLabs Facebook profile.'),
      '#default_value' => $config->get('social.fb_link'),
    ];

    $form['footer_social_links']['twitter'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Twitter Link'),
      '#description' => $this->t('Enter FirstWaveLabs Twitter profile.'),
      '#default_value' => $config->get('social.twitter_link'),
    ];

    $form['footer'] = [
      '#type' => 'details',
      '#title' => $this->t('Footer Details'),
      '#open' => TRUE,
      '#weight' => 100,
    ];

    $form['footer']['footer_site_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title'),
      '#default_value' => $config->get('footer_site_name'),
    ];

    $form['footer']['footer_subtitle'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Sub Title'),
      '#default_value' => $config->get('footer_subtitle'),
    ];

    $form['footer']['footer_address'] = [
      '#type' => 'textarea',
      '#rows' => 5,
      '#title' => $this->t('Address'),
      '#default_value' => $config->get('footer_address'),
    ];

    $form['footer']['footer_copyright'] = [
      '#type' => 'textarea',
      '#rows' => 5,
      '#title' => $this->t('Copyright'),
      '#default_value' => $config->get('footer_copyright'),
    ];

    $form['footer']['footer_order_status_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Order Status Page URL'),
      '#default_value' => $config->get('footer_order_status_url'),
    ];
    $form['footer']['footer_order_status_url_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Order Status Page URL Text'),
      '#default_value' => $config->get('footer_order_status_url_text'),
    ];

    $form['commerce'] = [
      '#type' => 'details',
      '#title' => $this->t('Commerce Configurations'),
      '#open' => TRUE,
      '#weight' => 100,
    ];

    $form['commerce']['max_quantity'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Max Quantity'),
      '#default_value' => $config->get('max_quantity'),
    ];

    $form['comment'] = [
      '#type' => 'details',
      '#title' => $this->t('Comment Configurations'),
      '#open' => TRUE,
      '#weight' => 100,
    ];

    $form['comment']['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Comment Title'),
      '#default_value' => $config->get('title'),
    ];

    $form['comment']['description'] = [
      '#type' => 'textarea',
      '#rows' => 5,
      '#title' => $this->t('Comment Description'),
      '#default_value' => $config->get('description'),
    ];

    $form['comment']['button_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Comment Button label'),
      '#default_value' => $config->get('button_label'),
    ];

    $form['captcha'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Captcha'),
      '#default_value' => $config->get('captcha'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if (!empty($form_state->getValue('logo')[0])) {
      $file = $this->entityTypeManager->getStorage('file')
        ->load($form_state->getValue('logo')[0]);

      $file->setPermanent();
      $file->save();
      $url = file_create_url($file->get('uri')->value);
      $this->configFactory->getEditable('general_settings.settings')
        ->set('logo', $url)->save();
    }

    $this->configFactory->getEditable('general_settings.settings')
      ->set('site_name', $form_state->getValue('site_name'))
      ->set('site_slogan', $form_state->getValue('site_slogan'))
      ->set('video.slogan_1', $form_state->getValue('slogan_1'))
      ->set('video.slogan_2', $form_state->getValue('slogan_2'))
      ->set('video.slogan_3', $form_state->getValue('slogan_3'))
      ->set('video.slogan_4', $form_state->getValue('slogan_4'))
      ->set('social.fb_link', $form_state->getValue('facebook'))
      ->set('social.twitter_link', $form_state->getValue('twitter'))
      ->set('contact.tel', $form_state->getValue('tel'))
      ->set('contact.email', $form_state->getValue('contact_email'))
      ->set('footer_site_name', $form_state->getValue('footer_site_name'))
      ->set('footer_subtitle', $form_state->getValue('footer_subtitle'))
      ->set('footer_address', $form_state->getValue('footer_address'))
      ->set('footer_copyright', $form_state->getValue('footer_copyright'))
      ->set('footer_order_status_url', $form_state->getValue('footer_order_status_url'))
      ->set('footer_order_status_url_text', $form_state->getValue('footer_order_status_url_text'))
      ->set('max_quantity', $form_state->getValue('max_quantity'))
      ->set('title', $form_state->getValue('title'))
      ->set('description', $form_state->getValue('description'))
      ->set('button_label', $form_state->getValue('button_label'))
      ->set('captcha', $form_state->getValue('captcha'))
      ->save();
  }

}

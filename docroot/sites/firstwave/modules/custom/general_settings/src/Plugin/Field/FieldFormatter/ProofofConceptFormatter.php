<?php

namespace Drupal\general_settings\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceFormatterBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'paragraph_proof_of_concept' formatter.
 *
 * @FieldFormatter(
 *   id = "prrof_of_concept",
 *   label = @Translation("Proof Of Concept"),
 *   field_types = {
 *     "entity_reference_revisions"
 *   }
 * )
 */
class ProofofConceptFormatter extends EntityReferenceFormatterBase {

  /**
   * The form builder.
   *
   * Drupal\Core\Entity\EntityTypeManagerInterface.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Class constructor.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    foreach ($this->getEntitiesToView($items, $langcode) as $entity) {
      // Description.
      if (NULL != $entity->field_description->getValue()) {
        $imageComponents['description'] = $entity->field_description->getValue()[0]['value'];
      }
      // Text alignment.
      if (NULL != $entity->field_text_alignment->getValue()) {
        $imageComponents['textAlignment'] = $entity->field_text_alignment->getValue()[0]['value'];
      }

      // Image url.
      if (NULL != $entity->field_image->referencedEntities()) {
        $imageDetails = $entity->field_image->referencedEntities();
        $targetID = $imageDetails[0]->image->getValue()[0]['target_id'];

        $file = $this->entityTypeManager->getStorage('file')->load($targetID);
        $imageComponents['imageUrl'] = file_create_url($file->getFileUri());
      }
    }

    return [
      '#theme' => 'proof_of_concept',
      '#items' => $imageComponents,
    ];
  }

}

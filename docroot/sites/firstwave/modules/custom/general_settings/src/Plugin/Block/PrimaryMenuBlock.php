<?php

namespace Drupal\general_settings\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides my custom block.
 *
 * @Block(
 *   id = "Primary_Menu_block",
 *   admin_label = @Translation("Primary Menu Block"),
 *   category = @Translation("Blocks")
 * )
 */
class PrimaryMenuBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Configuration Factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * @param array $configuration
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ConfigFactoryInterface $configFactory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->configFactory = $configFactory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
        $configuration, $plugin_id, $plugin_definition, $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $settings = $this->configFactory->get('general_settings.settings');
    $items['site_name'] = $settings->get('site_name');
    $items['site_slogan'] = $settings->get('site_slogan');
    $items['logoPath'] = $settings->get('logo');
    $items['fb_link'] = $settings->get('social.fb_link');
    $items['twitter_link'] = $settings->get('social.twitter_link');
    $user = \Drupal::currentUser()->getRoles();
    if (in_array("administrator", $user)) {
      $items['admin'] = 'TRUE';
    }

    return [
      '#type' => 'markup',
      '#theme' => 'primary_menu',
      '#items' => $items,
      '#cache' => [
        'max-age' => 0,
      ],
    ];
  }

}

<?php

namespace Drupal\general_settings\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides my custom block.
 *
 * @Block(
 *   id = "animation_video_block",
 *   admin_label = @Translation("Animation Block"),
 *   category = @Translation("Blocks")
 * )
 */
class AnimationVideoBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Configuration Factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * @param array $configuration
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ConfigFactoryInterface $configFactory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->configFactory = $configFactory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
        $configuration, $plugin_id, $plugin_definition, $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $settings = $this->configFactory->get('general_settings.settings');
    $items['slogan_1'] = $settings->get('video.slogan_1');
    $items['slogan_2'] = $settings->get('video.slogan_2');
    $items['slogan_3'] = $settings->get('video.slogan_3');
    $items['slogan_4'] = $settings->get('video.slogan_4');

    return [
      '#type' => 'markup',
      '#theme' => 'animation_video',
      '#items' => $items,
      '#cache' => [
        'max-age' => 0,
      ],
    ];
  }

}

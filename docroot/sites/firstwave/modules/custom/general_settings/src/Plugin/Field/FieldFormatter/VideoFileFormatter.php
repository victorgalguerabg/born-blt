<?php

namespace Drupal\general_settings\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\file\Plugin\Field\FieldFormatter\DescriptionAwareFileFormatterBase;

/**
 * Plugin implementation of the 'video_file_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "video_file_formatter",
 *   label = @Translation("Video File Formatter"),
 *   field_types = {
 *     "file"
 *   }
 * )
 */
class VideoFileFormatter extends DescriptionAwareFileFormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($this->getEntitiesToView($items, $langcode) as $delta => $file) {
      $ffmpeg = "/usr/bin/ffmpeg";

      $item = $file->toArray();

      $url = DRUPAL_ROOT . $item['url'][0]['value'];
      $videoItems['url'] = $item['url'][0]['value'];
      $videoName = $item['filename'][0]['value'];
      $videoNameArray = explode(".", $videoName);

      $input = $url;
      $output = DRUPAL_ROOT . "/sites/default/files/" . $videoNameArray[0] . ".png";
      $videoItems['thumbnailUrl'] = "/sites/default/files/" . $videoNameArray[0] . ".png";

      $ffmpeg = "/usr/bin/ffmpeg";
      exec("$ffmpeg -i {$input} -vf fps=1 {$output} 2>&1", $out);
      $elements[$delta] = [
        '#theme' => 'video_file_format',
        '#items' => $videoItems,
        '#cache' => [
          'tags' => $file->getCacheTags(),
        ],
      ];
    }
    return $elements;
  }

}

<?php

namespace Drupal\general_settings\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides my custom block.
 *
 * @Block(
 *   id = "contact_us_block",
 *   admin_label = @Translation("Contact Us Block"),
 *   category = @Translation("Blocks")
 * )
 */
class ContactUsBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * Class construtor.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, FormBuilderInterface $formBuilder) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->formBuilder = $formBuilder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
        $configuration, $plugin_id, $plugin_definition, $container->get('form_builder')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $items = [];
    $current_path = \Drupal::service('path.current')->getPath();
    $items['contact'] = 'FALSE';
    if ($current_path == 'contact') {
      $items['contact'] = 'TRUE';
    }
    return [
      '#theme' => 'contactus',
      '#items' => $items,
      '#cache' => [
        'max-age' => 0,
      ],
    ];
  }

}

<?php

namespace Drupal\general_settings\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Response;
use Drupal\node\Entity\Node;

/**
 * Class NewsletterSubscribe to achive subscribe functionlity.
 */
class NewsletterSubscribe extends ControllerBase {

  /**
   * Function subscribe to update the contact entity.
   */
  public static function subscribe() {
    $name = \Drupal::request()->request->get('name');
    $lastName = \Drupal::request()->request->get('lastname');
    $email = \Drupal::request()->request->get('email');
    try {
      $node = Node::create(['type' => 'contacts']);
      $node->set('title', $name);
      $node->set('field_first_name', $name);
      $node->set('field_last_name', $lastName);
      $node->set('field_email', $email);
      $node->save();

      $response = new Response();
      $response->setContent('Success');
      return $response;
    }
    catch (Exception $e) {
      $response = new Response();
      $response->setContent('Error');
      return $response;
    }
  }

}

<?php

namespace Drupal\general_settings;

use Drupal\Core\Form\FormStateInterface;

/**
 *
 */
class BusinessRules {

  /**
   *
   */
  public static function alterFieldCaption(array &$element, FormStateInterface &$form_state, array &$complete_form) {

    // Use whatever logic to target instances of `field_caption`.
    if (reset($element['#parents']) == 'field_future_products') {
      $element['value']['#maxlength'] = 40;
    }
    if (reset($element['#parents']) == 'field_how_it_works') {
      $element['value']['#maxlength'] = 50;
    }
    return $element;

  }

}

<?php

namespace Drupal\Tests\custom_user_creation\Unit;

use Drupal\Tests\UnitTestCase;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Request;

/**
 * @coversDefaultClass \Drupal\Core\Form\FormValidator
 * @group Form
 */
class CustomUserCreateTest extends UnitTestCase {

  /**
   * A logger instance.
   *
   * @var \Psr\Log\LoggerInterface|\PHPUnit_Framework_MockObject_MockObject
   */
  protected $logger;

  /**
   * The CSRF token generator to validate the form token.
   *
   * @var \Drupal\Core\Access\CsrfTokenGenerator|\PHPUnit_Framework_MockObject_MockObject
   */
  protected $csrfToken;

  /**
   * The form builder.
   *
   * @var \Drupal\Core\Form\FormErrorHandlerInterface|\PHPUnit_Framework_MockObject_MockObject
   */
  protected $formErrorHandler;

  /**
   * The form builder.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface|\PHPUnit_Framework_MockObject_MockObject
   */
  protected $formBuilder;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    $this->logger = $this->getMock('Psr\Log\LoggerInterface');
    $this->csrfToken = $this->getMockBuilder('Drupal\Core\Access\CsrfTokenGenerator')
      ->disableOriginalConstructor()
      ->getMock();
    $this->formErrorHandler = $this->getMock('Drupal\Core\Form\FormErrorHandlerInterface');
    $this->formBuilder = $this->getMock('Drupal\Core\Form\FormBuilderInterface');
  }

  /**
   * Make sure the site still works. For now just check the front page.
   */
  public function testCustomUserCreation() {
    $request_stack = new RequestStack();
    $request = new Request([], [], [], [], [], ['REQUEST_URI' => '/user-register']);
    $request_stack->push($request);
    $this->csrfToken->expects($this->once())
      ->method('validate')
      ->will($this->returnValue(FALSE));

    $form_validator = $this->getMockBuilder('Drupal\Core\Form\FormValidator')
      ->setConstructorArgs([$request_stack, $this->getStringTranslationStub(), $this->csrfToken, $this->logger, $this->formErrorHandler])
      ->setMethods(['doValidateForm'])
      ->getMock();
    $form_validator->expects($this->never())
      ->method('doValidateForm');

    $form['email']['#value'] = 'custom_user_register';
    $form_state = $this->getMockBuilder('Drupal\Core\Form\FormState')
      ->setMethods(['setErrorByName'])
      ->getMock();
    $form_state->expects($this->once())
      ->method('setErrorByName')
      ->with('email', 'The form has become outdated. Copy any unsaved work in the form below and then <a href="/test/example?foo=bar">reload this page</a>.');
    $form_state->setValue('email', 'some_random_token');
    $form_validator->validateForm('custom_user_register', $form, $form_state);
    $this->assertTrue($form_state->isValidationComplete());
  }

}

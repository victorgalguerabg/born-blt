<?php

namespace Drupal\custom_user_creation\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Session\AccountInterface;

/**
 * Builds an example page.
 */
class UserAccess {

  /**
   * Checks access for a specific request.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for this account.
   */
  public function access(AccountInterface $account) {
    if ($account->id()) {
      return AccessResult::forbidden();
    }
    return AccessResult::allowed();
  }

}

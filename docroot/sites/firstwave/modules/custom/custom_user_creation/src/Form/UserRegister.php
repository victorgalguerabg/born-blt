<?php

namespace Drupal\custom_user_creation\Form;

/**
 * List down all the dependent classes going to need.
 */
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Entity\User;

/**
 * Class to have custom user register form.
 */
class UserRegister extends FormBase {

  /**
   * Defines the form_id to use it in other places to get/render the form.
   */
  public function getFormId() {
    return 'custom_user_register';
  }

  /**
   * Build the form for custom user registration..
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = [];
    $form['firstName'] = [
      '#type' => 'textfield',
      '#title' => $this->t('First Name'),
      '#required' => TRUE,
    ];

    $form['lastName'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Last Name'),
      '#required' => TRUE,
    ];

    $form['email'] = [
      '#type' => 'textfield',
      '#title' => $this->t('E-Mail'),
      '#required' => TRUE,
    ];

    $form['password'] = [
      '#type' => 'password',
      '#title' => $this->t('Password'),
      '#required' => TRUE,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * Validate the custom user registration form.
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Validate the user email address.
    $email = $form_state->getValue('email');
    if (!valid_email_address($email)) {
      $form_state->setErrorByName('email', $this->t('Oops!!!. Please enter valid email address'));
    }
  }

  /**
   * Submit the custom user registration form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Get all the input values to register the user.
    $inputValues = $form_state->getValues();

    // Assigning Email id as username.
    if (!empty($inputValues)) {
      $user = User::create([
        'name' => $inputValues['email'],
        'mail' => $inputValues['email'],
        'pass' => $inputValues['password'],
        'status' => 1,
      ]);
      $user->sett('field_first_name', $inputValues['firstName']);
      $user->set('field_last_name', $inputValues['lastName']);

      // Save the user with associated data.
      $user->save();
    }
  }

}

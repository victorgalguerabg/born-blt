(function ($, Drupal) {

    Drupal.behaviors.shipping_form_validate = {
        attach: function (context, settings) {

            $('.checkout-shipping input[name="shipping_option"]:checked').trigger("click");

            // Append Shipping Price to Order Details.
            $(".shipping-option").once().on("click", function () {

                var selectedOption = $(this).val();
                if (selectedOption == "CGR") {
                    var currentSelectedValueCgr = $(this).attr("data-cgr");
                    var currentTotalCgr = getCurrentTotal(currentSelectedValueCgr);
                    $(".order-shipping-price").html("$"+currentSelectedValueCgr);
                    $(".order-total-line__total .order-total-line-value").html("$"+currentTotalCgr);
                }

                if (selectedOption == "CND") {
                    var currentSelectedValue = $(this).attr("data-cnd");
                    var currentTotal = getCurrentTotal(currentSelectedValue);
                    $(".order-total-line__total .order-total-line-value").html("$"+currentTotal);
                    $(".order-shipping-price").html("$"+currentSelectedValue);
                }
            });

            function getCurrentTotal(selectedOption) {
                var currentTotal = $.trim($(".shipping-order-subtotal").text());
                var splitTot = currentTotal.split("$");
                var total = parseFloat(splitTot[1]) + parseFloat(selectedOption);
                return total.toFixed(2);
            }
        }
    };

    Drupal.behaviors.orderSatus = {
        attach: function (context, settings) {
            $("#order-status").once().on("click", function (e) {
                e.preventDefault();

                var orderNo = $(this).attr("data-orderno");
                var email = $(this).attr("data-email");
                var zip = $(this).attr("data-zip");

                $.ajax({
                    url: "/ajax-order-status",
                    type: 'POST',
                    //data: { "orderno":"1612830","email": "ankitatest2019@gmail.com", "zip":"1234567" },
                    data: { "orderno": orderNo,"email": email, "zip": zip },
                    async: false,
                    success: function (result) {
                        if(result[0]['url'] == 'T') {
                            window.location.href = "/order-status-results";
                        }
                        else {
                            window.location.href = "/order-status";
                        }
                    },
                    error: function (result) {
                        $('html, body').animate({scrollTop:0},500);
                    }
                });
            });
        }
    };

})(jQuery, Drupal);
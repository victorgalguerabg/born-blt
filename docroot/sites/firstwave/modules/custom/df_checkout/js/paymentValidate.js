(function ($, Drupal) {

  Drupal.behaviors.payment_form_validate = {
    attach: function (context, settings) {

      $(".ordersubmitbtn").css("display", "none");
      //bill_ship_address_same
      $('#edit-bill-ship-address-same').on("click", function(){
        if($(this).prop("checked") === true){
          $("#edit-payment-info").css("display", "none");
          $(this).val(0);
        }
        else if($(this).prop("checked") === false){
          $("#edit-payment-info").css("display", "flex");
          $(this).val(1);
        }
      });

      $(".bill-ship-edit").once().on("click", function () {
        $.ajax({
          url: "/bill-to-ship",
          type: 'GET',
          async: false,
          success: function (result) {
            window.location.href = "/checkout";
          },
          error: function (result) {

          }
        });
      });
    }
  };

  Drupal.behaviors.payment_validate = {
    attach: function (context, settings) {
      // Attach a click listener to the clear button.
      var submitBtn = document.getElementById('payment-submit-button');
      submitBtn.addEventListener('click', function (event) {
        var errorCount = formValidation(event);
          var iframeUrl = $('#IFrame').attr('src');
          var access_token = $('#edit-access-token').val();
          if (iframeUrl) {
            $XIFrame.validate({
              iFrameId: 'dieCommFrame',
              targetUrl: iframeUrl,
              autosizeheight: true,
              onValidate: function(e) {
                  if (e) {
                    console.log(errorCount);
                    if(errorCount == 1) {
                      $XIFrame.submit({
                        autosizeheight: true,
                        iFrameId: 'dieCommFrame',
                        targetUrl: iframeUrl,
                        onSuccess: function (msg) {
                          var message = JSON.parse(msg);
                          if (message && message.data.HasPassed) {
                            // all good, send the form:
                            if (errorCount) {
                            $.ajax({
                              url: "/paymetric/response_packet/" + access_token,
                              contentType: "application/xml",
                              dataType: 'xml',
                              async: false,
                              success: function (result) {
                                if($(result).find('RequestError').length !== 0) {
                                  $(".error-on-paymetric").html("<span>Something Went wrong, page will reload after 3 seconds, please try again.");
                                  $('html, body').animate({scrollTop:0},500);
                                  setTimeout(function () {
                                    location.reload(true);
                                  }, 3000);
                                }
                                else {
                                  if($(result).find('FormField')) {
                                    var cardResponse = "";
                                    $(result).find('FormField').each(function(){
                                      if("Card Type" === $(this).find('Name').text()) {
                                        cardResponse += $(this).find('Value').text();
                                      }
                                      if("Expiration Month" === $(this).find('Name').text()) {
                                        cardResponse += "|"+ $(this).find('Value').text();
                                      }
                                      if("Expiration Year" === $(this).find('Name').text()) {
                                        cardResponse += "|"+ $(this).find('Value').text();
                                      }
                                      if("Card Number" === $(this).find('Name').text()) {
                                        cardResponse += "|"+ $(this).find('Value').text();
                                      }
                                      if("Card Holder Name" === $(this).find('Name').text()) {
                                        cardResponse += "|"+ $(this).find('Value').text();
                                      }
                                      if("Card Security Code" === $(this).find('Name').text()) {
                                        cardResponse += "|"+ $(this).find('Value').text();
                                      }
                                    });
                                    $("#xml-access-token").val(cardResponse);

                                    setTimeout(function () {
                                      $('#edit-submit').trigger('click');
                                    }, 500);
                                  }
                                }
                              },
                              error: function (result) {
                                $(".error-on-paymetric").html("<span>Token expired, page will reload after 3 seconds, please try again.");
                                $('html, body').animate({scrollTop:0},500);
                                setTimeout(function () {
                                  location.reload(true);
                                }, 3000);
                              }
                            });
                          }
                        }
                        },
                        onError: function (msg) {
                          $(".error-on-paymetric").html("<span>Token expired, page will reload after 3 seconds, please try again.");
                          $('html, body').animate({scrollTop:0},500);
                          setTimeout(function () {
                            location.reload(true);
                          }, 3000);
                        }
                      });
                    }
                  }
                }
            });
          }
      }, false);

      // Billing Form Validation.
      function formValidation(event) {
        var i = 1;
        if($("#edit-bill-ship-address-same").val() == 1) {

          var fullname = $("#edit-payment-fullname").val();
          var zipcode = $("#edit-payment-zipcode").val();
          var city = $("#edit-payment-city").val();
          var address = $("#edit-payment-address-line").val();
          var error_list = "";

          $("input, textarea,select").focus(function(){
            $(this).next(".validation").empty();
          });

          //Name validation
          if (!$('#edit-payment-fullname').val()) {
            $("#edit-payment-fullname").next(".validation").remove();
            if ($("#edit-payment-fullname").next(".validation").length == 0) // only add if not added
            {
              i++;

              $("#edit-payment-fullname").after("<div class='validation'>Please enter name</div>");
            }
            event.preventDefault(); // prevent form from POST to server
          }
          else if(!$('#edit-payment-fullname').val().match(/^[a-zA-Z ]+$/)) {
            $("input, textarea").focus(function(){
              $(this).next(".validation").empty();
            });
            $("#edit-payment-fullname").next(".validation").remove();
            if ($("#edit-payment-fullname").next(".validation").length < 1) // only add if not added
            {
              i++;
              $("#edit-payment-fullname").after("<div class='validation'>Full Name is allowed Only letters and white space</div>");
            }
            event.preventDefault();
          }
          else {
            $("#edit-payment-fullname").next(".validation").remove();
          }

          // Address Validation
          if (!$('#edit-payment-address-line').val()) {
            $("#edit-payment-address-line").next(".validation").remove();
            if ($("#edit-payment-address-line").next(".validation").length == 0) // only add if not added
            {

              i++;

              $("#edit-payment-address-line").after("<div class='validation'>Address Line 1 is required</div>");
            }
            event.preventDefault(); // prevent form from POST to server
          }
          else if(!$('#edit-payment-address-line').val().match(/^[0-9a-zA-Z,\/ ]+$/)) {
            $("input, textarea").focus(function(){

              $(this).next(".validation").empty();
            });
            $("#edit-payment-address-line").next(".validation").remove();
            if ($("#edit-payment-address-line").next(".validation").length < 1) // only add if not added
            {
              i++;
              $("#edit-payment-address-line").after("<div class='validation'>Address Line 1 allowed only letters, Numbers</div>");
            }
            event.preventDefault();
          }
          else {
            $("#edit-payment-address-line").next(".validation").remove();
          }

          //City Validation
          if (!$('#edit-payment-city').val()) {
            $("#edit-payment-city").next(".validation").remove();
            if ($("#edit-payment-city").next(".validation").length == 0) // only add if not added
            {
              i++;
              $("#edit-payment-city").after("<div class='validation'>City is required</div>");
            }
            event.preventDefault(); // prevent form from POST to server
          }
          else if(!$('#edit-payment-city').val().match(/^[a-zA-Z ]+$/)) {
            $("input, textarea").focus(function(){

              $(this).next(".validation").empty();
            });
            $("#edit-payment-city").next(".validation").remove();
            if ($("#edit-payment-city").next(".validation").length < 1) // only add if not added
            {
              i++;
              $("#edit-payment-city").after("<div class='validation'>City allowed only letters, Numbers</div>");
            }
            event.preventDefault();
          }
          else {
            $("#edit-payment-city").next(".validation").remove();
          }

          // State Validation
          if($("#edit-payment-state").val() === "") {
            $("#edit-payment-state").next(".validation").remove();
            if($('.form-item-payment-state .validation').length < 1) {
              i++;
              $("#edit-payment-state").after("<div class='validation'>Select a state</div>");
            }
            else {
              $("#edit-payment-state").next(".validation").remove();
            }
            event.preventDefault();
          }

          //ZipCode Validatio
          if (!$('#edit-payment-zipcode').val()) {
            $("#edit-payment-zipcode").next(".validation").remove();
            if ($("#edit-payment-zipcode").next(".validation").length == 0) // only add if not added
            {
              i++;

              $("#edit-payment-zipcode").after("<div class='validation'>Zip is required</div>");
            }
            event.preventDefault(); // prevent form from POST to server
          }

          else if(!$('#edit-payment-zipcode').val().match(/^[0-9]+$/)) {
            $("input, textarea").focus(function(){

              $(this).next(".validation").empty();
            });
            $("#edit-payment-zipcode").next(".validation").remove();
            if ($("#edit-payment-zipcode").next(".validation").length < 1) // only add if not added
            {
              i++;
              $("#edit-payment-zipcode").after("<div class='validation'>Zip allowed only Numbers</div>");
            }
            event.preventDefault();
          }
          else {
            $("#edit-payment-zipcode").next(".validation").remove();
          }
        }
        if($("#edit-accept-order-details").prop("checked") === true){
          $("label[for='edit-accept-order-details']").next(".validation").remove();
          event.preventDefault();
        }

        else if($("#edit-accept-order-details").prop("checked") === false) {

          $("#edit-accept-order-details").addClass("validation");
          i++;
          $("label[for='edit-accept-order-details']").after("<div class='validation'>Please Accept Terms and Conditions</div>");
          event.preventDefault();
        }
        else {
          $('.form-item-accept-order-details').next(".validation").remove();
        }
        return i;
      }
    }
  };
})(jQuery, Drupal);

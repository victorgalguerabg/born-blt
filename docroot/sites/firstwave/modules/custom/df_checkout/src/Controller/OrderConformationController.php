<?php
/**
 * Created by PhpStorm.
 * User: manikandan.kariyappa@wearefmg.net
 * Date: 7/10/19
 * Time: 12:29 PM
 */

namespace Drupal\df_checkout\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\user\PrivateTempStoreFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\df_services\DfCheckoutServices;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\checkout_services\CheckoutServices;

/**
 * An Order Conformation controller.
 */
class OrderConformationController extends ControllerBase {

  /**
   * @var \Drupal\user\PrivateTempStore
   */
  protected $store;

  /**
   * @var \Drupal\user\PrivateTempStore
   */
  protected $orderStore;

  /**
   * @var \Drupal\user\PrivateTempStoreFactory
   */
  protected $tempStoreFactory;

  /**
   * @var Drupal\df_services\DfCheckoutServices
   */
  protected $services;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\checkout_services\CheckoutServices
   */
  protected $globalCheckoutServices;

  public function __construct(PrivateTempStoreFactory $temp_store_factory, DfCheckoutServices $services, EntityTypeManagerInterface $entityTypeManager, CheckoutServices $checkoutServices) {
    $this->tempStoreFactory = $temp_store_factory;
    $this->store = $this->tempStoreFactory->get('multistep_data');
    //$this->orderStore = $this->tempStoreFactory->get('orderStatus');
    $this->services = $services;
    $this->entityTypeManager = $entityTypeManager;
    $this->globalCheckoutServices = $checkoutServices;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('user.private_tempstore'),
      $container->get('df_services.checkoutflow'),
      $container->get('entity_type.manager'),
      $container->get('checkout_services.checkoutflow')
    );
  }

  /**
   * Returns a render-able array for a Order Conformation page.
   */
  public function orderDetails() {
    $orderDetails = json_decode($this->store->get("orderConformation"), TRUE);
    if(!empty($orderDetails)) {
      $renderable = [
        '#theme' => 'order_conformation',
        '#order_conformation' => $orderDetails,
      ];

      //Delete temp store
      $this->store->delete("orderConformation");

      return $renderable;
    }
    else {
      $response = new RedirectResponse("/");
      $response->send();
    }
  }

  /**
   * Returns a render-able array for a Order Status page.
   */
  public function orderStatus() {
    $orderStatusInfo = json_decode($this->store->get("orderStatusInfo"), TRUE);
    if(!empty($orderStatusInfo)) {

      $renderable = [
        '#theme' => 'order_status_detail',
        '#orderStatusInfo' => $orderStatusInfo,
      ];

      //Delete temp store
      $this->store->delete("orderStatusInfo");

      return $renderable;
    }
    else {
      $response = new RedirectResponse("/");
      $response->send();
    }
  }

  /**
   * Function to return Ajax Ordered Status info.
   */
  public function ajaxOrderStatusReq() {
    $arguments = ['orderno' => $_POST['orderno'], "email" => $_POST['email'], "zipcode" => $_POST['zip']];
    $orderStatus = $this->globalCheckoutServices->orderStstusWebCall($arguments);
    if(count($orderStatus) != 0) {
      $productInfo = ['orderNo' => $orderStatus[0]['orderNumber']];
      $responsePrdInfo = $this->services->orderprdInfoApiCall($productInfo);
      if(count($responsePrdInfo) != 0 && !isset($responseStatus['Error'])) {
        $orderDetails = [];
        $orderDetails['orderNo'] = $orderStatus[0]['orderNumber'];
        if(!empty($orderStatus[0]['datePlaced'])) {
          $orderDetails['orderPlaced'] = date("F d, Y", strtotime($orderStatus[0]['datePlaced']));
        }
        else {
          $orderDetails['orderPlaced'] = "";
        }
        // fetch the main product image if image is not available in the SAP.
        if (isset($responsePrdInfo['orderItems'][0]['imageFilename']) && $responsePrdInfo['orderItems'][0]['imageFilename']) {
          $orderDetails['productImg'] = $responsePrdInfo['orderItems'][0]['imageFilename'];
        }else{
          $products = $this->entityTypeManager
                ->getStorage('commerce_product_variation')
                ->loadByProperties(['sku' => $responsePrdInfo['orderItems'][0]['skuCode']]);
          foreach ($products as $product_id => $product_variation) {
            if ($product_variation->getSku() == $responsePrdInfo['orderItems'][0]['skuCode']) {
              $product = $product_variation->getProduct();
              $product_images = $product->get('field_image_url')->getValue();
              $product_images = array_column($product_images, 'value');
              if (!empty($product_images)) {
                $main_image = current($product_images);
              }
            }
          }
          $orderDetails['productImg'] = $main_image;
        }
        $orderDetails['subTotal'] = $responsePrdInfo['orderSubTotal'];
        $orderDetails['shipCharge'] = $responsePrdInfo['shippingCharge'];
        $orderDetails['salesTax'] = $responsePrdInfo['tax'];
        $orderDetails['orderTotal'] = $responsePrdInfo['orderTotal'];
        $orderDetails['ProductName'] = $responsePrdInfo['orderItems'][0]['description'];
        $orderDetails['productCode'] = $responsePrdInfo['orderItems'][0]['skuCode'];
        $orderDetails['orderStatus'] = $orderStatus[0]['status'];
        $orderDetails['trackingNumber'] = $responsePrdInfo['orderItems'][0]['trackingNumber'];
        $orderDetails['orderQty'] = $responsePrdInfo['orderItems'][0]['qtyOrdered'];
        $orderDetails['productPrice'] = $responsePrdInfo['orderItems'][0]['unitPrice'];

        $this->store->set("orderStatusInfo",json_encode($orderDetails));
        return (new AjaxResponse())->addCommand(
          new RedirectCommand('T')
        );
      }
      else {
        return (new AjaxResponse())->addCommand(
          new RedirectCommand('F')
        );
      }
    }
  }

  /**
   * Function to redirecting billing page to shipping page.
   */
  public function redirectShippingPage() {
    $step = $this->services->changeStepToShip();
    return (new AjaxResponse())->addCommand(
      new RedirectCommand($step)
    );
  }

}
<?php

namespace Drupal\df_checkout\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\user\PrivateTempStoreFactory;
use CommerceGuys\Addressing\Subdivision\SubdivisionRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\df_services\DfCheckoutServices;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\RedirectCommand;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\paymetric\PaymetricTokenServiceInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Url;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;

/**
 *
 */
class ShippingForm extends FormBase {

  /**
   * @var \Drupal\user\PrivateTempStoreFactory
   */
  protected $tempStoreFactory;

  /**
   * @var \Drupal\user\PrivateTempStore
   */
  protected $store;

  /**
   * @var \Drupal\user\PrivateTempStore
   */
  protected $checkoutServices;

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Symfony\Component\HttpFoundation\RequestStack definition.
   *
   * @var
   */
  protected $requestStack;

  /**
   * Drupal\paymetric\PaymetricTokenServiceInterface definition.
   *
   * @var \Drupal\paymetric\PaymetricTokenServiceInterface
   */

  protected $paymetric_token_service;
  protected $access_token;

  /**
   * Drupal\Core\Messenger\MessengerInterface definition.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The mail manager service.
   *
   * @var \Drupal\Core\Mail\MailManagerInterface
   */
  protected $mailManager;

  /**
   * The current active user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Drupal\Core\Logger\LoggerChannelFactoryInterface definition.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactorXyInterface
   */
  protected $logger;

  /**
   * Constructs a new PaymetricCheckoutForm object.
   */
  public function __construct(PrivateTempStoreFactory $temp_store_factory,
                              DfCheckoutServices $checkoutServices,
                              ConfigFactoryInterface $configFactory,
                              RequestStack $requestStack,
                              PaymetricTokenServiceInterface $paymetric_token_service,
                              MessengerInterface $messenger,
                              EntityTypeManagerInterface $entity_type_manager,
                              MailManagerInterface $mail_manager,
                              AccountProxyInterface $current_user,
                              LoggerChannelFactoryInterface $logger) {
    $this->tempStoreFactory = $temp_store_factory;
    $this->store = $this->tempStoreFactory->get('multistep_data');
    $this->checkoutServices = $checkoutServices;
    $this->configFactory = $configFactory->get('api_config.apisettings');
    $this->requestStack = $requestStack;
    $this->paymetric_token_service = $paymetric_token_service;
    $this->messenger = $messenger;
    $this->entityTypeManager = $entity_type_manager;
    $this->mailManager = $mail_manager;
    $this->currentUser = $current_user;
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('user.private_tempstore'),
      $container->get('df_services.checkoutflow'),
      $container->get('config.factory'),
      $container->get('request_stack'),
      $container->get('paymetric.token'),
      $container->get('messenger'),
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.mail'),
      $container->get('current_user'),
      $container->get('logger.factory')
    );
  }

  /**
   * return @form id
   */
  public function getFormId() {
    return 'shipping_details_form';
  }

  /**
   * Building Shipping and Billing Form
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    //Loading Multistep form based on steps
    if (empty($this->store->get('step'))) {
      $this->store->set('step', 1);
    }

    //Step 1 form
    if (!empty($this->store->get('step')) && ($this->store->get('step') == 1)) {

      // Fetching Previous URL
      $path = $this->store->get('previous_url');
      if(is_null($this->store->get('previous_url'))) {
        $path = $this->getHttpRef();
      }

      // Product API CAll for before shipping Page Load
      $apiResponse = $this->beforeShippingApiCall($path);
      if(!empty($apiResponse['#items']['errors'])) {
        if ($apiResponse['#items']['errors'][0] != 0) {
          $this->store->set('erroronproduct', $apiResponse['#items']['errors'][1][0]['message']);
          $response = new RedirectResponse($path);
          $response->send();
        }
      }

      if ($this->store->get('shipping_option') == 'CGR') {
        $shippingCharge = $this->configFactory->get('ground');
      }else if ($this->store->get('shipping_option') == 'CND') {
        $shippingCharge = $this->configFactory->get('priority');
      }else{
        $shippingCharge = $this->configFactory->get('ground');
      }
      $apiResponse['#items']['shippingCharge'] = $shippingCharge;
      $apiResponse['#items']['orderTotal'] = $apiResponse['#items']['orderTotal'] + $apiResponse['#items']['shippingCharge'];

      // paymetric API call JS
      $form['#attached']['library'][] = 'df_checkout/df_checkout_shipping';

      // Shipping Form Fields
      $form['previous_url']= array(
        '#type' => 'hidden',
        '#value' => $path,
        '#default_value' => $this->store->get('previous_url') ? $this->store->get('previous_url') : '',
      );

      $form['shipping_option'] = [
        '#type' => 'radios',
        '#title' => $this->t('Shipping Option'),
        '#options' => [
          'CGR' => $this->t('Ground ($'.$this->configFactory->get('ground').')'),
          'CND' => $this->t('Priority ($'.$this->configFactory->get('priority').')'),
        ],
        '#default_value' => $this->store->get('shipping_option') ? $this->store->get('shipping_option') : "CGR",
        '#required' => TRUE,
        '#attributes' => [
          'class' => ['shipping-option'],
          'data-cgr' => [$this->configFactory->get('ground')],
          'data-cnd' => [$this->configFactory->get('priority')]
        ],
      ];

      $form['shipping_details'] = [
        '#type' => 'container',
        '#attributes' => [
          'class' => 'shipping_details',
        ],
      ];

      $form['shipping_details']['shipping_address'] = [
        '#type' => 'item',
        '#markup' => $this->t('Shipping Address.'),
      ];

      $form['shipping_details']['fullname'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Full Name'),
        '#required' => TRUE,
        '#default_value' => $this->store->get('fullname') ? $this->store->get('fullname') : '',
      ];

      $form['shipping_details']['address_line'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Address Line 1'),
        '#required' => TRUE,
        '#default_value' => $this->store->get('address_line') ? $this->store->get('address_line') : '',
      ];

      $form['shipping_details']['address_line_optional'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Address Line 2 (Optional)'),
        '#default_value' => $this->store->get('address_line_optional') ? $this->store->get('address_line_optional') : '',
      ];

      $form['shipping_details']['city'] = [
        '#type' => 'textfield',
        '#title' => $this->t('City'),
        '#required' => TRUE,
        '#default_value' => $this->store->get('city') ? $this->store->get('city') : '',
      ];

      $form['shipping_details']['state'] = [
        '#type' => 'select',
        '#title' => $this->t('State'),
        '#options' => $this->getUsStates(),
        '#default_value' => $this->store->get('state') ? $this->store->get('state') : '',
      ];

      $form['shipping_details']['zipcode'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Zip'),
        '#min' => 5,
        '#default_value' => $this->store->get('zipcode') ? $this->store->get('zipcode') : '',
      ];

      $form['contact_details'] = [
        '#type' => 'container',
        '#attributes' => [
          'class' => 'contact_details',
        ],
      ];

      $form['contact_details']['contact_info'] = [
        '#type' => 'item',
        '#markup' => $this->t('Contact Information'),
        '#attributes' => [
          'class' => 'contact_info',
        ],
      ];

      $form['contact_details']['contact_info_desc'] = [
        '#type' => 'item',
        '#markup' => $this->t('Please provide your phone number in case we have questions about your order. Your email address will be used to send order acknowledgements and tracking information.'),
        '#attributes' => [
          'class' => 'contact_info_desc',
        ],
      ];

      $form['contact_details']['phone_number'] = [
        '#type' => 'tel',
        '#title' => $this->t('Phone Number(xxx-xxx-xxxx)'),
        '#required' => TRUE,
        '#default_value' => $this->store->get('phone_number') ? $this->store->get('phone_number') : '',
      ];

      $form['contact_details']['email'] = [
        '#type' => 'email',
        '#title' => $this->t('Email Address'),
        '#required' => TRUE,
        '#default_value' => $this->store->get('email') ? $this->store->get('email') : '',
      ];

      $form['actions']['#type'] = 'actions';


      $form['actions']['cancel'] = [
        '#type' => 'markup',
        '#markup' => '<a href="' . $path . '" class="button js-form-submit cancel-form btn btn--primary">'.$this->t("Cancel").'</a>',
      ];


      $form['actions']['submit'] = [
        '#type' => 'submit',
        '#value' => $this->t('Continue'),
        '#button_type' => 'primary',
      ];

      $form['response'] = $apiResponse;
      $form['erroroncheckout'] = $this->store->get('erroroncheckout');
      $form['#theme'] = 'form_shipment';
      $this->store->delete('erroroncheckout');

    }

    //Step 2 Form

    if (!empty($this->store->get('step')) && ($this->store->get('step') == 2)) {

      //Shipping API Call
      $shipApiResponse = $this->shippingApiCall();
      if(!empty($shipApiResponse['#items']['errors'])) {
        if ($shipApiResponse['#items']['errors'][0] != 0) {
          $this->store->set('erroroncheckout', $shipApiResponse['#items']['errors'][1][0]['message']);
          $this->store->set('step', 1);
          $response = new RedirectResponse("/checkout");
          $response->send();
        }
        else {
          $this->store->set('shipping_response', $shipApiResponse);
        }
      }

      // paymetric API call JS
      $form['#attached']['library'][] = 'df_checkout/df_checkout';

      //Billing Form fields
      $form['payment_info'] = [
        '#type' => 'container',
        '#attributes' => [
          'class' => 'payment_info',
        ],
      ];

      $form['payment_billing_info'] = [
        '#type' => 'container',
        '#attributes' => [
          'class' => 'payment_billing_info',
        ],
      ];

      $form['card_info'] = [
        '#type' => 'container',
        '#attributes' => [
          'class' => 'card_info',
        ],
      ];

      $form['card_info']['pay_information'] = [
        '#type' => 'item',
        '#markup' => $this->t('Payment Information'),
        '#attributes' => [
          'class' => 'pay_information',
        ],
      ];

      $form['card_info']['payment_info_des'] = [
        '#type' => 'item',
        '#markup' => $this->t("<strong>PLEASE NOTE: </strong>All fields are required unless marked as <strong>optional</strong>"),
        '#attributes' => [
          'class' => 'payment_info_des',
        ],
      ];

      $values = $form_state->getValues();

      $this->access_token = !empty($values['access_token']) ? $values['access_token'] : $this->access_token = $this->paymetric_token_service->getAccessToken();

      $form['hidden']['access_token'] = [
        '#type' => 'hidden',
        '#title' => $this->t('Access Token'),
        '#value' => $this->access_token,
        '#attributes' => [
          'disabled' => 'disabled',
          'id' => 'edit-access-token',
        ]
      ];

      $title = 'Token Details:';

      $form['token'] = [
        '#type' => 'fieldset',
        '#title' => $title,
      ];

      $table_prefix = "<table>";
      $table_suffix = "</table>";
      $iframe_url = $this->paymetric_token_service->getEndpoint() . '/diecomm/View/Iframe/' . $this->paymetric_token_service->getGuid() . '/' . $this->access_token . '/True';
      $iframe_html = '<iframe name="dieCommFrame" src="' . $iframe_url . '" style="width: 100%; overflow: hidden; height: 200px;" scrolling="no" frameborder="0" id="IFrame"></iframe>';

      $token_output = '';
      $token_output .= "<tr><td/><b>Accesstoken" . ":</b></td><td>" .  $this->access_token . "</td></tr>";
      $token_output .= "<tr><td/><b>iFrame URL" . ":</b></td><td>" .  $iframe_url . "</td></tr>";
      $token_output .= "<tr><td/><b>iFrame HTML" . ":</b></td><td>" .  htmlspecialchars($iframe_html) . "</td></tr>";

      if (!empty($token_output)) {
        $token_output = $table_prefix . $token_output . $table_suffix;
      }

      $form['token']['render'] = [
        '#type' => 'markup',
        '#markup' => $token_output,
      ];

      $form['render']['iframe'] = [
        '#type' => 'markup',
        '#markup' => $iframe_html,
        '#allowed_tags' => ['iframe'],
        '#suffix' => '<br />'
      ];

      $form['payment_billing_info']['additional_info_des'] = [
        '#type' => 'item',
        '#markup' => $this->t('Billing Address'),
        '#attributes' => [
          'class' => 'additional_info_des',
        ],
      ];

      $form['payment_billing_info']['bill_ship_address_same'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('My Billing and Shipping address are the same'),
      ];

      $form['payment_info']['payment_fullname'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Full Name'),
        '#default_value' => $this->store->get('payment_fullname') ? $this->store->get('payment_fullname') : '',
      ];

      $form['payment_info']['payment_address_line'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Address Line 1'),
        '#default_value' => $this->store->get('payment_address_line') ? $this->store->get('payment_address_line') : '',
      ];

      $form['payment_info']['payment_address_line_optional'] = [
        '#type' => 'textfield',
        '#title' => t('Address Line 2 (Optional)'),
        '#default_value' => $this->store->get('payment_address_line_optional') ? $this->store->get('payment_address_line_optional') : '',
      ];

      $form['payment_info']['payment_city'] = [
        '#type' => 'textfield',
        '#title' => $this->t('City'),
        '#default_value' => $this->store->get('payment_city') ? $this->store->get('payment_city') : '',
      ];

      $form['payment_info']['payment_state'] = [
        '#type' => 'select',
        '#title' => $this->t('State'),
        '#options' => $this->getUsStates(),
      ];

      $form['payment_info']['payment_zipcode'] = [
        '#type' => 'number',
        '#title' => $this->t('Zip'),
        '#min' => 5,
        '#default_value' => $this->store->get('payment_zipcode') ? $this->store->get('payment_zipcode') : '',
      ];

      $form['accesstokenXml'] = [
        '#type' => 'hidden',
        '#title' => $this->t('Access Token Xml'),
        '#value' => "cardresponse",
        '#attributes' => [
          'id' => 'xml-access-token',
        ]
      ];

      $form['accept_order_details'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('By choosing to place your order, you are agreeing to the items, quantities, pricing and additional charges
                           , where applicable, as set forth in the order above. This order is subject to our return policy and our standard sales terms,
                            and conditions as in effect from time to time, which are incorporated here in reference and available at Return Policy and Terms,
                            and Conditions.'),
      ];

      $form['actions']['#type'] = 'actions';

      $form['actions']['Previous'] = array(
        '#type' => 'button',
        '#value' => $this->t('BACK TO SHIPPING'),
        '#limit_validation_errors' => [],
        '#ajax' => [
          'callback' => '::redirectShip',
          'event' => 'click',
          'wrapper' => 'edit-output',
        ],
      );

      $form['submit'] = [
        '#type' => 'html_tag',
        '#tag' => 'input',
        '#attributes' => [
          'type' => 'button',
          'value' => $this->t('CONTINUE'),
          'class' => 'button btn--primary',
          'name' => 'payment-submit-button',
          'id' => 'payment-submit-button'
        ],
      ];

      $form['actions']['submit'] = [
        '#type' => 'submit',
        '#value' => $this->t('CONTINUE'),
        '#button_type' => 'primary',
        '#attributes' => [
          'class' => ['ordersubmitbtn'],
        ],
        '#ajax' => [
          'callback' => '::ordersubmit',
          'event' => 'click',
          'wrapper' => 'edit-output',
        ],
      ];

      $form['ship_response'] = $shipApiResponse;
      $form['erroroncheckout'] = $this->store->get('erroroncheckout');
      $form['#theme'] = 'form_billing';
      //$this->store->delete('erroroncheckout');
    }

    return $form;
  }


  /**
   * Shipping Form Validation
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    $this->store->delete('erroroncheckout');

    //Step 1 Form validation
    if (!empty($this->store->get('step')) && ($this->store->get('step') == 1)) {
      if(!empty($form_state->getValue('phone_number'))) {
        if (!$this->validatePhoneNo($form_state->getValue('phone_number'))) {
          $form_state->setErrorByName('phone_number', $this->t('Please enter a valid 10 digit phone number(xxx-xxx-xxxx).'));
        }
      }
      else {
        $form_state->setErrorByName('phone_number', $this->t('Phone Number is required.'));
      }

      if(!empty($form_state->getValue('fullname'))) {
        if (!preg_match("/^[a-zA-Z ]*$/", $form_state->getValue('fullname'))) {
          $form_state->setErrorByName('fullname', $this->t('Full Name is allowed Only letters and white space.'));
        }
      }
      else {
        $form_state->setErrorByName('fullname', $this->t('Full Name required.'));
      }

      if(!empty($form_state->getValue('address_line'))) {
        if(!preg_match('/^[0-9a-zA-Z,\/ ]+$/', $form_state->getValue('address_line'))) {
          $form_state->setErrorByName('address_line', $this->t('Address Line 1 allowed only letters, Numbers.'));
        }
      }
      else {
        $form_state->setErrorByName('address_line', $this->t('Address Line 1 required.'));
      }

      if(!empty($form_state->getValue('city'))) {
        if (!preg_match("/^[a-zA-Z ]*$/", $form_state->getValue('city'))) {
          $form_state->setErrorByName('city', $this->t('City is allowed Only letters and white space.'));
        }
      }
      else {
        $form_state->setErrorByName('city', $this->t('City is required.'));
      }

      if(!empty($form_state->getValue('zipcode'))) {
        if (strlen($form_state->getValue('zipcode')) < 5) {
          $form_state->setErrorByName('zipcode', $this->t('Please enter a valid 5 digit Zipcode.'));
        }
      }
      else {
        $form_state->setErrorByName('zipcode', $this->t('Zipcode is required.'));
      }
    }
  }

  /* Validates if $phone is a valid U.S. phone number in the 202-555-1234 format.
   * Note that the first digit cannot be a 1 as no area code in the U.S. starts with a 1.
   */
  public function validatePhoneNo($phone) {
    if (preg_match("/^([1]-)?[0-9]{3}-[0-9]{3}-[0-9]{4}$/i", $phone)) {
      return true;
    }
    else {
      return false;
    }
  }

  /**
   * Shipping Form submit and Temp storage
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_data = $form_state->getUserInput();
    if (!empty($this->store->get('step')) && ($this->store->get('step') == 1)) {
      $this->store->set('shipping_option', $form_state->getValue('shipping_option'));
      $this->store->set('fullname', $form_state->getValue('fullname'));
      $this->store->set('address_line', $form_state->getValue('address_line'));
      $this->store->set('address_line_optional', $form_state->getValue('address_line_optional'));
      $this->store->set('city', $form_state->getValue('city'));
      $this->store->set('state', $form_state->getValue('state'));
      $this->store->set('zipcode', $form_state->getValue('zipcode'));
      $this->store->set('phone_number', $form_state->getValue('phone_number'));
      $this->store->set('email', $form_state->getValue('email'));
      $this->store->set('previous_url', $form_data['previous_url']);
      $this->store->set('step',2);
     // $this->redirectbilling();
      $response = new RedirectResponse("/checkout");
      $response->send();
      exit;
    }
  }

  /**
   * Billing Form submit and Temp storage
   */
  public function ordersubmit(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    $this->store->set('bill_ship_address_same','1');
    if($form_state->getValue('bill_ship_address_same') == 0) {
      $this->store->set('bill_ship_address_same', $form_state->getValue('bill_ship_address_same'));
      $this->store->set('payment_fullname', $form_state->getValue('payment_fullname'));
      $this->store->set('payment_address_line', $form_state->getValue('payment_address_line'));
      $this->store->set('payment_address_line_optional', $form_state->getValue('payment_address_line_optional'));
      $this->store->set('payment_city', $form_state->getValue('payment_city'));
      $this->store->set('payment_state', $form_state->getValue('payment_state'));
      $this->store->set('payment_zipcode', $form_state->getValue('payment_zipcode'));
    }

    // Card details from paymetric API.
    $shipping_response = $this->store->get('shipping_response');
    $form_data =  $form_state->getUserInput();
    $cardDetails = explode("|", $form_data['accesstokenXml']);
    if(count($cardDetails) > 1) {
      $shipping_response['#items']['ccNumber'] = $cardDetails[3];
      $shipping_response['#items']['ccConfirmationCode'] = $cardDetails[5];
      $shipping_response['#items']['ccType'] = $cardDetails[0];
      $shipping_response['#items']['ccName'] = $cardDetails[4];
      $shipping_response['#items']['ccLastFour'] = null;
      $shipping_response['#items']['ccExpireMonth'] = $cardDetails[1];
      $shipping_response['#items']['ccExpireYear'] = $cardDetails[2];
    }

    // payment API Call.
    $OrderapiResponse = $this->orderPaymentApiCall($shipping_response);
    if(!empty($OrderapiResponse['#items']['errors'])) {
      if ($OrderapiResponse['#items']['errors'][0] != 0) {
        $error_message = $this->getErrorMessage($OrderapiResponse['#items']['errors'][1][0]['message']);
        $this->store->set('erroroncheckout', $error_message);
        $command = new RedirectCommand('/checkout');
        return $response->addCommand($command);
      }
      else {
        // Order conformation API Call.
        $OrderapiConfResponse = $this->orderConformationApiCall($OrderapiResponse, $shipping_response['#items']['ccType']);
        if ($OrderapiConfResponse['#items']['errors'][0] != 0) {
          $this->store->set('erroroncheckout', $OrderapiConfResponse['#items']['errors'][1][0]['message']);
          $command = new RedirectCommand('/checkout');
          return $response->addCommand($command);
        }
        else {
          // Send mail
          $order_type_storage = $this->entityTypeManager->getStorage('commerce_order_type');
          /** @var \Drupal\commerce_order\Entity\OrderTypeInterface $order_type */
          $order_type = $order_type_storage->load('default');
          if ($order_type->shouldSendReceipt()) {
            $subject = "Order Completed on Firstwave - Order # ". $OrderapiConfResponse['#items']['orderNumber'];
            $template = [
              '#theme' => 'order_confirmation_email',
              '#order_conformation' => $OrderapiConfResponse
            ];
            $params['subject'] = $subject;
            $params['body'] = render($template);
            $params['headers'] = [
              //'bcc' => $order_type->getReceiptBcc(),
              'content-type' => 'text/html'
            ];

            $customerEmail = $this->store->get('email');
            $receiptEmail = $order_type->getReceiptBcc();

            if ($this->mailManager->mail('df_checkout', 'order_confirmation_email', $receiptEmail, $this->currentUser->getPreferredLangcode(), $params)) {
              $this->logger->get('df_checkout')->notice($subject);
            }
          }
          // End of send email
          $this->store->set('orderConformation', json_encode($OrderapiConfResponse));
          return (new AjaxResponse())->addCommand(
            new RedirectCommand('/order-confirmation')
          );
        }
      }
    }
  }

  /**
   * redirecting to Billing Form
   */
  public function redirectbilling() {
    return (new AjaxResponse())->addCommand(
      new RedirectCommand('/checkout')
    );
  }

  /**
   * Redirecting to Shippinf Form
   */
  public function redirectShip(array &$form, FormStateInterface $form_state) {

    $this->store->delete('step');
    $this->store->set('step',1);

    if($form_state->getValue('bill_ship_address_same') === 1) {
      $this->store->delete('bill_ship_address_same');
      $this->store->delete('payment_fullname');
      $this->store->delete('payment_address_line');
      $this->store->delete('payment_address_line_optional');
      $this->store->delete('payment_city');
      $this->store->delete('payment_state');
      $this->store->delete('payment_zipcode');
    }

    return (new AjaxResponse())->addCommand(
        new RedirectCommand('/checkout')
    );
  }

  /**
   * {@inheritdoc}
   * returns Us States list with Key value pair array
   */
  public function getUsStates() {
    $municipalities = [];
    $municipalities[''] = 'State';
    // Getting US sates for Addressing module.
    $subdivisionRepository = new SubdivisionRepository();
    // Get the subdivisions for US.
    $states = $subdivisionRepository->getAll(['US']);
    foreach ($states as $state) {
      $municipalities[$state->getCode()] = $state->getName();
    }
    return $municipalities;
  }

  /**
   * {@inheritdoc}
   * returns order details of product
   */
  public function beforeShippingApiCall($path) {
    //Before rendering shipping page API call
    $request_api = $this->checkoutServices->objConstruct();

    if(is_array($request_api)) {
      $response = $this->checkoutServices->ecommerceCurlCall($this->configFactory->get('before_shipping_api_url'), $request_api);
      if(!empty($response)) {
        return $this->checkoutServices->getOrderDetails($response);
      }
      else {
        $response = new RedirectResponse($path);
        $response->send();
      }
    }
    else {
      $url = Url::fromRoute('<front>');
      $response = new RedirectResponse($url->toString());
      $response->send();
    }
  }

  /**
   * {@inheritdoc}
   * returns order details of product and Shipping Address
   */
  public function shippingApiCall() {
    $apiObjResponse = $this->checkoutServices->objConstruct();
    if(empty($apiObjResponse)) {
      $url = Url::fromRoute('<front>');
      $response = new RedirectResponse($url->toString());
      $response->send();
    }
    $response = $this->checkoutServices->ecommerceCurlCall($this->configFactory->get('shipping_api_url'), $apiObjResponse);
    if(!empty($response)) {
      return $this->checkoutServices->getOrderDetails($response);
    }
    else {
      return $this->redirect('<front>');
    }
  }

  /**
   * {@inheritdoc}
   * returns order payment details of product
   */
  public function orderPaymentApiCall($shipping_response) {

    $orderApiObjResponse = $this->checkoutServices->OrderConfConstruct($shipping_response);
    $response = $this->checkoutServices->ecommerceCurlCall($this->configFactory->get('payment_api_url'), $orderApiObjResponse);

    if(!empty($response)) {
      return $this->checkoutServices->orderConfOrderDetails($response);
    }
    else {
      return $this->redirect('<front>');
    }
  }

  /**
   * {@inheritdoc}
   * returns order Conformation details of product
   */
  public function orderConformationApiCall($order_response, $ccType) {

    $orderconfApiObjResponse = $this->checkoutServices->OrderConfirmationConstruct($order_response);
    $confResponse = $this->checkoutServices->ecommerceCurlCall($this->configFactory->get('order_conformation_api_url'), $orderconfApiObjResponse);
    if(!empty($confResponse)) {
      return $this->checkoutServices->orderConfOrderDetails($confResponse, $ccType);
    }
    else {
      return $this->redirect('<front>');
    }
  }

  /**
   * {@inheritdoc}
   * returns previous page URL
   */
  public function getHttpRef() {
    $previousUrl = \Drupal::request()->server->get('HTTP_REFERER');
    $fake_request = Request::create($previousUrl);
    $url_object = \Drupal::service('path.validator')->getUrlIfValid($fake_request->getRequestUri());
    $path = "";
    if ($url_object) {
      $path = $url_object->toString();
    }
    return $path;
  }

  /**
   * {@inheritdoc}
   * Return checkout error messages
   */
  public function getErrorMessage($err_key) {
    $error_messages_list = explode('\n', $this->configFactory->get('checkout_error'));
    foreach ($error_messages_list as $key => $value) {
      $errors = explode('|', $value);
      $messages_list[$errors[0]] = $errors[1];
    }
    if (array_key_exists($err_key, $messages_list)) {
      $error_message = $messages_list[$err_key];
    }else{
      $error_message = $err_key;
    }
    return $error_message;
  }
}

<?php

namespace Drupal\df_checkout\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use CommerceGuys\Addressing\Subdivision\SubdivisionRepository;
use Drupal\Core\Url;

/**
 *
 */
class PaymentForm extends FormBase {

  /**
   * @var CommerceGuys\Addressing
   */
  protected $usa_states;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'payment_details_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    // Getting US sates for Addressing module.
    $subdivisionRepository = new SubdivisionRepository();
    // Get the subdivisions for US.
    $states = $subdivisionRepository->getAll(['US']);
    foreach ($states as $state) {
      $municipalities[$state->getCode()] = $state->getName();
    }

    /* Common container for all fields. */
    $form['payment_info'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => 'payment_info',
      ],
    ];

    $form['payment_info']['pay_information'] = [
      '#type' => 'item',
      '#markup' => $this->t('Payment Information'),
      '#attributes' => [
        'class' => 'pay_information',
      ],
    ];

    $form['payment_info']['payment_info_des'] = [
      '#type' => 'item',
      '#markup' => $this->t("<strong>PLEASE NOTE: </strong>All fields are required unless marked as <strong>optional</strong>"),
      '#attributes' => [
        'class' => 'payment_info_des',
      ],
    ];

    $form['payment_info']['card_info'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => 'card_info',
      ],
    ];

    $form['payment_info']['card_info']['credit_card_info'] = [
      '#type' => 'markup',
      '#markup' => $this->t("Credit Card Information"),
      '#attributes' => [
        'class' => 'credit_card_info',
      ],
    ];

    $form['payment_info']['card_info']['card_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Card Type'),
      '#options' => [
        '0' => $this->t(''),
        'visa' => $this->t('visa'),
        'mastro' => $this->t('mastro'),
      ],
    ];

    $form['payment_info']['card_info']['name_on_card'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Name on card'),
      '#required' => TRUE,
    ];

    $form['payment_info']['card_info']['credit_card_number'] = [
      '#type' => 'creditfield_cardnumber',
      '#title' => $this->t('Card Number'),
      '#maxlength' => 16,
      '#required' => TRUE,
    ];

    $form['payment_info']['card_info']['exp_month'] = [
      '#type' => 'select',
      '#title' => $this->t('Exp Month'),
      '#options' => [
        '0' => $this->t(''),
        '1' => $this->t('January'),
        '2' => $this->t('February'),
        '3' => $this->t('March'),
        '4' => $this->t('April'),
        '5' => $this->t('May'),
        '6' => $this->t('June'),
        '7' => $this->t('July'),
        '8' => $this->t('August'),
        '9' => $this->t('September'),
        '10' => $this->t('October'),
        '11' => $this->t('November'),
        '12' => $this->t('December'),
      ],
    ];

    $form['payment_info']['card_info']['exp_year'] = [
      '#type' => 'select',
      '#title' => $this->t('Exp Year'),
      '#options' => [
        '0' => $this->t(''),
        '1991' => $this->t('1991'),
        '2' => $this->t('02'),
        '3' => $this->t('03'),
        '4' => $this->t('04'),
        '5' => $this->t('05'),
        '6' => $this->t('06'),
        '7' => $this->t('07'),
        '8' => $this->t('08'),
        '9' => $this->t('09'),
        '10' => $this->t('10'),
        '11' => $this->t('11'),
        '12' => $this->t('12'),
      ],
    ];

    $form['payment_info']['card_info']['credit_card_cvv'] = [
      '#type' => 'creditfield_cardcode',
      '#title' => $this->t('CVV'),
      '#maxlength' => 4,
      '#required' => TRUE,
    ];

    $form['payment_info']['additional_info'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => 'additional_info',
      ],
    ];

    $form['payment_info']['additional_info']['additional_info_des'] = [
      '#type' => 'item',
      '#markup' => $this->t('Additional Information'),
      '#attributes' => [
        'class' => 'additional_info_des',
      ],
    ];

    $form['payment_info']['additional_info']['bill_ship_address_same'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('My Billing and Shipping address are the same'),
    ];

    $form['payment_info']['additional_info']['payment_fullname'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Full Name:'),
      '#required' => TRUE,
    ];

    $form['payment_info']['additional_info']['payment_address_line'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Address Line 1'),
      '#required' => TRUE,
    ];

    $form['payment_info']['additional_info']['payment_address_line_optional'] = [
      '#type' => 'textfield',
      '#title' => t('Address Line 2(Optional)'),
    ];

    $form['payment_info']['additional_info']['payment_city'] = [
      '#type' => 'textfield',
      '#title' => $this->t('City'),
      '#required' => TRUE,
    ];

    $form['payment_info']['additional_info']['payment_state'] = [
      '#type' => 'select',
      '#title' => $this->t('State'),
      '#options' => $municipalities,
    ];

    $form['expiration_date'] = [
      '#type' => 'creditfield_expiration',
      '#title' => 'Expiration Date',
    ];

    $form['payment_info']['additional_info']['payment_zipcode'] = [
      '#type' => 'number',
      '#title' => $this->t('Zip'),
      '#min' => 5,
    ];

    $form['payment_info']['additional_info']['accept_order_details'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('By choosing to place your order, you are agreeing to the items, quantities, pricing and additional charges
                           , where applicable, as set forth in the order above. This order is subject to our return policy and our standard sales terms,
                            and conditions as in effect from time to time, which are incorporated here in reference and available at Return Policy and Terms,
                            and Conditions.'),
    ];

    $form['actions']['#type'] = 'actions';

    /*$form['actions']['button'] = array(
    '#type' => 'button',
    '#value' => $this->t('BACK TO MY ORDERS'),
    '#button_type' => 'primary',
    );*/

    $form['actions']['previous'] = [
      '#type' => 'link',
      '#title' => $this->t('BACK TO MY ORDERS'),
      '#attributes' => [
        'class' => ['button'],
      ],
      '#url' => Url::fromRoute('df_checkout.shipping_details_form'),
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('PLACE ORDER'),
      '#button_type' => 'primary',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    /*if (strlen($form_state->getValue('phone_number')) < 10) {
    $form_state->setErrorByName('phone_number', $this->t('Please enter a valid 10 digit phone number(xxx-xxx-xxxx).'));
    }*/
    if (!preg_match("/^[a-zA-Z ]*$/", $form_state->getValue('payment_fullname'))) {
      $form_state->setErrorByName('payment_fullname', $this->t('Full Name is allowed Only letters and white space.'));
    }
    /*if (!preg_match('/^(?:\\d+ [a-zA-Z ]+, ){2}[a-zA-Z ]+$/',$form_state->getValue('address_line'))) {
    $form_state->setErrorByName('address_line', $this->t('Address is allowed letters and white space and digits.'));
    }*/
    if (!preg_match("/^[a-zA-Z ]*$/", $form_state->getValue('payment_city'))) {
      $form_state->setErrorByName('payment_city', $this->t('City is allowed Only letters and white space.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // drupal_set_message($this->t('@can_name ,Your application is being submitted!', array('@can_name' => $form_state->getValue('candidate_name'))));.
    foreach ($form_state->getValues() as $key => $value) {
      drupal_set_message($key . ': ' . $value);
    }
    $form_state->setRedirect('df_checkout.payment_details_form');

  }

}

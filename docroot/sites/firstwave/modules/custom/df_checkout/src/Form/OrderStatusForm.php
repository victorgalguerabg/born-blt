<?php
/**
 * Created by PhpStorm.
 * User: manikandan.kariyappa@wearefmg.net
 * Date: 7/17/19
 * Time: 12:56 PM
 */

namespace Drupal\df_checkout\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\PrivateTempStoreFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\df_services\DfCheckoutServices;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\checkout_services\CheckoutServices;

class OrderStatusForm extends FormBase {

  /**
   * @var \Drupal\df_services\DfCheckoutServices
   */
  protected $checkoutServices;

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * @var \Drupal\user\PrivateTempStoreFactory
   */
  protected $tempStoreFactory;

  /**
   * @var \Drupal\user\PrivateTempStoreFactory
   */
  protected $store;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\checkout_services\CheckoutServices
   */
  protected $globalCheckoutServices;

  /**
   * Constructs a new object.
   */
  public function __construct( DfCheckoutServices $checkoutServices, ConfigFactoryInterface $configFactory, PrivateTempStoreFactory $temp_store_factory, EntityTypeManagerInterface $entityTypeManager, CheckoutServices $globalCheckoutServices) {
    $this->tempStoreFactory = $temp_store_factory;
    $this->store = $this->tempStoreFactory->get('multistep_data');
    $this->checkoutServices = $checkoutServices;
    $this->configFactory = $configFactory->get('api_config.apisettings');
    $this->entityTypeManager = $entityTypeManager;
    $this->globalCheckoutServices = $globalCheckoutServices;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('df_services.checkoutflow'),
      $container->get('config.factory'),
      $container->get('user.private_tempstore'),
      $container->get('entity_type.manager'),
      $container->get('checkout_services.checkoutflow')
    );
  }

  /**
   * return @form id
   */
  public function getFormId() {
    return 'order_status_form';
  }

  /**
   * Order Status Form
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    //Order Number Field
    $form['ordernumber'] = [
      '#type' => 'textfield',
      '#attributes' => [
        'id' => 'edit-order-number',
        'placeholder' => "Order Number",
      ],
    ];

    //Email Address Field
    $form['email'] = [
      '#type' => 'email',
      '#attributes' => [
        'id' => 'edit-email-address',
        'placeholder' => "Email Address",
      ],
    ];

    $form['zipcode'] = [
      '#type' => 'number',
      '#min' => 5,
      '#attributes' => [
        'id' => 'edit-zipcode',
        'placeholder' => "Zip Code",
      ],
    ];

    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Track Order'),
      '#button_type' => 'primary',
      '#attributes' => [
        'class' => ['order-status-btn'],
      ],
    );

    $form['#theme'] = 'form_order_status';

    return $form;
  }

  /**
   * Order Status Form Validation
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    $i = 0;

    if(empty($form_state->getValue('ordernumber'))) {
      $form_state->setErrorByName('ordernumber', $this->t('Order Number is required.'));
      $i++;
    }

    if(empty($form_state->getValue('zipcode'))) {
      $form_state->setErrorByName('zipcode', $this->t('zipcode is required.'));
      $i++;
    }

    if($i == 0) {
      $responseStatus = $this->orderStatusInfo($form_state->getValue('ordernumber'), $form_state->getValue('email'), $form_state->getValue('zipcode'));
      if(count($responseStatus) != 0) {
        $responsePrdInfo = $this->orderProductInfo($form_state->getValue('ordernumber'));
        if(count($responsePrdInfo) != 0 && !isset($responseStatus['Error'])) {
          $orderDetails = [];
          // fetch the main product image if image is not available in the SAP.
          if (isset($responsePrdInfo['orderItems'][0]['imageFilename']) && $responsePrdInfo['orderItems'][0]['imageFilename']) {
            $orderDetails['productImg'] = $responsePrdInfo['orderItems'][0]['imageFilename'];
          }else{
            $products = $this->entityTypeManager
                  ->getStorage('commerce_product_variation')
                  ->loadByProperties(['sku' => $responsePrdInfo['orderItems'][0]['skuCode']]);
            foreach ($products as $product_id => $product_variation) {
              if ($product_variation->getSku() == $responsePrdInfo['orderItems'][0]['skuCode']) {
                $product = $product_variation->getProduct();
                $product_images = $product->get('field_image_url')->getValue();
                $product_images = array_column($product_images, 'value');
                if (!empty($product_images)) {
                  $main_image = current($product_images);
                }
              }
            }
            $orderDetails['productImg'] = $main_image;
          }
          $orderDetails['orderNo'] = $responseStatus[0]['orderNumber'];
          $date = new DrupalDateTime($responseStatus[0]['datePlaced']);
          $orderDetails['orderPlaced'] = $date->format('F d, Y');
          $orderDetails['subTotal'] = $responsePrdInfo['orderSubTotal'];
          $orderDetails['shipCharge'] = $responsePrdInfo['shippingCharge'];
          $orderDetails['salesTax'] = $responsePrdInfo['tax'];
          $orderDetails['orderTotal'] = $responsePrdInfo['orderTotal'];
          $orderDetails['ProductName'] = $responsePrdInfo['orderItems'][0]['description'];
          $orderDetails['productCode'] = $responsePrdInfo['orderItems'][0]['skuCode'];
          $orderDetails['orderStatus'] = $responseStatus[0]['status'];
          $orderDetails['trackingNumber'] = $responsePrdInfo['orderItems'][0]['trackingNumber'];
          $orderDetails['orderQty'] = $responsePrdInfo['orderItems'][0]['qtyOrdered'];
          $orderDetails['productPrice'] = $responsePrdInfo['orderItems'][0]['unitPrice'];

          $this->store->set("orderStatusInfo",json_encode($orderDetails));
        }
        else {
          $form_state->setErrorByName('ordernumber', $responseStatus['Error']);
        }
      }
      else {
        $form_state->setErrorByName('ordernumber', "There is no Order Found.");
      }
    }
  }

  /**
   * Order status Form Submit action
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if(!empty($this->store->get("orderStatusError"))) {
      $form_state->setRedirect("df_checkout.orderstatus");
    }
    else {
      $form_state->setRedirect("df_checkout.orderstatusinfo");
    }
  }


  /**
   * Order status API Call
   */
  public function orderStatusInfo($ordernumber, $email, $zipcode) {

    $orderDetails = [
      "orderno" => $ordernumber,
      "email" => $email,
      "zip" => $zipcode,
      "apiUrl" => $this->configFactory->get('order_status_api'),
      "username" => $this->configFactory->get('delta_api_username'),
      "password" => $this->configFactory->get('delta_api_password')
    ];
    //Order Status API Call
    $response = $this->globalCheckoutServices->orderStstusWebCall($orderDetails);
    return $response;
  }

  /**
   * Order Product Info API Call
   */
  public function orderProductInfo($ordernumber) {

    $orderDetails = [
      "orderNo" => $ordernumber,
      "companyNo" => $this->configFactory->get('companyNumber'),
      "documentType" => $this->configFactory->get('documentType'),
      "source" => $this->configFactory->get('site_name'),
      "apiUrl" => $this->configFactory->get('ordered_product_info_api'),
      "username" => $this->configFactory->get('delta_api_username'),
      "password" => $this->configFactory->get('delta_api_password')
    ];

    //Order Status API Call
    $response = $this->checkoutServices->orderprdInfoApiCall($orderDetails);
    return $response;
  }
}

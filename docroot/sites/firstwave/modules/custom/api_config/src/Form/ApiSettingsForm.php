<?php

namespace Drupal\api_config\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class DFApiSettingsForm.
 */
class ApiSettingsForm extends ConfigFormBase {

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs a new DFApiSettingsForm object.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    // parent::__construct($config_factory);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'api_config.apisettings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'api_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('api_config.apisettings');

    $form['domain_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Domain Url'),
      '#description' => $this->t('Domain URL of the API server.'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('domain_url'),
    ];

    $form['site_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Site Name'),
      '#description' => $this->t('List the products based on the Site Name.'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('site_name'),
    ];

    $form['delta_api_username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Username'),
      '#description' => $this->t('Enter Api Auth Username.'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('delta_api_username'),
    ];
    $form['delta_api_password'] = [
      '#type' => 'password',
      '#title' => $this->t('Password'),
      '#description' => $this->t('Enter Api Auth Password.'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('delta_api_password'),
    ];

    $form['source'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Source Name'),
      '#description' => $this->t('Source Name.'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('source'),
    ];

    $form['store_type'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Store Type'),
      '#description' => $this->t('Store Type Name'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('store_type'),
    ];

    $form['companyNumber'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Company Number'),
      '#description' => $this->t('Company Number.'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('companyNumber'),
    ];

    $form['carrier'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Carrier'),
      '#description' => $this->t('Carrier.'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('carrier'),
    ];

    $form['shiptoNumber'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Ship to Number'),
      '#description' => $this->t('Ship to Number.'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('shiptoNumber'),
    ];

    $form['documentType'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Document Type'),
      '#description' => $this->t('Document Type for API Call.'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('documentType'),
    ];

    $form['shippingOption'] = [
      '#type' => 'details',
      '#title' => $this->t('Shipping Option List and Price'),
    ];

    $form['shippingOption']['ground'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Ground'),
      '#description' => $this->t('Ground Ship Price.'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('ground'),
    ];

    $form['shippingOption']['priority'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Priority'),
      '#description' => $this->t('Priority Ship Price.'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('priority'),
    ];

    $form['before_shipping_api'] = [
      '#type' => 'details',
      '#title' => $this->t('Before Shipping API'),
    ];

    $form['before_shipping_api']['before_shipping_api_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Before Shipping API'),
      '#description' => $this->t('Before Shipping API.'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('before_shipping_api_url'),
    ];

    $form['shipping'] = [
      '#type' => 'details',
      '#title' => $this->t('Shipping API'),
    ];

    $form['shipping']['shipping_api_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Shipping API URL'),
      '#description' => $this->t('Shipping API URL.'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('shipping_api_url'),
    ];

    $form['product'] = [
      '#type' => 'details',
      '#title' => $this->t('Product API'),
    ];

    $form['product']['product_api_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Product info API'),
      '#description' => $this->t('Product info URL API.'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('product_api_url'),
    ];

    $form['product']['payment_api_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Order API'),
      '#description' => $this->t('Payment API URL.'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('payment_api_url'),
    ];

    $form['product']['order_conformation_api_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Order Conformation API'),
      '#description' => $this->t('Order Conformation API URL.'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('order_conformation_api_url'),
    ];

    $form['product']['order_status_api'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Order Status API'),
      '#description' => $this->t('Order Status API URL.'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('order_status_api'),
    ];

    $form['product']['ordered_product_info_api'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Ordered Product Info API'),
      '#description' => $this->t('Order Product Info API URL.'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('ordered_product_info_api'),
    ];

    $form['product']['product_availability_check'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Product Availability Check API'),
      '#description' => $this->t('Product Availability Check API URL.'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('product_availability_check'),
    ];

    $form['checkout'] = [
      '#type' => 'details',
      '#title' => $this->t('Checkout Custom Error Message'),
      '#weight' => 100,
    ];

    // @todo We have to implement the key as error code from SAP API response. Currently SAP API response doesn't having the error code.
    $form['checkout']['checkout_error'] = [
      '#type' => 'textarea',
      '#rows' => 5,
      '#title' => $this->t('Allowed Error Message List'),
      '#default_value' => $config->get('checkout_error'),
      '#description' => $this->t("The possible values this field can contain. Enter one value per line, in the format key|label. The key is the stored value. The label will be used in displayed values and edit forms. The label is optional: if a line contains a single string, it will be used as key and label."),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $this->config('api_config.apisettings')
      ->set('domain_url', $form_state->getValue('domain_url'))
      ->set('site_name', $form_state->getValue('site_name'))
      ->set('delta_api_username', $form_state->getValue('delta_api_username'))
      ->set('shipping_api_url', $form_state->getValue('shipping_api_url'))
      ->set('product_api_url', $form_state->getValue('product_api_url'))
      ->set('payment_api_url', $form_state->getValue('payment_api_url'))
      ->set('order_conformation_api_url', $form_state->getValue('order_conformation_api_url'))
      ->set('order_status_api', $form_state->getValue('order_status_api'))
      ->set('ordered_product_info_api', $form_state->getValue('ordered_product_info_api'))
      ->set('companyNumber', $form_state->getValue('companyNumber'))
      ->set('carrier', $form_state->getValue('carrier'))
      ->set('shiptoNumber', $form_state->getValue('shiptoNumber'))
      ->set('store_type', $form_state->getValue('store_type'))
      ->set('documentType', $form_state->getValue('documentType'))
      ->set('before_shipping_api_url', $form_state->getValue('before_shipping_api_url'))
      ->set('product_availability_check', $form_state->getValue('product_availability_check'))
      ->set('ground', $form_state->getValue('ground'))
      ->set('priority', $form_state->getValue('priority'))
      ->set('source', $form_state->getValue('source'))
      ->set('checkout_error', $form_state->getValue('checkout_error'))
      ->save();
      if (strlen(trim($form_state->getValue('delta_api_password'))) > 0) {
        $this->config('api_config.apisettings')
        ->set('delta_api_password', $form_state->getValue('delta_api_password'))->save();
    }
  }

}

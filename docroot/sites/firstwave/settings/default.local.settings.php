<?php


$databases['default']['default'] = array (
  'database' => 'drupal8',
  'username' => 'drupal8',
  'password' => 'drupal8',
  'prefix' => 'firstwave_',
  'host' => 'database',
  'port' => '3306',
  'namespace' => 'Drupal\\Core\\Database\\Driver\\mysql',
  'driver' => 'mysql',
);
$settings['hash_salt'] = 'VsgCF64Ck4LrqYDSZ5x2J69GrgB2U8TUgFMHSYh_6KIe77eqNKR29aM6K_QTB2FuniWPyqKHhw';

// Allow access from our internal Drupal sites
$settings['trusted_host_patterns'] = array(
 'BORNHCKHOST',
 'BORNHCKIPv4',
 '^firstwavelab\.com$',
 '^.+\.firstwavelab\.com$',
  '^amazonaws\.com$',
 '^.+\.amazonaws\.com$',
);

(function ($, Drupal, drupalSettings) {
   	Drupal.behaviors.companyTheme = {
		    attach: function(context, settings) {
          $("nav li.dropdown > a").click(function(){
            return false;
          })
          $("footer .copyright").insertAfter("footer > div nav:nth-last-child(2) .menu");

    			$(".mobile-menu").insertBefore("#block-company-theme-branding");

    			// Hamburger menu click event
    			$(".mobile-menu").click(function() {
    			   $(this).toggleClass('open');
    			   $("#block-company-theme-main-menu").toggle();
    			});

    			// When user click on mobile menu links, Hamburger menu should close
    			$("#block-company-theme-main-menu a").click(function() {
    			   $(".mobile-menu").toggleClass('open');
    			   $("#block-company-theme-main-menu").toggle();
    			});
	      }
    };

    // Slider carousel
    Drupal.behaviors.SliderCarousel = {
      attach: function (context, settings) {
        $(document).ready(function(){
          if($('.env-slider').length !== 0) {
            $('.env-slider').slick({
              dots: true,
              infinite: true,
              speed: 300,
              slidesToShow: 1,
              adaptiveHeight: true
            });
          }
        });
      }
    };

    //Home page Number counter in page load
    if($(".counter-content .counter-row").length !== 0) {
      $(".counter-content .counter-row").each(function(){
        var count = $(this).find('.counter-number').text();
        $(this).animate({ countNum: count }, {
          duration: 2000,
          easing: 'linear',
          step: function () {
            $(this).find('.counter-number').html(Math.floor(this.countNum));
          },
          complete: function () {
            $(this).find('.counter-number').html(this.countNum);
          }
        });
      });
    }


})(jQuery, Drupal);

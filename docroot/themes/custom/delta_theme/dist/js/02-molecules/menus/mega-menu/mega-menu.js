/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./components/02-molecules/menus/mega-menu/mega-menu.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./components/02-molecules/menus/mega-menu/mega-menu.js":
/*!**************************************************************!*\
  !*** ./components/02-molecules/menus/mega-menu/mega-menu.js ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

(function(){'use strict';var a=document.getElementById("toggle-expand-mobile"),b=document.getElementById("mega-nav-wrapper");if(null!==b&&0!==b)var c=b.getElementsByClassName("expand-sub");var d=document.getElementById("site-header"),e=document.getElementById("fullwidth-header");if(jQuery("#toggle-expand-mobile").click(function(){a.classList.toggle("toggle-expand--open"),b.classList.toggle("main-nav--open"),e.classList.toggle("active-main-mobile-header"),d.classList.toggle("active-mobile-header")}),0!=c&&null!=c&&null!=c)for(var f=0;f<c.length;f++)c[f].addEventListener("click",function(a){var b=a.currentTarget,c=b.nextElementSibling,d=b.parentElement;b.classList.toggle("expand-sub--open"),c.classList.toggle("mega-menu--sub-open"),d.classList.toggle("mega-menu--sub-parent-open")});jQuery("#toggle-expand-mobile").click(function(){jQuery("main.main").toggleClass("no-scrolling")}),jQuery(".mega-nav-wrapper > ul > li > .expand-sub").click(function(){jQuery(".menu-prod-title").next(".expand-sub").toggleClass("expand-sub--open"),jQuery(".menu-prod-title").next(".expand-sub").next(".mega-menu-nav").toggleClass("mega-menu--sub-open")}),jQuery(".mega-menu-nav a.disable-link").removeAttr("href")})();

/***/ })

/******/ });
//# sourceMappingURL=mega-menu.js.map
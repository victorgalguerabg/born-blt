/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./components/02-molecules/pdp-product-detail/pdp-product-detail.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./components/02-molecules/pdp-product-detail/pdp-product-detail.js":
/*!**************************************************************************!*\
  !*** ./components/02-molecules/pdp-product-detail/pdp-product-detail.js ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

(function(){'use strict';jQuery(".pdp-btn-buyonline a").click(function(){jQuery(".pdp-btn-find").removeClass("btn-hide"),jQuery(".pdp-btn-shop").removeClass("btn-hide"),jQuery(this).parent().hide()});var a=function(){if(-1!==window.location.href.indexOf("#documents-and-specs")){var a=jQuery(".pdp-tabs").offset().top-100;jQuery("html, body").animate({scrollTop:a}),jQuery("a[name='documents-and-specs']")[0].click()}if(-1!==window.location.href.indexOf("#product-features")){var a=jQuery(".pdp-tabs").offset().top-100;jQuery("html, body").animate({scrollTop:a}),jQuery("a[name='product-features']")[0].click()}if(-1!==window.location.href.indexOf("#reviews")){var a=jQuery(".pdp-tabs").offset().top-100;jQuery("html, body").animate({scrollTop:a}),jQuery("a[name='reviews']")[0].click()}if(-1!==window.location.href.indexOf("#questions-and-answers")){var a=jQuery(".pdp-tabs").offset().top-100;jQuery("html, body").animate({scrollTop:a}),jQuery("a[name='questions-and-answers']")[0].click()}if(-1!==window.location.href.indexOf("#parts-list")){var a=jQuery(".pdp-tabs").offset().top-100;jQuery("html, body").animate({scrollTop:a}),jQuery("a[name='parts-list']")[0].click()}},b=function(){if(-1!==window.location.href.indexOf("#reviewTab")){var a=jQuery(".pdp-tabs").offset().top-100;jQuery("html, body").animate({scrollTop:a}),jQuery("a[name='reviewsTab']")[0].click()}};jQuery(document).ready(function(){a(),b()})})();

/***/ })

/******/ });
//# sourceMappingURL=pdp-product-detail.js.map
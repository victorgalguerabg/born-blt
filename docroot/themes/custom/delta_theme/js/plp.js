(function ($, Drupal, drupalSettings, Backbone) {

  Drupal.behaviors.plpBehaviour = {
    attach: function (context, settings) {
      let gtm_plp = [];
      let gtm_plp_click = [];
      let productPerPage = parseInt(drupalSettings.productPerPage);
      let category = drupalSettings.productCategory;
      let secondaryCategory = drupalSettings.secondaryCategory;
      let productFacets = drupalSettings.productFacets;
      let filterFacets = drupalSettings.availableFacets.facets;
      let defaultFacets = drupalSettings.availableFacets.facets;
      let parentFacets = drupalSettings.facetParent;
      let facetMap = drupalSettings.facetMap;
      let cacheKeyData = drupalSettings.cacheKeyData;
      let facetMultiple = drupalSettings.facetMultiple;
      let facetData = drupalSettings.facetData;
      let categoryCount = drupalSettings.categoryCount;
      let setDefault = parseInt(drupalSettings.plpDefault);
      let productFilter = drupalSettings.productFilter;
      let pageNumber = 0;
      let action = 1;
      let clearall = 0;
      let loadProducts = true;
      let lastPage = false;
      let appliedFacets = '';
      let appliedSort = '';
      let defaultSort = 'sort_AvailableToShipDate=DESC&sort_ListPrice=DESC';
      let filterValue ='';
      let loader = $('.ajax-progress-throbber');
      let plpResults = $('.plp-results');
      let accordianHeader = "accordion-term accordion-term--light";
      let accordianOpen = "is-active";
      let headerClass = '';
      let filterBLockClass = '';
      let cacheFlag = 'T';
      let preservedFilters = '';
      let pathwithoutQuery = window.location.href.replace(window.location.search, "");
      let filtersforShare = '';
      let filtersFromUrl = 'F';
      let preserved_filters = '';
      let availablefilters = [];
      let availablefilterPairs = []; // Contains all the available filters in the PLP page.
      let validFilter = 'F';
      let savePrdObj = '';
      let defaultProduct = 1;
      let setFinishClass = "no-round";
      let _finishList = [];
      let _defaultFinish = '';
      let ignoreListFinish = ['na','notapplicable'];
      let ignoreStockTypeFinish = ['Obsolete','Use Up'];

      let size = $('.form-item__select--im').val();
      if (size == 'items-per-page') {
        size = productPerPage;
      }
      /* Click function for clearing all filters.*/
      $('#clear_filters').once('clearFilter').click(function(){
        $('.facets-checkbox').prop("checked", false);
        $('.js-filter-chip').remove();
        pageNumber = 0;
        defaultProduct = 1;
        applyFacets(1);
      });

      /* On selection of filters corresponding values sent to API. */
      $('.plp-sidebar').once().on('change', '.facets-checkbox', function () {
        filterValue = buildFilterParams($(this).attr('data-filter'), $(this).attr('id'), $(this).attr('value'), filtersFromUrl);
        let filterLabel = $('label[for="' + $(this).attr('id') + '"]').html();
        let filterId = $(this).attr('id');
        setFinishClass = filterId;
        let filterChecked = $(this).prop('checked');
        if (filterChecked == true) {
          filterFacets += filterValue;
          // Save selected filters a variable to be saved in localstorage.
          if(window.location.search == '?searchFilter=true'){
            localStorage.removeItem(window.location.href);
            localStorage.removeItem(window.location.href.replace(window.location.search, ""));
          }
          filterChips(filterLabel, filterId, 1);
        }
        else {
          filterFacets = filterFacets.replace(filterValue, "");
          localStorage.removeItem(pathwithoutQuery);
          localStorage.setItem(pathwithoutQuery, filterFacets);
          filterChips(filterLabel, filterId, 0);
        }
        pageNumber = 0;
        defaultProduct = 0;
        let checked_boxes = $('.plp-sidebar .facets-checkbox:checked').length;
        if(!checked_boxes) {
          defaultProduct = 1;
        }
        applyFacets();
      });
      /* Reset filters on clicking of filter chips */
      $('.plp-results__filters').once().on('click', '.filter-chip', function (){
        let filterParam = '';
        let chipId = $(this).attr('id');
        let facetId = chipId.split(/-(.+)?/)[1];
        let facetValue = '';
        $("button[id='" + chipId + "']").remove();
        $("input[id='" + facetId + "']").prop("checked", false);
        let filterAttrib = $("input[id='" + facetId + "']").attr('data-filter');
        filterParam = $("input[id='" + facetId + "']").attr('data-filter');
        facetValue = $("input[id='" + facetId + "']").val();
        if (filterAttrib == 'collections'){
          filterParam = 'or_' + filterAttrib;
        }
        if (filterAttrib == 'categories'){
          filterParam = 'and_' + filterAttrib;
          facetValue = encodeURIComponent(facetValue);
        }
        if (filterAttrib == 'facetFinish'){
          filterParam = 'or_' + filterAttrib;
          facetValue = $("input[id='" + facetId + "']").val().toLowerCase();
        }
        if (filterAttrib == 'facetStyle'){
          filterParam = $("input[id='" + facetId + "']").val();
          facetValue = 'T';
        }
        if ((filterAttrib == 'facetFeature') || (filterAttrib == 'facetInnovation') || (filterAttrib == 'facetHoleFilter')){
          filterParam = 'and_' + filterAttrib;
          facetValue = $("input[id='" + facetId + "']").val();
        }
        if(filterAttrib == 'FlowRate'){
          filterParam = 'and_' + filterAttrib;
          facetValue = $("input[id='" + facetId + "']").val().replace(/ /g, '+');;
        }
        if(filterAttrib == 'ListPrice200DollarRangeFacet') {
          filterParam = 'range_ListPrice';
          facetValue = $("input[id='" + facetId + "']").val();
        }
        if(filterAttrib == 'DrainType') {
          facetValue  = $("input[id='" + facetId + "']").val().split(" ").join("+");
        }
        if(filterAttrib == 'ValveType') {
          facetValue  = $("input[id='" + facetId + "']").val().replace(/ /g, '+');
        }
        let filterValue  = '&' + filterParam + '=' + facetValue + "&";
        if(filterAttrib == 'facetNarrowResults') {
          if($(this).attr('id') == 'chip-Show Discontinued Products'){
            filterValue  = '&or_MaterialStatus=50&or_MaterialStatus=60&or_MaterialStatus=70&';
          }
          else if($(this).attr('id') == 'chip-Hide Retail exclusive'){
            filterValue  = '&availableToTrade=true';
          }
        }
        let updatedFacets = filterFacets.replace(filterValue,'');
        filterFacets = updatedFacets;
        localStorage.removeItem(pathwithoutQuery);
        localStorage.setItem(pathwithoutQuery, filterFacets);
        pageNumber = 0;
        defaultProduct = 0;
        let checked_boxes = $('.plp-sidebar .facets-checkbox:checked').length;
        if(!checked_boxes) {
          defaultProduct = 1;
        }
        applyFacets();
      });

      /* Sorting products. */
      $('.sort_elements').once().on('change', '.form-item__select--sm', function (){
        let sortValue = $(this).find('option:selected').val();
        if (typeof sortValue !== 'undefined'){
          appliedSort = sortValue;
          localStorage.setItem("selected-sort", appliedSort);
        }
        else{
          appliedSort = appliedSort.replace(sortValue, "");
        }
        pageNumber = 0;
        applyFacets();
      });

      $('.sort_elements').on('change', '.form-item__select--im', function (){
        size = $('.form-item__select--im').val();
        if (size == 'items-per-page') {
          size = productPerPage;
        }
        pageNumber = 0;
        applyFacets();
      });

      // Renders product data in template using backbone JS.
      let ProdView = Backbone.View.extend({
        el: "#output",
        template: _.template($("#output-node").html()),
        initialize: function (initData, append) {
          this.render(initData, append);
          this.postRender(initData, append);
        },
        postRender: function (initData, append) {
          savePrdObj = initData;
          greyOutOnOffFilters(savePrdObj);
        },
        render: function (initData, append) {
          let _self = this;
          let _render = "";
          $("#output").addClass('grid');
            let plp_position = 0;
            if (initData.content != '') {
            _.each(initData.content, function (entry) {
              let plp_product = new Object();
              plp_position = plp_position + 1;
              plp_product.list = "product listing page";
              plp_product.brand = entry.collections[0];
              plp_product.name = entry.description;
              plp_product.position = plp_position;
              plp_product.price = '$' + entry.values.ListPrice;
              plp_product.id = entry.name;
              plp_product.variant = entry.values.Finish;
              let _price = entry.values.ListPrice;
              let _heroImage = entry.heroImageSmall;
              let _title = "Title";
              let _availableToOrderDate = entry.values.AvailableToOrderDateTimestamp;
              let _availableToShipDate = entry.values.AvailableToShipDateTimestamp;
              let _currentDate = entry.values.currentDate;
              let _viewAllCollectionLink = entry.values.viewAllCollectionLink;
              let _ribbonText = '';
              let _ribbonColor = '';
              let _refurbished = 0;
              if(entry.categories != undefined && entry.categories.length > 0) {
                entry.categories.forEach(function(gtm_cat){
                    if (gtm_cat.startsWith("Delta_") == true){
                        plp_product.category = gtm_cat.replace("_", "/");
                    }
                });
                // Product Banner Ribbon Text
                entry.categories.forEach(function(gtm_cat){
                    if (gtm_cat.startsWith("Delta_Outlet_Recommerce") == true){
                      _refurbished = 1;
                    }
                });
              }
              if (entry.description != '') {
                _title = entry.description;
              }
              let _collection = "COLLECTION NAME";
              if (entry.values.Collection != "") {
                _collection = entry.values.Collection;
              }
              let _sku = entry.name;
              let _pdp = '/product-detail/' + entry.name;
              let _finishList = entry.finishOptions;
              let _defaultFinish = entry.facetFinish;

              // Checking _refurbished product
              if(_refurbished == 1) {
                _ribbonText = "Recertified";
                _ribbonColor = "gray-30 banner-recertified";
              }
              else if(entry.values.discontinued) {
                  _ribbonText = entry.values.discontinueText;
                  _ribbonColor = "gray-30";
              }
              else {
                  if (_currentDate < _availableToShipDate) {
                      if (_currentDate > _availableToOrderDate) {
                          _ribbonText = entry.values.comingSoonText;
                          _ribbonColor = "delta-red";
                      }
                  }
              }
              gtm_plp.push(plp_product);
              gtm_plp_click[entry.name] = plp_product;

              let _listPrice = "";
              if(_refurbished == 1) {
                _listPrice = entry.values.SalePrice;
              }
              var getAPiFinishName = '';
              var setFinishIconPath = '';
              const getEachFinish = [];
              new Date;
              //For refubished products
                if(_refurbished == 1){
                  for (var i = 0; i <= _finishList.length; i++ ){
                  if(_finishList[i] !== undefined && typeof _finishList[i].facetFinish !== 'undefined' && _finishList[i].modelNumber.indexOf('-IN') == '-1' && (Date.now() > _finishList[i].availableToOrderDate)){
                    var getAPiFinishName = _finishList[i].facetFinish;
                    var getAPiFinishST = _finishList[i].stockingType;
                    var getTermFinishObj = facetData[getAPiFinishName];
                    var existInIgnoreList = ignoreListFinish.indexOf(getAPiFinishName);
                    var existIgnoreStockList = ignoreStockTypeFinish.indexOf(getAPiFinishST);
                    var finishModelNumber = _finishList[i].modelNumber;
                    if(finishModelNumber == _sku){
                      if(getTermFinishObj !== undefined && existInIgnoreList == -1 && existIgnoreStockList == -1 && getAPiFinishST != ''){
                         setFinishIconPath = getTermFinishObj.icon;
                         var imgIcon = '<a data="'+ _sku +'" href="/product-detail/' + finishModelNumber + '" class="no-line product-action-link">' +'<img src="' + setFinishIconPath + '" alt="'+ _finishList[i].facetFinish +'" title="'+ _finishList[i].finish +'" class="active finish-class"  /></a>';
                         getEachFinish.push(imgIcon);
                      }
                    }
                  }
                }
              }
              //For normal products
              if(_refurbished == 0){
                for (var i = 0; i <= _finishList.length; i++ ){
                if(_finishList[i] !== undefined && typeof _finishList[i].facetFinish !== 'undefined' && _finishList[i].modelNumber.indexOf('-IN') == '-1' && _finishList[i].modelNumber.slice(-2) != '-R'  && (Date.now() > _finishList[i].availableToOrderDate)){
                  var getAPiFinishName = _finishList[i].facetFinish;
                  var getAPiFinishST = _finishList[i].stockingType;
                  ActiveClass = 'in-active finish-class';
                  if(setFinishClass == getAPiFinishName){
                    ActiveClass = 'active finish-class';
                    _defaultFinish = '';
                  }
                  if(_defaultFinish == getAPiFinishName){
                    ActiveClass = 'active finish-class';
                  }
                  var getTermFinishObj = facetData[getAPiFinishName];
                  var existInIgnoreList = ignoreListFinish.indexOf(getAPiFinishName);
                  var existIgnoreStockList = ignoreStockTypeFinish.indexOf(getAPiFinishST);
                  if(getTermFinishObj !== undefined && existInIgnoreList == -1 && existIgnoreStockList == -1 && getAPiFinishST != ''){
                     setFinishIconPath = getTermFinishObj.icon;
                     var finishModelNumber = _finishList[i].modelNumber;
                     var imgIcon = '<a data="'+ _sku +'" href="/product-detail/' + finishModelNumber + '" class="no-line product-action-link">' +'<img src="' + setFinishIconPath + '" alt="'+ _finishList[i].facetFinish +'" title="'+ _finishList[i].finish +'" class="'+ ActiveClass +'"  /></a>';
                     getEachFinish.push(imgIcon);
                  }
                }
              }
            }

              _render += _self.template({
                  title: _title,
                  collectionName: _collection,
                  heroImage: _heroImage,
                  finishOptions: getEachFinish,
                  sku: _sku,
                  price: _price,
                  listprice: _listPrice,
                  pdp: _pdp,
                  ribbonText: _ribbonText,
                  ribbonColor: _ribbonColor,
                  viewAllCollectionLink:  _viewAllCollectionLink,
                  refurbished: _refurbished
              });
            });
            if (gtm_plp != undefined && gtm_plp.length > 0) {
              dataLayer.push({
                  'event':'pageReady',
                  'ecommerce': {
                      'impressions': gtm_plp
                  },
              });
                dataLayer.push({
                    'event':'productImpression',
                    'ecommerce': {
                        'impressions': gtm_plp
                    },
                });
            }
            if (append == true) {
              _self.$el.html(_render);
            }
            if (append == false) {
              _self.$el.append(_render);
            }
          }
          else {
            var noResultsMessage = '<div class ="error-message" role="contentinfo" aria-label="Error message">Oops!!! Nothing Found. Please refine your search</div>';
            $('#output').html(noResultsMessage);
          }
            // Product Click GA Event
            $("a.product-action-link",context).on("click", function (event) {
                event.stopPropagation();
                event.preventDefault();
                var currentUrl = $(this).attr("href");
                var spitedUrl = currentUrl.split("/");
                var parentClass = $(this).parent().attr("class");
                //When user click finishes, which is not an current sku.
                if(parentClass == 'prod-finishes' && gtm_plp_click[spitedUrl[2]] == undefined){
                  spitedUrl[2] = $(this).attr('data');
                }
                if(gtm_plp_click[spitedUrl[2]] != undefined) {
                    delete gtm_plp_click[spitedUrl[2]]["list"];
                    dataLayer.push({
                        'event':'productClick',
                        'ecommerce': {
                            'click': {
                                'actionField': {'list': 'plp'},
                                'products': [gtm_plp_click[spitedUrl[2]]]
                            }
                        },
                        'eventCallback': function() {
                            document.location = currentUrl;
                        }
                    });
                }
                document.location = currentUrl;
            });
          return this;
        },
      });

      //Facet View
      let facetView = Backbone.View.extend({
        initialize: function (initData, parentFacets, facetMap) {

          let facetBlock = getFacetBlock(initData);
          this.render(initData, parentFacets, facetBlock, facetMap);
        },
        events: {
          'click [type="checkbox"]': 'applyFacets',
        },
        applyFacets: function (e) {
          loadProducts = true;
        },
        render: function (initData, parentFacets, facetBlock, facetMap) {
          _.each(parentFacets,function (menu, index) {
            let targetContainer = '#' + index;
            let i;
            let count = menu.length;
            for (i = 0; i < menu.length; ++i) {

              if(count > 1){
                if((menu[i] == 'facetstyle') || (menu[i] == 'facetnarrowresults')){
                  facetDataName = facetData[menu[i]]['name'];
                }
                else{
                  facetDataName = facetData[menu[i]];
                }
                let subTitle = ( typeof facetData[menu[i]] !== 'undefined') ?
                  facetData[menu[i]] : menu[i];
                $(targetContainer).append('<h5 class="product-filter__section__subtitle">' +
                facetDataName +
                  '</h5>');
              }

              let collapseValue = facetMultiple[index]['collapse'];
              let minFilters = facetMultiple[index]['minFilters'];
              let maxFilters = facetMultiple[index]['maxFilters'];

              let filterContainer = '<div data-min="'+ minFilters +'" data-max="'+
               maxFilters +'" class="filter-container '+menu[i]+'">'+
               facetBlock[menu[i]] +
               '</div>';

             $(targetContainer).append(filterContainer);
             $(targetContainer).append(
              '<div class="product-filter-more filter-controls">View More</div> <div class="product-filter-less filter-controls">View Less</div>'
            );

              if(collapseValue == 1) {
                headerClass = accordianHeader + " " + accordianOpen;
                filterBlockClass = "accordion-def";
              }else if(collapseValue == 2) {
                headerClass = accordianHeader;
                filterBlockClass = "accordion-def";
              }

              let container = '<div class="product-filter__section__body '
                + filterBlockClass + '" data-min="'+ minFilters +'" data-max="'+
                maxFilters +'" >';

              if(count == 1){
                let header = '<div class="'+ headerClass +'">\n' +
                  '<h4 class="product-filter__section__title">' +
                      facetMultiple[index]['name'] +
                  '</h4>\n' +
                  '</div>';

                $(targetContainer).contents().wrapAll(
                  container
                );
                $(targetContainer).prepend(header);
              }else if(count > 1 && i == (menu.length -1)){
                let header = '<div class="'+ headerClass + '">\n' +
                  '<h4 class="product-filter__section__title">' +
                    facetMultiple[index]['name'] +
                  '</h4>\n' +
                  '</div>';
                $(targetContainer).contents().wrapAll(
                  container
                );
                $(targetContainer).prepend(header);
              }

            }
          });

          jQuery(".filter-container").each(function(){
            if(!jQuery(this).hasClass('collections')){
              jQuery(this).html(jQuery(this).children('.custom-input.custom-input--checkbox').sort(function(a, b){
                return (jQuery(b).data('position')) < (jQuery(a).data('position')) ? 1 : -1;
              }))
            }
          });
          jQuery(".custom-input__native").each(function(){
            availablefilters.push(jQuery(this).attr("data-filter") + "=" + jQuery(this).attr("value"));
          });
          availablefilterPairs = availablefilters;
          reviewFacet();
          /* Customization of facets block. */
          jQuery(".filter-container").each(function(){
            var sum = 0;
            var minval = jQuery(this).attr("data-min");
            var showval = minval - 1;
            var ttlchild = $(this).children().length;

            jQuery(this).children(".custom-input.custom-input--checkbox").each(function(){
              jQuery(this).css("height", jQuery(this).outerHeight());
              if(jQuery(this).index() <= showval) {
                var varheight = 0;
                varheight = jQuery(this).outerHeight();
                sum = sum + varheight;
              }

            });
            //Making next to next all to remove view all link.
            if(ttlchild <= parseInt(minval)) {
              jQuery(this).next(".product-filter-more").remove();
              jQuery(this).next(".product-filter-less").remove();
            }

            jQuery(this).css("max-height", sum);
          });

          jQuery(".product-filter-less").hide();
          viewMore();

          if(jQuery(".filter-icon-image").length) {
            jQuery(".filter-icon-image").parent(".custom-input.custom-input--checkbox").addClass("custom-filter-icon");
          }
          var pURL = $(location).attr("href");
          var question_mark = pURL.indexOf("?");
          var facet_text = pURL.substr(question_mark+1);
          var split_text = facet_text.split("=");
          var queryFacetCategory = split_text[0];
          var queryFacetValue = split_text[1];
          jQuery(".custom-input").each(function () {
            if(window.location.search){
              viewMore();
            }
          });
         // alert(savePrdObj);
          let response = new ProdView(savePrdObj, true);
          jQuery(".custom-input").each(function () {
            if((jQuery(this).find(".custom-input__native").attr("value") === queryFacetValue) && ((jQuery(this).find(".custom-input__native").attr("value") === queryFacetCategory))) {
              jQuery(this).find(".custom-input__native").attr("checked", "checked");
            }
            if(window.location.search){
              // When user navigates back to PLP page from any other page.
              if(window.location.search == '?searchFilter=true'){
                if(localStorage.getItem(pathwithoutQuery)) {
                  preserved_filters = localStorage.getItem(pathwithoutQuery).replace(/^&|&$/g,'');
                }
                else if(localStorage.getItem(window.location.href)){
                  preserved_filters = localStorage.getItem(window.location.href).replace(/^&|&$/g,'');
                }
              }
              // When filters are entered in the url as query parameters.
              else{
                checkedFilters = filterFacets.replace(defaultFacets, "");
                preserved_filters = filtersforShare.length ? checkedFilters.replace(/^&|&$/g,'').replace(/or_|and_/g,'').replace(/\s+/g, '+') : '';
              }
              if (preserved_filters != ''){
                if($("input.facets-checkbox").hasClass('greyed-out')){
                  $("input.facets-checkbox").removeClass('greyed-out').addClass('greyed-out-on').attr("disabled", true);
                }else{
                  $("input.facets-checkbox").addClass('greyed-out-on').attr("disabled", true);
              }

                greyOutOnOffFilters(savePrdObj);
                filterString = preserved_filters.split("&");
                var checkboxCategory = [];
                var checkboxName = [];
                var onetext = '';
                var twotext = '';
                for (var i = 0; i < filterString.length; i++) {
                  if (filterString[i] !== "" && filterString[i] !== null) {
                    onetext = filterString[i];
                    twotext = onetext.split('=');
                    checkboxCategory.push(twotext[0]);
                    checkboxName.push(twotext[1]);
                  }
                }
                for (var j = 0; j < checkboxName.length; j++) {
                  if (checkboxName[j] !== "" && checkboxName[j] !== null) {
                    if(checkboxCategory[j] == 'DrainType'){
                      checkBoxValue  = checkboxName[j].split("+").join(" ");
                    }
                    else if((checkboxCategory[j] == 'or_MaterialStatus') || (checkboxCategory[j] == 'MaterialStatus')){
                      checkBoxValue = 'Show Discontinued Products';
                    }
                    else{
                      checkBoxValue  = checkboxName[j];
                    }
                    if(checkBoxValue.indexOf('+')){
                      checkBoxValue = checkBoxValue.replace(/\+/g, ' ');
                    }
                    if(((jQuery(this).find(".custom-input__native").attr("value") === decodeURIComponent(checkBoxValue)) || (jQuery(this).find(".custom-input__native").attr("value") === checkBoxValue) && (jQuery(this).find(".custom-input__native").attr("value") === checkboxCategory[j])) || (jQuery(this).find(".custom-input__native").attr("value") === checkboxCategory[j])){
                      jQuery(this).find(".custom-input__native").attr("checked", "checked");
                      filterChips(jQuery(this).text(), jQuery(this).find(".custom-input__native").attr("id"), 1);
                      //jQuery('.js-filter-chip').show();
                      jQuery("button[id='chip-" + jQuery(this).find(".custom-input__native").attr("id") + "']").show();
                      if((checkboxCategory[j] == 'or_MaterialStatus') || (checkboxCategory[j] == 'MaterialStatus')){
                        break;
                      }
                    }
                  }
                }
                if($('.filter-chip').length < 1){
                  $('#clear_filters').hide();
                }else{
                  $('#clear_filters').show();
                }
              }
            }
          });
        }
      });

      //Get List price from DataBase

      function getListPrice(sku) {
        let listPrice = "";
        $.ajax({
          type: "GET",
          url: "/get-list-price",
          dataType: 'json',
          async: false,
          data: {
            sku: sku,
          },
           error: function (XMLHttpRequest, textStatus, errorThrown) {
             return false;
            },
          success: function (data) {
            if(!jQuery.isEmptyObject(data)){
              listPrice = data;
            }
          }
        });
        return listPrice;
      }

      /* Applies selected filters and makes API calls. */
      function applyFacets(clearall) {
        cacheFlag = 'F';
        if(appliedSort == ''){
          appliedSort = defaultSort;
        }
        loader.show();
        $('.plp-results__header').hide();
        $('.plp-pagination-output').hide('');
        loadProducts = true;
        if(clearall){
          filterFacets = defaultFacets;
        }
        if ($("input.facets-checkbox").hasClass('greyed-out')) {
          $("input.facets-checkbox").removeClass('greyed-out').addClass('greyed-out-on').attr("disabled", true);
        } else {
          $("input.facets-checkbox").addClass('greyed-out-on').attr("disabled", true);
        }
        // Dont greyout narrow result
        $('#narrowresults input[type=checkbox]').removeClass('greyed-out').removeClass('greyed-out-on').removeAttr("disabled");
        let applyFacetsFilter = productFacets;
        if (setDefault && defaultProduct) {
          applyFacetsFilter = productFacets + productFilter;
        }
        $.ajax({
          type: "GET",
          url: "/product-listing",
          dataType: 'json',
          data: {
            size: size,
            category: category,
            secondaryCategory: secondaryCategory,
            productFacets: applyFacetsFilter,
            categoryCount: categoryCount,
            facets: filterFacets,
            sort: appliedSort,
            cacheKeyData: cacheKeyData,
            pagenumber: pageNumber,
            cache: cacheFlag,
          },
           error: function (XMLHttpRequest, textStatus, errorThrown) {
             loader.hide();
             $('.plp-results').show();
             $('.product-count').html('0');
             var noResultsMessage = '<div class ="plp-api-error error-message" role="contentinfo" aria-label="Error message">Something has gone wrong on our end, please try refreshing this&nbsp;<a href="'+window.location.href+'">page</a>. If the issue persists, please try again later</div>';
             $('.plp-pagination-output').html('');
             $('#output').html(noResultsMessage);
             return false;
            },
          success: function (data) {
            if (data.totalElements > 0) {
              $('.plp-results__header').show();
              $('.plp-pagination-output').hide();
              $('.form-item__select--im').hide();
              if(data.totalElements > 24){
                $('.plp-pagination-output').show();
                $('.form-item__select--im').show();
              }
            } else {
              $("input.facets-checkbox").removeClass('greyed-out-on').attr("disabled", false);
            }
            let response = new ProdView(data, true);
            if(data){
              data.currentpage = 1;
            }
            let PlppaginationView = new plppaginationView(data);
            PlppaginationView.undelegateEvents();
            loadProducts = true;
            lastPage = data.last;
            pageNumber++;
            $('.product-count').html();
            let activePage = data.number +1;
            let from = ((activePage-1)*size)+1;
            let to = activePage*size;
            if (to > data.totalElements) {
              to = data.totalElements;
            }
            let resultString = "Results <strong>"+from+"-"+to+"</strong> of <strong>"+data.totalElements + "</strong>";
            $('.product-count').html(resultString);
            $('.category-name').html(category);
            $('.loader').hide();
            if($('.filter-chip').length < 1){
              $('#clear_filters').hide();
            }else{
              $('#clear_filters').show();
            }
            $('.js-filter-chip').show();
            loader.hide();
          }
        });
        preservedFilters = filterFacets;
        localStorage.setItem(window.location.href, preservedFilters);
        filtersApplied = filterFacets.replace(defaultFacets, "");
        filtersforShare = filtersApplied.replace(/^&|&$/g,'').replace(/or_|and_/g,'').replace(/\s+/g, '+');
        localStorage.setItem(pathwithoutQuery,filtersforShare);
      }
      jQuery(document).ready(function() {
        pageShown();
        $('.node--type-plp', context).once('getProducts').each(function () {
          apiCallforfilters();
          apiCallforProducts();
        });
      });

      function reviewFacet() {
        if(window.location.search){
          queryString = window.location.search.replace('?','');
          if(queryString.indexOf('=')){
            filterKey = queryString.split('=')[0];
          }
          else{
            filterKey = queryString;
          }
          var numericFacets = ['range_ListPrice'];
          if((availablefilterPairs && jQuery.inArray(filterKey,availablefilterPairs)) || (numericFacets.indexOf(filterKey) != -1) ||window.location.search == '?searchFilter=true'){
            validFilter = 'T';
          }
        }

        if(validFilter == 'T'){
          if(window.location.search == '?searchFilter=true'){
            if(localStorage.getItem(pathwithoutQuery)) {
              preFilterFacets = localStorage.getItem(pathwithoutQuery);
              facetinQuery = preFilterFacets.split('&');
              var queryFacetCategory = '';
              var queryFacetValue = '';
              var onetext = '';
              var twotext = '';
              for (var i = 0; i < facetinQuery.length; i++) {
                if (facetinQuery[i] !== "" && facetinQuery[i] !== null) {
                  onetext = facetinQuery[i];
                  twotext = onetext.split('=');
                  let decodedValue = decodeQueryParam(facetinQuery[i]);
                  if (twotext.length > 1){
                    queryFacetValue = twotext[1].replace('+', ' ');
                    var exceptionalFilters = ['StyleIsContemporary', 'StyleIsTransitional', 'StyleIsTraditional'];
                    if(exceptionalFilters.indexOf(twotext[0]) != -1 && twotext[1] == 'T'){
                      queryFacetCategory = twotext[0];
                      filtersFromUrl = 'T';
                      filterValue = buildFilterParams(queryFacetCategory , queryFacetValue, queryFacetValue, filtersFromUrl);
                      filterFacets += filterValue;
                    }
                    else if(availablefilterPairs && (jQuery.inArray(decodedValue,availablefilterPairs) !== -1)){
                      queryFacetCategory = twotext[0];
                      filtersFromUrl = 'T';
                      filterValue = buildFilterParams(queryFacetCategory , queryFacetValue, queryFacetValue, filtersFromUrl);
                      filterFacets += filterValue;
                    }
                    if(twotext[0] == 'range_ListPrice') {
                      queryFacetCategory = 'ListPrice200DollarRangeFacet';
                      filtersFromUrl = 'T';
                      filterValue = buildFilterParams(queryFacetCategory , queryFacetValue, queryFacetValue, filtersFromUrl);
                      filterFacets += filterValue;
                    }
                    var exceptionalNarrowFilters = ['MaterialStatus', 'availableToTrade'];
                    var allowedMaterialStatus = ['50','60','70'];
                    if((exceptionalNarrowFilters.indexOf(twotext[0]) != -1 && twotext[1] == 'true') || (exceptionalNarrowFilters.indexOf(twotext[0]) != -1 && allowedMaterialStatus.indexOf(twotext[1]) != -1)) {
                      queryFacetCategory = twotext[0];
                      filtersFromUrl = 'T';
                      filterValue = buildFilterParams(queryFacetCategory , queryFacetValue, queryFacetValue, filtersFromUrl);
                      filterFacets += filterValue;
                    }
                  }
                }
              }
              filtersforShare = filterFacets.replace(defaultFacets, "").replace(/^&|&$/g,'').replace(/or_|and_/g,'').replace(/\s+/g, '+');
              localStorage.setItem(pathwithoutQuery,filtersforShare);
              cacheFlag = 'F';
            }
            else if(localStorage.getItem(window.location.href)){
              filterFacets = localStorage.getItem(window.location.href);
              filtersforShare = localStorage.getItem(window.location.href).replace(defaultFacets, "").replace(/^&|&$/g,'').replace(/or_|and_/g,'').replace(/\s+/g, '+');
              cacheFlag = 'F';
            }
          }
          else {
            let facetinQuery = '';
            if (document.location.search.indexOf("%20&%20") != -1){
              facetinQuery = document.location.search.replace(/%20&%20/gi, "ampersand").replace('?','').split('&');
            }
            else {
              facetinQuery = document.location.search.replace('?','').split('&');
            }
            var queryFacetCategory = '';
            var queryFacetValue = '';
            var onetext = '';
            var twotext = '';
            for (var i = 0; i < facetinQuery.length; i++) {
              if (facetinQuery[i] !== "" && facetinQuery[i] !== null) {
                onetext = facetinQuery[i];
                twotext = onetext.split('=');
                let decodedValue = decodeQueryParam(facetinQuery[i]);
                if (twotext.length > 1){
                  queryFacetValue = twotext[1].replace('+', ' ');
                  var exceptionalFilters = ['StyleIsContemporary', 'StyleIsTransitional', 'StyleIsTraditional'];
                  if(exceptionalFilters.indexOf(twotext[0]) != -1 && twotext[1] == 'T'){
                    queryFacetCategory = twotext[0];
                    filtersFromUrl = 'T';
                    filterValue = buildFilterParams(queryFacetCategory , queryFacetValue, queryFacetValue, filtersFromUrl);
                    filterFacets += filterValue;
                  }
                  else if(availablefilterPairs && (jQuery.inArray(decodedValue,availablefilterPairs) !== -1)){
                    queryFacetCategory = twotext[0];
                    filtersFromUrl = 'T';
                    filterValue = buildFilterParams(queryFacetCategory , queryFacetValue, queryFacetValue, filtersFromUrl);
                    filterFacets += filterValue;
                  }
                  if(twotext[0] == 'range_ListPrice') {
                    queryFacetCategory = 'ListPrice200DollarRangeFacet';
                    filtersFromUrl = 'T';
                    filterValue = buildFilterParams(queryFacetCategory , queryFacetValue, queryFacetValue, filtersFromUrl);
                    filterFacets += filterValue;
                  }
                  var exceptionalNarrowFilters = ['MaterialStatus', 'availableToTrade'];
                  var allowedMaterialStatus = ['50','60','70'];
                  if((exceptionalNarrowFilters.indexOf(twotext[0]) != -1 && twotext[1] == 'true') || (exceptionalNarrowFilters.indexOf(twotext[0]) != -1 && allowedMaterialStatus.indexOf(twotext[1]) != -1)) {
                    queryFacetCategory = twotext[0];
                    filtersFromUrl = 'T';
                    filterValue = buildFilterParams(queryFacetCategory , queryFacetValue, queryFacetValue, filtersFromUrl);
                    filterFacets += filterValue;
                  }
                }
              }
              filtersforShare = filterFacets.replace(defaultFacets, "").replace(/^&|&$/g,'').replace(/or_|and_/g,'').replace(/\s+/g, '+');
              localStorage.setItem(pathwithoutQuery,filtersforShare);
              cacheFlag = 'F';
            }
          }
        }
        else{
          filterFacets = defaultFacets;
        }
      }

      function decodeQueryParam(p) {
        return decodeURIComponent(p.replace(/\+/g, ' '));
      }
      /* API call to get filters */
      function apiCallforfilters() {
        loader.show();
        let filterproductFacets = productFacets;
        $.ajax({
          type: "GET",
          url: "/product-listing",
          dataType: 'json',
          async:false,
          data: {
            size: size,
            category: category,
            secondaryCategory: secondaryCategory,
            productFacets: filterproductFacets,
            categoryCount: categoryCount,
            facets: defaultFacets,
            sort: defaultSort,
            cacheKeyData: "filters-"+cacheKeyData,
            pagenumber: pageNumber,
            cache: cacheFlag,
           },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
              loader.hide();
              $('.plp-results').show();
              $('.product-count').html('0');
              var noResultsMessage = '<div class ="plp-api-error error-message" role="contentinfo" aria-label="Error message">Something has gone wrong on our end, please try refreshing this&nbsp;<a href="'+window.location.href+'">page</a>. If the issue persists, please try again later</div>';
              $('#output').html(noResultsMessage);
              return false;
            },
          success: function (data) {
            $('.product-filter').show();
            let FacetView = new facetView(data, parentFacets, facetMap);
            if ($.trim($('.product-filter__body .region-sidebar').text()) == ''){
              $('.product-filter').hide();
            }
            else{
              $('.product-filter').show();
            }
          }
        });
      }
      /* API call to get products */
      function apiCallforProducts() {
        loadProducts = false;
        plpResults.hide();
        $('.plp-results__header').hide();
        $('.plp-pagination-output').hide();
        let appliedFilter = localStorage.getItem(pathwithoutQuery);
        let urlQueryString = document.location.search.replace('?','');
        if((urlQueryString == "") || (urlQueryString == "searchFilter=true" && appliedFilter == "")) {
          localStorage.setItem(pathwithoutQuery,"");
          localStorage.setItem(window.location.href,"");
          filterFacets = defaultFacets;
          if(urlQueryString == "") {
            defaultSort = $('.sort_elements .form-item__select--sm').find('option:selected').val();
            localStorage.setItem("selected-sort", defaultSort);
          }
          let selectedSort = localStorage.getItem("selected-sort");
          if(selectedSort && (urlQueryString == "searchFilter=true")) {
            defaultSort = selectedSort;
            cacheFlag = 'F';
          }
        }
        let existingFilter = localStorage.getItem(pathwithoutQuery);
        let productFacetsFilter = productFacets;
        if (setDefault && existingFilter == "") {
          productFacetsFilter = productFacets + productFilter;
        }
        $.ajax({
          type: "GET",
          url: "/product-listing",
          dataType: 'json',
          async: false,
          data: {
            size: size,
            category: category,
            secondaryCategory: secondaryCategory,
            productFacets: productFacetsFilter,
            categoryCount: categoryCount,
            facets: filterFacets,
            sort: defaultSort,
            cacheKeyData: cacheKeyData,
            pagenumber: pageNumber,
            cache: cacheFlag,
          },
          error: function (XMLHttpRequest, textStatus, errorThrown) {
            loader.hide();
            $('.plp-results').show();
            $('.product-count').html('0');
            var noResultsMessage = '<div class ="plp-api-error error-message" role="contentinfo" aria-label="Error message">Something has gone wrong on our end, please try refreshing this&nbsp;<a href="'+window.location.href+'">page</a>. If the issue persists, please try again later</div>';
            $('#output').html(noResultsMessage);
            $('.plp-pagination-output').html('');
            return false;
          },
          success: function (data) {
            if (data.totalElements > 0) {
              $('.plp-results__header').show();
              $('.plp-pagination-output').hide();
              $('.form-item__select--im').hide();
              if(data.totalElements > 24){
                $('.form-item__select--im').show();
                $('.plp-pagination-output').show();
              }
            }
            $('.product-filter').show();
            let response = new ProdView(data, false);
            if(data){
              data.currentpage = 1;
            }
            let PlppaginationView = new plppaginationView(data);
            lastPage = data.last;
            pageNumber++;
            loadProducts = true;
            let activePage = data.number +1;
            let from = ((activePage-1)*size)+1;
            let to = activePage*size;
            if (to > data.totalElements) {
              to = data.totalElements;
            }
            let resultString = "Results <strong>"+from+"-"+to+"</strong> of <strong>"+data.totalElements + "</strong>";
            $('.product-count').html(resultString);
            $('.category-name').html(category);
            $('.loader').hide();
            loader.hide();
            plpResults.show();
          }
        });
      }

      /* Creating the filter chips after filters selected. */
      function filterChips(displayName, elemId, action){
        if(action == 0){
          $("button[id='chip-" + elemId + "']").remove();
        }else{
          let chip = '<button class="filter-chip js-filter-chip" ' +
            'id="chip-'+ elemId +'" style="display:none">\n' +
            displayName +
            '<svg class="icon icon--filter-hide-small" aria-hidden="true" role="img" width="12" height="12" aria-labelledby="title-filter-hide-small">\n' +
            '      <title id="chip-'+ displayName +'">Remove</title>\n' +
            '  <use xlink:href="/themes/custom/delta_theme/dist/img/sprite/sprite.svg#filter-hide-small"></use>\n' +
            '</svg>\n' +
            '</button>';
          $('.plp-results .plp-results__filters').prepend(chip);
        }
      }

      // Grey out on and off on facet filters
      var greyOutOnOffFilters = function (initData) {
          let StyleIsContemporaryCnt = 0;
          let StyleIsTraditionalCnt = 0;
          let StyleIsTransitionalCnt = 0;
          $( ".product-filter-more" ).trigger( "click" );
          $( ".product-filter-less" ).trigger( "click" );
        _.each(initData.content, function (getProducts) {
            if(getProducts.values.StyleIsTransitional == 'T' && StyleIsTransitionalCnt == 0){
              StyleIsTransitionalCnt++;
              $('.Transitional').removeClass('greyed-out-on').addClass('greyed-out-off').removeAttr("disabled");
            }
            if(getProducts.values.StyleIsTraditional == 'T' && StyleIsTraditionalCnt == 0){
              StyleIsTraditionalCnt++;
               $('.Traditional').removeClass('greyed-out-on').addClass('greyed-out-off').removeAttr("disabled");
            }
            if(getProducts.values.StyleIsContemporary == 'T' && StyleIsContemporaryCnt == 0){
              StyleIsContemporaryCnt++;
               $('.Contemporary').removeClass('greyed-out-on').addClass('greyed-out-off').removeAttr("disabled");
            }
          });

          _.each(initData.facets, function (facet) {
            let facetName = facet.name;
            let facetType = facet.type;
            let output = '';
            let facetCount = 0;
            let helpText = '';
            let termWeight = '';
            let termId = '';
            let icon_img = '';
            if (facetType == 'term') {
              facetTerms = facet.terms;
              _.each(facetTerms, function (terms) {
                let termValues = facetData[terms.term];
                if((typeof termValues == 'object')) {
                  if (termValues.length == 0) {
                    termValues = void 0;
                  }
                }
                let termName = (typeof termValues !== 'undefined') ?
                 termValues['label'] : terms.term;
                let elemId = termName;
                if(typeof termValues !== 'undefined'){
                elemId = (typeof termValues['tid'] !== 'undefined') ?
                  termValues['tid'] : termName;
                helpText = (typeof termValues['help'] !== 'undefined') ?
                    termValues['help'] : '';
                termWeight =  (typeof termValues['position'] !== 'undefined') ?
                termValues['position'] : '';
                termId = terms.term;
              }
              if (facetName == 'categories'){
                 elemId = 'type-'+ termName;
              }
              let parentSelector = '';
              if(elemId == 'na'){
                parentSelector = '#container-na-' + facetName + ' ';
              }
              let filterElmID = parentSelector + '.' + elemId.replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, '-');

              if($(filterElmID).hasClass('greyed-out-on')){
                $(filterElmID).removeClass('greyed-out-on').addClass('greyed-out-off').removeAttr("disabled");
              }
            });

            }else {

            let facetCount = 0;
            _.each(facet.ranges, function (facetRange) {
              let facetLabel = "$" + facetRange.from + ".00 - $" + facetRange.to + ".00";
              if (facetRange.to == '1000000'){
                facetLabel = "$" + facetRange.from + "+";
              }
              let count = facetRange.count
              let setRangeClass = facetRange.from + "-" + facetRange.to;
              if (count !== 0) {
               if($("."+setRangeClass).hasClass('greyed-out-on')){
                  $("."+setRangeClass).removeClass('greyed-out-on').addClass('greyed-out-off').removeAttr("disabled");
                }
              }
            });
          }

          });
        var sectionFilters = ["&or_collections","&and_categories","&or_facetfinish","&StyleIsTransitional","&StyleIsTraditional","&StyleIsContemporary","&and_facetFeature","&range_ListPrice","&and_facetInnovation","&and_facetHoleFilter","&ValveType","&DrainType","&and_FlowRate"];
        str = filterFacets;
        if (!(new RegExp(sectionFilters.join("|")).test(str)) && setDefault) {
          $(".facets-checkbox").each(function () {
            if($(this).attr('data-filter') == 'facetFinish') {
              $(this).removeClass('greyed-out-on').addClass('greyed-out-off').removeAttr("disabled");
            }
          });
        }
      }

      // Method to sort json object using keys.
      function sortByKey(array, key) {
        return array.sort(function(a, b) {
          var x = a[key]; var y = b[key];
          return ((x < y) ? -1 : ((x > y) ? 1 : 0));
        });
      }
      /* Re-building the facets block after API call. */
      function getFacetBlock(initData){
        let facetBlock = [];
        let count = 0;
        _.each(initData.facets, function (facet) {
          let facetName = facet.name;
          let facetType = facet.type;
          let output = '';
          let facetCount = 0;
          let helpText = '';
          let termWeight = '';
          let termId = '';
          let icon_img = '';

          if (facetType == 'term') {
            facetTerms = facet.terms;
            if (facetName == 'facetStyle'){
              facetTerms = facetData['facetstyle']['terms'];
            }
            if (facetName == 'facetNarrowResults'){
              facetTerms = facetData['facetnarrowresults']['terms'];
            }
            if(facetName == "collections") {
              facetTerms = sortByKey(facetTerms, 'term');
            }

            _.each(facetTerms, function (terms) {
              if(terms.term == 'FltElectronicFaucet'){
                return;
              }
              let termValues = facetData[terms.term];
              if((typeof termValues == 'object')) {
                if (termValues.length == 0) {
                  termValues = void 0;
                }
              }
              let termName = (typeof termValues !== 'undefined') ?
                 termValues['label'] : terms.term ;
              let elemId = termName;
              termId = terms.term;
              if(typeof termValues !== 'undefined'){
                elemId = (typeof termValues['tid'] !== 'undefined') ?
                  termValues['tid'] : termName;
                helpText = (typeof termValues['help'] !== 'undefined') ?
                    termValues['help'] : '';
                termWeight =  (typeof termValues['position'] !== 'undefined') ?
                termValues['position'] : '';
                termId = terms.term;
                if ( typeof termValues['icon'] !== 'undefined') {
                 if ( termValues['icon'] !== '') {
                   icon_img = "<div class ='icon-image filter-icon-image'><img src='"+ termValues['icon'] +"'></div>"
                 }
                 else {
                   icon_img = '';
                 }
               }
               else {
                 icon_img = '';
               }
              }
              if (facetName == 'facetStyle'){
                termName = terms.label;
                termId = terms.name;
                elemId = termName;
                termWeight = terms.position;
                icon_img = '';
              }
              if (facetName == 'facetNarrowResults'){
                termName = terms.label;
                termId = terms.name;
                elemId = termName;
                termWeight = terms.position;
                icon_img = '';
              }
              if (facetName == 'categories'){
                elemId = 'type-'+ termName;
              }
              let termMachineName = 'facet_' + facet.name;
              let elemIdTrim = elemId.replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, '-');
              if(elemId == 'na' || elemId == 'NA'){
                elemId = elemId + '-' + facetName;
              }
              output += '<div class="custom-input custom-input--checkbox"  id="container-' + elemId + '" data-position="' + termWeight + '">\n' +
               '  <input class="custom-input__native facets-checkbox '+ elemIdTrim +'" data-filter="' + facetName + '" type="checkbox"' +
               ' id="'+ elemId +'" value="' + termId + '">\n' + icon_img +
               '  <label title="'+ helpText +'" class="custom-input__label  " for="' + elemId + '">\n' +
                termName +
               '  </label>\n' +
               '</div>';
              facetCount++;
            });
          }
          else {

            let facetCount = 0;
            _.each(facet.ranges, function (facetRange) {
              let facetLabel = "$" + facetRange.from + ".00 - $" + facetRange.to + ".00";
              if (facetRange.to == '1000000'){
                facetLabel = "$" + facetRange.from + "+";
              }
              let count = facetRange.count;
              let facetValue = facetRange.from + ".00-" + facetRange.to + ".00";
              let setRangeClass = facetRange.from + "-" + facetRange.to;
              let facetName = facet.name;
              termValues = facetData[setRangeClass];
              termWeight =  (typeof termValues['position'] !== 'undefined') ?
                termValues['position'] : '';

              if (count !== 0) {
                let elemIdTrim = facetValue.replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, '-');

                output += '<div class="custom-input custom-input--checkbox" id="container-' + facetValue + '" data-position="' + termWeight + '">\n' +
                  '  <input class="custom-input__native facets-checkbox '+ elemIdTrim + ' ' +setRangeClass +'" data-filter="' + facetName + '" type="checkbox" id="' + facetValue + '" value="' + facetValue + '">\n' +
                  '  <label class="custom-input__label" for="' + facetValue + '">\n' +
                  facetLabel +
                  '  </label>\n' +
                  '</div>';
              }
            });
          }
          facetBlock[facetName.toLowerCase()] = output;
        });
        return facetBlock;
      }
      /* Detects navigation back from any page. */
      function pageShown(){
        if (window.performance &&
        window.performance.navigation.type == 2) {
          window.history.replaceState(null, null, "?searchFilter=true");
        }
      }

      /* View more in facets block. */
      function viewMore(){
        jQuery(".product-filter-more").click(function() {
          jQuery(this).prev(".filter-container").addClass("show-filter-container");
          jQuery(this).hide();
          jQuery(this).next(".product-filter-less").show();
        });

        /* View less in facets block. */
        jQuery(".product-filter-less").click(function() {
          jQuery(this).prev().prev(".filter-container").removeClass("show-filter-container");
          jQuery(this).hide();
          jQuery(this).prev(".product-filter-more").show();
        });
      }
      /* Share link. */
      jQuery(".plp-share-link").click(function() {
        if (filtersforShare.length){
          filtersforShare = filtersforShare.replace(/&&/g,'&');
          document.getElementById("plpshareinput").value = pathwithoutQuery + '?' + decodeURIComponent(filtersforShare);
        }
        else{
          document.getElementById("plpshareinput").value = pathwithoutQuery;
        }
        jQuery(".plp-share-popup").addClass("active-plp-share-popup");
      });

      /* Building the query parameters for API call when filters applied. */
      function buildFilterParams(dataFilter, dataId, dataValue, filtersFromUrl) {
        switch (dataFilter) {
          case 'collections':
            filterValue  = '&or_' + dataFilter + '=' + decodeURIComponent(dataValue) + "&";
            break;
          case 'facetFinish':
            var finishString =  dataValue;
            filterValue  = '&or_' + dataFilter + '=' + finishString.toLowerCase() + "&";
            break;
          case 'categories':
            if(filtersFromUrl == 'T'){
              dataValue = decodeURIComponent(dataValue);
            }
            else{
              dataValue = encodeURIComponent(dataValue);
            }
            if(dataValue.indexOf("ampersand") != -1){
              dataValue = encodeURIComponent(dataValue.replace(/ampersand/gi, " & "));
            }
            filterValue  = '&and_' + dataFilter + '=' + dataValue + "&";
            break;
          case 'or_MaterialStatus=30&or_MaterialStatus=40':
            filterValue  = '&or_MaterialStatus=30&or_MaterialStatus=40&or_MaterialStatus=50&or_MaterialStatus=60&or_MaterialStatus=70'+ "&";
            break;
          case 'facetStyle':
            filterValue  = '&'+ dataValue +'=T&';
            break;
          case 'StyleIsContemporary':
          case 'StyleIsTraditional':
          case 'StyleIsTransitional':
            filterValue  = '&'+ dataFilter +'=T&';
            break;
          case 'facetFeature':
          case 'facetInnovation':
          case 'facetHoleFilter':
            filterValue  = '&and_' + dataFilter + '=' + dataValue + "&";
            break;
          case 'ListPrice200DollarRangeFacet':
            filterValue  = '&range_ListPrice=' + dataValue + "&";
            break;
          case 'range_ListPrice':
            filterValue  = '&range_ListPrice=' + dataValue + "&";
            break;
          case 'ValveType':
            filterValue  = '&' + dataFilter + '=' + dataValue.replace(/ /g, '+') + "&";
            break;
          case 'FlowRate':
            filterValue  = '&and_' + dataFilter + '=' + dataValue.replace(/ /g, '+') + "&";
            break;
          case 'DrainType':
            filterValue  = '&' + dataFilter + '=' + dataValue.split(" ").join("+") + "&";
            break;
          case 'facetNarrowResults':
            if(dataId == 'Show Discontinued Products'){
              filterValue  = '&or_MaterialStatus=50&or_MaterialStatus=60&or_MaterialStatus=70&';
            }
            else if(dataId == 'Hide Retail exclusive'){
              filterValue  = '&availableToTrade=true&';
            }
            break;
          default:
            filterValue  = '&' + dataFilter + '=' + dataId + "&";
        }
        return filterValue;
     }
      $(".pagination-bottom").once().on("click", "input[type='button']", function (){
        $('html, body').animate({
          'scrollTop' : $("#output").position().top
        });
      });
      // On Scroll to achieve lazyload functionality.
      function applyproductPagination(params) {
        if(appliedSort == ''){
          appliedSort = defaultSort;
        }
        let applyFacetsFilter = productFacets;
        if (setDefault && defaultProduct) {
          applyFacetsFilter = productFacets + productFilter;
        }
        loader.show();
        loadProducts = false;
        let page;
        if (params['action']) {
          if (params['action'] == 'prev') {
            if (params['pageNum'] == 2) {
              page = 0;
            }
            else {
              page = params['pageNum'] - 2;
            }
          }
          else {
            page = params['pageNum'];
          }
        }
        else {
          page = params['pageNum'] - 1;
        }
        $.ajax({
          type: "GET",
          url: "/product-listing",
          dataType: 'json',
          data: {
            size: size,
            category: category,
            secondaryCategory: secondaryCategory,
            productFacets: applyFacetsFilter,
            categoryCount: categoryCount,
            facets: filterFacets,
            cacheKeyData: cacheKeyData,
            sort: appliedSort,
            pagenumber: page,
            cache: 'F',
          },
          error: function (XMLHttpRequest, textStatus, errorThrown) {
            loader.hide();
            $('.plp-results').show();
            $('.product-count').html('0');
            var noResultsMessage = '<div class ="plp-api-error error-message" role="contentinfo" aria-label="Error message">Something has gone wrong on our end, please try refreshing this&nbsp;<a href="'+window.location.href+'">page</a>. If the issue persists, please try again later</div>';
            $('#output').html(noResultsMessage);
            $('.plp-pagination-output').html('');
            return false;
          },
          success: function (data) {
            $('.product-filter').show();
            let response = new ProdView(data, true);
            if(params['pageNum'] && data) {
              data.currentpage = params['pageNum'];
              data.first = params['first'];
              data.last = params['last'];
            }
            if(params['action'] && data) {
              data.action = params['action'];
            }
            let PlppaginationView = new plppaginationView(data);
            PlppaginationView.undelegateEvents();
            loadProducts = true;
            lastPage = data.last;
            pageNumber++;
            let activePage = data.number +1;
            let from = ((activePage-1)*size)+1;
            let to = activePage*size;
            if (to > data.totalElements) {
              to = data.totalElements;
            }
            let resultString = "Results <strong>"+from+"-"+to+"</strong> of <strong>"+data.totalElements + "</strong>";
            $('.product-count').html(resultString);
            $('.category-name').html(category);
            loader.hide();
            if ($.trim($('.product-filter__body .region-sidebar').text()) == ''){
              $('.product-filter').hide();
            }
            else{
              $('.product-filter').show();
            }
          }
        });
      }
      /* pagination starts here*/
      var plppaginationView = Backbone.View.extend({
        el: ".plp-pagination-output",
        template: _.template($("#plp-pagination").html()),
        initialize: function (initData) {
          this.render(initData);
        },
        events: {
          'click [type="button"]': 'plppagination'
        },
        plppagination: function (e) {
          var params = [];
          params['pageNum'] = $(e.currentTarget).val();
          params['first'] = $(e.currentTarget).attr('first');
          params['last'] = $(e.currentTarget).attr('last');
          if($(e.currentTarget).val() === '>') {
            params['pageNum'] = $('.pager__link.is-active').val();
            params['action'] = 'next';
          }
          if($(e.currentTarget).val() === '<') {
            params['pageNum'] = $('.pager__link.is-active').val();
            params['action'] = 'prev';
          }
          if($(e.currentTarget).attr('data') === 'last-page') {
            var total = $(e.currentTarget).attr('total');
            params['pageNum'] = parseInt(total);
            params['first'] = parseInt(total)-2;
            params['last'] = parseInt(total);
          }
          if($(e.currentTarget).attr('data') === 'first-page') {
            params['pageNum'] = 1;
            params['first'] = 1;
            params['last'] = 3;
          }
          applyproductPagination(params);        },
        render: function (initData) {
          if (initData === null) {
            return false;
          }
          var cont_disp_cnt = size;//drupalSettings.cont_disp_cnt;
          if (cont_disp_cnt == null) {
            cont_disp_cnt = productPerPage;
          }
          var _self = this;
          var _render = "<div class='page-desktop-pagination'>";
          var _mobilerender = "<div class='page-mobile-pagination'>";
          var _nextPage = (initData === null) ? 0 : parseInt(initData.number) + 1;
          var _currentpage = initData.currentpage;  // Current page
          var _totalmatches = initData.totalElements;
          var _totalpages = Math.ceil(_totalmatches / cont_disp_cnt);
          var _nextPage;
          var _first;
          if (_currentpage < _totalpages) {
            _nextPage = _currentpage + 1;
          } else {
            _nextPage = _currentpage;
          }
          var _previousPage;
          if (_currentpage === 1) {
            _previousPage = 1;
            _first = 1;
          } else {
            _previousPage = _currentpage - 1;
            _first = initData.first;
          }
          if(_currentpage>=initData.first && _currentpage<=initData.last) {
            _first = parseInt(initData.first);
          }
          if(initData.action && initData.action == 'next') {
            if(_currentpage>=initData.first && _currentpage<initData.last) {
              _first = parseInt(initData.first);
            } else {
              _first = parseInt(initData.first) + 1;
            }
            _currentpage = parseInt(_currentpage) + 1;
          }
          if(initData.action && initData.action == 'prev') {
            if(_currentpage>initData.first && _currentpage<=initData.last) {
              _first = parseInt(initData.first);
            } else {
              _first = parseInt(initData.first) - 1;
            }
            _currentpage = parseInt(_currentpage) - 1;
          }
          var pagination = parseInt(_first)+2;
          if(_totalpages < pagination) {
            pagination = _totalpages;
          }
          if(_currentpage != 1) {
            //_render += '<input total="'+_totalpages+'" class="pager__link first" type="button" data="" value="<<">';
            _render += '<input first="'+_first+'" last="'+pagination+'" class="pager__link prev" type="button" data="" value="<">';
            _mobilerender += '<input first="'+_first+'" last="'+pagination+'" class="pager__link prev" type="button" data="" value="<">';
          }
          if((_first+2 >= _totalpages) && (_totalpages > 3)) {
            _render += '<input total="'+_totalpages+'" class="pager__link first" type="button" data="first-page" value=1><span class="pagination-dots"> ... </span>';
          }
          for(var i=_first;i<=pagination;i++) {
            /*_render += _self.template({
              pageNumber: i,
              currentPage: _currentpage,
              totalRecordset: initData.totalElements
            });*/
            var classValue = '';
            if(i === parseInt(_currentpage)) {
              classValue = 'is-active';
              _mobilerender += '<p class="mobile_pagination"><span>Page </span><strong>' + i + '</strong></p>';
            }

            _render += '<input first="'+_first+'" last="'+pagination+'" class="pager__link '+ classValue +'" type="button" value="'+ i +'">';

          }
          if(!_first && _totalpages == 1) {
            _mobilerender += '<p class="mobile_pagination"><span>Page </span><strong>' + _totalpages + '</strong></p>';

          }
          _mobilerender += '<p class="mobile_pagination"><span>of </span><strong>' + _totalpages +'</strong></p>';
          if(_first+2 < _totalpages) {
            _render += '<span class="pagination-dots"> ... </span><input total="'+_totalpages+'" class="pager__link last" type="button" data="last-page" value='+_totalpages+'>';
          }
          if(_currentpage != _totalpages) {
            _render += '<input first="'+_first+'" last="'+pagination+'" class="pager__link next" type="button" data="" value=">">';
            _mobilerender += '<input first="'+_first+'" last="'+pagination+'" class="pager__link next" type="button" data="" value=">">';
            //_render += '<input total="'+_totalpages+'" class="pager__link last" type="button" data="" value=">>">';
          }
          var start_value = (_currentpage-1)*cont_disp_cnt;
          if(_currentpage ==1) {
            start_value = 1;
          }
          var endvalue = _currentpage*cont_disp_cnt;
          if(start_value == 1 && endvalue > initData.totalElements){
            // no pager
          }
          else if(endvalue > initData.totalElements){
            endvalue = initData.totalElements;
            $('.srp-result-doc').html('<span class="srp-qty">Results ' + start_value + '-' + endvalue  + ' of ' + initData.totalElements +'</span>').attr('data-content-cnt', initData.totalElements);
          }
          else{
            $('.srp-result-doc').html('<span class="srp-qty">Results ' + start_value + '-' + endvalue  + ' of ' + initData.totalElements +'</span>').attr('data-content-cnt', initData.totalElements);
          }
          _render += "</div>"
          _mobilerender += "</div>";
          _render = _render + _mobilerender;
          _self.$el.html(_render);
        }
      });
      /*End of pagination*/

    }
  };
})
(jQuery, Drupal, drupalSettings, Backbone);

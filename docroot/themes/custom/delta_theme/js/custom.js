(function ($, Drupal, drupalSettings) {
    Drupal.behaviors.deltaTheme = {
      attach: function (context, settings) {
        $(".product-registration-form .webform-multiple-table .required").on("change input keyup", function(e) {
          var element = $(this);
          var inputValue = element.val(),
              label = element.siblings('label').text(),
              model_number = Drupal.behaviors.commonvalidationfunction.modelnumber();

          if(element.hasClass("validate-modelnumber")) {
            $('.product-registration-form').find(".validate-modelnumber").removeClass("form-item--error");
            var inputValue = element.val();
            if( inputValue == "" ) {
              element.next('p.form-item--error-message').remove();
              element.parent().parent().next('p.form-item--error-message').remove();
              element.parent().parent('.ac-model-number').after("<p class='form-item--error-message'> "+ label + " field is required.</p>");
              //element.after("<p class='form-error'> "+ label + " field is required.</p>");
              element.parent().addClass("form-item--error");
            } else if(!inputValue.match(model_number)) {
              element.parent().parent().next('p.form-item--error-message').remove();
              element.parent().parent('.ac-model-number').after("<p class='form-item--error-message'>Please enter a valid " + label + ".</p>");
              element.parent().parent().addClass("form-item--error");
            }else {
              element.parent().parent().next('p.form-item--error-message').remove();
              element.parent().parent().removeClass("form-item--error");
            }
          };

          if((inputValue == "") || (inputValue == 0)) {
            if(element.next().find('p.form-item--error-message')) {
              element.next('p.form-item--error-message').remove();
              element.parent().parent().next('p.form-item--error-message').remove();
              element.parent().addClass("form-item--error");
              element.after("<p class='form-item--error-message'>" + label + " field is required.</p>");
            }
          } else {
            element.next('p.form-item--error-message').remove();
            element.parent().removeClass("form-item--error");
          }

          $('.auto-suggest .model-num-list').on('click','.row-sku',function() {
            if(element.parent().parent().next().find('p.form-item--error-message')) {
              element.parent().parent().next('p.form-item--error-message').remove();
              element.parent().parent().removeClass("form-item--error");
            }
          });
        });


        let wishlistCartIcons = "";

        $('.promotion-banner').once('dataLayer').bind('inview', function (e, v, t) {
           var o = $(this);
           if (o.data('inviewtimer')) {
             clearTimeout(o.data('inviewtimer'));
             o.removeData('inviewtimer');
           }
           if (v) {
             o.data('inviewtimer', setTimeout(function () {
               if (t == 'top') {
                 o.data('seenTop', true);
               } else if (t == 'bottom') {
                 o.data('seenBottom', true);
               } else {
                 o.data('seenTop', true);
                 o.data('seenBottom', true);
               }
               if (o.data('seenTop') && o.data('seenBottom')) {
                 o.unbind('inview');
                 // here we will do WHAT WHE NEED (for ex. Call Ajax stats collector)
                 // ...
                 $this = o;
                 if (!o.hasClass('impression-added')) {
                   o.addClass('impression-added');
                   if(o.find(".featured-banner").hasClass("featured-banner")) {
                     var type = o.find(".featured-banner").find(".featured-banner__title p").text();
                     if(type == "") {
                       type = o.find(".featured-banner").find("h4.h4").text();
                     }
                     dataLayer.push({
                       'event': 'promotionImpression',
                       'ecommerce': {
                         'promoView': {
                           'promotions': [{
                             'name': 'home page banner',
                             'creative': type,
                           }]
                         }
                       }
                     });
                   }
                   else if(o.find(".home-intro-banner").find(".hib-logo").hasClass("voiceiq_banner_img")) {
                     dataLayer.push({
                       'event': 'promotionImpression',
                       'ecommerce': {
                         'promoView': {
                           'promotions': [{
                             'name': 'home page banner',
                             'creative': "Voice IQ",
                           }]
                         }
                       }
                     });
                   }
                   else if(o.find(".hotspot-banner").find(".hotspot-banner__content").hasClass("hotspot-banner__content")) {
                     dataLayer.push({
                       'event': 'promotionImpression',
                       'ecommerce': {
                         'promoView': {
                           'promotions': [{
                             'name': 'home page banner',
                             'creative': "Stryke Bathroom Collection",
                           }]
                         }
                       }
                     });
                   }
                   else if(o.find(".homepage_video").find(".video-title-container").hasClass("video-title-container")) {
                     var type = o.find(".homepage_video").find(".video-title-container h2").text();
                     dataLayer.push({
                       'event': 'promotionImpression',
                       'ecommerce': {
                         'promoView': {
                           'promotions': [{
                             'name': 'home page banner',
                             'creative': type,
                           }]
                         }
                       }
                     });
                   }
                   else {
                     var type = o.find(".featured-banner").find(".featured-banner__title p").text();
                     if(type == "") {
                       type = o.find(".featured-banner").find("h4.h4").text();
                     }
                     if(type != "") {
                       dataLayer.push({
                         'event': 'promotionImpression',
                         'ecommerce': {
                           'promoView': {
                             'promotions': [{
                               'name': 'home page banner',
                               'creative': type,
                             }]
                           }
                         }
                       });
                     }
                   }
                 }
               }
             }, 50));
           }
        });

        // Language switch block
        $("a.lng-item").on("click", function(e){
          e.preventDefault();
          var label = $(this).text();
          var removespace = $.trim(label.toLowerCase());
          var language = removespace.replace(" ", "");
          language = language.replace(" ", "");
          dataLayer.push({
            'event': 'navigationClick',
            'category':'utility navigation clicks',
            'action':'clicked utility navigation link',
            'label': language,
          }); //  usa-eng,usa-esp,canada-eng,canada-fr,worldwide-eng,brasil(pt),china,india-eng
          var href = $(this).attr('href');
          window.open(href, '_blank');
        });

        // GTM Event for Footer Menus
        $(".footer__wrapper a").on("click", function(e){
          e.preventDefault();
          var type = $(this).text();
          dataLayer.push({
            'event': 'customEvent',
            'category': 'footer navigation clicks',
            'action': 'clicked a footer link',
            'label': $.trim(type),
          });
          var href = event.currentTarget.getAttribute('href');
          window.location.href = href;
        });

        $(".header a.anchor-link").on("click", function(e){
          e.preventDefault();
          var type = $(this).text();
          dataLayer.push({
            'event': 'navigationClick',
            'category': 'main menu navigation clicks',
            'action': 'clicked main menu navigation link',
            'label': $.trim(type),
          });
          var href = event.currentTarget.getAttribute('href');
          window.location.href = href;
        });

        $(".header a.nav-prod-img img.img").on("click", function(e){
          e.preventDefault();
          var type = $(this).attr("alt");
          dataLayer.push({
            'event': 'navigationClick',
            'category': 'main menu navigation clicks',
            'action': 'clicked main menu navigation link',
            'label': "image-"+$.trim(type),
          });
          var href = $(this).parent().attr('href');
          window.location.href = href;
        });

        $(".header .product-collections a.inline-text-link").on("click", function(e){
          e.preventDefault();
          var type = $(this).text();
          dataLayer.push({
            'event': 'navigationClick',
            'category': 'main menu navigation clicks',
            'action': 'clicked main menu navigation link',
            'label': $.trim(type.toLowerCase()),
          });
          var href = $(this).attr('href');
          window.location.href = href;
        });

        $(".header .delta-outlet a.inline-text-link").on("click", function(e){
          e.preventDefault();
          var type = $(this).text();
          dataLayer.push({
            'event': 'navigationClick',
            'category': 'main menu navigation clicks',
            'action': 'clicked main menu navigation link',
            'label': $.trim(type.toLowerCase()),
          });
          var href = $(this).attr('href');
          window.location.href = href;
        });

        $(".header .training a.inline-text-link").on("click", function(e){
          e.preventDefault();
          var type = $(this).text();
          dataLayer.push({
            'event': 'navigationClick',
            'category': 'main menu navigation clicks',
            'action': 'clicked main menu navigation link',
            'label': $.trim(type),
          });
          var href = $(this).attr('href');
          window.location.href = href;
        });


        $(".product-innovations a.inline-text-link").on("click", function(e){
          e.preventDefault();
          var type = $(this).text();
          dataLayer.push({
            'event': 'navigationClick',
            'category': 'main menu navigation clicks',
            'action': 'clicked main menu navigation link',
            'label': $.trim(type),
          });
          var href = $(this).attr('href');
          window.location.href = href;
        });

        $(".service--parts a.inline-text-link").on("click", function(e) {
          e.preventDefault();
          var type = $(this).text();
          dataLayer.push({
            'event': 'navigationClick',
            'category': 'main menu navigation clicks',
            'action': 'clicked main menu navigation link',
            'label': $.trim(type),
          });
          var href = $(this).attr('href');
          window.location.href = href;
        });

        $(".service--parts .support-resources a").on("click", function(e) {
          e.preventDefault();
          var type = $(this).text();
          dataLayer.push({
            'event': 'navigationClick',
            'category': 'main menu navigation clicks',
            'action': 'clicked main menu navigation link',
            'label': $.trim(type),
          });
          var href = $(this).attr('href');
          window.location.href = href;
        });

        $(".inspired-living-blog a.inline-text-link").on("click", function(e){
          e.preventDefault();
          var type = $(this).text();
          dataLayer.push({
            'event': 'navigationClick',
            'category': 'main menu navigation clicks',
            'action': 'clicked main menu navigation link',
            'label': $.trim(type),
          });
          var href = $(this).attr('href');
          window.location.href = href;
        });

        $(".design-experiences a.inline-text-link").on("click", function(e){
          e.preventDefault();
          var type = $(this).text();
          dataLayer.push({
            'event': 'navigationClick',
            'category': 'main menu navigation clicks',
            'action': 'clicked main menu navigation link',
            'label': $.trim(type),
          });
          var href = $(this).attr('href');
          window.location.href = href;
        });

        // GTM For Utility

        $(".utility-nav a.utility-menu-nav__link").on("click", function(e){
          e.preventDefault();
          var type = $(this).text();
          dataLayer.push({
            'event': 'navigationClick',
            'category': 'utility navigation clicks',
            'action': 'clicked utility navigation link',
            'label': $.trim(type),
          });
          var href = event.currentTarget.getAttribute('href');
          window.location.href = href;
        });

        // Promotion Banner GTM IN Home page
        $(".hotspot-banner .hotspot-banner__cta a.arrow-link").on("click", function(e){
          e.preventDefault();
          var type = $(this).text();
          dataLayer.push({
            'event': 'promotionClick',
            'ecommerce': {
              'promoClick': {
                'promotions': [{
                  'name': 'home page banner',
                  'creative': 'Stryke Bathroom Collection',
                  'label': "Stryke Bathroom Collection"
                }]
               }
              }
            });
          var href = event.currentTarget.getAttribute('href');
          window.location.href = href;
        });

        $(".home-page a.button").on("click", function(e){
          e.preventDefault();
          var type = $(this).parent().parent().find(".featured-banner__title").text();
          dataLayer.push({
            'event': 'promotionClick',
            'ecommerce': {
              'promoClick': {
                'promotions': [{
                  'name': 'home page banner',
                  'creative': type,
                  'label': type
                }]
               }
              }
            });
          var href = event.currentTarget.getAttribute('href');
          window.location.href = href;
        });

        $(".wishlist-icon").on("click",function(e){
          e.preventDefault();
          dataLayer.push({
            'event': 'navigationClick',
            'category': 'main menu navigation clicks',
            'action': 'clicked main menu navigation link',
            'label': "Wishlist",
          });
          var href = event.currentTarget.getAttribute('href');
          window.location.href = href;
        });

        $(".hide-account").on("click",function(e){
          e.preventDefault();
          dataLayer.push({
            'event': 'navigationClick',
            'category': 'main menu navigation clicks',
            'action': 'clicked main menu navigation link',
            'label': "Account",
          });
          var href = event.currentTarget.getAttribute('href');
          window.location.href = href;
        });

        $(".logo__link").on("click",function(e){
          e.preventDefault();
          dataLayer.push({
            'event': 'navigationClick',
            'category': 'main menu navigation clicks',
            'action': 'clicked main menu navigation link',
            'label': "Logo",
          });
          var href = event.currentTarget.getAttribute('href');
          window.location.href = href;
        });

        // Search suggestion Popup
        $(".search-bar-wrapper .clear-button").css("display","none");

        $(".header__search-trigger").on("click", function(){
          // Search icon GTM Events
          dataLayer.push({
            'event': 'navigationClick',
            'category': 'main menu navigation clicks',
            'action': 'clicked main menu navigation link',
            'label': "Search",
          });



          $(".site-search__overlay").toggle();
          $(".site-search__form").toggle();
          $("#nav--search--svg").toggle();
          $("#icon--close--svg").toggle();
          $(".nav--search-mobile").css("display","none");
          $(".icon--close-mobile").css("display","block");
          //$(".search-svg-close").css("display","block");
          $(".site-search__form .ajax-progress-throbber").css("display","none");
        });

        $(".search-product-input").on("keyup", function(){
          $(".header__search-trigger").trigger("click");
          var searchText = $(".search-product-input").val();
          $(".site-search__field").val(searchText);
          $(".site-search__field").focus();
        });

        $(".site-search__overlay").on("click", function(){
          $(".site-search__overlay").hide();
          $(".site-search__form").hide();
          $("#nav--search--svg").css("display","block");
          $(".search-svg-close").css("display","none");
          $(".nav--search-mobile").css("display","block");
          $(".icon--close-mobile").css("display","none");
          $(".site-search__suggestions-container").html("");
          $(".site-search__field").val("");
        });

        $(".search-clear__button").on("click", function(e){
          e.preventDefault();
          $(".site-search__field").val("");
          $(".site-search__suggestions-container").html("");
          $(".site-search__form").removeClass('has-content');
          $(".site-search__overlay").hide();
          $(".site-search__form").hide();
          $("#nav--search--svg").css("display","block");
          $(".search-svg-close").css("display","none");
          $(".nav--search-mobile").css("display","block");
          $(".icon--close-mobile").css("display","none");
        });

        $(".search-bar-wrapper .clear-button").on("click", function(e){
          e.preventDefault();
          $(".site-search__field").val("");
          $(this).css("display","none");
          $(".site-search__suggestions-container").html("");
        })

        // Search Page redirect while enter KEY press
        $(".site-search__field").keypress(function(event){
          var keycode = (event.keyCode ? event.keyCode : event.which);
          if(keycode == '13'){
            event.preventDefault();
            var searchTxt = $(this).val();
            window.location.href = settings.searchPageUrl+"?q="+searchTxt;
          }
        });

        $(".site-search__field").on("keyup",function(){
          var searchTxt = $(this).val();
          if(searchTxt.length != 0) {
            $(".search-bar-wrapper .clear-button").css("display","block");
          }
          else {
            $(".search-bar-wrapper .clear-button").css("display","none");
          }
          if(searchTxt.length > 2) {
            $(".site-search__form .ajax-progress-throbber").css("display","flex");
            $.ajax({
              type: "POST",
              url: "/sfcc-search-suggestion",
              dataType: 'json',
              data: {
                  searchtxt: searchTxt,
              },
              complete: function () {
                $(".site-search__form .ajax-progress-throbber").css("display","none");
              },
              success: function (response) {
                var productData = '<div class="site-search__suggestions">';
                if(response['suggestions'] != undefined && response['suggestions'] != "") {
                  productData += '<div class="site-search__suggestions-popular">';
                  $.each(response['suggestions'], function(sugkey, sugval) {
                    if(sugval['url'] != "didyoumean" && sugval['url'] != "") {
                      productData += '<a class="search-popular-text" href='+sugval['url']+'>'+sugval['value']+'</a>';
                    }
                    else if(sugval['url'] == 'didyoumean'){
                      productData += '<p class="search-popular-text-label">'+sugval['value']+'</p>';
                    }
                    else {
                      productData += '<p class="search-popular-text-label"><b>'+sugval['value']+'</b></p>';
                    }
                  });
                  productData += '</div>';
                  $(".site-search__form").addClass('has-content');
                }

                if(response['retail'] != undefined && response['retail'] != "") {
                  productData += '<hr> <div class="site-search__suggestions-section-products"><h2 class="site-search__suggestions-title">Products</h2><div class="site-search__suggestions-matches row">';
                  $.each(response['retail'], function(key, val) {
                    productData += '<div class="site-search__suggestions-item grid-col-12 grid-col-md-12 grid-col-lg-4"><div class="product-tile prdId-'+val['sku']+'" data-prdid="'+val['sku']+'">';
                    productData += '<div class="slider_flag flag_position banner-text">';
                    if(val['badge'] != "" && val['badge'] != undefined) {
                      productData += '<div class="slider_flag flag_position"><div class="flag flag--gray-30 banner-badge" data-badge="'+val['badge']+'">'+val['badge']+'</div></div>';
                    }
                    productData += '</div>';
                    productData += '<div class="right-align-content"><img class="search-wishlist" src="/themes/custom/delta_theme/images/icons/src/wishlist-icon.svg"/><img class="search-wishlist-black" src="/themes/custom/delta_theme/images/icons/src/wishlist-black.svg"/></div>';
                    productData += '<a class="product-tile__anchor no-line product-action-link" data-orginalUrl="'+val['url']+'" href="'+val['url']+'" data-product-url="productShow" itemprop="url">';
                    productData += '<figure class="product-tile__image"><img class="search-image-broken" data-src="'+val['imageUrl']+'" src="'+val['imageUrl']+'"></figure>';
                    productData += '<div class="product-tile__body">';
                    productData += '<h6 class="h6 product-tile__title">'+val['collection']+'</h6>';
                    productData += '<p class="paragraph">'+val['name']+'</p>';
                    if(val['listprice'] != "" && val['listprice'] != undefined ) {
                      productData += '<span class="product-tile__price"><span>List Price*: </span><span class="product-tile__price">'+val['listprice']+'</span>';
                      productData += '<div><span class="product-tile__price online-price">Online Price:</span><span class="product-tile__price online-price">'+val['saleprice']+'</span></div>';
                    }
                    else {
                      productData += '<span class="product-tile__price">List Price*:</span><span class="product-tile_price_val list-price-color" >'+val['saleprice']+'</span>';
                    }
                    if(val['promotionMsg'] != "") {
                      productData += '<span class="product-tile__promotion">'+val['promotionMsg']+'</span>';
                    }
                    if(val['variations'] != "") {
                      productData += '<div class="prd-variations">';
                      $.each(val['variations'],function(prdkey,prdval){
                        var badgeLabel= "";
                        var preSelectedBadge = "";
                        if(prdval['badge'] != "" && prdval['badge'] != undefined) {
                          badgeLabel = prdval['badge']['badgeLabel'];
                        }
                        if(prdval['selected']) {
                          preSelectedBadge = "selected-swatch";
                        }
                        productData += "<a href="+prdval['url']+" class='search-swatch "+preSelectedBadge+"' data-prdsku='"+val['sku']+"'><img src="+prdval['swatchImage']+" class='search-swatch-img "+preSelectedBadge+"' data-pointId='"+val['sku']+"' data-url="+prdval['url']+" data-src="+prdval['thumbImage']+" data-badge='"+badgeLabel+"'></a>";
                      });
                      productData += '</div>';
                    }
                    productData += '<span class="product-tile__sku">'+val['sku']+'</span>';
                    productData += '</div></a></div></div>';
                  });
                  productData += "</div><a class='all-products'  href="+response['links']['viewAllProducts']+">View All Products</a></div>";
                  $(".site-search__form").addClass('has-content');
                }

                if(response['parts'] != undefined && response['parts'] != "") {
                  productData += '<hr> <div class="site-search__suggestions-section-products"><h2 class="site-search__suggestions-title heading-type--h6 font-weight--semibold">Repair Parts</h2><div class="site-search__suggestions-matches list--reset row">';
                  $.each(response['parts'], function(rekey, reval) {
                    productData += '<div class="site-search__suggestions-item grid-col-12 grid-col-md-12 grid-col-lg-4"><div class="product-tile prdId-'+reval['sku']+'" data-prdid="'+reval['sku']+'">';
                    productData += '<div class="slider_flag flag_position banner-text">';
                    if(reval['badge'] != "" && reval['badge'] != undefined){
                      productData += '<div class="flag flag--gray-30 banner-badge" data-badge="'+reval['badge']+'">'+reval['badge']+'</div>';
                    }
                    productData += '</div>';
                    productData += '<div class="right-align-content"><img class="search-wishlist" src="/themes/custom/delta_theme/images/icons/src/wishlist-icon.svg"/><img class="search-wishlist-black" src="/themes/custom/delta_theme/images/icons/src/wishlist-black.svg"/></div>';
                    productData += '<a class="product-tile__anchor no-line product-action-link" data-orginalUrl="'+reval['url']+'" href="'+reval['url']+'" data-product-url="productShow" itemprop="url">';
                    productData += '<figure class="product-tile__image"><img class="search-image-broken" data-src="'+reval['imageUrl']+'" src="'+reval['imageUrl']+'"></figure>';
                    productData += '<div class="product-tile__body">';
                    productData += '<h6 class="h6 product-tile__title">'+reval['collection']+'</h6>';
                    productData += '<p class="paragraph">'+reval['name']+'</p>';
                    if(reval['listprice'] != "" && reval['listprice'] != undefined ) {
                      productData += '<span class="product-tile__price">List Price*: </span><span class="product-tile_price_val">'+reval['listprice']+'</span>';
                      productData += '<div><span class="product-tile__price online-price">Online Price:</span><span class="product-tile_price_val online-price">'+reval['saleprice']+'</span></div>';
                    }
                    else {
                      productData += '<span class="product-tile__price">List Price*: </span><span class="product-tile_price_val list-price-color" >'+reval['saleprice']+'</span>';
                    }
                    if(reval['promotionMsg'] != "") {
                      productData += '<span class="product-tile__promotion">'+reval['promotionMsg']+'</span>';
                    }
                    if(reval['variations'] != "") {
                      productData += '<div class="prd-variations">';
                      $.each(reval['variations'],function(prdkey,prdval){
                        var badgeLabel= "";
                        var preSelectedBadge = "";
                        if(prdval['badge'] != "" && prdval['badge'] != undefined) {
                          badgeLabel = prdval['badge']['badgeLabel'];
                        }
                        if(prdval['selected']) {
                          preSelectedBadge = "selected-swatch";
                        }
                        productData += "<a href="+prdval['url']+" class='search-swatch "+preSelectedBadge+"' data-prdsku='"+reval['sku']+"'><img src="+prdval['swatchImage']+" class='search-swatch-img "+preSelectedBadge+"'  data-pointId='"+reval['sku']+"' data-url="+prdval['url']+" data-src="+prdval['thumbImage']+" data-badge='"+badgeLabel+"'></img></a>";
                      });
                      productData += '</div>';
                    }
                    productData += '<span class="product-tile__sku">'+reval['sku']+'</span>';
                    productData += '</div></a></div></div>';
                  });
                  productData += "</div><a class='all-products' href="+response['links']['viewAllRepairParts']+">View All Repair Parts</a></div>";
                }
                if(response['others'] != undefined && response['others'] != "") {
                  productData += '<div class="site-search__suggestions-section"><div class="site-search__suggestions-matches list--reset row">';
                  $.each(response['others'], function(orekey, oreval) {
                    productData += '<div class="site-search__suggestions-item grid-col-12 grid-col-md-12 grid-col-lg-4"><div class="product-tile prdId-'+oreval['sku']+'" data-prdid="'+oreval['sku']+'">';
                    productData += '<div class="slider_flag flag_position">';
                    if(oreval['badge'] != "" && oreval['badge'] != undefined){
                      productData += '<div class="flag flag--gray-30 banner-badge" data-badge="'+oreval['badge']+'">'+oreval['badge']+'</div>';
                    }
                    productData += '</div>';
                    productData += '<div class="right-align-content"><img class="search-wishlist" src="/themes/custom/delta_theme/images/icons/src/wishlist-icon.svg"/><img class="search-wishlist-black" src="/themes/custom/delta_theme/images/icons/src/wishlist-black.svg"/></div>';
                    productData += '<a class="product-tile__anchor no-line product-action-link" data-orginalUrl="'+oreval['url']+'" href="'+oreval['url']+'" data-product-url="productShow" itemprop="url">';
                    productData += '<figure class="product-tile__image"><img class="search-image-broken" data-src="'+oreval['imageUrl']+'" src="'+oreval['imageUrl']+'"></figure>';
                    productData += '<div class="product-tile__body">';
                    productData += '<h6 class="h6 product-tile__title">'+oreval['collection']+'</h6>';
                    productData += '<p class="paragraph">'+oreval['name']+'</p>';
                    if(oreval['listprice'] != "" && oreval['listprice'] != undefined ) {
                      productData += '<span class="product-tile__price">List Price*: </span><span class="product-tile_price_val">'+oreval['listprice']+'</span>';
                      productData += '<div><span class="product-tile__price online-price">Online Price:</span><span class="product-tile_price_val online-price">'+oreval['saleprice']+'</span></div>';
                    }
                    else {
                      productData += '<span class="product-tile__price">List Price*: </span><span class="product-tile_price_val list-price-color" >'+oreval['saleprice']+'</span>';
                    }
                    if(oreval['promotionMsg'] != "") {
                      productData += '<span class="product-tile__promotion">'+oreval['promotionMsg']+'</span>';
                    }
                    if(oreval['variations'] != "") {
                      productData += '<div class="prd-variations">';
                      $.each(oreval['variations'],function(prdkey,prdval){
                        var badgeLabel= "";
                        if(prdval['badge'] != "" && prdval['badge'] != undefined) {
                          badgeLabel = prdval['badge']['badgeLabel'];
                        }
                        productData += "<a href="+prdval['url']+" class='search-swatch "+preSelectedBadge+"'><img src="+prdval['swatchImage']+" class='search-swatch-img'  data-pointId='"+oreval['sku']+"' data-url="+prdval['url']+" data-src="+prdval['thumbImage']+" data-badge='"+badgeLabel+"'></a>";
                      });
                      productData += '</div>';
                    }
                    productData += '<span class="product-tile__sku">'+oreval['sku']+'</span>';
                    productData += '</div></a></div></div>';
                  });
                  productData += "</div><a class='all-products'  href="+response['links']['viewAllRepairParts']+">View All Repair Parts</a></div>";
                }
                productData += "</div>";
               $(".site-search__suggestions-container").html(productData);
               $(".site-search__form").addClass('has-content');
               // Fixing error images
               $(".search-image-broken").bind("error",function() {
                 // Replacing image source
                 $(this).attr("src","/themes/custom/delta_theme/images/no-image-lg.png");
                });

                // Checking favorites list.
                $.ajax({
                  type: "GET",
                  url: "/customer-product-list",
                  dataType: 'json',
                  complete: function () {
                  },
                  success: function (response) {
                    $(".site-search__suggestions-matches .site-search__suggestions-item").each(function(){
                      var sku = $(this).find(".product-tile").data("prdid");
                      $.each(response, function(key,val) {
                        var productId = val.replace("Master-", "");
                        if(sku == productId) {
                          $(".prdId-"+sku).find(".search-wishlist").css("display","none");
                          $(".prdId-"+sku).find(".search-wishlist-black").css("display","block");
                        }
                      });
                    });
                  }
                });

                $(".prd-variations .search-swatch").each(function(){
                  if($(this).hasClass("selected-swatch")) {
                    var imgUrl = $(this).find("img").data("src");
                    var sku = $(this).find("img").data("prdsku");
                    $(this).parent().parent().parent().find(".search-image-broken").attr("src",imgUrl);
                    $(this).parent().parent().parent().find(".search-image-broken").data("src",imgUrl);
                  }
                });
              }
            });
          }
        });

        $(document).on("click",".search-wishlist-black", function(){
          var skuId = $(this).parent().parent().data("prdid");
          $.ajax({
            type: "POST",
            url: "/remove-search-suggestion-wishlist",
            dataType: 'json',
            data: {
                skuid: skuId,
            },
            complete: function () {
            },
            success: function (response) {
              if(response[0] == 'success') {
                $(".prdId-"+skuId).find(".search-wishlist-black").css("display","none");
                $(".prdId-"+skuId).find(".search-wishlist").css("display","block");
              }
            }
          });
        });

        // Mouse over events on Search suggestions.
        $(document).on("mouseover",".search-swatch",function(e) {
          var sku = $(this).find(".search-swatch-img").data("pointid")
          var thumbImage = $(this).find(".search-swatch-img").data("src");
          var badge = $(this).find(".search-swatch-img").data("badge");
          var url = $(this).find(".search-swatch-img").data("url");
          $(this).parents(".prdId-"+sku).find(".search-image-broken").attr("src",thumbImage);
          var defaultBadge = "";
          var previousbadge = $(this).parents(".prdId-"+sku).find(".banner-badge").data("badge");
          if(previousbadge != "" && previousbadge != undefined) {
            defaultBadge = previousbadge;
          }
          if(badge != "" && badge != undefined)  {
            var bannerHtml = '<div class="slider_flag flag_position"><div class="flag flag--gray-30 banner-badge" data-badge="'+defaultBadge+'">'+badge+'</div></div>';
            $(this).parents(".prdId-"+sku).find(".banner-text").html(bannerHtml);
          }
          else {
            $(this).parents(".prdId-"+sku).find(".banner-text").html("");
          }
        });

        // Mouse out events on Search suggestions.
        $(document).on("mouseout",".search-swatch",function(e) {
          var sku = $(this).find(".search-swatch-img").data("pointid")
          var imgurl = $(this).parents(".prdId-"+sku).find(".search-image-broken").data("src");
          var orgUrl = $(this).parents(".prdId-"+sku).find(".product-tile__anchor").data("orginalurl");
          var badge = $(this).parents(".prdId-"+sku).find(".banner-badge").data("badge");
          $(this).parents(".prdId-"+sku).find(".search-image-broken").attr("src",imgurl);
          if(badge != "" && badge != undefined) {
            var bannerHtml = '<div class="slider_flag flag_position"><div class="flag flag--gray-30 banner-badge" data-badge="'+badge+'">'+badge+'</div></div>';
            $(this).parents(".prdId-"+sku).find(".banner-text").html(bannerHtml);
          }
          else {
            $(this).parents(".prdId-"+sku).find(".banner-text").html("");
          }
          $(this).parents(".prdId-"+sku).find(".product-tile__anchor").attr("href",orgUrl);
        });


        // Onclick events on Search suggestions.
        $(document).on("click",".search-swatch",function(e) {
          e.preventDefault();
          var sku = $(this).find(".search-swatch-img").data("pointid")
          var thumbImage = $(this).find(".search-swatch-img").data("src");
          var badge = $(this).find(".search-swatch-img").data("badge");
          var url = $(this).find(".search-swatch-img").data("url");
          $(this).parents(".prdId-"+sku).find(".search-image-broken").attr("src",thumbImage);
          $(this).parents(".prdId-"+sku).find(".banner-badge").html(badge);
          $(this).parents(".prdId-"+sku).find(".product-tile__anchor").attr("href",url);

          $(this).parents(".prdId-"+sku).find(".search-image-broken").data("src",thumbImage);
          $(this).parents(".prdId-"+sku).find(".banner-badge").data("badge",badge);
          $(this).parents(".prdId-"+sku).find(".product-tile__anchor").data("orginalurl",url);
          $(this).parents(".prdId-"+sku).find(".selected-swatch").removeClass("selected-swatch");
          $(this).addClass("selected-swatch");
        });

        // Search suggestion Popup End.
        // Minicart Popup.
        $(".header__minicart").on("click",function(e){
          e.preventDefault();
          //Cart icon GTM Event
          dataLayer.push({
            'event': 'navigationClick',
            'category': 'main menu navigation clicks',
            'action': 'clicked main menu navigation link',
            'label': "Cart",
          });

          $("body").addClass("popup-scroll");
          var length = $("#mini-cart").children().length;
          if(length > 0) {
              var res = wishlistCartIcons.split("=");
              // Changing wishlist icons
              $.each(res, function(key,val){
                if(val != "") {
                  var splitedVals = val.split("|");
                  $(".prd-id-"+splitedVals[0]+" .cart-wishlist-icon .mini-wishlist-black").css("display","block");
                  $(".prd-id-"+splitedVals[0]+" .cart-wishlist-icon .mini-wishlist-black").attr("data-wishlistId",splitedVals[1]);
                  $(".prd-id-"+splitedVals[0]+" .cart-wishlist-icon .mini-wishlist-black").attr("data-wishlistItemId",splitedVals[2]);
                  $(".prd-id-"+splitedVals[0]+" .cart-wishlist-icon .mini-wishlist-black").attr("data-pId",splitedVals[0]);
                  $(".prd-id-"+splitedVals[0]+" .cart-wishlist-icon .mini-wishlist").css("display","none");
                }
             });
            $(".mini-cart-overlay").show();
            $(".mini-cart").css('display','flex');
          }
          else {
            if(settings.emptycart != "" && settings.emptycart != undefined) {
              window.location.href = settings.emptycart;
            }
          }
        });

        $(document).on('click','.minicart-header .close-button', function(e) {
          e.stopPropagation();
          $(".mini-cart-overlay").hide();
          $(".mini-cart").hide();
          $("body").removeClass("popup-scroll");
        });

        setTimeout(function(){
          $.ajax({
            type: "POST",
            url: "/sfcc-mini-cart",
            dataType: 'json',
            data: {
                searchtxt: "minicart",
            },
            complete: function () {
            },
            success: function (response) {
              var productCount = 0;
              if(response != undefined && response != "" && response.cart != undefined) {
                if(response.cart.total != 0 && response.cart.total != undefined) {
                  let wishListIds = "";
                  let cartInfo =
                  `<div class="flex minicart-header">
                    <h2 class="utility-overlay__header-title font-weight--semibold">
                      <a class="utility-overlay__header-anchor link link--underline link--capitalise" href="/cart" title="View Shopping Cart">View Shopping Cart</a>
                    </h2>
                    <div class="close-button">
                      <img src="/themes/custom/delta_theme/images/icons/src/close.svg"/>
                    </div>`;
                    if(response.cart.baskets[0].c_basket_valid == false){
                      if(typeof settings.cartBasketValid != undefined || settings.cartBasketValid != '' ) {
                        cartInfo += '<div class="basket-invalid">' + settings.cartBasketValid + '</div>';
                      }
                    }
                  cartInfo += `</div>`;

                  $.each(response.cart.baskets, function(basketkey,basketval){
                    $.each(basketval, function(key,val){
                      if(key == 'product_items') {
                        cartInfo += `<div class="shopping-cart-item row" data-basketid="${basketval['basket_id']}">`;
                        $.each(val, function(itemkey,itemval){
                          productCount = productCount + itemval['quantity'];
                          var price_adjustments = 0;
                          $.each(itemval, function(prdkey,prdval){
                            if(prdkey == 'price_adjustments') {
                              if(prdval != 0) {
                                price_adjustments = 1;
                              }
                            }
                            if(prdkey == 'c_product') {
                              var imgContainer = "";
                              var badge = "";
                              var finish = "";
                              var price = "";
                              var maxQuantity = "";
                              var promotionMessage = "";
                              var inStock = "";
                              var finalDiscountPrice = "";
                              $.each(prdval, function(keys,values){
                                if(keys == 'image') {
                                  imgContainer +=
                                    `<div class="product-img">
                                      <img src="${values['url']}" class="img-responsive" alt="${values['alt']}" title="${values['title']}"/>
                                    </div>`;
                                }
                                if(keys == 'badge') {
                                  if(values['badgeLabel'] != "" && values['badgeLabel']!= undefined ) {
                                    badge +=
                                      `<div class="slider_flag">
                                        <div class="flag flag--gray-30">${values['badgeLabel']}</div>
                                      </div>`;
                                  }
                                }
                                if(keys == 'variations') {
                                  if(prdval['variations'] != "" && prdval['variations'] != undefined ) {
                                    finish +=
                                      `<div class="finish finish-font">
                                        ${values[0]['displayValue']}: <span class="finish-icon">${values[0]['selectedValue']}</span>
                                      </div>`;
                                  }
                                }

                                if(keys == 'price') {
                                  if(values['list'] != null && values['list'] != "") {
                                    price +=
                                      `<div class="price">
                                        <div class="price-info label">Each</div>`;
                                    if(values['sale']['formatted'] != null && values['sale']['formatted'] != "") {
                                      price += `<span class="sale-price bold-price">${values['sale']['formatted']}</span>`;
                                    }
                                    price += '</div>';
                                  }
                                  else if(values['sale'] != null && values['sale'] != "") {
                                    price +=
                                      `<div class="price">
                                        <div class="price-info label">Each</div>
                                        <div class="list-price bold-price">${values['sale']['formatted']}</div>
                                      </div>`;
                                  }
                                }
                                if(keys == 'quantity') {
                                  maxQuantity = values['max'];
                                }
                                if(keys == 'callOutMsgs') {
                                  if(values.length != 0) {
                                    finalDiscountPrice = itemval['price_after_order_discount'];
                                    promotionMessage += '<div class="product-line-item__promotions">';
                                    $.each(values, function(index,message){
                                      promotionMessage +=  '<div class="cart-discount">'+message+'</div>';
                                    });
                                    promotionMessage += '</div>';
                                  }
                                }
                                if(keys == 'availability') {
                                  if(values['messages'] != "" && values['messages'] != undefined) {
                                    inStock += "<div class='instock'>"+values['messages'][0]+"</div>";
                                  }
                                }
                              });
                              cartInfo +=
                                `<div class="cart-item-container product-info prd-id-${itemval['product_id']} cart-item-${itemval['item_id']}" data-itemid="${itemval['item_id']}" data-prdid="${itemval['product_id']}">
                                  <div class="info-container">
                                    <div class="image-container"><a href="${prdval['url']}">${imgContainer}</a></div>
                                    <div class="product-info-section">${badge}
                                      <div class="product-name"><strong>${prdval['collection']} </strong><a class="product-item-name-link" href="${prdval['url']}">${itemval['product_name']}</a></div>
                                      <div class="sku">Model#: <span class="model-no">${itemval['product_id']}</span></div>
                                      ${finish}
                                      ${inStock}
                                      ${promotionMessage}
                                    </div>
                                  </div>
                                  <div class="pricing-info">${price}
                                    <div class="qty">
                                      <div class="label">Quantity</div>
                                      <select class="product-line-item__qty-input form-control form-control--select form-control--small" data-line-item-component="qty-action" data-product-component="qty" id="quantity" name="quantity">`;
                                for (i = 1; i <= maxQuantity; i++) {
                                  var selected = (itemval['quantity'] == i ) ? "selected":"";
                                  cartInfo += `<option ${selected}>${i}</option>`;
                                }
                                cartInfo +=
                                      `</select>
                                    </div>
                                    <div class="total-price">
                                      <div class="total-info">Price</div>`;
                                      if(price_adjustments == 0) {
                                        cartInfo += `<div class="bold-price list-prd-${itemval['item_id']}">$${itemval['price'].toFixed(2)}</div>`;
                                      }
                                      else {
                                        cartInfo += `<div class="bold-price sale-prds-${itemval['item_id']}">$${itemval['price_after_order_discount'].toFixed(2)}</div>`;
                                      }
                                    cartInfo += `</div>
                                  </div>
                                  <div class="cart-item-action-links">
                                    <div class="left-align-content cart-wishlist-icon">
                                      <img class="mini-wishlist" src="/themes/custom/delta_theme/images/icons/src/wishlist-icon.svg"/>
                                      <img class="mini-wishlist-black" src="/themes/custom/delta_theme/images/icons/src/wishlist-black.svg"/>
                                    </div>
                                  <div class="right-align-content">
                                    <span class="save-for-later">
                                      <a class="mini-save-later" href="#">Save for Later</a>
                                    </span>
                                    <span class="product-remove">
                                      <a href="#" class="mini-remove-product">Remove</a>
                                    </span>
                                  </div>
                                </div>
                                <div class="remove-popup-dialog"></div>
                              </div>`;
                            }
                            // Wish List checking
                            if(prdkey == 'c_wishlist') {
                              if(prdval['listId'] != "" && prdval['listId'] != undefined) {
                                $.ajax({
                                  type: "POST",
                                  url: "/get-whilist-items",
                                  dataType: 'json',
                                  async: false,
                                  data: {
                                      wishlistId: prdval['listId'],
                                  },
                                  complete: function () {
                                  },
                                  success: function (response) {
                                    if(response != "") {
                                      $.each(response, function(wishkey,wishval){
                                        if(wishkey == 'customer_product_list_items') {
                                          $.each(wishval,function(productkey,productval) {
                                            if(productval['product_id'] == itemval['product_id']){
                                              wishlistCartIcons += productval['product_id']+"|"+prdval['listId']+"|"+prdval['listItemId']+"=";
                                            }
                                          });
                                        }
                                      });
                                    }
                                  }
                                });
                              }
                            }
                          });
                        });
                        cartInfo += '</div>';
                      }
                    });
                    if(typeof basketval['c_basket_valid'] === 'undefined' || basketval['c_basket_valid'] == false) {
                      cart_class = 'checkout-button disableCart';
                    }else{
                      cart_class = 'checkout-button';
                    }
                    cartInfo +=
                      `<div class="minicart-footer">
                        <div class="estimated-total-wrapper">
                          <div class="estimated-total-label">Estimated Total</div>
                          <div class="estimated-total-value">$${basketval['product_total'].toFixed(2)}</div>
                        </div>
                        <p class="shipping-taxes-info">Shipping and taxes calculated at checkout.</p>
                        <div class="checkout-button-wrapper">
                           <a class="`+ cart_class +`" href="/s/deltafaucet/checkout-login">PROCEED TO CHECKOUT</a>
                        </div>
                      </div>
                      `;
                  });
                  if(productCount != 0 && productCount != undefined) {
                    $(".cart-count").html(productCount);
                    $(".cart-count").css("display","block");
                  }
                  $("#mini-cart").html(cartInfo);
                }
                else {
                  $(".cart-count").css("display","none");
                }
                // Fixing error images
                $(".img-responsive").bind("error",function(){
                  // Replacing image source
                  $(this).attr("src","/themes/custom/delta_theme/images/no-image-lg.png");
                 });
              }
              if(response.user != "" && response.user != null && response.user != undefined) {
                // User Details GTM Events.
                if(response.user.type != null && response.user.type != undefined && response.user.type != "") {
                  if(response.user.type == 'guest') {
                    dataLayer.push({
                      'visitor': {
                        "logged_in_status": "Not Logged In",
                        "visitorID": response.user.visitorId
                      }
                    });
                    $('[data-usertype="guest"]').css("display","block");
                    $('[data-usertype="auth"]').css("display","none");
                  }
                  else {
                    dataLayer.push({
                      'visitor': {
                        "customerID": response.user.customerId,
                        "customer_type": "new",
                        "doNotSellInfo": "y",
                        "email": response.user.email,
                        "logged_in_status": "Logged In",
                        "visitorID": response.user.visitorId
                      }
                    });
                    $('[data-usertype="auth"]').css("display","block");
                    $('[data-usertype="guest"]').css("display","none");
                  }
                }
              }
            }
          });
        },100);

        // Select box quantity update in mini cart
        $(document).on('change','.product-line-item__qty-input', function() {
          var itemId = $(this).parent().parent().parent(".product-info").data("itemid");
          var basketId = $(this).parent().parent().parent().parent(".shopping-cart-item").data("basketid");
          var quantity = $(this).val();
          if(itemId != "" && basketId != "") {
            $(".parent-mini .ajax-progress-throbber").css("display","flex");
            $.ajax({
              type: "POST",
              url: "/sfcc-mini-cart-update",
              dataType: 'json',
              data: {
                basketid: basketId,
                itemid: itemId,
                quantity:quantity,
              },
              complete: function () {
                $(".parent-mini .ajax-progress-throbber").css("display","none");

              },
              success: function (response) {
                if(response != "") {
                  $.each(response.product_items, function(basketkey,basketval){
                    if(basketval['item_id'] == itemId) {
                      $(".list-prd-"+itemId).html("$"+basketval['price']);
                      $(".sale-prd-"+itemId).html("$"+basketval['price_after_item_discount']);
                      $(".estimated-total-value").html("$"+response['product_sub_total']);
                      $.ajax({
                        type: "POST",
                        url: "/sfcc-mini-cart",
                        dataType: 'json',
                        data: {
                          searchtxt: "minicart",
                        },
                        complete: function () {
                        },
                        success: function (response) {
                          if(response != undefined && response != "" && response.cart != undefined) {
                            if(response.cart.baskets[0].c_basket_valid != false) {
                              $("div.basket-invalid").remove();
                              $( ".mini-cart .minicart-footer .disableCart" ).removeClass( "checkout-button disableCart" ).addClass( "checkout-button" );
                            }
                          }
                        }
                      });
                    }
                  });
                }
              }
            });
          }
        });

        // Save Later in mini cart
        $(document).on('click','.mini-save-later', function(e) {
          e.preventDefault();
          var itemId = $(this).parents(".product-info").data("itemid");
          var basketId = $(this).parent().parent().parent().parent().parent(".shopping-cart-item").data("basketid");
          var quantity = $(this).parents(".cart-item-container").find(".pricing-info").find(".qty").find(".product-line-item__qty-input").val();
          var productId = $(this).parents(".product-info").data("prdid");
          if(itemId != "" && basketId != "") {
            $(".parent-mini .ajax-progress-throbber").css("display","flex");
            $.ajax({
              type: "POST",
              url: "/sfcc-mini-cart-savelater",
              dataType: 'json',
              data: {
                basketid: basketId,
                itemid: itemId,
                quantity:quantity,
                productid:productId,
              },
              complete: function () {
                $(".parent-mini .ajax-progress-throbber").css("display","none");
              },
              success: function (response) {
                if(response != "") {
                  if(response.product_sub_total != 0) {
                    $(".estimated-total-value").html("$"+response['product_sub_total']);
                    $(".cart-item-"+itemId).remove();
                    $.ajax({
                      type: "POST",
                      url: "/sfcc-mini-cart",
                      dataType: 'json',
                      data: {
                        searchtxt: "minicart",
                      },
                      complete: function () {
                      },
                      success: function (response) {
                        if(response != undefined && response != "" && response.cart != undefined) {
                          if(response.cart.baskets[0].c_basket_valid != false) {
                            $("div.basket-invalid").remove();
                            $( ".mini-cart .minicart-footer .disableCart" ).removeClass( "checkout-button disableCart" ).addClass( "checkout-button" );
                          }
                        }
                      }
                    });
                  }
                  else {
                    $(".cart-item-"+itemId).remove();
                    var length = $(".cart-item-container").length;
                    if(length == 0) {
                      $("#mini-cart").html("");
                      $(".mini-cart-overlay").hide();
                      $(".mini-cart").hide();
                      $(".cart-count").html("");
                      $(".cart-count").css("display","none");
                    }
                  }
                }
              }
            });
          }
        });

        // Add to Wishlist in mini cart
        $(document).on('click','.mini-wishlist', function(e) {
          e.preventDefault();
          var itemId = $(this).parents(".product-info").data("itemid");
          var basketId = $(this).parent().parent().parent().parent(".shopping-cart-item").data("basketid");
          var quantity = $(this).parents(".cart-item-container").find(".pricing-info").find(".qty").find(".product-line-item__qty-input").val();
          var productId = $(this).parents(".product-info").data("prdid");
          if(itemId != "" && basketId != "") {
            $(".parent-mini .ajax-progress-throbber").css("display","flex");
            $.ajax({
              type: "POST",
              url: "/sfcc-mini-cart-wishlist",
              dataType: 'json',
              data: {
                basketid: basketId,
                itemid: itemId,
                quantity:quantity,
                productid:productId,
                type:"cart",
              },
              complete: function () {
                $(".parent-mini .ajax-progress-throbber").css("display","none");
              },
              success: function (response) {
                if(response != "") {
                  if(response.product_sub_total != 0) {
                    $(".estimated-total-value").html("$"+response['product_sub_total']);
                    $(".cart-item-"+itemId).remove();
                    $.ajax({
                      type: "POST",
                      url: "/sfcc-mini-cart",
                      dataType: 'json',
                      data: {
                        searchtxt: "minicart",
                      },
                      complete: function () {
                      },
                      success: function (response) {
                        if(response != undefined && response != "" && response.cart != undefined) {
                          if(response.cart.baskets[0].c_basket_valid != false) {
                            $("div.basket-invalid").remove();
                            $( ".mini-cart .minicart-footer .disableCart" ).removeClass( "checkout-button disableCart" ).addClass( "checkout-button" );
                          }
                        }
                      }
                    });
                  }
                  else {
                    $(".cart-item-"+itemId).remove();
                    var length = $(".cart-item-container").length;
                    if(length == 0) {
                      $("#mini-cart").html("");
                      $(".mini-cart-overlay").hide();
                      $(".mini-cart").hide();
                      $(".cart-count").html("");
                      $(".cart-count").css("display","none");
                    }
                  }
                }
              }
            });
          }
        });

        // Remove whishlist product_total
        $(document).on('click','.mini-wishlist-black', function(e) {
          e.preventDefault();
          var wishlistId = $(this).data("wishlistid");
          var wishlistItemId = $(this).data("wishlistitemid");
          var productId = $(this).data("pid");
          if(wishlistId != "" && wishlistItemId != "") {
            $(".parent-mini .ajax-progress-throbber") .css("display","flex");
            $.ajax({
              type: "POST",
              url: "/wishlist-remove",
              dataType: 'json',
              data: {
                wishlistId: wishlistId,
                wishlistItemId: wishlistItemId,
              },
              complete: function () {
                $(".parent-mini .ajax-progress-throbber").css("display","none");
              },
              success: function (response) {
                if(response != "") {
                  $(".prd-id-"+productId+" .cart-wishlist-icon .mini-wishlist-black").css("display","none");
                  $(".prd-id-"+productId+" .cart-wishlist-icon .mini-wishlist").css("display","block");
                }
              }
            });
          }
        });

        // Add to Wishlist in mini cart
        $(document).on('click','.search-wishlist', function(e) {
          e.preventDefault();
          var productId = $(this).parents(".product-tile").data("prdid");
          if(productId != "") {
            $(".site-search__form .ajax-progress-throbber").css("display","flex");
            $.ajax({
              type: "POST",
              url: "/sfcc-mini-cart-wishlist",
              dataType: 'json',
              data: {
                basketid: "",
                itemid: "",
                quantity:"1",
                productid:productId,
                type:"search",
              },
              complete: function () {
                $(".site-search__form .ajax-progress-throbber").css("display","none");
              },
              success: function (response) {
                if(response != "") {
                  if(response.product_id != "" && response.product_id != undefined) {
                    $(".prdId-"+productId).find(".search-wishlist").css("display", "none");
                    $(".prdId-"+productId).find(".search-wishlist-black").css("display", "block");
                  }
                }
              }
            });
          }
        });

        // Remove product in mini cart
        $(document).on('click','.mini-remove-product', function(e) {
          e.preventDefault();
          var itemId = $(this).parent().parent().parent().parent(".product-info").data("itemid");
          var basketId = $(this).parent().parent().parent().parent().parent(".shopping-cart-item").data("basketid");
          var c_basket_valid = $(this).parent().parent().parent().parent().parent(".shopping-cart-item").data("c_basket_valid");


          let popUpHtml = `<div class="inline-prompt flex flex-direction-col text-align--center gutter--small toggle--active" data-line-item-component="remove-confirm">
                            <div class="inline-prompt__header">
                              <h4 id="removeLineItem" class="inline-prompt__title font-weight--semibold text-line--large">Remove Product?</h4>
                            </div>
                            <div class="inline-prompt__body body-type--deci text-color--grey-5">
                              Are you sure you want to remove the following product from the cart?
                            </div>
                            <div class="inline-prompt__footer row flex-justify-center set--w-100">
                              <div class="col-4">
                                <button type="button" class="button cart-rmv-cancel-btn button--cancel button--small button--primary-outline body-type--deci promt--btn-align">Cancel</button>
                              </div>
                              <div class="col-4">
                                <button type="button" class="button cart-remove-btn button--small button--primary body-type--deci promt--btn-align" data-itemid="${itemId}" data-basketid="${basketId}">Yes</button>
                              </div>
                            </div>
                        </div>`;
          $(".remove-popup-dialog").html("");
          $(this).parents(".cart-item-container").find(".remove-popup-dialog").html(popUpHtml);
        });

        $(document).on("click",".cart-rmv-cancel-btn", function(){
          $(".remove-popup-dialog").html("");
        });

        // Remove product in mini cart
        $(document).on("click",".cart-remove-btn", function(e) {
          e.preventDefault();
          var itemId = $(this).data("itemid");
          var basketId = $(this).data("basketid");
          if(itemId != "" && basketId != "") {
            $(".parent-mini .ajax-progress-throbber").css("display","flex");
            $.ajax({
              type: "POST",
              url: "/sfcc-mini-cart-remove",
              dataType: 'json',
              data: {
                basketid: basketId,
                itemid: itemId,
              },
              complete: function () {
                $(".parent-mini .ajax-progress-throbber").css("display","none");
            },
              success: function (response) {
                if(response != "") {
                  if(response.product_sub_total != 0) {
                    $(".estimated-total-value").html("$"+response['product_sub_total']);
                    $(".cart-item-"+itemId).remove();
                    $.ajax({
                      type: "POST",
                      url: "/sfcc-mini-cart",
                      dataType: 'json',
                      data: {
                        searchtxt: "minicart",
                      },
                      complete: function () {
                      },
                      success: function (response) {
                        if(response != undefined && response != "" && response.cart != undefined) {
                          if(response.cart.baskets[0].c_basket_valid != false) {
                            $("div.basket-invalid").remove();
                            $( ".mini-cart .minicart-footer .disableCart" ).removeClass( "checkout-button disableCart" ).addClass( "checkout-button" );
                          }
                        }
                      }
                    });
                  }
                  else {
                    $(".cart-item-"+itemId).remove();
                    var length = $(".cart-item-container").length;
                    if(length == 0) {
                      $("#mini-cart").html("");
                      $(".mini-cart-overlay").hide();
                      $(".mini-cart").hide();
                      $(".cart-count").html("");
                      $(".cart-count").css("display","none");
                    }
                  }
                }
              }
            });
          }
        });

        // Appending HTML to Cookie complience
        setTimeout(function(){
          $('<button type="button" name="consent" value="false" class="window-modal__close cookie-consent-close-icon" title="No"></button>').prependTo( $( ".eu-cookie-compliance-banner-info" ) );
        },100);

        $(document).on("click", ".cookie-consent-close-icon", function(){
          //$("#sliding-popup").css("display", "none");
          $(".eu-cookie-compliance-banner button.decline-button").trigger("click");
        });


        // Dropzone Js message change.
        $(".dz-message").html("Drag your files here to upload them or <a class='dropzone-upload'>Click here to add them</a>");

        $('.content input', context).once('addForAttribute').each(function () {
            var elemId = $(this).attr('id');
            var elemType = $(this).attr('type');
            if(elemId != undefined && elemType != 'file'){
                $(this).attr('for', elemId);
            }
            $('#datepicker').removeClass('form-control')
            .addClass('form-item__textfield--date form-item__textfield form-item__textfield--date');
        });

        //Removing the duplicate label on the change of dropdown in PLP and CDP
        $('.plp-results .sort_elements').on('change','.form-item__select',function() {
          var select_val = $( ".plp-results .sort_elements .form-item__select option:selected" ).text();
          if(select_val == $('.plp-results .sort_elements .form-item__select option:first-child').text()) {
            $(".plp-results .sort_elements .form-item__select").siblings('.form-item__label--textfield').css('display','none');
          }
        });

        $('.collection-details__dropdowns .form-item__select.form-item__select--im').each(function() {
          $(this).on('change',function() {
            if($(".form-item__select.form-item__select--im option:first-child").is(':selected')) {
              var select_label = $(".form-item__select.form-item__select--im option:first-child").parent().siblings('.form-item__label--textfield').toggleClass('select_label');

            }
          })
        });

        //Lazy loading Images in Mega Menu
        $('.mega-nav .mega-menu-nav--sub').find('img').addClass('lazy');

          if($('.mega-menu-nav__item.mega-menu-nav__item--with-sub').hasClass('main-menu-level')) {
            $('.mega-menu-nav__item.mega-menu-nav__item--with-sub.main-menu-level').hover(function() {
              var img = $(this).find("img.lazy"), // select images inside .container
              len = img.length, // check if they exist
              arr = [];
              if( len > 0 ) {
                // images found, get id
                $(this).find($('.mega-menu-nav__item.mega-menu-nav__item--sub.mega-menu-nav__item--sub-1')).each(function(){
                  if($(this).find('img').hasClass('lazy')) {
                    var img_src =  $(this).find('img.lazy').attr("data-src");
                    $(this).find('img.lazy').attr('src', img_src);
                  }
                  $(this).find($("ul.mega-menu-nav--sub-1 li.mega-menu-nav__item--sub-2")).each(function(e) {
                    if($(this).find('img').hasClass('lazy')) {
                      var img_src =  $(this).find('img.lazy').attr("data-src");
                      $(this).find('img.lazy').attr('src', img_src);
                    }
                  })
                });
              }
            })
          }

          //Lazy load of images in home pages
          if($('.lazy-load-on').length > 0 ) {
            $(window).on('scroll',function() {
              $('.lazy-load-on').each(function() {
                if($(this).find('[data-src]')) {
                  $(this).find('[data-src]').each(function() {
                    if ($(this).css('background-image') != 'none') {
                      $(this).addClass('lazy');
                      if($(this).hasClass('lazy')) {
                        if($(this).attr('data-src') !== "") {
                          var window_height = $(window).height();
                          var offset = $(this).offset().top - window_height;
                          if ($(window).scrollTop() >= offset) {
                            var img_source = $(this).attr("data-src");
                            $(this).css('background-image', 'url(' + img_source + ')');
                          }
                        }
                      }
                    }
                    else {
                      $(this).addClass('lazy');
                      if($(this).hasClass('lazy')) {
                        if($(this).attr('data-src') !== "") {
                          var window_height = $(window).height();
                          var offset = $(this).offset().top - window_height;
                          if ($(window).scrollTop() >= offset) {
                            var img_source = $(this).attr("data-src");
                            $(this).attr('src', img_source);
                          }
                        }
                      }
                    }
                  })
                }
              })
            })
          }

          //Lazy load of the images in Collection Listing pages of kitchen and bath
          $('.kitchen-collection-1 .collection-listing,.bath-collection-1 .collection-listing').find('img').addClass('lazy');

          // Similar collections lazy load iages
          $('.similar-collection-section').find('img').addClass('lazy');

            $('.similar-collection-section img.lazy').each(function() {
              if($(this).attr('data-source')) {
                if ($(this).attr('data-source') !== "") {
                  var window_height = $(window).height();
                  var offset = $(this).offset().top - window_height;
                  var img_src =  $(this).attr('data-source');
                  $(this).attr('src', img_src);
                  $(this).css({
                     'width' : 'auto',
                     'height' : 'auto'
                  });
                  $(this).removeClass('lazy');
                  $(this).removeAttr('data-source');
                  return false;
                }
              }
              if($(this).attr('data-source') == "") {
                $(this).attr('src','');
                $(this).removeClass('lazy');
              }
            });

          $(window).on('scroll',function() {
            $('.kitchen-collection-1 .collection-listing img,.bath-collection-1 .collection-listing img').each(function() {
              if($(this).attr('data-source')) {
                if ($(this).attr('data-source') !== "") {
                  var window_height = $(window).height();
                  var offset = $(this).offset().top - window_height;
                  if ($(window).scrollTop() >= offset) {
                    var img_src =  $(this).attr('data-source');
                    $(this).attr('src', img_src);
                    $(this).css({
                       'width' : 'auto',
                       'height' : 'auto'
                    });
                    $(this).removeClass('lazy');
                    $(this).removeAttr('data-source');
                    return false;
                  }
                }
              }
              if($(this).attr('data-source') == "") {
                $(this).attr('src','');
                $(this).removeClass('lazy');
              }
            })
          });

          function desktopviewport() {
            $('.kitchen-collection-1 .collection-listing,.bath-collection-1 .collection-listing').find('img.lazy:lt(9)').each(function() {
              var src_img = $(this).attr('data-source');
              $(this).attr('src', src_img);
              $(this).css({
                 'width' : 'auto',
                 'height' : 'auto'
              });
              $(this).removeClass('lazy');
            })
          }

          function mobileviewport() {
            $('.kitchen-collection-1 .collection-listing,.bath-collection-1 .collection-listing').find('img.lazy:lt(4)').each(function() {
              var src_img = $(this).attr('data-source');
              $(this).attr('src', src_img);
              $(this).css({
                 'width' : 'auto',
                 'height' : 'auto'
              });
              $(this).removeClass('lazy');
            })
          }

          $(window).on('resize', function(e) {
            if ($(window).width() < 768) {
              mobileviewport();
            }
          });

          if ($(window).width() < 768) {
            mobileviewport();
          }

          $(window).on('resize', function(e) {
            if ($(window).width() >= 768) {
              desktopviewport()
            }
          });

          if ($(window).width() >= 768) {
            desktopviewport()
          }

        $(".videos-for-professional").on("click", ".region-content .video-thumbnail", function(e) {
            e.preventDefault();
            var href = $(this).attr('href');
            $.ajax({
                type: "POST",
                url: "/get-professional-video",
                dataType: 'json',
                data: {
                    url: href,
                },
                complete: function () {
                },
                success: function (data) {
                    $('.region-content').html(data.result);
                }
            });
        });

        $( document ).ready(function() {
          $(".region-site-branding .block-commerce-cart").addClass("mobile-cart-block");
          $(".region-site-branding .mobile-header-icons").remove();
          $(".mobile-header-icons" ).clone().insertBefore(".mobile-cart-block a.cart-icon" );

          jQuery("#mobile-serach-icon-block").click(function(){
            $("#search-wrapper").toggleClass('mobile-search-active');
          });

          var c = ['all', 'traditional', 'contemporary', 'transitional']
          var counter = 0
          $('.kitchen-collection .quicktabs-tabpage, .bath-collection .quicktabs-tabpage, .kitchen-sinkscollections .quicktabs-tabpage, .bathroom-sinkscollections .quicktabs-tabpage').each(function(){
            $(this).addClass(c[counter])
            if(counter < (c.length -1))
              counter++
            else
              counter = 0
          })
          jQuery(".collections-quicktabs li a").click(function(){
            if(jQuery(".collections-quicktabs li").hasClass("active")) {
              jQuery(".collections-quicktabs li").find("a.use-ajax").removeClass("sub-menu-active");
              jQuery(this).addClass("sub-menu-active");
            }
            jQuery(this).addClass("active")
            if(jQuery(".collections-quicktabs li a").hasClass("active")) {
              jQuery(".collections-quicktabs li").find("a.use-ajax").removeClass("sub-menu-active active");
              jQuery(this).addClass("sub-menu-active");
            }

            if(jQuery(this).attr("id") === "All"){
              if(jQuery(".quicktabs-tabpage").hasClass("all")){
                jQuery(".quicktabs-tabpage").removeClass("quicktabs-hide");
                jQuery(".quicktabs-tabpage.traditional").addClass("quicktabs-hide");
                jQuery(".quicktabs-tabpage.contemporary").addClass("quicktabs-hide");
                jQuery(".quicktabs-tabpage.transitional").addClass("quicktabs-hide");
              }
            }

            if(jQuery(this).attr("id") === "Traditional"){
              if(jQuery(".quicktabs-tabpage").hasClass("traditional")){
                jQuery(".quicktabs-tabpage").removeClass("quicktabs-hide");
                jQuery(".quicktabs-tabpage.all").addClass("quicktabs-hide");
                jQuery(".quicktabs-tabpage.contemporary").addClass("quicktabs-hide");
                jQuery(".quicktabs-tabpage.transitional").addClass("quicktabs-hide");
              }
            }

            if(jQuery(this).attr("id") === "Contemporary"){
              if(jQuery(".quicktabs-tabpage").hasClass("contemporary")){
                jQuery(".quicktabs-tabpage").removeClass("quicktabs-hide");
                jQuery(".quicktabs-tabpage.all").addClass("quicktabs-hide");
                jQuery(".quicktabs-tabpage.traditional").addClass("quicktabs-hide");
                jQuery(".quicktabs-tabpage.transitional").addClass("quicktabs-hide");
              }
            }

            if(jQuery(this).attr("id") === "Transitional"){
              if(jQuery(".quicktabs-tabpage").hasClass("transitional")){
                jQuery(".quicktabs-tabpage").removeClass("quicktabs-hide");
                jQuery(".quicktabs-tabpage.all").addClass("quicktabs-hide");
                jQuery(".quicktabs-tabpage.traditional").addClass("quicktabs-hide");
                jQuery(".quicktabs-tabpage.contemporary").addClass("quicktabs-hide");
              }
            }
          });

        jQuery('.active-lang-link').click(function(){
            jQuery(this).parent('.dropdown').toggleClass('active');
        });

        if($("main.main--with-sidebar").find("form.delta-contact-us-form-new")) {
          $("main.main--with-sidebar").addClass("delta-contact-us-form-new-container")
        }
        $(".footer-info .block--newsletter--footer.bg-grey-90").parents('.footer-info').first().addClass("bg-color-grey-90");
        });

        // FAQ Print
        $(".faq--print--content", context).click(function (e) {
          e.preventDefault();
          $(this).parent().parent().prev().printThis({
            debug: true,
            importCSS: false,
            importStyle: false,
            removeScripts: true
          });
        });

        // Triger Question and Answer Section

        $(document).on("click",".bv-content-product-stats-item-questions a", function () {
          $('a[name="qaTab"]')[0].click();
            if(!$('div[name="qaTab"]').hasClass("active-tab-title")) {
                $('div[name="qaTab"]').addClass("active-tab-title");
                $('div[name="qaTab"]').next().addClass("is-active");
            }
        });

        $(document).on("click",".bv-content-product-stats-item-answers a", function () {
            $('a[name="qaTab"]')[0].click();
            if(!$('div[name="qaTab"]').hasClass("active-tab-title")) {
                $('div[name="qaTab"]').addClass("active-tab-title");
                $('div[name="qaTab"]').next().addClass("is-active");
            }
        });


        // Ask a Question
        $( ".pdp-rating" ).on( "click", ".bv_button_buttonMinimalist", function() {
          $('a[name="qaTab"]')[0].click();
        });

        //Review star
        $(document).on( "click", ".bv_histogram_row_container", function() {
          $('a[name="reviewsTab"]')[0].click();
        });

        //Number of Reviews
        $( ".pdp-rating" ).on( "click", ".bv_numReviews_text", function() {
          $('a[name="reviewsTab"]')[0].click();
          var position = $("#tab--2").offset().top;
          $("body, html").animate({
            scrollTop: position
          }, 500 );
        });

        //Read Reviews Button
        $(document).on( "click", ".bv_button_buttonFull", function() {
          $('a[name="reviewsTab"]')[0].click();
          var position = $("#tab--2").offset().top;
          $("body, html").animate({
            scrollTop: position
          }, 500 );
        });

        //Average Rating
        $( ".pdp-rating" ).on( "click", ".bv_avgRating_component_container", function() {
          $('a[name="reviewsTab"]')[0].click();
          var position = $("#tab--2").offset().top;
          $("body, html").animate({
            scrollTop: position
          }, 500 );
        });

        //star BV
        $( ".pdp-rating" ).on( "click", ".bv_stars_component_container", function() {
          $('a[name="reviewsTab"]')[0].click();
          var position = $("#tab--2").offset().top;
          $("body, html").animate({
            scrollTop: position
          }, 500 );
        });
        // Close button trigger for Compare List
        $(".com-popup-close a").on('click', function(e) {
          $('button.ui-dialog-titlebar-close').click();
        });
        // Close button trigger for Compare List

        // Pre select contact us page product information
        $.urlParam = function(name){
          var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
          if (results == null){
            return null;
          }
          else {
            return decodeURI(results[1]) || 0;
          }
        }
        var defaultText = $.urlParam('topic');
        //var urlParams = new URLSearchParams(window.location.search);
        //var defaultText = urlParams.getAll('topic')[0];
        if(defaultText != '') {
          $('select#topic option').each(function () {
            if( $.trim( $(this).text() ) == defaultText ) {
              var selectedValue = $(this).val();
              $(this).val(selectedValue).attr('selected', true);
            }
          });
        }

        var originalTitle = $('.page-title').html();
        $('ul.tabs__nav li a').click(function() {
          if(!$(".quicktabs-wrapper").length) {
            if ($(this).parent('li').index() == 0) {
              $('.page-title').html(originalTitle);
            }
            else {
              event.preventDefault();
              $('.page-title').html($(this).html());
            }
          }
        });

        //Adding the class for the filename.jpg suffix
        $('.contact-form .field-suffix').addClass('file-select-name');

        //Ajax complete for the files upload
        $(document).ajaxComplete(function(event, xhr, settings) {
          var getExtraObj = settings.extraData;
          if (getExtraObj !== undefined && getExtraObj.hasOwnProperty("_triggering_element_name") && xhr.status == 200
          && settings.extraData != undefined && settings.extraData._triggering_element_value !== 'undefined' &&
          settings.extraData._triggering_element_name === "product_image_1_upload_button") {

            $('input[name="product_image_1_remove_button"]').parents('.upload-input__label').attr({'for': "", 'data-label': ""});
            $('input[name="product_image_1_remove_button"]').parents('.upload-input__label.file-upload-cont-first').addClass("remove1");
            $('.remove1 .file-select-name').toggle();

          }
          if (getExtraObj !== undefined && getExtraObj.hasOwnProperty("_triggering_element_name") && xhr.status == 200
          && settings.extraData != undefined && settings.extraData._triggering_element_value !== 'undefined' &&
          settings.extraData._triggering_element_name === "product_image_2_upload_button") {

            $('input[name="product_image_2_remove_button"]').parents('.upload-input__label').attr({'for': "", 'data-label': ""});
            $('input[name="product_image_2_remove_button"]').parents('.upload-input__label.file-upload-cont-sec').addClass("remove2");
            $('.remove2 .file-select-name').toggle();
          }
          if (getExtraObj !== undefined && getExtraObj.hasOwnProperty("_triggering_element_name") && xhr.status == 200
          && settings.extraData != undefined && settings.extraData._triggering_element_value !== 'undefined' &&
          settings.extraData._triggering_element_name === "product_image_3_upload_button") {

            $('input[name="product_image_3_remove_button"]').parents('.upload-input__label').attr({'for': "", 'data-label': ""});
            $('input[name="product_image_3_remove_button"]').parents('.upload-input__label.file-upload-cont-third').addClass("remove3");
            $('.remove3 .file-select-name').toggle();
          }
        });

        $('.ui-autocomplete-input').on('input change keyup',function() {
          $(this).addClass('ui-autocomplete-loading');

        });

        $('.auto-suggest .model-num-list').on('click','.row-sku',function() {
          $('.ui-autocomplete-input').removeClass('ui-autocomplete-loading');
        })

        $('.ui-autocomplete').on( "click", ".autocomplete-view-all", function(e) {
          e.preventDefault();
          var url = window.location.origin;
          $redirect = $(this).attr("href");
          var redirectUrl = url+$redirect;
          $(this).closest('li').remove();
          window.location.replace(redirectUrl);
        });

        // Removed tabindex to Addthis widget
        setTimeout(function(){
          $(context).find('#at-expanding-share-button').find('a').removeAttr( "tabindex" );
        },600);
      }
    };

    // Cart Products Delete.
    Drupal.behaviors.cartDelete = {
        attach: function (context, settings) {
            $(".views-field-unit-price__number .icon--menu-close").once().on("click", function () {
                $(this).parent(".views-field-unit-price__number").parent().find(".remove-order-items").trigger("click");
                $(".ecommerce-cart").prepend('<div class="loader"><div class="ajax-progress ajax-progress-throbber"><div class="throbber">&nbsp;</div><div class="message">Please wait...</div></div></div>');
                $(this).closest('tr').hide();

            });
        }
    };



    Drupal.behaviors.videoCarousel = {
      attach: function (context, settings) {
        $('.desktop-thumb-img .thumb-img-block').on('click', function(event, slick, currentSlide, nextSlide){
         var img_src = $('.prod-big-img').find("img").attr("src");
         $(".prod-big-img").find("video").remove();
           if(img_src.indexOf('mp4') != -1) {
             $(".prod-big-img").find("img").css("display","none");
             $(".prod-big-img").prepend("<video id='myVideo' width='100%' controls autoplay> <source src='"+ img_src +"' ></video>");
             $(".prod-big-img").find("video").get(0).play();
          } else {
            $(".prod-big-img").find("img").show();
            if($(".prod-big-img").find("video").length) {
              $(".prod-big-img").find("video").get(0).pause();
            }
            $(".prod-big-img").find("img").attr('src', img_src);
          }
        });
      }
    };

    Drupal.behaviors.mobileVideoCarousel = {
      attach: function (context, settings) {
        $('.mobile-thumb-img .multiple-items').on('afterChange', function(){
          var num = $('.slick-dots .slick-active button').attr('id').split('control')[1];
          var img_src = $('#slick-slide' + num).find('img').attr('src');
          $(".mobile-thumb-img .slick-list.draggable").removeClass("fixheight");
          if ($("#myVideomobile").length) {
           $("#myVideomobile").remove();
         }
          if(img_src.indexOf('mp4') != -1) {
            $("#myVideomobile").remove();
            $(".mobile-thumb-img .thumb-img-block.slick-current.slick-active").append("<video id='myVideomobile' width='100%' controls> <source src='"+ img_src + "'></video>");
            $(".mobile-thumb-img .thumb-img-block.slick-current.slick-active").find('img').hide();
            $(".mobile-thumb-img .slick-list.draggable").addClass("fixheight");
          }
        });
      }
    };


    //Stoping Video playing after closing the modal pop up in collection pages
    $('.path-bathroom .modal--collection-gallery,.path-kitchen .modal--collection-gallery').on('click', '.modal__close',function() {
      var collection_video = $(".path-bathroom .modal--collection-gallery .video iframe,.path-kitchen .modal--collection-gallery .video iframe").attr("src");
     $(".path-bathroom .modal--collection-gallery .video iframe,.path-kitchen .modal--collection-gallery .video iframe").attr("src","");
     $(".path-bathroom .modal--collection-gallery .video iframe,.path-kitchen .modal--collection-gallery .video iframe").attr("src",collection_video);
   })

    // Gallery in CDP for IPAD and Mobile
    $('.path-kitchen .featured-banner__button-group, .path-bathroom .featured-banner__button-group, .path-kitchen-sinks .featured-banner__button-group, .path-bathroom-sinks .featured-banner__button-group').on('click',function() {
      $('.modal-gallery')[0].slick.refresh();
    });



    //Gallery of CDP
    Drupal.behaviors.CDPgallery = {
       attach: function (context, settings) {
         function galleryImg() {
           var cuurentwidth = $('.path-kitchen .slick-active,.path-bathroom .slick-active, .path-kitchen-sinks .slick-active, .path-bathroom-sinks .slick-active').find("img")[0].naturalWidth;
           var cuurentheight = $('.path-kitchen .slick-active,.path-bathroom .slick-active, .path-kitchen-sinks .slick-active, .path-bathroom-sinks .slick-active').find("img")[0].naturalHeight;
           $(".path-kitchen #collection-gallery-modal .modal,.path-bathroom #collection-gallery-modal .modal, .path-kitchen-sinks #collection-gallery-modal .modal, .path-bathroom-sinks #collection-gallery-modal .modal").width(cuurentwidth);
           $(".path-kitchen #collection-gallery-modal .modal,.path-bathroom #collection-gallery-modal .modal, .path-kitchen-sinks #collection-gallery-modal .modal, .path-bathroom-sinks #collection-gallery-modal .modal").height(cuurentheight);
         }
         $(".path-kitchen .featured-banner__button-group .button--tertiary,.path-bathroom .featured-banner__button-group .button--tertiary, .path-kitchen-sinks .featured-banner__button-group .button--tertiary, .path-bathroom-sinks .featured-banner__button-group .button--tertiary").on("click", function () {
              galleryImg();
         });

         $('.path-kitchen .modal-gallery,.path-bathroom .modal-gallery, .path-kitchen-sinks .modal-gallery, .path-bathroom-sinks .modal-gallery').on('afterChange', function(event, slick, currentSlide, nextSlide){
            galleryImg();
          });
       }
    };

    jQuery.easing['jswing'] = jQuery.easing['swing'];
    jQuery.extend( jQuery.easing,
      {
        easeInExpo: function (x, t, b, c, d) {
          return (t==0) ? b : c * Math.pow(2, 10 * (t/d - 1)) + b;
        },
        easeOutExpo: function (x, t, b, c, d) {
          return (t==d) ? b+c : c * (-Math.pow(2, -10 * t/d) + 1) + b;
        },
        easeInOutExpo: function (x, t, b, c, d) {
          if (t==0) return b;
          if (t==d) return b+c;
          if ((t/=d/2) < 1) return c/2 * Math.pow(2, 10 * (t - 1)) + b;
          return c/2 * (-Math.pow(2, -10 * --t) + 2) + b;
        }
      });


    Drupal.behaviors.awardsActive = {
        attach: function (context, settings) {
            var pathname = window.location.pathname; // Returns path only (/path/example.html)
            var splitedPath = pathname.split("/");
            if(splitedPath[2] == "awards") {
                $(".sub-menu li").each(function() {
                    var subMenus = $(this).find("a").attr("href");
                    var subMenuSplitedPath = subMenus.split("/");
                    if(subMenuSplitedPath[2] == 'awards') {
                        $(this).find("a").addClass("sub-menu-active");
                    }
                });
            }
        }
    };

    //Ear Server Form
    $('.ear-saver-form #edit-address--wrapper').find('input').addClass('form-item__textfield');

    //recaptcha expired
    $('.g-recaptcha').attr('data-expired-callback','captchaExpired');

  //Product registration remove button
  $(window).on('load',function() {
    var removebutton = $('.webform-multiple-table .parent_product_sku_purchase_date_table_remove');
    if(removebutton.length === 1) {
      $(this).addClass('removecrossbutton');
    }
    var contactus = $('.webform-submission-delta-contactus-form');
    if(contactus.length === 1) {
      $('#edit-first-name').val();
      $('#edit-last-name').val();
      $('#edit-email-address').val();
      $('#edit-phone-number').val();
      $('#edit-address-line-1').val();
      $('#edit-address-line-2').val();
      $('#edit-city').val();
      $('#edit-zip').val();
      $('#edit-state').val('');
      $('#edit-country').val('');
      $('#edit-select-message-topic').val('');
      $('#edit-product-purchase-date').val('');
      $('#edit-purchase-location').val('');
      $('#edit-how-can-we-assist-you').val();
    }

    // // Promotions GTM Triggers
    // if($("body").hasClass("home-page")) {
    //   $(".home-page a.button").each(function(index,element){
    //     var type = $(this).parent().parent().find(".featured-banner__title").text();
    //     dataLayer.push({
    //       'event': 'promotionImpression',
    //       'ecommerce': {
    //         'promoView': {
    //           'promotions': [{
    //             'name': 'home page banner',
    //             'creative': type,
    //           }]
    //         }
    //       }
    //     });
    //   });
    //
    //   dataLayer.push({
    //     'event': 'promotionImpression',
    //     'ecommerce': {
    //       'promoView': {
    //         'promotions': [{
    //           'name': 'home page banner',
    //           'creative': 'Stryke Bathroom Collection',
    //         }]
    //       }
    //     }
    //   });
    // }
  });

  //Product registration form ajax complete
  $(document).ajaxComplete(function(event, xhr, settings) {
    var getUrl = settings.url;
    var removebutton = $('.webform-multiple-table .parent_product_sku_purchase_date_table_remove .image-button');
    if(removebutton.length === 1) {
      $('.webform-multiple-table .parent_product_sku_purchase_date_table_remove .image-button').addClass('removecrossbutton');
    }

    if(getUrl.indexOf('sku-autocomplete')) {
      $('.auto-suggest .model-num-list').on('click','.row-sku',function() {

        $('.product-model-number').parent().toggleClass('auto-suggest');
        $('.ui-autocomplete-input').removeClass('ui-autocomplete-loading');
      });
    }
  });

  //CCPA Form to make the popup appended to Body
  $(window).on('load',function() {
    $('.path-form-ccpa-requests #edit-validated-message').appendTo('body');
  });

    //CCPA Form radio button and check box add class
    $('.delta-ccpa-form #edit-residency').addClass('requiredField');
    $('.delta-ccpa-form #edit-type-of-request').addClass('requiredField');
    $('.delta-ccpa-form #edit-user-association-checkboxes').addClass('requiredField');

    //CCPA California Resident Radio button validation
    $('.delta-ccpa-form input[type=radio]').on('change',function() {
      if($('#edit-residency-ca').prop('checked')) {
        $(this).addClass('validated');

        if($('#edit-validated-message').hasClass('no-for-california') || $('#edit-validated-message').hasClass('popupmessage')) {
          $('#edit-validated-message').removeClass('no-for-california');
          $('#edit-validated-message').removeClass('popupmessage')
        }
      }
      else if($('#edit-residency-other').prop('checked')) {
        $('#edit-validated-message').addClass('no-for-california');
        $('main').toggleClass('popupopen');
        $('.delta-ccpa-form .form-submit').prop('disabled',true);
        $('#edit-residency').next('.form-item--error-message').remove();

        if($('#edit-residency-ca').hasClass('validated')) {
          $('#edit-residency-ca').removeClass('validated');
        }

        if($('#edit-validated-message').hasClass('popupmessage')) {
          $('#edit-validated-message').removeClass('popupmessage')
        }
      }
    });

    //CCPA Form State Validation only for California
    $('.delta-ccpa-form #edit-state').on('change',function() {
      $(this).removeClass('valid');
      if($(this).val() === 'CA') {
        $(this).addClass('required valid');
        $(this).parents('.form-item').addClass('is-filled');
        if($('#edit-validated-message').hasClass('no-for-california')) {
          $('#edit-validated-message').removeClass('no-for-california');
        }
      }else if ($(this).val() === '') {
        if($(this).val() === '' ) {
          $(this).addClass('required');
        }else {
          $(this).removeClass('required');
        }
      }
      else {
          $('.delta-ccpa-form .form-submit').prop('disabled',true);
          $('main').toggleClass('popupopen');
          $(this).removeClass('required valid');
          $('#edit-validated-message').removeClass('popupmessage').addClass('no-for-california');
      }
    });

    //CCPA Form for the close button of popup
    $('.path-form-ccpa-requests').on('click','.webform-message__link',function() {
      $('#edit-validated-message').removeClass('no-for-california').addClass('popupmessage');
      $('main').removeClass('popupopen');
    });

    //CCPA Form Validation for the Submit Button to get enabled
    $('.delta-ccpa-form .required, .delta-ccpa-form .requiredField').on('keyup change input paste dp.change',function() {
      $(this).val() != "" ? jQuery(this).addClass('valid') : jQuery(this).removeClass('valid');

      if($('#edit-user-association-checkboxes').find('input[type=checkbox]:checked').length > 0) {
        $('#edit-user-association-checkboxes').addClass('validated');
      }
      else {
        $('#edit-user-association-checkboxes').removeClass('validated');
      }

      //CCPA Form for the Type of Request Validation
      if($('#edit-type-of-request').find('input[type=checkbox]:checked').length > 0) {
        $('#edit-type-of-request').addClass('validated');
      }
      else {
        $('#edit-type-of-request').removeClass('validated');
      }

      if(($('.delta-ccpa-form .required').length === $('.valid').length) && ($('.delta-ccpa-form .requiredField').length === $('.validated').length)) {
        $('.delta-ccpa-form .form-submit').prop('disabled',false);
      }
      else {
        $('.delta-ccpa-form .form-submit').prop('disabled',true);
      }
    });

    $('.product-registration-sweepstakes-form').on("keyup change input paste dp.change",'.required' ,function() {
        $(this).val() != "" ? jQuery(this).addClass('valid') : jQuery(this).removeClass('valid');

        if($('.product-registration-sweepstakes-form .required').length === jQuery('.valid').length) {
          $('.product-registration-sweepstakes-form .product-reg-submit').prop('disabled', false);
        }
        else {
          $('.product-registration-sweepstakes-form .product-reg-submit').prop('disabled', true);
        }

    });

    //State Country dropdown
    var USStates = ["AL", "AK", "AZ", "AR", "CA", "CO", "CT", "DE", "FL", "GA", "HI",
    "ID", "IL", "IN", "IA", "KS", "KY", "LA", "ME", "MD", "MA", "MI", "MN", "MS",
    "MO", "MT", "NE", "NV", "NH", "NJ", "NM", "NY", "NC", "ND", "OH", "OK",
    "OR", "PA", "RI", "SC", "SD", "TN", "TX", "UT", "VT", "VA", "WA",
    "WV", "WI", "WY"];

    var CanadaStates = ["AB", "BC", "MB", "NB", "NF", "NT", "NS", "NU",
     "ON", "PE", "QC", "SK", "YT"];

    var PuertoRico = ["PR"];
    var VirginIslands = ["VI"];
    var Guam = ["GU"];

    var otherStates = ["INT","OT"];

    $(document).on('change','.statedependent',function() {
      //$(this).next('p.form-item--error-message').remove();
    //  $(".countrydependent#edit-country option").bind('touchmove', true);
      $(".countrydependent option").attr("disabled",true);
      $(".countrydependent option").attr("selected",false);
      $('.countrydependent option[value=""]').attr("disabled",false);
      $('.countrydependent').parents('.form-item').find('label').show();
      $('.countrydependent').parents('.form-item').addClass('has-focus is-filled');
      var selectedState = $(".statedependent option:selected" ).val();
      if(jQuery.inArray(selectedState, USStates) !== -1) {
        $('.countrydependent').val('USA');
        $('.countrydependent option[value="USA"]').attr("disabled",false);
        $('.countrydependent option[value="USA"]').parent().addClass('valid');
        // This is for only contact Us form.
        if($('.countrydependent').next().find('p.form-item--error-message')) {
          $('.countrydependent').next('p.form-item--error-message').remove();
          $('.countrydependent').parent().removeClass("form-item--error");
        }
      }else if (jQuery.inArray(selectedState, CanadaStates) !== -1) {
        $('.countrydependent').val('Canada');
        $('.countrydependent option[value="Canada"]').attr("disabled",false);
        $('.countrydependent option[value="Canada"]').parent().addClass('valid');
        if($('.countrydependent').next().find('p.form-item--error-message')) {
          $('.countrydependent').next('p.form-item--error-message').remove();
          $('.countrydependent').parent().removeClass("form-item--error");
        }
      }else if (jQuery.inArray(selectedState, PuertoRico) !== -1) {
        $('.countrydependent').val('PR');
        $('.countrydependent option[value="PR"]').attr("disabled",false);
        $('.countrydependent option[value="PR"]').parent().addClass('valid');
        if($('.countrydependent').next().find('p.form-item--error-message')) {
          $('.countrydependent').next('p.form-item--error-message').remove();
          $('.countrydependent').parent().removeClass("form-item--error");
        }
      }else if (jQuery.inArray(selectedState, VirginIslands) !== -1) {
        $('.countrydependent').val('VI');
        $('.countrydependent option[value="VI"]').attr("disabled",false);
        $('.countrydependent option[value="VI"]').parent().addClass('valid');
        if($('.countrydependent').next().find('p.form-item--error-message')) {
          $('.countrydependent').next('p.form-item--error-message').remove();
          $('.countrydependent').parent().removeClass("form-item--error");
        }
      }else if (jQuery.inArray(selectedState, Guam) !== -1) {
        $('.countrydependent').val('GU');
        $('.countrydependent option[value="GU"]').attr("disabled",false);
        $('.countrydependent option[value="GU"]').parent().addClass('valid');
        if($('.countrydependent').next().find('p.form-item--error-message')) {
          $('.countrydependent').next('p.form-item--error-message').remove();
          $('.countrydependent').parent().removeClass("form-item--error");
        }
      }else if(jQuery.inArray(selectedState, otherStates) !== -1) {
        $('.countrydependent').val('Other');
        $('.countrydependent option[value="Other"]').attr("disabled",false);
        $('.countrydependent option[value="Other"]').parent().addClass('valid');
        if($('.countrydependent').next().find('p.form-item--error-message')) {
          $('.countrydependent').next('p.form-item--error-message').remove();
          $('.countrydependent').parent().removeClass("form-item--error");
        }
      }else {
      //  $(this).after('<p class="form-item--error-message">Please select State</p>');
        var label = $($("label[for='" + this.id + "']").contents().get(0)).text();
        $('.contact-form .countrydependent').after("<p class='form-item--error-message'> "+ label + " field is required.</p>");
        $('.contact-form .countrydependent').parent().addClass('form-item--error')
        $('.countrydependent').val('');
        $(".countrydependent option").attr("disabled",false);
        $('.countrydependent option[value=""]').attr("disabled",false).parent().removeClass('valid');
      //  $('.countrydependent').parents('.form-item').removeClass('has-focus is-filled');
        $('.countrydependent').siblings('.form-item__label').toggle();
      }
    });

    // New ContactUs form Country selection
    $(document).on('change','.statedependent-contactus',function() {
      $(".countrydependent option").attr("disabled",true);
      $(".countrydependent option").attr("selected",false);
      $('.countrydependent option[value=""]').attr("disabled",false);
      $('.countrydependent').parents('.form-item').find('label').show();
      $('.countrydependent').parents('.form-item').addClass('has-focus is-filled');
      var selectedState = $(".statedependent-contactus option:selected" ).val();
      if(jQuery.inArray(selectedState, USStates) !== -1) {
        $('.countrydependent').val('US');
        $('.countrydependent option[value="US"]').attr("disabled",false);
        $('.countrydependent option[value="US"]').parent().addClass('valid');
        // This is for only contact Us form.
        if($('.countrydependent').next().find('p.form-item--error-message')) {
          $('.countrydependent').next('p.form-item--error-message').remove();
          $('.countrydependent').parent().removeClass("form-item--error");
        }
      }else if (jQuery.inArray(selectedState, CanadaStates) !== -1) {
        $('.countrydependent').val('CA');
        $('.countrydependent option[value="CA"]').attr("disabled",false);
        $('.countrydependent option[value="CA"]').parent().addClass('valid');
        if($('.countrydependent').next().find('p.form-item--error-message')) {
          $('.countrydependent').next('p.form-item--error-message').remove();
          $('.countrydependent').parent().removeClass("form-item--error");
        }
      }else if (jQuery.inArray(selectedState, PuertoRico) !== -1) {
        $('.countrydependent').val('PR');
        $('.countrydependent option[value="PR"]').attr("disabled",false);
        $('.countrydependent option[value="PR"]').parent().addClass('valid');
        if($('.countrydependent').next().find('p.form-item--error-message')) {
          $('.countrydependent').next('p.form-item--error-message').remove();
          $('.countrydependent').parent().removeClass("form-item--error");
        }
      }else if (jQuery.inArray(selectedState, VirginIslands) !== -1) {
        $('.countrydependent').val('VI');
        $('.countrydependent option[value="VI"]').attr("disabled",false);
        $('.countrydependent option[value="VI"]').parent().addClass('valid');
        if($('.countrydependent').next().find('p.form-item--error-message')) {
          $('.countrydependent').next('p.form-item--error-message').remove();
          $('.countrydependent').parent().removeClass("form-item--error");
        }
      }else if (jQuery.inArray(selectedState, Guam) !== -1) {
        $('.countrydependent').val('GU');
        $('.countrydependent option[value="GU"]').attr("disabled",false);
        $('.countrydependent option[value="GU"]').parent().addClass('valid');
        if($('.countrydependent').next().find('p.form-item--error-message')) {
          $('.countrydependent').next('p.form-item--error-message').remove();
          $('.countrydependent').parent().removeClass("form-item--error");
        }
      }else if(jQuery.inArray(selectedState, otherStates) !== -1) {
        $('.countrydependent').val('OT');
        $('.countrydependent option[value="OT"]').attr("disabled",false);
        $('.countrydependent option[value="OT"]').parent().addClass('valid');
        if($('.countrydependent').next().find('p.form-item--error-message')) {
          $('.countrydependent').next('p.form-item--error-message').remove();
          $('.countrydependent').parent().removeClass("form-item--error");
        }
      }else {
      //  $(this).after('<p class="form-item--error-message">Please select State</p>');
        var label = $($("label[for='" + this.id + "']").contents().get(0)).text();
        $('.contact-form .countrydependent').after("<p class='form-item--error-message'> "+ label + " field is required.</p>");
        $('.contact-form .countrydependent').parent().addClass('form-item--error')
        $('.countrydependent').val('');
        $(".countrydependent option").attr("disabled",false);
        $('.countrydependent option[value=""]').attr("disabled",false).parent().removeClass('valid');
      //  $('.countrydependent').parents('.form-item').removeClass('has-focus is-filled');
        $('.countrydependent').siblings('.form-item__label').toggle();
      }
    });

    var countryStateInfo = {
      "USA": [
        "AL", "AK", "AZ", "AR", "CA", "CO", "CT", "DE", "FL", "GA", "HI",
        "ID", "IL", "IN", "IA", "KS", "KY", "LA", "ME", "MD", "MA", "MI", "MN", "MS",
        "MO", "MT", "NE", "NV", "NH", "NJ", "NM", "NY", "NC", "ND", "OH", "OK",
        "OR", "PA", "RI", "SC", "SD", "TN", "TX", "UT", "VT", "VA", "WA",
        "WV", "WI", "WY"
      ],
      "Canada" : [
        "AB", "BC", "MB", "NB", "NF", "NT", "NS", "NU","ON", "PE", "QC", "SK", "YT"
      ],
      "Other" : [
        "INT","OT"
      ]
    }

    $(".countrydependent").on("change", function() {
      var selectedcountry = $(this).val();
      $(".statedependent option").hide();
      $.each($(".statedependent option"), function(key, option) {
         if(jQuery.inArray($(option).val(), countryStateInfo[selectedcountry]) >= 0) {
          $(option).show();
        }else if (selectedcountry == '') {
          $('.statedependent option').show();
        }
      })
    });

    //Contact Us form Realtime Validation
    $(".contact-form .required").on("change input", function(e) {
      var nameregex = Drupal.behaviors.commonvalidationfunction.usernameregex(),
          emailregex = Drupal.behaviors.commonvalidationfunction.emailidregex(),
          phoneregex = Drupal.behaviors.commonvalidationfunction.phonenumberregex(),
          address = Drupal.behaviors.commonvalidationfunction.addressreg(),
          city = Drupal.behaviors.commonvalidationfunction.cityvalidation(),
          zip_code = Drupal.behaviors.commonvalidationfunction.zipcodevalidation(),
          value = 0;

      var inputValue = $(this).val(),
          label = $($("label[for='" + this.id + "']").contents().get(0)).text(),
          element = $(this);

      //Name field
      var validatefunction = setTimeout(function() {
        if((element.val()) !== "") {
          element.next('p.form-item--error-message').remove();
          element.parent().removeClass('form-item--error');
        }

        if(element.hasClass("validate-name")) {
          $('.contact-form').find(".validate-name").removeClass("form-item--error");
          var inputValue = element.val();
          if( inputValue == "" ) {
            element.next('p.form-item--error-message').remove();
            element.after("<p class='form-item--error-message'> "+ label + " field is required.</p>");
            element.parent().addClass("form-item--error");
            value++;
          } else if(!inputValue.match(nameregex)) {
            element.next('p.form-item--error-message').remove();
            element.after("<p class='form-item--error-message'>Please enter a valid " + label + ".</p>");
            element.parent().addClass("form-item--error");
            value++;
          } else {
            element.next('p.form-item--error-message').remove();
            element.parent().removeClass("form-item--error");
            value--;
          }
        }

        //Email Address
        if(element.hasClass("validate-email")) {
          $('.contact-form').find(".validate-email").removeClass("form-item--error");
          var inputValue = element.val();
          if( inputValue == "" ) {
            element.next('p.form-item--error-message').remove();
            element.after("<p class='form-item--error-message'> "+ label + " field is required.</p>");
            element.parent().addClass("form-item--error");
            value++;
          } else if(!inputValue.match(emailregex)) {
            element.next('p.form-item--error-message').remove();
            element.after("<p class='form-item--error-message'>Please enter a valid " + label + ".</p>");
            element.parent().addClass("form-item--error");
            value++;
          } else {
            element.next('p.form-item--error-message').remove();
            element.parent().removeClass("form-item--error");
            value--;
          }
        };
        /* if(element.hasClass("melliansa-validation")) {
          $('.contact-form').find(".melliansa-validation").removeClass("form-item--error");
          var emailID = element.val();
          mellinsa = Drupal.behaviors.commonvalidationfunction.searchEmail(emailID, element);
        }; */

        //PhoneNumber validation
        if(element.hasClass("validate-phone")) {
          $('.contact-form').find(".validate-phone").removeClass("form-item--error");
          var inputValue = element.val();
          if( inputValue == "" ) {
            element.next('p.form-item--error-message').remove();
            element.after("<p class='form-item--error-message'> "+ label + " field is required.</p>");
            element.parent().addClass("form-item--error");
            value++;
          } else if(!inputValue.match(phoneregex)) {
            element.next('p.form-item--error-message').remove();
            element.after("<p class='form-item--error-message'>Please enter a valid " + label + ".</p>");
            element.parent().addClass("form-item--error");
            value++;
          } else {
            element.next('p.form-item--error-message').remove();
            element.parent().removeClass("form-item--error");
            value--;
          }
        };

        //Address field
        if(element.hasClass("validate-address")) {
          $('.contact-form').find(".validate-address").removeClass("form-item--error");
          var inputValue = element.val();
          if( inputValue == "" ) {
            element.next('p.form-item--error-message').remove();
            element.after("<p class='form-item--error-message'> "+ label + " field is required.</p>");
            element.parent().addClass("form-item--error");
            value++;
          } else if(!inputValue.match(address)) {
            element.next('p.form-item--error-message').remove();
            element.after("<p class='form-item--error-message'>Please enter a valid " + label + ".</p>");
            element.parent().addClass("form-item--error");
            value++;
          } else {
            element.next('p.form-item--error-message').remove();
            element.parent().removeClass("form-item--error");
            value--;
          }
        };

        //City
        if(element.hasClass("validate-city")) {
          $('.contact-form').find(".validate-city").removeClass("form-item--error");
          var inputValue = element.val();
          if( inputValue == "" ) {
            element.next('p.form-item--error-message').remove();
            element.after("<p class='form-item--error-message'> "+ label + " field is required.</p>");
            element.parent().addClass("form-item--error");
            value++;
          } else if(!inputValue.match(city)) {
            element.next('p.form-item--error-message').remove();
            element.after("<p class='form-item--error-message'>Please enter a valid " + label + ".</p>");
            element.parent().addClass("form-item--error");
            value++;
          } else {
            element.next('p.form-item--error-message').remove();
            element.parent().removeClass("form-item--error");
            value--;
          }
        };

        //Zipcode validation
        if(element.hasClass("validate-zipcode")) {
          $('.contact-form').find(".validate-zipcode").removeClass("form-item--error");
          var inputValue = element.val();
          if( inputValue == "" ) {
            element.next('p.form-item--error-message').remove();
            element.after("<p class='form-item--error-message'> "+ label + " field is required.</p>");
            element.parent().addClass("form-item--error");
            value++;
          } else if(!inputValue.match(zip_code)) {
            element.next('p.form-item--error-message').remove();
            element.after("<p class='form-item--error-message'>Please enter a valid " + label + ".</p>");
            element.parent().addClass("form-item--error");
            value++;
          } else {
            element.next('p.form-item--error-message').remove();
            element.parent().removeClass("form-item--error");
            value--;
          }
        };

        if(element.hasClass("validate-textarea")) {
          $('.contact-form').find(".validate-textarea").removeClass("form-item--error");
          var inputValue = element.val();
          if( inputValue == "" ) {
            element.next('p.form-item--error-message').remove();
            element.after("<p class='form-item--error-message'> "+ label + " field is required.</p>");
            element.parent().addClass("form-item--error");
            value++;
          } else {
            element.next('p.form-item--error-message').remove();
            element.parent().removeClass("form-item--error");
            value--;
          }
        };

        clearTimeout(validatefunction);
      }, 3000);
    });

    $('.contact-form .required').on('change',function(){

      var element = $(this);
      var validatefunction = setTimeout(function() {
        if(element.hasClass("melliansa-validation")) {
          $('.contact-form').find(".melliansa-validation").removeClass("form-item--error");
          var emailID = element.val();
          mellinsa = Drupal.behaviors.commonvalidationfunction.searchEmail(emailID, element);
        };
        clearTimeout(validatefunction);
      },3000)

    });

    //ContactUs On click of submit button.
    $('.contact-form').on('click','.form-submit',function() {
      var value = 0;
      $('.contact-form').find(".required:visible").not(".form-radio, .shipping_option .required").each(function() {
        var inputValue = $(this).val(),
        label = $($("label[for='" + this.id + "']").contents().get(0)).text();
        if((inputValue == "") || (inputValue == 0)) {
          if($(this).next().find('p.form-item--error-message')) {
            $(this).next('p.form-item--error-message').remove();
            value++;
            $(this).parent().addClass("form-item--error");
            $(this).after("<p class='form-item--error-message'>" + label + " field is required.</p>");
          }
        }
        element = $(this);
        value = delta.commonFunctions.FormFieldTypeValidation(element, label, value);
      });

      // For purchased location select box
      var topicOption = $("#edit-select-message-topic").val();
      if(topicOption == 'replacement-parts' ||
          topicOption == 'finish' ||
          topicOption == 'cleaning-and-care') {
        var inputValue = $(".purchased-location-option").val(),
            label = $($("label[for='edit-purchase-location']").contents().get(0)).text();
        if(inputValue !== "") {
          $(".purchased-location-option").next('p.form-item--error-message').remove();
          $(".purchased-location-option").parent().removeClass('form-item--error');
        }else {
          if($(".purchased-location-option").parent().find(".form-item--error-message").length == 0) {
            $(".purchased-location-option").after("<p class='form-item--error-message'>" + label + " field is required.</p>");
          }
          $(".purchased-location-option").parent().addClass('form-item--error');
        }
      }

      if($('.contact-form .dz-error-message').find("span").text()) {
        value++;
      }

      if(grecaptcha.getResponse() == "") {
        event.preventDefault();
        $('.g-recaptcha').next('.form-item--error-message').remove();
        $('.g-recaptcha').parent().addClass("form-item--error");
        value++;
        $('.g-recaptcha').after("<p class='form-item--error-message'>Recaptcha field is required.</p>");
      } /* else {
        $('.g-recaptcha').next('.form-item--error-message').remove();
        $('.g-recaptcha').parent().removeClass("form-item--error");
        value--;
      } */

      if($('.contact-form').find("p.form-item--error-message").text()) {
        value++;
      }

      if($('.contact-form').find('.form-item--error')) {
        $("html, body").animate({
          scrollTop:  ($('.form-item--error, .dz-error-message').offset() || $(this).offset()).top - 200
        }, 500);
      }

      if(value > 0) {
        return false;
      }
    });

    $('.contact-form').on('change','.form-select',function() {
      if(!$(this).hasClass("purchased-location-option")) {
        var inputValue = $(this).val(),
            label = $($("label[for='" + this.id + "']").contents().get(0)).text();
        if(inputValue !== "") {
          $(this).next('p.form-item--error-message').remove();
          $(this).parent().removeClass('form-item--error');
        }else {
          if($(this).parent().find(".form-item--error-message").length == 0){
            $(this).after("<p class='form-item--error-message'>" + label + " field is required.</p>");
          }
          $(this).parent().addClass('form-item--error');
        }
      }
    });

    //Product Registration form Realtime Validation
    $(".product-registration-form .required").on("change input", function(e) {
      var nameregex = Drupal.behaviors.commonvalidationfunction.usernameregex(),
          emailregex = Drupal.behaviors.commonvalidationfunction.emailidregex(),
          phoneregex = Drupal.behaviors.commonvalidationfunction.phonenumberregex(),
          address = Drupal.behaviors.commonvalidationfunction.addressreg(),
          city = Drupal.behaviors.commonvalidationfunction.cityvalidation(),
          zip_code = Drupal.behaviors.commonvalidationfunction.zipcodevalidation(),
          model_number = Drupal.behaviors.commonvalidationfunction.modelnumber(),
          value = 0;

      var inputValue = $(this).val(),
          label = $(this).siblings('label').text(),
          element = $(this);

      //Name field
      var validatefunction = setTimeout(function() {
        if((element.val()) !== "") {
          element.next('p.form-item--error-message').remove();
          element.parent().removeClass('form-item--error');
        } else {
          element.next('p.form-item--error-message').remove();
          element.after("<p class='form-item--error-message'> "+ label + " field is required.</p>");
          element.parent().addClass("form-item--error");
        }

        if(element.hasClass("validate-name")) {
          $('.product-registration-form').find(".validate-name").removeClass("form-item--error");
          var inputValue = element.val();
          if( inputValue == "" ) {
            element.next('p.form-item--error-message').remove();
            element.after("<p class='form-item--error-message'> "+ label + " field is required.</p>");
            element.parent().addClass("form-item--error");
            value++;
          } else if(!inputValue.match(nameregex)) {
            element.next('p.form-item--error-message').remove();
            element.after("<p class='form-item--error-message'>Please enter a valid " + label + ".</p>");
            element.parent().addClass("form-item--error");
            value++;
          }
        }

        //Email Address
        if(element.hasClass("validate-email")) {
          $('.product-registration-form').find(".validate-email").removeClass("form-item--error");
          var inputValue = element.val();
          if( inputValue == "" ) {
            element.next('p.form-item--error-message').remove();
            element.after("<p class='form-item--error-message'> "+ label + " field is required.</p>");
            element.parent().addClass("form-item--error");
            value++;
          } else if(!inputValue.match(emailregex)) {
            element.next('p.form-item--error-message').remove();
            element.after("<p class='form-item--error-message'>Please enter a valid " + label + ".</p>");
            element.parent().addClass("form-item--error");
            value++;
          }
        };

        //PhoneNumber validation
        if(element.hasClass("validate-phone")) {
          $('.product-registration-form').find(".validate-phone").removeClass("form-item--error");
          var inputValue = element.val();
          if( inputValue == "" ) {
            element.next('p.form-item--error-message').remove();
            element.after("<p class='form-item--error-message'> "+ label + " field is required.</p>");
            element.parent().addClass("form-item--error");
            value++;
          } else if(!inputValue.match(phoneregex)) {
            element.next('p.form-item--error-message').remove();
            element.after("<p class='form-item--error-message'>Please enter a valid " + label + ".</p>");
            element.parent().addClass("form-item--error");
            value++;
          }
        };

        //Address field
        if(element.hasClass("validate-address")) {
          $('.product-registration-form').find(".validate-address").removeClass("form-item--error");
          var inputValue = element.val();
          if( inputValue == "" ) {
            element.next('p.form-item--error-message').remove();
            element.after("<p class='form-item--error-message'> "+ label + " field is required.</p>");
            element.parent().addClass("form-item--error");
            value++;
          } else if(!inputValue.match(address)) {
            element.next('p.form-item--error-message').remove();
            element.after("<p class='form-item--error-message'>Please enter a valid " + label + ".</p>");
            element.parent().addClass("form-item--error");
            value++;
          }
        };

        //City
        if(element.hasClass("validate-city")) {
          $('.product-registration-form').find(".validate-city").removeClass("form-item--error");
          var inputValue = element.val();
          if( inputValue == "" ) {
            element.next('p.form-item--error-message').remove();
            element.after("<p class='form-item--error-message'> "+ label + " field is required.</p>");
            element.parent().addClass("form-item--error");
            value++;
          } else if(!inputValue.match(city)) {
            element.next('p.form-item--error-message').remove();
            element.after("<p class='form-item--error-message'>Please enter a valid " + label + ".</p>");
            element.parent().addClass("form-item--error");
            value++;
          }
        };

        //Zipcode validation
        if(element.hasClass("validate-zipcode")) {
          $('.product-registration-form').find(".validate-zipcode").removeClass("form-item--error");
          var inputValue = element.val();
          if( inputValue == "" ) {
            element.next('p.form-item--error-message').remove();
            element.after("<p class='form-item--error-message'> "+ label + " field is required.</p>");
            element.parent().addClass("form-item--error");
            value++;
          } else if(!inputValue.match(zip_code)) {
            element.next('p.form-item--error-message').remove();
            element.after("<p class='form-item--error-message'>Please enter a valid " + label + ".</p>");
            element.parent().addClass("form-item--error");
            value++;
          }
        };

        if(element.hasClass("validate-textarea")) {
          $('.product-registration-form').find(".validate-textarea").removeClass("form-item--error");
          var inputValue = element.val();
          if( inputValue == "" ) {
            element.next('p.form-item--error-message').remove();
            element.after("<p class='form-item--error-message'> "+ label + " field is required.</p>");
            element.parent().addClass("form-item--error");
            value++;
          }
        };

        clearTimeout(validatefunction);
      }, 3000);
    });

    $('.product-registration-form .required').on('change',function(){
      var element = $(this);
      var validatefunction = setTimeout(function() {
        if(element.hasClass("melliansa-validation")) {
          $('.product-registration-form').find(".melliansa-validation").removeClass("form-item--error");
          var emailID = element.val();
          mellinsa = Drupal.behaviors.commonvalidationfunction.searchEmail(emailID, element);
        };
        clearTimeout(validatefunction);
      },3000)

    });

    //Product Registration On click of submit button
    $('.product-registration-form').on('click','.form-submit',function() {
      var value = 0;
      $('.product-registration-form').find(".required:visible").not(".form-radio, .shipping_option .required").each(function() {
        var inputValue = $(this).val(),
        label = $(this).siblings('label').text();
        if((inputValue == "") || (inputValue == 0)) {
          if($(this).next().find('p.form-item--error-message')) {
            $(this).next('p.form-item--error-message').remove();
            value++;
            $(this).parent().addClass("form-item--error");
            $(this).after("<p class='form-item--error-message'>" + label + " field is required.</p>");
          }
        }
        element = $(this);
        value = delta.commonFunctions.FormFieldTypeValidation(element, label, value);
      });

      if(grecaptcha.getResponse() == "") {
        event.preventDefault();
        $('.g-recaptcha').next('.form-item--error-message').remove();
        $('.g-recaptcha').parent().addClass("form-item--error");
        value++;
        $('.g-recaptcha').after("<p class='form-item--error-message'>Recaptcha field is required.</p>");
      } /* else {
        $('.g-recaptcha').next('.form-item--error-message').remove();
        $('.g-recaptcha').parent().removeClass("form-item--error");
        value--;
      } */

      if($('.product-registration-form').find("p.form-item--error-message").text()) {
        value++;
      }

      if($('.product-registration-form').find('.form-item--error')) {
        $("html, body").animate({
          scrollTop:  ($('.form-item--error').offset() || { "top": NaN }).top - 200
        }, 500);
      }

      if(value > 0) {
        return false;
      }
    });

    $('.product-registration-form').on('change','.form-select',function() {
      var inputValue = $(this).val(),
          label = $($("label[for='" + this.id + "']").contents().get(0)).text();
      if(inputValue !== "") {
        $(this).next('p.form-item--error-message').remove();
        $(this).parent().removeClass('form-item--error');
      }else {
        $(this).after("<p class='form-item--error-message'>" + label + " field is required.</p>");
        $(this).parent().addClass('form-item--error');
      }
    });

    $(".contact-form-newsletter .required").on("change input", function(e) {
      var nameregex  = Drupal.behaviors.commonvalidationfunction.usernameregex(),
          emailregex = Drupal.behaviors.commonvalidationfunction.emailidregex(),
          value = 0;

          //var inputValue = $(this).val(),
          label = $(this).parent().find('label').text(),
          element = $(this);

      //Name field
      var validatefunction = setTimeout(function() {
        if((element.val()) !== "") {
          element.next('p.form-item--error-message').remove();
          element.parent().removeClass('form-item--error');
        } else {
          element.next('p.form-item--error-message').remove();
          element.after("<p class='form-item--error-message'> "+ label + " field is required.</p>");
          element.parent().addClass("form-item--error");
        }

        if(element.hasClass("validate-name")) {
          $('.contact-form-newsletter').find(".validate-name").removeClass("form-item--error");
          var inputValue = element.val();
          if( inputValue == "" ) {
            element.next('p.form-item--error-message').remove();
            element.after("<p class='form-item--error-message'> "+ label + " field is required.</p>");
            element.parent().addClass("form-item--error");
            value++;
          } else if(!inputValue.match(nameregex)) {
            element.next('p.form-item--error-message').remove();
            element.after("<p class='form-item--error-message'>Please enter a valid " + label + ".</p>");
            element.parent().addClass("form-item--error");
            value++;
          }
        }

        //Email Address
        if(element.hasClass("validate-email")) {
          $('.contact-form-newsletter').find(".validate-email").removeClass("form-item--error");
          var inputValue = element.val();
          if( inputValue == "" ) {
            element.next('p.form-item--error-message').remove();
            element.after("<p class='form-item--error-message'> "+ label + " field is required.</p>");
            element.parent().addClass("form-item--error");
            value++;
          } else if(!inputValue.match(emailregex)) {
            element.next('p.form-item--error-message').remove();
            element.after("<p class='form-item--error-message'>Please enter a valid " + label + ".</p>");
            element.parent().addClass("form-item--error");
            value++;
          }
        };

        clearTimeout(validatefunction);
      }, 3000);

    });

    $('.contact-form-newsletter .required').on('change',function(){
      var element = $(this);
      var validatefunction = setTimeout(function() {
        if(element.hasClass("melliansa-validation")) {
          $('.contact-form-newsletter').find(".melliansa-validation").removeClass("form-item--error");
          var emailID = element.val();
          mellinsa = Drupal.behaviors.commonvalidationfunction.searchEmail(emailID, element);
        };
        clearTimeout(validatefunction);
      },3000)

    });

    $(".contact-form-newsletter .webform-button--submit").click(function(e) {
      var value = 0;

      $(".contact-form-newsletter").find(".required:visible").each(function() {
        element = $(this);
        label = $(this).parent().find('label').text();
        value = delta.commonFunctions.FormFieldTypeValidation(element, label, value);
      });

      if(grecaptcha.getResponse() == "") {
        event.preventDefault();
        $('.g-recaptcha').next('.form-item--error-message').remove();
        $('.g-recaptcha').parent().addClass("form-item--error");
        value++;
        $('.g-recaptcha').after("<p class='form-item--error-message'>Recaptcha field is required.</p>");
      }

      if($(".contact-form-newsletter").find("p.form-item--error-message").text()) {
        value++;
      }

      if($('.contact-form-newsletter').find('.form-item--error')) {
        $("html, body").animate({
          scrollTop:  ($('.form-item--error').offset() || { "top": NaN }).top - 200
        }, 500);
      }

      if(value > 0){
        return false;
      }
    });


    $('.company-media-page .pr-search-input-wrapper .serach-input').keypress(function (e) {
      var key = e.which;
      if(key == 13)  // the enter key code
      {
        $('.company-media-page .serach-input').toggleClass('media-search');
      }
    });

    $('.company-media-page .pr-search-input-wrapper').on('click', '.serach-input.media-search',function() {
      $('.company-media-page .serach-input').val("");
      $('.company-media-page .serach-input').removeClass('media-search');
    });

  })(jQuery, Drupal);

// Form Validation
var delta = {};

(function ($, _Drupal) {
    delta.commonFunctions = {
      FormFieldTypeValidation: function(element, label, value){
        var nameregex  = Drupal.behaviors.commonvalidationfunction.usernameregex(),
            emailregex = Drupal.behaviors.commonvalidationfunction.emailidregex(),
            phoneregex = Drupal.behaviors.commonvalidationfunction.phonenumberregex(),
            address = Drupal.behaviors.commonvalidationfunction.addressreg(),
            city = Drupal.behaviors.commonvalidationfunction.cityvalidation(),
            zip_code = Drupal.behaviors.commonvalidationfunction.zipcodevalidation();

        if(element.hasClass("validate-name")) {
          $('.contact-form').find(".validate-name").removeClass("form-item--error");
          var inputValue = element.val();
          if( inputValue == "" ) {
            element.next('p.form-item--error-message').remove();
            element.after("<p class='form-item--error-message'> "+ label + " field is required.</p>");
            element.parent().addClass("form-item--error");
            value++;
          } else if(!inputValue.match(nameregex)) {
            element.next('p.form-item--error-message').remove();
            element.after("<p class='form-item--error-message'>Please enter a valid " + label + ".</p>");
            element.parent().addClass("form-item--error");
            value++;
          }
        }

        if(element.hasClass("validate-email")) {
          $('.contact-form').find(".validate-email").removeClass("form-item--error");
          var inputValue = element.val();
          if( inputValue == "" ) {
            element.next('p.form-item--error-message').remove();
            element.after("<p class='form-item--error-message'> "+ label + " field is required.</p>");
            element.parent().addClass("form-item--error");
            value++;

          } else if(!inputValue.match(emailregex)) {
            element.next('p.form-item--error-message').remove();
            element.after("<p class='form-item--error-message'>Please enter a valid " + label + ".</p>");
            element.parent().addClass("form-item--error");
            value++;
          }
        }

        //PhoneNumber validation
        if(element.hasClass("validate-phone")) {
          $('.contact-form').find(".validate-phone").removeClass("form-item--error");
          var inputValue = element.val();
          if( inputValue == "" ) {
            element.next('p.form-item--error-message').remove();
            element.after("<p class='form-item--error-message'> "+ label + " field is required.</p>");
            element.parent().addClass("form-item--error");
            value++;
          } else if(!inputValue.match(phoneregex)) {
            element.next('p.form-item--error-message').remove();
            element.after("<p class='form-item--error-message'>Please enter a valid " + label + ".</p>");
            element.parent().addClass("form-item--error");
            value++;
          }
        }

        //Address field
        if(element.hasClass("validate-address")) {
          $('.contact-form').find(".validate-address").removeClass("form-item--error");
          var inputValue = element.val();
          if( inputValue == "" ) {
            element.next('p.form-item--error-message').remove();
            element.after("<p class='form-item--error-message'> "+ label + " field is required.</p>");
            element.parent().addClass("form-item--error");
            value++;
          } else if(!inputValue.match(address)) {
            element.next('p.form-item--error-message').remove();
            element.after("<p class='form-item--error-message'>Please enter a valid " + label + ".</p>");
            element.parent().addClass("form-item--error");
            value++;
          }
        }

        //City
        if(element.hasClass("validate-city")) {
          $('.contact-form').find(".validate-city").removeClass("form-item--error");
          var inputValue = element.val();
          if( inputValue == "" ) {
            element.next('p.form-item--error-message').remove();
            element.after("<p class='form-item--error-message'> "+ label + " field is required.</p>");
            element.parent().addClass("form-item--error");
            value++;
          } else if(!inputValue.match(city)) {
            element.next('p.form-item--error-message').remove();
            element.after("<p class='form-item--error-message'>Please enter a valid " + label + ".</p>");
            element.parent().addClass("form-item--error");
            value++;
          }
        }

        //Zipcode validation
        if(element.hasClass("validate-zipcode")) {
          $('.contact-form').find(".validate-zipcode").removeClass("form-item--error");
          var inputValue = element.val();
          if( inputValue == "" ) {
            element.next('p.form-item--error-message').remove();
            element.after("<p class='form-item--error-message'> "+ label + " field is required.</p>");
            element.parent().addClass("form-item--error");
            value++;
          } else if(!inputValue.match(zip_code)) {
            element.next('p.form-item--error-message').remove();
            element.after("<p class='form-item--error-message'>Please enter a valid " + label + ".</p>");
            element.parent().addClass("form-item--error");
            value++;
          }
        }
        return value;
      },

      FormValidation: function(Form) {
        var value = 0,
            regex = Drupal.behaviors.commonvalidationfunction.emailidregex(),
            phone_number = Drupal.behaviors.commonvalidationfunction.phonenumberregex(),
            zip_code = Drupal.behaviors.commonvalidationfunction.zipcodevalidation(),
            alphanumeric = /^[a-zA-Z'.\s]{1,40}$/,
            pass_regex = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{8,15}$/,
            name = Drupal.behaviors.commonvalidationfunction.usernameregex(),
            address= Drupal.behaviors.commonvalidationfunction.addressreg();

            return $(Form).find(".form-item--error-message").remove(),
            $(Form).find(".required, .validate-email").parent().removeClass("form-item--error"),


            $(Form).find(".required:visible").not(".form-radio, .shipping_option .required").each(function() {
                   var inputValue = $(this).val(),
                       label = $($("label[for='" + this.id + "']").contents().get(0)).text();
                   "" != inputValue && "_none" != inputValue && 0 != inputValue || (value++, $(this).parent().addClass("form-item--error"),
                       $(this).after("<p class='form-item--error-message'>" + label + " field is required.</p>"));

                       if($('.checkboxes--wrapper').hasClass('required')) {
                         $('.checkboxes--wrapper').next('p.form-item--error-message').remove();
                         if(!$('.form-checkbox').is(":checked")) {
                           $('.checkboxes--wrapper').next('p.form-item--error-message').remove();
                           var error_message = $('.checkboxes--wrapper').attr('data-webform-required-error');
                           $('.checkboxes--wrapper').after('<p class="form-item--error-message">'+ error_message + '</p>');
                           value++;
                         } else {
                           $('.checkboxes--wrapper').next('p.form-item--error-message').remove();
                           $(this).parent().removeClass('form-item--error');
                           value--;
                         }
                       }

               }),
        $(Form).find("#edit-new-password .required").each(function() {
                $(this).val();
                var inputValue1 = $("input[name*='new_password[pass1]']").val(),
                    inputValue2 = $("input[name*='new_password[pass2]']").val();
                $($("label[for='" + this.id + "']").contents().get(0)).text();
                "" != inputValue1 && "" != inputValue2 && inputValue1 !== inputValue2 && (value++,
                    $(this).addClass("form-item--error"), $("#edit-new-password-pass2").after("<p class='form-item--error-message confirm-password'>Your password and confirm password do not match</p>"));
            }),
            $(Form).find(".password.required").each(function() {
              var inputValue1 = $(".new_pass").val(),
                  inputValue2 = $(".check__confirm_pass").val();

                  if(!inputValue1.match(pass_regex) && inputValue1 != ""){
                    $(this).parent().addClass("form-item--error");
                    $(".new_pass").after("<p class='form-item--error-message password-validation'>Password should have atleast minimum 8 characters, 1 digit and 1 special character and 1 capital letter.</p>");
                  }else if(inputValue1 !== inputValue2){
                    $(this).parent().addClass("form-item--error");
                     $(".check__confirm_pass").after("<p class='form-item--error-message confirm-password'>Your password and confirm password do not match</p>");
                  }

                  $($("label[for='" + this.id + "']").contents().get(0)).text();
              // "" == inputValue1.match(pass_regex) ? "" != inputValue1 && "" != inputValue2 && inputValue1 !== inputValue2 && (value++,
              //     $(this).parent().addClass("form-item--error"), $(".check__confirm_pass").after("<p class='form-item--error-message confirm-password'>Your password and confirm password do not match</p>")) : (value++,
              //     $(this).parent().addClass("form-item--error"), $(".check__confirm_pass").after("<p class='form-item--error-message password-validation'>Password should have atleast minimum 8 characters, 1 digit and 1 special character.</p>"));
          }),

        $(Form).find(".validate-phone").each(function() {
                $(Form).find(".validate-phone").removeClass("form-item--error");
                var inputValue = $(this).val();
                $($("label[for='" + this.id + "']").contents().get(0)).text();
                "" == inputValue || inputValue.match(phone_number) || (value++, $(this).after("<p class='form-item--error-message'>Please enter a valid 10 digit phone number(xxxxxxxxxx).</p>"),
                    $(this).parent().addClass("form-item--error"));
            }),
        $(Form).find(".validate-name").each(function() {
            $(Form).find(".validate-name").removeClass("form-item--error");
            var inputValue = $(this).val();
            $($("label[for='" + this.id + "']").contents().get(0)).text();
            "" == inputValue || inputValue.match(name) || (value++, $(this).after("<p class='form-item--error-message'>Please enter a valid Name.</p>"),
                $(this).parent().addClass("form-item--error"));
        }),

        $(Form).find(".validate-city").each(function() {
            $(Form).find(".validate-city").removeClass("form-item--error");
            var inputValue = $(this).val();
            $($("label[for='" + this.id + "']").contents().get(0)).text();
            "" == inputValue || inputValue.match(city) || (value++, $(this).after("<p class='form-item--error-message'>Please enter a valid City.</p>"),
                $(this).parent().addClass("form-item--error"));
        }),

        $(Form).find(".validate-address").each(function() {
            $(Form).find(".validate-address").removeClass("form-item--error");
            var inputValue = $(this).val();
            $($("label[for='" + this.id + "']").contents().get(0)).text();
            "" == inputValue || inputValue.match(address) || (value++, $(this).after("<p class='form-item--error-message'>Please enter a valid Address.</p>"),
                $(this).parent().addClass("form-item--error"));
        }),

        $(Form).find(".validate-zipcode").each(function() {
                $(Form).find(".validate-zipcode").removeClass("form-item--error");
                var inputValue = $(this).val();
                $($("label[for='" + this.id + "']").contents().get(0)).text();
                "" == inputValue || inputValue.match(zip_code) || (value++, $(this).after("<p class='form-item--error-message'>Zip code field is not valid.</p>"),
                    $(this).parent().addClass("form-item--error"));
            }),
        $(Form).find(".validate-email").each(function() {
                var inputValue = $(this).val();
                regex.test(inputValue) || "" == inputValue || (value++, $(this).after("<p class='form-item--error-message'>Email field is not valid.</p>"),
                    $(this).parent().addClass("form-item--error"));
            }),
        $(Form).find(".validate--conf-email").each(function() {
                var emailval = $(Form).find(".validate-email").val();
                var inputValue = $(this).val();
                if( emailval != inputValue )
                  {
                    $(this).after("<p class='form-item--error-message'>This field should be equal to Email Address.</p>"),
                    $(this).parent().addClass("form-item--error");
                  }

            }),

            $(Form).find(".g-recaptcha").each(function() {
              //var captchaValue = $(Form).find(".captcha-validate").val();
              if(grecaptcha.getResponse() == "") {
                event.preventDefault();
                $(this).next('.form-item--error-message').remove();
                $(this).parent().addClass("form-item--error");
                $(this).after("<p class='form-item--error-message'>Recaptcha field is required.</p>");
              } else {
                $(this).next('.form-item--error-message').remove();
                $(this).parent().removeClass("form-item--error");
              }
            }),

            !(value > 0);
    },

        ScrollTopPosition: function(ScrollPosition) {
          $("html, body").animate({
              scrollTop:  ($(ScrollPosition).offset() || { "top": NaN }).top - 200
          }, 500);
      },

        CheckoutLogin: function() {
            $("#checkout-flow-multi-step-form .checkout-login.form-submit").click(function(e) {
                var Form = $("#checkout-flow-multi-step-form"),
                    FormField = delta.commonFunctions.FormValidation(Form);
                return FormField || delta.commonFunctions.ScrollTopPosition($(Form).find(".form-item--error:first")),
                    FormField;
            });
        },

        Shipping: function() {
            $("#checkout-flow-shipping-form .checkout_shipping .ship-continue-btn.form-submit").click(function(e) {
                var Form = $("#checkout-flow-shipping-form"),
                    FormField = delta.commonFunctions.FormValidation(Form);
                return FormField || delta.commonFunctions.ScrollTopPosition($(Form).find(".form-item--error:first")),
                FormField;
            });
        },



        Billing: function() {

            $(".checkout_billing #payment-submit-button").on("click", function(e) {
                $("#edit-accept-order-details + label").next('p.check--error-message').remove();
                $("#edit-accept-order-details + label").removeClass("error-label");
                if(!$("#edit-accept-order-details").is(':checked')) {
                    $("label[for=edit-accept-order-details]").after("<p class='check--error-message'>Return Policy field is required</p>");
                    $("#edit-accept-order-details + label").addClass("error-label");
                }else{
                    $("#edit-accept-order-details + label").next('p.check--error-message').remove();
                    $("#edit-accept-order-details + label").removeClass("error-label");
                }

                if($("#edit-billshipaddresssame").val() == 1) {
                    var Form = $("#checkout-flow-billing-form"),
                        FormField = delta.commonFunctions.FormValidation(Form);
                    return FormField || (delta.commonFunctions.ScrollTopPosition($(Form).find(".form-item--error:first")),
                        setTimeout(function() {
                            $(".checkout-container input[type=submit]").removeAttr("disabled", "disabled");
                        }, 10)), FormField;
                    }

                });
        },

        UserRegister: function() {
            $("#user-registration-form .user-register-btn").click(function(e) {
                var Form = $("#user-registration-form"), FormField = delta.commonFunctions.FormValidation(Form);
                return FormField || delta.commonFunctions.ScrollTopPosition($(Form).find(".form-item--error:first")),
                    FormField;
            });
        },

        OrderStatus: function() {
          $("#order-status-form .order-status-btn").click(function(e) {
              var Form = $("#order-status-form"), FormField = delta.commonFunctions.FormValidation(Form);
              return FormField || delta.commonFunctions.ScrollTopPosition($(Form).find(".form-item--error:first")),
                  FormField;
          });
      },

        CampaignRegistration: function() {
          $(".product-registration-sweepstakes-form .product-reg-submit").click(function(e) {
              var Form = $(".product-registration-sweepstakes-form"), FormField = delta.commonFunctions.FormValidation(Form);
              return FormField || delta.commonFunctions.ScrollTopPosition($(Form).find(".form-item--error:first")),
                  FormField;
          });
      },

      EmailSweepstakes: function() {
        $(".form-emailsweepstakes .form-submit").click(function(e) {
            var Form = $(".form-emailsweepstakes"), FormField = delta.commonFunctions.FormValidation(Form);
            return FormField || delta.commonFunctions.ScrollTopPosition($(Form).find(".form-item--error:first")),
                FormField;
        });
    },

    WheretoBuyForm: function() {
      //Delta Location
      $(".where-to-buy-form").on('click','.form-submit',function() {
        $(".where-to-buy-form #edit-delta-location-type").next('p.check--error-message').remove();
        $(".where-to-buy-form #edit-delta-location-type").removeClass("error-label");
        if ($(".where-to-buy-form #edit-delta-location-type .form-checkbox:checked").length < 1 ) {
          $(".where-to-buy-form #edit-delta-location-type").after("<p class='check--error-message'>Select at least one Delta Type</p>");
        }
        else {
          $(".where-to-buy-form #edit-delta-location-type").next('p.check--error-message').remove();
          $(".where-to-buy-form #edit-delta-location-type").removeClass("error-label");
        }

        //Brizo Location
        $(".where-to-buy-form #edit-brizo-location-type").next('p.check--error-message').remove();
        $(".where-to-buy-form #edit-brizo-location-type").removeClass("error-label");
        if ($(".where-to-buy-form #edit-brizo-location-type .form-checkbox:checked").length == 0) {
          $(".where-to-buy-form #edit-brizo-location-type").after("<p class='check--error-message'>Select at least one Brizo Type</p>");
        } else {
          $(".where-to-buy-form #edit-brizo-location-type").next('p.check--error-message').remove();
          $(".where-to-buy-form #edit-brizo-location-type").removeClass("error-label");
        }

        //Commercial Location
        $(".where-to-buy-form #edit-commercial-location-type").next('p.check--error-message').remove();
        $(".where-to-buy-form #edit-commercial-location-type").removeClass("error-label");
        if ($(".where-to-buy-form #edit-commercial-location-type .form-checkbox:checked").length == 0) {
          $(".where-to-buy-form #edit-commercial-location-type").after("<p class='check--error-message'>Select at least one Commercial Location</p>");
        } else {
          $(".where-to-buy-form #edit-commercial-location-type").next('p.check--error-message').remove();
          $(".where-to-buy-form #edit-commercial-location-type").removeClass("error-label");
        }

        //Peerless Location
        $(".where-to-buy-form #edit-peerless-location-type").next('p.check--error-message').remove();
        $(".where-to-buy-form #edit-peerless-location-type").removeClass("error-label");
        if ($(".where-to-buy-form #edit-peerless-location-type .form-checkbox:checked").length == 0) {
          $(".where-to-buy-form #edit-peerless-location-type").after("<p class='check--error-message'>Select at least one Peerless Location</p>");
        } else {
          $(".where-to-buy-form #edit-peerless-location-type").next('p.check--error-message').remove();
          $(".where-to-buy-form #edit-peerless-location-type").removeClass("error-label");
        }

        //Radio Button
        $(".where-to-buy-form #edit-choose-one").next('p.check--error-message').remove();
        $(".where-to-buy-form #edit-choose-one").removeClass("error-label");
        if ($(".where-to-buy-form #edit-choose-one .form-radio:checked").length == 0) {
          $(".where-to-buy-form #edit-choose-one").after("<p class='check--error-message'>Please Select One</p>");
        } else {
          $(".where-to-buy-form #edit-choose-one").next('p.check--error-message').remove();
          $(".where-to-buy-form #edit-choose-one").removeClass("error-label");
        }


        var Form = $(".where-to-buy-form"), FormField = delta.commonFunctions.FormValidation(Form);
        return FormField || delta.commonFunctions.ScrollTopPosition($(Form).find(".form-item--error:first")),
        FormField;

      });
    },

      InspiredProForm: function() {
        $(".webform-submission-inspired-pro-email-sign-up-form .form-submit").click(function(e) {
            var Form = $(".webform-submission-inspired-pro-email-sign-up-form"), FormField = delta.commonFunctions.FormValidation(Form);
            return FormField || delta.commonFunctions.ScrollTopPosition($(Form).find(".form-item--error:first")),
                FormField;
        });
      },

      ProfessionalRequestTraining: function() {
        $(".professional-request-training .form-submit").click(function(e) {
            var Form = $(".professional-request-training"), FormField = delta.commonFunctions.FormValidation(Form);
            return FormField || delta.commonFunctions.ScrollTopPosition($(Form).find(".form-item--error:first")),
                FormField;
        });
      },

      deltaCCPAForm: function () {
        $(".delta-ccpa-form").on('click','.form-submit',function() {
            var Form = $(".delta-ccpa-form"), FormField = delta.commonFunctions.FormValidation(Form);
            return FormField || delta.commonFunctions.ScrollTopPosition($(Form).find(".form-item--error:first")),
                FormField;
        });
      },

      /* newsletterForm: function () {

        var formClass = '.contact-form-newsletter';

        $(formClass).on('click','.webform-button--submit',function() {
            var Form = $(formClass), FormField = delta.commonFunctions.FormValidation(Form);
            return FormField || delta.commonFunctions.ScrollTopPosition($(Form).find(".form-item--error:first")),
                FormField;
        });
      }, */


        init: function() {
          delta.commonFunctions.ScrollTopPosition(),
          delta.commonFunctions.Shipping(),
          delta.commonFunctions.Billing(),
          delta.commonFunctions.CheckoutLogin(),
          delta.commonFunctions.UserRegister(),
          delta.commonFunctions.OrderStatus(),
          delta.commonFunctions.CampaignRegistration(),
          delta.commonFunctions.EmailSweepstakes(),
          delta.commonFunctions.WheretoBuyForm(),
          delta.commonFunctions.InspiredProForm(),
          delta.commonFunctions.ProfessionalRequestTraining(),
          delta.commonFunctions.deltaCCPAForm()
          //delta.commonFunctions.newsletterForm()
        }
    };

    $( document ).ready(function() {
            delta.commonFunctions.init();

      $('.button--download-btn').on('click', function(e){
        e.preventDefault();
        download_file($(this).attr('href'), $(this).attr('download'))
      })

      $('form').find("input[name*='purchased_date']").each(function(ev)
      {
        $(this).attr("placeholder", "Product Purchase Date");
      });

      // Select Box overlap issue Fix
        var ua = window.navigator.userAgent;
        var isIE = /MSIE|Trident/.test(ua);
        if ( isIE ) {
          jQuery('.is-filled').each(function(){
            if(jQuery(this).find('.form-item__textfield').val() ==""){
              jQuery(this).removeClass('is-filled');
            }
            if(jQuery(this).find('.form-item__select').val() ==""){
              jQuery(this).removeClass('is-filled');
            }
          });


        }

      $('.form-item__select').parents('.form-item').find('label').hide();
      $('.form-item__select').on('change', function(){
        if($(this).val() != "") {
          $(this).parents('.form-item').find('label').show();
          $(this).parents('.form-item').addClass('is-filled');
        } else {
          $(this).parents('.form-item').find('label').hide();
          $(this).parents('.form-item').removeClass('is-filled');
        }
      });

      $('.delta-ccpa-form #edit-state, .delta-ccpa-form #edit-street-address,.delta-ccpa-form #edit-city,.delta-ccpa-form #edit-zipcode,.delta-ccpa-form #edit-first-name,.delta-ccpa-form #edit-last-name,.delta-ccpa-form #edit-email,.delta-ccpa-form #edit-phone').on('change',function() {
        if($(this).val() !== '') {

          var formitem = $(this).parents('.form-item');
          formitem.addClass('filledelement');
        }else {
          $(this).parents('.form-item').removeClass('filledelement');
        }
      });

      //ContactUs Form datepicker is-filled class
      $('.hasDatepicker.form-item__textfield').on('change', function(){
        if($(this).val() != "") {
          $(this).parents('.form-item').addClass('is-filled');
        } else {
          $(this).parents('.form-item').removeClass('is-filled');
        }
      });

      $('.product-registration-sweepstakes-form .product-reg-submit,.product-registration-sweepstakes-form .reset-values, .delta-ccpa-form .form-submit').prop('disabled', true);

      $(document).on("click",".path-service-parts .ui-autocomplete li a",function() {
        if(!$('.product-info-label .form-item').hasClass('is-filled')){
            $('.product-info-label .form-item').addClass('is-filled');
        }
      });

      // $('.product-registration-sweepstakes-form .form-item__select,.product-registration-sweepstakes-form .form-item__textfield, .product-registration-sweepstakes-form .form-checkbox').on("keyup change input paste dp.change", function() {
      //   $(this).val() != "" ? jQuery('.reset-values').prop('disabled', false) : jQuery('.reset-values').prop('disabled', true);
      //
      // });

      $('.product-registration-sweepstakes-form .form-item__select,.product-registration-sweepstakes-form .form-item__textfield, .product-registration-sweepstakes-form .form-checkbox').on("input", function() {
        $(this).val() != "" ? jQuery('.reset-values').prop('disabled', false) : jQuery('.reset-values').prop('disabled', true);

      });
      $('.product-registration-sweepstakes-form label[for="edit-purchased-date"]').removeClass('form-item__label');
    $('.product-registration-sweepstakes-form .reset-values').click(function() {
      $('.product-registration-sweepstakes-form .is-filled').find('input, select').val('');
      if ($(".form-checkbox").is(':checked')) {
        $('.form-checkbox').prop('checked', false);
      }
      $('.product-registration-sweepstakes-form').find('.valid').removeClass('valid');
      $('.product-registration-sweepstakes-form .is-filled').removeClass('is-filled');
      if($('.product-registration-sweepstakes-form').find('.form-item--error-message')) {
        $('.form-item--error-message').remove();
        $('input, select').parent().removeClass('form-item--error');
      }
      if($('.product-registration-sweepstakes-form .form-text,.product-registration-sweepstakes-form .form-select').val() !== "") {
        $(this).prop('disabled', false);
      }else {
        $(this).prop('disabled', true);
      }
      $('.product-registration-sweepstakes-form').find('.product-reg-submit').prop('disabled', true);
      grecaptcha.reset();
      if($('.product-registration-sweepstakes-form .required').length === $('.valid').length) {
          $('.product-registration-sweepstakes-form .product-reg-submit').prop('disabled', false);
        }else {
          $('.product-registration-sweepstakes-form .product-reg-submit').prop('disabled', true);
        }
    });

    //Global Search for Parts
    var initial  = $('#ui-id-1');

    $('#find-part-form .search-input').on('focus',function() {
      initial.appendTo('#find-a-part-autocomplete-results');
    });

    $('#find-product-form .search-input').on('focus',function() {
      initial.appendTo('#find-a-part-sfcc-autocomplete-results');
    });
    //Condition to check is location block is present before showing button in header banner.
    if (!$( ".pdp-find-local-delears" ).length) {
      if ($( "a[href='#block-findalocation']" ).length) {
        if (!$( "#block-findalocation" ).length) {
          $( "a[href='#block-findalocation']" ).hide();
        }
      }
    }

    if ($(".checkout_login" ).length) {
      if (window.performance && window.performance.navigation.type == window.performance.navigation.TYPE_BACK_FORWARD) {
        location.reload(true);
      }
    }

    });


  // Add to Compare Product
  $('.main-content').once().on('click', '.js-add-to-compare',function(e){
    e.preventDefault();
    var getproductsku = $(this).attr('name');
    $.ajax({
      type: "GET",
      url: "/compare",
      data: {"modelnumber" : getproductsku},
      cache: false,
      success: function(data){
        var getcomparedproductlist = data.split("||");
        if(data != '' && getcomparedproductlist.length > 0) {
          $("#compare_popup").click();
        }
      }
    });
  });
  // Add to Compare Product

  // Remove to Product from Compare list
  $('.js_remove_compare').once().on('click', function(e){
    e.preventDefault();
    var getproductsku = $(this).attr('name');
    $.ajax({
      type: "GET",
      url: "/remove-compare-product",
      data: {"modelnumber" : getproductsku},
      cache: false,
      success: function(data){
        $('.compare-popup button.ui-dialog-titlebar-close').click();
        $('.compare-products button.ui-dialog-titlebar-close').click();
        var getcomparedproductlist = data.split("||");
        if(data != '' && getcomparedproductlist.length > 0) {
          $("#compare_popup").click();
        }
      }
    });
  });
  // Remove to Product from Compare list

  // Add another compare product
  $('.add_another_product').once().on('click', function(e) {
    e.preventDefault();
    $('.compare-popup button.ui-dialog-titlebar-close').click();
  });
  // Add another compare product
  // Privacy back to page.
    var referrer =  document.referrer;
    $(".js-privacy-back").attr("href", referrer);

   // PDP Print
$(".pdp-print").click(function (){
  window.print();
});

$('.product-compare-print').on('click', function() {
  $(".compare-products").printThis({
      debug: true
  });
});

$('.com-popup-button-compare .compare_products').on('click', function() {
  $('body').addClass('compare_print');
});

$('.compare_print .ui-dialog-titlebar-close').on('click', function() {
  $('body').removeClass('compare_print');
});


  if($('.product-innovation').length){
    var path = window.location.pathname;
    var raw = path.split('/');
    var last = raw.length-1;
    if(raw.length == 3){
      var tab = 'all';
    }else{
      var tab = raw[last];
    }
    var activeTab = "innovation-" + tab.toLowerCase();
    $('.'+activeTab).addClass('is-active');
  }

  $(".tabs__link_to").on('click', function(event) {
    $('.tabs__link_to').removeClass('active');
    if (this.hash !== "") {
      event.preventDefault();

      var hash = this.hash;
      $(this).addClass('active');
      window.location.hash = hash;
      $('html, body').animate({
        scrollTop: ($(hash).offset().top - ($('#utility-wrapper').height() + $('#fullwidth-header').height()))
      });
    } // End if
  });


  $(document).ready(function () {
    if (window.location.hash !== "") {
      var hash = window.location.hash;
      if($(hash).length){
        $('html, body').animate({
          scrollTop: ($(hash).offset().top - ($('#utility-wrapper').height() + $('#fullwidth-header').height()))
        }, 500);
      }
    }

    // Lazyload footer social icons
    $('.footer__wrapper ul li').each(function(){
      if($(this).find('img').hasClass('lazy')) {
        var img_src =  $(this).find('img.lazy').attr("data-src");
        $(this).find('img.lazy').attr('src', img_src);
      }
    });

    $('.modal-body .section-lead-in').addClass('margin-top-0');
  });

  $(window).on('resize', function(e) {
    if ($(window).width() < 768) {
      searchResults();
    }
  });

  if ($(window).width() < 768) {
    searchResults();
  }

  function searchResults() {
    $('.path-search-results .tab-accordion-title').removeClass('active-tab-title');
    if($('.path-search-results .grid-container').find('.is-active')) {
      $('.path-search-results .tabs__tab.is-active').prev().addClass('active-tab-title');
    }
  }

  $(window).on('resize', function(e) {
    if ($(window).width() < 768) {
      productfeatures();
    }
  });

  if ($(window).width() < 768) {
    productfeatures();
  }

  function productfeatures() {
    $('.path-product .tab-accordion-title').removeClass('active-tab-title');
    if($('.path-product .grid-container').find('.is-active')) {
      $('.path-product .tabs__tab.is-active').prev().addClass('active-tab-title');
    }
  }

  $(document).ready(function(){
    $('.field_card_details').find('.4-col-grid').addClass('grid-col-lg-3');

    $('.field_card_details').find('.3-col-grid').addClass('grid-col-lg-4');

    $('.field_card_details').find('.4-col-title-bg').addClass('grid-col-lg-3');

    $('.banner .modal-pop-up-button, .featured-banner, .title-banner').find('.field_modal_box_button .paragraph--type--bp-modal').parent().addClass('button-inline');

    if(window.innerWidth < 920){
      $('.banner .only-destop, .feature_banner.only-destop, .video-banner-container .vid-banner.desktop, .banner-block-medium.only-destop').remove();
    }else{
      $('.banner .only-mobile, .feature_banner.only-mobile, .video-banner-container .vid-banner.mobile, .banner-block-medium.only-mobile').remove();
    }

    $(".popup").click(function(e){
    	e.preventDefault();
    	var target = $(this).attr("target");
    	if(target.length > 1) {
    		$("."+target).trigger("click");
    	}
    });
  });

  $( document ).ready(function() {
    $(".left-img, .right-img").find(".finish-product-img:eq(0)").addClass("selected");
    $(".layout__region--first, .layout__region--second").find(".finish-chip:eq(0)").addClass("selected");
    $('.alternative-image-copy-card .overlap_column').each(function(){
      var finishName = $(this).find(".collection-swatches .selected").text();
      $(this).find(".collection-swatches .finish-name").text(finishName);
    })
    $('.finish-chip').each(function(){

        $(this).hover(function(e){

          var finish = $(this).data('finish-value');
          var finishValue = $(this).text();
          var parent = $(this).closest('.collection-swatches');
          var finishLabel = $(parent).find('.finish-name');
          $(finishLabel).text(finishValue);
          $(this).parents('.collection-swatches').find('a.selected').removeClass('selected');
          $(this).addClass('selected');
          var image = $(this).parents('.alternative-image-copy-card ').find("[data-finish='"+finish+"']");
          $(this).parents('.alternative-image-copy-card ').find('img.selected').removeClass('selected');
          $(image).addClass('selected');
        });
      });
    });
})(jQuery, Drupal);

function compareNextProd() {
  jQuery(".main-com-desc").each(function() {
    var currentProd = jQuery(this).find(".compare-prod-visible").index();
    if((jQuery(this).children().length-1) == currentProd) {
      jQuery(this).find(".main-com-desc-block").removeClass("compare-prod-visible");
      jQuery(this).find(".main-com-desc-block:first").addClass("compare-prod-visible");
      return false;
    }
    var nextProd = currentProd + 1;
    jQuery(this).find(".main-com-desc-block").removeClass("compare-prod-visible");
    jQuery(this).find(".main-com-desc-block").eq(nextProd).addClass("compare-prod-visible");

  });
}

function comparePrevProd() {
  jQuery(".main-com-desc").each(function() {
    var currentProd = jQuery(this).find(".compare-prod-visible").index();
    var nextProd = currentProd - 1;
    jQuery(this).find(".main-com-desc-block").removeClass("compare-prod-visible");
    jQuery(this).find(".main-com-desc-block").eq(nextProd).addClass("compare-prod-visible");

  });
}
function download_file(fileURL, fileName) {
    // for non-IE
    if (!window.ActiveXObject) {
        var save = document.createElement('a');
        save.href = fileURL;
        save.target = '_blank';
        var filename = fileURL.substring(fileURL.lastIndexOf('/')+1);
        save.download = fileName || filename;
         if ( navigator.userAgent.toLowerCase().match(/(ipad|iphone|safari)/) && navigator.userAgent.search("Chrome") < 0) {
        document.location = save.href;
// window event not working here
      }else{
            var evt = new MouseEvent('click', {
                'view': window,
                'bubbles': true,
                'cancelable': false
            });
            save.dispatchEvent(evt);
            (window.URL || window.webkitURL).revokeObjectURL(save.href);
      }
    }

    // for IE < 11
    else if ( !! window.ActiveXObject && document.execCommand)     {
        var _window = window.open(fileURL, '_blank');
        _window.document.close();
        _window.document.execCommand('SaveAs', true, fileName || fileURL)
        _window.close();
    }
}

(function ($, Drupal) {
    Drupal.behaviors.gridview = {
        attach: function (context, settings) {
            const LIST_VIEW = 'list-view';
            const GRID_VIEW = 'grid-view';

            function getState(toggle) {
                if (toggle.classList.contains(LIST_VIEW)) {
                    return LIST_VIEW
                } else if (toggle.classList.contains(GRID_VIEW)) {
                    return GRID_VIEW;
                }

                return null;
            }


            $('.main-content',context).on('click', '.list-grid-toggle',function (e) {
                var currentToggle = $(this);
                var toggle = document.querySelector('.list-grid-toggle');
                var currentState = getState(toggle);
                const grids = [].slice.call(document.querySelectorAll('.download-card-grid'));
                if (currentState === LIST_VIEW) {
                    currentToggle.removeClass(LIST_VIEW);
                    currentToggle.addClass(GRID_VIEW);
                    currentToggle.text('Grid View');
                    grids.forEach(grid => grid.classList.add(LIST_VIEW));
                } else {
                    currentToggle.removeClass(GRID_VIEW);
                    currentToggle.addClass(LIST_VIEW);
                    currentToggle.text('List View');
                    grids.forEach(grid => grid.classList.remove(LIST_VIEW));
                }
            });
        }
    };
})(jQuery, Drupal);

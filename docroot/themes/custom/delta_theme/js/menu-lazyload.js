(function ($, Drupal, drupalSettings) {
    Drupal.behaviors.deltaMenuTheme = {
      attach: function (context, settings) {
        //Lazy loading Images in Mega Menu
        $('.mega-nav .mega-menu-nav--sub').find('img').addClass('lazy');

          if($('.mega-menu-nav__item.mega-menu-nav__item--with-sub').hasClass('main-menu-level')) {
            $('.mega-menu-nav__item.mega-menu-nav__item--with-sub.main-menu-level').hover(function() {
              var img = $(this).find("img.lazy"), // select images inside .container
              len = img.length, // check if they exist
              arr = [];
              if( len > 0 ) {
                // images found, get id
                $(this).find($('.mega-menu-nav__item.mega-menu-nav__item--sub.mega-menu-nav__item--sub-1')).each(function(){
                  if($(this).find('img').hasClass('lazy')) {
                    var img_src =  $(this).find('img.lazy').attr("data-src");
                    $(this).find('img.lazy').attr('src', img_src);
                  }
                  $(this).find($("ul.mega-menu-nav--sub-1 li.mega-menu-nav__item--sub-2")).each(function(e) {
                    if($(this).find('img').hasClass('lazy')) {
                      var img_src =  $(this).find('img.lazy').attr("data-src");
                      $(this).find('img.lazy').attr('src', img_src);
                    }
                  })
                });
              }
            })
          }

          //Lazy load of images in home pages
          if($('.lazy-load-on').length > 0 ) {
            $(window).on('scroll',function() {
              $('.lazy-load-on').each(function() {
                if($(this).find('[data-src]')) {
                  $(this).find('[data-src]').each(function() {
                    if ($(this).css('background-image') != 'none') {
                      $(this).addClass('lazy');
                      if($(this).hasClass('lazy')) {
                        if($(this).attr('data-src') !== "") {
                          var window_height = $(window).height();
                          var offset = $(this).offset().top - window_height;
                          if ($(window).scrollTop() >= offset) {
                            var img_source = $(this).attr("data-src");
                            $(this).css('background-image', 'url(' + img_source + ')');
                          }
                        }
                      }
                    }
                    else {
                      $(this).addClass('lazy');
                      if($(this).hasClass('lazy')) {
                        if($(this).attr('data-src') !== "") {
                          var window_height = $(window).height();
                          var offset = $(this).offset().top - window_height;
                          if ($(window).scrollTop() >= offset) {
                            var img_source = $(this).attr("data-src");
                            $(this).attr('src', img_source);
                          }
                        }
                      }
                    }
                  })
                }
              })
            })
          }

          //Lazy load of the images in Collection Listing pages of kitchen and bath
          $('.kitchen-collection .collection-listing,.bath-collection .collection-listing').find('img').addClass('lazy');

          $(window).on('scroll',function() {
            $('.kitchen-collection .collection-listing img,.bath-collection .collection-listing img').each(function() {
              if($(this).attr('data-source')) {
                if ($(this).attr('data-source') !== "") {
                  var window_height = $(window).height();
                  var offset = $(this).offset().top - window_height;
                  if ($(window).scrollTop() >= offset) {
                    var img_src =  $(this).attr('data-source');
                    $(this).attr('src', img_src);
                    $(this).css({
                       'width' : 'auto',
                       'height' : 'auto'
                    });
                    $(this).removeClass('lazy');
                    $(this).removeAttr('data-source');
                    return false;
                  }
                }
              }
              if($(this).attr('data-source') == "") {
                $(this).attr('src','');
                $(this).removeClass('lazy');
              }
            })
          });
          // Lazyload footer social icons
          $('.footer__wrapper ul li').each(function(){
            if($(this).find('img').hasClass('lazy')) {
              var img_src =  $(this).find('img.lazy').attr("data-src");
              $(this).find('img.lazy').attr('src', img_src);
            }
          });
    }
  };

})(jQuery, Drupal);

//Added a <sup> tag for the outlet and Voice Activated
jQuery( document ).ready(function() {
  jQuery('.mega-nav-wrapper .mega-menu-nav__item--with-sub.outlet').find('.outlets-new').append("<sup class='outlet-super-tag'>NEW</sup>");

  jQuery('.mega-nav-wrapper .mega-menu-nav__item--with-sub .mega-menu-nav__item--sub-3 a.new-link').append("<sup class='new-super-tag'>NEW</sup>");
});

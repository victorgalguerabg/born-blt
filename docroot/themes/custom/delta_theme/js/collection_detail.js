(function ($, Drupal, drupalSettings, Backbone) {
  if($(".path-kitchen .featured-banner,.path-bathroom .featured-banner, .path-kitchen-sinks .featured-banner, .path-bathroom-sinks .featured-banner").length) {
    $(".paragraph-component-structure").insertBefore(".main");
  }
  Drupal.behaviors.collectionDetailBehaviour = {
    attach: function (context, settings) {
      let gtm_collection = [];
      let gtm_collection_click = [];
      //let size = drupalSettings.collectionProductsPerPage;
      let category = drupalSettings.collectionCategory;
      let collection = drupalSettings.collectionName;
      let finishData = drupalSettings.finishData;
      let finishDataIcon = drupalSettings.finishImages;
      let cdpErrMsg = drupalSettings.cdp_err_msg;
      let pageNumber = 0;
      let filterFacets = '';
      let loadProducts = true;
      let lastPage = false;
      let appliedSort = '';
      let loader = $('.ajax-progress-throbber');
      let plpResults = $('.plp-results');
      let setFinishClass = "no-round";
      let ignoreListFinish = ['na','notapplicable'];
      let ignoreStockTypeFinish = ['Obsolete','Use Up'];
      let _finishList = [];
      let _defaultFinish = '';
      let sortvariable = "";
      let defaultPaginationOpttion = parseInt(drupalSettings.defaultPaginationOption);
      $('.collection-details__page-header').once().on('change', '#priceFilter', function (){
        let sortValue = $(this).find('option:selected').val();
        sortvariable = $(this).find('option:selected').text();
        if (typeof sortValue !== 'undefined') {
          appliedSort = sortValue;
        }else{
          appliedSort = appliedSort.replace(sortValue, "");
        }
        pageNumber = 0;
        applyFacets();
      });
      let size = $('.form-item__select--im').val();
      if (size == 'items-per-page') {
        size = defaultPaginationOpttion;
      }
      // On selection of finish facet corresponding value will be sent to API.
      $('.collection-details__page-header').on('change', '#finishFilter', function () {
        let finishValue = $(this).find('option:selected').val();
        sortvariable = $(this).find('option:selected').text();
        setFinishClass = finishValue;
        if (typeof finishValue !== 'undefined') {
          if(finishValue === 'all'){
            var count = 1,
            facet_str = [];
            $('#finishFilter option').each(function(index){
              if (index > 1){
                let id = $(this).val();
                facet_str += '&facetFinish=' + id;
              }
            });
            filterFacets = facet_str;
          }
          else {
            filterFacets = finishValue;
          }
        }else{
          filterFacets = appliedFacet.replace(finishValue, "");
        }
        pageNumber = 0;
        applyFacets();
      });
      $('.collection-details__dropdowns').on('change', '.form-item__select--im', function (){
        size = $('.form-item__select--im').val();
        sortvariable = $(this).find('option:selected').text();
        if (size == 'items-per-page') {
          size = defaultPaginationOpttion;
        }
        pageNumber = 0;
        applyFacets();
      });
      // Renders product data in template using backbone JS.
      let ProdView = Backbone.View.extend({
        el: "#output",
        template: _.template($("#output-node").html()),
        initialize: function (initData, append) {
          this.render(initData, append);
        },
        render: function (initData, append) {
          let _self = this;
          let _render = "";

          $("#output").addClass('grid');
            var collection_position = 0;

            // Getting SFCC product Redirect URL from SFCC apiCall
            var productIds = "";
            var productList = [];
            var loopCount = 0;
            _.each(initData.content, function (prdData) {
              if(loopCount != 24) {
                productIds += prdData.name+",";
                loopCount++;
              }
              else {
                productList.push(productIds);
                productIds = "";
                productIds += prdData.name+",";
                loopCount = 1;
              }
            });
            if(loopCount != 0) {
              productList.push(productIds);
              loopCount = 0;
              productIds = "";
            }

            var responsePrdUrls = {};
            _.each(productList, function (result) {
              if(result != "" && result != undefined) {
                var prdlist = result.replace(/,\s*$/, "");
                $.ajax({
                  type: "POST",
                  url: "/collection-listing-producturls",
                  dataType: 'json',
                  async: false,
                  data: {
                    prdids: prdlist,
                  },
                  error: function (XMLHttpRequest, textStatus, errorThrown) {
                    return false;
                  },
                  success: function (response) {
                    if(response != "" && response != undefined ){
                      _.each(response, function (result) {
                        responsePrdUrls[result.model_number] = result.view_details_link;
                      });
                    }
                  }
                });
              }
            });

            _.each(initData.content, function (entry) {
              let collection_product = new Object();
              collection_position = collection_position + 1;
              collection_product.list = "collection detail page";
              if(entry.collections[0] != undefined) {
                  collection_product.brand = entry.collections[0];
              }
              else {
                  collection_product.brand = "";
              }
              collection_product.name = entry.description;
              collection_product.position = collection_position;
              collection_product.id = entry.name;
              collection_product.variant = entry.values.Finish;
              if(entry.categories != undefined && entry.categories.length > 0) {
                  entry.categories.forEach(function(gtm_cat){
                      if (gtm_cat.startsWith("Delta_") == true){
                          collection_product.category = gtm_cat.replace("_", "/");
                      }
                  });
              }
              gtm_collection.push(collection_product);
              gtm_collection_click[entry.name] = collection_product;

              let _finishValue = entry.facetFinish;
              let _price = "Price";
              if (entry.values !== null) {
                _price = entry.values.ListPrice;
              }
              let _heroImage = entry.heroImageSmall;
              let _title = "Title";
              if (entry.description != '') {
                _title = entry.description;
              }
              let _collection = "COLLECTION NAME";
              if (entry.values !== null) {
                _collection = entry.values.Collection;
              }
              let _sku = entry.name;
              let _ribbonText = entry.ribbontext;
              let _ribbonColor = entry.ribboncolor;
              let _pdp = "";
              if(responsePrdUrls != "" && responsePrdUrls != undefined) {
                if(responsePrdUrls[entry.name] != "" && responsePrdUrls[entry.name] != undefined ) {
                  _pdp = responsePrdUrls[entry.name];
                }
              }
              let _finishList = entry.finishOptions;
              let _defaultFinish = entry.facetFinish;
              var setFinishIconPath = '';
              const getEachFinish = [];
              new Date;
              for (var i = 0; i <= _finishList.length; i++ ){
                if(_finishList[i] !== undefined && typeof _finishList[i].facetFinish !== undefined && _finishList[i].modelNumber.indexOf('-IN') == '-1' && _finishList[i].modelNumber.slice(-2) != '-R' && (Date.now() > _finishList[i].availableToOrderDate)){
                  var getAPiFinishName = _finishList[i].facetFinish;
                  var getAPiFinishST = _finishList[i].stockingType;
                  ActiveClass = 'in-active finish-class';
                  if(setFinishClass == getAPiFinishName){
                    ActiveClass = 'active finish-class';
                    _defaultFinish = '';
                  }
                  if(_defaultFinish == getAPiFinishName){
                    ActiveClass = 'active finish-class';
                  }
                  var setFinishIconPath = finishDataIcon[getAPiFinishName];
                  var existInIgnoreList = ignoreListFinish.indexOf(getAPiFinishName);
                  var existIgnoreStockList = ignoreStockTypeFinish.indexOf(getAPiFinishST);
                  if(setFinishIconPath !== undefined && existInIgnoreList == -1 && existIgnoreStockList == -1 && getAPiFinishST != ''){
                     var finishModelNumber = _finishList[i].modelNumber;
                     var imgIcon = '<a data="'+ _sku +'" href="/product-detail/' + finishModelNumber + '" class="no-line product-action-link">' +'<img src="' + setFinishIconPath + '" alt="'+ _finishList[i].facetFinish +'" title="'+ _finishList[i].finish +'" class="'+ ActiveClass +'"  /></a>';
                     getEachFinish.push(imgIcon);
                  }
                }
              }
              _render += _self.template({
                title: _title,
                collectionName: _collection,
                heroImage: _heroImage,
                finishOptions: getEachFinish,
                sku: _sku,
                price: _price,
                pdp: _pdp,
                ribbonText: _ribbonText,
                ribbonColor: _ribbonColor,
              });
            });
            if (gtm_collection != undefined) {
                dataLayer.push({
                    'event':'pageReady',
                    'ecommerce': {
                        'impressions': gtm_collection
                    },
                });
                dataLayer.push({
                    'event':'productImpression',
                    'ecommerce': {
                        'impressions': gtm_collection
                    },
                });
                gtm_collection = [];
            }
            if (append == true) {
              _self.$el.html(_render);
            }
            if (append == false) {
              _self.$el.append(_render);
            }

            // Product Click GA Event
            $(document).on("click","a.product-action-link", function (event) {
                event.stopPropagation();
                event.preventDefault();
                var currentUrl = $(this).attr("href");
                var spitedUrl = currentUrl.split("/");
                var parentClass = $(this).parent().attr("class");
                var sku = $(this).parents(".product-tile").find(".product-tile__sku a").text();
                //When user click finishes, which is not an current sku.
                if(parentClass == 'prod-finishes' && gtm_collection_click[spitedUrl[2]] == undefined){
                  spitedUrl[2] = $(this).attr('data');
                }
                if(sku != undefined && sku != "") {
                  var list = [];
                  list = gtm_collection_click[$.trim(sku)];
                  delete list["list"];
                    dataLayer.push({
                        'event':'productClick',
                        'ecommerce': {
                            'click': {
                                'actionField': {'list': 'collection detail page'},
                                'products': [list]
                            }
                        },
                        'eventCallback': function() {
                            document.location = currentUrl;
                        }
                    });
                }
                document.location = currentUrl;
            });

          return this;
        },
      });
      //Facet View
      let facetView = Backbone.View.extend({
        el: "#facets",
        template: _.template($("#finish-facets").html()),
        initialize: function (initData, append) {
          let facetBlock = getFinishFacets(initData);
          this.render(facetBlock, append);
        },
        events: {
          'change #finishFilter': 'applyFacets',
        },
        applyFacets: function (e) {
          loadProducts = true;

        },
        render: function (facetBlock, append) {
          let _self = this;
          let _render = "";

          $("#finish-facets").addClass('cdp-finish');
          if (facetBlock != '') {
            $('#finish-filters').html(facetBlock);
          }
          else {
            // TBD.
            var noResultsMessage = '<div class ="error-message" role="contentinfo" aria-label="Error message">'+cdpErrMsg+'</div>';
            $('#facets').html(noResultsMessage);
          }
          return this;
        }
        });


      function applyFacets(e) {
        loadProducts = true;
        loader.show();
        var url = window.location.origin;
        var actionurl = window.location.href;
        var sortValue = sortvariable.replace(/[\(\)']+/g,'');;
        if(sortValue != "") {
          dataLayer.push({
            'event': 'customEvents',
            'category': 'used sort filter',
            'action': sortValue,
            'label': actionurl,
          });
        }

        $.ajax({
          type: "GET",
          url: "/collection-listing",
          dataType: 'json',
          data: {
            size: size,
            sub_category: activeTab,
            category: category,
            collection: collection,
            facets: filterFacets,
            sort: appliedSort,
            pagenumber: pageNumber,
            cache: 'F',
          },
          error: function (XMLHttpRequest, textStatus, errorThrown) {
            $('.cdp-pagination-output').html('');
            return false;
          },
          success: function (data) {
            lastPage = data.last;
            let response = new ProdView(data, true);
            if(data){
              data.currentpage = 1;
            }
            $('.form-item__select--im').hide();
            $('.cdp-pagination-output').hide();
            if(data.totalElements > 24){
              $('.form-item__select--im').show();
              $('.cdp-pagination-output').show();
            }
            let CdppaginationView = new cdppaginationView(data);
            CdppaginationView.undelegateEvents();
            lastPage = data.last;
            loadProducts = true;
            $('.loader').hide();
            loader.hide();
          }
        });
      }
      var activeTab = '';
    jQuery( document ).ready(function() {
      if (drupalSettings.termfound != 'false'){
        pageNumber = 0;
        jQuery("#priceFilter").prop("selectedIndex", 1);
        if(jQuery(".collection-details__tabs ul.tabs__nav li").hasClass("tab-parent-active")) {
          jQuery(".collection-details__tabs ul.tabs__nav li").find("a.tabs__link").removeClass("is-active");
          jQuery(".tab-parent-active").find("a.tabs__link").addClass("is-active");
        }
        else{
          jQuery(".tabs__nav .tabs__nav").each(function() {
            jQuery(this).find("a.tabs__link").removeClass("is-active");
            if (jQuery(this).index() == 1) {
              jQuery(this).find("a.tabs__link").addClass("is-active");
            }
          });
        }
        if (jQuery(".collection-details__tabs a.tabs__link.is-active").attr("href")) {
          activeTab = jQuery(".collection-details__tabs a.tabs__link.is-active").attr("href").replace('#', '');
        }
        else {
          activeTab = '';
        }
        apiCall(activeTab);
      }
    });
    jQuery(".collection-details__tabs .tabs ul li .tabs__link").on( "click", function() {
      pageNumber = 0;
      jQuery(".collection-details__tabs ul.tabs__nav li").find("a.tabs__link").removeClass("is-active");
      jQuery(this).toggleClass('is-active');
      activeTabHref = jQuery(this).attr("href");
      if (jQuery(this).attr("href")){
        activeTab = jQuery(this).attr("href").replace('#', '');
      }
      else{
        activeTab = '';
      }
      $('.cdp-pagination-output').remove();
      $('.collection-details__dropdowns').append('<div class="pager__items js-pager__items cdp-pagination-output cdp-pagination-page-end"></div>');
      $('.cdp-pagination-bottom').html('<div class="pager__items js-pager__items cdp-pagination-output"></div>');
      $('#priceFilter').val('sort_ListPrice=DESC');
      $('.form-item__select--im').val('items-per-page');
      apiCall(activeTab);
    });
    function apiCall(activeTab){
      loadProducts = false;
      loader.show();
      /* API call to get the products */
      $.ajax({
        type: "GET",
        url: "/collection-listing",
        dataType: 'json',
        data: {
          size: size,
          sub_category: activeTab,
          category: category,
          collection: collection,
          pagenumber: pageNumber,
          cache: 'F',
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
          $('.cdp-pagination-output').html('');
          return false;
        },
        success: function (data) {
          if (data !== null && data.content != ''){
           let response = new ProdView(data, true);
            if(data){
              data.currentpage = 1;
            }
            $('.cdp-pagination-output').hide();
            $('.form-item__select--im').hide();
            if(data.totalElements > 24){
              $('.form-item__select--im').show();
              $('.cdp-pagination-output').show();
            }
            let CdppaginationView = new cdppaginationView(data);
            lastPage = data.last;
           $('.loader').hide();
           loader.hide();
          }
          else{
            var noResultsMessage = '<div class ="error-message" role="contentinfo" aria-label="Error message">'+cdpErrMsg+'</div>';
            $('#output').html(noResultsMessage);
            $('.loader').hide();
            loader.hide();
          }
          // if no products then set last as true
          lastPage = (data !== null) ? data.last : true;
          pageNumber++;
          loadProducts = true;
          $('.loader').hide();
          loader.hide();
        }
    });
      /* API call to get the finish facets */
      $.ajax({
        type: "GET",
        url: "/collection-finish-facets",
        dataType: 'json',
        data: {
          size: size,
          sub_category: activeTab,
          category: category,
          collection: collection,
          pagenumber: pageNumber,
          cache: 'T',
        },
        success: function (data) {
          if(data != null){
            let FacetView = new facetView(data);
          }
        }
      });
    }
    function getFinishFacets(initData){
      let output = '<option value="">Choose your finish</option><option value="all">All Finishes</option>';
      if(initData != null){
        let facetBlock = [];
        let count = 0;
          let facetName = initData.facets[0].name;
          let facetType = initData.facets[0].type;
          let facetCount = 0;
          if (facetType == 'term') {
            _.each(initData.facets[0].terms, function (terms) {
              var termLabel = findKey(finishData, terms.term);
              output += '<option value="' + terms.term + '">'+ termLabel + '</option>';
              facetCount++;
            });
          }
        }
        return '<select id="finishFilter" class="form-item__select form-item__select--sm">' + output + '</select>';
      }
      $(".pagination-bottom").once().on("click", "input[type='button']", function (){
        $('html, body').animate({
          'scrollTop' : $("#output").position().top
        });
      });
      // On Scroll to achieve lazyload functionality.
      function applyproductPagination(params) {
        loadProducts = false;
        loader.show();
        let page;
        if (params['action']) {
          if (params['action'] == 'prev') {
            if (params['pageNum'] == 2) {
              page = 0;
            }
            else {
              page = params['pageNum'] - 2;
            }
          }
          else {
            page = params['pageNum'];
          }
        }
        else {
          page = params['pageNum'] - 1;
        }
        $.ajax({
          type: "GET",
          url: "/collection-listing",
          dataType: 'json',
          data: {
            size: size,
            sub_category: activeTab,
            category: category,
            collection: collection,
            facets: filterFacets,
            sort: appliedSort,
            pagenumber: page,
            cache: 'F',
          },
          error: function (XMLHttpRequest, textStatus, errorThrown) {
            $('.cdp-pagination-output').html('');
            return false;
          },
          success: function (data) {
            let response = new ProdView(data, true);
            if(params['pageNum'] && data) {
              data.currentpage = params['pageNum'];
              data.first = params['first'];
              data.last = params['last'];
            }
            if(params['action'] && data) {
              data.action = params['action'];
            }
            let CdppaginationView = new cdppaginationView(data);
            CdppaginationView.undelegateEvents();
            loadProducts = true;
            lastPage = data.last;
            pageNumber++;
            loadProducts = true;
            lastPage = data.last;
            $('.loader').hide();
            loader.hide();
          }
        });
      }

      function findKey(input, target){
        var found;
        for (var prop in input) {
          if(prop == target){
            found = input[prop];
          }
        };
        return found;
      };
      /* pagination starts here*/
      var cdppaginationView = Backbone.View.extend({
        el: ".cdp-pagination-output",
        template: _.template($("#cdp-pagination").html()),
        initialize: function (initData) {
          this.render(initData);
        },
        events: {
          'click [type="button"]': 'cdppagination'
        },
        cdppagination: function (e) {
          var params = [];
          params['pageNum'] = $(e.currentTarget).val();
          params['first'] = $(e.currentTarget).attr('first');
          params['last'] = $(e.currentTarget).attr('last');
          if($(e.currentTarget).val() === '>') {
            params['pageNum'] = $('.pager__link.is-active').val();
            params['action'] = 'next';
          }
          if($(e.currentTarget).val() === '<') {
            params['pageNum'] = $('.pager__link.is-active').val();
            params['action'] = 'prev';
          }
          if($(e.currentTarget).attr('data') === 'last-page') {
            var total = $(e.currentTarget).attr('total');
            params['pageNum'] = parseInt(total);
            params['first'] = parseInt(total)-2;
            params['last'] = parseInt(total);
          }
          if($(e.currentTarget).attr('data') === 'first-page') {
            params['pageNum'] = 1;
            params['first'] = 1;
            params['last'] = 3;
          }
          applyproductPagination(params);        },
        render: function (initData) {
          if (initData === null || typeof initData == 'undefined') {
            return false;
          }
          var cont_disp_cnt = size;//drupalSettings.cont_disp_cnt;
          if (cont_disp_cnt == null) {
            cont_disp_cnt = defaultPaginationOpttion;
          }
          var _self = this;
          var _render = "<div class='page-desktop-pagination'>";
          var _mobilerender = "<div class='page-mobile-pagination'>";
          var _nextPage = (initData === null) ? 0 : parseInt(initData.number) + 1;
          var _currentpage = initData.currentpage;  // Current page
          var _totalmatches = initData.totalElements;
          var _totalpages = Math.ceil(_totalmatches / cont_disp_cnt);
          var _nextPage;
          var _first;
          if (_currentpage < _totalpages) {
            _nextPage = _currentpage + 1;
          } else {
            _nextPage = _currentpage;
          }
          var _previousPage;
          if (_currentpage === 1) {
            _previousPage = 1;
            _first = 1;
          } else {
            _previousPage = _currentpage - 1;
            _first = initData.first;
          }
          if(_currentpage>=initData.first && _currentpage<=initData.last) {
            _first = parseInt(initData.first);
          }
          if(initData.action && initData.action == 'next') {
            if(_currentpage>=initData.first && _currentpage<initData.last) {
              _first = parseInt(initData.first);
            } else {
              _first = parseInt(initData.first) + 1;
            }
            _currentpage = parseInt(_currentpage) + 1;
          }
          if(initData.action && initData.action == 'prev') {
            if(_currentpage>initData.first && _currentpage<=initData.last) {
              _first = parseInt(initData.first);
            } else {
              _first = parseInt(initData.first) - 1;
            }
            _currentpage = parseInt(_currentpage) - 1;
          }
          var pagination = parseInt(_first)+2;
          if(_totalpages < pagination) {
            pagination = _totalpages;
          }
          if(_currentpage != 1) {
            //_render += '<input total="'+_totalpages+'" class="pager__link first" type="button" data="" value="<<">';
            _render += '<input first="'+_first+'" last="'+pagination+'" class="pager__link prev" type="button" data="" value="<">';
            _mobilerender += '<input first="'+_first+'" last="'+pagination+'" class="pager__link prev" type="button" data="" value="<">';
          }
          if((_first+2 >= _totalpages) && (_totalpages > 3)) {
            _render += '<input total="'+_totalpages+'" class="pager__link first" type="button" data="first-page" value=1><span class="pagination-dots"> ... </span>';
          }
          for(var i=_first;i<=pagination;i++) {
            /*_render += _self.template({
              pageNumber: i,
              currentPage: _currentpage,
              totalRecordset: initData.totalElements
            });*/
            var classValue = '';
            if(i === parseInt(_currentpage)) {
              classValue = 'is-active';
              _mobilerender += '<p class="mobile_pagination"><span>Page </span><strong>' + i + '</strong></p>';
            }

            _render += '<input first="'+_first+'" last="'+pagination+'" class="pager__link '+ classValue +'" type="button" value="'+ i +'">';

          }
          if(!_first && _totalpages == 1) {
            _mobilerender += '<p class="mobile_pagination"><span>Page </span><strong>' + _totalpages + '</strong></p>';

          }
          _mobilerender += '<p class="mobile_pagination"><span>of </span><strong>' + _totalpages +'</strong></p>';
          if(_first+2 < _totalpages) {
            _render += '<span class="pagination-dots"> ... </span><input total="'+_totalpages+'" class="pager__link last" type="button" data="last-page" value='+_totalpages+'>';
          }
          if(_currentpage != _totalpages) {
            _render += '<input first="'+_first+'" last="'+pagination+'" class="pager__link next" type="button" data="" value=">">';
            _mobilerender += '<input first="'+_first+'" last="'+pagination+'" class="pager__link next" type="button" data="" value=">">';
            //_render += '<input total="'+_totalpages+'" class="pager__link last" type="button" data="" value=">>">';
          }
          var start_value = (_currentpage-1)*cont_disp_cnt;
          if(_currentpage ==1) {
            start_value = 1;
          }
          var endvalue = _currentpage*cont_disp_cnt;
          if(start_value == 1 && endvalue > initData.totalElements){
            // no pager
          }
          else if(endvalue > initData.totalElements){
            endvalue = initData.totalElements;
            $('.srp-result-doc').html('<span class="srp-qty">Results ' + start_value + '-' + endvalue  + ' of ' + initData.totalElements +'</span>').attr('data-content-cnt', initData.totalElements);
          }
          else{
            $('.srp-result-doc').html('<span class="srp-qty">Results ' + start_value + '-' + endvalue  + ' of ' + initData.totalElements +'</span>').attr('data-content-cnt', initData.totalElements);
          }
          _render += "</div>"
          _mobilerender += "</div>";
          _render = _render + _mobilerender;
          _self.$el.html(_render);
        }
      });
      /*End of pagination*/
    }
  };
})
(jQuery, Drupal, drupalSettings, Backbone);

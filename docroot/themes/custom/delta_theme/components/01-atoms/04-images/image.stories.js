import image from './00-image/_image.twig';
import picture from './00-image/_picture.twig';
import figure from './00-image/figure.twig';
import responsiveImage from './00-image/responsive-image.twig';
import backgroundImage from './00-image/background-image/background-image.twig';
import icon from './icons/icons.twig';
import iconCircle from './icons/_icon-circle.twig';

import figureYml from './00-image/02-figure.yml';
import responsiveImageData from './00-image/00-responsive-image.yml';
import backgroundImageData from './00-image/background-image/background-image.yml';
import iconData from './icons/icons.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Atoms/Images' };

export const Image = () => image();

export const Picture = () => picture();
export const Figure = () => figure(figureYml);
export const ResponsiveImage = () => responsiveImage(responsiveImageData);
export const BackgroundImage = () => backgroundImage(backgroundImageData);
export const icons = () => icon(iconData);
export const IconCircle = () => iconCircle();

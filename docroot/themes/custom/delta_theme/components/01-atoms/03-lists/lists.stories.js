import dl from './02-dl.twig';
import ul from './00-ul.twig';
import ol from './01-ol.twig';

import dlData from './02-dl.yml';
import ulData from './00-ul.yml';
import olData from './01-ol.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Atoms/Lists' };

export const definitionList = () => dl(dlData);

export const unorderedList = () => ul(ulData);

export const orderedList = () => ol(olData);

(function() {
  const steppers = [].slice.call(document.querySelectorAll('.numeric-stepper'));
  steppers.forEach(numericInput);

  function numericInput(wrapper) {
    const input = wrapper.querySelector('.numeric-stepper__input');
    const incrementButton = wrapper.querySelector('.js-numeric-increment');
    const decrementButton = wrapper.querySelector('.js-numeric-decrement');
    const min = input.dataset.min || 2;
    const max = input.dataset.max || 200000;

    const cleave = new Cleave(input, {
      numeral: true,
      numeralThousandsGroupStyle: 'none',
      numeralDecimalScale: 0,
      numeralPositiveOnly: true,
    });

    function increment() {
      const newValue = parseInt(input.value, 10) + 1;

      if (newValue > max) {
        return;
      }

      input.value = newValue;
    }

    function decrement() {
      const newValue = parseInt(input.value, 10) - 1;
      if (newValue <= min) {
        input.value = 1;
      }
      else {
        input.value = newValue;
      }

    }

    incrementButton.addEventListener('click', function() {
      increment();
    });

    decrementButton.addEventListener('click', function() {
      decrement();
    });

    input.addEventListener('keydown', function(e) {
      switch (e.keyCode) {
        case 38:
          e.preventDefault();
          return increment();
        case 40:
          e.preventDefault();
          return decrement();
      }
    });
  }
})();

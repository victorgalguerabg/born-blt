// Filter Chip Stories
import filterChip from './_filter-chip.twig';

/**
 * Storybook Definition.
 */
export default { title: 'Atoms/Filter Chip' };

export const FilterChip = () => filterChip();

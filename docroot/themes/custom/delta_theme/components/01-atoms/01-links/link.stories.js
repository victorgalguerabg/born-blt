import animatedLink from './animated-link/animation-link.twig';
import ctaLink from './cta-link/cta-link.twig';
import footerLink from './footer-link/footer-link.twig';
import inlineText from './inline-text/inline-text.twig';
import link from './link/link.twig';
import linkLevel from './link-level/link-level-1.twig';
import linkLevel2 from './link-level/link-level-2.twig';
import textListLink from './text-list-link/text-list-link.twig';
import twoLevelLinks from './two-level-links/two-level-links.twig';

import animatedLinkData from './animated-link/animation-link.yml';
import ctaLinkData from './cta-link/cta-link.yml';
import footerLinkData from './footer-link/footer-link.yml';
import inlineTextData from './inline-text/inline-text.yml';
import linkData from './link/link.yml';
import linkLevelData from './link-level/link-level-1.yml';
import linkLevelTwoData from './link-level/link-level-2.yml';
import textListLinkData from './text-list-link/text-list-link.yml';
import twoLevelLinksData from './two-level-links/two-level-links.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Atoms/Links' };

export const animatedLinks = () => animatedLink(animatedLinkData);
export const CTALinks = () => ctaLink(ctaLinkData);
export const footerLinks = () => footerLink(footerLinkData);
export const inlineTexts = () => inlineText(inlineTextData);
export const links = () => link(linkData);
export const linksLevel = () => linkLevel(linkLevelData);
export const linksLevelTwo = () => linkLevel2(linkLevelTwoData);
export const TextListLink = () => textListLink(textListLinkData);
export const TwoLevelLink = () => twoLevelLinks(twoLevelLinksData);

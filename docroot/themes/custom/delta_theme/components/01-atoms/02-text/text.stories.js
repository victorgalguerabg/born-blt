import heading from './00-headings/_heading.twig';
import pageTitle from './00-headings/page-title.twig';
import resultTotal from './result-totals/result-totals.twig';
import blockquote from './text/02-blockquote.twig';
import pre from './text/05-preformatted-text.twig';
import paragraph from './text/03-inline-elements.twig';
import textField from './text-fields/text-fields.twig';

import blockquoteData from './text/blockquote.yml';
import headingData from './00-headings/headings.yml';
import pageTitleData from './00-headings/page-title.yml';
import resultTotalData from './result-totals/result-totals.yml';
import textFieldlData from './text-fields/text-fields.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Atoms/Text' };

// Loop over items in headingData to show each one in the example below.
const headings = headingData.map((d) => heading(d)).join('');

export const headingsExamples = () => headings;

export const PageTitle = () => pageTitle(pageTitleData);

export const blockquoteExample = () => blockquote(blockquoteData);

export const preformatted = () => pre();

export const random = () => paragraph();

export const ResultTotal = () => resultTotal(resultTotalData);

export const TextFields = () => textField(textFieldlData);

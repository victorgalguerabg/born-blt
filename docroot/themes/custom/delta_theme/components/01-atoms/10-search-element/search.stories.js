// Search Button Stories
import searchButton from './search-button.twig';
import searchInput from './search-input.twig';

import searchButtonData from './search-button.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Atoms/Search Element' };

export const SearchButton = () => searchButton(searchButtonData);
export const SearchInput = () => searchInput();

// Finish Thumbnail Stories
import finishThumbnail from './_finish-thumbnail.twig'

/**
 * Storybook Definition.
 */
export default { title: 'Atoms/Finish Thumbnail' };

export const FinishThumbnail = () => finishThumbnail();

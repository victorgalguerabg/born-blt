// Catalog List Stories
import catalogList from './catalog-listing.twig';

import catalogListData from './catalog-listing.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Atoms/Catalog Listing' };

export const CatalogList = () => catalogList(catalogListData);

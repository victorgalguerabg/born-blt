// Download Badgeories
import downloadBadge from './download-badge.twig';

import downloadBadgeData from './download-badge.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Atoms/Download Badge' };

export const DownloadBadge = () => downloadBadge(downloadBadgeData);

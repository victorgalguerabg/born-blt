---
el: ".button"
title: "Button Base"
---

The base building block for all buttons. Not intended for use without modifiers.

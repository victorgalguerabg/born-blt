// Buttons Stories
import button from './01-button.twig';

import buttonData from './button.yml';
import buttonPDData from './01-button~primary-default.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Atoms/Button' };

export const Button = () => button(buttonData);

export const ButtonPrimaryDefault = () => button(buttonPDData);

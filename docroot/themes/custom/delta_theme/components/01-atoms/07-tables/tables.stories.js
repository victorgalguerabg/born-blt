// Buttons Stories
import table from './tables.twig';

import tableData from './tables.yml'; 
/**
 * Storybook Definition.
 */
export default { title: 'Atoms/Table' };

export const Tables = () => table(tableData);

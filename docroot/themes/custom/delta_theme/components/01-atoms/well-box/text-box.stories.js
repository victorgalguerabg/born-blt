// Text Box Stories
import textBox from './text-box.twig';
import textBoxSubtitle from './text-box-subtitle.twig';
import textBoxTitle from './text-box-title.twig';
import textBoxDiv from './textbox-div.twig';
import textBoxPhoneNumber from './textbox-phone-number.twig';

import textBoxData from './text-box.yml';
import textBoxSubtitleData from './text-box-subtitle.yml';
import textBoxTitleData from './text-box-title.yml';
import textBoxDivData from './textbox-div.yml';
import textBoxPhoneNumberData from './textbox-phone-number.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Atoms/Well Box' };

export const TextBox = () => textBox(textBoxData);
export const TextBoxSubtitle = () => textBoxSubtitle(textBoxSubtitleData);
export const TextBoxTitle = () => textBoxTitle(textBoxTitleData);
export const TextBoxDiv = () => textBoxDiv(textBoxDivData);
export const TextBoxPhoneNumber = () => textBoxPhoneNumber(textBoxPhoneNumberData);

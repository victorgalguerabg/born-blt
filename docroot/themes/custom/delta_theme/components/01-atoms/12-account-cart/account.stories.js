// Account Cart Stories
import accountCart from './_account-cart.twig';

import accountCartData from './_account-cart.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Atoms/Account Cart' };

export const AccountCart = () => accountCart(accountCartData);

// Image Title Stories
import imageTitle from './image-tile.twig';

import imageTitleData from './image-tile.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Atoms/Image Title' };

export const ImageTitle = () => imageTitle(imageTitleData);

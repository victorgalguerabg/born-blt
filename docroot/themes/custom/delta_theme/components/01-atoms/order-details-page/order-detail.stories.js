// Catalog List Stories
import orderModal from './order-model/order-model.twig';

import orderModalData from './order-model/order-model.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Atoms/Order Detail Page' };

export const OrderModal = () => orderModal(orderModalData);

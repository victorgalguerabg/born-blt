// Dropdown Stories
import dropdown from './dropdown.twig';
import languageDropdown from './language-dropdown.twig';

import dropdownData from './dropdown.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Atoms/Dropdown' };

export const Dropdown = () => dropdown(dropdownData);
export const LanguageDropdown = () => languageDropdown(dropdownData);

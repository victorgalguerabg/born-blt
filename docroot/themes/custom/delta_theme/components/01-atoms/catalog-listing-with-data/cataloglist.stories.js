// Catalog List With Data Stories
import catalogListWith from './catalog-listing-with-data.twig';

import catalogListWithData from './catalog-listing-with-data.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Atoms/Catalog Listing With Data' };

export const CatalogListWithData = () => catalogListWith(catalogListWithData);

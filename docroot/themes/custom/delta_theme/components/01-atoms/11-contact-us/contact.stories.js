// Contact Us Stories
import contact from './contact-us.twig';

import contactData from './contact-us.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Atoms/Contact Us' };

export const Contact = () => contact(contactData);

// Flag Stories
import flag from './flag.twig';

import flagData from './flag.yml';
import flagDeltaRedData from './flag~delta-red.yml';
import flagGrayData from './flag~gray-30.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Atoms/Flag' };

export const Flag = () => flag(flagData);
export const FlagDeltaRed = () => flag(flagDeltaRedData);
export const FlagGray = () => flag(flagGrayData);

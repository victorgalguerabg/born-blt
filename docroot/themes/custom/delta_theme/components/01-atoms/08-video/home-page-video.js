// Home page video
import jQuery from 'jquery';

Drupal.behaviors.homePageVideo = {
  attach: function (context, settings) {

    //(function () { // REMOVE IF DRUPAL.

      'use strict';


  jQuery(document).ready(function () {
    var homePanel3Video = document.getElementsByClassName("video_panel"),
      homePanel3VideoPlayback = document.getElementsByClassName("play_button_video");
    //load video meta data then set frame to 1
    homePanel3Video[0].addEventListener('loadeddata', function() { homePanel3Video[0].currentTime = 1;}, false);
    //the above loadeddata callback - not always working; adding this for backup:
    setTimeout(function(){ homePanel3Video.currentTime = 1; }, 100);

    // listen for playback
    jQuery(".play_button_video").on("click", function(e) {
      //e.preventDefault();


      let srcMp4	= jQuery(this).data('src-mp4');
      let srcWebm	= jQuery(this).data('src-webm');

      jQuery(this).parents('.video_wrapper').find('.backgroundimage').removeAttr('style');

      jQuery(this).parents('.video_wrapper').find('.video_panel').html('<source  src="'+srcMp4+'" type="video/mp4" /><source src="'+srcWebm+'m" type="video/webm" />');

      if (jQuery(this).parents('.video_wrapper').find('.video_panel')[0].paused) {
        jQuery(this).parents('.video_wrapper').find('.overlay-container').hide();
        jQuery(this).parents('.video_wrapper').find('.video-title-container').hide();
        jQuery(this).parents('.video_wrapper').find('.video_panel')[0].play();
      } else {
        jQuery(this).parents('.video_wrapper').find('.video_panel')[0].pause();
      }
    });


    jQuery('.video_panel').on("click", function(e) {
      e.preventDefault();
      if (jQuery(this).parents('.video_wrapper').find('.video_panel')[0].paused) {
      } else {
        jQuery(this).parents('.video_wrapper').find('.video_panel')[0].pause();
        jQuery(this).parents('.video_wrapper').find('.video-title-container').show();
        jQuery(this).parents('.video_wrapper').find('.overlay-container').show();
      }
    } );
    jQuery('.video_panel').on('ended', function(e) {
      this.currentTime = 1;
      jQuery(this).parents('.video_wrapper').find('.video-title-container').show();
      jQuery(this).parents('.video_wrapper').find('.overlay-container').show();
      jQuery(this).parents('.video_wrapper').find(homePanel3VideoPlayback).show();
    });

  });
  }
};



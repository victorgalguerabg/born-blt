import jQuery from 'jquery';

Drupal.behaviors.videoOnPopup = {
  attach: function (context, settings) {
  //(function () { // REMOVE IF DRUPAL.

  'use strict';

  jQuery(".video-card__thumbnail").click(function() {
    jQuery(this).next(".video-popup-wrapper").addClass("active-video-popup-wrapper");
    var vid = document.getElementById(jQuery(this).next(".video-popup-wrapper").find("video").attr("id"));
    vid.autoplay = true;
    vid.load();
  });

  jQuery(document).mouseup(function(e)
  {
      var container = jQuery("video-pop-data");
      if (!container.is(e.target) && container.has(e.target).length === 0)
      {
        var vid1 = document.getElementById(jQuery(".active-video-popup-wrapper").find("video").attr("id"));
        vid1.pause();
        vid1.currentTime = 0;
        jQuery(".video-popup-wrapper").removeClass("active-video-popup-wrapper");
      }
  });



//})(); // REMOVE IF DRUPAL.
  }
};

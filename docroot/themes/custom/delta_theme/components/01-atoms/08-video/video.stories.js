// Video Stories
import video from './video.twig';
import videoWithSource from './video-with-source.twig';
import videoOnClick from './video-on-click.twig';
import homePageVideo from './home-page-video.twig';
import videoOnPopup from './video-on-popup/video-on-popup.twig';
import videoOnTab from './video-on-tabs/video-on-tabs.twig';

import videoData from './video.yml';
import videoFullData from './video~full.yml';
import videoWithSourceData from './video-with-source.yml';
import videoOnClickData from './video-on-click.yml';
import homePageVideoData from './home-page-video.yml';
import videoOnPopupData from './video-on-popup/video-on-popup.yml';
import videoOnTabData from './video-on-tabs/video-on-tabs.yml';

import './video-on-click';
import './home-page-video';
import './video-on-popup/video-on-popup';
import './video-on-tabs/video-on-tabs';

/**
 * Storybook Definition.
 */
export default { title: 'Atoms/Video' };

export const Video = () => video(videoData);
export const VideoFull = () => video(videoFullData);
export const VideoWithSource = () => videoWithSource(videoWithSourceData);
export const VideoOnClick = () => videoOnClick(videoOnClickData);
export const HomePageVideo = () => homePageVideo(homePageVideoData);
export const VideoOnPopup = () => videoOnPopup(videoOnPopupData);
export const VideoOnTabs = () => videoOnTab(videoOnTabData);

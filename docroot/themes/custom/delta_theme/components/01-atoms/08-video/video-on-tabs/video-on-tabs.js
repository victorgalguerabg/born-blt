import jQuery from 'jquery';

Drupal.behaviors.videoOnTabs = {
  attach: function (context, settings) {
  //(function () { // REMOVE IF DRUPAL.

  'use strict';

  if((".active-vot-tab-block").length) {
    var vid = document.getElementById(jQuery(".active-vot-video-block").find("video").attr("id"));
    vid.autoplay = true;
    vid.load();
  }

  jQuery(".vot-tab-block").click(function(e) {
    e.preventDefault();
    var curTab = jQuery(this).index();
    jQuery(".vot-tab-block").removeClass("active-vot-tab-block");
    jQuery(".vot-tab-desc").removeClass("active-vot-tab-desc");
    jQuery(this).addClass("active-vot-tab-block");
    jQuery(".vot-video-block").removeClass("active-vot-video-block");
    jQuery(".vot-video-wrapper .vot-video-block").eq(curTab).addClass("active-vot-video-block");
    jQuery(".vot-tab-desc-wrapper .vot-tab-desc").eq(curTab).addClass("active-vot-tab-desc");

    var vid = document.getElementById(jQuery(".active-vot-video-block").find("video").attr("id"));
    vid.autoplay = true;
    vid.load();
  });

//})(); // REMOVE IF DRUPAL
  }
};

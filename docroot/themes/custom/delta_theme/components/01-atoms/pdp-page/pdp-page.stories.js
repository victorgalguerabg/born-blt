// PDP Pages Stories
import pdpCollection from './pdp-collection/pdp-collection.twig';
import pdpCollectionLink from './pdp-collection-link/pdp-collection-link.twig';
import pdpLabel from './pdp-label/pdp-label.twig';
import pdpModel from './pdp-model/pdp-model.twig';
import pdpProductName from './pdp-product-name/pdp-product-name.twig';
import pdpProductPrice from './pdp-product-price/pdp-product-price.twig';
import pdpWarning from './pdp-warning/pdp-warning.twig';

import pdpCollectionData from './pdp-collection/pdp-collection.yml';
import pdpCollectionLinkData from './pdp-collection-link/pdp-collection-link.yml';
import pdpLabelData from './pdp-label/pdp-label.yml';
import pdpModelData from './pdp-model/pdp-model.yml';
import pdpProductNameData from './pdp-product-name/pdp-product-name.yml';
import pdpProductPriceData from './pdp-product-price/pdp-product-price.yml';
import pdpWarningData from './pdp-warning/pdp-warning.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Atoms/PDP Pages' };

export const PdpCollection = () => pdpCollection(pdpCollectionData);
export const PdpCollectionLink = () => pdpCollectionLink(pdpCollectionLinkData);
export const PdpLabel = () => pdpLabel(pdpLabelData);
export const PdpModel = () => pdpModel(pdpModelData);
export const PdpProductName = () => pdpProductName(pdpProductNameData);
export const PdpProductPrice = () => pdpProductPrice(pdpProductPriceData);
export const PdpWarning = () => pdpWarning(pdpWarningData);

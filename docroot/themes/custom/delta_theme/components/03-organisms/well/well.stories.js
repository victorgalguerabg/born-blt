// Well Stories
import wellVarOne from './well-var-1.twig';
import wellVarTwo from './well-var-2.twig';
import wellVarThree from './well-var-3.twig';
import wellVarFour from './well-var-4.twig';
import wellVarFive from './well-var-5.twig';

import wellVarOneData from './well-var-1.yml';
import wellVarTwoData from './well-var-2.yml';
import wellVarThreeData from './well-var-3.yml';
import wellVarFourData from './well-var-4.yml';
import wellVarFiveData from './well-var-5.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Organisms/Well' };

export const wellVarsOne = () => wellVarOne(wellVarOneData);
export const wellVarsTwo = () => wellVarTwo(wellVarTwoData);
export const wellVarsThree = () => wellVarThree(wellVarThreeData);
export const wellVarsFour = () => wellVarFour(wellVarFourData);
export const wellVarsFive = () => wellVarFive(wellVarFiveData);
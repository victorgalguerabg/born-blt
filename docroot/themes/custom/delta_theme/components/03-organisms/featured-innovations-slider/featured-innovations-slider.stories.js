// Feature Innovation Slider Stories
import featureInnovationSlider from './featured-innovations-slider.twig';

import featureInnovationSliderData from './featured-innovations-slider.yml';

import './featured-innovations-slider';

/**
 * Storybook Definition.
 */
export default { title: 'Organisms/Feature Innovation Slider' };

export const FeatureInnovationSlider = () => featureInnovationSlider(featureInnovationSliderData);
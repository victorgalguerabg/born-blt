(function() {
  jQuery('.featured-innoslider').slick();

  jQuery('.featured-innoslider').on('afterChange', function(event, slick, currentSlide) {
    const videoIframe = jQuery('.featured-innoslider-video iframe');

    if (!videoIframe) {
      return;
    }
    if(jQuery('.featured-innoslider').find('.slick-current').data('video-url')){
      videoIframe.attr('src', jQuery('.featured-innoslider').find('.slick-current').data('video-url'))
    }
  });
})();

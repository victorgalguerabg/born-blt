// Download Card Grid Stories
import downloadCardGrid from './download-card-grid.twig';

import downloadCardGridData from './download-card-grid.yml';
import downloadCardGridListViewData from './download-card-grid~list-view.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Organisms/Download Card Grid' };

export const DownloadCardGrid = () => downloadCardGrid(downloadCardGridData);
export const DownloadCardGridListView = () => downloadCardGrid(downloadCardGridListViewData);
Drupal.behaviors.modal = {
  attach: function (context, settings) {

//(function() {
  const containers = queryAll('[data-dialog]');

  containers.forEach(Modal);

  function Modal(container) {
    var dialog = query('[role="dialog"]', container);
    var siteContent = getSiblings(container);
    var openers = queryAll(`[data-dialog-show="${container.id}"]`);
    var closers = queryAll(`[data-dialog-hide="${container.id}"]`)
      .concat(queryAll('[data-dialog-hide]', container));
    var shown = container.getAttribute('aria-hidden') !== 'true';

    if (!container.id) {
      throw new Error('The dialog container must have an `id` attribute specified.')
    }

    init();

    function init() {
      openers.forEach((opener) => {
        opener.addEventListener('click', show)
      });

      closers.forEach((opener) => {
        opener.addEventListener('click', hide)
      });
    }

    function show(e) {

      e.preventDefault();

      shown = true;
      document.documentElement.classList.add('has-open-dialog');
      container.style.display = 'block';
      setTimeout(function() {
        container.removeAttribute('aria-hidden');
      }, 5);
      siteContent.forEach((element) => {
        element.setAttribute('aria-hidden', "true");

        if (container.querySelector('.modal-gallery')) {
          jQuery('.modal-gallery').resize();
        }

      });
      dialog.addEventListener('transitionend', hideContainer)
    }

    function hide() {

      shown = false;
      document.documentElement.classList.remove('has-open-dialog');
      container.style.display = 'none';
      container.setAttribute('aria-hidden', true);
      siteContent.forEach((element) => {
        element.removeAttribute('aria-hidden');
      });

      const video = container.querySelector('iframe');

      if (video) {
        window.requestAnimationFrame(() => {
          video.contentWindow.postMessage('{"event":"command","func":"' + 'stopVideo' + '","args":""}', '*');
        })
      }
    }

    function hideContainer(e) {
      if (!shown) {
        e.preventDefault();
        container.style.display = 'none';
        dialog.removeEventListener('transitionend', hideContainer)
      }
    }
    document.onkeydown = function(evt) {
      evt = evt || window.event;
      if (evt.keyCode == 27) {
        hide()
      }
    };
  }
//})();



function query(selector, context = document) {
  return context.querySelector(selector);
}

function queryAll(selector, context = document) {
  var nodeList = context.querySelectorAll(selector);

  if (!nodeList) {
    return null;
  }

  return [].slice.call(nodeList);
}

function getSiblings(node) {
  var nodes = [].slice.call(node.parentNode.childNodes);
  var siblings = nodes.filter((node) => node.nodeType === 1);
  console.log(nodes);
  siblings.splice(siblings.indexOf(node), 1);

  return siblings;
}

  }
};


// Modal Stories
import modal from './modals.twig';

import modalData from './modals.yml';

import './modal';

/**
 * Storybook Definition.
 */
export default { title: 'Organisms/Modal' };

export const Modal = () => modal(modalData);
// PDP Section Stories
import pdpSection from './pdp-top-section.twig';

import pdpSectionData from './pdp-top-section.yml'

/**
 * Storybook Definition.
 */
export default { title: 'Organisms/PDP Section' };

export const PDPSection = () => pdpSection(pdpSectionData);
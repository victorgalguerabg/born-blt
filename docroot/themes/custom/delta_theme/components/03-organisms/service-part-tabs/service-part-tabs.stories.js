// Service Part Tab Stories
import servicePartTab from './service-part-tabs.twig';

import servicePartTabData from './service-part-tabs.yml';

import './service-part-tabs';

/**
 * Storybook Definition.
 */
export default { title: 'Organisms/Service Part Tab' };

export const servicePartTabs = () => servicePartTab(servicePartTabData);
(function() {
  const wrapper = document.querySelector('.service-part-tabs');

  if (!wrapper) {
    return;
  }

  const tabs = new Tabby('.service-parts__tablist');
  const customLinks = [].slice.call(wrapper.querySelectorAll('[data-tab-link]'));

  document.addEventListener('tabby', function(event) {
    const tab = event.target;
    const li = tab.parentNode;
    const list = li.parentNode;
    const activeItem = list.parentNode.querySelector('li.is-active');

    if (activeItem) {
      activeItem.classList.remove('is-active');
    }

    li.classList.add('is-active');
  });

  jQuery(".service-tab-trigger").click(function(e) {
    e.preventDefault();
  });

  document.addEventListener('click', function(e) {
    if (e.target.hasAttribute('data-tab-link')) {
      e.preventDefault();
      const target = e.target.getAttribute('href');
      tabs.toggle(target);
    }
  });
})();

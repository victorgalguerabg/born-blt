// Service Part Tab Stories
import siteFooter from './site-footer/site-footer.twig';
import siteHeader from './site-header/site-header.twig';

import siteFooterData from './site-footer/site-footer.yml';
import siteHeaderData from './site-header/site-header.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Organisms/Site' };

export const siteFooters = () => siteFooter(siteFooterData);
export const siteHeaders = () => siteHeader(siteHeaderData);
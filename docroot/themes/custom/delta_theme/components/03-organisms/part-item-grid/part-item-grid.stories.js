// Part Item Grid Stories
import partItemGrid from './part-item-grid.twig';

import partItemGridData from './part-item-grid.yml'

/**
 * Storybook Definition.
 */
export default { title: 'Organisms/Part Item Grid' };

export const PartItemGrid = () => partItemGrid(partItemGridData);
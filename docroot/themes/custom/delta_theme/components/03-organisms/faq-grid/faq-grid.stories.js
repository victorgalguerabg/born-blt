// Faq Grid Stories
import faqGrid from './faq-grid.twig';

import faqGridData from './faq-grid.yml';

import './faq-grid';

/**
 * Storybook Definition.
 */
export default { title: 'Organisms/Faq Grid' };

export const FaqGrid = () => faqGrid(faqGridData);
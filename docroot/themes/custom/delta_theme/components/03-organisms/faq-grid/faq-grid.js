(function() {
  const faqButtons = [].slice.call(document.querySelectorAll('.faq-card__button'));
  const modal = document.getElementById('faq-modal');
  const modalBody = modal.querySelector('.modal__body');

  faqButtons.forEach(function(button) {
    button.addEventListener('click', function(e) {
      const card = this.parentNode;
      const indexNode = card.querySelector('.faq-card__index').cloneNode(true);
      const answerNode = card.querySelector('.faq-card__answer').cloneNode(true);
      const imageNode = card.querySelector('.faq-card__img').cloneNode(true);
      const questionNode = document.createElement('h6');
      const modalFaqContent = document.createElement('div');
      modalFaqContent.classList.add('modal--faq__content');
      const modalFaqFigure = document.createElement('figure');
      modalFaqFigure.classList.add('modal--faq__figure');
      modalFaqFigure.appendChild(imageNode);

      answerNode.classList.remove('hidden');
      questionNode.innerHTML = card.querySelector('.faq-card__question').innerHTML;


      modalBody.innerHTML = '';
      modalFaqContent.appendChild(indexNode);
      modalFaqContent.appendChild(questionNode);
      modalFaqContent.appendChild(answerNode);
      modalBody.appendChild(modalFaqContent);
      modalBody.appendChild(modalFaqFigure);
    });
  });
})();

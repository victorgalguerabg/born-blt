// Blog Card Grid Stories
import blogCardGrid from './blog-card-grid.twig';

import blogCardGridData from './blog-card-grid.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Organisms/Blog Card Grid' };

export const BlogCardGrid = () => blogCardGrid(blogCardGridData);
// Summary Card Carousel Stories
import summaryCardCarousel from './summary-card-carousel.twig';

import summaryCardCarouselData from './summary-card-carousel.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Organisms/Summary Card Carousel' };

export const summaryCardCarousels = () => summaryCardCarousel(summaryCardCarouselData);
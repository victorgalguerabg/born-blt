// Product Tile Stories
import productTile from './product-tile.twig';
import productTileCollection from './product-tile-collection.twig';
import productTileRepair from './product-tile-repair.twig';

import productTileData from './product-tile.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Organisms/Product Tile' };

export const ProductTile = () => productTile(productTileData);
export const ProductTileCollection = () => productTileCollection();
export const ProductTileRepair = () => productTileRepair();
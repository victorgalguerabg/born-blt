// Video Banner Stories
import tabbedCarousel from './tabbed-carousel.twig';

/**
 * Storybook Definition.
 */
export default { title: 'Organisms/Tab Carousel' };

export const tabbedCarousels = () => tabbedCarousel();
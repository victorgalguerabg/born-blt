// Banner Stories
import banner from './03-banner-title--large/banner.twig';
import featureBanner from './04-banner-featured/featured-banner.twig';
import bannerTitle from './banner-title--fw/banner-title--fw.twig';
import bannerTitleMd from './banner-title--md/banner-title--md.twig';
import bannerTitleSm from './banner-title--sm/banner-title--sm.twig';

import bannerData from './03-banner-title--large/banner.yml';
import featureBannerData from './04-banner-featured/featured-banner.yml';
import featureBannerContentCenteredLightData from './04-banner-featured/featured-banner~content-centered-light.yml';
import featureBannerContentCenteredVideoData from './04-banner-featured/featured-banner~content-centered-video.yml';
import featureBannerLightData from './04-banner-featured/featured-banner~light.yml';
import featureBannerTextCenteredDarkData from './04-banner-featured/featured-banner~text-centered-dark.yml';
import featureBannerTextCenteredLightData from './04-banner-featured/featured-banner~text-centered-light.yml';
import bannerTitleData from './banner-title--fw/banner-title--fw.yml';
import bannerTitleAltData from './banner-title--fw/banner-title--fw~alt.yml';
import bannerTitleAltMdData from './banner-title--md/banner-title--md~alt.yml';
import bannerTitleAltSmData from './banner-title--sm/banner-title--sm~alt.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Organisms/Banner' };

export const Banner = () => banner(bannerData);
export const FeatureBanner = () => featureBanner(featureBannerData);
export const featureBannerContentCenteredLight = () => featureBanner(featureBannerContentCenteredLightData);
export const featureBannerContentCenteredVideo = () => featureBanner(featureBannerContentCenteredVideoData);
export const featureBannerLight = () => featureBanner(featureBannerLightData);
export const featureBannerTextCenteredDark = () => featureBanner(featureBannerTextCenteredDarkData);
export const featureBannerTextCenteredLight = () => featureBanner(featureBannerTextCenteredLightData);
export const BannerTitle = () => bannerTitle(bannerTitleData);
export const BannerTitleAlt = () => bannerTitle(bannerTitleAltData);
export const BannerTitleAltMd = () => bannerTitleMd(bannerTitleAltMdData);
export const BannerTitleAltSm = () => bannerTitleSm(bannerTitleAltSmData);


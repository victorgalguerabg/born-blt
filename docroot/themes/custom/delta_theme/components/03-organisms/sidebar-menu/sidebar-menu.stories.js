// Sidebar Menu Stories
import sidebarMenu from './sidebar-menu.twig';

import sidebarMenuData from './sidebar-menu.yml';

import './sidebar-menu';

/**
 * Storybook Definition.
 */
export default { title: 'Organisms/Sidebar Menu' };

export const sidebarMenus = () => sidebarMenu(sidebarMenuData);
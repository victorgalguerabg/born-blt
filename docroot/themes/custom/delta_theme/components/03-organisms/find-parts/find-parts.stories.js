// Find Parts Stories
import findParts from './find-parts.twig';

import findPartsData from './find-parts.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Organisms/Find Parts' };

export const FindParts = () => findParts(findPartsData);
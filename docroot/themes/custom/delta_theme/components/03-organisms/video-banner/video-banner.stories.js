// Video Banner Stories
import videoBanner from './video-banner.twig';

import videoBannerData from './video-banner.yml';

import './video-banner';

/**
 * Storybook Definition.
 */
export default { title: 'Organisms/Video Banner' };

export const videoBanners = () => videoBanner(videoBannerData);
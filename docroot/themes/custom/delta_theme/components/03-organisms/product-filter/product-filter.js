(function() {
  const toggle = document.querySelector('.js-filter-toggle');
  const filter = document.querySelector('.product-filter');
  const body = document.querySelector('.product-filter__body');
  const title = document.querySelector('.product-filter__mobile-title');
  
  if (!toggle || !body) {
    return;
  }
  
  function handleToggleClick() {
    const isActive = toggle.classList.contains('toggle-expand--open');
  
    document.documentElement.classList.toggle('has-open-dialog', !isActive);
    toggle.classList.toggle('toggle-expand--open', !isActive);
    filter.classList.toggle('is-active', !isActive);
    title.innerText = isActive ? 'Filter By' : 'Close Filters'
  }
  
  toggle.addEventListener('click', handleToggleClick);
})();

// Product Filter Stories
import productFilter from './product-filter.twig';

/**
 * Storybook Definition.
 */
export default { title: 'Organisms/Product Filter' };

export const ProductFilter = () => productFilter();
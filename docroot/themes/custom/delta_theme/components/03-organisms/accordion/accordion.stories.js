// Accordion Stories
import accordion from './accordion.twig';
import leftIconAccordion from './left-icon-accordion.twig';

import accordionData from './accordion.yml';
import leftIconAccordionData from './left-icon-accordion.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Organisms/Accordion' };

export const Accordion = () => accordion(accordionData);
export const LeftIconAccordion = () => leftIconAccordion(leftIconAccordionData);

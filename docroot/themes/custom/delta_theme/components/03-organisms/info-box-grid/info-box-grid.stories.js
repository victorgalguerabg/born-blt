// Info Box Grid Stories
import infoBoxGrid from './info-box-grid.twig';

import infoBoxGridData from './info-box-grid.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Organisms/Info Box Grid' };

export const InfoBoxGrid = () => infoBoxGrid(infoBoxGridData);
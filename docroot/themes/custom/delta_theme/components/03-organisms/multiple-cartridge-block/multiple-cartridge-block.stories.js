// Multiple Cartridge Block Stories
import multipleCartridgeBlock from './multiple-cartridge-block.twig';

import multipleCartridgeBlockData from './multiple-cartridge-block.yml'

/**
 * Storybook Definition.
 */
export default { title: 'Organisms/Multiple Cartridge Block' };

export const MultipleCartridgeBlock = () => multipleCartridgeBlock(multipleCartridgeBlockData);
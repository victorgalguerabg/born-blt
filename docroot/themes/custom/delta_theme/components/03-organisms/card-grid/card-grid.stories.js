// Card Grid Stories
import cardGrid from './card-grid.twig';

import cardGridData from './card-grid.yml';
import cardGridAltData from './card-grid~alt.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Organisms/Card Grid' };

export const CardGrid = () => cardGrid(cardGridData);
export const CardGridAlt = () => cardGrid(cardGridAltData);
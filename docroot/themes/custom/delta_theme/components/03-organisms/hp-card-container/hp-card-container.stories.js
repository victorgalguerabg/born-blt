// HP Card Container Stories
import hpCardContainer from './hp-card-container.twig';

import hpCardContainerData from './hp-card-container.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Organisms/HP Card Container' };

export const HPCardContainer = () => hpCardContainer(hpCardContainerData);
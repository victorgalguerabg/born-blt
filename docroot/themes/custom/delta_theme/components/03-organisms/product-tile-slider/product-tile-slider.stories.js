// Product Tile Stories
import productTileSlider from './product-tile-slider.twig';

import productTileSliderData from './product-tile-slider.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Organisms/Product Tile' };

export const ProductTileSlider = () => productTileSlider(productTileSliderData);
// Alt Image Grid Stories
import altImgGrid from './alt-img-grid.twig';

import altImgGridData from './alt-img-grid.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Organisms/Alt Image Grid' };

export const AltImgGrid = () => altImgGrid(altImgGridData);
// Modal Stories
import modalLogin from './login-modal.twig';

/**
 * Storybook Definition.
 */
export default { title: 'Organisms/Modal' };

export const ModalLogin = () => modalLogin();
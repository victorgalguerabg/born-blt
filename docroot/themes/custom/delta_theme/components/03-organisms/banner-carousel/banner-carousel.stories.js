// Banner Carousel Stories
import bannerCarousel from './banner-carousel.twig';

import bannerCarouselData from './banner-carousel.yml';

import './banner-carousel';

/**
 * Storybook Definition.
 */
export default { title: 'Organisms/Banner Carousel' };

export const BannerCarousel = () => bannerCarousel(bannerCarouselData);
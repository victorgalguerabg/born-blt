(function() {
  $('.banner-carousel').slick({
    arrows: true,
    dots: false,
    autoplay: false,
    responsive: [
      {
        breakpoint: 1020,
        settings: {
          arrows: false,
          dots: true,
        },
      },
    ],
  });
})();

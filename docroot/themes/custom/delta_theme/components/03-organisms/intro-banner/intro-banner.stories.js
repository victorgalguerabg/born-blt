// Intro Banner Stories
import introBanner from './intro-banner.twig';

import introBannerData from './intro-banner.yml';
import introBannerWithCtaData from './intro-banner~with-cta.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Organisms/Intro Banner' };

export const IntroBanner = () => introBanner(introBannerData);
export const IntroBannerWithCTA = () => introBanner(introBannerWithCtaData);
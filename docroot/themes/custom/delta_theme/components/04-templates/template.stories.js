// Template Stories
import fullWidth from './full-width.twig';
import withSidebar from './with-sidebar.twig';

/**
 * Storybook Definition.
 */
export default { title: 'Template/Template' };

export const FullWidth = () => fullWidth();
export const Sidebar = () => withSidebar();

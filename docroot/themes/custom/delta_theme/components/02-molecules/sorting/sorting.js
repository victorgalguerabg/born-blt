(function () {
  jQuery("document").ready(function() {
    jQuery(".filter-container").each(function(){
      jQuery(this).html(jQuery(this).children('.custom-input.custom-input--checkbox').sort(function(a, b){
      	return (jQuery(b).data('position')) < (jQuery(a).data('position')) ? 1 : -1;
      }));
    });



    jQuery(".filter-container").each(function(){
      var sum = 0;
      var showval = 1;

      jQuery(this).children(".custom-input.custom-input--checkbox").each(function(){

        if(jQuery(this).index() <= showval) {
          var varheight = 0;
          varheight = jQuery(this).outerHeight();
          sum = sum + varheight;
          console.log("index:" + jQuery(this).index());
          console.log("original:" + jQuery(this).outerHeight());
          console.log("height:" + jQuery(this).height());
          console.log("addition:" + sum);
        }

      });

      jQuery(this).css("max-height", sum);

      console.log("final:" + sum);
    });



    jQuery(".product-filter-more").click(function() {
      jQuery(this).prev(".filter-container").toggleClass("show-filter-container");
    });


  });
})();

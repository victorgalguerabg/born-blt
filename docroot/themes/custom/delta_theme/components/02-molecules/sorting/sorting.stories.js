// Sorting Stories
import sorting from './sorting.twig';

import sortingData from './sorting.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Molecules/Sorting' };

export const Sorting = () => sorting(sortingData);

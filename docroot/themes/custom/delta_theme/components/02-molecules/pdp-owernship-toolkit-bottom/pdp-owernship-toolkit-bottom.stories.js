// PDP Owernship Toolkit Bottom Stories
import pdpOwernshipToolkit from './pdp-owernship-toolkit-bottom.twig';

import pdpOwernshipToolkitData from './pdp-owernship-toolkit-bottom.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Molecules/PDP Ownership Toolkit Bottom' };

export const pdpOwnershipToolkit = () => pdpOwernshipToolkit(pdpOwernshipToolkitData);

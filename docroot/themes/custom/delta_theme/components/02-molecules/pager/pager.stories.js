// Pager Stories
import pager from './pager.twig';

import pagerData from './pager.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Molecules/Pager' };

export const Pager = () => pager(pagerData);

// HomePage Intro Banner Stories
import homePageIntroBanner from './homepage-intro-banner.twig';

import homePageIntroBannerData from './homepage-intro-banner.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Molecules/HomePage Intro Banner' };

export const HomePageIntroBanner = () => homePageIntroBanner(homePageIntroBannerData);

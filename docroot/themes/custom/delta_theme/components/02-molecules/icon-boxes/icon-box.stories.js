// Icon Box Stories
import iconBox from './icon-boxes.twig';
import iconBoxTop from './icon-boxes-top.twig';
import iconBoxTopCenter from './icon-boxes-top-center.twig';
import iconBoxWithoutTitle from './icon-boxes-witout-title.twig';

import iconBoxData from './icon-boxes.yml';
import iconBoxLargeData from './icon-boxes~large.yml';
import iconBoxMediumData from './icon-boxes~medium.yml';
import iconBoxTopData from './icon-boxes-top.yml';
import iconBoxTopCenterData from './icon-boxes-top-center.yml';
import iconBoxWithoutTitleData from './icon-boxes-witout-title.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Molecules/Icon Box' };

export const IconBox = () => iconBox(iconBoxData);
export const IconBoxLarge = () => iconBox(iconBoxLargeData);
export const IconBoxMedium = () => iconBox(iconBoxMediumData);
export const IconBoxTop = () => iconBoxTop(iconBoxTopData);
export const IconBoxTopCenter = () => iconBoxTopCenter(iconBoxTopCenterData);
export const IconBoxWithoutTitle = () => iconBoxWithoutTitle(iconBoxWithoutTitleData);

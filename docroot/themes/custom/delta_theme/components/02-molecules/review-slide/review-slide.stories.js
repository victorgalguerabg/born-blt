// Review Slide Stories
import reviewSlide from './review-slide.twig';

import reviewSlideData from './review-slide.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Molecules/Review Slide' };

export const ReviewSlide = () => reviewSlide(reviewSlideData);

// Compare Block Stories
import compareBlock from './compare-block.twig';

import compareBlockData from './compare-block.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Molecules/Compare Block' };

export const CompareBlock = () => compareBlock(compareBlockData);

// Search Block Stories
import searchBlock from './search-block.twig';
import searchPartsBlock from './search-parts-block.twig';
import searchResultDropdown from './search-result-dropdown/search-result-dropdown.twig';

import searchResultDropdownData from './search-result-dropdown/search-result-dropdown.yml';

import './search-block';

/**
 * Storybook Definition.
 */
export default { title: 'Molecules/Search Block' };

export const SearchBlock = () => searchBlock();
export const SearchPartsBlock = () => searchPartsBlock();
export const SearchResultDropdown = () => searchResultDropdown(searchResultDropdownData);

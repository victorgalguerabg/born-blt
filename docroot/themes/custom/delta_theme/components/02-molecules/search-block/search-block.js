/**
 * @file
 * A JavaScript file containing the main menu functionality (small/large screen)
 *
 */

// JavaScript should be made compatible with libraries other than jQuery by
// wrapping it with an "anonymous closure". See:
// - https://drupal.org/node/1446420
// - http://www.adequatelygood.com/2010/3/JavaScript-Module-Pattern-In-Depth


// (function (Drupal) { // UNCOMMENT IF DRUPAL.
//
//   Drupal.behaviors.mainMenu = {
//     attach: function (context) {

(function () { // REMOVE IF DRUPAL.

  'use strict';

  // Use context instead of document IF DRUPAL.
  var search_input = document.getElementById('edit-search-input');
  var search_wrapper = document.getElementById('search-wrapper');
  var mo_search_icon = document.getElementById('mobile-serach-icon-block');

  //close-button
  jQuery(".search-input-block").append('<i class="input-close hide"></i>');

  jQuery('.search-input-block .input-close').click(function () {
    jQuery('form#global-search-form input.autocomplete').val('');
    jQuery('#search-block-suggestion-result-parts').empty();
    jQuery('.search-input-block .input-close').addClass('hide');
  });

  jQuery('.search-input-block').focus(function () {
    jQuery('.search-input-block .input-close').removeClass('hide');
  });
  //search-button
  //jQuery(".mobile-header-icons").insertBefore(".mobile-cart-block a.cart-icon");

  jQuery("#mobile-serach-icon-block").click(function(){
    search_wrapper.classList.toggle('mobile-search-active');
  });

  jQuery(".main-nav-block  .search-submit-block .glyphicon.glyphicon-search").click(function(){
    search_wrapper.classList.remove('mobile-search-active');
  });


  jQuery(document).mouseup(e => {
     if (!jQuery("#global-search-form").is(e.target) // if the target of the click isn't the container...
     && jQuery("#global-search-form").has(e.target).length === 0) // ... nor a descendant of the container
     {
       jQuery(".header .search-wrapper").removeClass("search-active");
    }
   });

  jQuery(".header .header-serch-input").click(function (e) {
    jQuery(".header .search-wrapper").addClass("search-active");
  });

})(); // REMOVE IF DRUPAL.

// })(Drupal); // UNCOMMENT IF DRUPAL.

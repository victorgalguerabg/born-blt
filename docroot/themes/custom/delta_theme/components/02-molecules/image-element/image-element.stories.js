// Image Element Stories
import imageElement from './image-element.twig';

import imageElementData from './image-element.yml';
import imageElementCAData from './image-element~caption-arrow.yml';
import imageElementCData from './image-element~caption.yml';
import imageElementRAData from './image-element~right-arrow.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Molecules/Image Element' };

export const ImageElement = () => imageElement(imageElementData);
export const ImageElementCaptionArrow = () => imageElement(imageElementCAData);
export const ImageElementCaption = () => imageElement(imageElementCData);
export const ImageElementRightArrow = () => imageElement(imageElementRAData);

// Summary Card Stories
import summaryCard from './01-summary-card.twig';
import summaryCardLargeImage from './02-summary-card-large-image.twig';
import summaryCardArbitraryContent from './03-summary-card-arbitrary-content.twig';

import summaryCardData from './01-summary-card.yml';
import summaryCardLargeImageData from './02-summary-card-large-image.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Molecules/Summary Card' };

export const SummaryCard = () => summaryCard(summaryCardData);
export const SummaryCardLargeImage = () => summaryCardLargeImage(summaryCardLargeImageData);
export const SummaryCardArbitraryContent = () => summaryCardArbitraryContent();

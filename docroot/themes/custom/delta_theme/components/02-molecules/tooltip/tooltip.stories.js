// Tooltip Stories
import tooltip from './tooltip.twig';
import tooltipPdp from './tooltip-pdp.twig';

import tooltipData from './tooltip.yml';
import tooltipPdpData from './tooltip-pdp.yml';

import './tooltip';

/**
 * Storybook Definition.
 */
export default { title: 'Molecules/Tooltip' };

export const Tooltip = () => tooltip(tooltipData);
export const TooltipPdp = () => tooltipPdp(tooltipPdpData);

// Shower Spary Pattern Stories
import showerSparyPattern from './shower-spary-pattern.twig';

import showerSparyPatternData from './shower-spary-pattern.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Molecules/Shower Spary Pattern' };

export const ShowerSparyPattern = () => showerSparyPattern(showerSparyPatternData);

// Quote Image Desc Link Img CTA Stories
import quoteImageDescLinkImgCTA from './quote-image-desc-link-img-cta.twig';

import quoteImageDescLinkImgCTAData from './quote-image-desc-link-img-cta.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Molecules/Quote Image Desc Link Img CTA' };

export const QuoteImageDescLinkImgCTA = () => quoteImageDescLinkImgCTA(quoteImageDescLinkImgCTAData);

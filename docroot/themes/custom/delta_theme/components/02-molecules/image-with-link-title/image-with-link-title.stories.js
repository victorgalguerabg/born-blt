// Image With Link Title Stories
import imageWithLinkTitle from './image-with-link-title.twig';

import imageWithLinkTitleData from './image-with-link-title.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Molecules/Image With Link Title' };

export const ImageWithLinkTitle = () => imageWithLinkTitle(imageWithLinkTitleData);

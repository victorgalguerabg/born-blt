// Related Product Stories
import relatedProduct from './related-product.twig';

import relatedProductData from './related-product.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Molecules/Related Product' };

export const RelatedProduct = () => relatedProduct(relatedProductData);

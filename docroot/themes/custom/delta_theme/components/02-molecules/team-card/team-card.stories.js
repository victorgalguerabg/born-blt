// Team Card Stories
import teamCard from './team-card.twig';

import teamCardData from './team-card.yml';
import teamCardCenterData from './team-card~centered.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Molecules/Team Card' };

export const TeamCard = () => teamCard(teamCardData);
export const TeamCardCenter = () => teamCard(teamCardCenterData);

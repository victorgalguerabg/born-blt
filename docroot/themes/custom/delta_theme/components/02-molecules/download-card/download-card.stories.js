// Download Card Stories
import downloadCard from './download-card.twig';

import downloadCardData from './download-card.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Molecules/Download Card' };

export const DownloadCard = () => downloadCard(downloadCardData);

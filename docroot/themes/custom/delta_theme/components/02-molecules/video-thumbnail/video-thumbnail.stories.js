// Video Thumbnail Stories
import videoThumbnail from './_video-thumbnail.twig';

import videoThumbnailData from './video-thumbnail.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Molecules/Video Thumbnail' };

export const VideoThumbnail = () => videoThumbnail(videoThumbnailData)

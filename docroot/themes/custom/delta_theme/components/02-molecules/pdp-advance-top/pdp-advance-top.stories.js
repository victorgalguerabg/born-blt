// Pdp Advance Top Stories
import pdpAdvanceTop from './pdp-advance-top.twig';

import pdpAdvanceTopData from './pdp-advance-top.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Molecules/Pdp Advance Top' };

export const PdpAdvanceTop = () => pdpAdvanceTop(pdpAdvanceTopData);

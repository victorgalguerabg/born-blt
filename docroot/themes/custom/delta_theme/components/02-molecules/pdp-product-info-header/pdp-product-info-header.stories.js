// Pdp Product Info Header Stories
import pdpProductInfoHeader from './pdp-product-info-header.twig';
import pdpProductInfoHeaderReplacement from './pdp-product-info-header-replacement.twig';

import pdpProductInfoHeaderData from './pdp-product-info-header.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Molecules/Pdp Product Info Header' };

export const PdpProductInfoHeader = () => pdpProductInfoHeader(pdpProductInfoHeaderData);
export const PdpProductInfoHeaderReplacement = () => pdpProductInfoHeaderReplacement(pdpProductInfoHeaderData);

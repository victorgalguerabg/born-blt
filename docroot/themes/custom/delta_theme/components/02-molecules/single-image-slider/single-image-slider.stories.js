// Single Image Slider Stories
import singleImageSlider from './single-image-slider.twig';

import singleImageSliderData from './single-image-slider.yml';

import './single-image-slider';

/**
 * Storybook Definition.
 */
export default { title: 'Molecules/Single Image Slider' };

export const SingleImageSlider = () => singleImageSlider(singleImageSliderData);

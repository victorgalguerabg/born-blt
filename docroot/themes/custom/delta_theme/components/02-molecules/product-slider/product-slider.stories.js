// Product Slider Stories
import productSlider from './product-slider.twig';

import productSliderData from './product-slider.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Molecules/Product Slider' };

export const ProductSlider = () => productSlider(productSliderData);

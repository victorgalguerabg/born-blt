// Info Box Stories
import infoBox from './info-box.twig';

import infoBoxData from './info-box.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Molecules/Info Box' };

export const InfoBox = () => infoBox(infoBoxData);

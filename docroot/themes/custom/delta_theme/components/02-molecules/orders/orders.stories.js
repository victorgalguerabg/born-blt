// Order Stories
import order from './orders.twig';

import orderData from './orders.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Molecules/Order' };

export const orders = () => order(orderData);

// Title Desc Link Image Stories
import titleDescLinkImage from './title-desc-link-image.twig';

import titleDescLinkImageData from './title-desc-link-image.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Molecules/Title Desc Link Image' };

export const TitleDescLinkImage = () => titleDescLinkImage(titleDescLinkImageData)

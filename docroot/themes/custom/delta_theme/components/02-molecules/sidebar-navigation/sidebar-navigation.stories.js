// Sidebar Navigation Stories
import sidebarNavigation from './sidebar-navigation.twig';

import sidebarNavigationData from './sidebar-navigation.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Molecules/Sidebar Navigation' };

export const SidebarNavigation = () => sidebarNavigation(sidebarNavigationData);

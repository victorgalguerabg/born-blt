// Catalog Four Grid Stories
import catalogFourGrid from './catalog-four-grid.twig';

import catalogFourGridData from './catalog-four-grid.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Molecules/Catalog Four Grid' };

export const CatalogFourGrid = () => catalogFourGrid(catalogFourGridData);

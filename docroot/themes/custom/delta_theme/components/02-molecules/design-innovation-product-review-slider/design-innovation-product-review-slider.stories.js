// Design Innovation Product Review Slider Stories
import designInnovationProductReviewSlider from './design-innovation-product-review-slider.twig';

import designInnovationProductReviewSliderData from './design-innovation-product-review-slider.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Molecules/Design Innovation Product Review Slider' };

export const DesignInnovationProductReviewSlider = () => designInnovationProductReviewSlider(designInnovationProductReviewSliderData);

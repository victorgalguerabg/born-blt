// Press Release Leftbar Stories
import pressReleaseLeftbar from './press-releae-leftbar.twig';

import pressReleaseLeftbarData from './press-releae-leftbar.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Molecules/Press Release Leftbar' };

export const PressReleaseLeftbar = () => pressReleaseLeftbar(pressReleaseLeftbarData);

// Catalog Listing Grid Stories
import catalogListingGrid from './catalog-listing_grid.twig';

import catalogListingGridData from './catalog-listing_grid.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Molecules/Catalog Listing Grid' };

export const CatalogListingGrid = () => catalogListingGrid(catalogListingGridData);

// Single Image Slider Out Caption Stories
import singleImageSliderOutCaption from './single-image-slider-out-caption.twig';

import singleImageSliderOutCaptionData from './single-image-slider-out-caption.yml';

//import './single-image-slider';

/**
 * Storybook Definition.
 */
export default { title: 'Molecules/Single Image Slider Out Caption' };

export const SingleImageSliderOutCaption = () => singleImageSliderOutCaption(singleImageSliderOutCaptionData);

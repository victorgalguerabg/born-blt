// Featured Innovation Tile Stories
import featureInnovationTile from './featured-innovation-tile.twig';

import featureInnovationTileData from './featured-innovation-tile.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Molecules/Featured Innovation Tile' };

export const FeatureInnovationTile = () => featureInnovationTile(featureInnovationTileData);

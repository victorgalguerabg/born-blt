// Text Button Box Stories
import textButtonBox from './text-button-box.twig';

import textButtonBoxData from './text-button-box.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Molecules/Text Button Box' };

export const TextButtonBox = () => textButtonBox(textButtonBoxData)

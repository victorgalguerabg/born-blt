// Image Title Carousel Stories
import imageTileCarousel from './image-tile-carousel.twig';

import imageTileCarouselData from './image-tile-carousel.yml';

import './image-tile-carousel';

/**
 * Storybook Definition.
 */
export default { title: 'Molecules/Image Tile Carousel' };

export const ImageTileCarousel = () => imageTileCarousel(imageTileCarouselData);

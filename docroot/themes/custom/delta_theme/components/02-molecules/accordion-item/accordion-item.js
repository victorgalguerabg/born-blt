// UNCOMMENT IF DRUPAL - see components/_meta/_01-foot.twig for attachBehaviors
// Drupal.behaviors.accordion = {
//   attach: function (context, settings) {

(function () { // REMOVE IF DRUPAL

  'use strict';

  // Set 'document' to 'context' if Drupal
  var accordionTerm = document.querySelectorAll('.accordion-term');
  var accordionDef = document.querySelectorAll('.accordion-def');

  // If javascript, hide accordion definition on load
  function jsCheck() {
    for (var i = 0; i < accordionDef.length; i++) {
      if (accordionDef[i].classList) {
        accordionDef[i].classList.add('active');
        accordionDef[0].previousElementSibling.classList.add('is-active');
      }
      else {
        accordionDef[i].className += ' active';
        accordionDef[0].previousElementSibling.classList.add('is-active');
      }
    }
  }

  //jsCheck();
  
  
  document.addEventListener('click', function(e) {
    if (e.target.classList.contains('accordion-term') || e.target.parentNode.classList.contains('accordion-term') ) {
      const className = 'is-active';
      const target = e.target.classList.contains('accordion-term') ? e.target : e.target.parentNode;
      
      if (this.classList) {
        target.classList.toggle(className);
      }
      else {
        const classes = target.className.split(' ');
        const existingIndex = classes.indexOf(className);
    
        if (existingIndex >= 0) {
          classes.splice(existingIndex, 1);
        }
        else {
          classes.push(className);
        }
        target.className = classes.join(' ');
      }
      e.preventDefault();
    }
  });

})(); // REMOVE IF DRUPAL

// UNCOMMENT IF DRUPAL
//   }
// };

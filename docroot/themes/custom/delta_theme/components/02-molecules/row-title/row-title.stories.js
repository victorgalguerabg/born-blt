// Row Title Stories
import rowTitle from './row-title.twig';

import rowTitleData from './row-title.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Molecules/Row Title' };

export const RowTitle = () => rowTitle(rowTitleData);

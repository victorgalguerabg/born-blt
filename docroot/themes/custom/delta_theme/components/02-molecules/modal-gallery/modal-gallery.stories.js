// Modal Gallery Stories
import modalGallery from './modal-gallery.twig';

import './modal-gallery';

/**
 * Storybook Definition.
 */
export default { title: 'Molecules/Modal Gallery' };

export const ModalGallery = () => modalGallery();

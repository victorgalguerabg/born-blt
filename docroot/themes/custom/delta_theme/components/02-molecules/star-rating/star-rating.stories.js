// Star Rating Stories
import starRating from './star-ratings.twig';

import starRatingData from './star-ratings.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Molecules/Star Rating' };

export const StarRating = () => starRating(starRatingData);

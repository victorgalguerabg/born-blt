// Compare Block Full Grid Stories
import compareBlockFullGrid from './compare-block-full-grid.twig';

import compareBlockFullGridData from './compare-block-full-grid.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Molecules/Compare Block Full Grid' };

export const CompareBlockFullGrid = () => compareBlockFullGrid(compareBlockFullGridData);

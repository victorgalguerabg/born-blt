// Card Stories
import card from './01-card.twig';

import cardData from './card.yml';
import cardBgData from './card~bg.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Molecules/Card' };

export const Card = () => card(cardData);
export const CardBg = () => card(cardBgData);

// disclaimer Stories
import desclamer from './disclaimer.twig';

import desclamerData from './disclaimer.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Molecules/Disclaimer' };

export const Desclaimer = () => desclamer(desclamerData);

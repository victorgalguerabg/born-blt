// Featured Innovation Tile Grid Stories
import featureInnovationTileGrid from './featured-innovation-tile-grid.twig';

import featureInnovationTileGridData from './featured-innovation-tile-grid.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Molecules/Featured Innovation Tile Grid' };

export const FeatureInnovationTileGrid = () => featureInnovationTileGrid(featureInnovationTileGridData);

// Title Image Desc Block Stories
import titleImageDescBlock from './title-image-desc-block.twig';

import titleImageDescBlockData from './title-image-desc-block.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Molecules/Title Image Desc Block' };

export const TitleImageDescBlock = () => titleImageDescBlock(titleImageDescBlockData)

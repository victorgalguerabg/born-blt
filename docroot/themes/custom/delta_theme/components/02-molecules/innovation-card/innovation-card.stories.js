// Innovation Card Stories
import innovationCard from './innovation-card.twig';

import innovationCardData from './innovation-card.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Molecules/Innovation Card' };

export const InnovationCard = () => innovationCard(innovationCardData);

// Press Release Search Stories
import pressReleaseSearch from './press-releae-search.twig';

import pressReleaseSearchData from './press-releae-search.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Molecules/Press Release Search' };

export const PressReleaseSearch = () => pressReleaseSearch(pressReleaseSearchData);

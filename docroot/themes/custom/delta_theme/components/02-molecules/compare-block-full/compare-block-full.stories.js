// Compare Block Full Stories
import compareBlockFull from './compare-block-full.twig';

import compareBlockFullData from './compare-block-full.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Molecules/Compare Block Full' };

export const CompareBlockFull = () => compareBlockFull(compareBlockFullData);

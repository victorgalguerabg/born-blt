// Issue Type Tool Stories
import issueTypeTool from './issue-type-tool.twig';

import issueTypeToolData from './issue-type-tool.yml';

import './issue-type-tool';

/**
 * Storybook Definition.
 */
export default { title: 'Molecules/Issue Type Tool' };

export const IssueTypeTool = () => issueTypeTool(issueTypeToolData);

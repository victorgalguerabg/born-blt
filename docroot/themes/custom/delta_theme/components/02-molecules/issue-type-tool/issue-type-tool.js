(function () {
  jQuery("document").ready(function() {
    jQuery('.issue-type-tool-wrapper .content-card').on("click",function(){
      jQuery(".issue-type-tool-wrapper .issue-header").css("display","block")
      jQuery("#issue-header-issue_room").html(jQuery(this).attr("accordian-header"));
      jQuery(".issue-type-tool-wrapper .content-card").each(function(){
        jQuery(this).removeClass("selected_room");
      });
      jQuery(this).addClass("selected_room");

      jQuery(".issue-type-tool-wrapper .issue-wrapper ").each(function(){
        jQuery(this).children().removeClass("display_selected_room");
      });
      jQuery(".issue-type-tool-wrapper .issue-wrapper").find(".issue-block-"+(jQuery(this).attr("data-primary"))).addClass("display_selected_room");

    });
  });
})();

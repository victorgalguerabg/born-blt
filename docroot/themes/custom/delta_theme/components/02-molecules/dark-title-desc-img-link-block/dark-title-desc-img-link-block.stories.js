// Dark Title Desc Image Link Block Stories
import darkTitleDescImgLinkBlock from './dark-title-desc-img-link-block.twig';

import darkTitleDescImgLinkBlockData from './dark-title-desc-img-link-block.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Molecules/Dark Title Desc Image Link Block' };

export const DarkTitleDescImgLinkBlock = () => darkTitleDescImgLinkBlock(darkTitleDescImgLinkBlockData);

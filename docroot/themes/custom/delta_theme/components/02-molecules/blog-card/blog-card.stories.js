// Blog Card Stories
import blogCard from './blog-card.twig';

import blogCardData from './blog-card.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Molecules/Blog Card ' };

export const BlogCard = () => blogCard(blogCardData);

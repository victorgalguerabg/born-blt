// Faq Listing Stories
import faqListing from './faq-listing.twig';

import faqListingData from './faq-listing.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Molecules/Faq Listing' };

export const FaqListing = () => faqListing(faqListingData);

// Pager Item Stories
import pagerItem from './part-item.twig';

import pagerItemData from './part-item.yml';

import './part-item'

/**
 * Storybook Definition.
 */
export default { title: 'Molecules/Pager Item' };

export const PagerItem = () => pagerItem(pagerItemData);

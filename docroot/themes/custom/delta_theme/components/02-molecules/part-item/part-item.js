(function () {
  jQuery("document").ready(function() {
    jQuery(".part-item__img").each(function() {
      let src = jQuery(this).attr('src');
      let hover_src = jQuery(this).attr('hover-img');
      jQuery(this).hover(function() {
        if(hover_src != "") {
          jQuery(this).attr('src', hover_src);
        }
      }, function() {
        jQuery(this).attr('src', src);
      });
    });
  });
})();

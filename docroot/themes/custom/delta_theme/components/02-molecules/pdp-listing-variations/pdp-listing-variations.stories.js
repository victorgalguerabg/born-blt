// PDP listing Variations Stories
import pdpBasePart from './pdp-base-parts/pdp-base-parts.twig';
import pdpBaseConfigurablePart from './pdp-configurable-parts/pdp-configurable-parts.twig';
import pdpHandleAccent from './pdp-handle-or-accent/pdp-handle-or-accent.twig';
import pdpRecommendReplacement from './pdp-recommended-replacement/pdp-recommended-replacement.twig';
import pdpRoughValue from './pdp-rough-valve/pdp-rough-valve.twig';

import pdpBasePartsData from './pdp-base-parts/pdp-base-parts.yml';
import pdpBaseConfigurablePartData from './pdp-configurable-parts/pdp-configurable-parts.yml';
import pdpHandleAccentData from './pdp-handle-or-accent/pdp-handle-or-accent.yml';
import pdpRecommendReplacementData from './pdp-recommended-replacement/pdp-recommended-replacement.yml';
import pdpRoughValueData from './pdp-rough-valve/pdp-rough-valve.yml';

//import './pdp-handle-or-accent/pdp-handle-or-accent';
//import './pdp-rough-valve/pdp-rough-valve';

/**
 * Storybook Definition.
 */
export default { title: 'Molecules/PDP listing Variations' };

export const pdpBaseParts = () => pdpBasePart(pdpBasePartsData);
export const pdpBaseConfigurableParts = () => pdpBaseConfigurablePart(pdpBaseConfigurablePartData);
export const pdpHandleAccents = () => pdpHandleAccent(pdpHandleAccentData);
export const pdpRecommendReplacements = () => pdpRecommendReplacement(pdpRecommendReplacementData);
export const pdpRoughValues = () => pdpRoughValue(pdpRoughValueData);

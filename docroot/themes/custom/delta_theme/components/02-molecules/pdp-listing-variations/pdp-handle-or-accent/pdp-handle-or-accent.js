(function () {

  'use strict';

  if((jQuery(".pdp-handle-accent-big-image").length || jQuery(".thumb-img-wrapper-list").length ) && (jQuery(".pdp-handle-accent").length)) {

    var base_model_trim_no = jQuery(".desktop-pdp-prod-info-hdr .pdp-prod-info-hdr .product-model").text().replace("model#: ", "").trim();
    var two_underscore = base_model_trim_no.indexOf("--");

    if(two_underscore > 0) {
      var base_model_no = base_model_trim_no.substring(0, two_underscore);
    } else {
      var base_model_no = base_model_trim_no;
    }

    var ha_model_no = jQuery(".pdp-handle-accent .pdp-listing-variation-desc:first .model-name").text().replace("model#: ", "").trim();
    //var ha_img_url = "https://media.deltafaucet.com/elvis/ConfiguredOnWhite/md/"+base_model_no+"_"+ha_model_no+"_CONFIG.png";

    //DDLT-643
    //jQuery(".pdp-handle-accent-big-image").find("img").attr('src', ha_img_url);
  }

  /* if(jQuery(".thumb-img-wrapper-list").length) {
    jQuery(".prod-big-img").find("img").attr('src', ha_img_url);
    jQuery(".thumb-img-wrapper .thumb-img-block.slick-slide.slick-current.slick-active").addClass("pdp-first-img-thumbnail");
    jQuery(".pdp-first-img-thumbnail").find("img").attr('srcset', ha_img_url);
    jQuery(".pdp-first-img-thumbnail").find("img").attr('src', ha_img_url);
  } */

  jQuery('.pdp-handle-accent .pdp-listing-variation-img img').on('click', function(){

    var ha_model_no = jQuery(this).parents(".pdp-listing-variation-img").next(".pdp-listing-variation-block").find(".model-name").text().replace("model#: ", "").trim();

    var ha_img_url = "https://media.deltafaucet.com/elvis/ConfiguredOnWhite/md/"+base_model_no+"_"+ha_model_no+"_CONFIG.png";
    if(jQuery(".pdp-handle-accent-big-image").length) {
      jQuery(".pdp-handle-accent-big-image").find("img").attr('src', ha_img_url);
    }

    if(jQuery(".thumb-img-wrapper-list").length) {
      jQuery(".prod-big-img").find("img").attr('src', ha_img_url);
      jQuery(".pdp-first-img-thumbnail").find("img").attr('srcset', ha_img_url);
      jQuery(".pdp-first-img-thumbnail").find("img").attr('src', ha_img_url);
      jQuery(".thumb-img-wrapper1 .thumb-img-block.slick-current").find("img").attr("src",ha_img_url);
      jQuery(".thumb-img-wrapper1 .thumb-img-block.slick-current").find("img").attr("srcset",ha_img_url);
    } 

    jQuery("html, body").animate({scrollTop: jQuery(".prod-slider-wrapper").offset().top - 200}, 800);
    return false;
  });

  jQuery(".ha_show_opn a").click(function(){
    jQuery(".pdp-handle-accent").addClass("more-handle-accent");
  });

  jQuery(".ha_hide_opn a").click(function(){
    jQuery(".pdp-handle-accent").removeClass("more-handle-accent");
  });

})();

(function () {

  'use strict';

  jQuery(".rv_show_opn a").click(function(){
    jQuery(".pdp-rough-value").addClass("more-rough-value");
  });

  jQuery(".rv_hide_opn a").click(function(){
    jQuery(".pdp-rough-value").removeClass("more-rough-value");
  });

})();

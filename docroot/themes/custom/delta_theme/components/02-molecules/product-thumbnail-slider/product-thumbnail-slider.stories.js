// Product Thumbnail Slider Stories
import productThumbnailSlider from './product-thumbnail-slider.twig';

import productThumbnailSliderData from './product-thumbnail-slider.yml';

import './product-thumbnail-slider';

/**
 * Storybook Definition.
 */
export default { title: 'Molecules/Product Thumbnail Slider' };

export const ProductThumbnailSlider = () => productThumbnailSlider(productThumbnailSliderData);

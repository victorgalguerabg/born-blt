// Video Card Stories
import videoCard from './video-card.twig';

import videoCardData from './video-card.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Molecules/Video Card' };

export const VideoCard = () => videoCard(videoCardData)

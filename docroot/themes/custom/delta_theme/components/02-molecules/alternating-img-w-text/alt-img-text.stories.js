// Alt Image Text Stories
import altImageText from './alt-img-text.twig';

import altImageTextData from './alt-img-text.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Molecules/Alt Image Text' };

export const AltImageText = () => altImageText(altImageTextData);

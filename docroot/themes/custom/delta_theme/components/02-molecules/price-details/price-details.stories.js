// Price Details Stories
import priceDetail from './price-details.twig';

import priceDetailData from './price-details.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Molecules/Price Details' };

export const PriceDetail = () => priceDetail(priceDetailData);

// Order List Stories
import orderList from './orders-list.twig';

import orderListData from './orders-list.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Molecules/Order List' };

export const ordersList = () => orderList(orderListData);

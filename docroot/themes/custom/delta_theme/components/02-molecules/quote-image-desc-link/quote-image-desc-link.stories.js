// Quote Image Desc Link Stories
import quoteImageDescLink from './quote-image-desc-link.twig';

import quoteImageDescLinkData from './quote-image-desc-link.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Molecules/Quote Image Desc Link' };

export const QuoteImageDescLink = () => quoteImageDescLink(quoteImageDescLinkData);

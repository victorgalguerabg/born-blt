import breadcrumb from './breadcrumbs/breadcrumbs.twig';
import breadcrumbDark from './breadcrumbs/breadcrumbs-dark.twig';
import footerLinkList from './footer-link-list/footer-link-list.twig';
import footerSlim from './footer-slim/footer-slim.twig';
import imageTab from './image-tabs/image-tabs.twig';
import inlineMenu from './inline-menu/inline-menu.twig';
import mainMenu from './main-menu/main-menu.twig';

import breadcrumbsData from './breadcrumbs/breadcrumbs.yml';
import breadcrumbsDarkData from './breadcrumbs/breadcrumbs-dark.yml';
import footerLinkListData from './footer-link-list/footer-link-list.yml';
import footerSlimData from './footer-slim/footer-slim.yml';
import imageTabData from './image-tabs/image-tabs.yml';
import inlineMenuData from './inline-menu/inline-menu.yml';
import mainMenuData from './main-menu/main-menu.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Molecules/Menus' };

export const breadcrumbs = () => breadcrumb(breadcrumbsData);

export const breadcrumbsDark = () => breadcrumbDark(breadcrumbsDarkData);

export const FooterLinkList = () => footerLinkList(footerLinkListData);

export const FooterSlim = () => footerSlim(footerSlimData);

export const ImageTab = () => imageTab(imageTabData);

export const inline = () => inlineMenu(inlineMenuData);

export const main = () => mainMenu(mainMenuData);

/**
 * @file
 * A JavaScript file containing the main menu functionality (small/large screen)
 *
 */

// JavaScript should be made compatible with libraries other than jQuery by
// wrapping it with an "anonymous closure". See:
// - https://drupal.org/node/1446420
// - http://www.adequatelygood.com/2010/3/JavaScript-Module-Pattern-In-Depth


// (function (Drupal) { // UNCOMMENT IF DRUPAL.
//
//   Drupal.behaviors.mainMenu = {
//     attach: function (context) {

(function () { // REMOVE IF DRUPAL.

  'use strict';

  // Use context instead of document IF DRUPAL.
  var toggle_expand1 = document.getElementById('toggle-expand-mobile');
  var menu1 = document.getElementById('mega-nav-wrapper');
  if ((menu1 !== null) && (menu1 !== 0)) {
    var expand_menu1 = menu1.getElementsByClassName('expand-sub');
  }
  var header = document.getElementById("site-header");
  var mainheader = document.getElementById("fullwidth-header");

  // Mobile Menu Show/Hide.
  jQuery("#toggle-expand-mobile").click(function () {
    toggle_expand1.classList.toggle('toggle-expand--open');
    menu1.classList.toggle('main-nav--open');
    mainheader.classList.toggle('active-main-mobile-header');
    header.classList.toggle('active-mobile-header');
  });

  // Expose mobile sub menu on click.
  if (expand_menu1 != 0 && expand_menu1 != null && expand_menu1 != undefined) {
    for (var i = 0; i < expand_menu1.length; i++) {
      expand_menu1[i].addEventListener('click', function (e) {
        var menu_item1 = e.currentTarget;
        var sub_menu1 = menu_item1.nextElementSibling;
        var parent_menu1 = menu_item1.parentElement;

        menu_item1.classList.toggle('expand-sub--open');
        sub_menu1.classList.toggle('mega-menu--sub-open');
        parent_menu1.classList.toggle('mega-menu--sub-parent-open');
      });
    }
  }

  jQuery("#toggle-expand-mobile").click(function () {
    jQuery("main.main").toggleClass("no-scrolling");
  });

  jQuery(".mega-nav-wrapper > ul > li > .expand-sub").click(function () {
    jQuery(".menu-prod-title").next(".expand-sub").toggleClass("expand-sub--open");
    jQuery(".menu-prod-title").next(".expand-sub").next(".mega-menu-nav").toggleClass("mega-menu--sub-open");
  });

  jQuery(".mega-menu-nav a.disable-link").removeAttr("href");
})(); // REMOVE IF DRUPAL.

// })(Drupal); // UNCOMMENT IF DRUPAL.

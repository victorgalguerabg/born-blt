// cartridge-block Stories
import cartridgeBlock from './cartridge-block.twig';

import cartridgeBlockData from './cartridge-block.yml';
/**
 * Storybook Definition.
 */
export default { title: 'Molecules/Cartridge Block' };

export const CartridgeBlock = () => cartridgeBlock(cartridgeBlockData);

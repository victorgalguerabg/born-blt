// Animation Hover Text Stories
import animationHoverText from './animation-hover-text.twig';

import animationHoverTextData from './animation-hover-text.yml';

import './animation-hover-text';
import  './pl-popup'

/**
 * Storybook Definition.
 */
export default { title: 'Molecules/Animation Hover Text' };

export const AnimationHoverText = () => animationHoverText(animationHoverTextData);

(function () {
  jQuery(".call-popup").click(function() {
    jQuery(".pl-popup-wrapper").addClass("active-pl-popup-wrapper");
  });

  jQuery(document).mouseup(function(e)
  {
    if(jQuery(".pl-popup-data").length) {
      var container = jQuery("pl-popup-data");
      if (!container.is(e.target) && container.has(e.target).length === 0)
      {
        jQuery(".pl-popup-wrapper").removeClass("active-pl-popup-wrapper");

        if(jQuery(".aht-popup-video").length) {
          jQuery(".aht-popup-video").find("iframe").attr('src', "");
        }
      }
    }
  });

  jQuery(document).keydown(function(e) {
    if(jQuery(".pl-popup-data").length) {
      if (e.keyCode == 27) {
        jQuery(".pl-popup-wrapper").removeClass("active-pl-popup-wrapper");
        if(jQuery(".aht-popup-video").length) {
          jQuery(".aht-popup-video").find("iframe").attr('src', "");
        }
      }
    }
  });

})();

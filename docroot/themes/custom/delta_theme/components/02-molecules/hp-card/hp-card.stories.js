// Hp Card Stories
import hpCard from './hp-card.twig';

import hpCardData from './hp-card.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Molecules/Hp Card' };

export const HpCard = () => hpCard(hpCardData);

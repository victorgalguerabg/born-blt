// Result Totals Stories
import resultTotal from './result-totals.twig';

import resultTotalData from './result-totals.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Molecules/Result Totals' };

export const ResultTotal = () => resultTotal(resultTotalData);

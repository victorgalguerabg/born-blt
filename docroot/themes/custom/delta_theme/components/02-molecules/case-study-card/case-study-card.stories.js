// Case Study Card Stories
import caseStudyCard from './case-study-card.twig';
import caseStudyCardGrid from './case-study-card-grid.twig';

import caseStudyCardData from './case-study-card.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Molecules/Case Study Card' };

export const CaseStudyCard = () => caseStudyCard(caseStudyCardData);
export const CaseStudyCardGrid = () => caseStudyCardGrid();

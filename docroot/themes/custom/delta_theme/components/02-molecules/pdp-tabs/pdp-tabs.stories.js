// PDP Tabs Stories
import documentsSpecs from './documents-specs/documents-specs.twig';
import productFeature from './product-features/product-features.twig';

import documentsSpecsData from './documents-specs/documents-specs.yml';
import productFeatureData from './product-features/product-features.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Molecules/PDP Tabs' };

export const DocumentsSpecs = () => documentsSpecs(documentsSpecsData);
export const ProductFeature = () => productFeature(productFeatureData);

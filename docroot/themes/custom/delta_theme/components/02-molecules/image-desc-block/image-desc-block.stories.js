// Image Desc Block Stories
import imageDescBlock from './image-desc-block.twig';

import imageDescBlockData from './image-desc-block.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Molecules/Image Desc Block' };

export const ImageDescBlock = () => imageDescBlock(imageDescBlockData);

// PDP Product Detail Stories
import pdpProductDetail from './pdp-product-detail.twig';

import pdpProductDetailData from './pdp-product-detail.yml';

import './pdp-product-detail';

/**
 * Storybook Definition.
 */
export default { title: 'Molecules/PDP Product Detail' };

export const PdpProductDetail = () => pdpProductDetail(pdpProductDetailData);

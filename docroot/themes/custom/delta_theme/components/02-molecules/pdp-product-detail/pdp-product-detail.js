/**
 * @file
 * A JavaScript file containing the main menu functionality (small/large screen)
 *
 */

// JavaScript should be made compatible with libraries other than jQuery by
// wrapping it with an "anonymous closure". See:
// - https://drupal.org/node/1446420
// - http://www.adequatelygood.com/2010/3/JavaScript-Module-Pattern-In-Depth


// (function (Drupal) { // UNCOMMENT IF DRUPAL.
//
//   Drupal.behaviors.mainMenu = {
//     attach: function (context) {

  (function () { // REMOVE IF DRUPAL.

    'use strict';

    jQuery(".pdp-btn-buyonline a").click(function(){
      jQuery(".pdp-btn-find").removeClass("btn-hide");
      jQuery(".pdp-btn-shop").removeClass("btn-hide");
      jQuery(this).parent().hide();
    });

    var tabTrigger = function() {
			if (window.location.href.indexOf('#documents-and-specs') !== -1) {
				var offset = jQuery('.pdp-tabs').offset().top - 100;
				jQuery('html, body').animate({ scrollTop: offset });
				jQuery("a[name='documents-and-specs']")[0].click();
			}

      if (window.location.href.indexOf('#product-features') !== -1) {
				var offset = jQuery('.pdp-tabs').offset().top - 100;
				jQuery('html, body').animate({ scrollTop: offset });
				jQuery("a[name='product-features']")[0].click();
			}

      if (window.location.href.indexOf('#reviews') !== -1) {
				var offset = jQuery('.pdp-tabs').offset().top - 100;
				jQuery('html, body').animate({ scrollTop: offset });
				jQuery("a[name='reviews']")[0].click();
			}

      if (window.location.href.indexOf('#questions-and-answers') !== -1) {
				var offset = jQuery('.pdp-tabs').offset().top - 100;
				jQuery('html, body').animate({ scrollTop: offset });
				jQuery("a[name='questions-and-answers']")[0].click();
			}

      if (window.location.href.indexOf('#parts-list') !== -1) {
				var offset = jQuery('.pdp-tabs').offset().top - 100;
				jQuery('html, body').animate({ scrollTop: offset });
				jQuery("a[name='parts-list']")[0].click();
			}
		}

    var tabTriggerReview = function() {
			if (window.location.href.indexOf('#reviewTab') !== -1) {
				var offset = jQuery('.pdp-tabs').offset().top - 100;
				jQuery('html, body').animate({ scrollTop: offset });
				jQuery("a[name='reviewsTab']")[0].click();
			}
		}

    jQuery(document).ready(function() {
      tabTrigger();
      tabTriggerReview();
    });

  })(); // REMOVE IF DRUPAL.

  // })(Drupal); // UNCOMMENT IF DRUPAL.

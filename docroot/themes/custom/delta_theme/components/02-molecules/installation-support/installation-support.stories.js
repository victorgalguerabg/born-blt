// Installation Support Stories
import installationSupport from './installation-support.twig';

import installationSupportData from './installation-support.yml';
import installationSupportLeftData from './installation-support~left.yml';
import installationSupportRightData from './installation-support~right.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Molecules/Installation Support' };

export const InstallationSupport = () => installationSupport(installationSupportData);
export const InstallationSupportLeft = () => installationSupport(installationSupportLeftData);
export const InstallationSupportRight = () => installationSupport(installationSupportRightData);

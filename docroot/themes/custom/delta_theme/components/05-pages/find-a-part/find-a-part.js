(function() {

  jQuery(".sp-third-tab-img .image-with-link-block").click(function(e) {
    e.preventDefault();
    jQuery(this).parent(".sp-third-tab-img").parent(".accordion-def").prev(".accordion-term").find(".acc-ans-link").remove();
    var acc_ans_link = document.createElement('a');
    jQuery(acc_ans_link).appendTo(jQuery(this).parent(".sp-third-tab-img").parent(".accordion-def").prev(".accordion-term").find(".acc-ans"));
    jQuery(".acc-ans").find("a").addClass("acc-ans-link");
    var img_title = jQuery(this).find(".img-link-title").text();
    jQuery(this).parent(".sp-third-tab-img").parent(".accordion-def").prev(".accordion-term").find("label").addClass("sp-label-msg");
    jQuery(this).parent(".sp-third-tab-img").parent(".accordion-def").prev(".accordion-term").find(".acc-ans").find("a").text(img_title);

    jQuery(this).parent(".sp-third-tab-img").parent(".accordion-def").prev(".accordion-term").removeClass("is-active");
    jQuery(this).parent(".sp-third-tab-img").parent(".accordion-def").next(".accordion-term").addClass("is-active");
  });

  jQuery(".acc-ans").click(function() {
    jQuery(this).find(".acc-ans-link").remove();
    jQuery(".accordion-term").removeClass("is-active");
    jQuery(this).parent(".accordion-term").addClass("is-active");
  });

})();

// Find A Part Stories
import findAPart from './find-a-part.twig';

import findAPartData from './find-a-part.yml';

//import './find-a-part';

/**
 * Storybook Definition.
 */
export default { title: 'Pages/Find A Part' };

export const findAParts = () => findAPart(findAPartData);
// Two Column Template Stories
import twoColumnTemplate from './two-column-template.twig';

import twoColumnTemplateData from './two-column-template.yml'

/**
 * Storybook Definition.
 */
export default { title: 'Pages/Two Column Template' };

export const twoColumnTemplates = () => twoColumnTemplate(twoColumnTemplateData);

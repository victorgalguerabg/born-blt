// Service Parts Support Stories
import servicePartsSupport from './service-parts-support.twig';

import servicePartsSupportData from './service-parts-support.yml';

//import './service-parts-support';

/**
 * Storybook Definition.
 */
export default { title: 'Pages/Service Parts Support' };

export const servicePartsSupports = () => servicePartsSupport(servicePartsSupportData);
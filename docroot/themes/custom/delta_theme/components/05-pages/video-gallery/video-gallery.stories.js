// Video Gallery Stories
import videoGallery from './video-gallery.twig';

import videoGalleryData from './video-gallery.yml'

/**
 * Storybook Definition.
 */
export default { title: 'Pages/Video Gallery' };

export const videoGallerys = () => videoGallery(videoGalleryData);

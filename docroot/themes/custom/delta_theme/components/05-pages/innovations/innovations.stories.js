// Innovation Stories
import innovation from './innovations.twig';

import innovationsData from './innovations.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Pages/Innovation' };

export const innovations = () => innovation(innovationsData);
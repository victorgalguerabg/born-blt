// Press Release Stories
import foundationsKitchenCollection from './foundations-kitchen-collection/foundations-kitchen-collection.twig';
import kitchen from './kitchen/kitchen.twig';
import pressRelease from './press-releases/press-releases.twig';

import foundationsKitchenCollectionData from './foundations-kitchen-collection/foundations-kitchen-collection.yml';
import kitchenData from './kitchen/kitchen.yml';
import pressReleaseData from './press-releases/press-releases.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Pages/Press Release' };

export const FoundationsKitchenCollection = () => foundationsKitchenCollection(foundationsKitchenCollectionData);
export const Kitchen = () => kitchen(kitchenData);
export const PressRelease = () => pressRelease(pressReleaseData);
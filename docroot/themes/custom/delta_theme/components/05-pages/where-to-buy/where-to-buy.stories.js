// Where To Buy Stories
import whereToBuy from './where-to-buy.twig';

import whereToBuyData from './where-to-buy.yml'

/**
 * Storybook Definition.
 */
export default { title: 'Pages/Where To Buy' };

export const whereToBuys = () => whereToBuy(whereToBuyData);

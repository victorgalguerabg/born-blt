// SRP Stories
import srp from './srp.twig';
import srpContent from './srp-content.twig';
import srpDocument from './srp-documents.twig';
import srpNoResult from './srp-no-results.twig';

/**
 * Storybook Definition.
 */
export default { title: 'Pages/SRP' };

export const srps = () => srp();
export const srpContents = () => srpContent();
export const srpDocuments = () => srpDocument();
export const srpNoResults = () => srpNoResult();

// Design Beyond Stories
import designBeyond from './design-beyond.twig';

import designBeyondData from './design-beyond.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Pages/Design Beyond' };

export const DesignBeyond = () => designBeyond(designBeyondData);

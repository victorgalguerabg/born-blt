// Order Status Stories
import orderStatus from './order-status.twig';
import orderStatusDetail from './order-status-details/order-status-details.twig';
import orderStatusListing from './order-status-listing/order-status-listing.twig';

import orderStatusData from './order-status.yml';
import orderStatusDetailData from './order-status-details/order-status-details.yml';
import orderStatusListingData from './order-status-listing/order-status-listing.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Pages/Order Status' };

export const OrderStatus = () => orderStatus(orderStatusData);
export const OrderStatusDetail = () => orderStatusDetail(orderStatusDetailData);
export const OrderStatusListing = () => orderStatusListing(orderStatusListingData);
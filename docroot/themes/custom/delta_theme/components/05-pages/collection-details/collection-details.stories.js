// Collection Detail Stories
import collectionDetail from './collection-details.twig';

import collectionDetailsData from './collection-details.yml'

/**
 * Storybook Definition.
 */
export default { title: 'Pages/Collection Detail' };

export const collectionDetails = () => collectionDetail(collectionDetailsData);

// Product Design File Stories
import productDesignFile from './product-design-files.twig';
import productDesignFilesDownload from './product-design-files-download-page.twig';

import productDesignFileData from './product-design-files.yml';
import productDesignFilesDownloadData from './product-design-files-download-page.yml';

import './list-grid-toggle';

/**
 * Storybook Definition.
 */
export default { title: 'Pages/Product Design File' };

export const ProductDesignFiles = () => productDesignFile(productDesignFileData);
export const ProductDesignFilesDownload = () => productDesignFilesDownload(productDesignFilesDownloadData);

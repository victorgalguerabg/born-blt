(function() {
  const LIST_VIEW = 'list-view';
  const GRID_VIEW = 'grid-view';

  function getState(toggle) {
    if (toggle.classList.contains(LIST_VIEW)) {
      return LIST_VIEW
    } else if (toggle.classList.contains(GRID_VIEW)) {
      return GRID_VIEW;
    }

    return null;
  }

  const toggle = document.querySelector('.list-grid-toggle');

    document.addEventListener('click', function(e) {
        if (!e.target.classList.contains('list-grid-toggle')) {
            return;
        }

        const currentState = getState(toggle);
        const grids = [].slice.call(document.querySelectorAll('.download-card-grid'));

        if (currentState === LIST_VIEW) {
            toggle.classList.remove(LIST_VIEW);
            toggle.classList.add(GRID_VIEW);
            toggle.innerText = 'Grid View';
            grids.forEach(grid => grid.classList.add(LIST_VIEW));
        } else {
            toggle.classList.add(LIST_VIEW);
            toggle.classList.remove(GRID_VIEW);
            toggle.innerText = 'List View';
            grids.forEach(grid => grid.classList.remove(LIST_VIEW));
        }
    });
})();

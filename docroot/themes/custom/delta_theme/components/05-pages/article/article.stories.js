// Article Stories
import article from './article.twig';

/**
 * Storybook Definition.
 */
export default { title: 'Pages/Article' };

export const Article = () => article();

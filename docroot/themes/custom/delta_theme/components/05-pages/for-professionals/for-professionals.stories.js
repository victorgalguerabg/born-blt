// For Professional Stories
import forProfessionals from './for-professionals.twig';

import forProfessionalsData from './for-professionals.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Pages/For Professional' };

export const forProfessionalss = () => forProfessionals(forProfessionalsData);
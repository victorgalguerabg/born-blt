// Innovation Stories
import innovationDetail from './innovation-details.twig';
import innovationSecondaryBanner from './innovation-secondary-banner/innovation-secondary-banner.twig';
import innovationVideoSecondaryBanner from './innovation-video-secondary-banner/innovation-video-secondary-banner.twig';

import innovationDetailData from './innovation-details.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Pages/Innovation' };

export const innovationDetails = () => innovationDetail(innovationDetailData);
export const InnovationSecondaryBanner = () => innovationSecondaryBanner();
export const InnovationVideoSecondaryBanner = () => innovationVideoSecondaryBanner();
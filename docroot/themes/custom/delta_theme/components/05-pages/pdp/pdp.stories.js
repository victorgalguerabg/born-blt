// PDP Stories
import pdp from './pdp.twig';
import pdpOwnershipToolkit from './pdp-ownership-toolkit/pdp-ownership-toolkit.twig';
import pdpWhereToBuyOnline from './pdp-where-to-buy-online/pdp-where-to-buy-online.twig';

import pdpData from './pdp.yml';
import pdpOwnershipToolkitData from './pdp-ownership-toolkit/pdp-ownership-toolkit.yml';
import pdpWhereToBuyOnlineData from './pdp-where-to-buy-online/pdp-where-to-buy-online.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Pages/PDP' };

export const PDP = () => pdp(pdpData);
export const PDPOwnershipToolkit = () => pdpOwnershipToolkit(pdpOwnershipToolkitData);
export const PDPWhereToBuyOnline = () => pdpWhereToBuyOnline(pdpWhereToBuyOnlineData);
// Faq Stories
import faq from './faq-detail.twig';
import listingCategoryFaq from './listing-category-faq.twig';

import faqData from './faq-detail.yml';
import listingCategoryFaqData from './listing-category-faq.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Pages/Faq' };

export const Faq = () => faq(faqData);
export const ListingCategoryFaq = () => listingCategoryFaq(listingCategoryFaqData);
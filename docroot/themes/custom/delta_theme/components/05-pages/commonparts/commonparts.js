(function() {
  const links = [].slice.call(document.querySelectorAll('[data-repair-link]'));
  const preview = document.querySelector('.commonparts-preview');
  const items = [].slice.call(document.querySelectorAll('.commonparts-preview__item'));

  links.forEach((link, i) => {
    link.addEventListener('click', function(e) {
      e.preventDefault();

      const currentActiveItem = preview.querySelector('.is-active');

      if (currentActiveItem) {
        currentActiveItem.classList.remove('is-active');
      }

      items[i].classList.add('is-active');
    });
  });
})();

// Common Parts Stories
import commonPart from './commonparts.twig';

import commonPartsData from './commonparts.yml';

//import './commonparts'

/**
 * Storybook Definition.
 */
export default { title: 'Pages/Common Parts' };

export const commonParts = () => commonPart(commonPartsData);

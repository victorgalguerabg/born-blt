// Video Thumbnails Example Stories
import videoThumbnailsExample from './video-thumbnails-example.twig';

/**
 * Storybook Definition.
 */
export default { title: 'Pages/Video Thumbnails Example' };

export const videoThumbnailsExamples = () => videoThumbnailsExample();

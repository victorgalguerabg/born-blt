// Compare Block Popup Stories
import compareBlockPopup from './compare-block-popup.twig';

import compareBlockPopupsData from './compare-block-popup.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Pages/Compare Block Popup' };

export const CompareBlockPopup = () => compareBlockPopup(compareBlockPopupsData);

// Home Stories
import home from './home.twig';

/**
 * Storybook Definition.
 */
export default { title: 'Pages/Home' };

export const Home = () => home();

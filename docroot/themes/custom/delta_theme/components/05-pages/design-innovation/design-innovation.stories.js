// Design Innovation Stories
import designInnovation from './design-innovation.twig';

import designInnovationData from './design-innovation.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Pages/Design Innovation' };

export const DesignInnovation = () => designInnovation(designInnovationData);

import customGrid from './custom-grid/01-custom-grid.twig';
import grid from './grid/00-grid.twig';
import mediumGridStruture from './medium-grid-struture/medium-grid-struture.twig';

import gridData from './grid/00-grid.yml';

/**
 * Storybook Definition.
 */
export default {
  title: 'Base/Layouts',
};

export const CustomGrid = () => customGrid();
export const Grid = () => grid(gridData);
export const MediumGridStruture = () => mediumGridStruture();

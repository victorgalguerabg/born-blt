import colors from './01-colors/colors.twig';
import animation from './animations/_animations.twig';

import colorsData from './01-colors/colors.yml';

/**
 * Storybook Definition.
 */
export default {
  title: 'Base/Global',
};

export const ColorPalettes = () => colors(colorsData);
export const Animation = () => animation();

<?php

use Drupal\media\Entity\Media;
use Drupal\file\Entity\File;

/**
 * Implements hook_theme_suggestions_HOOK_alter() for form templates.
 *
 * @param array $suggestions
 * @param array $variables
 *   Adds page--<content_type_name>.html.twig to theme suggestions.
 */
function brizo_theme_theme_suggestions_page_alter(array &$suggestions, array $variables) {
  if ($node = \Drupal::routeMatch()->getParameter('node')) {
    if(is_object($node)){
      $suggestions[] = 'page__' . $node->bundle();
    }
  }
}

/**
 * Implements hook_preprocess_node_content-type().
 */
function brizo_theme_preprocess_node__webform(&$variables) {
  $variables['#attached']['library'][] = 'brizo_theme/global';
}

/**
 * Implements hook_preprocess_menu().
 */
function brizo_theme_preprocess_menu(&$variables) {
  if ($variables['menu_name'] === 'main') {
    $variables['items'] = _brizo_theme_preprocess_menu_item(
      $variables['items']
    );
    $variables['items']['countryselector'] = $variables['countrymenu'];
  }
  if ($variables['menu_name'] === 'countries') {
    $variables['items'] = $variables['countrymenu'];
  }
}

/**
 * Helper function to process menu tree items.
 */
function _brizo_theme_preprocess_menu_item(&$items) {
  foreach ($items as &$item) {
    $attributes = $item['url']->getOptions('attributes');
    if(array_key_exists('attributes', $attributes)){
      $class = '';
      if(is_array($attributes['attributes']['class'])){
        $class_in_arr = $attributes['attributes']['class'];
        if(is_array($class_in_arr) && count($class_in_arr) > 0){
          $class = $class_in_arr[0];
        }
      }
      $item['item_modifiers'] = [$class];
    }
    // Check if item has Menu Link Content's fields.
    if (!empty($item['content']['#menu_link_content'])) {
      $menu_link_content = $item['content']['#menu_link_content'];
      $view_mode = $menu_link_content->get('view_mode')->value;
      if ($menu_link_content->hasField('field_menu_summary')) {

        if (!$menu_link_content->get('field_menu_summary')->isEmpty()) {
          $item['menu_desc'] = $menu_link_content->get('field_menu_summary')
            ->value;
          $tag = 'p';
          $item['menu_desc'] = preg_replace(
            '/<\/?' . $tag . '(.|\s)*?>/',
            '',
            $item['menu_desc']
          );
        }
      }

      $allowed = ['thin_card', 'menu_with_image', 'wide_card'];

      if (in_array($view_mode, $allowed) &&
        $menu_link_content->hasField('field_menu_image')) {

        if (!$menu_link_content->get('field_menu_image')->isEmpty()) {
          $media = $menu_link_content->get('field_menu_image')
            ->referencedEntities()[0];
          $fid = $media->field_media_image->target_id;
          $file = File::load($fid);
          $image_file = $file->getFileUri();
          $item['menu_img_src'] = file_create_url($image_file);
          $item['menu_img_alt'] = $media->field_media_image->alt;
        }
      }

      $allowed_menu_img = ['default'];
      $item['col_menu_img_src'] = $item['col_menu_img_alt'] = '';
      if (in_array($view_mode, $allowed_menu_img) &&
        $menu_link_content->hasField('field_menu_image')) {
        if (!$menu_link_content->get('field_menu_image')->isEmpty()) {
          $media = $menu_link_content->get('field_menu_image')
            ->referencedEntities()[0];
          $fid = $media->field_media_image->target_id;
          $file = File::load($fid);
          $image_file = $file->getFileUri();
          $item['col_menu_img_src'] = file_create_url($image_file);
          $item['col_menu_img_alt'] = $media->field_media_image->alt;
        }
      }

    }
    // Process subitems.
    if ($item['below']) {
      _brizo_theme_preprocess_menu_item($item['below']);
    }
  }

  return $items;
}

/**
 * Implements hook_theme_suggestions_alter().
 *
 * @param array $suggestions
 * @param array $variables
 * @param string $hook
 *   Adds views-view--<view_name>.html.twig to theme suggestions.
 */
function brizo_theme_theme_suggestions_alter(array &$suggestions, array $variables, $hook) {
  $viewCurrentDisplay = '';
    switch ($hook) {
      case 'views_view_list' :
        $viewCurrentDisplay = $variables['view']->current_display;
        $suggestions[] = 'views_view_list__' . $viewCurrentDisplay;
        break;
      case 'views_view_unformatted' :
        $viewCurrentDisplay = $variables['view']->current_display;
        $suggestions[] = 'views_view_unformatted__' . $viewCurrentDisplay;
        break;
    }
}

function brizo_theme_preprocess_html(&$variables) {
  $path = \Drupal::service('path.current')->getPath();
  $current_path = \Drupal::service('path_alias.manager')->getAliasByPath($path);
  $current_user_status = \Drupal::currentUser()->isAuthenticated();
  if ($current_user_status != 1 && $current_path == "/user/login"){
    $variables['attributes']['class'][] = 'user-login-page';
  }
  if(strpos($current_path,'/shower/collection/') > -1) {
    $variables['attributes']['class'][] = 'shower-collection';
    $variables['attributes']['class'][] = 'full-width-content';
  }
  if (\Drupal::service('module_handler')->moduleExists('domain')) {
    $loader = \Drupal::service('domain.negotiator');
    $current_domain = $loader->getActiveDomain();
    if ($current_domain) {
      $variables['attributes']['class'][] = $current_domain->get('name');
    }
  }
}

function brizo_theme_preprocess_commerce_product(&$variables) {
  if (\Drupal::service('module_handler')->moduleExists('domain')) {
    try {
      $storage = \Drupal::entityTypeManager()->getStorage('domain');
    } catch (\Exception $error) {
      $logger = \Drupal::logger('checkout_flow_checkout_form');
      $messenger = \Drupal::messenger();
      $logger->alert(t('@err', ['@err' => $error->__toString()]));
      $messenger->addWarning(t('Unable to proceed, please try again. If the problem persists, please contact an administrator. Error code: 8FAAE95D-@line-@time', ['@line' => $error->getLine(), '@time' => time()]));
    }

    $domains = $storage->loadMultiple();
    $loader = \Drupal::service('domain.negotiator');
    $current_domain = $loader->getActiveDomain();
    if ($current_domain) {
      foreach ($domains as $domain) {
        if(( $domain->get('hostname') == $current_domain->get('hostname') ) &&  $domain->get('is_default') == TRUE ) {
          $variables['domain_show_price'] = TRUE;
          break;
        }
        else {
          $variables['domain_show_price'] = FALSE;
        }
      }
    }
    else {
      $variables['domain_show_price'] = TRUE;
    }
  }
  else {
    $variables['domain_show_price'] = TRUE;
  }

}

function brizo_theme_preprocess_maintenance_page(&$variables) {
  if (in_array('system/maintenance', $variables['#attached']['library'])) {
    $index = array_search('system/maintenance', $variables['#attached']['library']);
    unset($variables['#attached']['library'][$index]);
  }
  $variables['#attached']['library'] = [];
  $variables['#attached']['library'][] = 'brizo_theme/maintenance';
}

function brizo_theme_preprocess_views_view_list(&$variables) {

  $view = $variables['view'];
  // For /bath/collection , /kitchen/collection
  if($view->id() == 'collection_landing_kitchen' || $view->id() == 'collection_landing_bathroom'  || $view->id() == 'collection_landing_shower' || $view->id() == 'collection_landing_kitchen_sinks'|| $view->id() == 'collection_landing_bathroom_sinks') {
    if($view->current_display == 'kitchen_collections' || $view->current_display == 'bath_collections' || $view->current_display == 'shower_collections' || $view->current_display == 'essential_shower_block' || $view->current_display == 'kitchen_sinks_collections' || $view->current_display == 'bathroom_sinks_collections' ) {
      foreach ($view->result as $key => $value) {
        if(is_object($value) && property_exists($value, '_entity')){
          $viewGetInArr = $value->_entity->toArray();
          if(is_array($viewGetInArr) && array_key_exists('field_collection_image', $viewGetInArr) && count($viewGetInArr['field_collection_image']) > 0){
            $imageSrc = renderImgWitMid($viewGetInArr['field_collection_image'][0]['target_id']);
            $variables['kitch_col_img_src'][$key] = $imageSrc;
          }
          if(is_array($viewGetInArr) && array_key_exists('field_kitchen_collection_landing', $viewGetInArr) && count($viewGetInArr['field_kitchen_collection_landing']) > 0){
            $imageSrc = renderImgWitMid($viewGetInArr['field_kitchen_collection_landing'][0]['target_id']);
            $variables['kitch_col_land_img_src'][$key] = $imageSrc;
          }
          if(is_array($viewGetInArr) && array_key_exists('field_bath_collection_image', $viewGetInArr) && count($viewGetInArr['field_bath_collection_image']) > 0){
            $imageSrc = renderImgWitMid($viewGetInArr['field_bath_collection_image'][0]['target_id']);
            $variables['bath_col_img_src'][$key] = $imageSrc;
          }
          if(is_array($viewGetInArr) && array_key_exists('field_bath_collection_landing_im', $viewGetInArr) && count($viewGetInArr['field_bath_collection_landing_im']) > 0){
            $imageSrc = renderImgWitMid($viewGetInArr['field_bath_collection_landing_im'][0]['target_id']);
            $variables['bath_col_land_img_src'][$key] = $imageSrc;
          }
          if(is_array($viewGetInArr) && array_key_exists('field_shower_collection_image', $viewGetInArr) && count($viewGetInArr['field_shower_collection_image']) > 0){
            $imageSrc = renderImgWitMid($viewGetInArr['field_shower_collection_image'][0]['target_id']);
            $variables['shower_col_img_src'][$key] = $imageSrc;
          }
          if(is_array($viewGetInArr) && array_key_exists('field_shower_collection_landing', $viewGetInArr) && count($viewGetInArr['field_shower_collection_landing']) > 0){
            $imageSrc = renderImgWitMid($viewGetInArr['field_shower_collection_landing'][0]['target_id']);
            $variables['shower_col_land_img_src'][$key] = $imageSrc;
          }
          if(is_array($viewGetInArr) && array_key_exists('field_kitchen_sinks_col_image', $viewGetInArr) && count($viewGetInArr['field_kitchen_sinks_col_image']) > 0){
            $imageSrc = renderImgWitMid($viewGetInArr['field_kitchen_sinks_col_image'][0]['target_id']);
            $variables['kitch_col_img_src'][$key] = $imageSrc;
          }
          if(is_array($viewGetInArr) && array_key_exists('field_kitchen_sinks_col_land_img', $viewGetInArr) && count($viewGetInArr['field_kitchen_sinks_col_land_img']) > 0){
            $imageSrc = renderImgWitMid($viewGetInArr['field_kitchen_sinks_col_land_img'][0]['target_id']);
            $variables['kitch_col_land_img_src'][$key] = $imageSrc;
          }
          if(is_array($viewGetInArr) && array_key_exists('field_bathroom_sinks_col_image', $viewGetInArr) && count($viewGetInArr['field_bathroom_sinks_col_image']) > 0){
            $imageSrc = renderImgWitMid($viewGetInArr['field_bathroom_sinks_col_image'][0]['target_id']);
            $variables['kitch_col_img_src'][$key] = $imageSrc;
          }
          if(is_array($viewGetInArr) && array_key_exists('field_bath_sinks_col_land_img', $viewGetInArr) && count($viewGetInArr['field_bath_sinks_col_land_img']) > 0){
            $imageSrc = renderImgWitMid($viewGetInArr['field_bath_sinks_col_land_img'][0]['target_id']);
            $variables['kitch_col_land_img_src'][$key] = $imageSrc;
          }
        }
      }
    }
  }
}

function renderImgWitMid($mid){
  $imageSrc = '';
  if(isset(Media::load($mid)->field_media_image[0])) {
    $fid = Media::load($mid)->field_media_image[0]->getValue()['target_id'];
    $file = File::load($fid);
    $imageSrc = $file->createFileUrl();
  }
  return $imageSrc;
}
function brizo_theme_preprocess_views_view_unformatted(&$variables) {
  $storage = \Drupal::entityTypeManager()->getStorage('domain');
  $domains = $storage->loadMultiple();
  $loader = \Drupal::service('domain.negotiator');
  $current_domain = $loader->getActiveDomain();
  $variables['domain'] = $current_domain->get('hostname');

  $view = $variables['view'];

   if($view->id() == 'collection_detail_page') {
    if($view->current_display == 'kitchen_collections' or $view->current_display == 'bath_collections' or $view->current_display == 'shower_collections' or $view->current_display == 'kitchen_sinks_collections' or $view->current_display == 'bathroom_sinks_collections' ) {
      $config = \Drupal::config('delta_services.plpconfig');
      $itemsPerPage = $config->get('product_listing_items_per_page');
      $itemsPerPageOptions = $config->get('product_listing_items_per_page_options');
      $variables['#attached']['drupalSettings']['defaultPaginationOption'] = ($itemsPerPage)? $itemsPerPage : 24;
      if ($itemsPerPageOptions) {
        $optionsArray = explode(',', $itemsPerPageOptions);
        $labelArray[0]['type'] = "option";
        $labelArray[0]['label'] = "Items Per Page";
        $labelArray[0]['value'] = 'items-per-page';
        $optionConfigList = [];
        foreach($optionsArray as $key => $options) {
          $optionConfigList[$key]['type'] = "option";
          $optionConfigList[$key]['label'] = $options;
          $optionConfigList[$key]['value'] = $options;
        }
        $optionList = array_merge($labelArray, $optionConfigList);
        $variables['paginationOptions'] = $optionList;
      }
    }
  }
}

/**
 * Implements hook_preprocess_block().
 *
 * @param array $variables
 *  Block preprocessor to add logo image alt.
 *
 */
function brizo_theme_preprocess_block(&$variables) {
  if ($variables['plugin_id'] == 'system_branding_block') {
    $site_logo_alt = \Drupal::config('system.site')->get('site_logo_alt');
    $variables['site_logo_alt'] = $site_logo_alt;
  }
}

/**
 * Implements hook__preprocess_field().
 *
 * @param array $variables
 *  Field preprocessor for add some common class to each Component.
 *
 */
function brizo_theme_preprocess_field(&$variables) {
  if ($variables['element']['#field_name'] == "field_grid_sections") {
		$paragraph = $variables['element']['#object']->toArray();
		$gridNoOfColumns = '';
		if(isset($paragraph['field_grid_no_of_column'])) {
			$gridNoOfColumns = $paragraph['field_grid_no_of_column'][0]['value'];
		}
    foreach ($variables['items'] as $key => $value) {
      $variables['items'][$key]['dynamicClass'] = $gridNoOfColumns;
    }
  }
}

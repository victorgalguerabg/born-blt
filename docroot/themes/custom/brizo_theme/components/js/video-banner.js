
if(jQuery('.embedded-video-wrapper').length) {
  jQuery(".page-node-type-innovations .mobile-hide .embedded-video-wrapper").remove();
}

if(jQuery( window ).width() < 920) {
  jQuery(".desktop-hide .embedded-video-wrapper").insertBefore(".page-node-type-innovations .mobile-hide > div");

}

jQuery( window ).resize(function() {
  if(jQuery( window ).width() < 920) {
    jQuery(".desktop-hide .embedded-video-wrapper").insertBefore(".page-node-type-innovations .mobile-hide > div");
  } else {
    jQuery(".mobile-hide .embedded-video-wrapper").insertBefore(".page-node-type-innovations .desktop-hide > div");
  }
});
jQuery(".embedded-play-icon").click(function() {
  jQuery(this).parent(".video-banner-content").hide();
  var position = jQuery(this).parents('.vertical-align').offset().top - 70;
  jQuery("body, html").animate({
    scrollTop: position
  }, 500 );
});

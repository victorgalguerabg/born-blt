// Product Tile Stories
import productTile from './product-tile.twig';

import productTileData from './product-tile.yml';
import tileVariationData from './product-tile~all-variation.yml';
import tileCdpData from './product-tile~cdp.yml';
import tileCollectionData from './product-tile~collection.yml';
import tileFeatureCollectionData from './product-tile~featured-collection.yml';
import productTileLinkData from './product-tile~link.yml';
import productTilePLPData from './product-tile~PLP.yml';
import RecentlyViewedData from './product-tile~recently-viewed.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Molecules/Product Tile' };

export const ProductTile = () => productTile(productTileData);
export const ProductTileAllVariation = () => productTile(tileVariationData);
export const ProductTileCDP = () => productTile(tileCdpData);
export const ProductTileCollection = () => productTile(tileCollectionData);
export const FeatureCollection = () => productTile(tileFeatureCollectionData);
export const ProductTileLink = () => productTile(productTileLinkData);
export const ProductTilePLP = () => productTile(productTilePLPData);
export const ProductRecentlyView = () => productTile(RecentlyViewedData);

import jQuery from 'jquery';

Drupal.behaviors.tooltip = {
  attach: function (context) {
//(function() {
  // console.log('tooltip')
  // const template = document.getElementById('test');
  // const container = document.createElement('div');
  // container.appendChild(document.importNode(template.content, true));
  //
  tippy('.js-tooltip', {
    trigger: 'mouseenter',
    arrow: true,
    animation: 'fade',
    distance: 12,
    content(reference) {
      const id = reference.getAttribute('data-template');
      const container = document.createElement('div');
      const linkedTemplate = document.getElementById(id).innerHTML;
       jQuery(container).append(linkedTemplate);
      return container;
    },
  })
 //})();
  }
}
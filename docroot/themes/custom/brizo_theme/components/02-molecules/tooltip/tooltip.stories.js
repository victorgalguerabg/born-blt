// Tooltip Stories
import tooltip from './tooltip.twig';
import tooltipList from './tooltip-list.twig';

import tooltipData from './tooltip.yml';
import tooltipListData from './tooltip-list.yml';

import './tooltip';

/**
 * Storybook Definition.
 */
export default { title: 'Molecules/Tooltip' };

export const Tooltip = () => tooltip(tooltipData);
export const TooltipList = () => tooltipList(tooltipListData);

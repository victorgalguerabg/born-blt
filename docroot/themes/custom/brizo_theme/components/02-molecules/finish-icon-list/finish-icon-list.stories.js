// Finish Icon List Stories
import finishIconList from './finish-icon-list.twig';

import finishIconListData from './finish-icon-list.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Molecules/Finish Icon List' };

export const FinishIconList = () => finishIconList(finishIconListData);

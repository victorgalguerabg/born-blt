import breadcrumb from './breadcrumbs/breadcrumbs.twig';
import inlineMenu from './inline-menu/inline-menu.twig';
import mainMenu from './main-menu/main-menu.twig';
import sideBarMenu from './sidebar-menu/sidebar-menu.twig';

import breadcrumbsData from './breadcrumbs/breadcrumbs.yml';
import inlineMenuData from './inline-menu/inline-menu.yml';
import mainMenuData from './main-menu/main-menu.yml';
import sideBarMenuData from './sidebar-menu/sidebar-menu.yml';

import './main-menu-custom';

/**
 * Storybook Definition.
 */
export default { title: 'Molecules/Menus' };

export const breadcrumbs = () => breadcrumb(breadcrumbsData);

export const inline = () => inlineMenu(inlineMenuData);

export const main = () => mainMenu(mainMenuData);

export const SideBarMenu = () => sideBarMenu(sideBarMenuData);

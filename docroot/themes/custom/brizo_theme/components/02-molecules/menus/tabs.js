import jQuery from "jquery";

Drupal.behaviors.tabs = {
  attach: function (context, settings) {
//(function (Drupal) {
  'use strict';
   
    jQuery(".tabs li").click(function(event) {
      event.preventDefault();
      console.log("working");
      jQuery(".tabs li").removeClass("tab-title-active");
      jQuery(this).addClass("tab-title-active");

      jQuery(".tab-data").removeClass("tab-data-active");
      jQuery(".tab-data").eq(jQuery(this).index()).addClass("tab-data-active");

      return false;
    });

    jQuery(".tab-accordion-title").click(function(event) {
      event.preventDefault();

      if(jQuery(this).hasClass("tab-accordion-active") ) {
        jQuery(".tab-accordion-title").removeClass("tab-accordion-active");
        jQuery(".tab-data").slideUp();

        return false;
      }

      jQuery(".tab-accordion-title").removeClass("tab-accordion-active");
      jQuery(this).addClass("tab-accordion-active");

      jQuery(".tab-data").slideUp();
      jQuery(this).next(".tab-data").slideDown();

      return false;
    });

//})(Drupal);
  }
}

jQuery( window ).on( "load", function() {
  var hash = window.location.hash;
  if(hash == '#articles') {
    jQuery(".tabs li").removeClass("tab-title-active");
    jQuery(".tabs li.artTab").addClass("tab-title-active");
    jQuery(".tab-data").removeClass("tab-data-active");
    jQuery(".tab-data.artTab").addClass("tab-data-active");
  }
});
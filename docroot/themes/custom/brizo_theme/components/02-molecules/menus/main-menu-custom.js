import jQuery from "jquery";

Drupal.behaviors.mainMenu = {
  attach: function (context, settings) {
//(function (Drupal) {
  var toggle_expand = document.querySelectorAll('[id*="toggle-expand-id-"]');
  var menu = document.getElementById('main-nav');
  var expand_menu = menu.getElementsByClassName('expand-sub');

  // Mobile Menu Show/Hide.
  toggle_expand.forEach(
    function(cbox) {
      cbox.addEventListener('click', function (e) {
        cbox.classList.toggle('toggle-expand--open');
        menu.classList.toggle('main-nav--open');
      });
    }
  );

  // Expose mobile sub menu on click.
  for (var i = 0; i < expand_menu.length; i++) {
    expand_menu[i].addEventListener('click', function (e) {
      var menu_item = e.currentTarget;
      var sub_menu = menu_item.nextElementSibling;

      menu_item.classList.toggle('expand-sub--open');
      sub_menu.classList.toggle('main-menu--sub-open');
      jQuery(".main-menu").removeClass('sub-menu-active');
      sub_menu.classList.toggle('sub-menu-active');
    });
  }

  jQuery(document).on("click", ".main-nav > .main-menu > .sub-data > li > a", function(e) {
    jQuery(".main-menu__item .main-menu--sub").removeClass("open-main-menu");
    jQuery(".main-nav > .main-menu > .sub-data > li > a").removeClass("active-menu");
    jQuery(this).addClass("active-menu");
    //jQuery(this).nextUntil("ul").addClass("open-main-menu");
    jQuery(this).next().next().next("ul").addClass("open-main-menu");
    jQuery('.banner .breadcrumb-block').addClass('breadcrumbactive');
    e.preventDefault();
  });

  const jQuerymenu = jQuery('.main-nav .main-menu--sub-1');
  jQuery(document).mouseup(e => {
    if (!jQuerymenu.is(e.target) && jQuerymenu.has(e.target).length === 0) {
      jQuery(".main-menu__item .main-menu--sub").removeClass("open-main-menu");
      jQuery(".main-nav > .main-menu > .sub-data > li > a").removeClass("active-menu");
      jQuery('.banner .breadcrumb-block').removeClass('breadcrumbactive');
    }
  });

  jQuery(".expand-sub").click(function() {
    jQuery(".menu-back").addClass("menu-back--active-hide");
    jQuery(this).prev(".menu-back").addClass("menu-back--active");
    jQuery(this).prev(".menu-back").removeClass("menu-back--active-hide");
    jQuery(".main-menu").removeClass('prev-menu-active');

    jQuery(".utility-mobile").addClass("hide-utility-mobile");
  });

  jQuery(".menu-back").click(function() {
    jQuery(this).next().next(".main-menu.main-menu--sub").removeClass("main-menu--sub-open");
    jQuery(this).removeClass("menu-back--active");
    jQuery(".menu-back").removeClass("menu-back--active-hide");
    jQuery(".main-menu").removeClass('sub-menu-active');
    jQuery(this).parent().parent().parent('.main-menu--sub-open').addClass('sub-menu-active');

    jQuery(".main-menu").removeClass('prev-menu-active');
    jQuery(this).next().next('.main-menu').addClass('prev-menu-active');
    if(!(jQuery(".menu-back").hasClass("menu-back--active"))) {
      jQuery(".utility-mobile").removeClass("hide-utility-mobile");
    }
  });

  jQuery(".toggle-expand").click(function() {
    jQuery(".utility-mobile").toggleClass("active-utility-mobile");
    jQuery(".header-search-wrapper").removeClass("mobile-search-active");
    jQuery("#ui-id-1").removeClass("mobile-active-list");
  });

  jQuery(".main-menu__item--disable-menu-link").each(function() {
    jQuery(".main-menu__item--disable-menu-link > a").removeAttr("href");
  });



//})(Drupal);
  }
}

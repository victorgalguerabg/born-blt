// Accordian Stories
import accordion from './accordion-item.twig';

import accordionItemData from './accordion-item.yml';

import './accordion-item';

/**
 * Storybook Definition.
 */
export default { title: 'Molecules/Accordion Item' };

export const Accordion = () => accordion(accordionItemData);

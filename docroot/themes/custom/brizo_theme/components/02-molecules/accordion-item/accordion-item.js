// UNCOMMENT IF DRUPAL - see components/_meta/_01-foot.twig for attachBehaviors
import jQuery from 'jquery';

Drupal.behaviors.accordion = {
  attach: function (context, settings) {

   // (function () { // REMOVE IF DRUPAL

      'use strict';

      jQuery(".accordion .accordion-title").click(function() {
        jQuery(this).toggleClass("accordion-title-active");
        jQuery(this).next(".accordion-desc").toggleClass("accordion-desc-active").slideToggle();
        jQuery(this).parent(".accordion-block").toggleClass("accordion-block-active");
      });

      jQuery(".accordion-single .accordion-title").click(function() {
        if(jQuery(this).hasClass("accordion-title-active") ) {
          jQuery(".accordion-single .accordion-title").removeClass("accordion-title-active");
          jQuery(".accordion-single .accordion-desc").removeClass("accordion-desc-active").slideUp();
          jQuery(".accordion-single .accordion-block").removeClass("accordion-block-active");

          return false;
        }

        jQuery(".accordion-single .accordion-title").removeClass("accordion-title-active");
        jQuery(".accordion-single .accordion-desc").removeClass("accordion-desc-active").slideUp();
        jQuery(".accordion-single .accordion-block").removeClass("accordion-block-active");

        jQuery(this).addClass("accordion-title-active");
        jQuery(this).next(".accordion-desc").addClass("accordion-desc-active").slideDown();
        jQuery(this).parent(".accordion-block").addClass("accordion-block-active");
      });

    //})

  }
};

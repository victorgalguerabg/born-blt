(function () {

  jQuery(".showroom").click(function() {
    jQuery(".showroom-wrapper").addClass("active-showroom-wrapper");
  });

  jQuery(".showroom-data .icon-menu-close").click(function() {
    jQuery(".showroom-wrapper").removeClass("active-showroom-wrapper");
  });

  const jQuerymenu = jQuery('.showroom-wrapper');
  jQuery(document).mouseup(e => {
    if (!jQuerymenu.is(e.target) && jQuerymenu.has(e.target).length === 0) {
      jQuery(".showroom-wrapper").removeClass("active-showroom-wrapper");
    }
  });


  jQuery(function() {
    jQuery(window).scroll(function () {
        if (jQuery(this).scrollTop() < 2) {
          jQuery('.header-utility').removeClass("onscroll-hide");
          jQuery('.header').removeClass("sticky-header");
        } else {
          jQuery('.header-utility').addClass("onscroll-hide");
          jQuery('.header').addClass("sticky-header");
        }

        if (jQuery(this).scrollTop() < 2) {
          if(jQuery("ul#ui-id-1").length) {
            jQuery("ul#ui-id-1").removeClass("sticky-autosearch");
          }

        } else {
          if(jQuery("ul#ui-id-1").length) {
            jQuery("ul#ui-id-1").addClass("sticky-autosearch");
          }
        }
    });
});

  /* jQuery(".country-dd-title").click(function () {
    jQuery(this).toggleClass("active-country-dd-title")
    jQuery(".country-dd-desc").toggleClass("active-country-dd-desc");
    jQuery(".country-dd-desc").slideToggle();
    jQuery(".utility-country-dropdown").toggleClass("active-utility-country-dropdown");
  });

  });

  jQuery(".country-dd-title-img").html(jQuery(".active-country").find('img')[0].outerHTML); */

}) ();

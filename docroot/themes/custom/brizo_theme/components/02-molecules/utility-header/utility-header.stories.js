// Utility Header Stories
import utilityHeader from './utility-header.twig';

import utilityHeaderData from './utility-header.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Molecules/Utility Header' };

export const UtilityHeader = () => utilityHeader(utilityHeaderData);

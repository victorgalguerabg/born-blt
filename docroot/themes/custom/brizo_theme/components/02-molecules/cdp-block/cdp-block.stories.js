// CDP Block Stories
import cdpBlock from './cdp-block.twig';

/**
 * Storybook Definition.
 */
export default { title: 'Molecules/CDP Block' };

export const CDPBlock = () => cdpBlock();

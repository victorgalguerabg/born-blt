// Search Stories
import search from './serach.twig';

import searchData from './serach.yml';

//import './header-serach';

/**
 * Storybook Definition.
 */
export default { title: 'Molecules/Search' };

export const Search = () => search(searchData);

import jQuery from 'jquery';

/**
 * @file
 * A JavaScript file containing the main menu functionality (small/large screen)
 *
 */

// JavaScript should be made compatible with libraries other than jQuery by
// wrapping it with an "anonymous closure". See:
// - https://drupal.org/node/1446420
// - http://www.adequatelygood.com/2010/3/JavaScript-Module-Pattern-In-Depth


// (function (Drupal) { // UNCOMMENT IF DRUPAL.
//
Drupal.behaviors.productThumbnailSlider = {
  attach: function (context) {

  //(function () { // REMOVE IF DRUPAL.

    'use strict';

    jQuery('.multiple-items').on('init', function(event, slick){
      console.log("initialised")
     });
     jQuery('.multiple-items').slick();


     if(jQuery(".desktop-thumb-img .slick-track .thumb-img-block").length == 1) {
       jQuery(".desktop-thumb-img .slick-track .thumb-img-block").hide();
     }

     if(jQuery(".mobile-thumb-img .slick-dots li").length == 1) {
       jQuery(".mobile-thumb-img .slick-dots").hide();
     }


    var default_img = jQuery(".thumb-img-block.slick-current").find("img").attr("src");
    jQuery(".prod-big-img").find("img").attr('src', default_img);
    jQuery(".download__img-wrapper a").attr('href', default_img);

    var first_img_pos = jQuery(".thumb-img-block.slick-current").index();
    jQuery(".desktop-thumb-img .thumb-img-block").click(function(){
      jQuery(".desktop-thumb-img .thumb-img-block").removeClass("active-thumbnail");
      jQuery(this).toggleClass("active-thumbnail");
      var img_src = jQuery(this).find("img").attr("src");

      console.log("URL"+ img_src);
      var img_pos = jQuery(this).index();

      jQuery(".prod-big-img").find("img").attr('src', img_src);
      jQuery(".download__img-wrapper a").attr('href', img_src);

      if(img_pos == first_img_pos) {
        jQuery(".thumb-img-wrapper-list .slider_flag").show();
      } else {
        jQuery(".thumb-img-wrapper-list .slider_flag").hide()
      }
    });

    var m_first_img_pos = jQuery(".mobile-thumb-img .thumb-img-block.slick-current").index();

    jQuery('.mobile-thumb-img .multiple-items').on('afterChange', function(){
      var m_active_img_pos = jQuery(".thumb-img-wrapper-list.mobile-thumb-img").find(".thumb-img-block.slick-current").index();

      if(m_active_img_pos == m_first_img_pos) {
        jQuery(".thumb-img-wrapper-list .slider_flag").show();
      } else {
        jQuery(".thumb-img-wrapper-list .slider_flag").hide();
      }
    });
  //})(); // REMOVE IF DRUPAL.

  // })(Drupal); // UNCOMMENT IF DRUPAL.

  }
}

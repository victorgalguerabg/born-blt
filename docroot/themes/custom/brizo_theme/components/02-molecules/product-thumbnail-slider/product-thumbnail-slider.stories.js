// Product Thumbnail Slider Stories
import productSlider from './product-thumbnail-slider.twig';

import productSliderData from './product-thumbnail-slider.yml';

import './product-thumbnail-slider'

/**
 * Storybook Definition.
 */
export default { title: 'Molecules/Product Thumbnail Slider' };

export const ProductThumbnailSlider = () => productSlider(productSliderData);

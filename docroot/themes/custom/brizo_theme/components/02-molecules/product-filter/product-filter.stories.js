// Product Filter Stories
import cdpFilter from './cdp-filters/cdp-filter.twig';
import cdpSelectedFilter from './cdp-selected-filter/cdp-selected-filter.twig';
import plpFilters from './plp-filters/plp-filter.twig';
import plpSelectedFilter from './plp-selected-filter/selected-filter.twig';

import './product-filter';

/**
 * Storybook Definition.
 */
export default { title: 'Molecules/Product Filter' };

export const CdpFilter = () => cdpFilter();
export const CdpSelectedFilter = () => cdpSelectedFilter();
export const PlpFilters = () => plpFilters();
export const PlpSelectedFilter = () => plpSelectedFilter();

import jQuery from "jquery";

(function (Drupal) { // REMOVE IF DRUPAL

  'use strict';

  // --------  COLLECTION FILTER JS START  -------- //
  jQuery(".cdp-filter-view-all").click(function() {
    jQuery(this).parent().parent().addClass("load-more-filters");
    jQuery(this).hide();
    jQuery(this).next(".cdp-filter-view-less").css("display","inline-block");

    var maxval = jQuery(this).parent().parent().attr("data-max");
    var indexval = maxval - 1;
    var sum = 0;

    jQuery(this).parent().parent().find(".cdp-filter-block").each(function(){
      if(jQuery(this).index() <= indexval) {
        var filterheight = 0;
        filterheight = jQuery(this).outerHeight();
        sum = sum + filterheight;
      }
    });

    jQuery(this).parent().prev(".cdp-filter-wrapper").css("height", sum);

  });

  jQuery(".cdp-filter-view-less").click(function() {
    jQuery(this).hide();
    jQuery(this).prev(".cdp-filter-view-all").show();
    jQuery('.load-more-filters .cdp-filter-wrapper').scrollTop(0);
    jQuery(".accordion-desc").removeClass("load-more-filters");

    var maxval = jQuery(this).parent().parent().attr("data-min");
    var indexval = maxval - 1;
    var sum = 0;

    jQuery(this).parent().parent().find(".cdp-filter-block").each(function(){
      if(jQuery(this).index() <= indexval) {
        var filterheight = 0;
        filterheight = jQuery(this).outerHeight();
        sum = sum + filterheight;
      }
    });

    jQuery(this).parent().prev(".cdp-filter-wrapper").css("height", sum);
  });

  jQuery(".accordion-desc .cdp-filter-wrapper").each(function(){
    var sum = 0;
    var minval = jQuery(this).parent().attr("data-min");
    var showval = minval - 1;
    var ttlchild = jQuery(this).children().length;

    jQuery(this).children(".cdp-filter-block").each(function(){
      //jQuery(this).css("height", jQuery(this).outerHeight());

      if(jQuery(this).index() <= showval) {
        var varheight = 0;
        varheight = jQuery(this).outerHeight();
        sum = sum + varheight;
      }

    });

    if((ttlchild < minval) || (ttlchild == minval)) {
      jQuery(this).next(".cdp-filer-operations").find(".cdp-filter-view-all").hide();
    }

    jQuery(this).css("height", sum);
  });

  jQuery(".cdp-filter-mobile").click(function() {
    jQuery(".filter-expand").toggleClass("toggle-expand--open");
    jQuery(".cdp-accordion").toggleClass("cdp-accordion--open");
    jQuery(".mobile-clear-filter").toggleClass("active-mobile-clear-filter");
    jQuery(".cdp-filter").toggleClass("active-mobile-filter")
  });

  jQuery(".done-filter").click(function() {
    jQuery(".filter-expand").toggleClass("toggle-expand--open");
    jQuery(".cdp-accordion").toggleClass("cdp-accordion--open");
    jQuery(".mobile-clear-filter").toggleClass("active-mobile-clear-filter");
    jQuery(".cdp-filter").toggleClass("active-mobile-filter")
  });


  // --------  COLLECTION FILTER JS END  -------- //


  // --------  PLP FILTER JS START  -------- //

  jQuery(".plp-filter-view-all").click(function() {
    jQuery(this).parent().parent().addClass("load-more-filters");
    jQuery(this).hide();
    jQuery(this).next(".plp-filter-view-less").css("display","inline-block");

    var maxval = jQuery(this).parent().parent().attr("data-max");
    var indexval = maxval - 1;
    var sum = 0;

    jQuery(this).parent().parent().find(".plp-filter-block").each(function(){
      if(jQuery(this).index() <= indexval) {
        var filterheight = 0;
        filterheight = jQuery(this).outerHeight();
        sum = sum + filterheight;
      }
    });

    jQuery(this).parent().prev(".plp-filter-wrapper").css("height", sum);

  });

  jQuery(".plp-filter-view-less").click(function() {
    jQuery(this).hide();
    jQuery(this).prev(".plp-filter-view-all").show();
    jQuery('.load-more-filters .plp-filter-wrapper').scrollTop(0);
    jQuery(".accordion-desc").removeClass("load-more-filters");

    var maxval = jQuery(this).parent().parent().attr("data-min");
    var indexval = maxval - 1;
    var sum = 0;

    jQuery(this).parent().parent().find(".plp-filter-block").each(function(){
      if(jQuery(this).index() <= indexval) {
        var filterheight = 0;
        filterheight = jQuery(this).outerHeight();
        sum = sum + filterheight;
      }
    });

    jQuery(this).parent().prev(".plp-filter-wrapper").css("height", sum);

  });

  jQuery(".accordion-desc .plp-filter-wrapper").each(function(){
    var sum = 0;
    var minval = jQuery(this).parent().attr("data-min");
    var showval = minval - 1;
    var ttlchild = $(this).children().length;

    jQuery(this).children(".plp-filter-block").each(function(){
      //jQuery(this).css("height", jQuery(this).outerHeight());

      if(jQuery(this).index() <= showval) {
        var varheight = 0;
        varheight = jQuery(this).outerHeight();
        sum = sum + varheight;
      }

    });

    if(ttlchild < minval) {
      jQuery(this).next(".plp-filer-operations").find(".plp-filter-view-all").hide();
    }

    jQuery(this).css("height", sum);
  });


  jQuery(".plp-filter-mobile").click(function() {
    jQuery(".filter-expand").toggleClass("toggle-expand--open");
    jQuery(".plp-accordion").toggleClass("plp-accordion--open");
    jQuery(".mobile-clear-filter").toggleClass("active-mobile-clear-filter");
    jQuery(".plp-filter").toggleClass("active-mobile-filter")
  });

  jQuery(".done-filter").click(function() {
    jQuery(".filter-expand").removeClass("toggle-expand--open");
    jQuery(".plp-accordion").removeClass("plp-accordion--open");
    jQuery(".mobile-clear-filter").removeClass("active-mobile-clear-filter");
    jQuery(".plp-filter").removeClass("active-mobile-filter");
  });


  // --------  PLP FILTER JS END  -------- //


})(Drupal); // REMOVE IF DRUPAL

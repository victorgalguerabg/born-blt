// Showroom PopUp Stories
import showroomPopup from './showroom-popup.twig';

import showroomPopupData from './showroom-popup.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Molecules/Showroom Popup' };

export const ShowroomPopup = () => showroomPopup(showroomPopupData);

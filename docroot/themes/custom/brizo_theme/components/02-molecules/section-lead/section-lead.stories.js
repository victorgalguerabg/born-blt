// Section Lead Stories
import sectionLead from './section-lead.twig';
import categoryLead from './category-lead.twig';

import sectionLeadData from './section-lead.yml';
import categoryLeadData from './category-lead.yml';
import sectionLeadButtonData from './section-lead~button.yml';
import sectionLeadCWLLPData from './section-lead~content- with-link-left-position.yml';
import sectionLeadCWLData from './section-lead~content- with-link.yml';
import sectionLeadWCData from './section-lead~with-content.yml';
import sectionLeadWLData from './section-lead~with-link.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Molecules/Section Lead' };

export const SectionLead = () => sectionLead(sectionLeadData);
export const CategoryLead = () => categoryLead(categoryLeadData);
export const SectionLeadButton = () => sectionLead(sectionLeadButtonData);
export const sectionLeadCWLLP = () => sectionLead(sectionLeadCWLLPData);
export const sectionLeadContentWithLink = () => sectionLead(sectionLeadCWLData);
export const sectionLeadWithContent = () => sectionLead(sectionLeadWCData);
export const sectionLeadWithLink = () => sectionLead(sectionLeadWLData);

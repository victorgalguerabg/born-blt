// Card Stories
import card from './01-card/card.twig';
import cardCover from './02-card-cover/card-cover.twig';
import cardSummary from './03-card-summary/card-block-summary.twig';
import cardCta from './04-card-cta/card-cta.twig';

import cardData from './01-card/card.yml';
import cardBgData from './01-card/card~bg.yml';
import cardCoverData from './02-card-cover/card-cover.yml';
import cardSummaryData from './03-card-summary/card-block-summary.yml';
import cardCtaData from './04-card-cta/card-cta.yml';
import cardCtaLeftImageDarkData from './04-card-cta/card-cta~left-image-dark.yml';
import cardCtaLeftImageLightData from './04-card-cta/card-cta~left-image-light.yml';
import cardCtaRightImageDarkData from './04-card-cta/card-cta~right-image-dark.yml';
import cardCtaRightImageLightData from './04-card-cta/card-cta~right-image-light.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Molecules/Card' };

export const Cards = () => card(cardData);

export const cardWithBackground = () => card(cardBgData);

export const CardCover = () => cardCover(cardCoverData);

export const CardSummary = () => cardSummary(cardSummaryData);

export const CardCta = () => cardCta(cardCtaData);

export const CardCtaLeftImageDark = () => cardCta(cardCtaLeftImageDarkData);

export const CardCtaLeftImageLight = () => cardCta(cardCtaLeftImageLightData);

export const CardCtaRightImageDark = () => cardCta(cardCtaRightImageDarkData);

export const CardCtaRightImageLight = () => cardCta(cardCtaRightImageLightData);

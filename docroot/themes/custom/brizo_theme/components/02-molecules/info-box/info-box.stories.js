// Info Box Stories
import infoBlock from './info-box.twig';

import infoBlockData from './info-box.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Molecules/Info Block' };

export const InfoBlock = () => infoBlock(infoBlockData);

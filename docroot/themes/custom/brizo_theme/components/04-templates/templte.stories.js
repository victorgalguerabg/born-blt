// Search Stories
import template from './_default.twig';
import placeHolder from './_place-holder.twig';
import fullWidth from './full-width.twig';
import withSidebar from './with-sidebar.twig';

/**
 * Storybook Definition.
 */
export default { title: 'Template/Template' };

export const Template = () => template();
export const PlaceHolder = () => placeHolder();
export const FullWidth = () => fullWidth();
export const WithSidebar = () => withSidebar();

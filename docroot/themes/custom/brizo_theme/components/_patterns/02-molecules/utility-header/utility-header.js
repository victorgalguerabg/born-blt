(function () {

  jQuery(".showroom").click(function() {
    jQuery(".showroom-wrapper").addClass("active-showroom-wrapper");
  });

  jQuery(".showroom-data .icon-menu-close").click(function() {
    jQuery(".showroom-wrapper").removeClass("active-showroom-wrapper");
  });

  const jQuerymenu = jQuery('.showroom-wrapper');
  jQuery(document).mouseup(e => {
    if (!jQuerymenu.is(e.target) && jQuerymenu.has(e.target).length === 0) {
      jQuery(".showroom-wrapper").removeClass("active-showroom-wrapper");
    }
  });


  jQuery(function() {
    jQuery(window).scroll(function () {
        if (jQuery(this).scrollTop() < 2) {
          jQuery('.header-utility').removeClass("onscroll-hide");
          jQuery('.header').removeClass("sticky-header");
        } else {
          jQuery('.header-utility').addClass("onscroll-hide");
          jQuery('.header').addClass("sticky-header");
        }

        if (jQuery(this).scrollTop() < 2) {
          if(jQuery("ul#ui-id-1").length) {
            jQuery("ul#ui-id-1").removeClass("sticky-autosearch");
          }

        } else {
          if(jQuery("ul#ui-id-1").length) {
            jQuery("ul#ui-id-1").addClass("sticky-autosearch");
          }
        }
    });
});

jQuery(".desktop-country .country-dd-title").mouseover(function() {
  jQuery(this).addClass("active-country-dd-title")
   jQuery(".desktop-country .country-dd-desc").addClass("active-country-dd-desc");
    jQuery(".desktop-country .country-dd-desc").slideToggle();
   jQuery(".desktop-country .utility-country-dropdown").addClass("active-utility-country-dropdown");
 });

 jQuery(".mobile-country .country-dd-title").click(function() {
   jQuery('.mobile-country .country-dd-desc').addClass('mobile-active-country');

  });

jQuery(window).click(function() {
  //Hide the menus if visible
  jQuery(".desktop-country .country-dd-desc").css('display','none');
});

jQuery('header.header').mouseleave(function() {
  jQuery(".desktop-country .country-dd-desc").css('display','none');
})


jQuery(".desktop-country .country-dd-desc li a").click(function() {
  jQuery(".desktop-country .country-dd-desc li a").removeClass("active-country");
  jQuery(this).addClass("active-country");
  var countryLabelName = jQuery(".active-country").text();
  jQuery(".desktop-country .country-dd-title .active-country-title").html(countryLabelName.substr(0, jQuery(".active-country").text().indexOf('-')));
});

var countryLabel = jQuery(".active-country").text();
jQuery(".country-dd-title .active-country-title").html(countryLabel.substr(0, jQuery(".active-country").text().indexOf('-')));

}) ();

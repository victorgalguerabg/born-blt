---
title: Colors
---
Colors below are automatically pulled from `_color-vars.scss` and can be used like so:

```scss
.class {
  color: $white;
}
```

On Dark Backgrounds, use white and gray-10.

On Light Backgrounds, use gray-10 and gray-10.

For notifications, use the light color as the background, and the dark color as the border.

Reference: https://deltafaucet.invisionapp.com/share/7PTGK7MSZFK#/screens/379032060
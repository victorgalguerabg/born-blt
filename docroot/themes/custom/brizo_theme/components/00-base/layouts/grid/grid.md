---
el: ".grid"
title: "Grid"
---
## Grid Layout

The grid is using a 12 column grid structure.  Each class specifies the amount of columns for each column to spread.

Example: 
Full width: grid-col-12 grid-col-sm-12 grid-col-md-12 grid-col-lg-12

The following modifiers can be used to set different columns for each screen width:

sm - Narrow
md - Medium witdh
lg - Desktop

If no modifier is present, the default width will be used.

The following would be two columns in tablets and desktops.  For mobile devices, the column will be full width.

grid-col-12 grid-col-sm-12 grid-col-md-6 grid-col-lg-6

Background color and height added for display in Pattern Lab only.

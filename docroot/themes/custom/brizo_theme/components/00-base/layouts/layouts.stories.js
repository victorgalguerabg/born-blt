import grid from './grid/grid.twig';

/**
 * Storybook Definition.
 */
export default {
  title: 'Base/Layouts',
};

export const Grid = () => grid();

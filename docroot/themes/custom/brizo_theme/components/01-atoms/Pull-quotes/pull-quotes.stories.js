// Pull Quotes Stories
import pullQuotes from './Pull-quotes.twig';

import pullQuotesData from './Pull-quotes.yml';
import pullQuotesAuthorData from './Pull-quotes~author.yml';
import pullQuotesgrayData from './Pull-quotes~gray.yml';
import pullQuotesLeftAlignData from './Pull-quotes~left-align.yml';
import pullQuotesWhiteData from './Pull-quotes~white.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Atoms/Pull Quotes' };

export const PullQuotes = () => pullQuotes(pullQuotesData);
export const PullQuotesAuthor = () => pullQuotes(pullQuotesAuthorData);
export const PullQuotesGray = () => pullQuotes(pullQuotesgrayData);
export const PullQuotesLeftAlign = () => pullQuotes(pullQuotesLeftAlignData);
export const PullQuotesWhite = () => pullQuotes(pullQuotesWhiteData);

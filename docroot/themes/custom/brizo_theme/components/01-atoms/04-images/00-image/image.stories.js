import image from './_image.twig';
import picture from './_picture.twig';
import figure from './figure.twig';
import responsiveImage from './responsive-image.twig';

import figureYml from './02-figure.yml';
import responsiveImageData from './00-responsive-image.yml';
import imageLogoData from './image~logo.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Atoms/Images' };

export const Image = () => image();
export const ImageLogo = () => image(imageLogoData);
export const Picture = () => picture();
export const Figure = () => figure(figureYml);
export const ResponsiveImage = () => responsiveImage(responsiveImageData);

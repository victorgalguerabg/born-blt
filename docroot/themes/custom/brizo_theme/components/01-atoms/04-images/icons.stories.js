import icons from './icons/icons.twig';
import iconsList from './icons/icons-list.twig';
import finishIcon from './finish-icon/finish-icon.twig';

import iconsData from './icons/icons.yml';
import iconsListData from './icons/icons-list.yml';
import finishIconData from './finish-icon/finish-icon.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Atoms/Icons' };

export const Icons = () => icons(iconsData);
export const IconsList = () => iconsList(iconsListData);
export const FinishIcon = () => finishIcon(finishIconData);

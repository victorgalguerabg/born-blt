// Divider Stories
import divider from './divider.twig';

import dividerData from './divider.yml';
import dividerBlackData from './divider~ black.yml';
import dividerDarkData from './divider~ dark.yml';
import dividerDarkerData from './divider~ darker.yml';
import dividerDarkestData from './divider~ darkest.yml';
import dividerTealData from './divider~ Teal-1.yml';
import dividerWhiteData from './divider~ white.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Atoms/Divider' };

export const Divider = () => divider(dividerData);
export const DividerBlack = () => divider(dividerBlackData);
export const DividerDark = () => divider(dividerDarkData);
export const DividerDarker = () => divider(dividerDarkerData);
export const DividerDarkest = () => divider(dividerDarkestData);
export const DividerTeal = () => divider(dividerTealData);
export const DividerWhite = () => divider(dividerWhiteData);

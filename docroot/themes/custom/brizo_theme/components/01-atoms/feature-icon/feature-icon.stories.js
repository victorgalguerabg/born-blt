// Feature Icon Stories
import featureIcon from './_feature-icon.twig';

/**
 * Storybook Definition.
 */
export default { title: 'Atoms/Feature Icon' };

export const FeatureIcon = () => featureIcon();

import ctaLink from './cta-link/cta-link.twig';
import link from './link/link.twig';

import ctaLinkData from './cta-link/cta-link.yml';
import linkData from './link/link.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Atoms/Links' };

export const CTALinks = () => ctaLink(ctaLinkData);

export const links = () => link(linkData);

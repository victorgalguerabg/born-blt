// Table Stories
import table from './tables.twig';

/**
 * Storybook Definition.
 */
export default { title: 'Atoms/Table' };

export const Tables = () => table();

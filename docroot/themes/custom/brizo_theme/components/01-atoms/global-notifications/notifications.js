import jQuery from 'jquery';

Drupal.behaviors.alert = {
  attach: function (context, settings) {
    jQuery(".alert i").click(function(){
      jQuery(this).parent().fadeOut();
    });
  }
}
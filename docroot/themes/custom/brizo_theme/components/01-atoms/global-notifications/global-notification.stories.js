// Global Notification Stories
import notifications from './notifications.twig';

import notificationsErrorData from './notifications~error.yml';
import notificationsNeutralData from './notifications~neutral.yml';
import notificationsSuccessData from './notifications~success.yml';
import notificationsWarningData from './notifications~warning.yml';

import './notifications';
/**
 * Storybook Definition.
 */
export default { title: 'Atoms/Notification' };

export const Notifications = () => notifications(notificationsNeutralData);
export const NotificationsError = () => notifications(notificationsErrorData);
export const NotificationsSuccess = () => notifications(notificationsSuccessData);
export const NotificationsWarning = () => notifications(notificationsWarningData);

import checkbox from './checkbox/checkbox.twig';
import numericInput from './numeric-input/numeric-input.twig';
import radio from './radio/radio.twig';
import radioGroup from './radio/radio-group.twig';
import select from './select/select.twig';
import selectFullBorder from './select/full-border-select.twig';
import textfield from './01-textfields/textfields.twig';
import textfieldSizes from './01-textfields/textfields - sizes.twig';
import textfieldTextArea from './01-textfields/textfields - text - area.twig';
import textfieldSIF from './01-textfields/textfields-special-input-fields.twig';
import switchBox from './switch/switch.twig';
import upload from './upload/upload.twig';

import checkboxData from './checkbox/checkbox.yml';
import numericInputData from './numeric-input/numeric-input.yml';
import radioData from './radio/radio.yml';
import radioGroupData from './radio/radio-group.yml';
import selectOptionsData from './select/select.yml';
import selectFullBorderData from './select/full-border-select.yml';
import selectDisableData from './select/select~disabled.yml';
import switchBoxData from './switch/switch.yml';
import uploadData from './upload/upload.yml';
import uploadActiveData from './upload/upload~active.yml';

import './numeric-input/numeric-input';

/**
 * Storybook Definition.
 */
export default { title: 'Atoms/Forms' };

export const checkboxes = () => checkbox(checkboxData);

export const NumericInput = () => numericInput(numericInputData);

export const radioButtons = () => radioGroup(radioGroupData);

export const radioButtonsGroup = () => radio(radioData);

export const selectDropdowns = () => select(selectOptionsData);

export const selectDropdownFullBorder = () => selectFullBorder(selectFullBorderData);

export const selectDropdownsDisable = () => select(selectDisableData);

export const SwitchBox = () => switchBox(switchBoxData);

export const textfieldsExamples = () => textfield();

export const textfieldsSizes = () => textfieldSizes();

export const textfieldsTextArea = () => textfieldTextArea();

export const textfieldsSpecialInputField = () => textfieldSIF();

export const Upload = () => upload(uploadData);

export const UploadActive = () => upload(uploadActiveData);

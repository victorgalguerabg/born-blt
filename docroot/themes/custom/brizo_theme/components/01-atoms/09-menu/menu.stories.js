// Menu Stories
import tab from './tab/tab.twig';

import tabData from './tab/tab.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Atoms/Tab' };

export const Tab = () => tab(tabData);

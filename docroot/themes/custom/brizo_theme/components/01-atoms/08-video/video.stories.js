// Video Stories
import video from './video.twig';

import videoData from './video.yml';
import videoFullData from './video~full.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Atoms/Video' };

export const Video = () => video(videoData);
export const VideoFull = () => video(videoFullData);

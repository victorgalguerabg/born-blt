// Buttons Stories
import button from './01-button.twig';

import buttonData from './button.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Atoms/Button' };

export const Buttons = () => button(buttonData);

// CDP Stories
import cdp from './cdp.twig';

import cdpData from './cdp.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Pages/CDP' };

export const CDP = () => cdp(cdpData);

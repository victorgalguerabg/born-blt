// PLP Stories
import plp from './plp.twig';

import plpData from './plp.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Pages/PLP' };

export const PLP = () => plp(plpData);

import jQuery from 'jquery';

Drupal.behaviors.bannerCarousel = {
  attach: function (context) {

var $ = jQuery.noConflict();
//(function() {
  $('.banner-carousel').slick({
    arrows: true,
    dots: true,
    autoplay: true,
    autoplaySpeed: 5000,
    adaptiveHeight: true,
    responsive: [
      {
        breakpoint: 1020,
        settings: {
          arrows: true,
          dots: true,
        },
      },
    ],
  });
  $('.banner_carousel_noautopay').slick({
    arrows: true,
    dots: true,
    autoplay: false,
    autoplaySpeed: 5000,
    adaptiveHeight: true,
    responsive: [
      {
        breakpoint: 1020,
        settings: {
          arrows: true,
          dots: true,
        },
      },
    ],
  });

//})();
  }
}

// Banner Stories
import banner from './banner.twig';

import bannerData from './banner.yml';
import bpicData from './banner~page-image-carousel.yml';
import bpageTitleLightv1Data from './banner~page-title-light-v1.yml';
import bpageTitleLightv2Data from './banner~page-title-light-v2.yml';
import bpageTitleLightv3Data from './banner~page-title-light-v3.yml';

import './banner-carousel';
import './picturefill';

/**
 * Storybook Definition.
 */
export default { title: 'Organisms/Banner' };

export const Banner = () => banner(bannerData);
export const BannerPageImageCarousel = () => banner(bpicData);
export const BannerPageTitleLightV1 = () => banner(bpageTitleLightv1Data);
export const BannerPageTitleLightV2 = () => banner(bpageTitleLightv2Data);
export const BannerPageTitleLightV3 = () => banner(bpageTitleLightv3Data);

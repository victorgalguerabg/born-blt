// Site Header / Footer Stories
import siteHeader from './site-header/site-header.twig';
import siteFooter from './site-footer/site-footer.twig';

import siteHeaderData from './site-header/site-header.yml';
import siteFooterData from './site-footer/site-footer.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Organisms/Site' };

export const SiteHeader = () => siteHeader(siteHeaderData);
export const SiteFooter = () => siteFooter(siteFooterData);

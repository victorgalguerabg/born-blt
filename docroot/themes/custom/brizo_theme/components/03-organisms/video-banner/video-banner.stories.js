// Video Banner Stories
import videoBanner from './video-banner.twig';
import vbt from './video-banner-thumbnail.twig';

import videoBannerData from './video-banner.yml';
import vbtData from './video-banner-thumbnail.yml';

import './video-banner';

/**
 * Storybook Definition.
 */
export default { title: 'Organisms/Video Banner' };

export const VideoBanner = () => videoBanner(videoBannerData);
export const VideoBannerThumbnail = () => vbt(vbtData);

Drupal.behaviors.videoBanner = {
  attach: function (context) {
//(function() {
  const videoBanners = [].slice.call(document.querySelectorAll('.video-banner'));
  
  videoBanners.forEach(banner => {
    const playButton = banner.querySelector('.video-banner__play');
    const video = banner.parentNode.querySelector('iframe');
    
    playButton.addEventListener('click', function() {
      const src = video.getAttribute('src');
      video.setAttribute('src', src + '?rel=0&showinfo=0&autoplay=1');
      setTimeout(() => {
        banner.classList.add('is-playing');
      }, 150)
    });
  });
//})();
  }
}

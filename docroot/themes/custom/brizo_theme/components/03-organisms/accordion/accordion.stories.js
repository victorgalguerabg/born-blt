// Accordian Stories
import accordion from './accordion.twig';

import accordionData from './accordion.yml';
import accordionSingleOpenData from './accordion~single-open.yml';

// import './accordion-item';

/**
 * Storybook Definition.
 */
export default { title: 'Organisms/Accordion' };

export const Accordion = () => accordion(accordionData);
export const AccordionSingleOpen = () => accordion(accordionSingleOpenData);

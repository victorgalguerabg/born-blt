(function ($, Drupal, drupalSettings, Backbone) {
  if($(".featured-banner").length) {
    $(".featured-banner").insertBefore(".main");
  }
  Drupal.behaviors.collectionDetailBehaviour = {
    attach: function (context, settings) {
      let gtm_collection = [];
      let gtm_collection_click = [];
      //let size = drupalSettings.collectionProductsPerPage;
      let category = drupalSettings.collectionCategory;
      let prod_category = drupalSettings.productCategory || [];
      let collection = drupalSettings.collectionName;
      let finishData = drupalSettings.finishData;
      let availableFinish = drupalSettings.availableFinish;
      let pageNumber = 0;
      let filterFacets = [];
      let filterFeatures = [];
      let filterFlowRates = [];
      let filterShowerSys = [];
      let filterShowerSysVal = '';
      let filterSprayPatterns = [];
      let appliedFinish = [];
      var activeTab = [];
      let narrowResult = [];
      let appliedFeature = [];
      let appliedShowerSystem = [];
      let appliedFlowRate = [];
      let loadProducts = true;
      let lastPage = false;
      let appliedSort = '';
      let loader = $('.ajax-progress-throbber');
      let defaultPaginationOpttion = parseInt(drupalSettings.defaultPaginationOption);
      let cdpResults = $('.cdp-result');
      let filtersChked = '';
      let getChkedChip = [];
      let cdpFilterID = [];
      let appliedCat = [];
      pageNavigated();
      var getWinLocHref = window.location.href;
      if(window.location.search.substring(1) === ""){
        var getWinLocHref = getWinLocHref + '?searchFilter=true';
      }
      let pathwithQuery = getWinLocHref;
      let pathwithQueryCat = 'cat-' + getWinLocHref;
      let pathwithQueryNarrow = 'nar-'+ getWinLocHref;
      let narrowResultBackClick = [];

      //cleanup local localStorage when no query params in CLP.
      if(window.location.search.substring(1) === ""){
        localStorage.removeItem(pathwithQuery);
        localStorage.removeItem(pathwithQueryCat);
        localStorage.removeItem(pathwithQueryNarrow);
      }
      //cleanup local localStorage when no query params in CLP.
      $('.clear-filter').hide();
      var noResultsMessage = '<div class ="error-message" role="contentinfo" aria-label="Error message">Please refine your search for results</div>';
      let size = $('.form-item__select--im').val();
      if (size == 'items-per-page') {
        size = defaultPaginationOpttion;
      }
      //Facet View
      let facetView = Backbone.View.extend({
        el: "#facets",
        template: _.template($("#finish-facets").html()),
        initialize: function (initData, append) {
          let facetBlock = getFinishFacets(initData);
          this.render(facetBlock, append);
        },
        events: {
          'change #finishFilter': 'applyFacets',
        },
        applyFacets: function (e) {
          loadProducts = true;

        },
        render: function (facetBlock, append) {
          $("#finish-facets").addClass('cdp-finish');
          if (facetBlock != '') {
            $('#finish-filters').html(facetBlock);
          }
          else {
            // TBD.
            var noResultsMessage = '<div class ="error-message" role="contentinfo" aria-label="Error message">Please refine your search for results</div>';
            $('#facets').html(noResultsMessage);
          }
          return this;
        }
      });

      // Renders product data in template using backbone JS.
      let ProdView = Backbone.View.extend({
        el: "#output",
        template: _.template($("#output-node").html()),
        initialize: function (initData, append) {
          this.render(initData, append);
        },
        render: function (initData, append) {
          let _self = this;
          let _render = "";

          //$("#output").addClass('grid');
          var collection_position = 0;
          _.each(initData.content, function (entry) {
            let collection_product = new Object();
            let _availableToOrderDate = new Date(entry.values.AvailableToOrderDate).getTime();
            let _availableToShipDate = new Date(entry.values.AvailableToShipDate).getTime();
            let _currentDate = new Date().getTime();
            collection_position = collection_position + 1;
            collection_product.list = "Collection Results";
            if(entry.collections[0] != undefined) {
              collection_product.brand = entry.collections[0];
            }
            else {
              collection_product.brand = "";
            }
            collection_product.name = entry.description;
            collection_product.position = collection_position;
            collection_product.id = entry.name;
            collection_product.variant = entry.values.Finish;

            gtm_collection.push(collection_product);
            gtm_collection_click[entry.name] = collection_product;

            let _finishValue = entry.facetFinish;
            let _price = "Price";
            if (entry.values !== null) {
              _price = entry.values.ListPrice;
            }
            let _heroImage = entry.heroImageSmall;
            let _title = "Title";
            if (entry.description != '') {
              _title = upperCamelCase(entry.description).replace('&Reg;','®').replace('&Trade;','™');
            }

            var _handle = new Array();
            var _handleArr = new Array();
            let _sku = entry.name;
            let skuSplit = '';
            if(entry.configOptions.length > 0){
              let skuSplit = _sku.split('--');
              _.each(entry.configOptions, function (configOptions) {
                  if(configOptions.optionLabel == "Handle_or_Accent"
                  || configOptions.optionLabel == 'Rough_Valve'){
                    _.each(configOptions.optionProducts, function (optionProduct) {
                      if($.inArray(optionProduct.modelNumber, skuSplit) !== -1 &&
                        $.inArray(optionProduct.modelNumber, _handleArr) == -1) {
                          _handleArr.push(optionProduct.modelNumber);
                          let _prdName = upperCamelCase(optionProduct.description);
                          _prdName = _prdName.replace(/((?!<sup>\s*))®((?!\s*<\/sup>))/gi, '<sup>&reg;</sup>') ;
                          _handle.push({sku: optionProduct.modelNumber, name: _prdName});
                      }
                    });
                  }
                });
            }

            let _collection = "COLLECTION NAME";
            if (entry.values !== null) {
              _collection = (entry.values.Collection == '') ? entry.values.Collection:entry.values.defaultCollection;
            }

            let _ribbonText = '';
            let _ribbonColor = '';
            if(entry.values.discontinued) {
              _ribbonText = (entry.values.discontinueText)? entry.values.discontinueText : "Discontinued";
              _ribbonColor = "gray-30";
            }
            else {
              if (_currentDate < _availableToShipDate) {
                if (_currentDate > _availableToOrderDate) {
                  _ribbonText = 'Coming Soon';
                  _ribbonColor = "brizo-teal";
                }
              }
              else if(entry.values.FltWebExclusiveCustomerItem == 'T' && entry.values.WebExclusiveCustomerItem == 'Ferguson') {
                _ribbonText = 'Ferguson';
                _ribbonColor = "brizo-teal";
              }
            }
            let _pdp = '/product-detail/' + entry.name;
            _render += _self.template({
              title: _title,
              collectionName: _collection,
              heroImage: _heroImage,
              sku: _sku,
              price: _price,
              pdp: _pdp,
              ribbonText: _ribbonText,
              ribbonColor: _ribbonColor,
              handle:_handle
            });
          });

          if (append == true) {
            _self.$el.html(_render);
          }
          if (append == false) {
            _self.$el.append(_render);
          }

          // Product Click GA Event
          $("a.product-action-link",context).on("click", function (event) {
            event.stopPropagation();
            event.preventDefault();
            var currentUrl = $(this).attr("href");
            var spitedUrl = currentUrl.split("/");
            if(gtm_collection_click[spitedUrl[2]] != undefined) {
              delete gtm_collection_click[spitedUrl[2]]["list"];
              dataLayer.push({
                'event':'productClick',
                'ecommerce': {
                  'click': {
                    'actionField': {'list': 'Collection Results'},
                    'products': gtm_collection_click[spitedUrl[2]]
                  }
                },
                'eventCallback': function() {
                  document.location = currentUrl;
                }
              });
            }
            document.location = currentUrl;
          });

          return this;
        },
      });
      $('.collection-details__dropdowns').once().on('change', '.form-item__select--im', function (){
        size = $('.form-item__select--im').val();
        if (size == 'items-per-page') {
          size = defaultPaginationOpttion;
        }
        pageNumber = 0;
        applyFacets();
      });
      //retain filters selections when user coming back from pdp
      if (window.location.search == '?searchFilter=true') {
        if (context == document) {
          if (localStorage.getItem(pathwithQueryNarrow) || localStorage.getItem(pathwithQuery) || localStorage.getItem(pathwithQueryCat)) {
            let narrowResultBackClick = JSON.parse(localStorage.getItem(pathwithQueryNarrow));
            if(narrowResultBackClick == 'null' || narrowResultBackClick == null){
              narrowResult = [];
            }else{
              narrowResult = narrowResultBackClick;
            }
            // Narrow Results Filters
            if($.isArray(narrowResult) && narrowResult.length > 0){
              $.each(narrowResult, function (index, chipEleID) {
                let filterLabel = $('label[for="' + chipEleID + '"]').text();
                $("[id='"+ chipEleID +"']").prop("checked",true);
                filterChips(filterLabel, chipEleID, 1);
              });
            }

            filterFacets = JSON.parse(localStorage.getItem(pathwithQuery));
            if(filterFacets == 'null' || filterFacets == null){
              filterFacets = [];
            }
            let activeTabBackClick = JSON.parse(localStorage.getItem(pathwithQueryCat));
            if(activeTabBackClick == 'null' || activeTabBackClick == null){
              activeTabBackClick = '';
            }

            if($.isArray(activeTabBackClick) && activeTabBackClick.length > 0){
              $.each(activeTabBackClick, function (index, chipEleID) {
                chipEleID = $("[value='"+chipEleID+"']").attr('id');
                $("[id='"+ chipEleID +"']").prop("checked",true);
                let filterLabel = $('label[for="' + chipEleID + '"]').text();
                categoryid = $("[id='"+ chipEleID +"']").attr('categoryid');
                filterChips(filterLabel, categoryid, 1);
              });
            }
            activeTab = activeTabBackClick;
            pageNumber = 0;
            let size = 24;
            applyFacetssearchFilter();
            appliedFinish = JSON.parse(localStorage.getItem(pathwithQuery));
            if(appliedFinish == null || appliedFinish == 'null'){
              appliedFinish = [];
            }
            if($.isArray(filterFacets) && filterFacets.length > 0){
              $.each(filterFacets, function (index, chipEleID) {
                $("[id='"+ chipEleID +"']").prop("checked",true);
                let filterLabel = $('label[for="' + chipEleID + '"]').text();
                filterChips(filterLabel, chipEleID, 1);
              });
            }
          }
        }
      }
      /** Method to retain product on backbutton **/
      function applyFacetssearchFilter(e) {
        loadProducts = true;
        loader.show();
        cdpResults.hide();
        $.ajax({
          type: "GET",
          url: "/collection-listing",
          dataType: 'json',
          data: {
            size: size,
            sub_category: activeTab,
            category: category,
            collection: collection,
            facets: filterFacets,
            features:filterFeatures,
            showersys:filterShowerSys,
            flowrates:filterFlowRates,
            narrow_result: narrowResult,
            sort: appliedSort,
            pagenumber: pageNumber,
            cache: 'F',
          },
          error: function (XMLHttpRequest, textStatus, errorThrown) {
            $('.cdp-pagination-output').html('');
            return false;
          },
          success: function (data) {
            pageNumber++;
            lastPage = data.last || '';
            if (data.content == '' || data.totalElements == 0){
              $('#output').html(noResultsMessage);
            }else{
              $('#output').html('');
              $('.form-item__select--im').hide();
              $('.cdp-pagination-output').hide();
              let response = new ProdView(data, true);
              if(data){
                data.currentpage = 1;
              }
              if(data.totalElements > 24){
                $('.form-item__select--im').show();
                $('.cdp-pagination-output').show();
              }
              let CdppaginationView = new cdppaginationView(data);
              let activePage = data.number +1;
              let from = ((activePage-1)*size)+1;
              let to = activePage*size;
              if (to > data.totalElements) {
                to = data.totalElements;
              }
              let resultString = "Results <span>"+from+"-"+to+"</span> of <span>"+data.totalElements + "</span>";
              $('.cdp-results-count').html(resultString);
              loadProducts = true;
            }
            cdpResults.show();
            loader.hide();
            if($('.js-filter-chip').length){
              $('.clear-filter').show();
            }else{
              $('.clear-filter').hide();
            }
            if(data.totalElements == 0) {
              $('.plp-pagination-output').html('');
              cdpResults.hide();
            }
          }
        });
      }
      /** End of retain product on backbutton method**/


      //retain filters selections when user coming back from pdp

      function removeQueryParam(){
        var uri = window.location.toString();
      	if (uri.indexOf("?") > 0) {
      	    var clean_uri = uri.substring(0, uri.indexOf("?"));
      	    window.history.replaceState({}, document.title, clean_uri);
      	}
      }

      /* Detects navigation back from any page. */
      function pageNavigated(){
        if (window.performance &&
        window.performance.navigation.type == 2) {
          window.history.replaceState(null, null, "?searchFilter=true");
        }
      }
      function applyFacets(e) {
        loadProducts = true;
        loader.show();
        cdpResults.hide();
        if(activeTab.length >= 1) {
          sub_cat = activeTab
        } else {
          sub_cat = drupalSettings.productCategory
        }
        $.ajax({
          type: "GET",
          url: "/collection-listing",
          dataType: 'json',
          data: {
            size: size,
            sub_category: sub_cat,
            category: category,
            collection: collection,
            facets: filterFacets,
            features: filterFeatures,
            showersys:filterShowerSys,
            flowrates:filterFlowRates,
            narrow_result: narrowResult,
            sort: appliedSort,
            pagenumber: pageNumber,
            cache: 'F',
          },
          error: function (XMLHttpRequest, textStatus, errorThrown) {
            $('.cdp-pagination-output').html('');
            return false;
          },
          success: function (data) {
            pageNumber++;
            lastPage = data.last;
            if (data.content == '' || data.totalElements == 0){
              $('#output').html(noResultsMessage);
            }else{
              $('#output').html('');
              $('.form-item__select--im').hide();
              $('.cdp-pagination-output').hide();
              let response = new ProdView(data, true);
              if(data){
                data.currentpage = 1;
              }
              if(data.totalElements > 24){
                $('.form-item__select--im').show();
                $('.cdp-pagination-output').show();
              }
              let CdppaginationView = new cdppaginationView(data);
              CdppaginationView.undelegateEvents();
              let activePage = data.number +1;
              let from = ((activePage-1)*size)+1;
              let to = activePage*size;
              if (to > data.totalElements) {
                to = data.totalElements;
              }
              let resultString = "Results <span>"+from+"-"+to+"</span> of <span>"+data.totalElements + "</span>";
              $('.cdp-results-count').html(resultString);
              loadProducts = true;
            }
            cdpResults.show();
            loader.hide();
            if($('.js-filter-chip').length){
              $('.clear-filter').show();
            }else{
              $('.clear-filter').hide();
            }
            if(data.totalElements == 0) {
              $('.plp-pagination-output').html('');
              cdpResults.hide();
            }
          }
        });
      }
      //Facet - Category filters
      $('.cdp-data').once().on('click', '.js-filter-chip', function () {
        let filterParam = '';
        let chipId = $(this).attr('id');
        let facetId = chipId.split(/-(.+)?/)[1];
        let facetValue = '';
        $("a[id='" + chipId + "']").remove();
        var chkThisIsCat = facetId.substring(0,9);
        if(chkThisIsCat == 'category~'){
          facetId = $("[categoryid='"+ facetId +"']").attr('id');
        }
        $("input[id='" + facetId + "']").prop("checked", false);
        let str = $("input[id='" + facetId + "']").attr("value");
	if(str.indexOf("&&") != -1){
	  shower_pack_cat = str.split(/&&/);
	  for (var i = 0, length = shower_pack_cat.length; i < length; i++) {
	    let index = activeTab.indexOf(shower_pack_cat[i]);
	    if (index > -1) {
	      activeTab.splice(index, 1);
	    }
	  }
	} else {
	 let index = activeTab.indexOf(str);
         if (index > -1) {
           activeTab.splice(index, 1);
         }
	}        
        let filterId = facetId;
        let index1 = appliedFinish.indexOf(filterId);
        if (index1 > -1) {
          appliedFinish.splice(index1, 1);
        }


        let index2 = narrowResult.indexOf(filterId);
        if (index2 > -1) {
          narrowResult.splice(index2, 1);
        }
        $("a[id='chip-" + filterId + "']").remove();
        pageNumber=0;
        applyFacets();
      });

      $(".clear-filter").once().on("click",function () {
        //Start - Remove saved filter selections on the local storage
        localStorage.removeItem(pathwithQuery);
        localStorage.removeItem(pathwithQueryCat);
        localStorage.removeItem(pathwithQueryNarrow);
        if (window.location.href.indexOf('?') > -1) {
          history.pushState('', document.title, window.location.pathname);
        }
       //Start - Remove saved filter selections on the local storage
        $(this).hide();
        $('.js-filter-chip').remove();
        $('.custom-input__native').prop("checked", false);
        pageNumber = 0;
        activeTab = [];
        appliedFinish = [];
        filterFacets = [];
        filterFeatures = [];
        filterShowerSys = [];
        filterFlowRates = [];
        narrowResult =  [];
        pageNumber = 0;
        applyFacets();
      });
      $('.cdp-block', context).once('getProducts').each(function () {
        if (drupalSettings.termfound != 'false'){
          var checkLSDataExistFlag = 'F';
          if (localStorage.getItem(pathwithQueryNarrow) || localStorage.getItem(pathwithQuery) || localStorage.getItem(pathwithQueryCat)) {
            var checkLSDataExistFlag = 'T';
          }
          if (window.location.search !== '?searchFilter=true' || checkLSDataExistFlag == 'F') {
            pageNumber = 0;
            if(prod_category.length >= 1) {
              apiCall(prod_category);
            } else {
              let activeTab =  '' ;
              apiCall(activeTab);
            }

          }
        }
      });
      $(".catFilter").once().on("change",function () {
        pageNumber = 0;
        let filterValue = '';
        let filterChecked = $(this).prop('checked');
        let filterId = $(this).attr('categoryId');
        let elementId = $(this).attr('id');
        let str = jQuery(this).attr('value');
        let filterLabel = $('label[for="' + $(this).attr('id') + '"]').text();
        if (filterChecked == true) {
          filterFacets += filterValue;
          filterChips(filterLabel, filterId, 1);

          if (str) {
            if(str.indexOf("&&") != -1){
              shower_pack_cat = str.split(/&&/);
              for (var i = 0, length = shower_pack_cat.length; i < length; i++) {
                activeTab.push(shower_pack_cat[i]);
              }
            } else {
              let index = activeTab.indexOf(str);
              if (index = -1) {
                activeTab.push(str);
              }
            }
          }
        }else{
          if(str.indexOf("&&") != -1){
            shower_pack_cat = str.split(/&&/);
            for (var i = 0, length = shower_pack_cat.length; i < length; i++) {
              let index = activeTab.indexOf(shower_pack_cat[i]);
              if (index > -1) {
                activeTab.splice(index, 1);
              }
              $("a[id='chip-" + filterId + "']").remove();
              }
          } else {
            let index = activeTab.indexOf(str);
            if (index > -1) {
              activeTab.splice(index, 1);
            }
            $("a[id='chip-" + filterId + "']").remove();
          }
        }
        localStorage.setItem(pathwithQueryCat,JSON.stringify(activeTab));
        facetapiCall(activeTab);
      });
      $(".typeFilter").once().on("change",function () {
        pageNumber = 0;
        let filterValue = '';
        let filterChecked = $(this).prop('checked');
        let filterId = $(this).attr('categoryId');
        let elementId = $(this).attr('id');
        let str = jQuery(this).attr('value');
        let filterLabel = $('label[for="' + $(this).attr('id') + '"]').text();
        if (filterChecked == true) {
          filterFacets += filterValue;
          filterChips(filterLabel, filterId, 1);
          if (str) {
            let index = activeTab.indexOf(str);
            if (index == -1) {
              activeTab.push(str);
            } 
          }
        }else{
          let index = activeTab.indexOf(str);
          if (index > -1) {
            activeTab.splice(index, 1);
          }
          $("a[id='chip-" + filterId + "']").remove();
        }
        localStorage.setItem(pathwithQueryCat,JSON.stringify(activeTab));
        facetapiCall(activeTab);
      });


       // On selection of finish facet corresponding value will be sent to API.
       $(".finishFilter").once().on("change",function () {
        let filterChecked = $(this).prop('checked');
        let filterId = $(this).attr('id');
        if(filterChecked == true){
          if(Array.isArray(appliedFinish) && appliedFinish.length){
            $.extend(filterId,appliedFinish);
          }
          getChkedChip.push(filterId);
        }
        let filterLabel = $('label[for="' + $(this).attr('id') + '"]').text();
        if (filterChecked == true) {
          filterChips(filterLabel, filterId, 1);
          if (jQuery(this).attr('id')) {
            appliedFinish.push(filterId);
          }
        }else{
          let index = appliedFinish.indexOf(filterId);
          if (index > -1) {
            appliedFinish.splice(index, 1);
          }
          $("a[id='chip-" + filterId + "']").remove();
        }
        filterFacets = appliedFinish;
        localStorage.setItem(pathwithQuery,JSON.stringify(filterFacets));
        pageNumber = 0;
        applyFacets();
      });

      // On selection of flowrate facet corresponding value will be sent to API.
      $(".flowRateFilter").once().on("change",function () {
        let filterChecked = $(this).prop('checked');
        let filterId = $(this).attr('id');
        let filterLabel = $(this).val();
        if(filterChecked == true){
          if(Array.isArray(appliedFlowRate) && appliedFlowRate.length){
            $.extend(filterId,appliedFlowRate);
          }
          getChkedChip.push(filterLabel);
        }
        if (filterChecked == true) {
          filterChips(filterLabel, filterId, 1);
          if (jQuery(this).attr('id')) {
            appliedFlowRate.push(filterLabel);
          }
        }else{
          let index = appliedFlowRate.indexOf(filterLabel);
          if (index > -1) {
            appliedFlowRate.splice(index, 1);
          }
          $("a[id='chip-" + filterLabel + "']").remove();
        }
        filterFlowRates = appliedFlowRate;
        localStorage.setItem(pathwithQuery,JSON.stringify(filterFlowRates));
        pageNumber = 0;
        applyFacets();
      });
      
      // On selection of feature facet corresponding value will be sent to API.
      $(".featureFilter").once().on("change",function () {
      let filterChecked = $(this).prop('checked');
      let filterId = $(this).attr('id');
      let filterLabel = $(this).val();
      if(filterChecked == true){
        if(Array.isArray(appliedFeature) && appliedFeature.length){
          $.extend(filterId,appliedFeature);
        }
        getChkedChip.push(filterLabel);
      }
      if (filterChecked == true) {
        filterChips(filterLabel, filterId, 1);
        if (jQuery(this).attr('id')) {
          appliedFeature.push(filterLabel);
        }
      }else{
        let index = appliedFeature.indexOf(filterLabel);
        if (index > -1) {
          appliedFeature.splice(index, 1);
        }
        $("a[id='chip-" + filterLabel + "']").remove();
      }
      filterFeatures = appliedFeature;
      localStorage.setItem(pathwithQuery,JSON.stringify(filterFeatures));
      pageNumber = 0;
      applyFacets();
    });
    // On selection of Shower System facet corresponding value will be sent to API.
    $(".showersysFilter").once().on("change",function () {
      let filterChecked = $(this).prop('checked');
      let filterId = $(this).attr('id');
      let filterLabel = $(this).attr('data-filter')+"_"+$(this).val();
      let sysLabel = $(this).next('label').text();
      if(filterChecked == true){
        if(Array.isArray(appliedShowerSystem) && appliedShowerSystem.length){
          $.extend(filterId,appliedShowerSystem);
        }
        getChkedChip.push(sysLabel);
      }
      if (filterChecked == true) {
        filterChips(sysLabel, filterId, 1);
        if (jQuery(this).attr('id')) {
          appliedShowerSystem.push(filterLabel);
        }
      } else{
        let index = appliedShowerSystem.indexOf(filterLabel);
        if (index > -1) {
          appliedShowerSystem.splice(index, 1);
        }
        $("a[id='chip-" + sysLabel + "']").remove();
      }
      filterShowerSys = appliedShowerSystem;
      localStorage.setItem(pathwithQuery,JSON.stringify(filterShowerSys));
      pageNumber = 0;
      applyFacets();
    });



      /* Creating the filter chips after filters selected. */
      function filterChips(displayName, elemId, action){
        if(action == 0){
          $("a[id='chip-" + elemId + "']").remove();
        }else{
          let chip = '<a class="cdp-filter-chip js-filter-chip" ' +
            'id="chip-'+ elemId +'" title ="'+ displayName +'">\n' +
            displayName + '<i class="icon-filter-hide-small"></i></a>';
          $('.cdp-selected-filter').prepend(chip);
        }
      }
      /** Start of method which will be called when filters are selected **/
      function facetapiCall(activeTab){
        loadProducts = false;
        loader.show();
        cdpResults.hide();
        var callCategory = [];
        if(narrowResult.length == 0) {
          narrow = '';
        } else {
          narrow = narrowResult;
        }
        if(appliedFinish == null){
          appliedFinish = [];
        }
        if(activeTab.length == 0) {
          callCategory = prod_category;
        } else {
        	callCategory = activeTab;
        }
        /* API call to get the products */
        $.ajax({
          type: "GET",
          url: "/collection-listing",
          dataType: 'json',
          data: {
            size: size,
            sub_category: callCategory,
            category: category,
            facets: appliedFinish,
            narrow_result: narrow,
            collection: collection,
            pagenumber: pageNumber,
            cache: 'F',
          },
          error: function (XMLHttpRequest, textStatus, errorThrown) {
            $('.cdp-pagination-output').html('');
            return false;
          },
          success: function (data) {
            if (data.content != ''){
              $('#output').html('');
              let response = new ProdView(data, true);
              $('.form-item__select--im').hide();
              $('.cdp-pagination-output').hide();
              if(data){
                data.currentpage = 1;
                if(data.totalElements > 24){
                  $('.form-item__select--im').show();
                  $('.cdp-pagination-output').show();
                }
              }
              let CdppaginationView = new cdppaginationView(data);
              CdppaginationView.undelegateEvents();
              let activePage = data.number +1;
              let from = ((activePage-1)*size)+1;
              let to = activePage*size;
              if (to > data.totalElements) {
                to = data.totalElements;
              }
              let resultString = "Results  <span>"+from+"-"+to+"</span> of <span>"+data.totalElements + "</span>";
              $('.cdp-results-count').html(resultString);
              $('.loader').hide();
              loader.hide();
              cdpResults.show();
            }
            else{
              $('#output').html(noResultsMessage);
              $('.loader').hide();
              loader.hide();
            }
            if($('.js-filter-chip').length){
              $('.clear-filter').show();
            }else{
              $('.clear-filter').hide();
            }
            lastPage = data.last;
            pageNumber++;
            loadProducts = true;
            $('.loader').hide();
            loader.hide();
            if(data.totalElements == 0) {
              $('.plp-pagination-output').html('');
              cdpResults.hide();
            }
          }
        });
      }
      /** End of facet selection method **/
      function  apiCall(activeTab){
        loadProducts = false;
        loader.show();
        cdpResults.hide();
        if(narrowResult.length == 0) {
          narrow = '';
        } else {
          narrow = narrowResult;
        }
        if(appliedFinish == null){
          appliedFinish = [];
        }
        /* API call to get the products */
        $.ajax({
          type: "GET",
          url: "/collection-listing",
          dataType: 'json',
          data: {
            size: size,
            sub_category: activeTab,
            category: category,
            facets: appliedFinish,
            narrow_result: narrow,
            collection: collection,
            pagenumber: pageNumber,
            cache: 'F',
          },
          error: function (XMLHttpRequest, textStatus, errorThrown) {
            $('.cdp-pagination-output').html('');
            return false;
          },
          success: function (data) {
            if (data.content != ''){
              $('#output').html('');
              let response = new ProdView(data, true);
              $('.form-item__select--im').hide();
              $('.cdp-pagination-output').hide();
              if(data){
                data.currentpage = 1;
                if(data.totalElements > 24){
                  $('.form-item__select--im').show();
                  $('.cdp-pagination-output').show();
                }
              }
              let CdppaginationView = new cdppaginationView(data);
              let activePage = data.number +1;
              let from = ((activePage-1)*size)+1;
              let to = activePage*size;
              if (to > data.totalElements) {
                to = data.totalElements;
              }
              let resultString = "Results  <span>"+from+"-"+to+"</span> of <span>"+data.totalElements + "</span>";
              $('.cdp-results-count').html(resultString);
              $('.loader').hide();
              loader.hide();
              cdpResults.show();
            }
            else{
              $('#output').html(noResultsMessage);
              $('.loader').hide();
              loader.hide();
            }
            if($('.js-filter-chip').length){
              $('.clear-filter').show();
            }else{
              $('.clear-filter').hide();
            }
            lastPage = data.last;
            pageNumber++;
            loadProducts = true;
            $('.loader').hide();
            loader.hide();
            if(data.totalElements == 0) {
              $('.plp-pagination-output').html('');
              cdpResults.hide();
            }
          }
        });
        /* API call to get the finish facets */
        /*$.ajax({
          type: "GET",
          url: "/collection-finish-facets",
          dataType: 'json',
          data: {
            size: size,
            sub_category: activeTab,
            category: category,
            collection: collection,
            pagenumber: pageNumber,
            cache: 'T',
          },
          success: function (data) {
            //let FacetView = new facetView(data);
          }
        });*/
      }


      function getFinishFacets(initData){
        let facetCount = 0;
        let output = "";
        _.each(initData.facets[0].terms, function (termData) {
          let label = icon = img ='';

          if($.inArray(termData.term,Object.values(availableFinish)) !== -1){
            label = finishData[termData.term]['label'];
            icon = finishData[termData.term]['icon'];
            if(icon.length)
              img = '<div class="filter-icon">' +
                 '<img src="'+ icon +'">' +
              '</div>';
            if(label == ''){
              label = termData.term;
            }

            output += '<div class="cdp-filter-block">' +
              '<div class="custom-input custom-input--checkbox">' +
              '<input class="custom-input__native finishFilter" data-filter="facetFinish" type="checkbox" id="'+ termData.term+'">' +
              '<label class="custom-input__label" for="'+ termData.term +'">'+ label +
              '</label></div>'+ img + '</div>';
            facetCount++;
          }

        });
        return output;
      }
      $(".pagination-bottom").once().on("click", "input[type='button']", function (){
        $('html, body').animate({
          'scrollTop' : $("#output").position().top
        });
      });
      // On Scroll to achieve lazyload functionality.
        function applyproductPagination(params) {
          loadProducts = false;
          loader.show();
          cdpResults.hide();
          let page;
          if (params['action']) {
            if (params['action'] == 'prev') {
              if (params['pageNum'] == 2) {
                page = 0;
              }
              else {
                page = params['pageNum'] - 2;
              }
            }
            else {
              page = params['pageNum'];
            }
          }
          else {
            page = params['pageNum'] - 1;
          }

          if(prod_category.length >= 1) {
            activeTab = prod_category;
          }

          $.ajax({
            type: "GET",
            url: "/collection-listing",
            dataType: 'json',
            data: {
              size: size,
              sub_category: activeTab,
              category: category,
              collection: collection,
              facets: appliedFinish,
              narrow_result: narrowResult,
              sort: appliedSort,
              pagenumber: page,
              cache: 'F',
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
              $('.cdp-pagination-output').html('');
              return false;
            },
            success: function (data) {
              let response = new ProdView(data, true);
              if(params['pageNum'] && data) {
                data.currentpage = params['pageNum'];
                data.first = params['first'];
                data.last = params['last'];
              }
              if(params['action'] && data) {
                data.action = params['action'];
              }
              let CdppaginationView = new cdppaginationView(data);
              CdppaginationView.undelegateEvents();
              let activePage = data.number +1;
              let from = ((activePage-1)*size)+1;
              let to = activePage*size;
              if (to > data.totalElements) {
                to = data.totalElements;
              }
              let resultString = "Results <span>"+from+"-"+to+"</span> of <span>"+data.totalElements + "</span>";
              $('.cdp-results-count').html(resultString);
              loadProducts = true;
              lastPage = data.last;
              pageNumber++;
              loadProducts = true;
              lastPage = data.last;
              $('.loader').hide();
              loader.hide();
              cdpResults.show();
              if(data.totalElements == 0) {
                $('.plp-pagination-output').html('');
                cdpResults.hide();
              }
            }
          });
        }

      function findKey(input, target){
        var found;
        for (var prop in input) {
          if(prop == target){
            found = input[prop];
          }
        };
        return found;
      };

     
      
       // On selection of narrow facet corresponding value will be sent to API.
       $('.cdp-filter-block').once().on('change', '.facetNarrowResults', function (e) {
        let filterChecked = $(this).prop('checked');
        let filterId = $(this).attr('id');
        let filterLabel = $('label[for="' + $(this).attr('id') + '"]').text();
        if (filterChecked == true) {
          filterChips(filterLabel, filterId, 1);
          if (jQuery(this).attr('id')) {
            narrowResult.push(filterId);
          }
        }else{
          let index = narrowResult.indexOf(filterId);
          if (index > -1) {
            narrowResult.splice(index, 1);
          }
          $("a[id='chip-" + filterId + "']").remove();
        }
        pageNumber = 0;
        localStorage.setItem(pathwithQueryNarrow,JSON.stringify(narrowResult));
        applyFacets();
      });

      /* Convert to camel case .*/
      function upperCamelCase(text) {
        return text.replace(/\w+/g,
          function(w){
            return w[0].toUpperCase() + w.slice(1).toLowerCase();
          });
      }

      /* pagination starts here*/
      var cdppaginationView = Backbone.View.extend({
        el: ".cdp-pagination-output",
        template: _.template($("#cdp-pagination").html()),
        initialize: function (initData) {
          this.render(initData);
        },
        events: {
          'click [type="button"]': 'cdppagination'
        },
        cdppagination: function (e) {
          var params = [];
          params['pageNum'] = $(e.currentTarget).val();
          params['first'] = $(e.currentTarget).attr('first');
          params['last'] = $(e.currentTarget).attr('last');
          if($(e.currentTarget).attr("data-val") === 'next') {
            params['pageNum'] = $('.pager__link.is-active').val();
            params['action'] = 'next';
          }
          if($(e.currentTarget).attr("data-val") === 'prev') {
            params['pageNum'] = $('.pager__link.is-active').val();
            params['action'] = 'prev';
          }
          if($(e.currentTarget).attr('data') === 'last-page') {
            var total = $(e.currentTarget).attr('total');
            params['pageNum'] = parseInt(total);
            params['first'] = parseInt(total)-2;
            params['last'] = parseInt(total);
          }
          if($(e.currentTarget).attr('data') === 'first-page') {
            params['pageNum'] = 1;
            params['first'] = 1;
            params['last'] = 3;
          }
          applyproductPagination(params);        },
        render: function (initData) {
          if (initData === null || typeof initData == 'undefined') {
            return false;
          }
          var cont_disp_cnt = size;//drupalSettings.cont_disp_cnt;
          if (cont_disp_cnt == null) {
            cont_disp_cnt = defaultPaginationOpttion;
          }
          var _self = this;
          var _render = "<div class='page-desktop-pagination pagination'>";
          var _mobilerender = "<div class='page-mobile-pagination pagination'>";
          var _nextPage = (initData === null) ? 0 : parseInt(initData.number) + 1;
          var _currentpage = initData.currentpage;  // Current page
          var _totalmatches = initData.totalElements;
          var _totalpages = Math.ceil(_totalmatches / cont_disp_cnt);
          var _nextPage;
          var _first;
          if (_currentpage < _totalpages) {
            _nextPage = _currentpage + 1;
          } else {
            _nextPage = _currentpage;
          }
          var _previousPage;
          if (_currentpage === 1) {
            _previousPage = 1;
            _first = 1;
          } else {
            _previousPage = _currentpage - 1;
            _first = initData.first;
          }
          if(_currentpage>=initData.first && _currentpage<=initData.last) {
            _first = parseInt(initData.first);
          }
          if(initData.action && initData.action == 'next') {
            if(_currentpage>=initData.first && _currentpage<initData.last) {
              _first = parseInt(initData.first);
            } else {
              _first = parseInt(initData.first) + 1;
            }
            _currentpage = parseInt(_currentpage) + 1;
          }
          if(initData.action && initData.action == 'prev') {
            if(_currentpage>initData.first && _currentpage<=initData.last) {
              _first = parseInt(initData.first);
            } else {
              _first = parseInt(initData.first) - 1;
            }
            _currentpage = parseInt(_currentpage) - 1;
          }
          var pagination = parseInt(_first)+2;
          if(_totalpages < pagination) {
            pagination = _totalpages;
          }
          if(_currentpage != 1) {
            //_render += '<input total="'+_totalpages+'" class="pager__link first" type="button" data="" value="<<">';
            _render += '<input first="'+_first+'" last="'+pagination+'" class="pager__link prev" type="button" data="" data-val="prev" value="<">';
            _mobilerender += '<input first="'+_first+'" last="'+pagination+'" class="pager__link prev" type="button" data="" data-val="prev" value="<">';
          }
          if((_first+2 >= _totalpages) && (_totalpages > 3)) {
            _render += '<input total="'+_totalpages+'" class="pager__link first" type="button" data="first-page" value=1><span class="pagination-dots"> ... </span>';
          }
          for(var i=_first;i<=pagination;i++) {
            /*_render += _self.template({
              pageNumber: i,
              currentPage: _currentpage,
              totalRecordset: initData.totalElements
            });*/
            var classValue = '';
            if(i === parseInt(_currentpage)) {
              classValue = 'is-active';
              _mobilerender += '<p class="mobile_pagination"><span>Page </span><span class="totalpages">' + i + '</span></p>';
            }

            _render += '<input first="'+_first+'" last="'+pagination+'" class="pager__link '+ classValue +'" type="button" value="'+ i +'">';

          }
          if(!_first && _totalpages == 1) {
            _mobilerender += '<p class="mobile_pagination"><span>Page </span><span class="totalpages">' + _totalpages +'</span></p>';

          }
          _mobilerender += '<p class="mobile_pagination"><span>of </span><span class="totalpages">' + _totalpages +'</span></p>';
          if(_first+2 < _totalpages) {
            _render += '<span class="pagination-dots"> ... </span><input total="'+_totalpages+'" class="pager__link last" type="button" data="last-page" value='+_totalpages+'>';
          }
          if(_currentpage != _totalpages) {
            _render += '<input first="'+_first+'" last="'+pagination+'" class="pager__link next" type="button" data-val="next" data="" value=">">';
            _mobilerender += '<input first="'+_first+'" last="'+pagination+'" class="pager__link next" type="button" data-val="next" data="" value=">">';
            //_render += '<input total="'+_totalpages+'" class="pager__link last" type="button" data="" value=">>">';
          }
          var start_value = (_currentpage-1)*cont_disp_cnt;
          if(_currentpage ==1) {
            start_value = 1;
          }
          var endvalue = _currentpage*cont_disp_cnt;
          if(start_value == 1 && endvalue > initData.totalElements){
            // no pager
          }
          else if(endvalue > initData.totalElements){
            endvalue = initData.totalElements;
            $('.srp-result-doc').html('<span class="srp-qty">Results ' + start_value + '-' + endvalue  + ' of ' + initData.totalElements +'</span>').attr('data-content-cnt', initData.totalElements);
          }
          else{
            $('.srp-result-doc').html('<span class="srp-qty">Results ' + start_value + '-' + endvalue  + ' of ' + initData.totalElements +'</span>').attr('data-content-cnt', initData.totalElements);
          }
          _render += "</div>"
          _mobilerender += "</div>";
          _render = _render + _mobilerender;
          _self.$el.html(_render);

        }
      });
      /*End of pagination*/
    }
  };
})

(jQuery, Drupal, drupalSettings, Backbone);

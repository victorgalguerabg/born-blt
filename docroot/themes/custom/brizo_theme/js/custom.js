var captchaFlag = 0;
var formFlag = 0;

//Captcha Success Validation.
function enableBtn() {
  if(formFlag === 1) {
    jQuery('.webform-submission-consumer-registration-form .webform-button--submit').prop('disabled', false);
  } else {
    jQuery('.webform-submission-consumer-registration-form .webform-button--submit').prop('disabled', true);
  }
  captchaFlag = 1;
 //  if(jQuery('.webform-submission-product-registration-form .required').length === jQuery('.valid').length) {
 //   jQuery('.webform-submission-product-registration-form .webform-button--submit').prop('disabled', false);
 // }else {
 //   jQuery('.webform-submission-product-registration-form .webform-button--submit').prop('disabled', true);
 // }
return new Promise(function(resolve, reject) {
  document.getElementsByName("recaptcha")[0].value = "1";
 // jQuery('.webform-submission-contact-us-form input[name="recaptcha"]')[0].value = "1";
 // jQuery('.webform-submission-product-registration-form input[name="hdn_captcha"]')[0].value = "1";
 if(jQuery('.webform-submission-contact-us-form .captcha-validate,.webform-submission-product-registration-form .captcha-validate').next().find('p.form-item--error-message')) {
   jQuery('.webform-submission-contact-us-form .captcha-validate,.webform-submission-product-registration-form .captcha-validate').parent().removeClass('form-item--error');
   jQuery('.webform-submission-contact-us-form .captcha-validate,.webform-submission-product-registration-form .captcha-validate').next('p.form-item--error-message').remove();
 }
 });
}

//capptcha expired validation.
function captchaExpired() {
  captchaFlag = 0;
  jQuery('.webform-submission-contact-us-form input[name="recaptcha"]')[0].value = "0";
  if(jQuery('.webform-submission-contact-us-form input[name="recaptcha"]').val() == 0) {
    grecaptcha.reset();
  }
  // jQuery('.webform-submission-consumer-registration-form .webform-button--submit').prop('disabled', true);
  // jQuery('.webform-submission-product-registration-form .webform-button--submit').prop('disabled', true);
  //jQuery('.webform-submission-contact-us-form .webform-button--submit').prop('disabled', true);
  jQuery('.webform-submission-product-registration-form input[name = "recaptcha"]').val("0");
  if(jQuery('.webform-submission-product-registration-form input[name = "recaptcha"]').val() == 0) {
    grecaptcha.reset();
  }
}

function EqualHeight() {
jQuery(".bath-collection .grid li .product-tile-product-tile--collection, .kitchen-collection .grid li .product-tile-product-tile--collection").each(function(){
  var maxHeight = Math.max.apply(null, jQuery(".bath-collection .grid li .product-tile-product-tile--collection, .kitchen-collection .grid li .product-tile-product-tile--collection").map(function ()
  {
      return jQuery(this).height();
  }).get());
  jQuery(".bath-collection .grid li .product-tile-product-tile--collection, .kitchen-collection .grid li .product-tile-product-tile--collection").css("min-height", maxHeight);
});
}

//function deviceclick(){
  //if($(window).width() < 1024){
      //$('a').on('click touchend', function(e) {
        //var el = $(this);
        //var link = el.attr('href');
        //window.location = link;
    //});
  //}
//}

(function ($, Drupal, drupalSettings) {

    document.createElement( "picture" );

  Drupal.behaviors.custom = {
    attach: function(context, settings) {

      if($(context).find('.banner-carousel').hasClass("single-image-slider")) {
        var total_elements = $(context).find('.banner_video').length + $(context).find('.banner_img').length;
        if(total_elements > 1){
          $(context).find('.banner__wrapper').each(function() {
            $(context).find('.banner-carousel').find(".banner_video").find("video").trigger('pause');
            $(context).find('.banner-carousel').find(".banner_video").find(".banner_video_controls").find('.pause_button').addClass( "disable" );
            $(context).find('.banner-carousel').find(".banner_video").find(".banner_video_controls").find('.play_button').removeClass( "disable" );
          });

          // On before slide change
          $(context).find('.banner-carousel').on('beforeChange', function(event, slick, currentSlide, nextSlide){
            $(this).find(".banner_video").find(".play-icon").show();
            $('.banner_video iframe').each(function(i,e){
              $(e).parent().prev().css({"display": "none"});
              let iframeSrc = $(e).attr('src');
              iframeSrc = iframeSrc.replace('&autoplay=1', '');
              $(e).attr('src',iframeSrc);
            });
            $(this).find(".banner_video").find("video").trigger('pause');
            $(this).find(".banner_video").find(".banner_video_controls").find('.pause_button').addClass( "disable" );
            $(this).find(".banner_video").find(".banner_video_controls").find('.play_button').removeClass( "disable" );
            var nextPanel = $('[data-slick-index=' + nextSlide + ']');
            if(typeof(nextPanel.find(".banner_video")) != "undefined" ){
              var checkVideo = nextPanel.find(".banner_video").find("video")[0];
              if(typeof(checkVideo) != "undefined" && checkVideo !== null) {
                if(typeof(checkVideo.autoplay) !== "undefined" && checkVideo.autoplay == true) {
                  nextPanel.find(".banner_video").find("video").trigger('play');
                  nextPanel.find(".banner_video").find(".banner_video_controls").find('.play_button').addClass( "disable" );
                  nextPanel.find(".banner_video").find(".banner_video_controls").find('.pause_button').removeClass( "disable" );
                }
              }
            }
          });

          $(context).find('.banner-carousel').on('init', function(event, slick){
            var thisPanel = $(context).find('.banner-carousel').find('[data-slick-index="0"]').find(".banner_video");
            if(typeof(thisPanel) != "undefined"  && thisPanel !== null){
              var checkVideo = thisPanel.find("video");
              if(typeof(checkVideo[0].autoplay) !== "undefined" && checkVideo[0].autoplay == true) {
                checkVideo.trigger('play');
                thisPanel.find(".banner_video_controls").find('.play_button').addClass( "disable" );
                thisPanel.find(".banner_video_controls").find('.pause_button').removeClass( "disable" );
              }
            }
          });
        }
      }

      if ($("body").hasClass('page-node-type-landing-page')){
        if($('.banner__wrapper').length){
          var myVideoWrapper = $('.banner__wrapper');
          if(myVideoWrapper.find("[id^='video-embed-container-']").length){
            myVideoWrapper.find(".banner__content").addClass("content-div");
          }
        }
      }

      $(".webform-submission-product-registration-form .webform-multiple-table .required").on("change input keyup", function(e) {
        var element = $(this);
        var inputValue = element.val(),
            label = element.parent().find('label').text(),
            model_number = Drupal.behaviors.commonvalidationfunction.modelnumber();
        // setTimeout (function() {

        if(element.hasClass("validate-modelnumber")) {
          $('.webform-submission-product-registration-form').find(".validate-modelnumber").removeClass("form-item--error");
          var inputValue = element.val();
          if( inputValue == "" ) {
            element.next('p.form-item--error-message').remove();
            element.parent().next('p.form-item--error-message').remove();
            element.parent('.ac-model-number').after("<p class='form-item--error-message'> "+ label + " field is required.</p>");
            //element.after("<p class='form-error'> "+ label + " field is required.</p>");
            element.parent().addClass("form-item--error");

          } else if(!inputValue.match(model_number)) {
            element.parent().next('p.form-item--error-message').remove();
            element.parent('.ac-model-number').after("<p class='form-item--error-message'>Please enter a valid " + label + ".</p>");
            element.parent().addClass("form-item--error");
          }else {
            element.parent().next('p.form-item--error-message').remove();
          }
        };

        if((inputValue == "") || (inputValue == 0)) {
          if(element.next().find('p.form-item--error-message')) {
            element.next('p.form-item--error-message').remove();
            element.parent().next('p.form-item--error-message').remove();
            element.parent().addClass("form-item--error");
            element.after("<p class='form-item--error-message'>" + label + " field is required.</p>");
          }
        } else {
          element.next('p.form-item--error-message').remove();
          element.parent().removeClass("form-item--error");
        }
        // },1000)

        $('.auto-suggest .model-num-list').on('click','.row-sku',function() {
          if(element.parent().next().find('p.form-item--error-message')) {
            element.parent().next('p.form-item--error-message').remove();
          }
        });
      });

      $(".utility-block .showroom").attr("href","#");

      //Hide Empty P tags Quote__author
      $('.pull__quotes_wrapper p').once().each(function(){
        var $this = $(this);
        if($this.html().replace(/\s|&nbsp;/g, '').length == 0){
          $this.remove();
        }
      });

      //Find showroom close popup
      $(".utility-block .showroom-data .icon-menu-close").click(function () {
        $(".utility-block .showroom-wrapper").removeClass("active-showroom-wrapper");
      });

      // Contact Us Page Purcahse date dropdown option changes
      $("form.webform-submission-contact-us-form #edit-purchase-date-month option:first").text("MM");
      $("form.webform-submission-contact-us-form #edit-purchase-date-day option:first").text("DD");
      $("form.webform-submission-contact-us-form #edit-purchase-date-year option:first").text("YYYY");
      $("form.webform-submission-consumer-registration-form #edit-password").attr('type', 'password');
      $("form.webform-submission-consumer-registration-form #edit-re-type-password").attr('type', 'password');
      var pathname = window.location.href,
      params = pathname.split('?')[1],
      iframesrc = $('#wheretobuy-dccf0fac-8e7a-4095-9642-7d8df866cc1a').attr('src');
      var renderparam = iframesrc+'?form=locator_search=""&'+params;
      $('#block-showroomlocator').once().each(function(){
        $('#wheretobuy-dccf0fac-8e7a-4095-9642-7d8df866cc1a').attr('src',renderparam);
      });
      if(pathname.split('/')[6] == "media-room-article-list"){
        $(".sidebar-main li a").first().addClass("is-active");
      }
      $('#gallery').once().on('click', '.pager__link', function(e){
        var roomselectvalue =  $('select[name="roomselect"]').find(":selected").val();
        var collectionselectvalue = $('select[name="collection"]').find(":selected").val();
        var finishselectvalue = $('select[name="finishes"]').find(":selected").val();
        var pagevalue = $(this).attr('data-load');
        $.ajax({
          type: "GET",
          url: "/design-gallery-page",
          data: {"roomselectvalue" : roomselectvalue,
            "collectionselectvalue" : collectionselectvalue,
            "finishselectvalue" : finishselectvalue,
            "pagevalue" : pagevalue
          },
          success:function(data) {
            $("#gallery").html(data);
            Drupal.attachBehaviors();

            $('.design_gallery_link').once().on('click', function(e) {
              var getimage = $(this).attr('data-id');
              $("#gallery_popup_"+getimage).click();
            });
          }
        });
      });
      $('.page-node-type-landing-page').on('click','.design_gallery_link', function(e) {
        var getimage = $(this).attr('data-id');
        $("#gallery_popup_"+getimage).click();
      });
    $('.prev').once().on('click', function(e){
      var getprev = $(this).attr('data-target-prev');
      $("#gallery_popup_"+getprev).click();
    });
    $('.next').once().on('click', function(e){
      var getnext = $(this).attr('data-target-next');
      $("#gallery_popup_"+getnext).click();
      });

      // design gallery class.
      $(".design_gallery_list").on('click', function(e){
          $("body").addClass("pop-open");
      });
      $('.design-gallery-node div').on('dialogclose', function(event) {
          $("body").removeClass("pop-open");
      });

        $( document, context).ajaxComplete(function( event, xhr, settings ) {
          if($(".design_gallery_list").length > 0 ) {
            var ajax_url = settings.url;
            if(ajax_url.indexOf('/design-gallery-modal') != -1) {
                $('.a2a_dd.addtoany_share').on("click", function(){
                $(".a2a_localize").toggleClass("share-active-open");
                });
            }
          }
        });

    }
  };

  $('.plp-operation .products-sorting').on('change','.form-item__select',function() {
    var select_val = $( ".plp-operation .products-sorting .form-item__select option:selected" ).text();
    if(select_val == $('.plp-operation .products-sorting .form-item__select option:first-child').text()) {
      $(".plp-operation .products-sorting .form-item__select").siblings('.form-item__label--textfield').css('display','none');
    }
  });

  //Product Registration
$('#product_upc_sku_date_table [name="product_upc_sku_date[add][more_items]"]').parent().addClass('add-more-items');
if(!$('.webform-multiple-table input').hasClass('form-item__textfield')) {
  $('.webform-multiple-table input').addClass('form-item__textfield');
}

$('#product_upc_sku_date_table').attr('class','grid-col-lg-6  grid-col-md-12');
$('#product_upc_sku_date_table .product-store-name').parent().addClass('webform-store-name');
$('#edit-product-upc-sku-date').removeClass('js-form-item form-item');

$('#product_upc_sku_date_table .purchase-date,#product_upc_sku_date_table .manufacture-date').on('focus change','select',function() {
  $('#product_upc_sku_date_table .purchase-date,#product_upc_sku_date_table .manufacture-date').parent().removeClass('form-item');
});

$('.purchase-date').parent().css('margin-bottom', 0);

$('#product_upc_sku_date_table .product-date .form-item__select').on('focus',function() {
  $('.font-condensed ').parent().parent().parent().removeClass('form-item');
});

function ajaxfloating() {
  $('#product_upc_sku_date_table .form-item__textfield').on('focus',function() {
    $(this).parent().parent().addClass('has-focus');
  });
  $('#product_upc_sku_date_table .form-item__textfield').on('blur',function() {
    $(this).parent().parent().removeClass('has-focus');
  });
  $('#product_upc_sku_date_table .form-item__textfield').on('change',function() {
    $(this).parent().parent().addClass('is-filled');
  });
  $('.webform-multiple-table .form-item__textfield').each(function() {
    if($(this).val() !== "" ) {
      $(this).parent().parent().addClass('is-filled');
    }else {
      $(this).parent().parent().removeClass('is-filled');
    }
  });

  $('.product-model-number').on('change','.form-item__textfield',function() {
    var getCurID = this.id;
    var getDynamicID = $(this).attr('data-drupal-selector');
    var dynamicID = $(this).attr('id');
    if($('[data-drupal-selector="'+getDynamicID+'"').val() !== "" ) {
      $(this).parent().parent().addClass('is-filled');
    }else {
      $(this).parent().parent().removeClass('is-filled');
      $(this).parent().parent().attr('id',"");
      //$(this).parent().parent().attr('id').remove();
    }
  })

  $('.webform-multiple-table').removeClass('is-filled');
  $('.purchased-date').parent().parent().css('position','static');
}

$('.webform-submission-product-registration-form .purchased-date').parent().parent().css('position','static');
$('.webform-submission-product-registration-form .purchase-date').parent().find('label.form-item__label').css('position', 'static');
  $('.webform-submission-product-registration-form .purchase-date, .webform-submission-product-registration-form .manufacture-date').parent().find('label.form-item__label span').attr('class', 'font-condensed');
$(document).ready(function() {
  prodMfgPurchase();
})

function prodMfgPurchase() {
  $('.webform-submission-product-registration-form .purchase-date .purchase-month').find('.form-select').addClass('select-month');
  $('.webform-submission-product-registration-form .purchase-date .purchase-day').find('.form-select').addClass('select-day');
  $('.webform-submission-product-registration-form .purchase-date .purchase-year').find('.form-select').addClass('select-year');

  $('.webform-submission-product-registration-form .select-month,.webform-submission-product-registration-form .select-day,.webform-submission-product-registration-form .select-year').once().on('change',function() {
    $('.purchase-date').next('.form-item--error-message').remove();
    $('.purchase-date').find('.form-item--error').removeClass('form-item--error');
    $('.purchase-date').find('.required').removeClass('required');
    var getCurID = this.id;
    var getDynamicID = $(this).attr('data-drupal-selector');
    var getDynamicTitle = $(this).attr('title');
    getDynamicTitle = getDynamicTitle.toLowerCase();
    var getLeftSidePart = getDynamicID.replace('edit-product-upc-sku-date-items-', '');
    var searchStr = '-item-product-purchase-date-' + getDynamicTitle;
    var getNum = getLeftSidePart.replace(searchStr, '');
    var d = new Date();
    var n = d.getMonth() + 1;
    var y = d.getFullYear();
    date =  d.getDate();
    today_date =   ("0" + (d.getMonth() + 1)).slice(-2) +"/" + date +"/" + y;


    //if block

    var selectNamYear = 'product_upc_sku_date[items]['+getNum+'][_item_][product_purchase_date][year]';
    var SelectedYear = $('[name="'+selectNamYear+'"').val();
    if ( y == SelectedYear ) {
      var selectNamDay = 'product_upc_sku_date[items]['+getNum+'][_item_][product_purchase_date][day]';
      selected_date = $('[name="'+selectNamDay+'"').val();
      var selectNamMonth = 'product_upc_sku_date[items]['+getNum+'][_item_][product_purchase_date][month]';
      selected_month = $('[name="'+selectNamMonth+'"').val();
      $('[data-drupal-selector="edit-product-upc-sku-date-items-'+getNum+'-item-product-purchase-date"').next(".form-item--error-message").remove();
      $('[name="'+selectNamMonth+'"').parent().removeClass('form-item--error');
      $('[name="'+selectNamYear+'"').parent().removeClass("form-item--error");
      $('[name="'+selectNamDay+'"').parent().removeClass("form-item--error");

      if(selected_month > n) {
        $('[data-drupal-selector="edit-product-upc-sku-date-items-'+getNum+'-item-product-purchase-date"').after("<p class='form-item--error-message'>Date of Purchase must be on or before " + today_date +"</p>");
        $('[name="'+selectNamMonth+'"').parent().addClass("form-item--error");
        $('[name="'+selectNamYear+'"').parent().addClass("form-item--error");
        if(!$('[name="'+selectNamDay+'"').val() === "" ) {
          $('[name="'+selectNamDay+'"').parent().addClass("form-item--error");
        }
        //$('.webform-submission-product-registration-form .webform-button--submit').prop('disabled', true);
        $('[name="'+selectNamMonth+'"').addClass('required');
      }
      else if( selected_date > date && selected_month == n) {
        $('[data-drupal-selector="edit-product-upc-sku-date-items-'+getNum+'-item-product-purchase-date"').after("<p class='form-item--error-message'>Date of Purchase must be on or before " + today_date +"</p>");
        $('[name="'+selectNamDay+'"').parent().addClass("form-item--error");
        $('[name="'+selectNamYear+'"').parent().addClass("form-item--error");
        if(!$('[name="'+selectNamDay+'"').val() ) {
          $('[name="'+selectNamDay+'"').parent().addClass("form-item--error");
        }
      //  $('.webform-submission-product-registration-form .webform-button--submit').prop('disabled', true);
        $('[name="'+selectNamMonth+'"').addClass('required');


      }else {
        $('[name="'+selectNamMonth+'"').removeClass('required');
        if($('.webform-submission-product-registration-form .required').length === jQuery('.valid').length)  {
          formFlag = 1;
          if(captchaFlag === 1 ) {
            //$('.webform-submission-product-registration-form .webform-button--submit').prop('disabled', false);
          }
        }else {
          formFlag = 0;
          //$('.webform-submission-product-registration-form .webform-button--submit').prop('disabled', true);
        }
      }
    }else {
      $('[name="'+selectNamMonth+'"').parent().removeClass("form-item--error");
      $('[name="'+selectNamYear+'"').parent().removeClass("form-item--error");
      $('[name="'+selectNamDay+'"').parent().removeClass("form-item--error");
      $('[name="'+selectNamMonth+'"').removeClass('required');
      if($('.webform-submission-product-registration-form .required').length === jQuery('.valid').length)  {
        formFlag = 1;
        if(captchaFlag === 1 ) {
          //$('.webform-submission-product-registration-form .webform-button--submit').prop('disabled', false);
        }
      }else {
        formFlag = 0;
        //$('.webform-submission-product-registration-form .webform-button--submit').prop('disabled', true);
      }
    }
  });

  //Manufacture Date
  $('.webform-submission-product-registration-form .manufacture-date .manufacture-month').find('.form-select').addClass('select-mfg-month');
  $('.webform-submission-product-registration-form .manufacture-date .manufacture-day').find('.form-select').addClass('select-mfg-day');
  $('.webform-submission-product-registration-form .manufacture-date .manufacture-year').find('.form-select').addClass('select-mfg-year');

  $('.webform-submission-product-registration-form .select-mfg-month,.webform-submission-product-registration-form .select-mfg-day,.webform-submission-product-registration-form .select-mfg-year').once().on('change',function() {
    $('.manufacture-date').next('.form-item--error-message').remove();
    $('.manufacture-date').find('.required').removeClass('required');
    $('.manufacture-date').find('.form-item--error').removeClass('form-item--error');
    var getCurID1 = this.id;
    var getDynamicID1 = $(this).attr('data-drupal-selector');
    var getDynamicTitle1 = $(this).attr('title');
    getDynamicTitle1 = getDynamicTitle1.toLowerCase();

    var getLeftSidePart1 = getDynamicID1.replace('edit-product-upc-sku-date-items-', '');
    var searchStr1 = '-item-product-manufacture-date-' + getDynamicTitle1;
    var getNum1 = getLeftSidePart1.replace(searchStr1, '');
    var day = new Date();
    var month = day.getMonth() + 1;
    var year = day.getFullYear();
    date =  day.getDate();
    today_date =   ("0" + (day.getMonth() + 1)).slice(-2) +"/" + date +"/" + year;

    //if block

    var selectManYear = 'product_upc_sku_date[items]['+getNum1+'][_item_][product_manufacture_date][year]';
    var yearSelected = $('[name="'+selectManYear+'"').val();
    if ( year == yearSelected ) {
      var selectManDay = 'product_upc_sku_date[items]['+getNum1+'][_item_][product_manufacture_date][day]';
      selectedDate = $('[name="'+selectManDay+'"').val();
      var selectManMonth = 'product_upc_sku_date[items]['+getNum1+'][_item_][product_manufacture_date][month]';
      selectedMonth = $('[name="'+selectManMonth+'"').val();
      $('[data-drupal-selector="edit-product-upc-sku-date-items-'+getNum1+'-item-product-manufacture-date"').next(".form-item--error-message").remove();
      $('[name="'+selectManMonth+'"').parent().removeClass('form-item--error');
      $('[name="'+selectManYear+'"').parent().removeClass("form-item--error");
      $('[name="'+selectManDay+'"').parent().removeClass("form-item--error");
      if(selectedMonth > month) {
        $('[data-drupal-selector="edit-product-upc-sku-date-items-'+getNum1+'-item-product-manufacture-date"').after("<p class='form-item--error-message'>Date of Manufacture must be on or before " + today_date +"</p>");
        $('[name="'+selectManMonth+'"').parent().addClass("form-item--error");
        $('[name="'+selectManYear+'"').parent().addClass("form-item--error");
        if(!$('[name="'+selectManDay+'"').val() === "" ) {
          $('[name="'+selectManDay+'"').parent().addClass("form-item--error");
        }
      //  $('.webform-submission-product-registration-form .webform-button--submit').prop('disabled', true);
        $('[name="'+selectManMonth+'"').addClass('required');
      }
      else if( selectedDate > date && selectedMonth == month) {
        $('[data-drupal-selector="edit-product-upc-sku-date-items-'+getNum1+'-item-product-manufacture-date"').after("<p class='form-item--error-message'>Date of Manufacture must be on or before " + today_date +"</p>");
        $('[name="'+selectManMonth+'"').parent().addClass("form-item--error");
        $('[name="'+selectManYear+'"').parent().addClass("form-item--error");
        if(!$('[name="'+selectManDay+'"').val() === "" ) {
          $('[name="'+selectManDay+'"').parent().addClass("form-item--error");
        }
      //  $('.webform-submission-product-registration-form .webform-button--submit').prop('disabled', true);
        $('[name="'+selectManMonth+'"').addClass('required');
      }
      else {
        $('[name="'+selectManMonth+'"').removeClass('required');
        if($('.webform-submission-product-registration-form .required').length === jQuery('.valid').length)  {
          formFlag = 1;
          if(captchaFlag === 1 ) {
          //  $('.webform-submission-product-registration-form .webform-button--submit').prop('disabled', false);
          }
        }else {
          formFlag = 0;
        //  $('.webform-submission-product-registration-form .webform-button--submit').prop('disabled', true);
        }
      }
    }
    else {
      $('[name="'+selectManMonth+'"').parent().removeClass("form-item--error");
      $('[name="'+selectManYear+'"').parent().removeClass("form-item--error");
      $('[name="'+selectManDay+'"').parent().removeClass("form-item--error");
      $('[name="'+selectManMonth+'"').removeClass('required');
      if($('.webform-submission-product-registration-form .required').length === jQuery('.valid').length)  {
        formFlag = 1;
        if(captchaFlag === 1 ) {
        //  $('.webform-submission-product-registration-form .webform-button--submit').prop('disabled', false);
        }
      }else {
        formFlag = 0;
      //  $('.webform-submission-product-registration-form .webform-button--submit').prop('disabled', true);
      }
    }
  })
}

//Ajax Complete for ProductRegistration
$(document).ajaxComplete(function(event, xhr, settings) {
  if((settings.extraData != undefined && settings.extraData.view_name !== 'undefined' && settings.extraData.view_name != 'wob_filtered_view')
  || (settings.extraData != undefined && settings.extraData.view_name !== 'undefined' && settings.extraData.view_name != 'world_of_brizo_category_titles')){

    var getUrl = settings.url;
    $('.webform-multiple-table input').addClass('form-item__textfield');
    $('#product_upc_sku_date_table').attr('class','grid-col-lg-6');

    if(getUrl.indexOf('sku-autocomplete')) {
      $('.auto-suggest .model-num-list').on('click','.row-sku',function() {
        $('.ui-autocomplete-input').removeClass('ui-autocomplete-loading');
      });
    }

    $('.webform-submission-product-registration-form .purchase-date').parent().css('margin-bottom', 0);
    $('.webform-submission-product-registration-form [name="product_upc_sku_date[add][more_items]"]').parent().addClass('add-more-items');
    if ( xhr.status == 200 && settings.extraData != undefined && settings.extraData._triggering_element_value !== 'undefined' &&
    settings.extraData._triggering_element_name == 'product_upc_sku_date_table_add') {
      //$('.webform-submission-product-registration-form .manufacture-date').find('.form-item__select').prop('selectedIndex',0);
      $('#product_upc_sku_date_table .product-store-name').parent().addClass('webform-store-name');
      ajaxfloating();
      prodMfgPurchase();
    }
    if ( xhr.status == 200 && settings.extraData != undefined && settings.extraData._triggering_element_value !== 'undefined'){
      if (settings.extraData._triggering_element_name !== 'undefined' && settings.extraData._triggering_element_name?.slice(0,-2) == 'product_upc_sku_date_table_remove') {
        ajaxfloating();
        prodMfgPurchase();
        $('#product_upc_sku_date_table .product-store-name').parent().addClass('webform-store-name');

        var removebutton = $('.webform-submission-product-registration-form .webform-multiple-table .parent_product_upc_sku_date_table_remove');
        if(removebutton.length === 1) {
          $('.webform-submission-product-registration-form .webform-multiple-table .parent_product_upc_sku_date_table_remove').addClass('removecrossbutton');
        }
      }
    }
  }
});

$(".webform-submission-contact-us-form .form-item__textfield").on("change input", function(e) {
  var nameregex  = Drupal.behaviors.commonvalidationfunction.usernameregex(),
      emailregex = Drupal.behaviors.commonvalidationfunction.emailidregex(),
      phoneregex = Drupal.behaviors.commonvalidationfunction.phonenumberregex(),
      address = Drupal.behaviors.commonvalidationfunction.addressreg(),
      city = Drupal.behaviors.commonvalidationfunction.cityvalidation(),
      zip_code = Drupal.behaviors.commonvalidationfunction.zipcodevalidation(),
      value = 0;

  var inputValue = $(this).val(),
      label = $($("label[for='" + this.id + "']").contents().get(0)).text(),
      element = $(this);

  //Name field
  var validatefunction = setTimeout(function() {
    if((element.val()) !== "") {
      element.next('p.form-item--error-message').remove();
      element.parent().removeClass('form-item--error');
    }

    if(element.hasClass("validate-name")) {
      $('.webform-submission-contact-us-form').find(".validate-name").removeClass("form-item--error");
      var inputValue = element.val();
      if( inputValue == "" ) {
        element.next('p.form-item--error-message').remove();
        element.after("<p class='form-item--error-message'> "+ label + " field is required.</p>");
        element.parent().addClass("form-item--error");
        value++;
      } else if(!inputValue.match(nameregex)) {
        element.next('p.form-item--error-message').remove();
        element.after("<p class='form-item--error-message'>Please enter a valid " + label + ".</p>");
        element.parent().addClass("form-item--error");
        value++;
      } else {
        element.next('p.form-item--error-message').remove();
        element.parent().removeClass("form-item--error");
        value--;
      }
    }

    //Email Address
    if(element.hasClass("validate-email")) {
      $('.webform-submission-contact-us-form').find(".validate-email").removeClass("form-item--error");
      var inputValue = element.val();
      if( inputValue == "" ) {
        element.next('p.form-item--error-message').remove();
        element.after("<p class='form-item--error-message'> "+ label + " field is required.</p>");
        element.parent().addClass("form-item--error");
        value++;
      } else if(!inputValue.match(emailregex)) {
        element.next('p.form-item--error-message').remove();
        element.after("<p class='form-item--error-message'>Please enter a valid " + label + ".</p>");
        element.parent().addClass("form-item--error");
        value++;
      } else {
        element.next('p.form-item--error-message').remove();
        element.parent().removeClass("form-item--error");
        value--;
      }
    };
    if(element.hasClass("melliansa-validation")) {
      $('.webform-submission-contact-us-form').find(".melliansa-validation").removeClass("form-item--error");
      var emailID = element.val();
      mellinsa = Drupal.behaviors.commonvalidationfunction.searchEmail(emailID, element);
    };

    //PhoneNumber validation
    if(element.hasClass("validate-phone")) {
      $('.webform-submission-contact-us-form').find(".validate-phone").removeClass("form-item--error");
      var inputValue = element.val();
      if( inputValue == "" ) {
        element.next('p.form-item--error-message').remove();
        // element.after("<p class='form-item--error-message'> "+ label + " field is required.</p>");
        element.parent().removeClass("form-item--error");
        // value++;
      }else if(!inputValue.match(phoneregex)) {
        element.next('p.form-item--error-message').remove();
        element.after("<p class='form-item--error-message'>Please enter a valid " + label + ".</p>");
        element.parent().addClass("form-item--error");
        value++;
      }
    };

    //Address field
    if(element.hasClass("validate-address")) {
      $('.webform-submission-contact-us-form').find(".validate-address").removeClass("form-item--error");
      var inputValue = element.val();
      if( inputValue == "" ) {
        element.next('p.form-item--error-message').remove();
        element.after("<p class='form-item--error-message'> "+ label + " field is required.</p>");
        element.parent().addClass("form-item--error");
        value++;
      } else if(!inputValue.match(address)) {
        element.next('p.form-item--error-message').remove();
        element.after("<p class='form-item--error-message'>Please enter a valid " + label + ".</p>");
        element.parent().addClass("form-item--error");
        value++;
      } else {
        element.next('p.form-item--error-message').remove();
        element.parent().removeClass("form-item--error");
        value--;
      }
    };

    //City
    if(element.hasClass("validate-city")) {
      $('.webform-submission-contact-us-form').find(".validate-city").removeClass("form-item--error");
      var inputValue = element.val();
      if( inputValue == "" ) {
        element.next('p.form-item--error-message').remove();
        element.after("<p class='form-item--error-message'> "+ label + " field is required.</p>");
        element.parent().addClass("form-item--error");
        value++;
      } else if(!inputValue.match(city)) {
        element.next('p.form-item--error-message').remove();
        element.after("<p class='form-item--error-message'>Please enter a valid " + label + ".</p>");
        element.parent().addClass("form-item--error");
        value++;
      } else {
        element.next('p.form-item--error-message').remove();
        element.parent().removeClass("form-item--error");
        value--;
      }
    };

    //Zipcode validation
    if(element.hasClass("validate-zipcode")) {
      $('.webform-submission-contact-us-form').find(".validate-zipcode").removeClass("form-item--error");
      var inputValue = element.val();
      if( inputValue == "" ) {
        element.next('p.form-item--error-message').remove();
        element.after("<p class='form-item--error-message'> "+ label + " field is required.</p>");
        element.parent().addClass("form-item--error");
        value++;
      } else if(!inputValue.match(zip_code)) {
        element.next('p.form-item--error-message').remove();
        element.after("<p class='form-item--error-message'>Please enter a valid " + label + ".</p>");
        element.parent().addClass("form-item--error");
        value++;
      } else {
        element.next('p.form-item--error-message').remove();
        element.parent().removeClass("form-item--error");
        value--;
      }
    };

    if(element.hasClass("validate-textarea")) {
      $('.contact-form').find(".validate-textarea").removeClass("form-item--error");
      var inputValue = element.val();
      if( inputValue == "" ) {
        element.next('p.form-item--error-message').remove();
        element.after("<p class='form-item--error-message'> "+ label + " field is required.</p>");
        element.parent().addClass("form-item--error");
        value++;
      } else {
        element.next('p.form-item--error-message').remove();
        element.parent().removeClass("form-item--error");
        value--;
      }
    };

    clearTimeout(validatefunction);
  }, 3000);
});

//ContactUs On click of submit button
$('.webform-submission-contact-us-form').on('click','.form-submit',function() {
  var value = 0,
      phoneregex = Drupal.behaviors.commonvalidationfunction.phonenumberregex();
  $('.webform-submission-contact-us-form').find(".required:visible").not(".form-radio, .shipping_option .required").each(function() {
    var inputValue = $(this).val(),
    label = $($("label[for='" + this.id + "']").contents().get(0)).text();
    if((inputValue == "") || (inputValue == 0)) {
      if($(this).next().find('p.form-item--error-message')) {
        $(this).next('p.form-item--error-message').remove();
        value++;
        $(this).parent().addClass("form-item--error");
        $(this).after("<p class='form-item--error-message'>" + label + " field is required.</p>");
      }
    }
    element = $(this);
    value = brizo.commonFunctions.FormFieldTypeValidation(element, label, value);
  });

  if($('.webform-submission-contact-us-form').find("validate-phone")) {
    $('.webform-submission-contact-us-form').find(".validate-phone").removeClass("form-item--error");
    var inputValue = $('.webform-submission-contact-us-form .validate-phone').val();
        label = $($("label[for='" + $('.webform-submission-contact-us-form .validate-phone').id + "']").contents().get(0)).text();
    if( inputValue == "" ) {
      $('.webform-submission-contact-us-form .validate-phone').next('p.form-item--error-message').remove();
      // element.after("<p class='form-item--error-message'> "+ label + " field is required.</p>");
      $('.webform-submission-contact-us-form .validate-phone').parent().removeClass("form-item--error");
      // value++;
    }else if(!inputValue.match(phoneregex)) {
      $('.webform-submission-contact-us-form .validate-phone').next('p.form-item--error-message').remove();
      $('.webform-submission-contact-us-form .validate-phone').after("<p class='form-item--error-message'>Please enter a valid Phone number.</p>");
      $('.webform-submission-contact-us-form .validate-phone').parent().addClass("form-item--error");
      value++;
    }
  };

  if(grecaptcha.getResponse() == "") {
    event.preventDefault();
  }else {
    $('.captcha-validate').next('p.form-item--error-message').remove();

  }

  if($('.webform-submission-contact-us-form').find("p.form-item--error-message").text()) {
    value++;
  }

  if($('.webform-submission-contact-us-form').find('.form-item--error')) {
    $("html, body").animate({
      scrollTop:  ($('.form-item--error').offset() || { "top": NaN }).top - 200
    }, 500);
  }

  if(value > 0) {
    return false;
  }
});

$('.webform-submission-contact-us-form').on('change','.form-select',function() {
  var inputValue = $(this).val(),
      label = $($("label[for='" + this.id + "']").contents().get(0)).text();
  if(inputValue !== "") {
    $(this).next('p.form-item--error-message').remove();
    $(this).parent().removeClass('form-item--error');
  }else {
      $(this).after("<p class='form-item--error-message'>" + label + " field is required.</p>");
      $(this).parent().addClass('form-item--error');
  }
});

//Product Registration form Realtime Validation
$(".webform-submission-product-registration-form .required").on("change input", function(e) {
  var nameregex  = Drupal.behaviors.commonvalidationfunction.usernameregex(),
      emailregex = Drupal.behaviors.commonvalidationfunction.emailidregex(),
      phoneregex = Drupal.behaviors.commonvalidationfunction.phonenumberregex(),
      address = Drupal.behaviors.commonvalidationfunction.addressreg(),
      city = Drupal.behaviors.commonvalidationfunction.cityvalidation(),
      zip_code = Drupal.behaviors.commonvalidationfunction.zipcodevalidation(),
      model_number = Drupal.behaviors.commonvalidationfunction.modelnumber(),
      value = 0;

  var inputValue = $(this).val(),
      label = $(this).parent().find('label').text(),
      element = $(this);

  //Name field
  var validatefunction = setTimeout(function() {
    if((element.val()) !== "") {
      element.next('p.form-item--error-message').remove();
      element.parent().removeClass('form-item--error');
    } else {
      element.next('p.form-item--error-message').remove();
      element.after("<p class='form-item--error-message'> "+ label + " field is required.</p>");
      element.parent().addClass("form-item--error");
    }

    if(element.hasClass("validate-name")) {
      $('.webform-submission-product-registration-form').find(".validate-name").removeClass("form-item--error");
      var inputValue = element.val();
      if( inputValue == "" ) {
        element.next('p.form-item--error-message').remove();
        element.after("<p class='form-item--error-message'> "+ label + " field is required.</p>");
        element.parent().addClass("form-item--error");
        value++;
      } else if(!inputValue.match(nameregex)) {
        element.next('p.form-item--error-message').remove();
        element.after("<p class='form-item--error-message'>Please enter a valid " + label + ".</p>");
        element.parent().addClass("form-item--error");
        value++;
      }
    }

    //Email Address
    if(element.hasClass("validate-email")) {
      $('.webform-submission-product-registration-form').find(".validate-email").removeClass("form-item--error");
      var inputValue = element.val();
      if( inputValue == "" ) {
        element.next('p.form-item--error-message').remove();
        element.after("<p class='form-item--error-message'> "+ label + " field is required.</p>");
        element.parent().addClass("form-item--error");
        value++;
      } else if(!inputValue.match(emailregex)) {
        element.next('p.form-item--error-message').remove();
        element.after("<p class='form-item--error-message'>Please enter a valid " + label + ".</p>");
        element.parent().addClass("form-item--error");
        value++;
      }
    };
    if(element.hasClass("melliansa-validation")) {
      $('.webform-submission-product-registration-form').find(".melliansa-validation").removeClass("form-item--error");
      var emailID = element.val();
      mellinsa = Drupal.behaviors.commonvalidationfunction.searchEmail(emailID, element);
    };

    //PhoneNumber validation
    if(element.hasClass("validate-phone")) {
      $('.webform-submission-product-registration-form').find(".validate-phone").removeClass("form-item--error");
      var inputValue = element.val();
      if( inputValue == "" ) {
        element.next('p.form-item--error-message').remove();
        element.after("<p class='form-item--error-message'> "+ label + " field is required.</p>");
        element.parent().addClass("form-item--error");
        value++;
      } else if(!inputValue.match(phoneregex)) {
        element.next('p.form-item--error-message').remove();
        element.after("<p class='form-item--error-message'>Please enter a valid " + label + ".</p>");
        element.parent().addClass("form-item--error");
        value++;
      }
    };

    //Address field
    if(element.hasClass("validate-address")) {
      $('.webform-submission-product-registration-form').find(".validate-address").removeClass("form-item--error");
      var inputValue = element.val();
      if( inputValue == "" ) {
        element.next('p.form-item--error-message').remove();
        element.after("<p class='form-item--error-message'> "+ label + " field is required.</p>");
        element.parent().addClass("form-item--error");
        value++;
      } else if(!inputValue.match(address)) {
        element.next('p.form-item--error-message').remove();
        element.after("<p class='form-item--error-message'>Please enter a valid " + label + ".</p>");
        element.parent().addClass("form-item--error");
        value++;
      }
    };

    //City
    if(element.hasClass("validate-city")) {
      $('.webform-submission-product-registration-form').find(".validate-city").removeClass("form-item--error");
      var inputValue = element.val();
      if( inputValue == "" ) {
        element.next('p.form-item--error-message').remove();
        element.after("<p class='form-item--error-message'> "+ label + " field is required.</p>");
        element.parent().addClass("form-item--error");
        value++;
      } else if(!inputValue.match(city)) {
        element.next('p.form-item--error-message').remove();
        element.after("<p class='form-item--error-message'>Please enter a valid " + label + ".</p>");
        element.parent().addClass("form-item--error");
        value++;
      }
    };

    //Zipcode validation
    if(element.hasClass("validate-zipcode")) {
      $('.webform-submission-product-registration-form').find(".validate-zipcode").removeClass("form-item--error");
      var inputValue = element.val();
      if( inputValue == "" ) {
        element.next('p.form-item--error-message').remove();
        element.after("<p class='form-item--error-message'> "+ label + " field is required.</p>");
        element.parent().addClass("form-item--error");
        value++;
      } else if(!inputValue.match(zip_code)) {
        element.next('p.form-item--error-message').remove();
        element.after("<p class='form-item--error-message'>Please enter a valid " + label + ".</p>");
        element.parent().addClass("form-item--error");
        value++;
      }
    };

    if(element.hasClass("validate-textarea")) {
      $('.webform-submission-product-registration-form').find(".validate-textarea").removeClass("form-item--error");
      var inputValue = element.val();
      if( inputValue == "" ) {
        element.next('p.form-item--error-message').remove();
        element.after("<p class='form-item--error-message'> "+ label + " field is required.</p>");
        element.parent().addClass("form-item--error");
        value++;
      }
    };

    clearTimeout(validatefunction);
  }, 3000);
});

//Product Registration On click of submit button
$('.webform-submission-product-registration-form').on('click','.webform-button--submit',function(e) {
  var value = 0;
  $('.webform-submission-product-registration-form').find(".required:visible").not(".form-radio, .shipping_option .required").each(function() {
    var inputValue = $(this).val(),
    label = $(this).parent().find('label').text();
    if((inputValue == "") || (inputValue == 0)) {
      if($(this).next().find('p.form-item--error-message')) {
        $(this).next('p.form-item--error-message').remove();
        value++;
        $(this).parent().addClass("form-item--error");
        $(this).after("<p class='form-item--error-message'>" + label + " field is required.</p>");
      }
    }
    element = $(this);
    value = brizo.commonFunctions.FormFieldTypeValidation(element, label, value);
  });

  if(grecaptcha.getResponse() == "") {
    event.preventDefault();
  } else {
    $('.captcha').next('p.form-item--error-message').remove();
  }

  if($('.webform-submission-product-registration-form').find('.form-item--error')) {
    $("html, body").animate({
      scrollTop:  ($('.form-item--error').offset() || { "top": NaN }).top - 200
    }, 500);
  }

  var error = $('.form-item--error');
  console.log($('.webform-submission-product-registration-form .webform-multiple-table .purchase-date').find('.form-item--error'));

  if($('.webform-submission-product-registration-form .webform-multiple-table .purchase-date .js-form-item').hasClass('form-item--error')) {
    value++;
  }

  if($('.webform-submission-product-registration-form .webform-multiple-table .manufacture-date .js-form-item').hasClass('form-item--error')) {
    value++;
  }
  if($('.webform-submission-product-registration-form').find("p.form-item--error-message").text()) {
    value++;
  }

  // if(value == 1) {
  //   value--;
  // }
  if(value > 0) {
    return false;
  }

});

$('.webform-submission-product-registration-form').on('change','.form-select',function() {
  var inputValue = $(this).val(),
      label = $($("label[for='" + this.id + "']").contents().get(0)).text();
  if(inputValue !== "") {
    $(this).next('p.form-item--error-message').remove();
    $(this).parent().removeClass('form-item--error');
  }else {
    $(this).after("<p class='form-item--error-message'>" + label + " field is required.</p>");
    $(this).parent().addClass('form-item--error');
  }
});

$(".contact-form-newsletter .required").on("change input", function(e) {
  var nameregex  = Drupal.behaviors.commonvalidationfunction.usernameregex(),
      emailregex = Drupal.behaviors.commonvalidationfunction.emailidregex(),
      value = 0;

      //var inputValue = $(this).val(),
      label = $(this).parent().find('label').text(),
      element = $(this);

  //Name field
  var validatefunction = setTimeout(function() {
    if((element.val()) !== "") {
      element.next('p.form-item--error-message').remove();
      element.parent().removeClass('form-item--error');
    } else {
      element.next('p.form-item--error-message').remove();
      element.after("<p class='form-item--error-message'> "+ label + " field is required.</p>");
      element.parent().addClass("form-item--error");
    }

    if(element.hasClass("validate-name")) {
      $('.contact-form-newsletter').find(".validate-name").removeClass("form-item--error");
      var inputValue = element.val();
      if( inputValue == "" ) {
        element.next('p.form-item--error-message').remove();
        element.after("<p class='form-item--error-message'> "+ label + " field is required.</p>");
        element.parent().addClass("form-item--error");
        value++;
      } else if(!inputValue.match(nameregex)) {
        element.next('p.form-item--error-message').remove();
        element.after("<p class='form-item--error-message'>Please enter a valid " + label + ".</p>");
        element.parent().addClass("form-item--error");
        value++;
      }
    }

    //Email Address
    if(element.hasClass("validate-email")) {
      $('.contact-form-newsletter').find(".validate-email").removeClass("form-item--error");
      var inputValue = element.val();
      if( inputValue == "" ) {
        element.next('p.form-item--error-message').remove();
        element.after("<p class='form-item--error-message'> "+ label + " field is required.</p>");
        element.parent().addClass("form-item--error");
        value++;
      } else if(!inputValue.match(emailregex)) {
        element.next('p.form-item--error-message').remove();
        element.after("<p class='form-item--error-message'>Please enter a valid " + label + ".</p>");
        element.parent().addClass("form-item--error");
        value++;
      }
    };
    if(element.hasClass("validate-email")) {
      $(".contact-form-newsletter").find(".melliansa-validation").each(function() {
        var element = $(this);
        var emailID = element.val();
        mellinsa = Drupal.behaviors.commonvalidationfunction.searchEmail(emailID, element);
      })
    };

    clearTimeout(validatefunction);
  }, 3000);

});

$(".contact-form-newsletter .webform-button--submit").click(function(e) {
  var value = 0;

  $(".contact-form-newsletter").find(".required:visible").each(function() {
    element = $(this);
    label = $(this).parent().find('label').text();
    value = brizo.commonFunctions.FormFieldTypeValidation(element, label, value);
  });

  if($(".contact-form-newsletter").find("p.form-item--error-message").text()) {
    value++;
  }
  if(value > 0){
    return false;
  }
});

  //Lazy load
  function desktoplazyloadhomepage() {
    if($('.lazy-load-on').length > 0 ) {
      $(window).on('scroll',function() {
        $('.lazy-load-on').each(function() {
          if($(this).find('[data-src-desk]')) {
            $(this).find('[data-src-desk]').addClass('desktop-lazy-load')
            $(this).find('[data-src-desk]').siblings().addClass('lazy');
            if ($(this).find('.lazy').attr('data-src-desk') !== "") {
              var window_height = $(window).height();
              var offset = $(this).find('.lazy').offset().top - window_height;
              if ($(window).scrollTop() >= offset) {
                var img_source = $(this).find('.desktop-lazy-load').attr("data-src-desk");
                $(this).find('.desktop-lazy-load').attr('srcset', img_source);
                $(this).removeAttr('data-src-desk');
              }
            }
          } if ($(this).find('[data-src]')) {
            $(this).find('[data-src]').addClass('lazy');
            if ($(this).find('.lazy').attr('data-src') !== "") {
              var window_height = $(window).height();
              var offset = $(this).find('.lazy').offset().top - window_height;
              if ($(window).scrollTop() >= offset) {
                var img_source = $(this).find('.lazy').attr("data-src");
                $(this).find('.lazy').css('background-image', 'url(' + img_source + ')');
                $(this).removeAttr('data-src');
              }
            }
          }
        })
      })
    }
  }

  $(window).on('resize', function(e) {
    if ($(window).width() >= 768) {
      desktoplazyloadhomepage();
    }
  });

  if ($(window).width() >= 768) {
    desktoplazyloadhomepage();
  }

  function mobilelazyloadhomepage() {
    if($('.lazy-load-on').length > 0 ) {
      $(window).on('scroll',function() {
        $('.lazy-load-on').each(function() {
          if($(this).find('[data-src-mobile]')) {
            $(this).find('[data-src-mobile]').addClass('lazy');
            if ($(this).find('.lazy').attr('data-src-mobile') !== "") {
              var window_height = $(window).height();
              var offset = $(this).find('.lazy').offset().top - window_height;
              if ($(window).scrollTop() >= offset) {
                var img_source = $(this).find('.lazy').attr("data-src-mobile");
                $(this).find('.lazy').attr('srcset', img_source);
                $(this).removeAttr('data-src-mobile');
              }
            }
          }
        })
      })
    }
  }

  $(window).on('resize', function(e) {
    if ($(window).width() < 768) {
      mobilelazyloadhomepage();
    }
  });

  if ($(window).width() < 768) {
    mobilelazyloadhomepage();
  }

  $('.listing-lazy-load').find('img').addClass('lazy');

  $(window).on('scroll',function() {
    $('.listing-lazy-load img').each(function() {
      if($(this).attr('data-src')) {
        if ($(this).attr('data-src') !== "") {
          var window_height = $(window).height();
          var offset = $(this).offset().top - window_height;
          if ($(window).scrollTop() >= offset) {
            var img_src =  $(this).attr('data-src');
            $(this).attr('src', img_src);
            $(this).removeClass('lazy');
            $(this).removeAttr('data-src');
            return false;
          }
        }
      }
      if($(this).attr('data-src') == "") {
        $(this).attr('src','');
        $(this).removeClass('lazy');
      }
    })
  });

  function mobileviewport() {
    $('.listing-lazy-load').find('img.lazy:lt(3)').each(function() {
      var src_img = $(this).attr('data-src');
      $(this).attr('src', src_img);
      $(this).removeClass('lazy');
      $(this).removeAttr('data-src');
    })
  }

  $(window).on('resize', function(e) {
    if ($(window).width() < 768) {
      mobileviewport();
    }
  });

  if ($(window).width() < 768) {
    mobileviewport();
  }

  // Form Validation
  var brizo = {};

  (function($, _Drupal) {

     // Recaptcha Exp
     $('.g-recaptcha').attr('data-expired-callback','captchaExpired');

    brizo.commonFunctions = {
      FormFieldTypeValidation: function(element, label, value){
        var nameregex  = Drupal.behaviors.commonvalidationfunction.usernameregex(),
            emailregex = Drupal.behaviors.commonvalidationfunction.emailidregex(),
            phoneregex = Drupal.behaviors.commonvalidationfunction.phonenumberregex(),
            address = Drupal.behaviors.commonvalidationfunction.addressreg(),
            city = Drupal.behaviors.commonvalidationfunction.cityvalidation(),
            zip_code = Drupal.behaviors.commonvalidationfunction.zipcodevalidation();

        if(element.hasClass("validate-name")) {
          $('.contact-form').find(".validate-name").removeClass("form-item--error");
          var inputValue = element.val();
          if( inputValue == "" ) {
            element.next('p.form-item--error-message').remove();
            element.after("<p class='form-item--error-message'> "+ label + " field is required.</p>");
            element.parent().addClass("form-item--error");
            value++;
          } else if(!inputValue.match(nameregex)) {
            element.next('p.form-item--error-message').remove();
            element.after("<p class='form-item--error-message'>Please enter a valid " + label + ".</p>");
            element.parent().addClass("form-item--error");
            value++;
          }
        }

        if(element.hasClass("validate-email")) {
          $('.contact-form').find(".validate-email").removeClass("form-item--error");
          var inputValue = element.val();
          if( inputValue == "" ) {
            element.next('p.form-item--error-message').remove();
            element.after("<p class='form-item--error-message'> "+ label + " field is required.</p>");
            element.parent().addClass("form-item--error");
            value++;

          } else if(!inputValue.match(emailregex)) {
            element.next('p.form-item--error-message').remove();
            element.after("<p class='form-item--error-message'>Please enter a valid " + label + ".</p>");
            element.parent().addClass("form-item--error");
            value++;
          }
        }

        //PhoneNumber validation
        if(element.hasClass("validate-phone")) {
          $('.contact-form').find(".validate-phone").removeClass("form-item--error");
          var inputValue = element.val();
          if( inputValue == "" ) {
            element.next('p.form-item--error-message').remove();
            //element.after("<p class='form-item--error-message'> "+ label + " field is required.</p>");
            element.parent().addClass("form-item--error");
            //value++;
          } else if(!inputValue.match(phoneregex)) {
            element.next('p.form-item--error-message').remove();
            element.after("<p class='form-item--error-message'>Please enter a valid " + label + ".</p>");
            element.parent().addClass("form-item--error");
            value++;
          }
        }

        //Address field
        if(element.hasClass("validate-address")) {
          $('.contact-form').find(".validate-address").removeClass("form-item--error");
          var inputValue = element.val();
          if( inputValue == "" ) {
            element.next('p.form-item--error-message').remove();
            element.after("<p class='form-item--error-message'> "+ label + " field is required.</p>");
            element.parent().addClass("form-item--error");
            value++;
          } else if(!inputValue.match(address)) {
            element.next('p.form-item--error-message').remove();
            element.after("<p class='form-item--error-message'>Please enter a valid " + label + ".</p>");
            element.parent().addClass("form-item--error");
            value++;
          }
        }

        //City
        if(element.hasClass("validate-city")) {
          $('.contact-form').find(".validate-city").removeClass("form-item--error");
          var inputValue = element.val();
          if( inputValue == "" ) {
            element.next('p.form-item--error-message').remove();
            element.after("<p class='form-item--error-message'> "+ label + " field is required.</p>");
            element.parent().addClass("form-item--error");
            value++;
          } else if(!inputValue.match(city)) {
            element.next('p.form-item--error-message').remove();
            element.after("<p class='form-item--error-message'>Please enter a valid " + label + ".</p>");
            element.parent().addClass("form-item--error");
            value++;
          }
        }

        //Zipcode validation
        if(element.hasClass("validate-zipcode")) {
          $('.contact-form').find(".validate-zipcode").removeClass("form-item--error");
          var inputValue = element.val();
          if( inputValue == "" ) {
            element.next('p.form-item--error-message').remove();
            element.after("<p class='form-item--error-message'> "+ label + " field is required.</p>");
            element.parent().addClass("form-item--error");
            value++;
          } else if(!inputValue.match(zip_code)) {
            element.next('p.form-item--error-message').remove();
            element.after("<p class='form-item--error-message'>Please enter a valid " + label + ".</p>");
            element.parent().addClass("form-item--error");
            value++;
          }
        }
        return value;
      },

      FormValidation: function(Form) {
        var value = 0,
            regex = Drupal.behaviors.commonvalidationfunction.emailidregex(),
            //phone_number = /^(?:(?:\+?1\s*(?:[.-]\s*)?)?(?:\(\s*([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9])\s*\)|([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9]))\s*(?:[.-]\s*)?)?([2-9]1[02-9]|[2-9][02-9]1|[2-9][02-9]{2})\s*(?:[.-]\s*)?([0-9]{4})(?:\s*(?:#|x\.?|ext\.?|extension)\s*(\d+))?$/i,
            phone_number = Drupal.behaviors.commonvalidationfunction.phonenumberregex(),
            zip_code = Drupal.behaviors.commonvalidationfunction.zipcodevalidation(),
            alphanumeric = /^[a-zA-Z'.\s]{1,40}$/,
            pass_regex = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{8,15}$/,
            name = Drupal.behaviors.commonvalidationfunction.usernameregex(),
            address = Drupal.behaviors.commonvalidationfunction.addressreg(),
            upc = /^(\d{12})/,
            date_val_counter = 0;
        var date_fields = ['purchase-date-valid', 'date-of-birth-valid', 'date-of-manuf-valid'];

        return $(Form).find(".form-item--error-message").remove(),
          $(Form).find(".required, .validate-email").parent().removeClass("form-item--error"),


          $(Form).find(".required:visible").not(".form-radio, .shipping_option .required").each(function() {
            var inputValue = $(this).val(),
              label = $($("label[for='" + this.id + "']").contents().get(0)).text();
            "" != inputValue && "_none" != inputValue && 0 != inputValue || (value++, $(this).parent().addClass("form-item--error"),
              $(this).after("<p class='form-item--error-message'>" + label + " field is required.</p>"));

              if($('.checkboxes--wrapper').hasClass('required')) {
                $('.checkboxes--wrapper').next('p.form-item--error-message').remove();
                if(!$('.form-checkbox').is(":checked")) {
                  $('.checkboxes--wrapper').next('p.form-item--error-message').remove();
                  var error_message = $('.checkboxes--wrapper').attr('data-webform-required-error');
                  $('.checkboxes--wrapper').after('<p class="form-item--error-message">'+ error_message + '</p>');

                  value++;

                } else {
                  $('.checkboxes--wrapper').next('p.form-item--error-message').remove();
                  $(this).parent().removeClass('form-item--error');

                  value--;
                }
              }

          }),

          $(Form).find(".pass_field").each(function() {
            var inputValue1 = $(this).val();
            pass_regex.test(inputValue1) || "" == inputValue1 || (value++, $(this).after("<p class='form-item--error-message password-validation'>Password should have atleast minimum 8 characters, 1 digit and 1 special character and 1 capital letter.</p>"),
              $(this).parent().addClass("form-item--error"));
          }),
          $(Form).find(".confirm_pass").each(function() {
            var passval = $(Form).find(".pass_field").val();
            var inputValue2 = $(this).val();
            if (passval != inputValue2) {
              (value++,$(this).after("<p class='form-item--error-message password-validation'>Your password and confirm password do not match</p>"),
                $(this).parent().addClass("form-item--error"));
            }
          }),

          $(Form).find(".validate-phone").each(function() {
            $(Form).find(".validate-phone").removeClass("form-item--error");
            var inputValue = $(this).val();
            $($("label[for='" + this.id + "']").contents().get(0)).text();
            "" == inputValue || inputValue.match(phone_number) || (value++, $(this).after("<p class='form-item--error-message'>Please enter a valid 10 digit phone number(xxx-xxx-xxxx).</p>"),
              $(this).parent().addClass("form-item--error"));
          }),
          $(Form).find(".validate-name").each(function() {
            $(Form).find(".validate-name").removeClass("form-item--error");
            var inputValue = $(this).val();
            $($("label[for='" + this.id + "']").contents().get(0)).text();
            "" == inputValue || inputValue.match(name) || (value++, $(this).after("<p class='form-item--error-message'>Please enter a valid Name.</p>"),
              $(this).parent().addClass("form-item--error"));
          }),

          $(Form).find(".validate-city").each(function() {
            $(Form).find(".validate-city").removeClass("form-item--error");
            var inputValue = $(this).val();
            $($("label[for='" + this.id + "']").contents().get(0)).text();
            "" == inputValue || inputValue.match(name) || (value++, $(this).after("<p class='form-item--error-message'>Please enter a valid City.</p>"),
              $(this).parent().addClass("form-item--error"));
          }),

          $(Form).find(".validate-address").each(function() {
            $(Form).find(".validate-address").removeClass("form-item--error");
            var inputValue = $(this).val();
            $($("label[for='" + this.id + "']").contents().get(0)).text();
            "" == inputValue || inputValue.match(address) || (value++, $(this).after("<p class='form-item--error-message'>Please enter a valid Address.</p>"),
              $(this).parent().addClass("form-item--error"));
          }),

          $(Form).find(".validate-zipcode").each(function() {
            $(Form).find(".validate-zipcode").removeClass("form-item--error");
            var inputValue = $(this).val();
            $($("label[for='" + this.id + "']").contents().get(0)).text();
            "" == inputValue || inputValue.match(zip_code) || (value++, $(this).after("<p class='form-item--error-message'>Zip code field is not valid.</p>"),
              $(this).parent().addClass("form-item--error"));
          }),
          $(Form).find(".validate-email").each(function() {
            var inputValue = $(this).val();
            regex.test(inputValue) || "" == inputValue || (value++, $(this).after("<p class='form-item--error-message'>Email field is not valid.</p>"),
              $(this).parent().addClass("form-item--error"));
          }),
          $(Form).find(".validate--conf-email").each(function() {
            var emailval = $(Form).find(".validate-email").val();
            var inputValue = $(this).val();
            if (emailval != inputValue) {
              $(this).after("<p class='form-item--error-message'>This field should be equal to Email Address.</p>"),
                $(this).parent().addClass("form-item--error");
            }

          }),
          $.each( date_fields, function( index, date_class ){
            date_val_counter = 0;
            label_text = "";
            $('.webform-submission-form').find(".form-select."+date_class).each(function(loop) {
              $(this).parent().find('.js-form-item').removeClass("form-item--error");
              $('.date_formate.'+date_class).find(".form-item--error-message").remove();
              label_text = $('.form-item__label.'+date_class).find('span').text();
              if( $(this).val() == "") {
                date_val_counter = date_val_counter + 1;
              }
              if( loop ==  ( $(".form-select."+date_class).length - 1 )) {
                  if ( date_val_counter != $(".form-select."+date_class).length && date_val_counter > 0) { console.log(".container-inline."+date_class);
                    $(".container-inline."+date_class).after("<p class='form-item--error-message'> Please enter valid " +  label_text + "</p>");
                    $('.webform-submission-consumer-registration-form .webform-button--submit, .webform-submission-product-registration-form .webform-button--submit, .webform-submission-contact-us-form .webform-button--submit').prop('disabled', true);
                    $(this).parent().find('.js-form-item').addClass("form-item--error");
                    value++;
                  }
              }
            });
          }),
          $(Form).find(".upc").each(function() {
            $(Form).find(".upc").removeClass("form-item--error");
            var inputValue = $(this).val();
            $($("label[for='" + this.id + "']").contents().get(0)).text();
            "" == inputValue || inputValue.match(upc) || (value++, $(this).after("<p class='form-item--error-message'>UPC should be 12 digits</p>"),
              $(this).parent().addClass("form-item--error"));
          }),
          !(value > 0);
      },

      ScrollTopPosition: function(ScrollPosition) {
        $("html, body").animate({
          scrollTop: ($(ScrollPosition).offset() || {
            "top": NaN
          }).top - 200
        }, 500);
      },


      UserRegister: function() {
        $(".webform-submission-consumer-registration-form .webform-button--submit").click(function(e) {
          var Form = $(".webform-submission-consumer-registration-form"),
            FormField = brizo.commonFunctions.FormValidation(Form);
          return FormField || brizo.commonFunctions.ScrollTopPosition($(Form).find(".form-item--error:first")),
            FormField;
        });
      },

      VettisWaitlist: function() {
        $(".webform-submission-vettis-registration-form .webform-button--submit").click(function(e) {
          var Form = $(".webform-submission-vettis-registration-form"),
            FormField = brizo.commonFunctions.FormValidation(Form);
          return FormField || brizo.commonFunctions.ScrollTopPosition($(Form).find(".form-item--error:first")),
            FormField;
        });
      },

      // ProductRegistration: function() {
      //   $(".webform-submission-product-registration-form .webform-button--submit").click(function(e) {
      //     var Form = $(".webform-submission-product-registration-form"),
      //       FormField = brizo.commonFunctions.FormValidation(Form);
      //     return FormField || brizo.commonFunctions.ScrollTopPosition($(Form).find(".form-item--error:first")),
      //       FormField;
      //   });
      // },

      // ContactUS: function() {
      //   $(".webform-submission-contact-us-form .webform-button--submit").click(function(e) {
      //     var Form = $(".webform-submission-contact-us-form"),
      //     FormField = brizo.commonFunctions.FormValidation(Form);
      //     return FormField || brizo.commonFunctions.ScrollTopPosition($(Form).find(".form-item--error:first")),
      //       FormField;
      //   });
      // },

      /* Newsletter: function() {
        $(".contact-form-newsletter .webform-button--submit").click(function(e) {
          var Form = $(".contact-form-newsletter"),
            FormField = brizo.commonFunctions.FormValidation(Form);
          return FormField || brizo.commonFunctions.ScrollTopPosition($(Form).find(".form-item--error:first")),
            FormField;
        });
      }, */


      init: function() {
        brizo.commonFunctions.UserRegister(),
        brizo.commonFunctions.VettisWaitlist()
        //brizo.commonFunctions.ProductRegistration(),
        //brizo.commonFunctions.ContactUS(),
        //brizo.commonFunctions.Newsletter()
      }
    };

  })(jQuery, Drupal);


  $(document).ready(function() {
    brizo.commonFunctions.init();
    if(jQuery('.design-gallery-body .design-gallery-general-form').length > 0 ) {
      jQuery("#edit-roomselect").val("").change();
    }
    if(jQuery('.kitchen-design-gallery .design-gallery-general-form').length > 0 ) {
      jQuery("#edit-roomselect").val("Kitchen").change();
    }
    if(jQuery('.bath-design-gallery .design-gallery-general-form').length > 0 ) {
      jQuery("#edit-roomselect").val("Bath").change();
    }
    // contatc us form date field
    $("#edit-purchase-date-year, #edit-purchase-date-day, #edit-purchase-date-month").on('change', function(){

     $("#edit-purchase-date .form-item").removeClass("form-item--error");
        var d = new Date(),
        n = d.getMonth() + 1,
        y = d.getFullYear();
        date =  d.getDate();
        $("#edit-purchase-date-year, #edit-purchase-date-day, #edit-purchase-date-month").removeClass('required');
        today_date =   ("0" + (d.getMonth() + 1)).slice(-2) +"/" + date +"/" + y;
        label_text = $(".date_formate .field-prefix").text();
        $(".date_formate .form-item--error-message").remove();


        if ( y == $("#edit-purchase-date-year").val() ) {
          selected_date = $("#edit-purchase-date-day").val();
          selected_month = $("#edit-purchase-date-month").val();

          if(selected_month > n) {
            $("#edit-purchase-date").after("<p class='form-item--error-message'>" +  label_text + " must be on or before " + today_date +"</p>");
            $(this).parent().addClass("form-item--error");
            $('.webform-submission-consumer-registration-form .webform-button--submit').prop('disabled', true);
            $("#edit-purchase-date-year, #edit-purchase-date-day, #edit-purchase-date-month").addClass('required');

          }
          else if( selected_date > date && selected_month == n) {
            $("#edit-purchase-date").after("<p class='form-item--error-message'>" +  label_text + " must be on or before " + today_date +"</p>");
            $(this).parent().addClass("form-item--error");
            $("#edit-purchase-date-year, #edit-purchase-date-day, #edit-purchase-date-month").addClass('required');
            $('.webform-submission-consumer-registration-form .webform-button--submit').prop('disabled', true);
          }
          else {
            $("#edit-purchase-date-year, #edit-purchase-date-day, #edit-purchase-date-month").removeClass('required');

              if($('.webform-submission-consumer-registration-form .required, .webform-submission-product-registration-form .required, .webform-submission-contact-us-form .required').length === $('.valid').length)  {
                $('.webform-submission-consumer-registration-form .webform-button--submit').prop('disabled', false);
              }

          }
        }
        else {
          $("#edit-purchase-date-year, #edit-purchase-date-day, #edit-purchase-date-month").removeClass('required');

            if($('.webform-submission-consumer-registration-form .required, .webform-submission-product-registration-form .required, .webform-submission-contact-us-form .required').length === $('.valid').length)  {
              $('.webform-submission-consumer-registration-form .webform-button--submit').prop('disabled', false);
            }
        }
    });

    // product registeration
    // date of birth
    $("#edit-date-of-birth-month, #edit-date-of-birth-day, #edit-date-of-birth-year").on('change', function(){

     $(".date-of-birth-valid .form-item").removeClass("form-item--error");
        var d = new Date();
        var n = d.getMonth() + 1;
        var y = d.getFullYear();
        date =  d.getDate();
        $("#edit-date-of-birth-month, #edit-date-of-birth-day, #edit-date-of-birth-year").removeClass('required');
        today_date =   ("0" + (d.getMonth() + 1)).slice(-2) +"/" + date +"/" + y;
        label_text = $(".date-of-birth-valid .field-prefix").text();
        $(".date-of-birth-valid .form-item--error-message").remove();


        if ( y == $("#edit-date-of-birth-year").val() ) {
          selected_date = $("#edit-date-of-birth-day").val();
          selected_month = $("#edit-date-of-birth-month").val();

          if(selected_month > n) {
            $("#edit-date-of-birth").after("<p class='form-item--error-message'>" +  label_text + " must be on or before " + today_date +"</p>");
            $(this).parent().addClass("form-item--error");
            $('.webform-submission-consumer-registration-form .webform-button--submit').prop('disabled', true);
              $("#edit-date-of-birth-month, #edit-date-of-birth-day, #edit-date-of-birth-year").addClass('required');

          }
          else if( selected_date > date && selected_month == n) {
            $("#edit-date-of-birth").after("<p class='form-item--error-message'>" +  label_text + " must be on or before " + today_date +"</p>");
            $(this).parent().addClass("form-item--error");
              $("#edit-date-of-birth-month, #edit-date-of-birth-day, #edit-date-of-birth-year").addClass('required');
            $('.webform-submission-consumer-registration-form .webform-button--submit').prop('disabled', true);
          }
          else {
            $("#edit-date-of-birth-month, #edit-date-of-birth-day, #edit-date-of-birth-year").removeClass('required');

              if($('.webform-submission-consumer-registration-form .required, .webform-submission-product-registration-form .required, .webform-submission-contact-us-form .required').length === $('.valid').length)  {
                $('.webform-submission-consumer-registration-form .webform-button--submit, .webform-submission-product-registration-form .webform-button--submit').prop('disabled', false);
              }

          }
        }
        else {
          $("#edit-date-of-birth-month, #edit-date-of-birth-day, #edit-date-of-birth-year").removeClass('required');

            if($('.webform-submission-consumer-registration-form .required, .webform-submission-product-registration-form .required, .webform-submission-contact-us-form .required').length === $('.valid').length)  {
              $('.webform-submission-consumer-registration-form .webform-button--submit').prop('disabled', false);
            }
        }
    });
    // product registeration
    // date of Manufacture
    $("#edit-date-of-manufacture-if-the-manufacturer-s-code-does-not-appear-o-month, #edit-date-of-manufacture-if-the-manufacturer-s-code-does-not-appear-o-day, #edit-date-of-manufacture-if-the-manufacturer-s-code-does-not-appear-o-year").on('change', function(){

     $(".date-of-manuf-valid .form-item").removeClass("form-item--error");
        var d = new Date();
        var n = d.getMonth() + 1;
        var y = d.getFullYear();
        date =  d.getDate();
        $("#edit-date-of-manufacture-if-the-manufacturer-s-code-does-not-appear-o-month, #edit-date-of-manufacture-if-the-manufacturer-s-code-does-not-appear-o-day, #edit-date-of-manufacture-if-the-manufacturer-s-code-does-not-appear-o-year").removeClass('required');
        today_date =   ("0" + (d.getMonth() + 1)).slice(-2) +"/" + date +"/" + y;
        label_text = $(".date-of-manuf-valid .field-prefix").text();
        $(".date-of-manuf-valid .form-item--error-message").remove();


        if ( y == $("#edit-date-of-manufacture-if-the-manufacturer-s-code-does-not-appear-o-year").val() ) {
          selected_date = $("#edit-date-of-manufacture-if-the-manufacturer-s-code-does-not-appear-o-day").val();
          selected_month = $("#edit-date-of-manufacture-if-the-manufacturer-s-code-does-not-appear-o-month").val();

          if(selected_month > n) {
            $("#edit-date-of-manufacture-if-the-manufacturer-s-code-does-not-appear-o").after("<p class='form-item--error-message'>" +  label_text + " must be on or before " + today_date +"</p>");
            $(this).parent().addClass("form-item--error");
            $('.webform-submission-consumer-registration-form .webform-button--submit').prop('disabled', true);
            $("#edit-date-of-manufacture-if-the-manufacturer-s-code-does-not-appear-o-month, #edit-date-of-manufacture-if-the-manufacturer-s-code-does-not-appear-o-day, #edit-date-of-manufacture-if-the-manufacturer-s-code-does-not-appear-o-year").addClass('required');

          }
          else if( selected_date > date && selected_month == n) {
            $("#edit-date-of-manufacture-if-the-manufacturer-s-code-does-not-appear-o").after("<p class='form-item--error-message'>" +  label_text + " must be on or before " + today_date +"</p>");
            $(this).parent().addClass("form-item--error");
            $("#edit-date-of-manufacture-if-the-manufacturer-s-code-does-not-appear-o-month, #edit-date-of-manufacture-if-the-manufacturer-s-code-does-not-appear-o-day, #edit-date-of-manufacture-if-the-manufacturer-s-code-does-not-appear-o-year").addClass('required');
            $('.webform-submission-consumer-registration-form .webform-button--submit').prop('disabled', true);
          }
          else {
            $("#edit-date-of-manufacture-if-the-manufacturer-s-code-does-not-appear-o-month, #edit-date-of-manufacture-if-the-manufacturer-s-code-does-not-appear-o-day, #edit-date-of-manufacture-if-the-manufacturer-s-code-does-not-appear-o-year").removeClass('required');

              if($('.webform-submission-consumer-registration-form .required, .webform-submission-product-registration-form .required, .webform-submission-contact-us-form .required').length === $('.valid').length)  {
                $('.webform-submission-consumer-registration-form .webform-button--submit').prop('disabled', false);
              }
          }
        }
        else {
          $("#edit-date-of-manufacture-if-the-manufacturer-s-code-does-not-appear-o-month, #edit-date-of-manufacture-if-the-manufacturer-s-code-does-not-appear-o-day, #edit-date-of-manufacture-if-the-manufacturer-s-code-does-not-appear-o-year").removeClass('required');

            if($('.webform-submission-consumer-registration-form .required, .webform-submission-product-registration-form .required, .webform-submission-contact-us-form .required').length === $('.valid').length)  {
              $('.webform-submission-consumer-registration-form .webform-button--submit, .webform-submission-product-registration-form .webform-button--submit').prop('disabled', false);
            }
        }
    });

    // product registeration
    // date of purchase
    $("#edit-date-of-purchase-month, #edit-date-of-purchase-day, #edit-date-of-purchase-year").on('change', function(){

     $(".purchase-date-valid .form-item").removeClass("form-item--error");
        var d = new Date();
        var n = d.getMonth() + 1;
        var y = d.getFullYear();
        date =  d.getDate();
        $("#edit-date-of-purchase-month, #edit-date-of-purchase-day, #edit-date-of-purchase-year").removeClass('required');
        today_date =   ("0" + (d.getMonth() + 1)).slice(-2) +"/" + date +"/" + y;
        label_text = $(".purchase-date-valid .field-prefix").text();
        $(".purchase-date-valid .form-item--error-message").remove();


        if ( y == $("#edit-date-of-purchase-year").val() ) {
          selected_date = $("#edit-date-of-purchase-day").val();
          selected_month = $("#edit-date-of-purchase-month").val();

          if(selected_month > n) {
            $("#edit-date-of-purchase").after("<p class='form-item--error-message'>" +  label_text + " must be on or before " + today_date +"</p>");
            $(this).parent().addClass("form-item--error");
            $('.webform-submission-consumer-registration-form .webform-button--submit').prop('disabled', true);
            $("#edit-date-of-purchase-month, #edit-date-of-purchase-day, #edit-date-of-purchase-year").addClass('required');

          }
          else if( selected_date > date && selected_month == n) {
            $("#edit-date-of-purchase").after("<p class='form-item--error-message'>" +  label_text + " must be on or before " + today_date +"</p>");
            $(this).parent().addClass("form-item--error");
            $("#edit-date-of-purchase-month, #edit-date-of-purchase-day, #edit-date-of-purchase-year").addClass('required');
            $('.webform-submission-consumer-registration-form .webform-button--submit').prop('disabled', true);
          }
          else {
            $("#edit-date-of-purchase-month, #edit-date-of-purchase-day, #edit-date-of-purchase-year").removeClass('required');

              if($('.webform-submission-consumer-registration-form .required, .webform-submission-product-registration-form .required, .webform-submission-contact-us-form .required').length === $('.valid').length)  {
                $('.webform-submission-consumer-registration-form .webform-button--submit').prop('disabled', false);
              }

          }
        }
        else {
          $("#edit-date-of-purchase-month, #edit-date-of-purchase-day, #edit-date-of-purchase-year").removeClass('required');

            if($('.webform-submission-consumer-registration-form .required, .webform-submission-product-registration-form .required, .webform-submission-contact-us-form .required').length === $('.valid').length)  {
              $('.webform-submission-consumer-registration-form .webform-button--submit').prop('disabled', false);
            }
        }
    });

    //Product Registration
    $('.product-details-container').find('input').addClass('form-item__textfield');


    // Select Box overlap issue Fix
    var ua = window.navigator.userAgent;
    var isIE = /MSIE|Trident/.test(ua);
    if (isIE) {
      jQuery('.is-filled').each(function() {
        if (jQuery(this).find('.form-item__textfield').val() == "") {
          jQuery(this).removeClass('is-filled');
        }
        if (jQuery(this).find('.form-item__select').val() == "") {
          jQuery(this).removeClass('is-filled');
        }
      });

      jQuery('.form-item__textfield').on("keyup change input paste dp.change click check", function() {
        if (jQuery(this).val() == "") {
          jQuery(this).parent('.form-item').removeClass('is-filled');
        }else{
          jQuery(this).parent('.form-item').addClass('is-filled');
        }
      });

    }

    // $('.form-item__select').parents('.form-item').find('label').hide();
    $('.form-item__select').on('change', function() {
      if ($(this).val() != "") {
        $(this).parents('.form-item').find('label').show();
        $(this).parents('.form-item').addClass('is-filled');
      } else {
        $(this).parents('.form-item').find('label').hide();
        $(this).parents('.form-item').removeClass('is-filled');
      }
    });

    $('.webform-submission-consumer-registration-form .webform-button--submit').prop('disabled', true);

    $('.webform-submission-consumer-registration-form .required').on("keyup change input paste dp.change click check", function() {
      $(this).val() != "" ? jQuery(this).addClass('valid') : jQuery(this).removeClass('valid');
      if($('.webform-submission-consumer-registration-form .required').length === jQuery('.valid').length)  {
        formFlag = 1;
        if(captchaFlag === 1 ) {
         $('.webform-submission-consumer-registration-form .webform-button--submit').prop('disabled', false);
        }
      }else {
        formFlag = 0;
        $('.webform-submission-consumer-registration-form .webform-button--submit').prop('disabled', true);
      }
    });

    $('.webform-submission-consumer-registration-form .form-item__textfield, .webform-submission-vettis-registration-form .form-item__textfield, .webform-submission-vettis-registration-form .form-item__select, .webform-submission-contact-us-form .form-item__textfield, .webform-submission-contact-us-form .form-file, .webform-submission-contact-us-form .form-item__select, .webform-submission-email-updates-form .form-item__textfield').on("keyup change input paste dp.change", function(e) {
      $(this).val() != "" ? jQuery('.webform-button--reset').prop('disabled', false) : jQuery('.webform-button--reset').prop('disabled', true);
    });

    $('.webform-submission-consumer-registration-form .webform-button--reset, .webform-submission-vettis-registration-form .webform-button--reset, .webform-submission-contact-us-form .webform-button--reset, .webform-submission-email-updates-form .webform-button--reset').click(function(e){
      e.preventDefault();
      $(this).closest('form').get(0).reset();
      $('.webform-submission-consumer-registration-form .is-filled, .webform-submission-product-registration-form .is-filled, .webform-submission-vettis-registration-form .is-filled, .webform-submission-contact-us-form .is-filled, .webform-submission-email-updates-form .is-filled').removeClass('is-filled');
      $('.webform-submission-consumer-registration-form , .webform-submission-product-registration-form , .webform-submission-vettis-registration-form , .webform-submission-contact-us-form, .webform-submission-email-updates-form').find(".form-item--error-message, .messages--error").remove();
      $('.webform-submission-consumer-registration-form .form-item--error, .webform-submission-product-registration-form .form-item--error, .webform-submission-vettis-registration-form .form-item--error, .webform-submission-contact-us-form .form-item--error, .webform-submission-email-updates-form .form-item--error').removeClass('form-item--error');
      $(this).prop('disabled', true);
      $('.webform-submission-consumer-registration-form .webform-button--submit, .webform-submission-product-registration-form .webform-button--submit').prop('disabled', true);
      $('.form-item__select').parents('.form-item').find('label').hide();
    });

    $(".webform-submission-vettis-registration-form .webform-button--reset").click(function(e){
      e.preventDefault();
    });

    $('.webform-submission-contact-us-form .webform-button--reset').click(function(){
      $(".webform-submission-contact-us-form input[value='Remove']").trigger("mousedown");
    });


    if ( $(".customshower-page").length > 0 ) {
      href_path = $(".banner .link").attr('href');
      segment = href_path.split("/");
      getPath = segment[segment.length-1];
      if(getPath.indexOf('pdf') != -1) {
        $(".banner .link").attr('target', "_blank")
      }
    }

    if($(".page-banner-wrapper .banner").length) {
      //$(".breadcrumb").insertBefore(".page-banner-wrapper .banner .banner__wrapper picture");
    } else {
      //$("#block-brizo-theme-breadcrumbs").appendTo(".page-banner-wrapper");
    }

    $(document).on("click", ".header-search-wrapper .search-label", function(){
      $(".header-serch-input").show();
      $(".header-serch-input").addClass("active-header-serch-input");
      $(".header-search-wrapper .search-label").hide();
      $(".header-search-wrapper .search-input-wrapper").addClass("active-search-input-wrapper");
      $(".header-serch-input").focus();
    });

    $(document).on("click ontouchstart", ".search-input-wrapper .icon-menu-close", function(){
      $(".header-serch-input").removeClass("active-header-serch-input");
      $('.mobile-search-active .header-serch-input').val("");
      $(".header-search-wrapper").removeClass("mobile-search-active");
      $(".header-search-wrapper .search-label").show();
      $(".header-serch-input").hide();
      $(".header-search-wrapper .search-input-wrapper").removeClass("active-search-input-wrapper");
      $("#ui-id-1").removeClass("mobile-active-list");
      $(".dialog-off-canvas-main-canvas").removeClass("no-scroll");
    });

    $(".mobile-search-icon").click(function() {
      $(".header-search-wrapper").addClass("mobile-search-active");
      $(".header-serch-input").focus();
      $("#main-nav").removeClass("main-nav--open");
      $(".utility-mobile").removeClass("active-utility-mobile");
      $(".toggle-expand").removeClass("toggle-expand--open");


      var timer = null;
      $('.mobile-search-active .header-serch-input').on("paste keyup", function(){
        clearTimeout(timer);
        timer = setTimeout(stickSearchList, 2000);
      });

      function stickSearchList() {
        $("#ui-id-1").addClass("mobile-active-list");
        $(".dialog-off-canvas-main-canvas").addClass("no-scroll");

        if ($('.mobile-search-active .header-serch-input').val() == "") {
          $("#ui-id-1").removeClass("mobile-active-list");
          return false;
        }
      }

    });

    $('.ui-autocomplete').on( "click", ".autocomplete-view-all", function(e) {
      e.preventDefault();
      var url = window.location.origin;
      $redirect = $(this).attr("href");
      var redirectUrl = url+$redirect;
      $(this).closest('li').remove();
      window.location.replace(redirectUrl);
    });

    jQuery(".a2a_label.a2a_localize").click(function(){
      //jQuery("#a2apage_dropdown").toggleClass("active-a2apage_dropdown");
    });

    if($('.cdp-block-slider').length){

      resizeWindow();
      window.addEventListener('resize', resizeWindow);
      function resizeWindow(){
        $('.cdp-block-slider').slick({
          responsive: [
            {
              breakpoint: 2500,
              settings: "unslick"
            },
            {
              breakpoint: 920,
              settings: {
                dots: true
              }
            }
          ]
        });
      }
    }

    $('.main').once().on('click', '.back-to-top', function(){
      $("html, body").animate({
        scrollTop: ($(".design-gallery-filter").offset() || {
          "top": NaN
        }).top - 200
      }, 500);
    });

    EqualHeight();
    //deviceclick();
    $(".popup").click(function(e){
    	e.preventDefault();
    	var target = $(this).attr("target");
    	if(target.length > 1) {
    		$("."+target).trigger("click");
    	}
    });

  });


  $( document ).ajaxComplete(function( event, xhr, settings ) {
    var ajax_url = settings.url;
      if(ajax_url.indexOf('/customer-support/contact-us') != -1) {
        triggeredElement = settings.extraData._triggering_element_name;
        if( triggeredElement.indexOf('upload_button') !== -1 ){
        targetElement = triggeredElement.replace('upload_button', 'remove_button');
        file_name = $( "input[name*='"+ targetElement +"']" ).parent().find('.file').text();
         //$( "input[name*='"+ targetElement +"']" ).parent().find('.file').remove()
        $( "input[name*='"+ targetElement +"']" ).parent().parent().after('<div class="file-upload-url">'+ file_name + '</div>');

        }

      }
  });

  $( document ).ready(function() {
    if(jQuery('.recently_viewed_slider').length){

      console.log("in slider")
      var $carousel = $('.recently_viewed_slider');

      var settings = {

        slidesToShow: 2,
        slidesToScroll: 1,
        mobileFirst: true,
        arrows: true,
        dots: false,
        slide: '.recently_viewed__slide',
        slidesToShow: 2,
        centerMode: true,
        centerPadding: '100px',

        responsive: [
            {
                breakpoint: 920,
                settings: 'unslick'
            }
        ]
      };

      console.log("in slider setting")

      function setSlideVisibility() {
        //Find the visible slides i.e. where aria-hidden="false"
        var visibleSlides = $carousel.find('.recently_viewed__slide[aria-hidden="false"]');
        //Make sure all of the visible slides have an opacity of 1
        $(visibleSlides).each(function() {
          $(this).css('opacity', 1);
        });

        //Set the opacity of the first and last partial slides.
        $(visibleSlides).first().prev().css('opacity', 0);
        $(visibleSlides).prev().css('opacity', 1);
      }

      $carousel.slick(settings);
      $carousel.slick('slickGoTo', 1);
      setSlideVisibility();

      $carousel.on('afterChange', function() {
        setSlideVisibility();
      });
    }

    $(window).on('resize', function() {
      $('.recently_viewed_slider').slick('resize');
    });

    $('.modal-body .section-lead-in').addClass('margin-top-0');

  });


  $(window).on('resize', function(e) {
    if ($(window).width() > 999) {
      collectionImage();
    }
  });

  if ($(window).width() > 999) {
    collectionImage();
  }

  function collectionImage() {
    $(document).on('mouseover','.main-nav .open-main-menu > .sub-data > .main-menu__item:nth-child(2) .main-menu--sub-2 .main-menu__item',function() {
      $(this).each(function() {
        if($(this).find('.main-menu__link')[0].hasAttribute("img-src")) {
          var image_src = $(this).find('.main-menu__link').attr('img-src');
          var collection_name = $(this).find('.main-menu__link').text();
          var image_alt = $(this).find('.main-menu__link').attr('img-alt');
          $(this).parent().next('li.collection_image').remove();
          $(this).parent().next('p.collection_name').remove();
          $(this).parent().after('<li class="collection_image" style="transition-delay: 5s"><img src=' + image_src + ' alt = '+ image_alt + '> <p class="collection_name">'+ collection_name + '</p></li>');
        }else {
          if($(this).parent().next().hasClass('collection_image')) {
            $(this).parent().next('li.collection_image').remove();
          }
        }
      })
    });
  }

  $(window).on('resize', function(e) {
    if ($(window).width() > 999) {
      collectionImage();
    }
  });

  if ($(window).width() > 999) {
    collectionImage();
  }

  function collectionImage() {
    $(document).on('mouseover','.main-nav .open-main-menu > .sub-data > .main-menu__item:nth-child(2) .main-menu--sub-2 .main-menu__item',function() {
      $(this).each(function() {
        if($(this).find('.main-menu__link')[0].hasAttribute("img-src")) {
          var image_src = $(this).find('.main-menu__link').attr('img-src');
          var collection_name = $(this).find('.main-menu__link').text();
          var image_alt = $(this).find('.main-menu__link').attr('img-alt');
          $(this).parent().next('li.collection_image').remove();
          $(this).parent().next('p.collection_name').remove();
          $(this).parent().after('<li class="collection_image" style="transition-delay: 5s"><img src=' + image_src + ' alt = '+ image_alt + '> <p class="collection_name">'+ collection_name + '</p></li>');
        }else {
          if($(this).parent().next().hasClass('collection_image')) {
            $(this).parent().next('li.collection_image').remove();
          }
        }
      })
    });
  }

  var countryLabel = jQuery(".active-country").text();
  var selectedCountry = countryLabel.substr(0, jQuery(".active-country").text().indexOf('-'));
  jQuery(".country-dd-title .active-country-title").html(selectedCountry);




})(jQuery, Drupal);

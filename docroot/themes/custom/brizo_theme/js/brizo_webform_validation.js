(function ($, Drupal) {

  Drupal.behaviors.vettis_form_validation = {
    attach: function (context, drupalSettings) {

      // webform  vettis_form_validation
    if ($("body.vettis-waitlist").length  > 0  || $("body.vettis-registration").length > 0) {
      $('#edit-number-of-faucets').keypress(function () {
          return event.charCode >= 48 && event.charCode <= 57
      });
      $("#edit-number-of-faucets").keydown(function () {
        // Save old value.
        if (!$(this).val() || (parseInt($(this).val()) <= 4 && parseInt($(this).val()) >= 1))
        $(this).data("old", $(this).val());
      });
      $("#edit-number-of-faucets").keyup(function () {
          // Check correct, else revert back to old value.
          if (!$(this).val() || (parseInt($(this).val()) <= 4 && parseInt($(this).val()) >= 1))
            {
              var validated = 1;
            }
          else {
            $(this).val($(this).data("old"));
          }

      });
      // hide dependent field on reset.
      $("#edit-actions-reset").click(function(){
        $(".webform-submission-vettis-registration-form :input").each(function(){
        var inText = $(this).attr('data-drupal-states');
        if( inText !='' && inText != undefined) {
          if(inText.indexOf('i_am_a_showroom_associate_ordering_for_a_customer_') != -1){
          $(this).parent('div').hide();
          }
        }
        });
        $("#edit-i-am-a-showroom-associate-ordering-for-a-customer-").change(function(){
          if($("#edit-i-am-a-showroom-associate-ordering-for-a-customer-").is(':checked')){
            $(".webform-submission-vettis-registration-form :input").each(function(){
            var inText = $(this).attr('data-drupal-states');
            if( inText !='' && inText != undefined) {
              if(inText.indexOf('i_am_a_showroom_associate_ordering_for_a_customer_') != -1){
              $(this).parent('div').show();
              }
            }
            });
          }
       });
      });

    }

    /* EOF webform limitation*/
    }
  };
})(jQuery, Drupal);

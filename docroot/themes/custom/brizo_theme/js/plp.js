(function ($, Drupal, drupalSettings, Backbone) {

  Drupal.behaviors.plpBehaviour = {
    attach: function (context, settings) {
      let productPerPage = drupalSettings.productPerPage;
      let category = drupalSettings.productCategory;
      let shower_system_filter = drupalSettings.showserSystemFilter;
      let secondaryCategory = drupalSettings.secondaryCategory;
      let productFacets = drupalSettings.productFacets;
      let filterFacets = drupalSettings.availableFacets.facets;
      let defaultFacets = drupalSettings.availableFacets.facets;
      let facetData = drupalSettings.facetData;
      let parentFacets = drupalSettings.facetParent;
      let facetMap = drupalSettings.facetMap;
      let cacheKeyData = drupalSettings.cacheKeyData;
      let facetMultiple = drupalSettings.facetMultiple;
      let categoryCount = drupalSettings.categoryCount;
      let categoryLevel = drupalSettings.categoryLevel;
      let pageNumber = 0;
      let loadProducts = true;
      let lastPage = false;
      let appliedSort = '';
      let defaultSort = 'sort_CollectionsTypeFunctionSubfunction=ASC';
      let loader = $('.ajax-progress-throbber');
      let plpResults = $('.plp-result');
      let accordianHeader = "accordion-title accordion-title-active";
      let filtersFromUrl = 'F';
      let cacheFlag = 'T';
      let defaultModel = '';
      let clearall = 0;
      let preservedFilters = '';
      let preserved_filters = '';
      let pathwithoutQuery = window.location.href.replace(window.location.search, "");
      let filtersforShare = '';
      let availablefilters = [];
      let availablefilterPairs = []; // Contains all the available filters in the PLP page.
      let validFilter = 'F';
      let finishOptions = '';
      let filterChecked = false;
      let displayControlField = 'displayControlKitchen';
      var filterCategory = [];
      if(jQuery.inArray("Brizo_Bath", category) !== -1){
        displayControlField = 'displayControlBath';
      }
      if($( "body" ).hasClass( "shower" ) ) {
        displayControlField = 'displayControlShower';
      }

      let size = $('.form-item__select--im').val();
      if (size == 'items-per-page') {
        size = productPerPage;
      }

      /* Click function for clearing all filters.*/
      $('#clear_filters').click(function(){
        $('.facets-checkbox').prop("checked", false);
        $('.js-filter-chip').remove();
        pageNumber = 0;
        filterCategory = [];
        defaultModel = true;
        applyFacets(defaultModel, 1);
      });

      $('#mobile_clear_filters').click(function(){
        $('.facets-checkbox').prop("checked", false);
        pageNumber = 0;
        filterCategory = [];
        applyFacets(defaultModel, 1);
      });

      jQuery(document).ready(function() {
        pageNavigated();
        if(window.location.search){
          queryString = window.location.search.replace('?','');
          if(queryString.indexOf('=')){
            filterKey = queryString.split('=')[0];
          }
          else{
            filterKey = queryString;
          }

          if((availablefilterPairs && jQuery.inArray(filterKey,availablefilterPairs)) || window.location.search == '?searchFilter=true'){
            validFilter = 'T';
          }
        }
        if(validFilter == 'T'){
          if(window.location.search == '?searchFilter=true'){
            if(localStorage.getItem(pathwithoutQuery)) {
              filterFacets = localStorage.getItem(pathwithoutQuery);
              filtersforShare = localStorage.getItem(pathwithoutQuery).replace(defaultFacets, "").replace(/^&|&$/g,'').replace(/or_|and_/g,'').replace(/\s+/g, '+');
              cacheFlag = 'F';
            }
            else if(localStorage.getItem(window.location.href)){
              filterFacets = localStorage.getItem(window.location.href);
              filtersforShare = localStorage.getItem(window.location.href).replace(defaultFacets, "").replace(/^&|&$/g,'').replace(/or_|and_/g,'').replace(/\s+/g, '+');
              cacheFlag = 'F';
            }
          }
          else {
            let facetinQuery = '';
            facetinQuery = document.location.search.replace('?','').split('&');
            var queryFacetCategory = '';
            var queryFacetValue = '';
            var onetext = '';
            var twotext = '';
            for (var i = 0; i < facetinQuery.length; i++) {
              if (facetinQuery[i] !== "" && facetinQuery[i] !== null) {
                onetext = facetinQuery[i];
                twotext = onetext.split('=');
                if (twotext.length > 1){
                  queryFacetValue = twotext[1].replace('+', ' ');
                  if(availablefilterPairs && jQuery.inArray(facetinQuery[i],availablefilterPairs)){
                    queryFacetCategory = twotext[0];
                    filtersFromUrl = 'T';
                    filterValue = buildFilterParams(queryFacetCategory , queryFacetValue, queryFacetValue, filtersFromUrl, filterChecked);
                    filterFacets += filterValue;
                  }
                }
              }
              filtersforShare = filterFacets.replace(defaultFacets, "").replace(/^&|&$/g,'').replace(/or_|and_/g,'').replace(/\s+/g, '+');
              localStorage.setItem(pathwithoutQuery,filtersforShare);
              cacheFlag = 'F';
            }
          }
        }
        else{
          filterFacets = defaultFacets;
        }
        $('.page-node-type-plp', context).once('getProducts').each(function () {
          apiCallforfilters();
          apiCallforProducts();
        });
      });

      /* API call to get filters */
      function apiCallforfilters() {
        let showOnlyHighestCat = 0;
        if(categoryLevel == 1){
          showOnlyHighestCat = categoryLevel;
        }
        loader.show();
        var pageNumberVal = Number.isInteger(pageNumber) ? pageNumber : 0 ;
        $('.accordion-block').hide();
        $.ajax({
          type: "GET",
          url: "/product-listing",
          dataType: 'json',
          data: {
            size: size,
            category: category,
            secondaryCategory: secondaryCategory,
            productFacets: productFacets,
            categoryCount: categoryCount,
            facets: defaultFacets,
            sort: defaultSort,
            cacheKeyData: cacheKeyData,
            pagenumber: pageNumberVal,
            cache: cacheFlag,
            defaultModel: false,
            categoryLevel: showOnlyHighestCat,
            showerSystemFilter: shower_system_filter,
          },
          error: function (XMLHttpRequest, textStatus, errorThrown) {
            $('.plp-pagination-output').html('');
            $('.plp-result').hide();
            return false;
          },
          success: function (data) {
            $('.plp-result').show();
            $('.accordion-block').show();
            let FacetView = new facetView(data, parentFacets, facetMap);
            if ($.trim($('.accordion-block').text()) == ''){
              $('.product-filter').hide();
            }
            else{
              $('.product-filter').show();
            }
          }
        });
      }
      /* API call to get products */
      function apiCallforProducts() {
        $('.plp-operation').hide();
        if(filterFacets.indexOf('&facetFinish') != -1){
          defaultModel = false;
        }
        else{
          defaultModel = true;
        }
          loadProducts = false;
          plpResults.hide();
          var pageNumberVal = Number.isInteger(pageNumber) ? pageNumber : 0 ;
          $.ajax({
            type: "GET",
            url: "/product-listing",
            dataType: 'json',
            async: false,
            data: {
              size: size,
              category: category,
              secondaryCategory: secondaryCategory,
              productFacets: productFacets,
              categoryCount: categoryCount,
              facets: filterFacets,
              sort: defaultSort,
              cacheKeyData: cacheKeyData,
              pagenumber: pageNumberVal,
              cache: "F",
              defaultModel: defaultModel,
              showerSystemFilter: shower_system_filter,
            },
            success: function (data) {
              $('.plp-result').show();
              $('.product-filter').show();
              if (data.totalElements > 0) {
                $('.plp-operation').show();
                $('.plp-pagination-output').hide();
                $('.form-item__select--im').hide();
                if(data.totalElements > 24){
                  $('.form-item__select--im').show();
                  $('.plp-pagination-output').show();
                }
              }
              let response = new ProdView(data, false);
              if(data){
                data.currentpage = 1;
              }
              let PlppaginationView = new plppaginationView(data);
              lastPage = data.last;
              pageNumber++;
              loadProducts = true;
              let activePage = data.number +1;
              let from = ((activePage-1)*size)+1;
              let to = activePage*size;
              if (to > data.totalElements) {
                to = data.totalElements;
              }
              let resultString = "Results <span>"+from+"-"+to+"</span> of <span>"+data.totalElements + "</span>";
              $('.plp-results-count').html(resultString);
              $('.category-name').html(category);
              $('.loader').hide();
              loader.hide();
              plpResults.show();
              if(data.totalElements == 0) {
                $('.plp-pagination-output').html('');
                $('.plp-result').hide();
              }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
              $('.plp-pagination-output').html('');
              $('.plp-result').hide();
              return false;
            }
          });
        }

      /* On selection of filters corresponding values sent to API. */
      $('.product-filter').once().on('change', '.facets-checkbox', function () {
        filterChecked = $(this).prop('checked');
        filterValue = buildFilterParams($(this).attr('data-filter'), $(this).attr('id'), $(this).attr('value'), filtersFromUrl, filterChecked);
        let filterLabel = $('label[for="' + $(this).attr('id') + '"]').text();
        let filterId = $(this).attr('id');
        let dataFilter = $(this).attr('data-filter');
        var div = document.createElement("div");
	div.innerHTML = filterId;
	var showerPackageType = div.textContent || div.innerText || "";
	showerPackageType = showerPackageType.replace(' and ', '/');
        if (filterChecked == true) {
          filterFacets += filterValue;
          // Save selected filters a variable to be saved in localstorage.
          if(window.location.search == '?searchFilter=true'){
            localStorage.removeItem(window.location.href);
            localStorage.removeItem(window.location.href.replace(window.location.search, ""));
          }
          filterChips(filterLabel, filterId, 1);
          if($(this).attr('data-filter') == 'facetFinish'){
            defaultModel = false;
          }
          else{
            defaultModel = true;
          }
          if (dataFilter = 'shower_packages') {
	    for (var i = 0, length = category.length; i < length; i++) {
	      if(category[i].includes(' '+showerPackageType) == true){
                filterCategory.push(category[i]);
              }
	    }
          }
        }
        else {
          filterFacets = filterFacets.replace(filterValue, "");
          filterChips(filterLabel, filterId, 0);
          if (dataFilter = 'shower_packages') {
	    for (var i = 0, length = filterCategory.length; i < length; i++) {
	      if(!!filterCategory[i]) {
	        if(filterCategory[i].includes(' '+showerPackageType) == true){
                  filterCategory.splice(i,1);
                  for (var j = 0, innerLength = filterCategory.length; j < innerLength; j++) {
                    if(!!filterCategory[j]) {
                      if(filterCategory[j].includes(' '+showerPackageType) == true){
                        filterCategory.splice(j,1);
                      }
                    }
                  }
                }
	      }
	    }
          }
        }
        pageNumber = 0;
        applyFacets(defaultModel, 0, filterCategory);
      });
      /* Reset filters on clicking of filter chips */
      $('.selected-filter').once().on('click', '.filter-chip', function (){
        let filterParam = '';
        let chipId = $(this).attr('id');
        let facetId = chipId.split(/-(.+)?/)[1];
        let facetValue = '';
        let filterValue = '';
        $("a[id='" + chipId + "']").remove();
        $("input[id='" + facetId + "']").prop("checked", false);
        let filterAttrib = $("input[id='" + facetId + "']").attr('data-filter');
        filterParam = $("input[id='" + facetId + "']").attr('data-filter');
        facetValue = $("input[id='" + facetId + "']").val();
        if (filterAttrib == 'categories'){
          filterParam = filterAttrib;
          //facetValue = encodeURIComponent(facetValue);
          if(facetValue.indexOf("&") != -1){
            facetValue = encodeURIComponent(facetValue);
          }
        }
        
        
        if (filterAttrib == 'facetFinish'){
          defaultModel = false;
          filterParam = filterAttrib;
          facetValue = $("input[id='" + facetId + "']").val().toLowerCase();
        }
        if ((filterAttrib == 'facetBrizofeature') || (filterAttrib == 'facetHoleFilter')){
          filterParam = filterAttrib;
          facetValue = $("input[id='" + facetId + "']").val();
        }
        if (filterAttrib == 'collections'){
          filterParam = 'or_' + filterAttrib;
          facetValue = encodeURIComponent(facetValue);
        }
        if (filterAttrib == 'shower_package'){
          filterParam = 'categories';
          //facetValue = encodeURIComponent(facetValue);
          if(facetValue.indexOf("&&") != -1){
            shower_pack_cat = chipId.split(/&&(.+)?/);
            $.each(shower_pack_cat , function(index, val) {
              filterValue  = '&' + filterParam + '=' + val + "&";
            });
          }
        } else {
          filterValue  = '&' + filterParam + '=' + facetValue + "&";
        }

        if(filterAttrib == 'facetNarrowResults') {
          if($(this).attr('id') == 'chip-Include Discontinued Products'){
            filterValue  = '&includeObsolete=true';
          }
          if($(this).attr('id') == 'chip-Include Ferguson Exclusive Products'){
            filterValue  = '&includeFerguson=true';
          }
          if($(this).attr('id') == 'chip-Exclude Coming Soon Products'){
            filterValue  = '&includeFuture=false';
          }
        }
        let updatedFacets = filterFacets.replace(filterValue,'');
        filterFacets = updatedFacets;
        pageNumber = 0;
	if (filterAttrib = 'shower_packages') {
          var div = document.createElement("div");
	  div.innerHTML = facetId;
	  var showerPackageType = div.textContent || div.innerText || "";
	  showerPackageType = showerPackageType.replace(' and ', '/');
          for (var i = 0, length = filterCategory.length; i < length; i++) {
	     if(!!filterCategory[i]) {
	      if(filterCategory[i].includes(' '+showerPackageType) == true){
		 filterCategory.splice(i,1);
		 for (var j = 0, innerLength = filterCategory.length; j < innerLength; j++) {
		   if(!!filterCategory[j]) {
		     if(filterCategory[j].includes(' '+showerPackageType) == true){
		       filterCategory.splice(j,1);
		     }
		   }
		 }
	       }
	     }
	   }
	}
        applyFacets(defaultModel, 0, filterCategory);
      });
      /* Building the query parameters for API call when filters applied. */
      function buildFilterParams(dataFilter, dataId, dataValue, filtersFromUrl, filterChecked) {
        switch (dataFilter) {
          case 'FltHighFlow':
            filterValue = '&' + dataFilter + '=' + dataValue + "&";
            break;
          case 'FltTempAssure':
            filterValue = '&' + dataFilter + '=' + dataValue + "&";
            break;
          case 'FltMonitor':
            filterValue = '&' + dataFilter + '=' + dataValue + "&";
            break;
          case 'categories':
            dataValue = decodeURIComponent(dataValue);
            if(dataValue.indexOf("&") != -1){
              dataValue = encodeURIComponent(dataValue);
            }
            filterValue  = '&' + dataFilter + '=' + dataValue + "&";
            break;
          case 'facetFinish':
            var finishString =  dataValue;
            filterValue  = '&' + dataFilter + '=' + finishString.toLowerCase() + "&";
            break;
          case 'facetBrizoFeature':
          case 'facetHoleFilter':
            filterValue  = '&' + dataFilter + '=' + dataValue + "&";
            break;
          case 'collections':
            if(filtersFromUrl == 'T') {
              if(filterChecked == true){
                filterValue  = '&or_' + dataFilter + '=' + encodeURIComponent(dataValue) + "&";
              }
              else{
                if(window.location.search.indexOf(dataValue) != -1){
                  filterValue  = '&or_' + dataFilter + '=' + dataValue + "&";
                }
                else{
                filterValue  = '&or_' + dataFilter + '=' + encodeURIComponent(dataValue) + "&";
                }
              }
            }
            else{
              filterValue  = '&or_' + dataFilter + '=' + encodeURIComponent(dataValue) + "&";
            }
            break;
          case 'shower_packages':
          filterValue = '';
          break;
          case 'shower_trims_and_roughs':
            //filterValue  = '&' + dataFilter + '=' + dataValue + "&";
            let package_str = '';
            if(dataValue.indexOf("&&") != -1) {
              shower_pack_cat = dataValue.split(/&&/);
              for (var i = 0, length = shower_pack_cat.length; i < length; i++) {
                if(shower_pack_cat[i].toLowerCase().indexOf(shower_system_filter.toLowerCase()) != -1) {
                  if(shower_pack_cat[i].toLowerCase().indexOf("&") != -1){
                    shower_pack_cat[i] = encodeURIComponent(shower_pack_cat[i]);
                  }
                 package_str  += '&' + 'categories' + '=' + shower_pack_cat[i] + "&"; 
                }
                
              }
            } else {
              if(dataValue.indexOf("&") != -1){
                dataValue = encodeURIComponent(dataValue);
              }
              package_str += '&' + 'categories' + '=' + dataValue + "&";
            }
            filterValue = package_str; 
            break;
          case 'FlowRate':
              filterValue  = '&' + dataFilter + '=' + dataValue + "&";
              break;
          case 'facetNarrowResults':
            if(dataId == 'Include Discontinued Products'){
              filterValue  = '&includeObsolete=true';
            }
            if(dataId == 'Include Ferguson Exclusive Products'){
              filterValue  = '&includeFerguson=true';
            }
            if(dataId == 'Exclude Coming Soon Products'){
              filterValue  = '&includeFuture=false';
            }
            break;
          default:
            filterValue  = '&' + dataFilter + '=' + dataId + "&";
        }
        return filterValue;
     }
      $(".pagination-bottom").once().on("click", "input[type='button']", function (){
        $('html, body').animate({
          'scrollTop' : $("#output").position().top
        });
      });
        /* On Scroll to achieve lazyload functionality.  */
        function applyproductPagination(params) {
          if(appliedSort == ''){
            appliedSort = defaultSort;
          }
          loader.show();
          loadProducts = false;
          let page;
          if (params['action']) {
            if (params['action'] == 'prev') {
              if (params['pageNum'] == 2) {
                page = 0;
              }
              else {
                page = params['pageNum'] - 2;
              }
            }
            else {
              page = params['pageNum'];
            }
          }
          else {
            page = params['pageNum'] - 1;
          }
          $.ajax({
            type: "GET",
            url: "/product-listing",
            dataType: 'json',
            data: {
              size: size,
              category: category,
              secondaryCategory: secondaryCategory,
              productFacets: productFacets,
              categoryCount: categoryCount,
              facets: filterFacets,
              cacheKeyData: cacheKeyData,
              sort: appliedSort,
              pagenumber: page,
              cache: "F",
              defaultModel: defaultModel,
              showerSystemFilter: shower_system_filter,
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
              $('.plp-pagination-output').html('');
              $('.plp-result').hide();
              return false;
            },
            success: function (data) {
              $('.plp-result').show();
              let response = new ProdView(data, true);
              if(params['pageNum'] && data) {
                data.currentpage = params['pageNum'];
                data.first = params['first'];
                data.last = params['last'];
              }
              if(params['action'] && data) {
                data.action = params['action'];
              }
              let PlppaginationView = new plppaginationView(data);
              PlppaginationView.undelegateEvents();
              loadProducts = true;
              lastPage = data.last;
              pageNumber++;
              let activePage = data.number +1;
              let from = ((activePage-1)*size)+1;
              let to = activePage*size;
              if (to > data.totalElements) {
                to = data.totalElements;
              }
              let resultString = "Results <span>"+from+"-"+to+"</span> of <span>"+data.totalElements + "</span>";
              $('.plp-results-count').html(resultString);
              $('.category-name').html(category);
              loader.hide();
              if ($.trim($('.accordion-block').text()) == ''){
                $('.product-filter').hide();
              }
              else{
                $('.product-filter').show();
              }
              if(data.totalElements == 0) {
                $('.plp-pagination-output').html('');
                $('.plp-result').hide();
              }
            }
          });
        }
      /* Sorting products. */
      $('.plp-sorting').once().on('change', '.form-item__select--sm', function (){
        let sortValue = $(this).find('option:selected').val();
        if (typeof sortValue !== 'undefined'){
          appliedSort = sortValue;
        }
        else{
          appliedSort = appliedSort.replace(sortValue, "");
        }
        pageNumber = 0;
        applyFacets(defaultModel, 0);
      });

      $('.plp-items-per-page').once().on('change', '.form-item__select--im', function (){
        size = $('.form-item__select--im').val();
        if (size == 'items-per-page') {
          size = productPerPage;
        }
        pageNumber = 0;
        applyFacets(defaultModel, 0);
      });

      // Renders product data in template using backbone JS.
      let ProdView = Backbone.View.extend({
        el: "#output",
        template: _.template($("#output-node").html()),
        initialize: function (initData, append) {
          this.render(initData, append);
          //this.postRender(initData, append);
        },
        /*
        postRender: function (initData, append) {
          savePrdObj = initData;
          greyOutOnOffFilters(savePrdObj);
        },*/
        render: function (initData, append) {
          let _self = this;
          let _render = "";

          $("#output").addClass('grid');
            let plp_position = 0;
            if (initData.content != '') {
            _.each(initData.content, function (entry) {
              let plp_product = new Object();
              plp_position = plp_position + 1;
              plp_product.list = "plp";
              plp_product.brand = entry.collections[0];
              plp_product.name = entry.description;
              plp_product.position = plp_position;
              plp_product.price = '$' + entry.values.ListPrice;
              plp_product.id = entry.name;
              plp_product.variant = entry.values.Finish;
              let _heroImage = entry.heroImageSmall;
              let _title = "Title";
              let _finishValuesArray = [];
              let finishArray = [];
              let _finishValues = [];
              let _shownWithText = [];
              let shownWithDescription = [];
              let _availableToOrderDate = entry.values.AvailableToOrderDateTimestamp;
              let _availableToShipDate = entry.values.AvailableToShipDateTimestamp;
              let _currentDate = entry.values.currentDate;
              let _viewAllCollectionLink = entry.values.viewAllCollectionLink;
              let _ribbonText = '';
              let _ribbonColor = '';
              let _sku = entry.name;
              finishOptions = entry.finishOptions;
              finishOptions.forEach(function(item,index) {
                  finish_tid = item.facetFinish;
                  let finishAvaiiabletoOrderdate = item.availableToOrderDate;
                  new Date;
                  if(facetData[finish_tid] && (Date.now() > finishAvaiiabletoOrderdate) && (item.stockingType != 'Obsolete')){
                    let termValues = facetData[finish_tid];
                    if (facetData[finish_tid][displayControlField] != 1) {
                      _finishValues.push(termValues.icon);
                    }
                    _finishValues.join(' ');
                  }
              });
              finishArray = _finishValues;
              var configOptions = entry.configOptions;
              configOptions.forEach(function(item,index0) {
                if(item.optionLabel != 'Default_Rough_Valve'){
                  optionProducts = item.optionProducts;
                  optionProducts.forEach(function(product,index1) {
                    if(entry.name.indexOf(product.modelNumber) !== -1){
                      _shownWithText.push('Shown with ' + product.description.replace(/((?!<sup>\s*))®((?!\s*<\/sup>))/gi, '<sup>&reg;</sup>') + ' ' + '<div class="product-submodelno">' + product.modelNumber + '</div>');
                      _shownWithText.join(' ');
                    }
                  });
                }
              });
              shownWithDescription = _shownWithText;

              // PDP URL generation
              let cat_list = new Array();
              entry.categories.forEach(function(category_item,index) {
                var category_item_array = category_item.split("_");
                if(category_item_array[0] == 'Brizo') {
                  cat_list.push(category_item_array[1]);
                }
              });

              let pdp_url = '/product-detail/';

              if (entry.description != '') {
                _title = entry.description.replace('&Reg;','®').replace('&Trade;','™');
              }
              let _collection = "";
              if(entry.values.Collection && entry.values.Collection != "") {
                _collection = entry.values.Collection;
              }
              else if(entry.values.defaultCollection && entry.values.defaultCollection !=""){
                _collection = entry.values.defaultCollection;
              }
              //let _sku = entry.name;
              let _pdp = pdp_url + entry.name;
              if(entry.values.discontinued) {
                _ribbonText = entry.values.discontinueText ? entry.values.discontinueText : 'Discontinued';
                _ribbonColor = "gray-30";
              }
              else {
                if (_currentDate < _availableToShipDate) {
                  if (_currentDate > _availableToOrderDate) {
                     _ribbonText = entry.values.comingSoonText ? entry.values.comingSoonText : 'Coming Soon';
                     _ribbonColor = "brizo-teal";
                  }
                }
                else if(entry.values.FltWebExclusiveCustomerItem == 'T' && entry.values.WebExclusiveCustomerItem == 'Ferguson') {
                  _ribbonText = 'Ferguson';
                  _ribbonColor = "brizo-teal";
                }
              }
              _render += _self.template({
                  title: _title,
                  collectionName: _collection,
                  heroImage: _heroImage,
                  sku: _sku,
                  pdp: _pdp,
                  ribbonText: _ribbonText,
                  ribbonColor: _ribbonColor,
                  viewAllCollectionLink:  _viewAllCollectionLink,
                  shownWithDescription: shownWithDescription,
                  finishes: finishArray
              });
            });
            if (append == true) {
              _self.$el.html(_render);
            }
            if (append == false) {
              _self.$el.append(_render);
            }
          }
          else {
            var noResultsMessage = '<div class ="error-message" role="contentinfo" aria-label="Error message">Please refine your search for results</div>';
            $('#output').html(noResultsMessage);
          }
        }
      });
      let facetView = Backbone.View.extend({
        initialize: function (initData, parentFacets, facetMap) {

          let facetBlock = getFacetBlock(initData);
          this.render(initData, parentFacets, facetBlock, facetMap);
        },
        events: {
          'click [type="checkbox"]': 'applyFacets',
        },
        applyFacets: function (e) {
          loadProducts = true;
        },
        render: function (initData, parentFacets, facetBlock, facetMap) {
          _.each(parentFacets,function (menu, index) {
            let targetContainer = '#' + index;
            let i;
            let count = menu.length;
            for (i = 0; i < menu.length; ++i) {

              if(count > 1){
                if(menu[i] == 'facetnarrowresults'){
                  facetDataName = facetData[menu[i]]['name'];
                }
                else{
                  facetDataName = facetData[menu[i]];
                }
                let subTitle = ( typeof facetData[menu[i]] !== 'undefined') ?
                  facetData[menu[i]] : menu[i];
                $(targetContainer).append('<h5 class="product-filter__section__subtitle">' +
                facetDataName +
                  '</h5>');
              }

              let collapseValue = facetMultiple[index]['collapse'];
              let minFilters = facetMultiple[index]['minFilters'];
              let maxFilters = facetMultiple[index]['maxFilters'];

              let filterContainer = '<div class="plp-filter-wrapper">'+
               facetBlock[menu[i]] +
               '</div>';

             $(targetContainer).append(filterContainer);
             $(targetContainer).append(
              '<div class="plp-filer-operations"><div class="plp-filter-view-all">View All<i class="icon-arrow-2-medium-down"></i></div><div class="plp-filter-view-less">Show Less<i class="icon-arrow-2-medium-down"></i></div></div>'
            );

              if(collapseValue == 1) {
                headerClass = accordianHeader;
                filterBlockClass = "accordion-def";
              }else if(collapseValue == 2) {
                headerClass = accordianHeader;
                filterBlockClass = "accordion-def";
              }

              let container = '<div class="accordion-desc accordion-desc-active" data-min="'+ minFilters +'" data-max="'+
                maxFilters +'"  ></div>';
                let plpname = '';
              if(count == 1) {

                if($( "body" ).hasClass( "shower-plp" ) && facetMultiple[index]['name'].toLowerCase() == 'collection' ) {
                   plpname = 'Collection & Series';
                } else {
                   plpname = facetMultiple[index]['name'];
                }
                let header = '<div class="'+ headerClass +'">\n' +
                plpname +
                  '</div>';

                $(targetContainer).contents().wrapAll(
                  container
                );
                $(targetContainer).prepend(header);
              } else if(count > 1 && i == (menu.length -1)) {
                if (facetMultiple[index]['name'].toLowerCase() == "collection") {
                  let header = '<div class="'+ headerClass + '">\n' +
                    "Collection & Series" +
                  '</div>';
                $(targetContainer).contents().wrapAll(
                  container
                );
                } else {
                  let header = '<div class="'+ headerClass + '">\n' +
                    facetMultiple[index]['name'] +
                  '</div>';
                $(targetContainer).contents().wrapAll(
                  container
                );
                }

                $(targetContainer).prepend(header);
              }

            }
          });
          /* to be implemented later
          function loadjscssfile(filename, filetype){
            if (filetype=="js"){ //if filename is a external JavaScript file
                var fileref=document.createElement('script')
                fileref.setAttribute("type","text/javascript")
                fileref.setAttribute("src", filename)
            }
            else if (filetype=="css"){ //if filename is an external CSS file
                var fileref=document.createElement("link")
                fileref.setAttribute("rel", "stylesheet")
                fileref.setAttribute("type", "text/css")
                fileref.setAttribute("href", filename)
            }
            if (typeof fileref!="undefined")
                document.getElementsByTagName("body")[0].appendChild(fileref)
        }
        loadjscssfile("https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js", "js")
        */

          jQuery(".plp-filter-wrapper").each(function(){
            jQuery(this).html(jQuery(this).children('.plp-filter-block').sort(function(a, b){
              return (jQuery(b).data('position')) < (jQuery(a).data('position')) ? 1 : -1;
            }))
          });
          jQuery(".custom-input__native").each(function(){
            availablefilters.push(jQuery(this).attr("data-filter") + "=" + jQuery(this).attr("value"));
          });
          availablefilterPairs = availablefilters;
          jQuery(".accordion .accordion-title").click(function() {
            jQuery(this).toggleClass("accordion-title-active");
            jQuery(this).next(".accordion-desc").toggleClass("accordion-desc-active").slideToggle();
            jQuery(this).parent(".accordion-block").toggleClass("accordion-block-active");
          });
          jQuery(".plp-filter-mobile").click(function() {
            jQuery(".filter-expand").toggleClass("toggle-expand--open");
            jQuery(".plp-accordion").toggleClass("plp-accordion--open");
            jQuery(".mobile-clear-filter").toggleClass("active-mobile-clear-filter");
            jQuery(".plp-filter").toggleClass("active-mobile-filter");
          });

          jQuery(".done-filter").click(function() {
            jQuery(".filter-expand").removeClass("toggle-expand--open");
            jQuery(".plp-accordion").removeClass("plp-accordion--open");
            jQuery(".mobile-clear-filter").removeClass("active-mobile-clear-filter");
            jQuery(".plp-filter").removeClass("active-mobile-filter")
          });

          /* View all filters. */
          jQuery(".plp-filter-view-all").click(function() {
            jQuery(this).parent().parent().addClass("load-more-filters");
            jQuery(this).hide();
            jQuery(this).next(".plp-filter-view-less").css("display","inline-block");
            var maxval = jQuery(this).parent().parent().attr("data-max");
            var indexval = maxval - 1;
            var sum = 0;
            jQuery(this).parent().parent().find(".plp-filter-block").each(function(){
            if(jQuery(this).index() <= indexval) {
            var filterheight = 0;
            filterheight = jQuery(this).outerHeight();
            sum = sum + filterheight;
            }
            });
            jQuery(this).parent().prev(".plp-filter-wrapper").css("height", sum);
            });

          jQuery(".accordion-desc .plp-filter-wrapper").each(function(){
            var sum = 0;
            var minval = jQuery(this).parent().attr("data-min");
            var showval = minval - 1;
            var ttlchild = $(this).children().length;
            jQuery(this).children(".plp-filter-block").each(function(){
              if(jQuery(this).index() <= showval) {
                var varheight = 0;
                varheight = jQuery(this).outerHeight();
                sum = sum + varheight;
              }
              if(jQuery(this).find(".custom-input__native.notapplicable").length) {
                jQuery(this).addClass("not-applicable-wrapper");
              }
            });
            if((ttlchild < minval) || (ttlchild == minval)) {
              jQuery(this).next(".plp-filer-operations").find(".plp-filter-view-all").hide();
            }

            jQuery(this).css("height", sum);
          });

          /* View less filters. */
          jQuery(".plp-filter-view-less").click(function() {
            jQuery(this).hide();
            jQuery(this).prev(".plp-filter-view-all").show();
            jQuery('.load-more-filters .plp-filter-wrapper').scrollTop(0);
            jQuery(".accordion-desc").removeClass("load-more-filters");
            var maxval = jQuery(this).parent().parent().attr("data-min");
            var indexval = maxval - 1;
            var sum = 0;
            jQuery(this).parent().parent().find(".plp-filter-block").each(function(){
            if(jQuery(this).index() <= indexval) {
            var filterheight = 0;
            filterheight = jQuery(this).outerHeight();
            sum = sum + filterheight;
            }
            });
            jQuery(this).parent().prev(".plp-filter-wrapper").css("height", sum);
            });

          /* Share link. */
          jQuery(".plp-share-link").click(function() {
            if (filtersforShare.length){
              filtersforShare = filtersforShare.replace(/&&/g,'&');
              document.getElementById("plpshareinput").value = pathwithoutQuery + '?' + filtersforShare;
            }
            else{
              document.getElementById("plpshareinput").value = pathwithoutQuery;
            }
          jQuery(".plp-share-popup").addClass("active-plp-share-popup");
          });

         /* Logic for enabling preserved filters when user comes back to PLP page */
          var pURL = $(location).attr("href");
          var question_mark = pURL.indexOf("?");
          var facet_text = pURL.substr(question_mark+1);
          var split_text = facet_text.split("=");
          var queryFacetCategory = split_text[0];
          var queryFacetValue = split_text[1];

          jQuery(".custom-input").each(function () {
            if((jQuery(this).find(".custom-input__native").attr("value") === queryFacetValue) && ((jQuery(this).find(".custom-input__native").attr("value") === queryFacetCategory))) {
              jQuery(this).find(".custom-input__native").attr("checked", "checked");
            }
            if(window.location.search){
              // When user navigates back to PLP page from any other page.
              if(window.location.search == '?searchFilter=true'){
                if(localStorage.getItem(pathwithoutQuery)) {
                  preserved_filters = localStorage.getItem(pathwithoutQuery).replace(/^&|&$/g,'');
                }
                else if(localStorage.getItem(window.location.href)){
                  preserved_filters = localStorage.getItem(window.location.href).replace(/^&|&$/g,'');
                }
              }
              // When filters are entered in the url as query parameters.
              else{
                checkedFilters = filterFacets.replace(defaultFacets, "");
                preserved_filters = filtersforShare.length ? checkedFilters.replace(/^&|&$/g,'').replace(/or_|and_/g,'').replace(/\s+/g, '+') : '';
              }
              if (preserved_filters != ''){
                /*
                if($("input.facets-checkbox").hasClass('greyed-out')){
                  $("input.facets-checkbox").removeClass('greyed-out').addClass('greyed-out-on').attr("disabled", true);
                }
                else{
                  $("input.facets-checkbox").addClass('greyed-out-on').attr("disabled", true);
                }
                */
                //greyOutOnOffFilters(savePrdObj);
                filterString = preserved_filters.split("&");
                var checkboxCategory = [];
                var checkboxName = [];
                var onetext = '';
                var twotext = '';
                for (var i = 0; i < filterString.length; i++) {
                  if (filterString[i] !== "" && filterString[i] !== null) {
                    onetext = filterString[i];
                    twotext = onetext.split('=');
                    checkboxCategory.push(twotext[0]);
                    checkboxName.push(twotext[1]);
                  }
                }
                for (var j = 0; j < checkboxName.length; j++) {
                  if (checkboxName[j] !== "" && checkboxName[j] !== null) {
                    if(checkboxCategory[j] == 'includeObsolete'){
                      checkBoxValue  = checkBoxValue = 'Include Discontinued Products';
                    }
                    else if(checkboxCategory[j] == 'includeFerguson'){
                      checkBoxValue  = 'Include Ferguson Exclusive Products';
                    }
                    else if(checkboxCategory[j] == 'includeFuture'){
                      checkBoxValue  = 'Exclude Coming Soon Products';
                    }
                    else{
                      checkBoxValue  = checkboxName[j];
                    }
                    if(checkBoxValue.indexOf('+')){
                      checkBoxValue = checkBoxValue.replace(/\+/g, ' ');
                    }
                    if(((jQuery(this).find(".custom-input__native").attr("value") === decodeURIComponent(checkBoxValue)) || (jQuery(this).find(".custom-input__native").attr("value") === checkBoxValue) && (jQuery(this).find(".custom-input__native").attr("value") === checkboxCategory[j])) || (jQuery(this).find(".custom-input__native").attr("value") === checkboxCategory[j])){
                      jQuery(this).find(".custom-input__native").attr("checked", "checked");
                      filterChips(jQuery(this).text(), jQuery(this).find(".custom-input__native").attr("id"), 1);
                      jQuery("a[id='chip-" + jQuery(this).find(".custom-input__native").attr("id") + "']").show();
                    }
                  }
                }
                if($('.filter-chip').length < 1){
                  $('#clear_filters').hide();
                }else{
                  $('#clear_filters').show();
                }
              }
            }
          });
        }
      });

      /* Re-building the facets block after API call. */
      function getFacetBlock(initData){
        let facetBlock = [];
        let count = 0;
        _.each(initData.facets, function (facet) {
          let facetName = facet.name;
          let facetType = facet.type;
          let output = '';
          let facetCount = 0;
          let helpText = '';
          let termWeight = '';
          let termId = '';
          let icon_img = '';
          let termHideDisplay = '';

          if (facetType == 'term') {
            facetTerms = facet.terms;
            if (facetName == 'facetNarrowResults' ){
              facetTerms = facetData['facetnarrowresults']['terms'];
            }
            if(facetName == 'shower_components'){
              facetTerms = facetData['shower_components']['terms'];
            }
            if(facetName == 'shower_trims_and_roughs'){
              facetTerms = facetData['shower_trims_and_roughs']['terms'];
            }
            if(facetName == 'accessories_and_grab_bars'){
              facetTerms = facetData['accessories_and_grab_bars']['terms'];
            }
            if(facetName == 'shower_packages'){
              facetTerms = facetData['shower_packages']['terms'];
            }
            if(facetName == 'shower_systems'){
              facetTerms = facetData['shower_systems']['terms'];

            }

            if(facetName == "collections") {
              facetTerms = sortByKey(facetTerms, 'term');
            }

            _.each(facetTerms, function (terms) {
              let termValues = facetData[terms.term];
              let termName = (typeof termValues !== 'undefined') ?
                 termValues['label'] : terms.term ;
              let elemId = termName;
              termId = terms.term;
              if(typeof termValues !== 'undefined'){
                elemId = (typeof termValues['tid'] !== 'undefined') ?
                  termValues['tid'] : termName;
                helpText = (typeof termValues['help'] !== 'undefined') ?
                    termValues['help'] : '';
                termWeight =  (typeof termValues['position'] !== 'undefined') ?
                termValues['position'] : '';
                termId = terms.term;
                termHideDisplay = (typeof termValues[displayControlField] !== 'undefined') ?
                  termValues[displayControlField] : '';

                if ( typeof termValues['icon'] !== 'undefined') {
                 if ( termValues['icon'] !== '') {
                   icon_img = "<div class ='icon-image filter-icon-image'><img src='"+ termValues['icon'] +"'></div>"
                 }
                 else {
                   icon_img = '';
                 }
               }
               else {
                 icon_img = '';
               }
              }
              if (facetName == 'facetNarrowResults' || facetName == 'shower_components' || facetName == 'shower_trims_and_roughs' || facetName == 'accessories_and_grab_bars' || facetName == 'shower_packages'  ) {
                termName = terms.label;
                termId = terms.name;
                elemId = termName.trim();
                termWeight = terms.position;
                termHideDisplay = 0;
                icon_img = '';
              }
              if( facetName == 'shower_systems') {
                termName = terms.name;
                termId = terms.label;
                elemId = termName.trim();
                termWeight = terms.position;
                termHideDisplay = 0;
                icon_img = '';
              }
              if (facetName == 'categories'){
                elemId = 'type-'+ termName;
              }
              if (facetName == 'facetHandle'){
                elemId = terms.term;
              }
              let termMachineName = 'facet_' + facet.name;
              let elemIdTrim = '';
              if(typeof elemId !== 'undefined') {
                elemIdTrim = elemId.replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, '-');
              }
              else {
                termHideDisplay = 1;
              }
              if(elemId == 'na' || elemId == 'NA'){
                elemId = elemId + '-' + facetName;
              }
              if(facetName == 'shower_components' ||  facetName == 'accessories_and_grab_bars' ) {
                data_filter = "categories";
              }  else {
                data_filter = facetName;
              }

              if(facetName == 'shower_systems') {
                data_filter = termId;
                termId = 'T';
              }
              if(facetName == 'shower_trims_and_roughs') {
                data_filter = 'shower_trims_and_roughs';
              }
              if( facetName == 'shower_packages') {
                data_filter = 'shower_packages';
              }

              if(termHideDisplay != 1 ){
                output += '<div class="plp-filter-block" data-position="' + termWeight + '"><div class="custom-input custom-input--checkbox"  id="container-' + elemId + '">\n' +
                  '  <input class="custom-input__native facets-checkbox '+ elemIdTrim +'" data-filter="' + data_filter + '" type="checkbox"' +
                  ' id="'+ elemId +'" value="' + termId + '">\n' +
                  '  <label title="'+ helpText +'" class="custom-input__label  " for="' + elemId + '">\n' +
                  termName +
                  '  </label>\n' +
                  '</div>' + '<div class="filter-icon">' + icon_img + '</div></div>';
              }

              facetCount++;
            });
          }
          else {

            let facetCount = 0;
            _.each(facet.ranges, function (facetRange) {
              let facetLabel = "$" + facetRange.from + ".00 - $" + facetRange.to + ".00";
              if (facetRange.to == '1000000'){
                facetLabel = "$" + facetRange.from + "+";
              }
              let count = facetRange.count
              let facetValue = facetRange.from + ".00-" + facetRange.to + ".00";
              let setRangeClass = facetRange.from + "-" + facetRange.to;
              let facetName = facet.name;

              if (count !== 0) {
                let elemIdTrim = facetValue.replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, '-');

                output += '<div class="custom-input custom-input--checkbox" >\n' +
                  '  <input class="custom-input__native facets-checkbox '+ elemIdTrim + ' ' +setRangeClass +'" data-filter="' + facetName + '" type="checkbox" id="' + facetValue + '" value="' + facetValue + '">\n' +
                  '  <label class="custom-input__label" for="' + facetValue + '">\n' +
                  facetLabel +
                  '  </label>\n' +
                  '</div>';
              }
            });
          }
          facetBlock[facetName.toLowerCase()] = output;
        });
        return facetBlock;
      }

      /* Detects navigation back from any page. */
      function pageNavigated(){
        if (window.performance &&
        window.performance.navigation.type == 2) {
          window.history.replaceState(null, null, "?searchFilter=true");
        }
      }
      /* Creating the filter chips after filters selected. */
      function filterChips(displayName, elemId, action){
        if(action == 0){
          $("a[id='chip-" + elemId + "']").remove();
        }else{
          let chip = '<a class="filter-chip js-filter-chip" ' +
            'id="chip-'+ elemId +'" style="display:none">\n' +
            displayName +'<i class="icon-filter-hide-small"></i></a>';
          $('.selected-filter').prepend(chip);
        }
      }

      // Grey out on and off on facet filters
      var greyOutOnOffFilters = function (initData) {
        _.each(initData.facets, function (facet) {
          let facetName = facet.name;
          let facetType = facet.type;
          let output = '';
          let facetCount = 0;
          let helpText = '';
          let termWeight = '';
          let termId = '';
          let icon_img = '';
          if (facetType == 'term') {
            facetTerms = facet.terms;
            _.each(facetTerms, function (terms) {
              let termValues = facetData[terms.term];
              let termName = (typeof termValues !== 'undefined') ?
               termValues['label'] : terms.term;
              let elemId = termName;
              if(typeof termValues !== 'undefined'){
              elemId = (typeof termValues['tid'] !== 'undefined') ?
                termValues['tid'] : termName;
              helpText = (typeof termValues['help'] !== 'undefined') ?
                  termValues['help'] : '';
              termWeight =  (typeof termValues['position'] !== 'undefined') ?
              termValues['position'] : '';
              termId = terms.term;
            }
            if (facetName == 'categories'){
               elemId = 'type-'+ termName;
            }
            let parentSelector = '';
            if(elemId == 'na'){
              parentSelector = '#container-na-' + facetName + ' ';
            }
            let filterElmID = parentSelector + '.' + elemId.replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, '-');
            if(filterElmID == '.twohandle'){
              $(".twohandles").removeClass('greyed-out-on').addClass('greyed-out-off').removeAttr("disabled");
            }
            if($(filterElmID).hasClass('greyed-out-on')){
              $(filterElmID).removeClass('greyed-out-on').addClass('greyed-out-off').removeAttr("disabled");
            }
          });

          }

        });
      }
      /* Method to sort json object using keys. */
      function sortByKey(array, key) {
        return array.sort(function(a, b) {
          var x = a[key]; var y = b[key];
          return ((x < y) ? -1 : ((x > y) ? 1 : 0));
        });
      }
      /* Convert to camel case .*/
      function upperCamelCase(text) {
        return text.replace(/\w+/g,
          function(w){
            return w[0].toUpperCase() + w.slice(1).toLowerCase();
          });
      }
      /* Applies selected filters and makes API calls. */
      function applyFacets(defaultModel, clearall, dataFilter= []) {
        defaultModel = true;
        cacheFlag = 'F';
        if(appliedSort == ''){
          appliedSort = defaultSort;
        }
        loader.show();
        $('.plp-operation').hide();
        loadProducts = true;
        if(clearall){
          filterFacets = defaultFacets;
        }
        if(filterFacets.indexOf('&facetFinish') != -1){
          defaultModel = false;
        }
        if (dataFilter.length > 0) {
	  filterCategoryFormatted = dataFilter;
        }
        else {
          filterCategoryFormatted = category;
        }
        /*
        if($("input.facets-checkbox").hasClass('greyed-out')){
          $("input.facets-checkbox").removeClass('greyed-out').addClass('greyed-out-on').attr("disabled", true);
        }else{
           $("input.facets-checkbox").addClass('greyed-out-on').attr("disabled", true);
        }
        // Dont greyout narrow result
        $('#narrowresultsby input[type=checkbox]').removeClass('greyed-out').removeClass('greyed-out-on').removeAttr("disabled");
        */
        $.ajax({
          type: "GET",
          url: "/product-listing",
          dataType: 'json',
          data: {
            size: size,
            category: filterCategoryFormatted,
            secondaryCategory: secondaryCategory,
            productFacets: productFacets,
            categoryCount: categoryCount,
            facets: filterFacets,
            sort: appliedSort,
            cacheKeyData: cacheKeyData,
            pagenumber: pageNumber,
            cache: "F",
            defaultModel: defaultModel,
            showerSystemFilter: shower_system_filter,
          },
          error: function (XMLHttpRequest, textStatus, errorThrown) {
            $('.plp-pagination-output').html('');
            $('.plp-result').hide();
            return false;
          },
          success: function (data) {
            $('.plp-result').show();
            if (data.totalElements > 0) {
              $('.plp-operation').show();
              $('.plp-pagination-output').hide();
              $('.form-item__select--im').hide();
              if(data.totalElements > 24){
                $('.form-item__select--im').show();
                $('.plp-pagination-output').show();
              }
            }
            let response = new ProdView(data, true);
            if(data){
              data.currentpage = 1;
            }
            let PlppaginationView = new plppaginationView(data);
            PlppaginationView.undelegateEvents();
            loadProducts = true;
            lastPage = data.last;
            pageNumber++;
            $('.plp-results-count').html();
            let activePage = data.number +1;
            let from = ((activePage-1)*size)+1;
            let to = activePage*size;
            if (to > data.totalElements) {
              to = data.totalElements;
            }
            let resultString = "Results <span>"+from+"-"+to+"</span> of <span>"+data.totalElements + "</span>";
            $('.plp-results-count').html(resultString);
            $('.category-name').html(category);
            $('.loader').hide();
            if($('.filter-chip').length < 1){
              $('#clear_filters').hide();
            }else{
              $('#clear_filters').show();
            }
            $('.js-filter-chip').show();
            if(data.totalElements == 0) {
              $('.plp-pagination-output').html('');
              $('.plp-result').hide();
            }
            loader.hide();
            if(clearall){
              localStorage.setItem(pathwithoutQuery,filterFacets);
            }
          }
        });
        preservedFilters = filterFacets;
        localStorage.setItem(window.location.href, preservedFilters);
        filtersApplied = filterFacets.replace(defaultFacets, "");
        filtersforShare = filtersApplied.replace(/^&|&$/g,'').replace(/or_|and_/g,'').replace(/\s+/g, '+');
      }

      /* pagination starts here*/
      var plppaginationView = Backbone.View.extend({
        el: ".plp-pagination-output",
        template: _.template($("#plp-pagination").html()),
        initialize: function (initData) {
          this.render(initData);
        },
        events: {
          'click [type="button"]': 'plppagination'
        },
        plppagination: function (e) {
          var params = [];
          params['pageNum'] = $(e.currentTarget).val();
          params['first'] = $(e.currentTarget).attr('first');
          params['last'] = $(e.currentTarget).attr('last');
          if($(e.currentTarget).attr("data-val") === 'next') {
            params['pageNum'] = $('.pager__link.is-active').val();
            params['action'] = 'next';
          }
          if($(e.currentTarget).attr("data-val") === 'prev') {
            params['pageNum'] = $('.pager__link.is-active').val();
            params['action'] = 'prev';
          }
          if($(e.currentTarget).attr('data') === 'last-page') {
            var total = $(e.currentTarget).attr('total');
            params['pageNum'] = parseInt(total);
            params['first'] = parseInt(total)-2;
            params['last'] = parseInt(total);
          }
          if($(e.currentTarget).attr('data') === 'first-page') {
            params['pageNum'] = 1;
            params['first'] = 1;
            params['last'] = 3;
          }
          applyproductPagination(params);        },
        render: function (initData) {
          if (initData === null) {
            return false;
          }
          var cont_disp_cnt = size;//drupalSettings.cont_disp_cnt;
          if (cont_disp_cnt == null) {
            cont_disp_cnt = productPerPage;
          }
          var _self = this;
          var _render = "<div class='page-desktop-pagination pagination'>";
          var _mobilerender = "<div class='page-mobile-pagination pagination'>";
          var _nextPage = (initData === null) ? 0 : parseInt(initData.number) + 1;
          var _currentpage = initData.currentpage;  // Current page
          var _totalmatches = initData.totalElements;
          var _totalpages = Math.ceil(_totalmatches / cont_disp_cnt);
          var _nextPage;
          var _first;
          if (_currentpage < _totalpages) {
            _nextPage = _currentpage + 1;
          } else {
            _nextPage = _currentpage;
          }
          var _previousPage;
          if (_currentpage === 1) {
            _previousPage = 1;
            _first = 1;
          } else {
            _previousPage = _currentpage - 1;
            _first = initData.first;
          }
          if(_currentpage>=initData.first && _currentpage<=initData.last) {
            _first = parseInt(initData.first);
          }
          if(initData.action && initData.action == 'next') {
            if(_currentpage>=initData.first && _currentpage<initData.last) {
              _first = parseInt(initData.first);
            } else {
              _first = parseInt(initData.first) + 1;
            }
            _currentpage = parseInt(_currentpage) + 1;
          }
          if(initData.action && initData.action == 'prev') {
            if(_currentpage>initData.first && _currentpage<=initData.last) {
              _first = parseInt(initData.first);
            } else {
              _first = parseInt(initData.first) - 1;
            }
            _currentpage = parseInt(_currentpage) - 1;
          }
          var pagination = parseInt(_first)+2;
          if(_totalpages < pagination) {
            pagination = _totalpages;
          }
          if(_currentpage != 1) {
            //_render += '<input total="'+_totalpages+'" class="pager__link first" type="button" data="" value="<<">';
            _render += '<input first="'+_first+'" last="'+pagination+'" class="pager__link prev" type="button" data="" data-val="prev" value="&lsaquo;">';
            _mobilerender += '<input first="'+_first+'" last="'+pagination+'" class="pager__link prev" type="button" data="" data-val="prev" value="&lsaquo;">';
          }
          if((_first+2 >= _totalpages) && (_totalpages > 3)) {
            _render += '<input total="'+_totalpages+'" class="pager__link first" type="button" data="first-page" value=1><span class="pagination-dots"> ... </span>';
          }
          for(var i=_first;i<=pagination;i++) {
            /*_render += _self.template({
              pageNumber: i,
              currentPage: _currentpage,
              totalRecordset: initData.totalElements
            });*/
            var classValue = '';
            if(i === parseInt(_currentpage)) {
              classValue = 'is-active';
              _mobilerender += '<p class="mobile_pagination"><span>Page </span><span class="totalpages">' + i + '</span></p>';
            }

            _render += '<input first="'+_first+'" last="'+pagination+'" class="pager__link '+ classValue +'" type="button" value="'+ i +'">';

          }
          if(!_first && _totalpages == 1) {
            _mobilerender += '<p class="mobile_pagination"><span>Page </span><span class="totalpages">' + _totalpages +'</span></p>';

          }
          _mobilerender += '<p class="mobile_pagination"><span>of </span><span class="totalpages">' + _totalpages +'</span></p>';
          if(_first+2 < _totalpages) {
            _render += '<span class="pagination-dots"> ... </span><input total="'+_totalpages+'" class="pager__link last" type="button" data="last-page" value='+_totalpages+'>';
          }
          if(_currentpage != _totalpages) {
            _render += '<input first="'+_first+'" last="'+pagination+'" class="pager__link next" type="button" data="" data-val="next" value="&rsaquo;">';
            _mobilerender += '<input first="'+_first+'" last="'+pagination+'" class="pager__link next" type="button" data="" data-val="next" value="&rsaquo;">';
            //_render += '<input total="'+_totalpages+'" class="pager__link last" type="button" data="" value=">>">';
          }
          var start_value = (_currentpage-1)*cont_disp_cnt;
          if(_currentpage ==1) {
            start_value = 1;
          }
          var endvalue = _currentpage*cont_disp_cnt;
          if(start_value == 1 && endvalue > initData.totalElements){
            // no pager
          }
          else if(endvalue > initData.totalElements){
            endvalue = initData.totalElements;
            $('.srp-result-doc').html('<span class="srp-qty">Results ' + start_value + '-' + endvalue  + ' of ' + initData.totalElements +'</span>').attr('data-content-cnt', initData.totalElements);
          }
          else{
            $('.srp-result-doc').html('<span class="srp-qty">Results ' + start_value + '-' + endvalue  + ' of ' + initData.totalElements +'</span>').attr('data-content-cnt', initData.totalElements);
          }
          _render += "</div>"
          _mobilerender += "</div>";
          _render = _render + _mobilerender;
          _self.$el.html(_render);
        }
      });
      /*End of pagination*/

    }
  };
})
(jQuery, Drupal, drupalSettings, Backbone);

(function ($, Drupal, drupalSettings, Backbone) {
    
    Drupal.behaviors.WOBContent = {
     attach: function(context, settings) {
 
       if($('.wob-columns-text-all').length){
         $(".wob-columns-text-all").once().each(function(e){
           var heightColumn = 0;
           var column = $(this).find('.wob-column');
           if(column.length){
             column.once().each(function(e){
               if(!$(this).hasClass('one-column')){
                 if(!$(this).hasClass('top-other-paragraph') || !$(this).hasClass('middle')){
                   heightColumn = $(this).height();
                 }
                 if($(this).hasClass('top-other-paragraph')){
                   topMargin = 0;
                   if($(this).prev(),find('h5').lenght){
                     topMargin = $(this).prev().find('h5').outerHeight();
                   }
                   if($(this).next(),find('h5').lenght){
                     topMargin = $(this).next().find('h5').outerHeight();
                   }
                   $(this).css('margin-top',(topMargin+31)+"px");
                 }
                 if($(this).hasClass('middle')){
                   var thisHeight = $(this).height();
                   var thisHeightColumn = $(this)[0].parentElement.clientHeight;
                   /* var calcPosition = 0;
                   if(thisHeight < thisHeightColumn){
                     calcPosition = (thisHeightColumn - thisHeight) /2;
                   }else{
                     calcPosition = ( thisHeightColumn /2 );
                   } */
                   var calcPosition = (thisHeightColumn - thisHeight - 150) /2;
                   $(this).css('margin-top',(calcPosition)+"px");
                 }
               }
             });
           }
         });
       }
 
       if($("[id*='block-views-block-wob-featured-products-with-images-wob-featured-products-with-images-block']").length){
         $("[id*='block-views-block-wob-featured-products-with-images-wob-featured-products-with-images-block']").once().each(function(e){
           var block = $(this);
           var index = block.index(this);
           if(block.next().hasClass('credits-featured-products-align-left') || block.prev().hasClass('credits-featured-products-align-left')){
             block.addClass('grid-col-md-9').removeClass('grid-col-md-12');
             block.addClass('grid-col-lg-9').removeClass('grid-col-lg-12');
             block.find('.grid').find('.grid-col-md-4').addClass('grid-col-md-6').removeClass('grid-col-md-4');
             block.find('.grid').find('.grid-col-lg-3').addClass('grid-col-lg-4').removeClass('grid-col-lg-3');
             block.addClass('credits-featured-products-align-left');
           }
         });
       }
       if($("[id*='block-views-block-wob-credits-view-credits-block']").length){
        $("[id*='block-views-block-wob-credits-view-credits-block']").once().each(function(e){
          var block = $(this);
          var index = block.index(this);
          if(block.next().attr('id') || block.prev().attr('id')){
            if((typeof block.next().attr('id') !== "undefined" && block.next().attr('id').indexOf('block-views-block-wob-credits-view-credits-block') >= 0 && block.next().hasClass('credits-featured-products-align-left'))
            || (typeof block.prev().attr('id') !== "undefined" && block.prev().attr('id').indexOf('block-views-block-wob-credits-view-credits-block') >= 0 && block.prev().hasClass('credits-featured-products-align-left')) ){
              block.addClass('grid-col-md-9').removeClass('grid-col-md-12');
              block.addClass('grid-col-lg-9').removeClass('grid-col-lg-12');
              block.find('.grid').find('.grid-col-md-4').addClass('grid-col-md-6').removeClass('grid-col-md-4');
              block.find('.grid').find('.grid-col-lg-3').addClass('grid-col-lg-4').removeClass('grid-col-lg-3');
              block.addClass('credits-featured-products-align-second');
            }
          }
        });
      }

      function fixVideoColumn(){
        if($(".wob-columns").length){
          var keepImageHeight = 0;
          var captionHeight = 0;
          $(".wob-column").each(function(e){
            var block = $(this).find('.wob-video-article');
            if($(this).hasClass('hide-item-column')){
              $(this).show();
              keepImageHeight = $(this).find('picture')[0].clientHeight;
              $(this).hide();
            }
            if($(this).find('.image-caption')){
              if($(this).find('.image-caption').height() > captionHeight){
                captionHeight = $(this).find('.image-caption').height();
              }
            }
            if(block.length && window.innerWidth < 920 && keepImageHeight > 0){
              $(this).find('.image-caption').height(captionHeight);
              if($(this).first().find('picture')){
                $(this).addClass('fix-video-column');
                block.height(keepImageHeight);
                if(block.find('.youtube-player')[0]){
                  block.find('.youtube-player').height(keepImageHeight);
                }
              }
            }else{
              block.height('auto');
              $(this).height('auto');
              $(this).removeClass('fix-video-column');
              if(block.find('.youtube-player')[0]){
                block.find('.youtube-player').height('auto');
              }
            }
          });
        }
      }
      $( window ).resize(function() {
        fixVideoColumn();
      });
      fixVideoColumn();
 
     }
   };

   Drupal.behaviors.downloadWOBImages = {
    attach: function (context, settings) {
      if($(".download-imgicon").length){
        $('.download-imgicon', context).on('click', function(e){
            e.preventDefault();
            var img_path = $(this).attr('src');
            $("#edit-downlod-file-url").val(img_path);
            $("#edit-downlod-file-type").val('image');
            $("#download-handler-form").submit();
            return true;
        });
      }
    }
  };
 
  })(jQuery, Drupal, drupalSettings, Backbone);
(function ($, Drupal, drupalSettings) {
   	Drupal.behaviors.discoverTheme = {
		attach: function(context, settings) {
			//Get current page URl to redirect Home Page.
			$(".mainmenu li a").once().on("click", function() {
			   var pathname = window.location.pathname; // Returns path only (/path/example.html)
			   if (pathname !== "/") {
				  var currentUrl = $(this).attr('href');
				  window.location.href = '/' + currentUrl;
			   }
			});
			// Scroll Page
			function scrollNav() {
			   $('#block-discover-theme-main-menu a').click(function() {
				  //Toggle Class
				  $(".active").removeClass("active");
				  $(this).closest('li').addClass("active");
				  var theClass = $(this).attr("class");
				  $('.' + theClass).parent('li').addClass('active');
				  //Animate
				  $('html, body').stop().animate({
					 scrollTop: $($(this).attr('href')).offset().top - 63
				  }, 600);
				  return false;
			   });
			}
			scrollNav();
			// Inserting ID for anchor
			$(".paragraph--type--description-video").attr('id', 'about');
			if (window.location.href.indexOf("about") > -1) {
			   $('html, body').stop().animate({
				  scrollTop: ($("#about").offset() || {
					 "top": NaN
				  }).top - 64
			   }, 100);
			} else if (window.location.href.indexOf("Brands") > -1) {
			   $('html, body').stop().animate({
				  scrollTop: ($("#Brands").offset() || {
					 "top": NaN
				  }).top - 66
			   }, 600);
			} else if (window.location.href.indexOf("Locations") > -1) {
			   $('html, body').stop().animate({
				  scrollTop: ($("#Locations").offset() || {
					 "top": NaN
				  }).top - 62
			   }, 600);
			}
			// Inserting humbergur icon on first place for mobile
			$(".mobile-menu").insertBefore("#block-discover-theme-branding");
			// Hamburger menu click event
			$(".mobile-menu").click(function() {
			   $(this).toggleClass('open');
			   $("#block-discover-theme-main-menu").toggle();
			});
			// When user click on mobile menu links, Hamburger menu should close
			$("#block-discover-theme-main-menu a").click(function() {
			   $(".mobile-menu").toggleClass('open');
			   $("#block-discover-theme-main-menu").toggle();
			});
			// When user scroll, header will be stick always on top
			window.onscroll = function() {
			   jumpToSection()
			};
			var header = $("[role='banner']");
			var sticky = header.offset().top;
			// Sticky Header function
			function jumpToSection() {
			   if (window.pageYOffset > sticky) {
				  $("[role='banner']").addClass("sticky");
				  $("body").addClass("stickyBody");
			   } else {
				  $("[role='banner']").removeClass("sticky");
				  $("body").removeClass("stickyBody");
			   }
			}
			// Form Validation
			$('#webform-submission-request-information-add-form .form-submit').attr("disabled", true);
			// Error placeholder inserted below each field
			$('.webform-request  .required').each(function(i) {
			   var error = $(this).attr('data-webform-required-error');
			   $(this).after("<div class='request-form-error'>" + error + " </div>");
			});
			$("#webform-submission-request-information-add-form #edit-name, #webform-submission-request-information-add-form #edit-address, #webform-submission-request-information-add-form #edit-company, #webform-submission-request-information-add-form #edit-city, #webform-submission-request-information-add-form #edit-title, #webform-submission-request-information-add-form #edit-state, #webform-submission-request-information-add-form #edit-email, #webform-submission-request-information-add-form #edit-zipcode, #webform-submission-request-information-add-form #edit-phone, #webform-submission-request-information-add-form #edit-are-you-interested-in--wrapper input.form-checkbox").on("click change keyup focusout", function() {
			   var Fname = /^[a-zA-Z,\/ ]+$/;
			   var phoneregex = /^[0-9]{3}-[0-9]{3}-[0-9]{4}$/;
			   var emailregex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
			   var companynameregex = /^[0-9a-zA-Z,\/ ]+$/;
			   var zipcode = /(^\d{5}$)|(^\d{5}-\d{4}$)/;
			   if ($("#edit-name").val() != "" && Fname.test($("#edit-name").val()) &&
				  $("#edit-address").val() != "" &&
				  $("#edit-company").val() != "" && companynameregex.test($("#edit-company").val()) &&
				  $("#edit-city").val() != "" &&
				  $("#edit-title").val() != "" && Fname.test($("#edit-title").val()) &&
				  $("#edit-state option:selected").val() != "" &&
				  $("#edit-email").val() != "" && emailregex.test($("#edit-email").val()) &&
				  $("#edit-zipcode").val() != "" && zipcode.test($("#edit-zipcode").val()) &&
				  $("#edit-phone").val() != "" && phoneregex.test($("#edit-phone").val()) &&
				  $('#edit-are-you-interested-in--wrapper input.form-checkbox:checked').length != 0
			   ) {
				  $('#webform-submission-request-information-add-form .form-submit').attr("disabled", false);
			   } else {
				  $('#webform-submission-request-information-add-form .form-submit').attr("disabled", true);
			   }
			});
			// Phone Number Formatter
			$('#edit-phone').keyup(function() {
			   $(this).val($(this).val().replace(/(\d{3})\-?(\d{3})\-?(\d{4})/, '$1-$2-$3'));
			});
			// If user select more then one checkobox
			$('#edit-are-you-interested-in .form-checkbox').on('change', function() {
			   $('#edit-are-you-interested-in .form-checkbox').not(this).prop('checked', false);
			});
			$("#edit-what-ceu-subjects--wrapper").hide();
			// Yes or NO
			$('#edit-are-you-interested-in-no, #edit-are-you-interested-in-yes').on("click change keyup focusout", function() {
			   if ($(this).is(":checked") === true) {
				  if ($(this).val() == 'No') {
					 $("#edit-what-ceu-subjects--wrapper").hide();
					 $('#edit-what-ceu-subjects--wrapper input:checkbox').prop('checked', false);
					 checkValidationIfNo();
				  } else {
					 $("#edit-what-ceu-subjects--wrapper").show();
					 checkValidation();
				  }
			   }
			});
		 
			function checkValidation() {
			   $("#webform-submission-request-information-add-form #edit-name, #webform-submission-request-information-add-form #edit-address, #webform-submission-request-information-add-form #edit-company, #webform-submission-request-information-add-form #edit-city, #webform-submission-request-information-add-form #edit-title, #webform-submission-request-information-add-form #edit-state, #webform-submission-request-information-add-form #edit-email, #webform-submission-request-information-add-form #edit-zipcode, #webform-submission-request-information-add-form #edit-phone, #webform-submission-request-information-add-form #edit-are-you-interested-in--wrapper input.form-checkbox, #webform-submission-request-information-add-form #edit-what-ceu-subjects input.form-checkbox").on("click change keyup focusout", function() {
				  var Fname = /^[a-zA-Z,\/ ]+$/;
				  var phoneregex = /^[0-9]{3}-[0-9]{3}-[0-9]{4}$/;
				  var emailregex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
				  var companynameregex = /^[0-9a-zA-Z,\/ ]+$/;
				  var zipcode = /(^\d{5}$)|(^\d{5}-\d{4}$)/;
				  if ($("#edit-name").val() != "" && Fname.test($("#edit-name").val()) &&
					 $("#edit-address").val() != "" &&
					 $("#edit-company").val() != "" && companynameregex.test($("#edit-company").val()) &&
					 $("#edit-city").val() != "" &&
					 $("#edit-title").val() != "" && Fname.test($("#edit-title").val()) &&
					 $("#edit-state option:selected").val() != "" &&
					 $("#edit-email").val() != "" && emailregex.test($("#edit-email").val()) &&
					 $("#edit-zipcode").val() != "" && zipcode.test($("#edit-zipcode").val()) &&
					 $("#edit-phone").val() != "" && phoneregex.test($("#edit-phone").val()) &&
					 $('#edit-are-you-interested-in--wrapper input.form-checkbox:checked').length != 0 &&
					 $('#edit-what-ceu-subjects--wrapper input.form-checkbox:checked').length != 0
				  ) {
					 $('#webform-submission-request-information-add-form .form-submit').attr("disabled", false);
				  } else {
					 $('#webform-submission-request-information-add-form .form-submit').attr("disabled", true);
				  }
			   });
			}
		 
			function checkValidationIfNo() {
			   $("#webform-submission-request-information-add-form #edit-name, #webform-submission-request-information-add-form #edit-address, #webform-submission-request-information-add-form #edit-company, #webform-submission-request-information-add-form #edit-city, #webform-submission-request-information-add-form #edit-title, #webform-submission-request-information-add-form #edit-state, #webform-submission-request-information-add-form #edit-email, #webform-submission-request-information-add-form #edit-zipcode, #webform-submission-request-information-add-form #edit-phone, #webform-submission-request-information-add-form #edit-are-you-interested-in--wrapper input.form-checkbox").on("click change keyup focusout", function() {
				  var Fname = /^[a-zA-Z,\/ ]+$/;
				  var phoneregex = /^[0-9]{3}-[0-9]{3}-[0-9]{4}$/;
				  var emailregex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
				  var companynameregex = /^[0-9a-zA-Z,\/ ]+$/;
				  var zipcode = /(^\d{5}$)|(^\d{5}-\d{4}$)/;
				  if ($("#edit-name").val() != "" && Fname.test($("#edit-name").val()) &&
					 $("#edit-address").val() != "" &&
					 $("#edit-company").val() != "" && companynameregex.test($("#edit-company").val()) &&
					 $("#edit-city").val() != "" &&
					 $("#edit-title").val() != "" && Fname.test($("#edit-title").val()) &&
					 $("#edit-state option:selected").val() != "" &&
					 $("#edit-email").val() != "" && emailregex.test($("#edit-email").val()) &&
					 $("#edit-zipcode").val() != "" && zipcode.test($("#edit-zipcode").val()) &&
					 $("#edit-phone").val() != "" && phoneregex.test($("#edit-phone").val()) &&
					 $('#edit-are-you-interested-in--wrapper input.form-checkbox:checked').length != 0
				  ) {
					 $('#webform-submission-request-information-add-form .form-submit').attr("disabled", false);
				  } else {
					 $('#webform-submission-request-information-add-form .form-submit').attr("disabled", true);
				  }
			   });
			}
			//   // Error Placeholder For Email, Select box and check box
			$("<div class='request-form-error'> Please select either Yes or No</div>").insertAfter("#edit-are-you-interested-in");
			$("<div class='request-form-error'> Please choose one either one subject</div>").insertAfter("#edit-what-ceu-subjects");
			// Name Error Message
			$("#webform-submission-request-information-add-form #edit-company").on("change keyup keypress", function() {
			   var companynameregex = /^[0-9a-zA-Z,\/ ]+$/;
			   checkPattern($(this), companynameregex);
			});
			// Name Error Message
			$("#webform-submission-request-information-add-form #edit-name, #webform-submission-request-information-add-form #edit-title").on("change keyup keypress", function() {
			   var Fname = /^[a-zA-Z,\/ ]+$/;
			   checkPattern($(this), Fname);
			});
			// Email Address Error Message
			$("#webform-submission-request-information-add-form #edit-email").on("change keyup keypress", function() {
			   var email_regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
			   checkPattern($(this), email_regex);
			});
			// Phone Number Error Message
			$("#webform-submission-request-information-add-form #edit-phone").on("change keyup keypress", function() {
			   var phoneregex = /^[0-9]{3}-[0-9]{3}-[0-9]{4}$/;
			   checkPattern($(this), phoneregex);
			});
			// Zip Code Error Message
			$("#webform-submission-request-information-add-form #edit-zipcode").on("change keyup keypress", function() {
			   var zipcode = /(^\d{5}$)|(^\d{5}-\d{4}$)/;
			   checkPattern($(this), zipcode);
			});
			// Checkbox error
			$('#edit-are-you-interested-in, #edit-what-ceu-subjects').on("change keyup keypress", function() {
			   var check = $(this).find('input[type=checkbox]:checked').length;
			   if (check) {
				  $(this).next("div").css("display", "none");
			   } else {
				  $(this).next("div").css("display", "block");
			   }
			});
			// Pattern Check Function
			function checkPattern(current, regex) {
			   if (current.val() === "") {
				  $(current).next("div").css("display", "block");
			   } else if (!regex.test(current.val())) {
				  $(current).next("div").css("display", "block");
			   } else {
				  $(current).next("div").css("display", "none");
			   }
			}
			// Show Validation message if user missed the field
			$('#webform-submission-request-information-add-form .required:not(#edit-email, #edit-phone, #edit-zipcode, #edit-name, #edit-title, #edit-company)').on("change blur keyup ", function() {
			   if (this.value === '') {
				  $(this).addClass("red-outline");
				  $(this).next("div").css("display", "block");
			   } else {
				  $(this).removeClass("red-outline");
				  $(this).next("div").css("display", "none");
			   }
			});
		 }
	};
})(jQuery, Drupal);

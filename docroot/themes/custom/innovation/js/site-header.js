"use strict";

/*!
 * Site Header jQuery Plugin
 *
 * Initialize this plugin on the DOM element which contains the
 * site header
 */
// the semi-colon before the function invocation is a safety
// net against concatenated scripts and/or other plugins
// that are not closed properly.
;

(function ($, window, document, ScrollMagic, undefined) {
  // Create the defaults once
  var pluginName = "siteHeader",
      defaults = {
    controller: null,
    scene: null
  },
      selectors = {
    scrollLink: "[data-site-header-scroll-link]"
  }; // The actual plugin constructor

  function Plugin(element, options) {
    this.element = element;
    this.options = $.extend({}, defaults, options);
    this._defaults = defaults;
    this._name = pluginName;
    this.init();
  }

  Plugin.prototype = {
    init: function init() {
      // alias the scope
      var self = this; // cache the ScrollMagic controller

      this.options.controller = new ScrollMagic.Controller(); // change behavior of controller to animate scroll instead of jump

      this.options.controller.scrollTo(function (newpos) {
        TweenMax.to(window, 1.5, {
          scrollTo: {
            y: newpos - 73,
            ease: Linear.easeNone
          }
        });
      });
      var $headerLinks = $(this.element).find(selectors.scrollLink); // bind scroll to anchor links

      $headerLinks.on("click", function (e) {
        var id = $(this).attr("href");

        if ($(id).length > 0) {
          e.preventDefault(); // trigger scroll

          window.requestAnimationFrame(function () {
            setTimeout(function () {
              self.options.controller.scrollTo(id);
            }, 10);
          });
        }
      });
    }
  }; // A really lightweight plugin wrapper around the constructor,
  // preventing against multiple instantiations

  $.fn[pluginName] = function (options) {
    return this.each(function () {
      if (!$.data(this, "plugin_" + pluginName)) {
        $.data(this, "plugin_" + pluginName, new Plugin(this, options));
      }
    });
  };
})(jQuery, window, document, ScrollMagic);
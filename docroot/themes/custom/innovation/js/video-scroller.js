"use strict";

/*!
 * Video Scroller jQuery Plugin
 *
 * Initialize this plugin on the DOM element which contains the
 * video and has an extended height to dictate the duration of
 * the scrolling animation.  
 */
// the semi-colon before the function invocation is a safety
// net against concatenated scripts and/or other plugins
// that are not closed properly.
;

(function ($, window, document, ScrollMagic, undefined) {
  // Create the defaults once
  var pluginName = "videoScroller",
      classes = {
    captionItemHidden: "video-scroller__caption-item--hidden",
    videoEnd: "video-scroller--end",
    videoScrolling: "video-scroller--scrolling"
  },
      dataAttr = {
    captionTrigger: "video-scroller-caption-trigger"
  },
      defaults = {
    captionsEnabled: false,
    controller: null,
    prevScrollPos: 0,
    videoScene: null,
    scrollPos: 0,
    video: null
  },
      selectors = {
    captions: "[data-video-scroller-captions]",
    captionItems: "[data-video-scroller-caption-item]",
    captionItemsHidden: ".video-scroller__caption-item--hidden",
    captionTriggers: "[data-video-scroller-caption-trigger]",
    video: "[data-video-scroller-src]"
  }; // The actual plugin constructor

  function Plugin(element, options) {
    this.element = element;
    this.options = $.extend({}, defaults, options);
    this._defaults = defaults;
    this._name = pluginName;
    this.init();
  }

  Plugin.prototype = {
    init: function init() {
      // cache the ScrollMagic controller
      this.options.controller = new ScrollMagic.Controller(); // cache the ScrollMagic scene for the video

      this.options.videoScene = new ScrollMagic.Scene({
        triggerElement: this.element,
        triggerHook: "onEnter"
      }); // cache the HTML5 video DOM element

      this.options.video = $(this.element).find(selectors.video).get(0);
      this.startScrollAnimation(); // check if the "captions" function should be enabled

      this.options.captionsEnabled = $(this.element).find(selectors.captions).length; // if the captions function is enabled initialize the scrolling captions

      if (this.options.captionsEnabled) {
        this.startScrollCaptions();
      }
    },
    startScrollAnimation: function startScrollAnimation() {
      // alias the scope
      var self = this;
      this.options.videoScene.addTo(this.options.controller).duration(this.element.clientHeight).on("progress", function (e) {
        self.options.scrollPos = e.progress; // alias the video source jquery element

        var $element = $(self.element); // scrolling reached the end point

        if (self.options.scrollPos >= 1) {
          // if the video has the scrolling class, remove it and add the end class
          if ($element.hasClass(classes.videoScrolling)) {
            $element.removeClass(classes.videoScrolling).addClass(classes.videoEnd);
          }
        } // otherwise make sure the video has the scrolling class
        else if (self.options.scrollPos > 0) {
            if (!$element.hasClass(classes.videoScrolling)) {
              $element.removeClass(classes.videoEnd).addClass(classes.videoScrolling);
            }
          }
      });
      setInterval(function () {
        if (self.options.prevScrollPos === self.options.scrollPos) return;
        requestAnimationFrame(function () {
          self.options.video.currentTime = self.options.video.duration * self.options.scrollPos;
          self.options.video.pause();
          self.options.prevScrollPos = self.options.scrollPos;
        });
      }, 50);
    },
    startScrollCaptions: function startScrollCaptions() {
      // alias the scope
      var self = this; // initialize the captions which will be shown with autoAlpha: 0 (hidden)

      TweenMax.set(selectors.captionItemsHidden, {
        autoAlpha: 0
      });
      $(selectors.captionItemsHidden).css("display", "none");
      var $capTriggers = $(self.element).find(selectors.captionTriggers),
          $captionItems = $(self.element).find(selectors.captionItems); // iterate through each caption trigger to position it in the scroll container

      for (var i = 0; i < $capTriggers.length; i++) {
        var $capTriggerItem = $capTriggers.eq(i); // count the caption trigger points and set their top offset 
        // within the scrolling container - evenly spaced based
        // on the total number of triggers

        var triggerTop = (i + 1) / ($capTriggers.length + 1) * 100; // offset the even spacing by 15% to accomodate the video

        triggerTop = triggerTop + 15;
        $capTriggerItem.css("top", triggerTop + "%");
      } // iterate through each caption trigger again to setup the caption fade animations


      for (var j = 0; j < $capTriggers.length; j++) {
        var $capTriggerItem = $capTriggers.eq(j); // setup the ScrollMagic scene

        var scene = new ScrollMagic.Scene({
          triggerElement: $capTriggerItem.get(0),
          triggerHook: "onEnter"
        }); // get the trigger data from the HTML data attribute

        var triggerTarget = $capTriggerItem.data(dataAttr.captionTrigger); // setup the "hide" and "show" animations at the trigger point

        var tween = new TimelineMax(),
            hideTweens = [],
            showTween = null; // iterate through all the caption items which are NOT the showing item
        // and hide those items

        for (var k = 0; k < $captionItems.length; k++) {
          var $caption = $captionItems.eq(k); // if this caption element is not the target "show" element then add
          // a tween in the timeline to hide it first

          if ("#" + $caption.attr("id") !== triggerTarget.show) {
            var hideVars = {
              autoAlpha: 0,
              display: "none"
            };

            if (k !== 0) {
              hideVars.immediateRender = false;
            }

            var hideTween = TweenMax.to($caption, 0.2, hideVars);
            hideTweens.push(hideTween);
          } else {
            var showTween = TweenMax.to($caption, 0.2, {
              autoAlpha: 1,
              display: "block",
              immediateRender: false
            });
          }
        } // add the tweens in sequential order so the
        // "show" tween is the final animation


        for (var l = 0; l < hideTweens.length; l++) {
          tween.add(hideTweens);
        }

        tween.add(showTween); // add the timeline to the new scrollmagic scene

        scene.setTween(tween).addTo(self.options.controller);
      }
    }
  }; // A really lightweight plugin wrapper around the constructor,
  // preventing against multiple instantiations

  $.fn[pluginName] = function (options) {
    return this.each(function () {
      if (!$.data(this, "plugin_" + pluginName)) {
        $.data(this, "plugin_" + pluginName, new Plugin(this, options));
      }
    });
  };
})(jQuery, window, document, ScrollMagic);
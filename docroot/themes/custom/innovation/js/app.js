"use strict";

// main app entry point
(function ($) {
  var wWidth = window.innerWidth > 0 ? window.innerWidth : screen.width;
  var timebar = TweenMax.fromTo('.time-bar', 1, {
    yPercent: 100,
    opacity: 1
  }, {
    yPercent: 0,
    opacity: 1,
    ease: Back.easeOut
  });
  var fdin1 = TweenMax.fromTo('.timeline .li-0', 1, {
    yPercent: 100,
    opacity: 0.2
  }, {
    yPercent: 0,
    opacity: 1,
    ease: Power1.easeInOut
  });
  var fdin2 = TweenMax.fromTo('.timeline .li-1', 1, {
    yPercent: 100,
    opacity: 0.2
  }, {
    yPercent: 0,
    opacity: 1,
    ease: Power1.easeInOut
  });
  var fdin3 = TweenMax.fromTo('.timeline .li-2', 1, {
    yPercent: 100,
    opacity: 0.2
  }, {
    yPercent: 0,
    opacity: 1,
    ease: Power1.easeInOut
  });
  var fdin4 = TweenMax.fromTo('.timeline .li-3', 1, {
    yPercent: 100,
    opacity: 0.2
  }, {
    yPercent: 0,
    opacity: 1,
    ease: Power1.easeInOut
  });
  var controller = new ScrollMagic.Controller({
    globalSceneOptions: {
      duration: "100%"
    }
  });
  var li_scene1 = new ScrollMagic.Scene({
    triggerElement: '.section-2-intro',
    offset: 150
  }).setTween(fdin1);
  var li_scene1active = new ScrollMagic.Scene({
    triggerElement: '.li-0 .title'
  }).setClassToggle(".li-0", "active");
  var li_scene2 = new ScrollMagic.Scene({
    triggerElement: '.li-0'
  }).setTween(fdin2);
  var li_scene2active = new ScrollMagic.Scene({
    triggerElement: '.li-1 .title'
  }).setClassToggle(".li-1", "active");
  var li_scene3 = new ScrollMagic.Scene({
    triggerElement: '.li-1'
  }).setTween(fdin3);
  var li_scene3active = new ScrollMagic.Scene({
    triggerElement: '.li-2 .title'
  }).setClassToggle(".li-2", "active");
  var li_scene4 = new ScrollMagic.Scene({
    triggerElement: '.li-1',
    offset: 250
  }).setTween(fdin4);
  var li_scene4active = new ScrollMagic.Scene({
    triggerElement: '.li-3 .title'
  }).setClassToggle(".li-3", "active");
  var li_sceneLast = new ScrollMagic.Scene({
    triggerElement: '.li-2'
  }).setClassToggle(".time-bar", "done-scroll");
  controller.addScene([//timebar_scene,
  li_scene1, li_scene2, li_scene3, li_scene4, li_scene1active, li_scene2active, li_scene3active, li_scene4active, li_sceneLast]);
  var tween_news = TweenMax.staggerFromTo(".news-item", 2, {
    opacity: 0,
    yPercent: 25
  }, {
    opacity: 1,
    yPercent: 0,
    ease: Back.easeOut
  }, 0.35);
  var scene = new ScrollMagic.Scene({
    triggerElement: ".news--banner-flex-right p",
    duration: 0
  }).setTween(tween_news).addTo(controller); // Drupal.behaviors.ajaxpagination = {
  //   attach: function (context, settings) {
  //     $('ul.pager__items li a').click(function() {
  //       var href = $(this).attr('href');
  //       if(href && href != '#'){ // Remove active Page onclick
  //         $.ajax({
  //           url: href,
  //           type: "GET",
  //           success: function(data) {
  //             //$('#comment-right-section').html(data);
  //             console.log(data + " data");
  //             Drupal.attachBehaviors("#comment-right-section");
  //           }
  //         });
  //       }
  //       return false;
  //     });
  //
  //   }
  // };

  $('.homesignupform .form-submit,.contactusform .form-submit').on('click', function () {
    if (typeof Drupal.Ajax !== 'undefined' && typeof Drupal.Ajax.prototype.beforeSubmitOriginal === 'undefined') {
      Drupal.Ajax.prototype.beforeSubmitOriginal = Drupal.Ajax.prototype.beforeSubmit;

      Drupal.Ajax.prototype.beforeSubmit = function (form_values, element_settings, options) {
        var flag = 0; //Last name Validation

        if (!$('input[name="last_name"]').val()) {
          $('input[name="last_name"]').next(".validation").remove();

          if ($('input[name="last_name"]').next(".validation").length == 0) {
            // only add if not added
            var errormessage = $('input[name="last_name"]').attr("data-webform-required-error");
            $("input, textarea").focus(function () {
              $(this).next(".validation").empty();
            });
            $('input[name="last_name"]').after("<div class='validation'>" + errormessage + "</div>");
            flag = 1;
          }
        } else if (!$('input[name="last_name"]').val().match(/^[a-zA-Z-]*$/)) {
          $("input, textarea").focus(function () {
            $(this).next(".validation").empty();
          });
          $('input[name="last_name"]').next(".validation").remove();
          if ($('input[name="last_name"]').next(".validation").length < 1) // only add if not added
            var errormessage = $('input[name="last_name"]').attr("data-webform-pattern-error");
          {
            $('input[name="last_name"]').after("<div class='validation'>" + errormessage + "</div>");
            flag = 1;
          }
        } else {
          $('input[name="last_name"]').next(".validation").remove();
        }

        if (!$('input[name="first_name"]').val()) {
          $('input[name="first_name"]').next(".validation").remove();

          if ($('input[name="first_name"]').next(".validation").length == 0) {
            // only add if not added
            var errormessage = $('input[name="first_name"]').attr("data-webform-required-error");
            $("input, textarea").focus(function () {
              $(this).next(".validation").empty();
            });
            $('input[name="first_name"]').after("<div class='validation'>" + errormessage + "</div>");
            flag = 1;
          }
        } else if (!$('input[name="first_name"]').val().match(/^[a-zA-Z-]*$/)) {
          $("input, textarea").focus(function () {
            $(this).next(".validation").empty();
          });
          $('input[name="first_name"]').next(".validation").remove();
          if ($('input[name="first_name"]').next(".validation").length < 1) // only add if not added
            var errormessage = $('input[name="first_name"]').attr("data-webform-pattern-error");
          {
            $('input[name="first_name"]').after("<div class='validation'>" + errormessage + "</div>");
            flag = 1;
          }
        } else {
          $('input[name="first_name"]').next(".validation").remove();
        } //email validation


        if (!$('input[name="email"]').val()) {
          $('input[name="email"]').next(".validation").remove();

          if ($('input[name="email"]').next(".validation").length == 0) {
            // only add if not added
            var errormessage = $('input[name="email"]').attr("data-webform-required-error");
            $("input, textarea").focus(function () {
              $(this).next(".validation").empty();
            });
            $('input[name="email"]').after("<div class='validation'>" + errormessage + "</div>");
            flag = 1;
          }
        } else if (!$('input[name="email"]').val().match(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/)) {
          $("input, textarea").focus(function () {
            $(this).next(".validation").empty();
          });
          $('input[name="email"]').next(".validation").remove();
          if ($('input[name="email"]').next(".validation").length < 1) // only add if not added
            var errormessage = $('#edit-email').attr("data-webform-pattern-error");
          {
            $('input[name="email"]').after("<div class='validation'>" + errormessage + "</div>");
            flag = 1;
          }
        } else {
          $('input[name="email"]').next(".validation").remove();
        }

        if ($('textarea[name="commetns"]').length && !$('textarea[name="commetns"]').val()) {
          $('textarea[name="commetns"]').next(".validation").remove();

          if ($('textarea[name="commetns"]').next(".validation").length == 0) {
            // only add if not added
            var errormessage = $('textarea[name="commetns"]').attr("data-webform-required-error");
            $("input, textarea").focus(function () {
              $(this).next(".validation").empty();
            });
            $('textarea[name="commetns"]').after("<div class='validation'>" + errormessage + "</div>");
            flag = 1;
          }
        } else {
          $('textarea[name="commetns"]').next(".validation").remove();
        }

        if (grecaptcha.getResponse() == "") {
          $('.g-recaptcha').next(".validation").remove();
          $('.g-recaptcha').after("<div class='validation'>Recaptcha is required</div>");
          flag = 1;
        } else {
          $('.g-recaptcha').next(".validation").remove();
        }

        if (flag === 1) {
          this.ajaxing = false;
          return false;
        }

        return this.beforeSubmitOriginal();
      };
    }
  });
  $(document).ready(function () {
    checkout();
    orderStatus(); // initialize the site header DOM element with the site header plugin

    $("[data-site-header]").siteHeader({}); // if the window width is typical desktop resolution then initialize desktop-only plugins

    if (window.innerWidth >= 1024) {
      // initialize the parallax page sections with the plugin
      $("[data-section-parallax]").pageSectionParallax({});
      $("[data-section-fade-up]").pageSectionFadeUp({});
      $("[data-section-fade-left-item]").pageSectionFadeLeft({});
    }

    $(window).on('resize', function () {
      if (window.innerWidth >= 1024) {
        // initialize the parallax page sections with the plugin
        $("[data-section-parallax]").pageSectionParallax({});
        $("[data-section-fade-up]").pageSectionFadeUp({});
        $("[data-section-fade-left-item]").pageSectionFadeLeft({});
      }
    });

    function customVideo() {
      var videoSource = $('.fullscreen');

      if (window.innerWidth < 768) {
        var mobileVideo = videoSource.data('mobilevideo');
        $('#video-source').attr("src", mobileVideo);
        videoSource[0].load();
        videoSource[0].play();
      } else {
        var desktopVideo = videoSource.data('desktopvideo');
        $('#video-source').attr("src", desktopVideo);
        videoSource[0].load();
        videoSource[0].play();
      }
    }

    if ($("#video-source").length) {
      customVideo();
      $(window).resize(function () {
        if (window.innerWidth >= 1024) {
          // initialize the parallax page sections with the plugin
          $("[data-section-parallax]").pageSectionParallax({});
          $("[data-section-fade-up]").pageSectionFadeUp({});
          $("[data-section-fade-left-item]").pageSectionFadeLeft({});
        }

        customVideo();
      });
    }

    ; // Hero parallax

    if (window.innerWidth >= 1140) {
      var img = new Image();
      img.src = '/themes/custom/innovation/img/hero-static.jpg';

      img.onload = function () {
        $('.hero-parallax-background').addClass('is-loaded');
        setTimeout(function () {
          $(".hero-parallax").heroParallax({});
          window.requestAnimationFrame(function () {
            $('.hero-parallax-content ').removeClass('is-hidden');
          });
        }, 200);
      };
    } // Mobile navigation


    var navButton = $('.site-header__nav-button');
    navButton.click(toggleMobileMenu);

    function toggleMobileMenu() {
      var menu = $('.site-header__nav');
      $('body').toggleClass('menu-is-active');
      navButton.toggleClass('is-active');
      menu.toggleClass('is-active');
      setTimeout(function () {
        if (menu.hasClass('is-active')) {
          menu.on('click', toggleMobileMenu);
        } else {
          menu.off();
        }
      }, 0);
    } //
    // Form validation


    function checkout() {
      $('.shipping-details-form .form-submit').on('click', function (e) {
        //Name validation
        if (!$('.shipping-details-form #edit-fullname').val()) {
          $("#edit-fullname").next(".validation").remove();

          if ($(".shipping-details-form #edit-fullname").next(".validation").length == 0) // only add if not added
            {
              $("input, textarea").focus(function () {
                $(this).next(".validation").empty();
              });
              $(".shipping-details-form #edit-fullname").after("<div class='validation'>Please enter name</div>");
            }

          e.preventDefault(); // prevent form from POST to server
        } else if (!$('.shipping-details-form #edit-fullname').val().match(/^[a-zA-Z ]+$/)) {
          $("input, textarea").focus(function () {
            $(this).next(".validation").empty();
          });
          $("#edit-fullname").next(".validation").remove();

          if ($("#edit-fullname").next(".validation").length < 1) // only add if not added
            {
              $(" #edit-fullname").after("<div class='validation'>Full Name is allowed Only letters and white space</div>");
            }

          e.preventDefault();
        } else {
          $("#edit-fullname").next(".validation").remove();
        } //address


        if (!$('.shipping-details-form #edit-address-line').val()) {
          $("input, textarea").focus(function () {
            $(this).next(".validation").empty();
          });
          $("#edit-address-line").next(".validation").remove();

          if ($(".shipping-details-form #edit-address-line").next(".validation").length == 0) // only add if not added
            {
              $(".shipping-details-form #edit-address-line").after("<div class='validation'>Please enter the address</div>");
            }

          e.preventDefault(); // prevent form from POST to server
        } else if (!$('.shipping-details-form #edit-address-line').val().match(/^[0-9a-zA-Z,\/ ]+$/)) {
          $("input, textarea").focus(function () {
            $(this).next(".validation").empty();
          });
          $("#edit-address-line").next(".validation").remove();

          if ($("#edit-address-line").next(".validation").length < 1) // only add if not added
            {
              $("#edit-address-line").after("<div class='validation'>Address Line 1 allows only letters, Numbers</div>");
            }

          e.preventDefault();
        } else {
          $("#edit-address-line").next(".validation").remove();
        } //city validation


        if (!$('.shipping-details-form #edit-city').val()) {
          $("#edit-city").next(".validation").remove();

          if ($(".shipping-details-form #edit-city").next(".validation").length == 0) // only add if not added
            {
              $(".shipping-details-form #edit-city").after("<div class='validation'>Please enter the city</div>");
            }

          e.preventDefault(); // prevent form from POST to server
        } else if (!$('.shipping-details-form #edit-city').val().match(/^[a-zA-Z ]+$/)) {
          $("#edit-city").next(".validation").remove();

          if ($("#edit-city").next(".validation").length < 1) // only add if not added
            {
              $("#edit-city").after("<div class='validation'>City is allowed Only letters and white space</div>");
            }

          e.preventDefault();
        } else {
          $("#edit-city").next(".validation").remove();
        } //state validation


        if (!$('.shipping-details-form #edit-state').val()) {
          $(" #edit-state").next(".validation").remove();

          if ($(".shipping-details-form #edit-state").next(".validation").length == 0) // only add if not added
            {
              $(".shipping-details-form #edit-state").after("<div class='validation'>Please enter the state</div>");
            }

          e.preventDefault(); // prevent form from POST to server
        } else {
          $("#edit-state").next(".validation").remove();
        }

        zipValidation();
        emailvalidation();
        validatePhone();
      });
    }

    var phoneValidationFlag = true;

    function validatePhone() {
      $('.contact_details').find('#edit-phone-number').each(function (e) {
        if (!$('.contact_details #edit-phone-number').val()) {
          $("#edit-phone-number").next(".validation").remove();

          if ($(".contact_details #edit-phone-number").next(".validation").length == 0) // only add if not added
            {
              $(".contact_details #edit-phone-number").after("<div class='validation'>Please enter phone number</div>");
            }

          event.preventDefault(); // prevent form from POST to server
        } else if (!$('.contact_details #edit-phone-number').val().match(/^(?:\(\d{3}\)|\d{3}-)\d{3}-\d{4}$/)) {
          $("input, textarea").focus(function () {
            $(this).next(".validation").empty();
          });
          $("#edit-phone-number").next(".validation").remove();

          if ($("#edit-phone-number").next(".validation").length < 1) // only add if not added
            {
              $("#edit-phone-number").after("<div class='validation'>Please enter valid phone number</div>");
            }

          event.preventDefault();
        } else {
          $("#edit-phone-number").next(".validation").remove();
        }
      });
    }

    var formBtn = $('#buttonSaveFormROI');
    formBtn.click(function (e) {
      e.stopPropagation();
    }); //
    // Phone number Formater
    //

    $('#edit-phone-number').keyup(function () {
      $(this).val($(this).val().replace(/(\d{3})\-?(\d{3})\-?(\d{4})/, '$1-$2-$3'));
    });
  }); //Order status Validation

  function orderStatus() {
    $(".path-order-status .order-status-btn").on('click', function (e) {
      if (!$('#edit-order-number').val()) {
        $("#edit-order-number").next(".validation").remove();

        if ($("#edit-order-number").next(".validation").length == 0) // only add if not added
          {
            $("#edit-order-number").after("<div class='validation'>Please enter the ordernumber</div>");
          }

        e.preventDefault(); // prevent form from POST to server
      } else if (!$('#edit-order-number').val().match(/^[0-9]+$/)) {
        $("input, textarea").focus(function () {
          $(this).next(".validation").empty();
        });
        $("#edit-order-number").next(".validation").remove();

        if ($("#edit-order-number").next(".validation").length < 1) // only add if not added
          {
            $("#edit-order-number").after("<div class='validation'>OrderNumber allows Only numbers</div>");
          }

        e.preventDefault();
      } else {
        $("#edit-order-number").next(".validation").remove();
      }

      emailvalidation();
      zipValidation();
    });
  } //validate email address


  function emailvalidation() {
    if (!$('.form-email').val()) {
      $(".form-email").next(".validation").remove();

      if ($(".form-email").next(".validation").length == 0) // only add if not added
        {
          $(".form-email").after("<div class='validation'>Please enter email</div>");
          return false;
        }

      event.preventDefault(); // prevent form from POST to server
    } else if (!$('.form-email').val().match(/^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/)) {
      $("input, textarea").focus(function () {
        $(this).next(".validation").empty();
      });
      $(".form-email").next(".validation").remove();

      if ($(".form-email").next(".validation").length < 1) // only add if not added
        {
          $(".form-email").after("<div class='validation'>Please enter valid email address</div>");
          return false;
        }

      event.preventDefault();
    } else {
      $(".form-email").next(".validation").remove();
    }
  } //zip validation


  function zipValidation() {
    if (!$('.shipping-details-form #edit-zipcode,.order-status-form #edit-zipcode').val()) {
      $("#edit-zipcode").next(".validation").remove();

      if ($(".shipping-details-form #edit-zipcode,.order-status-form #edit-zipcode").next(".validation").length == 0) // only add if not added
        {
          $(".shipping-details-form #edit-zipcode,.order-status-form #edit-zipcode").after("<div class='validation'>Please enter the zipcode</div>");
        }

      event.preventDefault(); // prevent form from POST to server
    } else if (!$('.shipping-details-form #edit-zipcode,.order-status-form #edit-zipcode').val().match(/^[0-9]+$/)) {
      $("input, textarea").focus(function () {
        $(this).next(".validation").empty();
      });
      $("#edit-zipcode").next(".validation").remove();

      if ($("#edit-zipcode").next(".validation").length < 1) // only add if not added
        {
          $("#edit-zipcode").after("<div class='validation'>Zip is allowed Only numbers</div>");
        }

      event.preventDefault();
    } else {
      $("#edit-zipcode").next(".validation").remove();
    }
  }

  if ($('#toolbar-bar').length > 0) {
    $('body').addClass('admin');
  }

  function getImageSlider(slick) {
    var total = slick.slideCount;
    var current = slick.currentSlide;
    var prevSlide, nextSlide;

    if (current == 0) {
      prevSlide = parseInt(total - 1);
    } else {
      prevSlide = parseInt(current - 1);
    }

    if (current == total - 1) {
      nextSlide = 0;
    } else {
      nextSlide = parseInt(current + 1);
    }

    var imgLeft = $('.pdp--slider-item[data-slick-index="' + prevSlide + '"] .slide-thumbnail img').attr('src');
    var imgRight = $('.pdp--slider-item[data-slick-index="' + nextSlide + '"] .slide-thumbnail img').attr('src');

    if (imgLeft === undefined && imgRight === undefined) {
      $('.left_nav_slider .img').css('background-image', 'none').attr('data-item', prevSlide);
      ;
      $('.right_nav_slider .img').css('background-image', 'none').attr('data-item', nextSlide);
    } else {
      $('.left_nav_slider .img').css('background-image', 'url(' + imgLeft + ')').attr('data-item', prevSlide);
      $('.right_nav_slider .img').css('background-image', 'url(' + imgRight + ')').attr('data-item', nextSlide);
    }
  }

  function fadeInNav() {
    $('.left_nav_slider .img, .right_nav_slider .img').fadeTo(300, 1);
  }

  function fadeOutNav() {
    $('.left_nav_slider .img, .right_nav_slider .img').fadeTo(300, 0);
  }

  $('.slider-main').on('beforeChange', function (event, slick) {
    fadeOutNav();
  });
  $('.slider-main').on('init afterChange', function (event, slick) {
    getImageSlider(slick);
    fadeInNav();
  });
  $('.left_nav_slider').on('click', function (e, slick) {
    $('.slider-main').slick('slickPrev'); // e.preventDefault();
    // var slideIndex = parseInt($(this).data('item'));
    // $('.slider-main').slick('slickGoTo', slideIndex);
  });
  $('.right_nav_slider').on('click', function (e, slick) {
    $('.slider-main').slick('slickNext'); // e.preventDefault();
    // var slideIndex = parseInt($(this).data('item'));
    // $('.slider-main').slick('slickGoTo', slideIndex);
  });
  $('.slider-main').slick({
    infinite: true,
    fade: true,
    dots: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false // prevArrow: $('.left_nav_slider'),
    // nextArrow: $('.right_nav_slider')

  }); // FROM HOME
  // adjust footer position

  function fadeOutNav() {
    var wHeight = $(window).height();
    var cHeight = $('body').outerHeight();

    if (wHeight > cHeight) {
      $('main').css('padding-bottom', wHeight - cHeight);
    }
  }

  fadeOutNav(); //Scroll of glass rinser

  $(document).ready(function () {
    $(".page-node-type-product-detail-page .pdp--getupdates a.scroll").on('click', function (e) {
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 800, function () {
        window.location.hash = hash;
      });
    });
  }); //Scroll

  $(document).ready(function () {
    $("a.scroll").on('click', function (e) {
      if (this.hash !== "") {
        e.preventDefault();
        var hash = this.hash;
        $('html, body').animate({
          scrollTop: $(hash).offset().top
        }, 800, function () {
          window.location.hash = hash;
        });
      } // End if

    });
  }); //news

  if (wWidth > 767) {
    var getHeight = function getHeight(item) {
      var elmMargin = 0;
      var elmHeight = Math.ceil(item.offsetHeight);
      elmMargin += Math.ceil(parseFloat(getComputedStyle(item).marginTop));
      elmMargin += Math.ceil(parseFloat(getComputedStyle(item).marginBottom));
      return elmHeight + elmMargin;
    };

    var buildBreaker = function buildBreaker(content) {
      var breakerSpan = document.createElement('span');
      breakerSpan.classList.add('item', 'break');
      content.appendChild(breakerSpan);
    };

    var buildItem = function buildItem(item) {} // const rand = Math.floor(( Math.random() * 360) + 150);
    // item.style.minHeight = rand+'px';
    // item.style.backgroundColor = 'hsl('+rand+',50%,75%)';
    // @link: https://davidwalsh.name/javascript-debounce-function
    ;

    var debounce = function debounce(func, wait, immediate) {
      var timeout;
      return function () {
        var context = this,
            args = arguments;

        var later = function later() {
          timeout = null;
          if (!immediate) func.apply(context, args);
        };

        var callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
      };
    };

    if ($(".news").length) {
      var calculateMasonryHeight = function calculateMasonryHeight(items) {
        var index = 0;
        var columns = [];
        items.forEach(function (item) {
          if (!columns[index]) {
            columns[index] = getHeight(item);
          } else {
            columns[index] += getHeight(item);
          }

          index === maxCount - 1 ? index = 0 : index++;
        });
        var maxHeight = Math.max.apply(Math, columns);
        container.style.height = maxHeight + 'px';
      };

      var items = Array.prototype.slice.call(document.querySelectorAll('.news-item'));
      var container = document.querySelector('.news-container');
      var maxCount = container.dataset.columns;
      var recalcMasonryHeight = debounce(function () {
        calculateMasonryHeight(items);
      }, 250);
      window.addEventListener('resize', recalcMasonryHeight);
      items.forEach(function (item) {
        buildItem(item);
      });

      for (var i = 1; i <= maxCount; i++) {
        buildBreaker(container);
      }

      ;
      calculateMasonryHeight(items);
    }

    ;
    ;
  } // $('.click-open-mobile').on('click', function(){
  //
  // });


  $('.click-open-mobile').on('click', function () {
    $('.layout-region-checkout-secondary').toggleClass('showcheckout');

    if ($('.layout-region-checkout-secondary').hasClass('showcheckout')) {
      $('.path-checkout .layout-region-checkout-secondary').parent().find('.left').addClass('changeArrow');
    } else {
      $('.path-checkout .layout-region-checkout-secondary').parent().find('.left').removeClass('changeArrow');
    }
  });
  $('.pdp-primary-image').on('init', function (event, slick) {
    slick.$slider.find('img').first().on('load', function () {
      $(window).trigger('resize');
    });
  }); // PDP main image slider
  //Slick slider initialize

  $('.pdp-primary-image').slick({
    lazyLoad: 'ondemand',
    arrows: false,
    dots: false,
    infinite: true,
    speed: 500,
    autoplay: false,
    slidesToShow: 1,
    slidesToScroll: 1,
    adaptiveHeight: true,
    responsive: [{
      breakpoint: 1024,
      settings: {
        dots: true
      }
    }]
  }).on('lazyLoaded', function (event, slick, image, imageSource) {
    slick.resize();
  }).on("afterChange", function (event, slick, currentSlide, nextSlide) {
    if ($('.slick-current').hasClass('embed-player')) {
      $(".slick-list").toggleClass('iframeHeight');
    } else {
      $(".slick-list").removeClass('iframeHeight');
    }
  }).on("beforeChange", function (event, slick, currentSlide, nextSlide) {
    $('iframe').each(function () {
      $(this)[0].contentWindow.postMessage('{"event":"command","func":"' + 'stopVideo' + '","args":""}', '*');
    });

    if ($('.slick-current').hasClass('pdp-video')) {
      $('.pdp-video.slick-current')[0].pause();
    }
  }); //On click of slider-nav childern,
  //Slick slider navigate to the respective index.

  $('.pdp-primary-image-reel > div').click(function () {
    $('.pdp-primary-image').slick('slickGoTo', $(this).index());
  });
  $('.comment-section .submit-review').on('click', function () {
    $('.review-form,.comment-form ').slideDown('slow').css('display', 'block');
    $('html, body').animate({
      scrollTop: $(".review-form").first().offset().top - 200
    }, 900);
    $('.comment-form').find('#edit-name').attr('placeholder', 'Name'); //$('.path-product .signup_block').css('background','#fff');
  }); // quantity field

  var incrementPlus;
  var incrementMinus;
  var buttonPlus = $(".btn-increment");
  var buttonMinus = $(".btn-decrement");
  var incrementPlus = buttonPlus.click(function () {
    var $n = $(this).parent(".numeric-stepper").find(".numeric-stepper__input");
    var incrementAmount = Number($n.val());
    var maxCount = $('.numeric-stepper__input').attr('data-max');

    if (incrementAmount < maxCount) {
      $n.val(incrementAmount + 1);
    }
  });
  var incrementMinus = buttonMinus.click(function () {
    var $n = $(this).parent(".numeric-stepper").find(".numeric-stepper__input");
    var amount = Number($n.val());

    if (amount > 1) {
      $n.val(amount - 1);
    }
  });
  $('.numeric-stepper__input').on('keypress keydown', function (e) {
    e.preventDefault();

    if (e.which === 38 || e.which === 40) {
      e.preventDefault();
    }
  }); // if the quantity is zero

  $('.button--add-to-cart').on('click', function () {
    var incrementAmount = Number($('.numeric-stepper__input').val());
    var maxCount = $('.numeric-stepper__input').attr('data-max');

    if ($('.numeric-stepper__input').val() == 0) {
      $('.numeric-stepper').once().after('<p class="validation">Quanity field should not be empty</p>');
      return false;
    } else if (incrementAmount > maxCount) {
      $('.numeric-stepper').once().after('<p class="validation">Value should be less than or equal to ' + maxCount + '</p>');
      return false;
    } else {
      $('.numeric-stepper').next('p.validation').remove();
    }
  });
  var videocomponent = $('.path-product').find('.half-tone-background');
  videocomponent.parent().find('.header.header_large').addClass('videoheader');
  $(document).ajaxComplete(function (event, xhr, settings) {
    var getUrl = settings.url; //var removeproduct  = settings.extraData._triggering_element_name.substring(0,38);

    if (xhr.status == 200 && settings.extraData != undefined && settings.extraData._triggering_element_value !== 'undefined' && settings.extraData._triggering_element_name == 'product_sku_purchase_date_table_add' && getUrl.indexOf('register')) {
      var what = settings.extraData._triggering_element_name.substring(0, 38);

      $('.product-registration-form').on('click', '.form-submit', function () {
        $('#product_sku_purchase_date_table .form-text').each(function () {
          if ($(this).val() == "") {
            $('.product-registration-form .product-model-number-label').next('.validation').remove();
            $(this).parent().next('.validation').remove();
            $(this).parent().parent().removeClass('form-item--error').addClass('form-item--error');
            var text_value = $(this).parent().parent().parent().text();
            $(this).parent().after('<p class="validation">' + text_value + ' field is required</p>');
          } else {
            $(this).parent().next('.validation').remove();
            $(this).parent().parent().removeClass('form-item--error');
          }
        });

        if ($(".form-item--error").length) {
          $('html, body').animate({
            scrollTop: $(".form-item--error").first().offset().top - 200
          }, 300);
        }
      });
    }

    if (xhr.status == 200 && settings.extraData != undefined && settings.extraData._triggering_element_value !== 'undefined' && settings.extraData._triggering_element_name.substring(0, 38) == 'product_sku_purchase_date_table_remove' && getUrl.indexOf('register')) {
      var removebutton = jQuery('.webform-multiple-table .parent_product_sku_purchase_date_table_remove');

      if (removebutton.length === 1) {
        jQuery('.webform-multiple-table .parent_product_sku_purchase_date_table_remove').addClass('removecrossbutton');
      }
    }
  });
  $('.pdp-finishes-list .finish-class').on('click', function () {
    $('.site-header__nav .header__nav--prime > li a').removeClass('active');
  });
  $(".registerbtn .btn").on('click', function () {
    $('html, body').animate({
      scrollTop: $(".bilt").offset().top - 90
    }, 0);
  });
})(jQuery);

var checkoutLabels = function () {
  var handleFocus = function handleFocus(e) {
    var target = e.target;
    target.parentNode.classList.add('active');
    var placeholder = target.getAttribute('data-placeholder');

    if (placeholder) {
      target.setAttribute('placeholder', target.getAttribute('data-placeholder'));
    }
  };

  var handleBlur = function handleBlur(e) {
    var target = e.target;

    if (!target.value) {
      target.parentNode.classList.remove('active');
    }

    target.removeAttribute('placeholder');
  };

  var bindEvents = function bindEvents(element) {
    var floatField = element.querySelector('input');
    floatField.addEventListener('focus', handleFocus);
    floatField.addEventListener('input', handleFocus);
    floatField.addEventListener('blur', handleBlur);
  };

  var init = function init() {
    var labelContainers = document.querySelectorAll('.path-checkout .js-form-type-textfield, .path-checkout .js-form-type-number, .path-checkout .js-form-type-email, .path-checkout .js-form-item-phone-number, .path-checkout .js-form-type-number, .path-checkout .js-form-type-textfield'); //$('#shipping-details-form .form-text').focus();

    for (var i = 0; i < labelContainers.length; i++) {
      var element = labelContainers[i];

      if (element.querySelector('input').value) {
        element.classList.add('active');
      }

      bindEvents(element);
    }
  };

  return {
    init: init
  };
}();

checkoutLabels.init();
jQuery(window).on('load', function () {
  var removebutton = jQuery('.webform-multiple-table .parent_product_sku_purchase_date_table_remove');

  if (removebutton.length === 1) {
    jQuery(this).addClass('removecrossbutton');
  }
});
jQuery(window).on('load', function () {
  if (window.location.href.indexOf("#biltid") > -1) {
    jQuery('html, body').animate({
      scrollTop: jQuery(".bilt").offset().top - 90
    }, 2000);
  }
});
var firstwave = {};

(function ($, _Drupal) {
  firstwave.commonFunctions = {
    FormValidation: function FormValidation(Form) {
      var value = 0,
          regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/,
          phone_number = /^(\+\d{1,2}\s?)?1?\-?\.?\s?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$/i,
          zip_code = /^([a-zA-Z0-9]){3,6}$/,
          alphanumeric = /^[a-zA-Z'.\s]{1,40}$/,
          pass_regex = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{8,15}$/,
          name = /^[a-zA-Z ]*$/,
          address = /^[0-9a-zA-Z,\/ ]+$/;
      return $(Form).find(".validation").remove(), $(Form).find(".required, .validate-email").parent().removeClass("form-item--error"), $(Form).find(".required:visible").not(".form-radios,.captcha-validate ,.shipping_option .required").each(function () {
        var inputValue = $(this).val(),
            label = $($("label[for='" + this.id + "']").contents().get(0)).text();

        if (label !== "") {
          "" != inputValue && "_none" != inputValue && 0 != inputValue || (value++, $(this).parent().addClass("form-item--error"), $(this).after("<p class='validation'>" + label + " field is required.</p>"));
        }

        if ($(Form).hasClass('comment-form')) {
          if ($('.comment-form input[name="field_your_recommendation"]:checked').length == 0) {
            $('.comment-form .form-radios').next('.validation').remove();
            $('.comment-form .form-radios').parent().removeClass('form-item--error').addClass('form-item--error');
            $('.comment-form .form-radios').after('<p class="validation"> Your recommendation field is required</p>');
            value++;
          } else {
            $('.comment-form .form-radios').next('.validation').remove();
          }
        }

        if ($(Form).hasClass('product-registration-form')) {
          if ($('.product-registration-form #product_sku_purchase_date_table .product-model-number > input').val() == "") {
            $('.product-registration-form .product-model-number-label').next('.validation').remove();
            $('.product-registration-form .product-model-number-label').parent().removeClass('form-item--error');
            $('.product-registration-form .product-model-number-label').after('<p class="validation">Product Model Number field is required</p>'); //value++;
          }

          if ($('.product-registration-form #product_sku_purchase_date_table .product-purchase-date > input').val() == "") {
            $('.product-registration-form .product-purchase-date').next('.validation').remove();
            $('.product-registration-form .product-purchase-date').parent().removeClass('form-item--error');
            $('.product-registration-form .product-purchase-date').after('<p class="validation">Product Purchase Date field is required</p>'); //value++;
          }

          if ($('.product-registration-form').find('.g-recaptcha')) {
            var rcres = grecaptcha.getResponse();

            if (rcres == "") {
              event.preventDefault();
              $('.product-registration-form .g-recaptcha').next('.validation').remove();
              $('.product-registration-form .g-recaptcha').parent().removeClass('form-item--error').addClass('form-item--error');
              $('.product-registration-form .g-recaptcha').after('<p class="validation">Recaptcha field is required</p>');
              value++;
            } else {
              $('.product-registration-form .g-recaptcha').next('.validation').remove();
            }
          }
        }
      }), $(Form).find("#edit-new-password .required").each(function () {
        $(this).val();
        var inputValue1 = $("input[name*='new_password[pass1]']").val(),
            inputValue2 = $("input[name*='new_password[pass2]']").val();
        $($("label[for='" + this.id + "']").contents().get(0)).text();
        "" != inputValue1 && "" != inputValue2 && inputValue1 !== inputValue2 && (value++, $(this).addClass("form-item--error"), $("#edit-new-password-pass2").after("<p class='validation confirm-password'>Your password and confirm password do not match</p>"));
      }), $(Form).find(".password.required").each(function () {
        var inputValue1 = $(".new_pass").val(),
            inputValue2 = $(".check__confirm_pass").val();

        if (!inputValue1.match(pass_regex) && inputValue1 != "") {
          $(this).parent().addClass("form-item--error");
          $(".new_pass").after("<p class='validation password-validation'>Password should have atleast minimum 8 characters, 1 digit and 1 special character and 1 capital letter.</p>");
        } else if (inputValue1 !== inputValue2) {
          $(this).parent().addClass("form-item--error");
          $(".check__confirm_pass").after("<p class='validation confirm-password'>Your password and confirm password do not match</p>");
        }

        $($("label[for='" + this.id + "']").contents().get(0)).text(); // "" == inputValue1.match(pass_regex) ? "" != inputValue1 && "" != inputValue2 && inputValue1 !== inputValue2 && (value++,
        //     $(this).parent().addClass("form-item--error"), $(".check__confirm_pass").after("<p class='validation confirm-password'>Your password and confirm password do not match</p>")) : (value++,
        //     $(this).parent().addClass("form-item--error"), $(".check__confirm_pass").after("<p class='validation password-validation'>Password should have atleast minimum 8 characters, 1 digit and 1 special character.</p>"));
      }), $(Form).find(".validate-phone").each(function () {
        $(Form).find(".validate-phone").removeClass("form-item--error");
        var inputValue = $(this).val();
        $($("label[for='" + this.id + "']").contents().get(0)).text();
        "" == inputValue || inputValue.match(phone_number) || (value++, $(this).after("<p class='validation'>Please enter a valid 10 digit phone number(xxxxxxxxxx).</p>"), $(this).parent().addClass("form-item--error"));
      }), $(Form).find(".validate-name").each(function () {
        $(Form).find(".validate-name").removeClass("form-item--error");
        var inputValue = $(this).val();
        $($("label[for='" + this.id + "']").contents().get(0)).text();
        "" == inputValue || inputValue.match(name) || (value++, $(this).after("<p class='validation'>Please enter a valid Name.</p>"), $(this).parent().addClass("form-item--error"));
      }), $(Form).find(".validate-city").each(function () {
        $(Form).find(".validate-city").removeClass("form-item--error");
        var inputValue = $(this).val();
        $($("label[for='" + this.id + "']").contents().get(0)).text();
        "" == inputValue || inputValue.match(name) || (value++, $(this).after("<p class='validation'>Please enter a valid City.</p>"), $(this).parent().addClass("form-item--error"));
      }), $(Form).find(".validate-address").each(function () {
        $(Form).find(".validate-address").removeClass("form-item--error");
        var inputValue = $(this).val();
        $($("label[for='" + this.id + "']").contents().get(0)).text();
        "" == inputValue || inputValue.match(address) || (value++, $(this).after("<p class='validation'>Please enter a valid Address.</p>"), $(this).parent().addClass("form-item--error"));
      }), $(Form).find(".validate-zipcode").each(function () {
        $(Form).find(".validate-zipcode").removeClass("form-item--error");
        var inputValue = $(this).val();
        $($("label[for='" + this.id + "']").contents().get(0)).text();
        "" == inputValue || inputValue.match(zip_code) || (value++, $(this).after("<p class='validation'>Zip code field is not valid.</p>"), $(this).parent().addClass("form-item--error"));
      }), $(Form).find(".validate-email").each(function () {
        var inputValue = $(this).val();
        regex.test(inputValue) || "" == inputValue || (value++, $(this).after("<p class='validation'>Email field is not valid.</p>"), $(this).parent().addClass("form-item--error"));
      }), $(Form).find(".validate--conf-email").each(function () {
        var emailval = $(Form).find(".validate-email").val();
        var inputValue = $(this).val();

        if (emailval != inputValue) {
          $(this).after("<p class='validation'>This field should be equal to Email Address.</p>"), $(this).parent().addClass("form-item--error");
        }
      }), !(value > 0);
    },
    ScrollTopPosition: function ScrollTopPosition(ScrollPosition) {
      $("html, body").animate({
        scrollTop: ($(ScrollPosition).offset() || {
          "top": NaN
        }).top - 200
      }, 500);
    },
    RegisterProduct: function RegisterProduct() {
      $(".product-registration-form").on('click', '.product-reg-submit', function () {
        var Form = $(".product-registration-form"),
            FormField = firstwave.commonFunctions.FormValidation(Form);
        return FormField || firstwave.commonFunctions.ScrollTopPosition($(Form).find(".form-item--error:first")), FormField;
      });
    },
    CommentForm: function CommentForm() {
      $(".comment-form").on('click', '.form-submit', function () {
        var Form = $(".comment-form"),
            FormField = firstwave.commonFunctions.FormValidation(Form);
        return FormField || firstwave.commonFunctions.ScrollTopPosition($(Form).find(".form-item--error:first")), FormField;
      });
    },
    fw110DSTUpdates: function fw110DSTUpdates() {
      $(".product-update-form").on('click', '.form-submit', function () {
        var Form = $(".product-update-form"),
            FormField = firstwave.commonFunctions.FormValidation(Form);
        return FormField || firstwave.commonFunctions.ScrollTopPosition($(Form).find(".form-item--error:first")), FormField;
      });
    },
    init: function init() {
      firstwave.commonFunctions.ScrollTopPosition(), firstwave.commonFunctions.RegisterProduct(), firstwave.commonFunctions.CommentForm(), firstwave.commonFunctions.fw110DSTUpdates();
    }
  };
  $(document).ready(function () {
    firstwave.commonFunctions.init();
  });
})(jQuery, Drupal);

jQuery(document).ready(function () {
  var oldSrc = jQuery(".paragraph--type--media-module .video iframe").attr("src"); //Get the src of the iframe

  var newSrc = oldSrc.replace("autoplay=1", "autoplay=0"); //Replace "autoplay=1" by "autoplay=0"

  jQuery(".paragraph--type--media-module .video iframe").attr("src", newSrc); //Change the src attr to the new value
});

function enableBtn(response) {
  if (response.length > 1) {
    jQuery('.product-registration-form .captcha').find(".validation").remove();
  } else {
    jQuery('.product-registration-form .captcha').find(".validation").remove();
    jQuery('.product-registration-form .captcha').after("<div class='validation'>Recaptcha is required</div>");
  }
}
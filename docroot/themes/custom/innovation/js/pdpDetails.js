"use strict";

(function ($, Drupal, drupalSettings) {
  Drupal.behaviors.pdpDetails = {
    attach: function attach(context, settings) {
      $(".pdp-finishes-finish").on("click", function () {
        $(".pdp-finishes-finish").each(function (index, value) {
          $(this).removeClass("active");
        });
        $(this).addClass("active");

        if ($(this).hasClass("active")) {
          var finishesType = $(this).find("span.name").html();
          $(".pdp-finishes-title span").html(finishesType);
        }
      });
    }
  };
})(jQuery, Drupal, drupalSettings);
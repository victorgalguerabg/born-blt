"use strict";

/*!
 * Parallax Page Section jQuery Plugin
 *
 *
 *
 */
// the semi-colon before the function invocation is a safety
// net against concatenated scripts and/or other plugins
// that are not closed properly.
;

(function ($, window, document, ScrollMagic, undefined) {
  // Create the defaults once
  var pluginName = "heroParallax",
      defaults = {
    background: null,
    controller: null,
    scene: null
  },
      selectors = {
    background: ".hero-parallax-background"
  }; // The actual plugin constructor

  function Plugin(element, options) {
    this.element = element;
    this.options = $.extend({}, defaults, options);
    this._defaults = defaults;
    this._name = pluginName;
    this.init();
  }

  Plugin.prototype = {
    init: function init() {
      // cache the ScrollMagic controller
      this.options.controller = new ScrollMagic.Controller(); // cache the ScrollMagic scene

      this.options.scene = new ScrollMagic.Scene({
        triggerHook: 0,
        triggerElement: this.element,
        duration: '350%'
      });
      this.options.scene.offset(-85);
      this.options.isEnabled = true;
      var self = this;
      var spacerHeight;
      var heroHeight;
      var ua = navigator.userAgent.toLowerCase();
      var isSafari = ua.indexOf('safari') !== -1 && ua.indexOf('chrome') === -1; // Matrix transforms cause weird artifacts in Safari, so we simply move the background

      var bgTo = isSafari ? {
        yPercent: '-10%',
        ease: Power1.easeInOut
      } : {
        yPercent: '-20%',
        rotationX: 5,
        rotationY: 4,
        scale: 1.105,
        transformOrigin: 'top',
        ease: Power2.easeInOut
      };
      var timeline = new TimelineLite();
      timeline.fromTo($('.hero-parallax-background'), 7, {
        y: 0
      }, bgTo, '-=7').fromTo($('.hero-cpt--1'), 1, {
        yPercent: 0
      }, {
        yPercent: '-70%',
        autoAlpha: 0,
        ease: Linear.easeNone
      }, '-=7').fromTo($('.hero-cpt--2'), 1, {
        yPercent: '+70%'
      }, {
        yPercent: 0,
        autoAlpha: 1,
        ease: Linear.easeNone
      }, '-=6').fromTo($('.hero-cpt--2'), 1, {
        yPercent: 0
      }, {
        yPercent: '-70%',
        autoAlpha: 0,
        ease: Linear.easeNone
      }, '-=5').fromTo($('.hero-cpt--3'), 1, {
        yPercent: '+70%'
      }, {
        yPercent: 0,
        autoAlpha: 1,
        ease: Linear.easeNone
      }, '-=4').fromTo($('.hero-cpt--3'), 1, {
        yPercent: 0
      }, {
        yPercent: '-70%',
        autoAlpha: 0,
        ease: Linear.easeNone
      }, '-=3').fromTo($('.hero-cpt--4'), 1, {
        yPercent: '+70%'
      }, {
        yPercent: 0,
        autoAlpha: 1,
        ease: Linear.easeNone
      }, '-=2').fromTo($('.hero-cpt--4'), 1, {
        yPercent: 0
      }, {
        yPercent: '-70%',
        autoAlpha: 0,
        ease: Linear.easeNone
      }, '-=1');

      function toggleClasses(e) {}

      $('[data-site-header-scroll-link]').click(function () {
        if (window.pageYOffset < document.documentElement.clientHeight * 3.5) {
          spacerHeight = $('.scrollmagic-pin-spacer')[0].offsetHeight;
          heroHeight = $('.hero-parallax')[0].offsetHeight; //self.options.scene.removePin(true);

          $('.scrollmagic-pin-spacer').addClass('spacer-pin-disabled ');
          $('.hero-parallax').addClass('hero-fix-disabled');
          $('.hero-parallax-content ').addClass('is-hidden');
          self.options.isEnabled = false;
          setTimeout(function () {
            window.addEventListener('scroll', reEnable);
          }, 1700);
        }
      });

      function reEnable() {
        if (!self.options.isEnabled) {
          console.log('reenabling');
          setTimeout(function () {
            self.options.isEnabled = true; //self.options.scene.setPin('.hero-parallax');

            $('.scrollmagic-pin-spacer').removeClass('spacer-pin-disabled ');
            $('.hero-parallax').removeClass('hero-fix-disabled');
            var currentPos = $(window).scrollTop();
            $('.hero-parallax-content ').removeClass('is-hidden');
            $(window).scrollTop(currentPos + spacerHeight - heroHeight);
          }, 1700);
          window.removeEventListener('scroll', reEnable);
        }
      }

      this.options.scene //.on('enter', toggleClasses)
      //.on('leave', toggleClasses)
      .on('progress', toggleClasses).setPin('.hero-parallax').setTween(timeline).addTo(this.options.controller);
    }
  }; // A really lightweight plugin wrapper around the constructor,
  // preventing against multiple instantiations

  $.fn[pluginName] = function (options) {
    return this.each(function () {
      if (!$.data(this, "plugin_" + pluginName)) {
        $.data(this, "plugin_" + pluginName, new Plugin(this, options));
      }
    });
  };
})(jQuery, window, document, ScrollMagic);
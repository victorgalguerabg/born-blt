"use strict";

/*!
 * Parallax Page Section jQuery Plugin
 *
 *
 *
 */
// the semi-colon before the function invocation is a safety
// net against concatenated scripts and/or other plugins
// that are not closed properly.
;

(function ($, window, document, ScrollMagic, undefined) {
  // Create the defaults once
  var pluginName = "pageSectionParallax",
      defaults = {
    background: null,
    controller: null,
    scene: null
  },
      selectors = {
    background: "[data-section-parallax-background]"
  }; // The actual plugin constructor

  function Plugin(element, options) {
    this.element = element;
    this.options = $.extend({}, defaults, options);
    this._defaults = defaults;
    this._name = pluginName;
    this.init();
  }

  Plugin.prototype = {
    init: function init() {
      // cache the ScrollMagic controller
      this.options.controller = new ScrollMagic.Controller({
        globalSceneOptions: {
          triggerHook: $(this.element).attr('id') === 'process' ? "onLeave" : "onEnter",
          duration: $(this.element).attr('id') === 'process' ? "300%" : "200%"
        }
      }); // cache the ScrollMagic scene

      this.options.scene = new ScrollMagic.Scene({
        triggerElement: this.element
      }); // find the background DOM element

      this.options.background = $(this.element).find(selectors.background).get(0);
      var tween = {
        y: "-15%",
        ease: Linear.easeNone
      };
      this.options.scene.setTween(this.options.background, tween).addTo(this.options.controller);
    }
  }; // A really lightweight plugin wrapper around the constructor,
  // preventing against multiple instantiations

  $.fn[pluginName] = function (options) {
    return this.each(function () {
      if (!$.data(this, "plugin_" + pluginName)) {
        $.data(this, "plugin_" + pluginName, new Plugin(this, options));
      }
    });
  };
})(jQuery, window, document, ScrollMagic);
"use strict";

/*!
 * Hover Accordion jQuery Plugin
 *
 */
// the semi-colon before the function invocation is a safety
// net against concatenated scripts and/or other plugins
// that are not closed properly.
;

(function ($, window, document, undefined) {
  // Create the defaults once
  var pluginName = "hoverAccordion",
      defaults = {
    section: null
  },
      selectors = {
    container: "[data-hover-accordion-container]",
    section: "[data-hover-accordion-section]"
  }; // The actual plugin constructor

  function Plugin(element, options) {
    this.element = element;
    this.options = $.extend({}, defaults, options);
    this._defaults = defaults;
    this._name = pluginName;
    this.init();
  }

  Plugin.prototype = {
    init: function init() {
      // get the section to manipulate the height
      this.options.section = $(this.element).parents(selectors.section);
      this.resizeSection(); // register the window resize event to resize the accordion section

      $(window).on('resize', this.resizeSection.bind(this));
    },
    resizeSection: function resizeSection(evt) {
      // alias the scope
      var self = this; // get the container element

      var $container = $(this.element).parents(selectors.container);
      var containerHt = $container.outerHeight(); // add 15% extra height to make space for the background image

      var extraHt = containerHt * 0.15;
      var newHeight = containerHt + extraHt; // set the section height to the container height with padding

      this.options.section.height(newHeight);
    }
  }; // A really lightweight plugin wrapper around the constructor,
  // preventing against multiple instantiations

  $.fn[pluginName] = function (options) {
    return this.each(function () {
      if (!$.data(this, "plugin_" + pluginName)) {
        $.data(this, "plugin_" + pluginName, new Plugin(this, options));
      }
    });
  };
})(jQuery, window, document);
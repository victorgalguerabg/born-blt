var gulp = require('gulp');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('gulp-autoprefixer');
var minCss = require('gulp-minify-css')
var rename = require('gulp-rename')
var babel = require('gulp-babel');

var input = 'assets/scss/**/*.scss';
var output = 'css';

var sassOptions = {
  errLogToConsole: true,
  outputStyle: 'expanded'
};

var extname = {
  extname: '.min.css'
}

var autoprefixerOptions = {
  browsers: ['last 2 versions', '> 5%', 'Firefox ESR']
};

gulp.task('sass', function () {
  return gulp
    .src(input)
    .pipe(sourcemaps.init())
    .pipe(sass(sassOptions).on('error', sass.logError))
    .pipe(sourcemaps.write())
    .pipe(autoprefixer(autoprefixerOptions))
    .pipe(gulp.dest(output))
    .pipe(minCss())
    .pipe(rename(extname))
    .pipe(gulp.dest(output));
});
var jsGlobs = 'assets/js/*.js'

gulp.task('js', () =>
  gulp.src(jsGlobs)
    .pipe(babel({
  presets: [
    ['@babel/env', {
      "targets": {
        "browsers": [
          "> 1%",
          "last 3 versions",
          "ie 11"
        ]
      }
    }]
  ]}))
      .pipe(gulp.dest('js'))
    );

gulp.task('watch:js', function () {
  gulp.watch(jsGlobs, gulp.series('js'));
});


gulp.task('watch', function () {
  gulp.watch(input, gulp.series('sass'));
});

gulp.task('copyfonts', function() {
  return gulp.src('assets/fonts/*.{ttf,woff,woff2,eot,svg}')
  .pipe(gulp.dest('css/'));
});

gulp.task('copyslickfonts', function() {
  return gulp.src('assets/slick/**/*.{ttf,woff,woff2,eot,svg,gif}')
  .pipe(gulp.dest('css/'));
});

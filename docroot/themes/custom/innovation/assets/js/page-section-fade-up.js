/*!
 * Parallax Page Section jQuery Plugin
 *
 *
 *
 */

// the semi-colon before the function invocation is a safety
// net against concatenated scripts and/or other plugins
// that are not closed properly.
;(function($, window, document, ScrollMagic, undefined) {

	// Create the defaults once
	var pluginName = "pageSectionFadeUp",
		defaults = {
			background: null,
			controller: null,
			scene: null,
		},
		selectors = {
			heading: "[data-section-fade-up-heading]",
			text: "[data-section-fade-up-text]"
		};

	// The actual plugin constructor
	function Plugin(element, options) {
		this.element = element;
		this.options = $.extend({}, defaults, options);

		this._defaults = defaults;
		this._name = pluginName;

		this.init();
	}

	Plugin.prototype = {

		init: function() {
			var transition = this;
			// cache the ScrollMagic controller
			transition.options.controller = new ScrollMagic.Controller({
				globalSceneOptions: {
					triggerHook: "onEnter",
					duration: "100%"
				}
			});

			// cache the ScrollMagic scene
			transition.options.scene = new ScrollMagic.Scene(
				{
					triggerElement: this.element
				}
			);

			var offset = ($(transition.element).outerHeight() / 2) > 1260
				? 260
				: $(transition.element).outerHeight() / 2;
			transition.options.scene.offset(offset / 2);

			// find the background DOM element
			var heading = $(transition.element).find(selectors.heading).get(0);
			var text = $(transition.element).find(selectors.text).get(0);

			$(document).ready(function () {
			function toggleClasses(e) {
				if (e.type === 'enter') {
					$(heading).add(text).addClass('fade-up');
				}
				else if (e.type === 'shift') {
					$(heading).add(text).addClass('fade-up');
				}
				else if (e.type === 'leave' && e.scrollDirection === 'REVERSE') {
					$(heading).add(text).removeClass('fade-up');
				}

			}


			transition.options.scene
				.on('enter', toggleClasses)
				.on('leave', toggleClasses)
				.on('shift', toggleClasses)
				.addTo(transition.options.controller);
		});
		}
	};


	// A really lightweight plugin wrapper around the constructor,
	// preventing against multiple instantiations
	$.fn[pluginName] = function(options) {
		return this.each(function() {
			if (!$.data(this, "plugin_" + pluginName)) {
				$.data(this, "plugin_" + pluginName,
					new Plugin(this, options));
			}
		});
	};

})(jQuery, window, document, ScrollMagic);

/*!
 * Parallax Page Section jQuery Plugin
 *
 *
 *
 */

// the semi-colon before the function invocation is a safety
// net against concatenated scripts and/or other plugins
// that are not closed properly.
;(function($, window, document, ScrollMagic, undefined) {
	
	// Create the defaults once
	var pluginName = "pageSectionFadeLeft",
		defaults = {
			background: null,
			controller: null,
			scene: null,
		},
		selectors = {
			heading: "[data-section-fade-left-heading]",
			item: "[data-section-fade-left-item]"
		};
	
	// The actual plugin constructor
	function Plugin(element, options) {
		this.element = element;
		this.options = $.extend({}, defaults, options);
		
		this._defaults = defaults;
		this._name = pluginName;
		
		this.init();
	}
	
	Plugin.prototype = {
		
		init: function() {
			// cache the ScrollMagic controller
			this.options.controller = new ScrollMagic.Controller({
				globalSceneOptions: {
					triggerHook: "onEnter",
					duration: "100%"
				}
			});
			
			// cache the ScrollMagic scene
			this.options.scene = new ScrollMagic.Scene(
				{
					triggerElement: this.element
				}
			);
			
			this.options.scene.offset($(this.element).outerHeight() / 3);
			
			// find the background DOM element
			var heading = $(this.element).find(selectors.heading).get(0);
			var item = $(this.element);
			
			function toggleClasses(e) {
				if (e.type === 'enter') {
					$(item).addClass('fade-left');
				}
				else if (e.type === 'leave' && e.scrollDirection === 'REVERSE') {
					$(item).removeClass('fade-left');
				}
			}
			
			this.options.scene
				.on('enter', toggleClasses)
				.on('leave', toggleClasses)
				.addTo(this.options.controller);
		}
	};
	
	// A really lightweight plugin wrapper around the constructor,
	// preventing against multiple instantiations
	$.fn[pluginName] = function(options) {
		return this.each(function() {
			if (!$.data(this, "plugin_" + pluginName)) {
				$.data(this, "plugin_" + pluginName,
					new Plugin(this, options));
			}
		});
	};
	
})(jQuery, window, document, ScrollMagic);

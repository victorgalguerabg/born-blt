// Captcha Validation for webform

var captchaFlag = 0;
var formFlag = 0;
//Captcha Success Validation.
function enableBtn() {
	if(formFlag === 1) {
		jQuery('#webform-submission-schedule-and-request-add-form #edit-actions-submit').attr("disabled", false);
	} else {
		jQuery('#webform-submission-schedule-and-request-add-form #edit-actions-submit').attr("disabled", true);
	}
	captchaFlag = 1;
}

//capptcha expired validation.
function captchaExpired() {
	captchaFlag = 0;
	jQuery('#webform-submission-schedule-and-request-add-form #edit-actions-submit').attr("disabled", true);
}

(function($, Drupal, drupalSettings) {
	Drupal.behaviors.deltaTheme = {
		attach: function(context, settings) {

				$('nav ul li a').on('click', function(e) {
					var target = $(this).attr("href");
					if (e.target.hash !== '') {
						e.preventDefault();
						$('html, body').stop().animate({
							scrollTop: ($(target).offset() || {
								"top": NaN
							}).top - 106
						}, 600);
						$('nav ul li a.active').removeClass('active');
						$(this).addClass('active');
					}
				});

					$('.home-page .mainmenu li a').on("click",function() {
						if ($(window).width() < 1024) {
							$('.home-page .mainmenu').removeClass('megamenu-open');
							$('.home-page .toggle-expand').removeClass('toggle-expand__open');
							$('body').removeClass('body-no_scroll');
					}
				});

				// Triggers Google analytics scripts for Outbound Links.
				$(".outbound-link").once().on("click", function(){
					var linkName = $(this).text();
					var url = window.location.href;     // Returns full URL (https://example.com/path/example.html)
					dataLayer.push({
						'event': 'outBoundLink',
						'category': 'outbound link click',
						'linkname': linkName,
						'pageurl': url,
					});
				});

				// Triggers Google Analytics scripts for Contact Links.
				$(".contact-ga-link").once().on("click", function(){
					var linkName = $(this).text();
					var url = window.location.href;     // Returns full URL (https://example.com/path/example.html)
					dataLayer.push({
					  'event': 'contactLink',
					  'category': 'contact link click',
					  'linkname': linkName,       // Eg: submit an enquiry
					  'pageurl': url,
		      });
				});

				// Triggers Google Analytics scripts for Download Links.
				$(".download-ga-link").once().on("click", function(){
					var extension = $(this).data("extension");
					var url = window.location.href;     // Returns full URL (https://example.com/path/example.html)
					dataLayer.push({
					  'event': 'fileDownload',
					  'category': 'File Downloads',
					  'extensionname': extension,       // Eg: xls, doc, .txt etc
					  'pageurl': url,
	      	});
				});


			$(window).scroll(function() {
				var scrollDistance = $(window).scrollTop();

				// Assign active class to nav links while scolling
				$('.page-section').each(function(i) {
					if ($(this).position().top - 106 <= scrollDistance) {
						$('nav ul li a.active').removeClass('active');
						$('nav ul li a').eq(i).addClass('active');
					}
				});
			}).scroll();

			$(window).scroll(function(){
				var sticky = $('header'),
				scroll = $(window).scrollTop();

				if (scroll >= 38) sticky.addClass('headerfixed');
				else sticky.removeClass('headerfixed');
			});

			//mobile menu
			$('.toggle-expand').on('click',function() {
				if ($('.mainmenu').hasClass('megamenu-open')) {
					$('.mainmenu').removeClass('megamenu-open');
					$('.toggle-expand').removeClass('toggle-expand__open');
					$('body').removeClass('body-no_scroll');
				}else {
					$('.mainmenu').addClass('megamenu-open');
					$('.toggle-expand').addClass('toggle-expand__open');
					$('body').addClass('body-no_scroll');
				}
			});

			// Disabling user input for From date.
			$('#edit-from, #edit-to').keypress(function(e) {
            return false;
      });

			// Disabling user input for To date.
      $('#edit-from, #edit-to').keydown(function(e) {
          return false;
      });

			//Phone Number Formatter
			$('#edit-phone-number').keyup(function () {
				$(this).val($(this).val().replace(/(\d{3})\-?(\d{3})\-?(\d{4})/, '$1-$2-$3'));
			});

			//addind attribute to recaptcha
					$('.g-recaptcha').attr('data-expired-callback','captchaExpired')


			//Form Validation
					$('#webform-submission-schedule-and-request-add-form .form-submit').attr("disabled", true);

					$("#webform-submission-schedule-and-request-add-form #edit-requestor,#webform-submission-schedule-and-request-add-form #edit-phone-number,#webform-submission-schedule-and-request-add-form #edit-email,#webform-submission-schedule-and-request-add-form #edit-company-name,#webform-submission-schedule-and-request-add-form #edit-from,#webform-submission-schedule-and-request-add-form #edit-to,#webform-submission-schedule-and-request-add-form #edit-number-of-attendees").on("click change keyup focusout", function() {

						var requestregex  = /^[a-zA-Z,\/ ]+$/;
						var phoneregex = /^[0-9]{3}-[0-9]{3}-[0-9]{4}$/;
						var emailregex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
						var companynameregex = /^[0-9a-zA-Z,\/ ]+$/;

						if($("input[name='requestor']").val() != "" && requestregex.test($("input[name='requestor']").val()) &&
						$("input[name='email']").val() != "" && emailregex.test($("input[name='email']").val()) &&
						$("input[name='company_name']").val() != "" && companynameregex.test($("input[name='company_name']").val()) &&
						$("input[name='phone_number']").val() != "" && phoneregex.test($("input[name='phone_number']").val()) &&
						$("input[name='from']").val() != "" && $("input[name='to']").val() != "" && $("input[name='number_of_attendees']").val() != "" ) {
							formFlag = 1 ;
							if(captchaFlag === 1 ) {
								$('#webform-submission-schedule-and-request-add-form #edit-actions-submit').attr("disabled", false);
							}
						}
						else {
							formFlag = 0;
							$('#webform-submission-schedule-and-request-add-form #edit-actions-submit').attr("disabled", true);
						}

					});

			$('#webform-submission-schedule-and-request-add-form input').on('keyup',function(){
				$('#webform-submission-schedule-and-request-add-form #edit-reset-submit').attr("disabled", false);
			})

			$("#webform-submission-schedule-and-request-add-form #edit-requestor").on("change keyup keypress", function() {
					var inputValue = $("#edit-requestor").val();
					var request_regex  = /^[a-zA-Z,\/ ]+$/;
					var name = $(this).attr("name");
					$(this).attr('maxlength','60');

				//Requestor
				if(inputValue == "")  {
					$("#edit-requestor").next(".form-error").remove();
					$("#edit-requestor").after("<p class='form-error'>Requestor field is required </p>");
				} else if(!request_regex.test(inputValue)) {
					$("#edit-requestor").next(".form-error").remove();
					$("#edit-requestor").after("<p class='form-error'>Requestor field is not valid</p>");
				}
				else {
					$("#edit-requestor").next(".form-error").remove();
				}
			});

			//Phone Number
			$("#webform-submission-schedule-and-request-add-form #edit-phone-number").on("change keyup keypress", function() {

				var inputValue = $("#edit-phone-number").val();
				var phone = $(this).attr("name");
				var phone_regex = /^[0-9]{3}-[0-9]{3}-[0-9]{4}$/;
				if( inputValue == "")  {
					$("#edit-phone-number").next(".form-error").remove();
					$("#edit-phone-number").after("<p class='form-error'>Phone Number field is required </p>");

				} else if(!phone_regex.test(inputValue)) {
					$("#edit-phone-number").next(".form-error").remove();
					$("#edit-phone-number").after("<p class='form-error'> Please enter a Phone Number in the following formats: (123) 456-7890, 123-456-7890.</p>");
				}
				else {
					$("#edit-phone-number").next(".form-error").remove();
				}
			});

			//Email Validation
			$("#webform-submission-schedule-and-request-add-form #edit-email").on("change keyup keypress", function() {

				var inputValue = $("#edit-email").val();
				var email = $(this).attr("name");
				var email_regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
				if(inputValue == "")  {
					$("#edit-email").next(".form-error").remove();
					$("#edit-email").after("<p class='form-error'>Email field is required </p>");

				} else if(!email_regex.test(inputValue)) {
					$("#edit-email").next(".form-error").remove();
					$("#edit-email").after("<p class='form-error'>Please enter a valid Email address</p>");
				}
				else {
					$("#edit-email").next(".form-error").remove();
				}
			});

			//Company Name Validation
			$("#webform-submission-schedule-and-request-add-form #edit-company-name").on("change keyup keypress", function() {

				var inputValue = $("#edit-company-name").val();
				var company_name = $(this).attr("name");
				var company_name_regex = /^[0-9a-zA-Z,\/ ]+$/;
				$(this).attr('maxlength','100');

				if(inputValue == "")  {
					$("#edit-company-name").next(".form-error").remove();
					$("#edit-company-name").after("<p class='form-error'>Company Name field is required </p>");

				} else if(!company_name_regex.test(inputValue)) {
					$("#edit-company-name").next(".form-error").remove();
					$("#edit-company-name").after("<p class='form-error'>Company Name field is not valid</p>");
				}
				else {
					$("#edit-company-name").next(".form-error").remove();
				}
			});

			//Dates

			$("#webform-submission-schedule-and-request-add-form #edit-from").on("change keyup keypress", function() {

				var inputValue = $("#edit-from").val();
				var from_date = $(this).attr("name");
				if(inputValue == "")  {
					$("#edit-from").next(".form-error").remove();
					$("#edit-from").after("<p class='form-error'>" + from_date + " field is required </p>");
				}
				else {
					$("#edit-from").next(".form-error").remove();
				}
			});

			//To Date
			$("#webform-submission-schedule-and-request-add-form #edit-to").on("change keyup keypress", function() {

				var inputValue = $("#edit-to").val();
				var to_date = $(this).attr("name");
				if(inputValue == "")  {
					$("#edit-to").next(".form-error").remove();
					$("#edit-to").after("<p class='form-error'>" + to_date + " field is required </p>");
				}
				else {
					$("#edit-to").next(".form-error").remove();
				}
			});

			//Number of attendies
			$("#webform-submission-schedule-and-request-add-form #edit-number-of-attendees").on("change keyup keypress", function() {

				var inputValue = $("#edit-number-of-attendees").val();
				var attendies = $(this).attr("name");
				$(this).attr('min','1');
				$(this).attr('max','100');

				if(inputValue == "")  {
					$("#edit-number-of-attendees").next(".form-error").remove();
					$("#edit-number-of-attendees").after("<p class='form-error'>Number of Attendees field is required </p>");
				} else if (inputValue == 0) {
						$("#edit-number-of-attendees").val("1");
						$("#edit-number-of-attendees").next(".form-error").remove();
				} else if (inputValue.length > 2) {
						$("#edit-number-of-attendees").next(".form-error").remove();
						$("#edit-number-of-attendees").after("<p class='form-error'>Number of Attendees field should be less than 100 </p>");
				} else {
					$("#edit-number-of-attendees").next(".form-error").remove();
				}
			});

			$("#webform-submission-schedule-and-request-add-form #edit-comments-and-or-questions").on("change keyup keypress", function() {
				$(this).attr('maxlength','250');
			});

			//Reset button
			$('#webform-submission-schedule-and-request-add-form #edit-reset-submit').on('click', function() {
				$(this).closest('form').get(0).reset();
      	$(this).closest('form').find("p.form-error").remove();
			});


			$('#webform-submission-schedule-and-request-add-form').on('change', ".hasDatepicker", function() {
				var from_date = $('.form-item-from');
				var to_date = $('.form-item-to');
				var from_date_value = $('#edit-from').val();
				var to_date_value = $('#edit-to').val();
				var selectedDate_from = new Date(from_date_value);
				var selectedDate_to = new Date(to_date_value);
			//	var now1 = new Date();

				if (selectedDate_to < selectedDate_from) {
					to_date.next(".form-error").remove();
					to_date.after("<p class='form-error'> Selected date must be greater than the From date</p>");
				}else {
					to_date.next(".form-error").remove();
				}
			});
		}
	};
})(jQuery, Drupal);

var headerTop = jQuery('header').outerHeight();
jQuery('main').css({'padding-top': headerTop + 13 + 'px'});

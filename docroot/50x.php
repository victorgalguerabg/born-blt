<?php
$logo = "";
$host = $_SERVER['HTTP_HOST'];
if (strpos($host, 'deltafaucet.com') !== FALSE) {
  $logo = '/themes/custom/delta_theme/logo_500.svg';
}
if (strpos($host, 'brizo.com') !== FALSE) {
  $logo = '/themes/custom/brizo_theme/logo_500.png';
}
if (strpos($host, 'peerlessfaucet.com') !== FALSE) {
  $logo = '/sites/peerless/themes/custom/peerless/logo_500.jpg';
}
if (strpos($host, 'firstwavelab.com') !== FALSE) {
  $logo = '/themes/custom/innovation/logo_500.png';
}
if (strpos($host, 'brizoanddelta.com') !== FALSE) {
  $logo = '/themes/custom/brizodelta_theme/logo_500.jpg';
}
if (strpos($host, 'deltafaucetcompany.com') !== FALSE) {
  $logo = '/themes/custom/company_theme/logo_500.png';
}
if (strpos($host, 'discoverdfc.com') !== FALSE) {
  $logo = '/themes/custom/discover_theme/logo_500.png';
}
?>

<!DOCTYPE html>
<html lang="en" >
<head>
  <meta charset="UTF-8">
  <title>500 - Internal Error</title>
  <meta name=viewport content="width=device-width, initial-scale=1" />
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Merriweather:400,400i" /><link rel="stylesheet" href="/50x_style.css">

</head>
<body>
<!-- partial:index.partial.html -->
<div class="wrapper">
<div class="box">
<img src='<?php echo $logo; ?>' width="200">
<h1>We have a problem</h1>
<p>Our team is working on this</p>
<br>
<p><a href="/">Click here to try again</a></p>
</div>
</div>
<!-- partial -->
  
</body>
</html>
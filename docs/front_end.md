# Front End Team

The Drupal themes are located at /docroot/themes/custom

Things to know...
- The Theme .info.yml file gives you info about the theme
- The Theme .libraries.yml file gives you info about any libraries being used.
- There is a DEBUG setting at /docroot/sites/delta/services.yml which will aid in identifying the templates being used.  See below for configuration notes


- Drupal uses the theme's /templates folder for all output.
- The tempates in the /templates folder OVERRIDE the templates provided in modules and from the base theme - /core/themes/stable/templates folder.

- Files in the /dist folder are generated when compiled.  Do not edit these files - as they will be overwritten.

- CSS naming - utilize the BEM methodoligy
 - http://getbem.com/

- Do not use path names for a class.  Instead use Style types, Paragraph Types, Region Names, Node Types, etc.


# Pattern Libraries

Delta & Brizo themes use a Pattern Lab in conjunction with the Drupal theme.

The current URLs for the Pattern labs are:
Delta: http://prddeltapatternlab.s3.amazonaws.com/pattern-lab/public/index.html
Brizo: http://stgdeltapatternlab.s3-website-us-east-1.amazonaws.com/build/pattern-lab/public/index.html

# Install Code

## Clone the repo to your local machine.
git clone --branch develop git@bitbucket.org:borngroup/born-blt.git delta_multisite

## Enter the new directory and run all of the Lando commands from the project directory.
```
cd delta_multisite/docroot/themes/delta_theme
```

## Install NPM (if needed)

### Check if you have v10.16.0 of node.js installed
node -v
v10.16.0

## If node.js is not installed...
## Install Node Version Manager (NVM)
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.34.0/install.sh | bash

## Make nvm executable with the command nvm
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm

## Install node 10.16.0 
```
nvm install 10.16.0
```

# Install Theme Dependencies

```
npm install  # This will take a while
```

## Serve Pattern Lab

```
gulp compile  # Creates style.css
gulp serve    # Launches the server
```

Output will look like:
```
[15:37:41] Using gulpfile ~/Sites/delta_multisite/docroot/themes/custom/delta_theme/gulpfile.js
[15:37:41] Starting 'css'...
[15:37:41] Starting 'scripts'...
[15:37:41] Finished 'scripts' after 1.59 ms
[15:37:41] Starting 'watch:pl'...
[15:37:41] Finished 'watch:pl' after 484 ms
Browserslist: caniuse-lite is outdated. Please run next command `yarn upgrade`
[15:38:00] Finished 'css' after 20 s
[15:38:00] Starting 'serve'...
[15:38:08] Finished 'serve' after 7.68 s
[Browsersync] Access URLs:
 ------------------------------------------------------
    Local: http://localhost:3000/pattern-lab/public/
 External: http://192.168.1.98:3000/pattern-lab/public/
 ------------------------------------------------------
[Browsersync] Serving files from: ./
```

(These are older versions - but serve as examples of what a deployed Pattern Lab will look like in a local environment)

# Drush

It is very helpful to use a Command Line Interface to execute Drupal commands.  If you do not have Drush configured, you can log into the Drupal instance, and execute the commands from the Drupal interface.

```
drush cc
```

```
drush uli  #Gets you the log in
```



## Usage

## How to enable DEBUG mode

Edit /docroot/sites/delta/services.yml

```
  twig.config:
    # Twig debugging:
    #
    # When debugging is enabled:
    # - The markup of each Twig template is surrounded by HTML comments that
    #   contain theming information, such as template file name suggestions.
    # - Note that this debugging markup will cause automated tests that directly
    #   check rendered HTML to fail. When running automated tests, 'debug'
    #   should be set to FALSE.
    # - The dump() function can be used in Twig templates to output information
    #   about template variables.
    # - Twig templates are automatically recompiled whenever the source code
    #   changes (see auto_reload below).
    #
    # For more information about debugging Twig templates, see
    # https://www.drupal.org/node/1906392.
    #
    # Not recommended in production environments
    # @default false
    debug: TRUE
    # Twig auto-reload:
    #
    # Automatically recompile Twig templates whenever the source code changes.
    # If you don't provide a value for auto_reload, it will be determined
    # based on the value of debug.
    #
    # Not recommended in production environments
    # @default null
    auto_reload: TRUE
    # Twig cache:
    #
    # By default, Twig templates will be compiled and stored in the filesystem
    # to increase performance. Disabling the Twig cache will recompile the
    # templates from source each time they are used. In most cases the
    # auto_reload setting above should be enabled rather than disabling the
    # Twig cache.
    #
    # Not recommended in production environments
    # @default true
    cache: FALSE
  ```

